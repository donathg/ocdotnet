﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sharedcode.BLL
{
    public class AankoopIM
    {
        public int Id { get; set; }
        public DateTime? ClosedDate { get; set; }

        public int ClientId { get; set; }

        public String ClientRijksregisternummer { get; set; }
        public String ClientFirstName { get; set; }
        public String ClientLastName { get; set; }

        public String ClientFullName { get { return ClientFirstName + " " + ClientLastName; } }

        public String ClientOndersteuningsvorm { get; set; }
        public String ArtikelInfo { get; set; }
        
        public String Afleverplaats { get; set; }
        public int ArtikelId { get; set; }

        public String ArtikelNaam { get; set; }
        public String ArtikelMaat { get; set; }
        public String ArtikelKleur { get; set; }

        public String ArtikelNr { get; set; }

        public String LeefgroepInfo { get; set; }

        public double ArtikelPrijs { get; set; }

        public int Aantal { get; set; }

        public double Aankoopbedrag { get { return ArtikelPrijs * Aantal; } }

        public string MainLeefgroep { get; internal set; }
    }
}
