﻿
using sharedcode.common;
using sharedcode.CommonObjects;
using sharedcode.DAL;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;

using System.Linq;

using System.Text.RegularExpressions;


namespace sharedcode.BLL
{
    public class WerkopdrachtEmailAddress
    {
        public bool isChecked { get; set; }
        public String eMailadress { get; set; }
        public bool isGroupEmailAddress { get; set; }
        public int? userId { get; set; }
        public String name { get; set; }
    }
    public class Dienst
    {
        public String Code { get; set; }
        public String Naam { get; set; }
    }
    public enum WerkopdrachtUserType
    {
        normal,
        supervisor,
        excecuter
    }
    public enum WerkopdrachtType
    {
        Werkaanvraag,
        Werkbon,
    }
    public class StatusHistoriekItem
    {
        string _status;
        string _opmerking;
        string _modifier;
        string _datum;
        public string Status
        {
            get
            {
                return _status;
            }
            set
            {
                _status = value;
            }
        }
        public string Opmerking
        {
            get
            {
                return _opmerking;
            }

            set
            {
                _opmerking = value;
            }
        }
        public string Modifier
        {
            get
            {
                return _modifier;
            }
            set
            {
                _modifier = value;
            }
        }
        public string Datum
        {
            get
            {
                return _datum;
            }
            set
            {
                _datum = value;
            }
        }
    }
    public enum WerkopdrachtStatusType
    {
        wb_1_nieuwe_werkbon = 1,
        wb_2_materiaal_besteld = 2,
        wb_3_in_uitvoering = 3,
        wb_4_afgewerkt = 4,
        wb_10_gesloten = 10, //enkel supervisor kan WB sluiten
        wa_5_nieuwe_aanvraag = 5,
        wa_6_gelezen = 6,
        wa_7_goedgekeurd = 7,
        wa_8_afgewezen = 8,
        wa_9_gesloten = 9,
    }
    public class WerkopdrachtItem : INotifyPropertyChanged
    {
        public WerkopdrachtItem(String dienst)
        {
            _doNotify = true;
            this.Groepen = new ObservableCollection<string>();
            this.Medewerkers = new ObservableCollection<User>();
            this.PrioriteitId = 1; //low 
            this.Dienst = dienst;
            SetDefaultStatus();
            SetDefaultType();
        }

        //gelezen ?
        private DateTime? _readDate;
        public void SetDefaultType()
        {
            //      this.TypeId = this.Dienst == "TECHD" ? 1 /*herstelling*/ : 5 /*Computerprobleem*/;this.TypeId 
            this.TypeId = null;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="type">WA or WB</param>
        public WerkopdrachtItem(int? parentId, String dienst)
            : this(dienst)
        {
            this._aanvraagdatum = DateTime.Now;
            this.Aanvrager = GlobalData.Instance.LoggedOnUser.FirstName + " " + GlobalData.Instance.LoggedOnUser.LastName;
            this.AanvragerId = GlobalData.Instance.LoggedOnUser.Id;
        }
        private bool _doNotify = true;
        public bool DoNotify
        {
            get { return _doNotify; }
            set { _doNotify = value; }
        }
        private bool _isModified = false;
        public bool IsModified
        {
            get { return _isModified; }
            set
            {

                if (_isModified != value)
                {
                    _isModified = value;
                    NotifiyPropertyChanged("IsModified");
                }
            }
        }
        public WerkopdrachtItem Clone(bool isNewEntry)
        {
            WerkopdrachtItem clonedWo = new WerkopdrachtItem(this.Dienst);

            if (isNewEntry)
            {
                clonedWo._aanvraagdatum = DateTime.Now;
                clonedWo._aanvrager = GlobalData.Instance.LoggedOnUser.Name;
                clonedWo._aanvragerId = GlobalData.Instance.LoggedOnUser.Id;
                clonedWo.SetDefaultStatus();
                clonedWo.Id = null;
            }
            else
            {
                clonedWo._aanvraagdatum = this._aanvraagdatum;
                clonedWo._aanvrager = this.Aanvrager;
                clonedWo._aanvragerId = this.AanvragerId;
                clonedWo.StatusId = this.StatusId;
                clonedWo.Id = this.Id;
            }
            clonedWo._begindatum = this._begindatum;
            clonedWo._beschrijving = this._beschrijving;
            clonedWo._einddatum = this._einddatum;
            clonedWo._inventarisId = this._inventarisId;
            clonedWo._inventarisText = this._inventarisText;
            clonedWo._locatieId = this._locatieId;
            clonedWo._locatiePath = this._locatiePath;
            clonedWo._parent_id = this._parent_id;
            clonedWo._prioriteitId = this._prioriteitId;
            clonedWo._titel = this._titel;
            clonedWo._typeId = this._typeId;
            clonedWo.Dienst = this.Dienst;

            if (this.WerkopdrachtType == BLL.WerkopdrachtType.Werkbon)
            {
                foreach (User u in this.Medewerkers)
                    clonedWo.Medewerkers.Add(u.Clone());
                clonedWo.Groepen = this.Groepen;
            }
            clonedWo.IsModified = false;
            return clonedWo;
        }
        public void SetDefaultStatus()
        {
            if (this.WerkopdrachtType == BLL.WerkopdrachtType.Werkaanvraag)
                this.StatusId = 5;
            if (this.WerkopdrachtType == BLL.WerkopdrachtType.Werkbon)
                this.StatusId = 1;
            this.SetStatusGewijzigdText(DateTime.Now, GlobalData.Instance.LoggedOnUser.Name);
        }
        private ObservableCollection<User> _medewerkers;
        public ObservableCollection<User> Medewerkers
        {
            get { return _medewerkers; }
            set
            {
                _medewerkers = value;
                NotifiyPropertyChanged("Medewerkers");
                NotifiyPropertyChanged("MedewerkersString");
            }
        }
        private ObservableCollection<String> _groepen;
        public ObservableCollection<String> Groepen
        {
            get { return _groepen; }
            set
            {
                _groepen = value;
                NotifiyPropertyChanged("Groepen");
                NotifiyPropertyChanged("GroepenString");
            }
        }
        private int? _aantalBijlagen;
        private string _dienst;
        public String Dienst
        {
            get { return _dienst; }
            set
            {
                _dienst = value;

            }
        }
        private int? _id;
        public String WoType
        {
            get
            {
                return this.Parent_id == null ? "WA" : "WB";
            }
        }
        public WerkopdrachtType WerkopdrachtType
        {
            get
            {
                return this.Parent_id == null ? WerkopdrachtType.Werkaanvraag : WerkopdrachtType.Werkbon;
            }
        }
        public String Nr
        {
            get
            {
                if (this.WerkopdrachtType == BLL.WerkopdrachtType.Werkaanvraag)
                    return "WA" + Id.GetValueOrDefault().ToString("D5");
                else
                    return "WB" + Id.GetValueOrDefault().ToString("D5");
            }
            set
            {
            }
        }
        public int? Id
        {
            get { return _id; }
            set
            {

                if (_id != value)
                {
                    _id = value;
                    this.IsModified = true;
                    NotifiyPropertyChanged("Id");
                }
            }
        }
        private int? _parent_id;
        public int? Parent_id
        {
            get { return _parent_id; }
            set
            {
                if (_parent_id != value)
                {
                    SetDefaultStatus();
                    _parent_id = value;
                    this.IsModified = true;
                    NotifiyPropertyChanged("Parent_id");
                    NotifiyPropertyChanged("StatusId");

                }
            }
        }
        private string _titel = String.Empty;
        public string Titel
        {
            get { return _titel; }
            set
            {

                if (_titel != value)
                {
                    _titel = value;
                    this.IsModified = true;
                    NotifiyPropertyChanged("Titel");

                }
            }
        }
        private string _beschrijving = String.Empty;
        public string Beschrijving
        {
            get { return _beschrijving; }
            set
            {
                if (_beschrijving != value)
                {
                    _beschrijving = value;
                    this.IsModified = true;
                    NotifiyPropertyChanged("Beschrijving");

                }
            }
        }
        private int? _locatieId;
        public int? LocatieId
        {
            get { return _locatieId; }
            set
            {
                if (_locatieId != value)
                {
                    _locatieId = value;
                    this.IsModified = true;
                    NotifiyPropertyChanged("LocatieId");

                }
            }
        }
        private int? _prioriteitId;
        public int? PrioriteitId
        {
            get { return _prioriteitId; }
            set
            {
                if (_prioriteitId != value)
                {
                    _prioriteitId = value;
                    this.IsModified = true;
                    NotifiyPropertyChanged("PrioriteitId");

                }
            }
        }
        private DateTime? _begindatum;
        public DateTime? Begindatum
        {
            get { return _begindatum; }
            set
            {
                if (_begindatum != value)
                {
                    _begindatum = value;
                    this.IsModified = true;
                    NotifiyPropertyChanged("Begindatum");

                }
            }
        }
        private DateTime? _einddatum;
        public DateTime? Einddatum
        {
            get { return _einddatum; }
            set
            {
                if (_einddatum != value)
                {
                    _einddatum = value;
                    this.IsModified = true;
                    NotifiyPropertyChanged("Einddatum");

                }
            }
        }
        private int? _inventarisId;
        public int? InventarisId
        {
            get { return _inventarisId; }
            set
            {
                if (_inventarisId != value)
                {
                    _inventarisId = value;
                    this.IsModified = true;
                    NotifiyPropertyChanged("InventarisId");

                }
            }
        }
        private String _inventarisText = String.Empty;
        public String InventarisText
        {
            get { return _inventarisText; }
            set
            {
                if (_inventarisText != value)
                {
                    _inventarisText = value;
                    NotifiyPropertyChanged("InventarisText");

                }
            }
        }
        private int? _statusId;
        public int? StatusId
        {
            get { return _statusId; }
            set
            {
                if (_statusId != value)
                {
                    _statusId = value;
                    this.IsModified = true;
                    NotifiyPropertyChanged("StatusId");

                }
            }
        }
        private String _statusOpmerking = String.Empty;
        public String StatusOpmerking
        {
            get { return _statusOpmerking; }
            set
            {
                if (_statusOpmerking != value)
                {
                    _statusOpmerking = value;
                    this.IsModified = true;
                    NotifiyPropertyChanged("StatusOpmerking");

                }
            }
        }
        private String _statusGewijzigdText = String.Empty;
        public String StatusGewijzigdText
        {
            get { return _statusGewijzigdText; }
            set
            {
                if (_statusGewijzigdText != value)
                {
                    _statusGewijzigdText = value;
                    this.IsModified = true;
                    NotifiyPropertyChanged("StatusGewijzigdText");
                }
            }
        }
        public void SetStatusGewijzigdText(DateTime datum, String Fullname)
        {
            StatusGewijzigdText = Fullname + ", " + datum.ToString("dd-MM-yyyy");

        }
        private string _locatiePath = String.Empty;
        public string LocatiePath
        {
            get { return _locatiePath; }
            set
            {
                if (_locatiePath != value)
                {
                    _locatiePath = value;
                    this.IsModified = true;
                    NotifiyPropertyChanged("LocatiePath");

                }
            }
        }
        private int? _aanvragerId;
        public int? AanvragerId
        {
            get { return _aanvragerId; }
            set
            {
                if (_aanvragerId != value)
                {
                    _aanvragerId = value;
                    this.IsModified = true;
                    NotifiyPropertyChanged("AanvragerId");

                }
            }
        }
        private string _aanvrager = String.Empty;
        public string Aanvrager
        {
            get { return _aanvrager; }
            set
            {
                if (_aanvrager != value)
                {
                    _aanvrager = value;
                    this.IsModified = true;
                    NotifiyPropertyChanged("Aanvrager");

                }
            }
        }
        private DateTime _aanvraagdatum;
        public DateTime Aanvraagdatum
        {
            get { return _aanvraagdatum; }
            set
            {
                if (value != null && _aanvraagdatum != value)
                {
                    _aanvraagdatum = value;
                    this.IsModified = true;
                    NotifiyPropertyChanged("Aanvraagdatum");

                }
            }
        }
        public String CreatedByText
        {

            get
            {
                if (!String.IsNullOrWhiteSpace(Aanvrager) && Aanvraagdatum != null)
                    return Aanvrager + ",  " + Aanvraagdatum.ToString("dd-MM-yyyy");
                return String.Empty;
            }
            set
            {


            }

        }
        private String _modifier = String.Empty;
        public String Modifier
        {
            get { return _modifier; }
            set { _modifier = value; }
        }
        private DateTime _modifyDate;
        public DateTime ModifyDate
        {
            get { return _modifyDate; }
            set { _modifyDate = value; }
        }
        public String ModifiedByText
        {

            get
            {
                if (!String.IsNullOrWhiteSpace(Modifier) && ModifyDate != null)
                    return "Laatst gewijzigd door " + Modifier + " om " + ModifyDate.ToString("dd-MM-yyyy HH:mm");
                return String.Empty;
            }
            set
            {


            }

        }
        private int? _typeId;
        public int? TypeId
        {
            get { return _typeId; }
            set
            {
                if (_id != value)
                {
                    _typeId = value;
                    this.IsModified = true;
                    NotifiyPropertyChanged("TypeId");

                }
            }
        }
        public string MedewerkersString
        {
            get
            {
                String s = String.Empty;

                for (int i = 0; i < this.Medewerkers.Count; i++)
                {

                    if (i > 0)
                        s = s + ",";
                    s = s + this.Medewerkers[i].Name;


                }
                return s;

            }
            set
            {


            }

        }
        public string MedewerkersIdString
        {
            get
            {
                String s = String.Empty;

                for (int i = 0; i < this.Medewerkers.Count; i++)
                {

                    if (i > 0)
                        s = s + ",";
                    s = s + this.Medewerkers[i].Id;


                }
                return s;

            }
            set
            {


            }

        }
        public string GroepenString
        {
            get
            {

                String s = String.Empty;

                for (int i = 0; i < this.Groepen.Count; i++)
                {

                    if (i > 0)
                        s = s + ",";
                    s = s + this.Groepen[i];
                }
                return s;


            }
            set
            {


            }

        }
        public int? AantalBijlagen
        {
            get
            {
                return _aantalBijlagen;
            }

            set
            {

                if (_aantalBijlagen.GetValueOrDefault() != value.GetValueOrDefault())
                {
                    _aantalBijlagen = value;
                    NotifiyPropertyChanged("AantalBijlagen");
                }
            }
        }
        private String ModifyDetails
        {
            get
            {
                return "";

            }

        }
        public DateTime? ReadDate
        {
            get
            {
                return _readDate;
            }

            set
            {

                if (_readDate != value)
                {
                    _readDate = value;
                    NotifiyPropertyChanged("ReadDate");
                }
            }
        }
        public String Telefoon { get; set; }
        public String TelefoonIntern { get; set; }
        public String TelefoonGroep { get; set; }

        public String TelefoonInfo
        {
            get

            {
               String t = String.Empty;/*
                if (String.IsNullOrWhiteSpace(Telefoon) == false)
                    t += "tel : " + Telefoon + " ";*/
                if (String.IsNullOrWhiteSpace(TelefoonIntern) == false)
                    t += TelefoonIntern + " ";
             /*   if (String.IsNullOrWhiteSpace(TelefoonGroep) == false)
                    t += "groep : " + TelefoonGroep + " ";*/


                return t;
            }
        }
        void NotifiyPropertyChanged(string property)
        {
            if (PropertyChanged != null && _doNotify)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(property));


            }
        }
        public event PropertyChangedEventHandler PropertyChanged;
    }



    public class PrioriteitItem
    {
        int _id;

        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }
        string _naam;

        public string Naam
        {
            get { return _naam; }
            set { _naam = value; }
        }
    }
    public class TypeItem
    {
        int _id;

        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }
        string _naam;

        public string Naam
        {
            get { return _naam; }
            set { _naam = value; }
        }
        string _dienstCode;
        /// <summary>
        /// TECHD of IT
        /// </summary>
        public string DienstCode
        {
            get { return _dienstCode; }
            set { _dienstCode = value; }
        }
        public bool OnlyForSupervisor { get; set; }
        public bool IsActif { get; set; }
        

    }
    public class StatusItem
    {
        int _id;

        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }
        string _naam;

        public string Naam
        {
            get { return _naam; }
            set { _naam = value; }
        }
        string _categorie;
        public string Categorie
        {
            get { return _categorie; }
            set { _categorie = value; }
        }
    }
    public class DienstItem
    {
        String _code;

        public String Code
        {
            get { return _code; }
            set { _code = value; }
        }
        string _naam;

        public string Naam
        {
            get { return _naam; }
            set { _naam = value; }
        }
        String _groep;

        public String Groep
        {
            get { return _groep; }
            set { _groep = value; }
        }
    }


    public class WerkOpdrachtPrintItem
    {

        public String BeknopteBEschrijvingText { get; set; }
        public String UitgebreideBeschrijvingText { get; set; }
        public String StatusText { get; set; }
        public String StatusOpmerkingText { get; set; }
        public String PrioriteitText { get; set; }
        public String TypeText { get; set; }
        public String LocatieText { get; set; }
        public String InventarisText { get; set; }
        public String AangevraagdDoorText { get; set; }
        public String ToegewezenGebruikersText { get; set; }
        public String ToegewezenGroepenText { get; set; }
        public String DatumBeginText { get; set; }
        public String DatumEindeText { get; set; }

        public String DienstText { get; set; }

        public String NrText { get; set; }
        public WerkOpdrachtPrintItem(WerkopdrachtItem werkAanvraag, WerkopdrachtItem werkopdracht)
        {
            if (werkopdracht.Begindatum.HasValue)
                this.DatumBeginText = werkopdracht.Begindatum.Value.ToString("dd-MM-yyyy");
            if (werkopdracht.Einddatum.HasValue)
                this.DatumEindeText = werkopdracht.Einddatum.Value.ToString("dd-MM-yyyy");
            this.BeknopteBEschrijvingText = werkopdracht.Titel;
            this.UitgebreideBeschrijvingText = werkopdracht.Beschrijving;
            this.ToegewezenGebruikersText = werkopdracht.MedewerkersString;
            this.ToegewezenGroepenText = werkopdracht.GroepenString;
            this.LocatieText = werkopdracht.LocatiePath;
            this.NrText = werkopdracht.Id + "";
            this.PrioriteitText = WerkbonnenBeheerBLL.GetPrioriteitItemById(werkopdracht.PrioriteitId.Value).Naam;
            this.StatusText = WerkbonnenBeheerBLL.GetStatusItemById(werkopdracht.StatusId.Value).Naam;
            this.StatusOpmerkingText = werkopdracht.StatusOpmerking;
            this.TypeText = WerkbonnenBeheerBLL.GetTypeItemById(werkopdracht.TypeId.Value).Naam;
            this.AangevraagdDoorText = werkAanvraag.Aanvrager + " " + werkAanvraag.Aanvraagdatum.ToString("dd-MM-yyyy");
            this.DienstText = WerkbonnenBeheerBLL.GetDienstFullName(werkopdracht.Dienst);
        }

    }

    public class WerkbonnenBeheerSettings2
    {
        public WerkbonnenBeheerSettings2(WerkopdrachtType woType)
        {

            this.woType = woType;
            GroepFilter = new List<string>();
            _subGroepMijnWBFilter = new List<string>();
            _userMijnWBFilter = new List<int>();
            _locationFilter = new List<int>();
        }
        public String Dienst { get; set; }
        public WerkopdrachtUserType UserType { get; set; }
        public WerkopdrachtType woType { get; set; }


        public void AddUserToMyWBFilter(int userId)
        {
            if (!_userMijnWBFilter.Contains(userId))
                _userMijnWBFilter.Add(userId);
        }

        public void AddLocationToFilter(int locId)
        {
            if (!_locationFilter.Contains(locId))
                _locationFilter.Add(locId);
        }
        public void AddLocationToFilter(List<int> locIds)
        {
            if (locIds == null)
                return;
            foreach (int locId in locIds)
            {
                if (!_locationFilter.Contains(locId))
                    _locationFilter.Add(locId);
            }
        }
        /// <summary>
        /// enkel werkopdrachten uit subgroepen Schilder, Loodgieter
        /// </summary>
        /// <param name="subGroep"></param>
        public void AddSubGroupToMyWBFilter(String subGroep)
        {
            if (!_subGroepMijnWBFilter.Contains(subGroep))
                _subGroepMijnWBFilter.Add(subGroep);
        }
        List<String> _groepFilter;

        public List<String> GroepFilter
        {
            get { return _groepFilter; }
            set { _groepFilter = value; }
        }
        List<String> _subGroepMijnWBFilter;
        List<int> _userMijnWBFilter;

        public List<int> UserMijnWBFilter
        {
            get { return _userMijnWBFilter; }
            set { _userMijnWBFilter = value; }
        }
        private List<int> _locationFilter;

        public List<int> LocationFilter
        {
            get { return _locationFilter; }
            set { _locationFilter = value; }
        }

        public List<String> SubGroepMijnWBFilter
        {
            get { return _subGroepMijnWBFilter; }
        }
        public void AddGroepToFilter(String groep)
        {
            if (!GroepFilter.Contains(groep))
                GroepFilter.Add(groep);
        }
        public void SetGroepFilter(List<String> groep)
        {
            GroepFilter = groep;
        }
    }

    public class WerkbonnenBeheerSettings
    {
        List<String> _dienstFilter;
        List<String> _groepFilter;

        public List<String> GroepFilter
        {
            get { return _groepFilter; }
            set { _groepFilter = value; }
        }
        List<String> _subGroepMijnWBFilter;
        List<int> _userMijnWBFilter;

        public List<int> UserMijnWBFilter
        {
            get { return _userMijnWBFilter; }
            set { _userMijnWBFilter = value; }
        }
        private List<int> _locationFilter;

        public List<int> LocationFilter
        {
            get { return _locationFilter; }
            set { _locationFilter = value; }
        }

        public List<String> SubGroepMijnWBFilter
        {
            get { return _subGroepMijnWBFilter; }
        }
        /// <summary>
        /// Welke opdrahten van welke dienst moeten worden geladen? IT, TECHD ?
        /// </summary>
        public List<String> DienstList
        {
            get { return _dienstFilter; }
        }



        /// <summary>
        /// Geef de diensten mee van welke de werkopdrachten geladen moeten worden (IT,TECHD)
        /// </summary>
        public WerkbonnenBeheerSettings()
        {
            _dienstFilter = new List<String>();
            GroepFilter = new List<string>();
            _subGroepMijnWBFilter = new List<string>();
            _userMijnWBFilter = new List<int>();
            _locationFilter = new List<int>();
        }

        /// <summary>
        /// Enkel werkopdrachten van een bepaalde dienst : IT, TECHD
        /// </summary>
        /// <param name="dienst"></param>
        public void AddDienstToFilter(String dienst)
        {
            if (!_dienstFilter.Contains(dienst))
                _dienstFilter.Add(dienst);
        }

        /// <summary>
        /// enkel werkopdrachten van een gebruiker
        /// </summary>
        /// <param name="userId"></param>
        public void AddUserToMyWBFilter(int userId)
        {
            if (!_userMijnWBFilter.Contains(userId))
                _userMijnWBFilter.Add(userId);
        }

        public void AddLocationToFilter(int locId)
        {
            if (!_locationFilter.Contains(locId))
                _locationFilter.Add(locId);
        }
        /// <summary>
        /// enkel werkopdrachten uit subgroepen Schilder, Loodgieter
        /// </summary>
        /// <param name="subGroep"></param>
        public void AddSubGroupToMyWBFilter(String subGroep)
        {
            if (!_subGroepMijnWBFilter.Contains(subGroep))
                _subGroepMijnWBFilter.Add(subGroep);
        }

        /// <summary>
        /// enkel weropdrachten uit Groep 'BOEKH,KKINF'
        /// </summary>
        /// <param name="groep"></param>
        public void AddGroepToFilter(String groep)
        {
            if (!GroepFilter.Contains(groep))
                GroepFilter.Add(groep);
        }
    }
    public class WerkbonnenBeheerBLL : IDisposable /*, IClaraFeyCleanup*/
    {

        private static List<PrioriteitItem> _cachedPrioriteitItems = null;
        private static List<StatusItem> _cachedStatusItems = null;
        private static List<DienstItem> _cachedDienstItems = null;
        private static List<TypeItem> _cachedTypeItems = null;
        WerkbonnenBeheerSettings2 _settings;

        public WerkbonnenBeheerBLL(WerkbonnenBeheerSettings2 settings)
        {
            _settings = settings;
        }



        private static DateTime? dateLastLoaded = null;
        private List<WerkopdrachtItem> _werkaanvragen = new List<WerkopdrachtItem>();

        public List<WerkopdrachtItem> Werkaanvragen
        {
            get { return _werkaanvragen; }
            set { _werkaanvragen = value; }
        }
        private List<WerkopdrachtItem> _werkbonnen = new List<WerkopdrachtItem>();
        public List<WerkopdrachtItem> Werkbonnen
        {
            get { return _werkbonnen; }
            set { _werkbonnen = value; }
        }
        private List<WerkopdrachtItem> _werkaanvragenHist = new List<WerkopdrachtItem>();
        public List<WerkopdrachtItem> WerkaanvragenHist
        {
            get { return _werkaanvragenHist; }
            set { _werkaanvragenHist = value; }
        }
        private List<WerkopdrachtItem> _werkbonnenHist = new List<WerkopdrachtItem>();
        public List<WerkopdrachtItem> WerkbonnenHist
        {
            get { return _werkbonnenHist; }
            set { _werkbonnenHist = value; }
        }

        private List<PrioriteitItem> _prioriteitItems = null;
        public List<PrioriteitItem> PrioriteitItems
        {
            get
            {
                if (_prioriteitItems == null)
                    _prioriteitItems = GetPrioriteitItems();
                return _prioriteitItems;
            }
            set
            {
                _prioriteitItems = value;
            }
        }
        public static List<StatusHistoriekItem> GetStatusHistoriek(int id)
        {
            List<StatusHistoriekItem> list = new List<StatusHistoriekItem>();
            DataTable dt = WerkbonnenBeheerDAL.GetStatusHistoriek(id);
            foreach (DataRow dr in dt.Rows)
            {
                StatusHistoriekItem item = new StatusHistoriekItem()
                {
                    Modifier = dr["voornaam"].ToString() + " " + dr["achternaam"].ToString(),
                    Datum = ((DateTime)dr["wosh_datum"]).ToString("dd-MM-yyyy"),
                    Status = dr["wos_naam"].ToString(),
                    Opmerking = dr["wosh_opmerking"].ToString()
                };
                list.Add(item);
            }
            return list;

        }

        public static PrioriteitItem GetPrioriteitItemById(int id)
        {
            List<PrioriteitItem> items = GetPrioriteitItems();
            return items.First(i => i.Id == id);

        }
        public void Save()
        {
            var modifiedWAItems = from wo in _werkaanvragen where wo.IsModified == true select wo;
            List<WerkopdrachtItem> itemsWA = modifiedWAItems.ToList();
            var modifiedWBItems = from wo in Werkbonnen where wo.IsModified == true select wo;
            List<WerkopdrachtItem> itemsWB = modifiedWBItems.ToList();
            List<WerkopdrachtItem> items = new List<WerkopdrachtItem>();
            items.AddRange(itemsWA);
            items.AddRange(itemsWB);

            if (items.Count > 0)
            {
                try
                {
                    WerkbonnenBeheerDAL.SaveWerkopdrachten(items);
                }
                catch
                {
                    throw;

                }
            }
        }

        public static List<PrioriteitItem> GetPrioriteitItems()
        {
            if (_cachedPrioriteitItems == null)
            {
                _cachedPrioriteitItems = new List<PrioriteitItem>();
                DataTable dt = WerkbonnenBeheerDAL.LoadPrioriteiten();
                foreach (DataRow dr in dt.Rows)
                {
                    PrioriteitItem item = ParsePrioriteitItem(dr);
                    _cachedPrioriteitItems.Add(item);
                }
            }
            return _cachedPrioriteitItems;
        }
        private List<StatusItem> _statusItems = null;
        public List<StatusItem> StatusItems
        {
            get
            {
                if (_statusItems == null)
                    _statusItems = GetStatusItems();
                return _statusItems;
            }
            set
            {
                _statusItems = value;
            }
        }
        public static StatusItem GetStatusItemById(int id)
        {
            List<StatusItem> items = GetStatusItems();
            return (StatusItem)items.First(i => i.Id == id);
        }
        public static List<StatusItem> GetStatusItems()
        {
            if (_cachedStatusItems == null)
            {
                _cachedStatusItems = new List<StatusItem>();
                DataTable dt = WerkbonnenBeheerDAL.LoadStatussen();
                foreach (DataRow dr in dt.Rows)
                {
                    StatusItem item = ParseStatusItem(dr);
                    _cachedStatusItems.Add(item);
                }
            }
            return _cachedStatusItems;
        }


        private List<DienstItem> _dienstItems = null;
        public List<DienstItem> DienstItems
        {
            get
            {
                if (_dienstItems == null)
                    _dienstItems = GetDienstItems();
                return _dienstItems;
            }
            set
            {
                _dienstItems = value;
            }
        }
        public static DienstItem GetDienstItemByCode(String code)
        {
            List<DienstItem> items = GetDienstItems();
            return (DienstItem)items.First(i => i.Code == code);
        }
        public static List<DienstItem> GetDienstItems()
        {
            if (_cachedDienstItems == null)
            {
                _cachedDienstItems = new List<DienstItem>();
                DataTable dt = WerkbonnenBeheerDAL.LoadDiensten();
                foreach (DataRow dr in dt.Rows)
                {
                    DienstItem item = ParseDienstItems(dr);
                    _cachedDienstItems.Add(item);
                }
            }
            return _cachedDienstItems;
        }

        private List<TypeItem> _typeItems = null;
        public List<TypeItem> TypeItems
        {
            get
            {
                if (_typeItems == null)
                    _typeItems = GetTypeItems();
                return _typeItems;
            }
            set
            {
                _typeItems = value;
            }
        }
        public static TypeItem GetTypeItemById(int id)
        {
            List<TypeItem> items = GetTypeItems();
            return (TypeItem)items.First(i => i.Id == id);
        }
        public static List<TypeItem> GetTypeItems()
        {

            if (_cachedTypeItems == null)
            {
                _cachedTypeItems = new List<TypeItem>();
                DataTable dt = WerkbonnenBeheerDAL.LoadTypes();
                foreach (DataRow dr in dt.Rows)
                {
                    TypeItem item = ParseTypeItems(dr);
                    _cachedTypeItems.Add(item);
                }
            }
            return _cachedTypeItems;
        }


        private bool _activeLoaded = false;
        private bool _histLoaded = false;
        public void LoadWerkopdrachten(HistoryType histtype)
        {

            if (histtype == HistoryType.active && _activeLoaded == true)
                return;
            if (histtype == HistoryType.history && _histLoaded == true)
                return;

            DataTable dt = WerkbonnenBeheerDAL.LoadWerkopdrachten(_settings, null, histtype);
            foreach (DataRow dr in dt.Rows)
            {
                WerkopdrachtItem wo = ParseWerkopdrachtItem(dr);
                if (wo.WoType == "WA")
                    _werkaanvragen.Add(wo);
                else
                    _werkbonnen.Add(wo);
            }

            if (histtype == HistoryType.active)
                _activeLoaded = true;
            if (histtype == HistoryType.history)
                _histLoaded = true;

            dateLastLoaded = DateTime.Now;
        }
        public WerkopdrachtItem findWerkopdrachtById(int id)
        {
            foreach (WerkopdrachtItem i in this.Werkaanvragen)
            {
                if (i.Id == id)
                    return i;
            }
            foreach (WerkopdrachtItem i in this.Werkbonnen)
            {
                if (i.Id == id)
                    return i;
            }
            return null;
        }


        /// <summary>
        /// Deze methode gaat eerst kijken wat de hoogste ID is van de werkopdrachten die reeds zijn geladen 
        /// en gaat daarna enkel werkopdrachten laden met een hoger id (maw nieuwe werkopdrachten)
        /// </summary>
        public void LoadWerkopdrachtenNew()
        {

            DataTable dt = WerkbonnenBeheerDAL.LoadWerkopdrachten(_settings, dateLastLoaded, HistoryType.both);
            foreach (DataRow dr in dt.Rows)
            {
                WerkopdrachtItem wo = ParseWerkopdrachtItem(dr);
                WerkopdrachtItem woExisting = findWerkopdrachtById(wo.Id.Value);

                if (wo.WoType == "WA")
                {


                    if (woExisting != null)
                    {
                        int i = this._werkaanvragen.IndexOf(woExisting);
                        this._werkaanvragen[i] = wo;
                    }
                    else
                        this._werkaanvragen.Add(wo);
                }
                else
                {
                    if (woExisting != null)
                    {
                        int i = this._werkbonnen.IndexOf(woExisting);
                        this._werkbonnen[i] = wo;
                    }
                    else
                        this._werkbonnen.Add(wo);
                }
            }
            dateLastLoaded = DateTime.Now;
        }

        private static PrioriteitItem ParsePrioriteitItem(DataRow dr)
        {
            PrioriteitItem item = new PrioriteitItem();
            if (dr.IsNull("wop_id") == false)
                item.Id = (int)dr["wop_id"];
            if (dr.IsNull("wop_naam") == false)
                item.Naam = (String)dr["wop_naam"];
            return item;
        }


        //Status wijzigen naar gelezen; enkel wanneer de supervisor voor de juiste dienst dit leest.
        public void MarkAsRead(WerkopdrachtItem item, bool IsSuperVisor)
        {
          /*  bool IsSuperVisor = false;
            if (item.Dienst == "TECHD")
                IsSuperVisor = GlobalData.Instance.LoggedOnUser.GetAccesType("325") != AccessType.none;
            if (item.Dienst == "IT")
                IsSuperVisor = GlobalData.Instance.LoggedOnUser.GetAccesType("330") != AccessType.none;*/


            if (item.WerkopdrachtType == WerkopdrachtType.Werkaanvraag && item.StatusId == 5/*nieuwe werkaanvraag*/
                && IsSuperVisor)
            {



                item.StatusId = 6;
                SaveStatus(item);
            }

        }

        //WB marker als gelezen 
        public void MarkReadByUser(WerkopdrachtItem item)
        {
            if (item != null && item.WerkopdrachtType == WerkopdrachtType.Werkbon && item.ReadDate.HasValue == false)
            {
                WerkbonnenBeheerDAL.MarkReadByUser(item.Id.GetValueOrDefault(), GlobalData.Instance.LoggedOnUser.Id.GetValueOrDefault());
                item.ReadDate = DateTime.Now;
            }

        }
        public void SaveStatus(WerkopdrachtItem item)
        {
            WerkbonnenBeheerDAL.SaveStatus(item, out int newWaStatus);
            item.SetStatusGewijzigdText(DateTime.Now, GlobalData.Instance.LoggedOnUser.Name);

            if (item.WerkopdrachtType == WerkopdrachtType.Werkaanvraag)
                item.StatusId = newWaStatus;
            else
            {
                var werkaanvraag = from wo in this._werkaanvragen where wo.Id == item.Parent_id select wo;
                List<WerkopdrachtItem> itemsWA = werkaanvraag.ToList();
                if (itemsWA.Count() == 1)
                {
                    itemsWA[0].StatusId = newWaStatus;

                }
            }
        }

        public static List<WerkopdrachtEmailAddress> GetEmailList(int userId)
        {

            DataTable dt = WerkbonnenBeheerDAL.GetEmailList(userId);
            List<WerkopdrachtEmailAddress> emailList = new List<WerkopdrachtEmailAddress>();
            foreach (DataRow dr in dt.Rows)
            {
                WerkopdrachtEmailAddress e = new WerkopdrachtEmailAddress();
                String em1 = dr["wo_email"].ToString().Trim();
                String em2 = dr["email"].ToString().Trim();

                int userIdEmail = (int)dr["id"];


                if (!String.IsNullOrWhiteSpace(em1))
                {
                    if (em1 != em2)
                    {
                        WerkopdrachtEmailAddress emg = new WerkopdrachtEmailAddress()
                        {
                            isGroupEmailAddress = true,
                            eMailadress = em1,
                            name = "email van groep",
                            userId = null,
                            isChecked = true
                        };
                        var existingEmailG = emailList.Where(x => x.eMailadress == emg.eMailadress).FirstOrDefault();
                        if (existingEmailG == null)
                            emailList.Add(emg);
                    }
                }
                if (!String.IsNullOrWhiteSpace(em2))
                {
                    WerkopdrachtEmailAddress em = new WerkopdrachtEmailAddress()
                    {
                        isGroupEmailAddress = false,
                        eMailadress = em2,
                        userId = userIdEmail,
                        name = dr["voornaam"].ToString().Trim() + " " + dr["achternaam"].ToString().Trim()
                    };
                    if (userIdEmail == userId)
                        em.isChecked = true;
                    var existingEmail = emailList.Where(x => x.eMailadress == em.eMailadress).FirstOrDefault();

                    if (existingEmail == null)
                        emailList.Add(em);
                    else
                    {
                        if (existingEmail.userId != em.userId)
                            existingEmail.name += ", " + em.name;
                    }
                }
            }
            return emailList;
        }

        public static String GetDienstFullName(String dienst)
        {
            return dienst;
        }

        private static TypeItem ParseTypeItems(DataRow dr)
        {
            TypeItem item = new TypeItem();

            if (dr.IsNull("wot_id") == false)
                item.Id = (int)dr["wot_id"];
            if (dr.IsNull("wot_naam") == false)
                item.Naam = (String)dr["wot_naam"];
            if (dr.IsNull("wot_wodienst_code") == false)
                item.DienstCode = (String)dr["wot_wodienst_code"];
            if (dr.IsNull("onlyforsupervisor") == false)
                item.OnlyForSupervisor = (bool)dr["onlyforsupervisor"];
            if (dr.IsNull("actif") == false)
                item.IsActif = (bool)dr["actif"];
            return item;
        }
        private static StatusItem ParseStatusItem(DataRow dr)
        {
            StatusItem item = new StatusItem();
            if (dr.IsNull("wos_id") == false)
                item.Id = (int)dr["wos_id"];
            if (dr.IsNull("wos_naam") == false)
                item.Naam = (String)dr["wos_naam"];
            if (dr.IsNull("wos_type") == false)
                item.Categorie = (String)dr["wos_type"];

            return item;
        }
        private static DienstItem ParseDienstItems(DataRow dr)
        {
            DienstItem item = new DienstItem();
            if (dr.IsNull("wodienst_code") == false)
                item.Code = (String)dr["wodienst_code"];
            if (dr.IsNull("wodienst_naam") == false)
                item.Naam = (String)dr["wodienst_naam"];
            if (dr.IsNull("wodienst_groep_code") == false)
                item.Groep = (String)dr["wodienst_groep_code"];

            return item;
        }



        private WerkopdrachtItem ParseWerkopdrachtItem(DataRow dr)
        {
            //String naar enum parsen
            String dienst = dr["wo_wodienst_code"].ToString();

            //nieuwe werkopdrachtItem maken
            WerkopdrachtItem item = new WerkopdrachtItem(dienst)
            {
                DoNotify = false
            };

            if (dr.IsNull("wo_parent_id") == false)
                item.Parent_id = (int)dr["wo_parent_id"];
            else item.Parent_id = null;
            if (dr.IsNull("wo_id") == false)
                item.Id = (int)dr["wo_id"];

            if (dr.IsNull("id") == false)
            {
                item.InventarisId = (int)dr["id"];
                item.InventarisText = dr["nummer_263"] + " " + dr["productOmschrijving"];
            }

            if (dr.IsNull("wo_titel") == false)
                item.Titel = (String)dr["wo_titel"];
            if (dr.IsNull("wo_beschrijving") == false)
                item.Beschrijving = (String)dr["wo_beschrijving"];
            if (dr.IsNull("wo_wop_id") == false)
                item.PrioriteitId = (int)dr["wo_wop_id"];
            if (dr.IsNull("wo_loc_id") == false)
                item.LocatieId = (int)dr["wo_loc_id"];
            if (dr.IsNull("wosh_wos_id") == false)
            {
                item.StatusId = (int)dr["wosh_wos_id"];
                if (dr.IsNull("wosh_opmerking") == false)
                    item.StatusOpmerking = (String)dr["wosh_opmerking"];
                item.SetStatusGewijzigdText((DateTime)dr["wosh_datum"], (String)dr["statusgebruiker"]);
            }
            if (dr.IsNull("wo_wot_id") == false)
                item.TypeId = (int)dr["wo_wot_id"];
            if (dr.IsNull("creator") == false)
                item.Aanvrager = (String)dr["creator"];
            if (dr.IsNull("modifier") == false)
                item.Modifier = (String)dr["modifier"];
            if (dr.IsNull("wo_modificatiedatum") == false)
                item.ModifyDate = (DateTime)dr["wo_modificatiedatum"];

            if (dr.IsNull("wo_geb_id_creator") == false)
                item.AanvragerId = (int)dr["wo_geb_id_creator"];
            if (dr.IsNull("wo_creatiedatum") == false)
                item.Aanvraagdatum = (DateTime)dr["wo_creatiedatum"];
            if (dr.IsNull("wo_begindatum") == false)
                item.Begindatum = (DateTime)dr["wo_begindatum"];
            if (dr.IsNull("wo_einddatum") == false)
                item.Einddatum = (DateTime)dr["wo_einddatum"];
            if (dr.IsNull("aantalbijlagen") == false && (int)dr["aantalbijlagen"] > 0)
                item.AantalBijlagen = (int)dr["aantalbijlagen"];
            if (dr.IsNull("readdate") == false)
                item.ReadDate = (DateTime)dr["readdate"];

            if (dr.IsNull("telefoon") == false)
                item.Telefoon = (String)dr["telefoon"];

            if (dr.IsNull("telefoon_intern") == false)
                item.TelefoonIntern = (String)dr["telefoon_intern"];


            if (dr.IsNull("telefoon_intern_groep") == false)
                item.TelefoonGroep = (String)dr["telefoon_intern_groep"];

      

          Location loc = GebouwenBeheerLoader.GetLocationById(item.LocatieId.Value);

            item.LocatiePath = loc.GetFullPath();

            if (dr.IsNull("gebruikers") == false)
            {
                String result = String.Empty;
                String dbUserString = (String)dr["gebruikers"];
                if (!String.IsNullOrWhiteSpace(dbUserString))
                {
                    String[] split = Regex.Split(dbUserString, ",");
                    foreach (String s in split)
                    {
                        if (String.IsNullOrWhiteSpace(s) == false)
                        {
                            String[] splitGebruiker = Regex.Split(s, "@");
                            User u = new User()
                            {
                                Id = Int32.Parse(splitGebruiker[0].ToString().Trim()),
                                FirstName = splitGebruiker[1].ToString().Trim(),
                                LastName = splitGebruiker[2].ToString().Trim()
                            };
                            item.Medewerkers.Add(u);
                        }
                    }
                }
            }
            if (dr.IsNull("gebruikersgroepen") == false)
            {
                String dbGroepen = dr["gebruikersgroepen"].ToString();
                String[] split = Regex.Split(dbGroepen, ",");
                if (split != null)
                {
                    foreach (String s in split)
                    {
                        if (String.IsNullOrWhiteSpace(s) == false)
                        {
                            item.Groepen.Add(s.Trim());
                        }
                    }
                }
            }
            //ImageSourceConverter c = new ImageSourceConverter();
            item.DoNotify = true;
            item.IsModified = false;
            return item;
        }


        public static void ClearStaticCache()
        {
            if (_cachedPrioriteitItems != null)
            {
                _cachedPrioriteitItems.Clear();
                _cachedPrioriteitItems = null;

            }
            if (_cachedStatusItems != null)
            {
                _cachedStatusItems.Clear();
                _cachedStatusItems = null;

            }
            if (_cachedDienstItems != null)
            {
                _cachedDienstItems.Clear();
                _cachedDienstItems = null;

            }
            if (_cachedTypeItems != null)
            {
                _cachedTypeItems.Clear();
                _cachedTypeItems = null;
            }
        }
        public void CleanUp()
        {

        }

        public void Dispose()
        {
            CleanUp();
        }
    }
}
