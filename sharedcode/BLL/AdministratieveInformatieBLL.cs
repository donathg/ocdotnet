﻿using sharedcode.common;
using sharedcode.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Runtime.Remoting.Contexts;
using System.Text;
using System.Threading.Tasks;

namespace sharedcode.BLL
{
    public partial class gepland_begeleidingsteam
    {
        public string GroepenNaam
        {
            get
            {
                if (groepcode != null)
                    return AdministratieveInformatieBLL.GetBegeleidingsgroepNaam(groepcode);

                return string.Empty;
            }
        }
    }
    public partial class AdministratieveInformatieBLL
    {
        public static cd_administratieveinfo GetData(int bewonerId)
        {
            using (Entities entity = new Entities(sharedcode.common.GlobalData.Instance.Entity))
            {
                return entity.cd_administratieveinfo.Where(x => x.bewonerid == bewonerId).FirstOrDefault();
            }
        }
        public static string GetBegeleidingsgroepNaam(string groepcode)
        {
            try
            {
                using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
                {
                    string sql = "SELECT groep_naam FROM rechten_groep WHERE groep_code = @groepcode";
                    List<DbParam> paramList = new List<DbParam>()
                    {
                        new DbParam() { Name = "@groepcode", Value = groepcode, DbType = DbTypes.db_string }
                    };
                    DataTable dt = db.GetDataTable(sql, paramList.ToArray());

                    return dt.Rows.OfType<DataRow>().Select(x => x[0]).SingleOrDefault().ToString();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static void AddBegeleidingsTeamEntry(int bewonerId, int gebruikerId, int typeId)
        {
            using (Entities entity = new Entities(sharedcode.common.GlobalData.Instance.Entity))
            {
                //  var team = entity.gepland_begeleidingsteam.Where(x => x.bewonerid == bewonerId);
                gepland_begeleidingsteam newEntry = new gepland_begeleidingsteam()
                {
                    bewonerid = bewonerId,
                    begeleidingsteamtypeid = typeId,
                    gebruikerid = gebruikerId,
                    groepcode = null
                };
                entity.gepland_begeleidingsteam.Add(newEntry);
                entity.SaveChanges();
            }

        }
        public static void AddBegeleidingsTeamEntry(int bewonerId, String groepCode, int typeId)
        {
            using (Entities entity = new Entities(sharedcode.common.GlobalData.Instance.Entity))
            {
                //  var team = entity.gepland_begeleidingsteam.Where(x => x.bewonerid == bewonerId);
                gepland_begeleidingsteam newEntry = new gepland_begeleidingsteam()
                {
                    bewonerid = bewonerId,
                    begeleidingsteamtypeid = typeId,
                    gebruikerid = null,
                    groepcode = groepCode
                };
                entity.gepland_begeleidingsteam.Add(newEntry);
                entity.SaveChanges();
            }

        }
        public static void RemoveBegeleidingsTeamEntry(int id)
        {
            using (Entities entity = new Entities(GlobalData.Instance.Entity))
            {
                if (entity.gepland_begeleidingsteam.Where(x => x.id == id).FirstOrDefault() is gepland_begeleidingsteam teamEntry)
                {
                    entity.gepland_begeleidingsteam.Remove(teamEntry);
                    entity.SaveChanges();
                }
            }
        }
        public static cd_administratieveinfo Save(cd_administratieveinfo data)
        {

            if (data.id == 0)
            {
                using (Entities entity = new Entities(sharedcode.common.GlobalData.Instance.Entity))
                {
                    try {
                    
                        data.modifier = GlobalData.Instance.LoggedOnUser.Id.GetValueOrDefault();
                        data.modificationdate = DateTime.Now;
                        entity.cd_administratieveinfo.Add(data);
                        entity.SaveChanges();
                        return data;
                    }
                    catch(Exception)
                    {
                        throw;
                    }
                }

            }
            else
            {
                using (Entities entity = new Entities(sharedcode.common.GlobalData.Instance.Entity))
                {
                    cd_administratieveinfo data2 = entity.cd_administratieveinfo.Where(x => x.bewonerid == data.bewonerid).FirstOrDefault();
                    if (data2 != null)
                    {
                        sharedcode.common.GlobalSharedMethods.CopyPublicProperties(data, data2);

                        data2.modifier = GlobalData.Instance.LoggedOnUser.Id.GetValueOrDefault();
                        data2.modificationdate = DateTime.Now;
                        entity.SaveChanges();
                        return data2;
                    }
                }

            }
            return null;

        }
        public static List<gepland_begeleidingsteam> LoadGeplandeBegeleidingsTeams(int id)
        {
            using (Entities entity = new Entities(sharedcode.common.GlobalData.Instance.Entity))
            {
                return entity.gepland_begeleidingsteam.Where(x => x.bewonerid == id).Include("gebruiker").ToList();
            }
        }
    }
}
