﻿using sharedcode.BLL;
using sharedcode.common;
using sharedcode.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace sharedcode.BLL
{
    /** PROVIDERS FOR ADRESSEN 
    public abstract class IAdresProvider
    {
        /** PROPERTIES:  saved data in lists 
        private List<country> CountryList;
        private List<adrestype> AdrestypeList;
        protected List<AdresData> AdresdataList;
        
        /** CONSTRUCTOR: inits and fills the lists 
        public IAdresProvider()
        {
            LoadCountries();
            LoadAdresTypes();
            LoadAdresDataList();
        }

        /** METHODS 
        private void LoadCountries()
        {
            using (Entities entity = new Entities(GlobalData.Instance.Entity))
            {
                CountryList = entity.country.ToList();
            }
        }

        private void LoadAdresTypes()
        {
            using (Entities entity = new Entities(GlobalData.Instance.Entity))
            {
                AdrestypeList = entity.adrestype.Where(x => x.actief==true).ToList();
            }
        }
        public abstract void LoadAdresDataList();

        public List<country> GetAllCountries()
        {
            return CountryList;
        }
        public country GetCountryFromCode(String landcode)
        {
            return CountryList.Where(x => x.code == landcode).Single();
        }
        public List<adrestype> GetAllHoofdAdrestypes()
        {
            return AdrestypeList.Where(x => x.ishoofdkeuze == true).ToList();
        }
        public List<adrestype> GetAllSubAdrestypes()
        {
            return AdrestypeList.Where(x => x.ishoofdkeuze == false).ToList();
        }
        public adrestype GetAdresTypeFromId(int id)
        {
            return AdrestypeList.Where(x => x.id == id).Single();
        }
        public AdresData GetAdresDataFromId(int campusid)
        {
            return AdresdataList.Where(x => x.Locatie.loc_id == campusid).SingleOrDefault();
        }
        public locatie GetCampusFromId(int campusid)
        {
            using (Entities entitiy = new Entities(GlobalData.Instance.Entity))
            {
                return entitiy.locatie.Where(x => x.loc_id == campusid).SingleOrDefault();
            }
        }

        public abstract List<AdresData> GetAllAdresDataFromObject(Object obj);
        public abstract List<ContactpersoonData> GetAllContactpersonenFromAdresData(AdresData item);
        public abstract void DeleteAdresAndContactpersonen(AIViewAdres selectedAdres);
        public abstract void DeleteContactpersoon(ContactpersoonData selectedContactpersoon);
        public abstract void AddOrUpdateAdres(AdresData bewonerAdres);
    }

    public class AdresBewonerProvider : IAdresProvider
    {
        public override void LoadAdresDataList()
        {
            using (Entities entity = new Entities(GlobalData.Instance.Entity))
            {
                base.AdresdataList = new List<AdresData>();
                foreach (bewoner_adressen item in entity.bewoner_adressen.Include("adressen").Include("adrestype").Include("bewoner"))
                {
                    base.AdresdataList.Add(new AdresData(item));
                }
            }
        }

        public override List<AdresData> GetAllAdresDataFromObject(Object obj)
        {
            if (obj is adrestype type)
            {
                return base.AdresdataList.Where(x => x.AdresType.id == type.id).ToList();
            }
            if (obj is bewoner bew)
            {
                return base.AdresdataList.Where(x => x.Bewoner.id == bew.id).ToList();
            }
            return new List<AdresData>();
        }
        public override List<ContactpersoonData> GetAllContactpersonenFromAdresData(AdresData adresData)
        {
            using (Entities entity = new Entities(GlobalData.Instance.Entity))
            {
                List<ContactpersoonData> contactpersoonList = new List<ContactpersoonData>();
                foreach (bewoner_contactpersonen item in entity.bewoner_contactpersonen.Include("bewoner_adressen").Include("adrestype").Where(x => x.bewoneradres_id == adresData.Id).ToList())
                {
                    contactpersoonList.Add(new ContactpersoonData(item));
                }
                return contactpersoonList;
            }
        }
        public override void DeleteAdresAndContactpersonen(AIViewAdres selectedAdres)
        {
            using (Entities entity = new Entities(GlobalData.Instance.Entity))
            {
                bewoner_adressen adres = entity.bewoner_adressen.Include("bewoner_contactpersonen")
                                                                          .Include("adressen")
                                                                          .Include("adrestype")
                                                                          .Where(x => x.id == selectedAdres.Id)
                                                                          .Single();
                if (adres.adressen.isBedrijf && AdresdataList.FindAll(x => x.Adres.id == adres.id).Count == 1)
                {
                    if (MessageBox.Show("Bestaat dit bedrijf nog?\n OPGELET! Zo niet, wordt deze uit de databank verwijdert", "Waarschuwing", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.No)
                    {
                        adres.adressen.active = false;
                    }
                }
                else
                {
                    entity.adressen.Remove(adres.adressen);
                }
                entity.bewoner_contactpersonen.RemoveRange(adres.bewoner_contactpersonen);
                entity.bewoner_adressen.Remove(adres);

                entity.SaveChanges();
            }
        }
        public override void DeleteContactpersoon(ContactpersoonData selectedContactpersoon)
        {
            using (Entities entity = new Entities(GlobalData.Instance.Entity))
            {
                bewoner_contactpersonen contactpersoon = entity.bewoner_contactpersonen.Find(selectedContactpersoon.Id);
                if (contactpersoon != null)
                {
                    entity.bewoner_contactpersonen.Remove(contactpersoon);

                    entity.SaveChanges();
                }
            }
        }
        public override void AddOrUpdateAdres(AdresData bewonerAdres)
        {
            using (Entities entity = new Entities(GlobalData.Instance.Entity))
            {
                bewoner_adressen bewonerAdresDB = entity.bewoner_adressen.Find(bewonerAdres.Id); // find bewonerAdres
                if (bewonerAdresDB == null)
                {
                    bewonerAdresDB = new bewoner_adressen() {
                        adres_id = bewonerAdres.Adres.id,
                        adrestype_id = bewonerAdres.AdresType.id,
                        bewoner_id = bewonerAdres.Bewoner.id,
                        bewoner_contactpersonen = new List<bewoner_contactpersonen>()
                    };
                    
                    adressen adres = entity.adressen.Find(bewonerAdres.Adres.id); // find adres
                    if (adres == null)
                    {
                        bewonerAdres.Adres.country = null;
                        bewonerAdres.Adres.active = true;
                        bewonerAdresDB.adressen = bewonerAdres.Adres;
                    }
                    else
                    {
                        var adresDB = entity.Entry(adres);
                        adresDB.CurrentValues.SetValues(bewonerAdres.Adres);
                    }
                    
                    if (bewonerAdres.Childeren.Count != 0)
                    {
                        foreach (ContactpersoonData item in bewonerAdres.Childeren)
                        {
                            bewoner_contactpersonen contactpersoon = new bewoner_contactpersonen()
                            {
                                adrestype_id = item.Type.id,
                                bewoneradres_id = item.Parent.Id,
                                voornaam = item.Voornaam,
                                naam = item.Naam,
                                functie = item.Functie,
                                telefoon = item.Telefoon,
                                gsm = item.Gsm,
                                email = item.Email,
                                opmerking = item.Opmerking
                            };

                            bewonerAdresDB.bewoner_contactpersonen.Add(contactpersoon);
                        }
                    }

                    entity.bewoner_adressen.Add(bewonerAdresDB);
                }
                else
                {
                    adressen adres = entity.adressen.Find(bewonerAdres.Adres.id); // find adres
                    if (adres == null)
                    {
                        bewonerAdres.Adres.country = null;
                        bewonerAdres.Adres.active = true;
                        bewonerAdresDB.adressen = bewonerAdres.Adres;
                    }
                    else
                    {
                        var adresEntry = entity.Entry(adres);
                        adresEntry.CurrentValues.SetValues(bewonerAdres.Adres);
                    }
                    
                    if (bewonerAdres.Childeren.Count != 0)
                    {
                        foreach (ContactpersoonData item in bewonerAdres.Childeren)
                        {
                            if (item.Id == 0)
                            {
                                bewonerAdresDB.bewoner_contactpersonen.Add(new bewoner_contactpersonen() {
                                    adrestype_id = item.Type.id,
                                    voornaam = item.Voornaam,
                                    naam = item.Naam,
                                    functie = item.Functie,
                                    telefoon = item.Telefoon,
                                    gsm = item.Gsm,
                                    email = item.Email,
                                    opmerking = item.Opmerking
                                });
                            }
                            else
                            {
                                bewoner_contactpersonen contactpersoon = entity.bewoner_contactpersonen.Find(item.Id);

                                contactpersoon.voornaam = item.Voornaam;
                                contactpersoon.naam = item.Naam;
                                contactpersoon.functie = item.Functie;
                                contactpersoon.telefoon = item.Telefoon;
                                contactpersoon.gsm = item.Gsm;
                                contactpersoon.email = item.Email;
                                contactpersoon.opmerking = item.Opmerking;
                            }
                        }
                    }
                }
                entity.SaveChanges();
            }
        }
    }
    public class AdresCampusProvider : IAdresProvider
    {
        public override void LoadAdresDataList()
        {
            using (Entities entity = new Entities(GlobalData.Instance.Entity))
            {
                base.AdresdataList = new List<AdresData>();
                foreach (campussen item in entity.campussen.Include("adressen").Include("adrestype").Include("locatie"))
                {
                    base.AdresdataList.Add(new AdresData(item));
                }
            }
        }
        
        public override List<AdresData> GetAllAdresDataFromObject(object obj)
        {
            if (obj is adrestype)
            {
                return base.AdresdataList.Where(x => x.AdresType.id == ((adrestype)obj).id).ToList();
            }
            if (obj is locatie)
            {
                return base.AdresdataList.Where(x => x.Locatie.loc_id == ((locatie)obj).loc_id).ToList();
            }
            return new List<AdresData>();
        }
        public override List<ContactpersoonData> GetAllContactpersonenFromAdresData(AdresData adresData)
        {
            using (Entities entity = new Entities(GlobalData.Instance.Entity))
            {
                List<ContactpersoonData> contactpersoonList = new List<ContactpersoonData>();
                foreach (campussen_contactpersonen item in entity.campussen_contactpersonen.Include("test_campussen").Include("adrestype").Where(x => x.campussen.id == adresData.Id).ToList())
                {
                    contactpersoonList.Add(new ContactpersoonData(item));
                }
                return contactpersoonList;
            }
        }
        public override void DeleteAdresAndContactpersonen(AIViewAdres selectedAdres)
        {
            using (Entities entity = new Entities(GlobalData.Instance.Entity))
            {
                campussen adres = entity.campussen.Include("test_campussen_contactpersonen")
                                                            .Include("adressen")
                                                            .Where(x => x.id == selectedAdres.Id)
                                                            .Single();
                if (base.AdresdataList.FindAll(x => x.Adres.id == adres.id).Count == 1)
                {
                    if (MessageBox.Show("Bestaat deze campus nog?\n OPGELET! Zo niet, wordt deze uit de databank verwijdert", "Waarschuwing", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.No)
                    {
                        adres.adressen.active = false;
                    }
                    else
                    {
                        entity.adressen.Remove(adres.adressen);
                    }
                }
                else
                {
                    entity.adressen.Remove(adres.adressen);
                }
                entity.campussen_contactpersonen.RemoveRange(adres.campussen_contactpersonen);
                entity.campussen.Remove(adres);

                entity.SaveChanges();
            }
        }
        public override void DeleteContactpersoon(ContactpersoonData selectedContactpersoon)
        {
            using (Entities entity = new Entities(GlobalData.Instance.Entity))
            {
                campussen_contactpersonen contactpersoon = entity.campussen_contactpersonen.Find(selectedContactpersoon.Id);
                if (contactpersoon != null)
                {
                    entity.campussen_contactpersonen.Remove(contactpersoon);

                    entity.SaveChanges();
                }
            }
        }
        public override void AddOrUpdateAdres(AdresData campus)
        {
            using (Entities entity = new Entities(GlobalData.Instance.Entity))
            {
                campussen CampusDB = entity.campussen.Find(campus.Id); // find bewonerAdres
                if (CampusDB == null)
                {
                    CampusDB = new campussen()
                    {
                        adres_id = campus.Adres.id,
                        adrestype_id = campus.AdresType.id,
                        locatie_id = campus.Locatie.loc_id,
                        campussen_contactpersonen = new List<campussen_contactpersonen>()
                    };

                    adressen adres = entity.adressen.Find(campus.Adres.id); // find adres
                    if (adres == null)
                    {
                        CampusDB.adressen = campus.Adres;
                        CampusDB.adressen.country = null;
                    }
                    else
                    {
                        var adresDB = entity.Entry(adres);
                        adresDB.CurrentValues.SetValues(campus.Adres);
                    }
                    
                    if (campus.Childeren.Count != 0)
                    {
                        foreach (ContactpersoonData item in campus.Childeren)
                        {
                           campussen_contactpersonen contactpersoon = new campussen_contactpersonen()
                            {
                                adrestype_id = item.Type.id,
                                campus_id = item.Parent.Id,
                                voornaam = item.Voornaam,
                                naam = item.Naam,
                                functie = item.Functie,
                                telefoon = item.Telefoon,
                                gsm = item.Gsm,
                                email = item.Email,
                                opmerking = item.Opmerking
                            };

                            CampusDB.campussen_contactpersonen.Add(contactpersoon);
                        }
                    }

                    entity.campussen.Add(CampusDB);
                }
                else
                {
                    CampusDB.adrestype_id = campus.AdresType.id;
                    CampusDB.adres_id = campus.Adres.id;
                    CampusDB.locatie_id = campus.Locatie.loc_id;
                    CampusDB.campussen_contactpersonen = new List<campussen_contactpersonen>();

                    adressen adres = entity.adressen.Find(campus.Adres.id); // find adres
                    if (adres == null)
                    {
                        CampusDB.adressen = campus.Adres;
                    }
                    else
                    {
                        var adresDB = entity.Entry(adres);
                        adresDB.CurrentValues.SetValues(campus.Adres);
                    }
                    
                    if (campus.Childeren.Count != 0)
                    {
                        foreach (ContactpersoonData item in campus.Childeren)
                        {
                            campussen_contactpersonen contactpersoon = entity.campussen_contactpersonen.Find(item.Id); // find contactpersoon
                            if (contactpersoon == null)
                            {
                                contactpersoon = new campussen_contactpersonen()
                                {
                                    adrestype_id = item.Type.id,
                                    voornaam = item.Voornaam,
                                    naam = item.Naam,
                                    functie = item.Functie,
                                    telefoon = item.Telefoon,
                                    gsm = item.Gsm,
                                    email = item.Email,
                                    opmerking = item.Opmerking
                                };
                                CampusDB.campussen_contactpersonen.Add(contactpersoon);
                            }
                            else
                            {
                                contactpersoon.voornaam = item.Voornaam;
                                contactpersoon.naam = item.Naam;
                                contactpersoon.functie = item.Functie;
                                contactpersoon.telefoon = item.Telefoon;
                                contactpersoon.gsm = item.Gsm;
                                contactpersoon.email = item.Email;
                                contactpersoon.opmerking = item.Opmerking;
                            }
                        }
                    }
                }

                entity.SaveChanges();
            }
        }
    }
    */

    /** CLASSES NECCESSARY TO STORE DATA FOR ADRESPROVIDERS 
    public class AdresData
    {
        public int Id { get; set; }
        public adrestype AdresType { get; set; }
        public adressen Adres { get; set; }
        public locatie Locatie { get; set; }
        public bewoner Bewoner { get; set; }
        public List<ContactpersoonData> Childeren { get; set; }

        public AdresData()
        {
            Id = 0;
            Childeren = new List<ContactpersoonData>();
        }
        public AdresData(bewoner_adressen adres): this()
        {
            Id = adres.id;
            Adres = adres.adressen;
            AdresType = adres.adrestype;
            Bewoner = adres.bewoner;
            if (adres.bewoner_contactpersonen != null && adres.bewoner_contactpersonen.Count != 0)
            {
                foreach (bewoner_contactpersonen item in adres.bewoner_contactpersonen)
                {
                    Childeren.Add(new ContactpersoonData(item));
                }
            }
        }
        public AdresData(campussen campus): this()
        {
            Id = campus.id;
            Adres = campus.adressen;
            AdresType = campus.adrestype;
            Locatie = campus.locatie;
            if (campus.campussen_contactpersonen != null && campus.campussen_contactpersonen.Count != 0)
            {
                foreach (campussen_contactpersonen item in campus.campussen_contactpersonen)
                {
                    Childeren.Add(new ContactpersoonData(item));
                }
            }
        }
    }
    public class ContactpersoonData
    {
        public int Id { get; set; }
        public String Voornaam { get; set; }
        public String Naam { get; set; }
        public String Functie { get; set; }
        public String Telefoon { get; set; }
        public String Gsm { get; set; }
        public String Email { get; set; }
        public String Opmerking { get; set; }
        public adrestype Type { get; set; }
        public AdresData Parent { get; set; }

        public ContactpersoonData()
        {
            Id = 0;
            Parent = new AdresData();
        }
        public ContactpersoonData(bewoner_contactpersonen bewonerContactpersoon)
        {
            Id = bewonerContactpersoon.id;
            Voornaam = bewonerContactpersoon.voornaam;
            Naam = bewonerContactpersoon.naam;
            Functie = bewonerContactpersoon.functie;
            Telefoon = bewonerContactpersoon.telefoon;
            Gsm = bewonerContactpersoon.gsm;
            Email = bewonerContactpersoon.email;
            Opmerking = bewonerContactpersoon.opmerking;
            Type = bewonerContactpersoon.adrestype;

            bewonerContactpersoon.bewoner_adressen.bewoner_contactpersonen = null; // otherwise endless loop between creation ContactpersoonData and AdresData
            Parent = new AdresData(bewonerContactpersoon.bewoner_adressen);
        }
        public ContactpersoonData(campussen_contactpersonen campusContactpersoon)
        {
            Id = campusContactpersoon.id;
            Voornaam = campusContactpersoon.voornaam;
            Naam = campusContactpersoon.naam;
            Functie = campusContactpersoon.functie;
            Telefoon = campusContactpersoon.telefoon;
            Gsm = campusContactpersoon.gsm;
            Email = campusContactpersoon.email;
            Opmerking = campusContactpersoon.opmerking;
            Type = campusContactpersoon.adrestype;

            campusContactpersoon.campussen.campussen_contactpersonen = null; // otherwise endless loop between creation ContactpersoonData and AdresData
            Parent = new AdresData(campusContactpersoon.campussen);
        }
    }
        */
}