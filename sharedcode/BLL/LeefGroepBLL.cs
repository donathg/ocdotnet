﻿using sharedcode.common;
using sharedcode.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sharedcode.BLL
{
    [Serializable]
    public partial class leefgroep {
        public String TreeViewItemTitle
        {
            get
            {

                return this.naam;
            }
        }
        public override string ToString()
        {
            return naam;
        }
    }

    public class LeefgroepBLL
    {
        private static List<leefgroep> _leefgroepList = null;

        public static List<leefgroep> GetLeefgroepenBewoners()
        {
            using (Entities entity = new Entities(sharedcode.common.GlobalData.Instance.Entity))
            {
                return entity.leefgroep.Include("bewoner").OrderBy(x=> x.naam).ToList();
            }
        }
        public static List<LeefgroepBewonerTreeItem> GetLeefGroepenBewonersForTree(int? bewonerId = null)
        {
            using (Entities entity = new Entities(sharedcode.common.GlobalData.Instance.Entity))
            {
                LeefgroepBewonerTreeItem.CounterId = 0;
                List<LeefgroepBewonerTreeItem> Itemslist = new List<LeefgroepBewonerTreeItem>();

                List<leefgroep> l = new List<leefgroep>();

                if (bewonerId.HasValue)
                 l = entity.leefgroep.Include("bewoner").Where(x => x.actief == "Y").OrderBy(x => x.naam).ToList();
                else
                 l = entity.leefgroep.Include("bewoner").Where(x => x.actief == "Y").OrderBy(x => x.naam).ToList();

                foreach (leefgroep item in l)
                {

                    bool added = false;
                    LeefgroepBewonerTreeItem treeItem = new LeefgroepBewonerTreeItem(item);
                   

                    foreach (bewoner subItem in item.bewoner)
                    {
                        if (bewonerId.HasValue && bewonerId.GetValueOrDefault() != subItem.id)
                            continue;
                        Itemslist.Add(new LeefgroepBewonerTreeItem(subItem) { ParentId = treeItem.Id });
                        added = true;
                    }
                    if(added)
                        Itemslist.Add(treeItem);
                }
                return Itemslist;
            }
        }
        public static List<leefgroep> GetLeefgroepen()
        {
            List<leefgroep> test = GetLeefgroepenBewoners();
            if (_leefgroepList == null || _leefgroepList.Count==0)
            {
                _leefgroepList = new List<leefgroep>();
                DataTable dt = LeefGroepDAL.LoadLeefGroepen();
                foreach (DataRow dr in dt.Rows)
                {
                    leefgroep l = new leefgroep();
                    l.id = (int)dr["id"];
                    l.code = (String)dr["code"];
                    l.naam = (String)dr["naam"];
                    l.fromorbis = (String)dr["fromorbis"];
                    l.forboekh = (String)dr["forboekh"];
                    l.actief = (String)dr["actief"];
                    _leefgroepList.Add(l);
                }
            }
            return _leefgroepList.ToList();
        }
        public static List<leefgroep> GetLeefgroepenKM()
        {
            List<leefgroep> _leefgroepList = GetLeefgroepen();
            return _leefgroepList.Where(x => x.forboekh == "Y" && x.actief=="Y").ToList();
        
        }

        public static leefgroep GetLeefgroepFromId(int id)
        {
            using (Entities entity = new Entities(GlobalData.Instance.Entity))
            {
                return entity.leefgroep.Where(x => x.id == id).SingleOrDefault();
            }
        }
        public static void ClearCache()
        {
            if (_leefgroepList!=null)
            _leefgroepList.Clear();
        }
        public static void CleanUp()
        {
            ClearCache();
            _leefgroepList = null;
        }
    }

    public class LeefgroepBewonerTreeItem
    {
        /** FIELDS */
        public static int CounterId = 0;

        public int Id { get; set; }
        public int? ParentId { get; set; }
        public String Naam { get; set; }
        public object Object { get; set; }

        /** CONSTRUCTOR*/
        public LeefgroepBewonerTreeItem()
        {
            Id = CounterId;
            CounterId++;
        }
        public LeefgroepBewonerTreeItem(leefgroep groep): this()
        {
            ParentId = null;
            Naam = groep.naam;
            Object = groep;
        }
        public LeefgroepBewonerTreeItem(bewoner bew): this()
        {
            Naam = bew.VoornaamAchternaam;
            Object = bew;
        }
    }
}
