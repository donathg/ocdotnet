﻿using sharedcode.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sharedcode.BLL
{
    public static class LandenBLL
    {
        public static List<country> GetAllCountries()
        {
            return LandenDAL.SelectAllCountries();
        }
        public static country GetCountryFromId(int id)
        {
            return LandenDAL.SelectCountryFromId(id);
        }
    }
}
