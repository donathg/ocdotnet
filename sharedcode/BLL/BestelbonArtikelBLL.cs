﻿using sharedcode.common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sharedcode.BLL
{
  /*  public partial class bestelbonartikel : INotifyPropertyChanged
    {

      
            public static List<bestelbonartikelcategorie> GetAllArtikelCategorien(int companyId, bool? actif)
            {
                using (Entities i = new Entities(GlobalData.Instance.ConnectionStringEF))
                {
                    if (actif == null)
                        return (from p in i.bestelbonartikelcategorie   select p).ToList();
                    else
                    {
                        return (from p in i.bestelbonartikelcategorie where  p.actif == actif.GetValueOrDefault() select p).ToList();
                    }
                }
            }
        


        private void BeforeUpdate()
        {

            if (String.IsNullOrWhiteSpace(this.naam))
                throw new Exception("Gelieve een naam in te geven");
            this.modifier = GlobalData.Instance.LoggedOnUser.Id.GetValueOrDefault();
            this.modificationdate = DateTime.Now;

        }
        public static void Update(Entities e, bestelbonartikel i)
        {
            if (e != null)
            {
                i.BeforeUpdate();
                e.SaveChanges();
            }
        }
        public static void Update(bestelbonartikel i)
        {
            using (Entities e = new Entities(GlobalData.Instance.ConnectionStringEF))
            {
                Update(e, i);
            }
        }
        public static void Insert(Entities e, bestelbonartikel i)
        {
            if (e != null)
            {
                i.BeforeUpdate();
                e.bestelbonartikel.Add(i);
                e.SaveChanges();
            }
        }
        public static void Insert(bestelbonartikel i)
        {
            using (Entities e = new Entities(GlobalData.Instance.ConnectionStringEF))
            {
                Insert(e, i);
            }
        }
        private void Init()
        {
            this.actif = true;
            this.creator = GlobalData.Instance.LoggedOnUser.Id.GetValueOrDefault();
            this.creationdate = DateTime.Now;

        }
        public static bestelbonartikel CloneOrCreateNew(bestelbonartikel source)
        {
            bestelbonartikel newi = new bestelbonartikel();
            if (source != null)
                newi.TransferValues(source);
            else
                newi.Init();
            return newi;
        }

        public void TransferValues(bestelbonartikel source)
        {
           GlobalSharedMethods.CopyPublicProperties(source, this);
            List<String> ps = GlobalSharedMethods.GetAllProperties(this);
            foreach (String s in ps)
                NotifiyPropertyChanged(s); 
        }

        public static List<bestelbonartikel> GetAllItems(int companyId, bool? actif)
        {

            using (Entities e = new Entities(GlobalData.Instance.ConnectionStringEF))
            {
                return GetAllItems(e, companyId, actif);
            }
        }
        public static bestelbonartikel GetItemInfo(Entities e, int id)
        {
            return (from p in e.bestelbonartikel where p.id == id select p).FirstOrDefault();

        }

        public static List<bestelbonartikel> GetAllItems(Entities e, int companyId, bool? actif)
        {

            if (actif == null)
                return (from p in e.bestelbonartikel select p).ToList();
            else
            {


                bool bActif = actif.GetValueOrDefault();
                return (from p in e.bestelbonartikel where p.actif == bActif select p).ToList();

            }

        }

        void NotifiyPropertyChanged(string property)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(property));

            }
        }
        public event PropertyChangedEventHandler PropertyChanged;

    }
    */
}

   
