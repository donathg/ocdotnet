﻿using sharedcode.common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sharedcode.BLL
{/*
    public partial class begeleiding
    {

        public begeleiding ()
        {
            datum = DateTime.Now.Date; 
            datum_van = DateTime.Now.Date;
            datum_tot = DateTime.Now.Date;
        }


        private DateTime _datumEdit;

        public DateTime DatumEdit
        {
            get
            {
                return this.datum;
            }

            set
            {
                _datumEdit = value;
                datum = value;
                datum_van = new DateTime(datum.Year, datum.Month, datum.Day, datum_van.Hour, datum_van.Minute, 0);
                datum_tot = new DateTime(datum.Year, datum.Month, datum.Day, datum_tot.Hour, datum_tot.Minute, 0);
            }
        }

        public TimeSpan? AantalUren
        {
            get
            {

                try {


                  //  return TimeSpan.FromHours(2);
                    TimeSpan sp = datum_tot - datum_van;
                    return sp; // return new DateTime(sp.Ticks);
                }
                catch
                {
                    return null;
                }
            }
        }
        /// <summary>
        /// < 2 uur  = 1      2:01-4:00 = 2
        /// </summary>
        public int AantalContactMomenten
        {
            get
            {
                try
                {
                    TimeSpan sp = datum_tot - datum_van;
                    return (int)(decimal)sp.TotalHours / 2;
                    
              
                }
                catch
                {

                    return 0;
                }
            }


        }
    }
*//*
    public class begeleidingReportItem
    {
        public begeleidingReportItem()
        {
            TotalSpentTime = new TimeSpan(0);
         }
        public int GebruikerId { get; set; }
        public String Naam { get; set; }
        public TimeSpan TotalSpentTime { get; set; }
    }
    public class begeleidingReport
    {
        public begeleidingReport(DateTime dtVan, DateTime dtTot, List<begeleiding> begeleidingen)
        {
            this.dtVan = dtVan;
            this.dtTot = dtTot;
            this.begeleidingen = new List<begeleidingReportItem>();
            Convert(begeleidingen);
        }
        public DateTime dtVan { get; set; }
        public DateTime dtTot { get; set; }
        public List<begeleidingReportItem> begeleidingen { get; set; }
        private void Convert(List<begeleiding> beg)
        {
            foreach (begeleiding b in beg)
            {
                begeleidingReportItem y = begeleidingen.Where(x => x.GebruikerId == b.gebruikerid).FirstOrDefault();
                if (y == null)
                {
                    y = new begeleidingReportItem();
                    begeleidingen.Add(y);
                }
                y.GebruikerId = b.gebruikerid;
               y.TotalSpentTime += b.AantalUren.GetValueOrDefault();
               y.Naam = b.gebruiker.Naam;
            }
        }
    }
    public class BegeleidingBLL
    {

        public static begeleiding Save( begeleiding b)
        {

            if (b.id == 0)
            {
                using (Entities entity = new Entities(sharedcode.common.GlobalData.Instance.Entity))
                {
                    try
                    {

                      //  b.bewoner = null;
                        b.creator =  b.modifier = GlobalData.Instance.LoggedOnUser.Id.GetValueOrDefault();
                        b.creationdate = b.modificationdate = DateTime.Now;
                        entity.begeleiding.Add(b);
                        entity.SaveChanges();
                        return b;
                    }
                    catch (Exception)
                    {

                    }
                }

            }
            else
            {
                using (Entities entity = new Entities(sharedcode.common.GlobalData.Instance.Entity))
                {
                    begeleiding b2 = entity.begeleiding.Where(x => x.id == b.id).FirstOrDefault();
                    if (b2 != null)
                    {
                        sharedcode.common.GlobalSharedMethods.CopyPublicProperties(b, b2);

                        b2.modifier = GlobalData.Instance.LoggedOnUser.Id.GetValueOrDefault();
                        b2.modificationdate = DateTime.Now;
                      
                        entity.SaveChanges();
                        return b2;
                    }
                }

            }
            return null;

        }

        public static List<begeleiding_begeleidingen> GetBegeleidingTypes()
        {
            using (Entities entity = new Entities(sharedcode.common.GlobalData.Instance.Entity))
            {
                return entity.begeleiding_begeleidingen.ToList();
            }
        }
        public static List<begeleiding_groepen> GetBegeleidingGroepen()
        {
            using (Entities entity = new Entities(sharedcode.common.GlobalData.Instance.Entity))
            {
                return entity.begeleiding_groepen.ToList();
            }
        }
        public static List<begeleiding_types> GetTypes()
        {
            using (Entities entity = new Entities(sharedcode.common.GlobalData.Instance.Entity))
            {
                return entity.begeleiding_types.ToList();
            }
        }
        public static List<begeleiding> GetBegeleidingen(DateTime van, DateTime tot, int? gebruikerid, int? bewonerid)
        {
            using (Entities entity = new Entities(sharedcode.common.GlobalData.Instance.Entity))
            {
                if (bewonerid.HasValue && gebruikerid.HasValue)
                     return entity.begeleiding.Include("gebruiker").Where(x => x.bewonerid == bewonerid && x.gebruikerid == gebruikerid && x.datum>=van.Date && x.datum <= tot.Date).OrderByDescending(x => x.datum).ToList();
                else if (gebruikerid.HasValue)
                    return entity.begeleiding.Include("gebruiker").Where(x=>x.gebruikerid == gebruikerid && x.datum >= van.Date && x.datum <= tot.Date).OrderByDescending(x => x.datum ).ToList();
                else
                    return entity.begeleiding.Include("gebruiker").Where(x => x.datum >= van.Date && x.datum <= tot.Date).OrderByDescending(x => x.datum).ToList();
            }
        }

        public static List<begeleiding_locaties> GetLocaties()
        {

            using (Entities entity = new Entities(sharedcode.common.GlobalData.Instance.Entity))
            {
                return entity.begeleiding_locaties.ToList();
            }
        }
    }

     
}*/
}
