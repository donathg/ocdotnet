﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sharedcode.BLL
{
    [Serializable]
    partial class kampen
    {
        public static List<kampen> GetAll(bool onlyActives)
        {

            using (Entities entity = new Entities(sharedcode.common.GlobalData.Instance.Entity))
            {
                
                    if (onlyActives)
                    return entity.kampen.Where(x=>x.aktief==true).ToList();
                else 
                    return entity.kampen.ToList(); 
               
            }
        }
    }
}
