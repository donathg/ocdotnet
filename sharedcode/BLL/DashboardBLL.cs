﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using sharedcode.common;

namespace sharedcode.BLL
{
    public partial class dashboard
    {
       // public Byte[] ImageGebruiker { get; set; }
        public Byte[] ImageBewoner { get; set; }
        public bool IsVerwijderd { get; set; }

        public String creationdateIntelligentText
        {
            get { return this.gebruiker.VoornaamAchternaam + ", " +  GlobalSharedMethods.DateTimeToIntelligentText(this.creationdate); }
        }
        public void InitPictures()
        {
            if (this.bewoner != null)
            {
                this.ImageBewoner = this.bewoner.GetFoto();
            }
            /*  if (this.gebruiker != null)
            {
                this.ImageGebruiker = UserBLL.GetPicture(this.gebruiker.personeelscode);
            }*/
        }
    }

    // Klasse die alle zoekfilters bijhoudt van dashboard
    public class DashboardFilter
    {
        public bewoner bewoner { get; set; }
        public gebruiker gekozenGebruiker { get; set; }
        public leefgroep gekozenLeefgroep { get; set; }
        public DateTime vanDatum { get; set; }
        public DateTime totDatum { get; set; }
        public String bericht { get; set; }
    }


    public class DashbboardBLL
    {
        /***** OPHALEN DAGBOEKITEMS MET FILTERING *****/
        public static List<dashboard> GetDashboardItems(int? userId, DashboardFilter filter)
        {
            using (Entities entity = new Entities(GlobalData.Instance.Entity))
            {
                /** Er wordt altijd gefilterd op datum **/
                IQueryable<dashboard> dashboardItems = entity.dashboard.Include("dashboard_verwijderd")
                                                                  .Include("bewoner")
                                                                  .Include("gebruiker")
                                                                  .Where(x => filter.vanDatum < x.creationdate && x.creationdate < filter.totDatum);
                // is in verwijderd mode
                if (userId.HasValue)
                {
                    dashboardItems = dashboardItems.Where(x => x.dashboard_verwijderd.Any(y => y.gebruikerId == userId.Value));
                }
                else
                {
                    dashboardItems = dashboardItems.Where(x => x.dashboard_verwijderd.All(y => y.gebruikerId != GlobalData.Instance.LoggedOnUser.Id.Value));
                }
                // heeft bewoner
                if (filter.bewoner != null)
                {
                    dashboardItems = dashboardItems.Where(x => x.bewonerId == filter.bewoner.id);
                }
                // heeft leefgroep
                else if (filter.gekozenLeefgroep != null)
                {
                    dashboardItems = dashboardItems.Where(x => x.bewoner.leefgroep.Any(y => y.id == filter.gekozenLeefgroep.id));
                }
                // heeft gebruiker
                if (filter.gekozenGebruiker != null)
                {
                    dashboardItems = dashboardItems.Where(x => x.creatorId == filter.gekozenGebruiker.id);
                }
                // heeft bericht
                if (!String.IsNullOrWhiteSpace(filter.bericht))
                {
                    dashboardItems = dashboardItems.Where(x => x.descr.Contains(filter.bericht));
                }

                return dashboardItems.OrderByDescending(x => x.creationdate).ToList();
            }
        }

        /***** OPHALEN DASHBOARD_VERWIJDERD *****/
        public static List<dashboard_verwijderd> GetAllDashboardVerwijderd()
        {
            using (Entities entity = new Entities(GlobalData.Instance.Entity))
            {
                return entity.dashboard_verwijderd.ToList();
            }
        }

        /***** TOEVOEGEN DASHBOARD_VERWIJDERD OBJECT NAAR DB *****/
        public static void InsertDashboardVerwijderdItem(int gebruikerid, int dashboardId)
        {
            using (Entities entity = new Entities(GlobalData.Instance.Entity))
            {
                dashboard_verwijderd verwijderd = new dashboard_verwijderd();
                verwijderd.gebruikerId = gebruikerid;
                verwijderd.dashboardId = dashboardId;
                verwijderd.creationDate = DateTime.Now;

                entity.dashboard_verwijderd.Add(verwijderd);
                entity.SaveChanges();
            }
        }

        /***** VERWIJDEREN DASHBOARD_VERWIJDERD OBJECT UIT DB *****/
        public static void RemoveDashboardVerwijderdItem(int verwijderdId)
        {
            using (Entities entity = new Entities(GlobalData.Instance.Entity))
            {
                dashboard_verwijderd removeVerwijderd = entity.dashboard_verwijderd.Find(verwijderdId);

                entity.dashboard_verwijderd.Remove(removeVerwijderd);
                entity.SaveChanges();
            }
        }
    }
}
