﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace sharedcode.BLL
{
    public class IdName
    {
        public int Id { get; set; }
        public String Name { get; set; }
    }
    
    public class ProjectMethods
    {
        public static void ClearCache()
        {
            GebouwenBeheerBLL.ClearStaticCache();
        }
    }

    public class HelperFunctions
    {
        /// <summary>
        /// Converts a String to a enum
        /// Then I can do:
        /// StatusEnum MyStatus = EnumUtil.ParseEnum<StatusEnum>("Active");
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value"></param>
        /// <returns></returns>
        public static T ParseEnum<T>(string value)
        {
            return (T)Enum.Parse(typeof(T), value, true);
        }
    }
    
    /// <summary>
    ///How to implement a reusable ICommand 
    ///http://wpftutorial.net/DelegateCommand.html
    ///Introduction
    ///If you are using the MVVM (model-view-viewmodel) pattern, one of the most used mechanism to bind actions to the view are commands. To provide a command, you have to implement the ICommand interface. This is quite simple, but you have to do it over and over again. This is cumbersome.
    ///The idea of this pattern build an universal command, that takes two delegates: One that is called, when ICommand.Execute(object param) is invoked, and one that evalues the state of the command when ICommand.CanExecute(object param) is called.
    ///In addition to this, we need a method, that triggers the CanExecuteChanged event. This causes the ui element to reevaluate the CanExecute() of the commmand.
    /// </summary>
    public class DelegateCommand : ICommand
    {
        public event EventHandler CanExecuteChanged;

        private readonly Predicate<object> _canExecute;
        private readonly Action<object> _execute;

        public DelegateCommand(Action<object> execute, Predicate<object> canExecute)
        {
            _execute = execute;
            _canExecute = canExecute;
        }
        public DelegateCommand(Action<object> execute) : this(execute, null)
        {
        }
        public virtual bool CanExecute(object parameter)
        {
            if (_canExecute == null)
            {
                return true;
            }

            return _canExecute(parameter);
        }
        public virtual void Execute(object parameter)
        {
            _execute(parameter);
        }
        public void RaiseCanExecuteChanged()
        {
            if (CanExecuteChanged != null)
            {
                CanExecuteChanged(this, EventArgs.Empty);
            }
        }
    }
}
