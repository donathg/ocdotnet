﻿using sharedcode.common;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;

using sharedcode.DAL;

namespace sharedcode.BLL
{
    public enum BeeldvormingtreeMainType
    {
        none,
        anamnese,
        diagnostiek,
        sociaal_emotioneel_functioneren,
        functioneringsprofiel
    }
    public enum BeeldvormingReportPrintTypes
    {
        A,
        B,
        AB
    }

    public partial class beeldvormingheader
    {
        public String versionText
        {
            get
            {

                if (closingdate.HasValue)
                    return "gesloten op : " + closingdate.GetValueOrDefault().ToString("dd'/'MM'/'yyyy") + " - vervaldatum : " + this.expirationDate.GetValueOrDefault().ToString("dd'/'MM'/'yyyy");
                else
                    return "actuele versie ";
            }
        }
    }

    public partial class beeldvormingtree
    {
        private BeeldvormingtreeMainType _beeldvormingtreeMainType;
        public BeeldvormingtreeMainType BeeldvormingtreeMainType
        {
            get
            {
                if (ParentItem == null)
                    return _beeldvormingtreeMainType;
                else if (ParentItem.ParentItem != null)
                    return ParentItem.ParentItem.BeeldvormingtreeMainType;
                else
                    return ParentItem.BeeldvormingtreeMainType;
            }
            set
            {
                _beeldvormingtreeMainType = value;
            }
        }
        public int Level
        {
            get { return 0; }
        }
        public beeldvormingtree ParentItem { get; set; }
        public List<Object> Children { set; get; } = new List<Object>();
        public bool IsExpanded
        {
            get
            {
                return true;
            }
        }
        public String NumberName
        {
            get
            {
                return this.numbering + " " + this.name;

            }

        }

    }

    public class BeeldvormingReport
    {
        public DateTime? BesprekingClientDatum { get; set; }
        public DateTime? BesprekingTeamDatum { get; set; }
        public DateTime? BesprekingContexttDatum { get; set; }
        public string BesprekingClientText { get; set; }
        public string BesprekingTeamText { get; set; }
        public string BesprekingContextText { get; set; }
        public DateTime? AfgeslotenDatum { get; set; }
        public bewoner Bewoner { get; set; }
        public List<BeeldvormingReportItem> ReportItems { get; set; }
        public BeeldvormingReportPrintTypes Type { get; set; }
        public bool VertrouwelijkeInfo { get; set; }

        public BeeldvormingReport()
        {
            ReportItems = new List<BeeldvormingReportItem>();
        }
    }

    public class BeeldvormingReportItem
    {
        public String BeeldvormingLevel0Naam { get; set; }
        public String BeeldvormingLevel1Naam { get; set; }
        public String BeeldvormingLevel2Naam { get; set; }
        public String A { get; set; }
        public String B { get; set; }
        public String V { get; set; }
    }

    public static class BeeldvormingBLL
    {
        private static List<beeldvormingtree> _beeldvormingTreeList;
        public static List<beeldvormingtree> BeeldvormingTreeList
        {
            get
            {
                if (_beeldvormingTreeList == null || _beeldvormingTreeList.Count == 0)
                    LoadBeeldVormingTree();

                return _beeldvormingTreeList;
            }
        }

        /// <summary>
        /// Laadt alle beeldvorming (A,B en V) van 1 TreeItem + alle versies hiervan
        /// </summary>
        /// <param name="bewonerId"></param>
        /// <returns></returns>
        public static List<beeldvorming> LoadBeeldVorming(int bewonerId)
        {
            using (Entities entity = new Entities(sharedcode.common.GlobalData.Instance.Entity))
            {
                return entity.beeldvorming.Where(x => x.bewonerid == bewonerId).ToList();
            }
        }
        private static void LoadBeeldVormingTree()
        {
            using (Entities entity = new Entities(sharedcode.common.GlobalData.Instance.Entity))
            {
                _beeldvormingTreeList = entity.beeldvormingtree.OrderBy(x=>x.numbering).ToList();
            }
            foreach (beeldvormingtree b in _beeldvormingTreeList)
            {
                //assign Types
                if (b.parentid == null)
                {
                    if (b.id == 1)          /*anamnsese*/
                        b.BeeldvormingtreeMainType = BeeldvormingtreeMainType.anamnese;
                    else if (b.id == 6)     /*diagnostiek*/
                        b.BeeldvormingtreeMainType = BeeldvormingtreeMainType.diagnostiek;
                    else if (b.id == 10)    /*emotioneel functioneren*/
                        b.BeeldvormingtreeMainType = BeeldvormingtreeMainType.sociaal_emotioneel_functioneren;
                    else if (b.id == 16)    /*functioneringsprofiel*/
                        b.BeeldvormingtreeMainType = BeeldvormingtreeMainType.functioneringsprofiel;
                }
            }

            //Assign Children
            foreach (beeldvormingtree b in _beeldvormingTreeList)
            {
                foreach (beeldvormingtree c in _beeldvormingTreeList)
                {
                    if (c.parentid == b.id)
                    {
                        c.ParentItem = b;
                        c.BeeldvormingtreeMainType = b.BeeldvormingtreeMainType;
                        b.Children.Add(c);
                    }
                }
            }
        }
        public static List<beeldvormingheader> LoadBeeldVormingHeaders(int bewonerId)
        {
            using (Entities entity = new Entities(GlobalData.Instance.Entity))
            {
                List<beeldvormingheader> list = entity.beeldvormingheader.Where(x => x.bewonerid == bewonerId).OrderBy(x => x.version).ToList();
                if (list.Count == 0)
                {
                    list = new List<beeldvormingheader>();
                    beeldvormingheader bh = new beeldvormingheader()
                    {
                        bewonerid = bewonerId,
                        creator = GlobalData.Instance.LoggedOnUser.Id.GetValueOrDefault(),
                        version = 1,
                        creationdate = DateTime.Now,
                        descr = "nieuw versie"
                    };
                    entity.beeldvormingheader.Add(bh);
                    entity.SaveChanges();

                    list = entity.beeldvormingheader.Where(x => x.bewonerid == bewonerId).ToList();
                }
                return list;
            }
        }
        public static BeeldvormingReport CreateBeeldvormingReport(bewoner bewoner, beeldvormingheader header, List<beeldvormingtree> tree, List<beeldvorming> beeldvormingen, BeeldvormingReportPrintTypes type, bool vertrouwelijkeInfo)
        {
            BeeldvormingReport report = new BeeldvormingReport()
            {
                AfgeslotenDatum = header.closingdate,
                Bewoner = bewoner,
                Type = type,
                VertrouwelijkeInfo = vertrouwelijkeInfo,
                BesprekingClientDatum = header.besprekingClientDate,
                BesprekingTeamDatum = header.besprekingTemDate,
                BesprekingContexttDatum = header.besprekingContextDate,
                BesprekingClientText = header.besprekingClientText,
                BesprekingTeamText = header.besprekingTeamText,
                BesprekingContextText = header.besprekingContextText
            };
            foreach (beeldvormingtree treeItem in tree.Where(x => x.isEditable))
            {
                BeeldvormingReportItem repItem = new BeeldvormingReportItem();
                List<beeldvormingtree> treeList = GetTreePath(treeItem.id);

                if (treeList.Count == 3)
                {
                    repItem.BeeldvormingLevel0Naam = treeList[2].NumberName;
                    repItem.BeeldvormingLevel1Naam = treeList[1].NumberName;
                    repItem.BeeldvormingLevel2Naam = treeList[0].NumberName;
                }
                else if (treeList.Count == 2)
                {
                    repItem.BeeldvormingLevel0Naam = treeList[1].NumberName;
                    repItem.BeeldvormingLevel1Naam = string.Empty;
                    repItem.BeeldvormingLevel2Naam = treeList[0].NumberName;
                }
                if (beeldvormingen.Where(x => x.treeid == treeItem.id).FirstOrDefault() is beeldvorming bv)
                {
                    repItem.A = bv.content_a;
                    repItem.V = bv.content_v;

                    if (string.IsNullOrWhiteSpace(bv.content_b))
                        repItem.B = bv.content_a;
                    else
                        repItem.B = bv.content_b;
                }

                bool shouldPrint = true;
                if (type == BeeldvormingReportPrintTypes.A && String.IsNullOrWhiteSpace(repItem.A))
                    shouldPrint = false;
                else if (type == BeeldvormingReportPrintTypes.B && String.IsNullOrWhiteSpace(repItem.B))
                    shouldPrint = false;
                else if (type == BeeldvormingReportPrintTypes.AB && String.IsNullOrWhiteSpace(repItem.A) && String.IsNullOrWhiteSpace(repItem.B))
                    shouldPrint = false;

                if (shouldPrint)
                    report.ReportItems.Add(repItem);
            }

            if (report.ReportItems.Count == 0)
                throw new Exception("Geen gegevens gevonden");

            return report;
        }
        public static void SaveDatesAndAttendees(int bewonerId, beeldvormingheader selectedBeeldvormingHeader)
        {
            using (Entities entity = new Entities(GlobalData.Instance.Entity))
            {
                beeldvormingheader bvh = entity.beeldvormingheader.Where(x => x.bewonerid == bewonerId).OrderByDescending(x => x.version).FirstOrDefault();
                bvh.besprekingClientDate = selectedBeeldvormingHeader.besprekingClientDate;
                bvh.besprekingClientText = selectedBeeldvormingHeader.besprekingClientText;
                bvh.besprekingContextDate = selectedBeeldvormingHeader.besprekingContextDate;
                bvh.besprekingContextText = selectedBeeldvormingHeader.besprekingContextText;
                bvh.besprekingTeamText = selectedBeeldvormingHeader.besprekingTeamText;
                bvh.besprekingTemDate = selectedBeeldvormingHeader.besprekingTemDate;

                entity.SaveChanges();
            }
        }
        public static List<beeldvormingtree> GetTreePath(beeldvormingtree item)
        {
            List<beeldvormingtree> list = new List<beeldvormingtree>();
            if (item == null || BeeldvormingTreeList == null)
                return list;

            list.Add(item);

            if (FindParentTreeItem(item) is beeldvormingtree p1)
            {
                list.Add(p1);
                
                if (FindParentTreeItem(p1) is beeldvormingtree p2)
                    list.Add(p2);
            }

            return list;
        }
        public static List<beeldvormingtree> GetTreePath(int itemId)
        {
            return GetTreePath(BeeldvormingTreeList.Where(x => x.id == itemId).FirstOrDefault());
        }
        public static string GetTreePathString(beeldvormingtree item)
        {
            string text = string.Empty;

            foreach (beeldvormingtree t in GetTreePath(item))
            {
                if (text != string.Empty)
                    text = text + "->";

                text = text + t.name;
            }
            return text;
        }
        public static string GetTreePathString(int itemId)
        {
            return GetTreePathString(BeeldvormingTreeList.Where(x => x.id == itemId).FirstOrDefault());
        }
        private static beeldvormingtree FindParentTreeItem(beeldvormingtree child)
        {
            if (child != null)
            {
                foreach (beeldvormingtree x in BeeldvormingTreeList)
                {
                    if (x.id == child.parentid)
                        return x;
                }
            }

            return null;
        }
        public static beeldvormingheader ReopenCurrentVersion(int bewonerId, int latestVersion, int userId)
        {
            using (Entities entity = new Entities(sharedcode.common.GlobalData.Instance.Entity))
            {
                //1 Header exists ?
                beeldvormingheader bvh = entity.beeldvormingheader.Where(x => x.bewonerid == bewonerId).OrderByDescending(x => x.version).FirstOrDefault();
                bvh.closingdate = null;
                bvh.closer = null;

                entity.SaveChanges();

                return bvh;
            }
        }
        public static beeldvormingheader CloseCurrentVersion(int bewonerId, int latestVersion, int userId)
        {
            using (Entities entity = new Entities(sharedcode.common.GlobalData.Instance.Entity))
            {
                //1 Header exists ?
                beeldvormingheader bvh = entity.beeldvormingheader.Where(x => x.bewonerid == bewonerId).OrderByDescending(x => x.version).FirstOrDefault();
                bvh.closingdate = DateTime.Now;
                bvh.closer = userId;

                entity.SaveChanges();

                return bvh;
            }
        }
        public static beeldvorming SaveBeeldvorming(int bewonerId, int treeId, int version, String A, String B, String V, String O, bool closed)
        {
            beeldvorming modItem = new beeldvorming();

            using (Entities entity = new Entities(GlobalData.Instance.Entity))
            {
                using (var t = entity.Database.BeginTransaction())
                {
                    try
                    {
                        // 1 Header exists ?
                        beeldvormingheader bvh = entity.beeldvormingheader.Where(x => x.bewonerid == bewonerId && x.version == version).FirstOrDefault();

                        if (bvh == null)
                        {
                            bvh = new beeldvormingheader()
                            {
                                creator = GlobalData.Instance.LoggedOnUser.Id.GetValueOrDefault(),
                                creationdate = DateTime.Now,
                                bewonerid = bewonerId,
                                version = version,
                                descr = "versie " + version
                            };
                            entity.beeldvormingheader.Add(bvh);
                            entity.SaveChanges();

                        }
                        // 2 save texts
                        beeldvorming bv = entity.beeldvorming.Where(x => x.treeid == treeId && x.version == version && x.bewonerid == bewonerId).FirstOrDefault();

                        if (bv == null)
                        {
                            bv = new beeldvorming();
                            entity.beeldvorming.Add(bv);
                        }
                        bv.modifier = GlobalData.Instance.LoggedOnUser.Id.GetValueOrDefault();
                        bv.modificationdate = DateTime.Now;
                        bv.content_a = A;
                        bv.content_b = B;
                        bv.content_v = V;
                        bv.content_o = O;
                        bv.version = version;
                        bv.bewonerid = bewonerId;
                        bv.treeid = treeId;
                        bv.closed = closed;
                        bv.headerid = bvh.id;

                        modItem = bv;

                        entity.SaveChanges();

                        t.Commit();
                    }
                    catch
                    {
                        t.Rollback();
                    }
                }
            }
            return modItem;
        }
        /***** SAVE CHANGES FROM SETTINGS IN DB *****/
        public static void SaveBeeldvorming(beeldvormingtree tree)
        {
            using (Entities entity = new Entities(GlobalData.Instance.Entity))
            {
                beeldvormingtree foundTree = entity.beeldvormingtree.Find(tree.id);

                if (tree.descr != foundTree.descr)
                    foundTree.descr = tree.descr;
                if (tree.infohelp != foundTree.infohelp)
                    foundTree.infohelp = tree.infohelp;

                entity.SaveChanges();
            }
        }
        public static void CreateNewVersion(int bewonerId, DateTime ExpirationDate)
        {
            beeldvormingDAL.CreateNewVersion(bewonerId, ExpirationDate);
        }
    }
}
