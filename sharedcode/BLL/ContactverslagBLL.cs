﻿using sharedcode.common;
using sharedcode.DAL;
using sharedcode.WeergaveClasses;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace sharedcode.BLL
{
    public class ContactverslagFilter: MyNotifyPropertyChanged
    {
        public gebruiker GekozenGebruiker { get; set; }
        public Contactverslag_SoortBegeleiding GekozenSoortBegeleiding { get; set; }
        public DateTime VanDatum { get; set; }
        public DateTime TotDatum { get; set; }
        public string GekozenAfspraak { get; set; }
        public bewoner GekozenBewoner { get; set; }
        public List<labels> Labels { get; set; }

        public ContactverslagFilter()
        {
            Labels = new List<labels>();
        }
        
        public string GetLabelIds()
        {
            String labelIds = "";
            foreach (labels label in Labels)
            {
                if (!String.IsNullOrWhiteSpace(labelIds))
                    labelIds += ", ";
                labelIds += label.id;
            }

            return labelIds;
        }
        public void Clear()
        {
            GekozenGebruiker = null;
            GekozenSoortBegeleiding = null;
            GekozenAfspraak = String.Empty;
            VanDatum = DateTime.Today.AddMonths(-3);
            TotDatum = DateTime.Today;

            NotifiyPropertyChanged(nameof(GekozenGebruiker));
            NotifiyPropertyChanged(nameof(GekozenSoortBegeleiding));
            NotifiyPropertyChanged(nameof(GekozenAfspraak));
            NotifiyPropertyChanged(nameof(VanDatum));
            NotifiyPropertyChanged(nameof(TotDatum));
        }
    }

    public class ContactverslagBLL
    {
        /** FIELDS */
        private ContactverslagDAL _dal;

        /** CONSTRUCTOR */
        public ContactverslagBLL()
        {
            _dal = new ContactverslagDAL();
        }

        /** METHODS */
        #region Contactverslag Methods
        public List<ContactverslagWeergave> GetAllContactverslagenFromFilter(ContactverslagFilter filter)
        {
            if (filter != null)
                return _dal.SelectAllContactverslagen(filter);

            throw new Exception("De ContactVerslagFilter is null! Contacteer a.u.b. de programmeurs.");
        }
        public ContactverslagWeergave GetContactverslagFromId(int id)
        {
            return _dal.SelectContactverslagFromId(id);
        }
        public void AddContactverslag(ContactverslagWeergave contactverslag)
        {
            _dal.InsertContactverslag(contactverslag);
        }
        public void EditContactverslag(ContactverslagWeergave contactverslag)
        {
            _dal.UpdateContactverslag(contactverslag);
        }
        public void RemoveContactverslag(ContactverslagWeergave contactverslag)
        {
            _dal.DeleteContactverslag(contactverslag);
        }
        #endregion
        #region Contactverslag_SoortBegeleiding Methods
        public List<Contactverslag_SoortBegeleiding> GetAllSoortBegeleidingen()
        {
            return _dal.SelectAllSoortBegeleidingen(false);
        }
        public List<Contactverslag_SoortBegeleiding> GetAllActiveSoortBegeleidingen()
        {
            return _dal.SelectAllSoortBegeleidingen(true);
        }
        public Contactverslag_SoortBegeleiding GetSoortBegeleidingFromId(int id)
        {
            return _dal.SelectSoortBegeleidingFromId(id);
        }
        public void ChangeSoortBegeleiding(Contactverslag_SoortBegeleiding soortBegeleiding)
        {
            _dal.UpdateSoortBegeleiding(soortBegeleiding);
        }
        #endregion
    }
}
