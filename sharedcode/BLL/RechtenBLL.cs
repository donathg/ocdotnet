﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sharedcode.BLL
{
    public class RechtenBLL
    {
        public static List<rechten_groep> GetGroepen(bool onlyOrbis)
        {
            using (Entities entity = new Entities(sharedcode.common.GlobalData.Instance.Entity))
            {
                if (onlyOrbis)
                    return entity.rechten_groep.Where(x => x.groep_orbis == "Y" && x.actif).OrderBy(x=>x.groep_code).ToList();
                else
                    return entity.rechten_groep.Where( x=> x.actif).ToList();
            }
        }

    }
}
