﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sharedcode.BLL
{

    public class CdListSaveResults
    {

        public int listHeaderId { get; set; }
        public int bewonerid { get; set; }
        public List<int> detailIds { get; set; }
        public List<String> textColumnVals { get; set; }
        public List<DateTime?> dateColumnVals { get; set; }
        public List<Decimal?> decimalColumnVals { get; set; }


    }

    public partial class cd_lists
    { 
        
 
             public static cd_lists_header GetHeader(int listHeaderId)
        {
            using (Entities entity = new Entities(sharedcode.common.GlobalData.Instance.Entity))
            {
                return entity.cd_lists_header.Where(x => x.id == listHeaderId).FirstOrDefault();
            }
        }

        public static List<cd_lists_header> GetHeaders(List<int> listHeaderIds)
        {
            using (Entities entity = new Entities(sharedcode.common.GlobalData.Instance.Entity))
            {
                return entity.cd_lists_header.Where(x => listHeaderIds.Contains(x.id)).ToList();
            }
        }

        public static List<cd_lists_detail> GetListItems(int listHeaderId)
        {
            using (Entities entity = new Entities(sharedcode.common.GlobalData.Instance.Entity))
            {
                return entity.cd_lists_detail.Where(x => x.headerid == listHeaderId && x.actif==true).OrderBy(x=>x.orderNum).ToList();
            }
        }
        public static List<cd_lists_detail> GetListItems(List<int> listHeaderIds)
        {
            using (Entities entity = new Entities(sharedcode.common.GlobalData.Instance.Entity))
            {
                return entity.cd_lists_detail.Where(x => listHeaderIds.Contains(x.headerid) && x.actif == true).OrderBy(x => x.orderNum).ToList();
            }
        }

        public static List<cd_lists_results> GetResultItems(int listHeaderId, int bewonerId)
        {
            using (Entities entity = new Entities(sharedcode.common.GlobalData.Instance.Entity))
            {
                return entity.cd_lists_results.Where(x => x.headerid == listHeaderId && x.bewonerid == bewonerId).ToList();
            }
        }
        public static List<cd_lists_results> GetResultItems(List<int> listHeaderIds, int bewonerId)
        {
            using (Entities entity = new Entities(sharedcode.common.GlobalData.Instance.Entity))
            {
                return entity.cd_lists_results.Where(x => listHeaderIds.Contains(x.headerid) && x.bewonerid == bewonerId).ToList();
            }
        }



        

        public static void SaveResults(List<CdListSaveResults> results)
        {
            using (Entities entity = new Entities(sharedcode.common.GlobalData.Instance.Entity))
            {
                foreach (CdListSaveResults r in results)
                {
                    String sql = "delete from  cd_lists_results where bewonerid = " + r.bewonerid + " and headerid = " + r.listHeaderId;
                    entity.Database.ExecuteSqlCommand(sql);
                    List<cd_lists_results> newList = new List<cd_lists_results>();
                    int i = 0;
                    foreach (int idd in r.detailIds)
                    {
                        cd_lists_results ni = new cd_lists_results() { bewonerid = r.bewonerid, headerid = r.listHeaderId, detailid = idd, columnstring = r.textColumnVals[i], columndate = r.dateColumnVals[i], columndecimal = r.decimalColumnVals[i] };
                        entity.cd_lists_results.Add(ni);
                        i++;
                    }
                }
                entity.SaveChanges();
            }
        }
    }
}
