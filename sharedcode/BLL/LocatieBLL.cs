﻿using sharedcode.common;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.ComponentModel;
using sharedcode.DAL;
using System;
using sharedcode.WeergaveClasses;

namespace sharedcode.BLL
{
    [Serializable]
    public partial class locatie: MyNotifyPropertyChanged
    {
        private int _aantalAsbest;
        public int AantalAsbest
        {
            get
            {
                return _aantalAsbest;
            }
            set
            {
                _aantalAsbest = value;
                NotifiyPropertyChanged(nameof(AantalAsbest));
            }
        }
        private int _aantalBijlagen;
        public int AantalBijlagen
        {
            get
            {
                return _aantalBijlagen;
            }
            set
            {
                _aantalBijlagen = value;
                NotifiyPropertyChanged(nameof(AantalBijlagen));
            }
        }
        private int _aantalRacks;
        public int AantalRacks
        {
            get
            {
                return _aantalRacks;
            }
            set
            {
                _aantalRacks = value;

                NotifiyPropertyChanged(nameof(AantalRacks));
            }
        }
    }


    public class LocatieBLL
    {
        public List<locatie> GetAllLocaties()
        {
            return LocatieDAL.SelectAllLocaties(null);
        }
        public List<locatie> GetAllActiveLocaties()
        {
            return LocatieDAL.SelectAllLocaties(true);
        }
        public List<locatie> GetAllInactiveLocaties()
        {
            return LocatieDAL.SelectAllLocaties(false);
        }
        public void AddLocatie(locatie loc)
        {
            LocatieDAL.InsertLocatie(loc);
        }
        public void ModifyLocatie(locatie loc)
        {
            LocatieDAL.UpdateLocatie(loc);
        }
        public void RemoveLocatie(locatie loc)
        {
            LocatieDAL.DeleteLocatieAndSublocaties(loc);
        }
        public void SetStatusInactive(locatie loc)
        {
            loc.active = false;
            LocatieDAL.SetActiveStatus(loc);
        }
        public void SetStatusActive(locatie loc)
        {
            loc.active = true;
            LocatieDAL.SetActiveStatus(loc);
        }
    }

    public class LocatieManager
    {
        private LocatieProvider _provider;

        public LocatieManager(LocatieProvider provider)
        {
            _provider = provider;
        }

        public List<locatie> GetAllLocaties()
        {
            return _provider.GetAllLocaties();
        }
        public locatie GetLocatieFromId(int id)
        {
            return _provider.GetLocatieFromId(id);
        }
        public List<InventarisWeergave> GetAllInventarisFromLocatie(locatie loc)
        {
            return _provider.GetAllInventarisFromLocatie(loc);
        }
        public List<locatie_objecten> GetAllObjectenFromLocatie(locatie loc)
        {
            return _provider.GetAllObjectenFromLocatie(loc);
        }
        public List<objecttype> GetallObjecttypes()
        {
            return _provider.GetallObjecttypes();
        }
    }

    public class LocatieProvider
    {
        private List<locatie> _locatieList;
        private List<InventarisWeergave> _inventarisList;
        private List<locatie_objecten> _objectenList;
        private List<objecttype> _objecttypesList;
        private Dictionary<int, int> _dicAsbestOnLocation = new Dictionary<int, int>();

        public LocatieProvider()
        {
            _locatieList = new List<locatie>();
            LoadLocaties();
            LoadInventaris();
            LoadObjecten();
            LoadObjecttypes();
            SetObjectenIconsOnLocation();
        }

        private void LoadObjecten()
        {
            using (Entities entity = new Entities(GlobalData.Instance.Entity))
            {
                _objectenList = entity.locatie_objecten.Include("locatie").Include("objecttype").ToList();
            }
        }
        private void LoadInventaris()
        {
            _inventarisList = new InventarisBLL(new InventarisSettings()).GetAllInventarisItems();
        }
        private void LoadLocaties()
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                String sql = @"SELECT *, (SELECT count(loc_id) FROM locatie_files WHERE l.id = locatie_files.loc_id) AS aantalBijlagen 
                                FROM locatie AS l
                                ORDER BY l.naam";

                try
                {
                    foreach (DataRow row in db.GetDataTable(sql).Rows)
                    {
                        locatie loc = new locatie()
                        {
                            id = (int)row["id"],
                            naam = row["naam"].ToString(),
                            beschrijving = row["beschrijving"].ToString(),
                            opmerking = row["opmerking"].ToString(),
                            AantalBijlagen = (int)row["aantalBijlagen"]
                        };

                        if (!row.IsNull("parent_id"))
                        {
                            loc.parent_id = (int)row["parent_id"];
                        }

                        _locatieList.Add(loc);
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
        private void LoadObjecttypes()
        {
            using (Entities entity = new Entities(GlobalData.Instance.Entity))
            {
                _objecttypesList = entity.objecttype.OrderBy(x => x.naam).ToList();
            }
        }
        private void SetObjectenIconsOnLocation()
        {
            foreach (locatie_objecten obj in _objectenList)
            {
                if (obj.objecttype_id == 1 /*asbest*/)
                {
                    if (_dicAsbestOnLocation.ContainsKey(obj.loc_id) == false)
                    {
                        _dicAsbestOnLocation.Add(obj.loc_id, 1);
                    }
                    else
                    {
                        _dicAsbestOnLocation[obj.loc_id]++;
                    }
                    _locatieList.Where(x => x.id == obj.loc_id).Single().AantalAsbest++;
                }
                else if (obj.objecttype_id == 4 /*Datarack*/)
                {
                    _locatieList.Where(x => x.id == obj.loc_id).Single().AantalRacks++;
                }
            }
        }
            
        public List<locatie> GetAllLocaties()
        {
            return _locatieList;
        }
        public locatie GetLocatieFromId(int id)
        {
            return _locatieList.Where(x => x.id == id).Single();
        }
        public List<InventarisWeergave> GetAllInventarisFromLocatie(locatie loc)
        {
            return _inventarisList.Where(x => x.locatie_id == loc.id).ToList();
        }
        public List<locatie_objecten> GetAllObjectenFromLocatie(locatie loc)
        {
            return _objectenList.Where(x => x.loc_id == loc.id).ToList();
        }
        public List<objecttype> GetallObjecttypes()
        {
            return _objecttypesList;
        }
    }
}