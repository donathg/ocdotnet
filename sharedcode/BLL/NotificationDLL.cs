﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using sharedcode.common;

namespace sharedcode.BLL
{
    public class NotificationDLL
    {
        public static List<dashboard> GetNotificationsClientDossier (List<int> bewonerIdList,DateTime fromDate)
        {
            using (Entities entity = new Entities(sharedcode.common.GlobalData.Instance.Entity))
            {
                return entity.dashboard.Where(x => bewonerIdList.Contains(x.bewonerId.GetValueOrDefault()) && x.creationdate >= fromDate && x.creatorId != GlobalData.Instance.LoggedOnUser.Id.Value).Include("bewoner").OrderByDescending(x=>x.creationdate).Take(20).ToList();
            }
        }
        public static List<dashboard> GetNotificationsClientDossier( DateTime fromDate)
        {
            using (Entities entity = new Entities(sharedcode.common.GlobalData.Instance.Entity))
            {
                return entity.dashboard.Where(x=> x.bewonerId != null  && x.creationdate >= fromDate && x.creatorId != GlobalData.Instance.LoggedOnUser.Id.Value).OrderByDescending(x => x.creationdate).Include("bewoner").Take(20).ToList();
            }
        }

        public static List<dashboard> GetNotifications(DateTime fromDate)
        {
            using (Entities entity = new Entities(sharedcode.common.GlobalData.Instance.Entity))
            {
                return entity.dashboard.Where (x=> x.creationdate >= fromDate && x.creatorId != GlobalData.Instance.LoggedOnUser.Id.Value && x.bewonerId==null).OrderByDescending(x => x.creationdate).Take(20).ToList();
            }
        }
        public static List<dashboard> GetNotifications()
        {
            using (Entities entity = new Entities(sharedcode.common.GlobalData.Instance.Entity))
            {
                return entity.dashboard.Where(x=>x.creatorId != GlobalData.Instance.LoggedOnUser.Id.Value && x.bewonerId == null).OrderByDescending(x => x.creationdate).Take(20).ToList();
            }
        }
    }
}
