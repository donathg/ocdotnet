﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using sharedcode.DAL;
using sharedcode.WeergaveClasses;

namespace sharedcode.BLL
{
    public class AdressenListSettings
    {
        public bewoner Bewoner { get; set; }
        public List<adrestype> AdresTypes { get; set; }

        public String GetAllAdrestypeIds()
        {
            String typeIds = "";
            foreach (adrestype type in AdresTypes)
            {
                if (!String.IsNullOrWhiteSpace(typeIds))
                    typeIds += ", ";
                typeIds += type.id;
            }

            return typeIds;
        }
    }

    public abstract class AdresBLLProvider
    {
        #region adrestype methods
        public adrestype GetAdrestypeFromId(int id)
        {
            return AdresDALProvider.SelectAdrestypeFromId(id);
        }
        public abstract List<adrestype> GetAllAdrestypes();
        public abstract List<adrestype> GetAllHoofdAdrestypes();
        public abstract List<adrestype> GetAllSubAdrestypes();
        public abstract List<adrestype> GetAllHoofdPersoonAdrestypes();
        public abstract List<adrestype> GetAllSubPersoonAdrestypes();
        public abstract List<adrestype> GetAllHoofdBedrijfAdrestypes();
        public abstract List<adrestype> GetAllSubBedrijfAdrestypes();
        #endregion
        #region contactpersonen methods
        public List<ContactpersoonWeergave> GetContactpersonenListFromAdresid(int adresId)
        {
            return AdresDALProvider.SelectAllContactpersonenFromAdresId(adresId);
        }
        public ContactpersoonWeergave GetContactpersoonFromId(int id)
        {
            return AdresDALProvider.SelectContactpersoonFromId(id);
        }
        public abstract void RemoveContactpersoonFromDB(ContactpersoonWeergave selectedContactpersoon);
        #endregion
        #region adres methods
        public AdresWeergave GetAdresWeergaveFromId(int id)
        {
            return AdresDALProvider.SelectAdressenFromId(id);
        }
        public abstract void AddNewAdres(AdresWeergave adres);
        public abstract void EditAdres(AdresWeergave adres);
        public List<AdresWeergave> GetAllAdressenFromType(adrestype type)
        {
            return AdresDALProvider.SelectAllAdresweergaveFromType(type);
        }
        #endregion
    }
    public class BewonerAdresBLLProvider : AdresBLLProvider
    {
        /** FIELDS */
        private AdresBewonerDALProvider _dalProvider;
        private AdressenListSettings _settings;

        public AdressenListSettings Settings
        {
            get
            {

                return _settings;
            }

        }

        /** CONSTRUCTORS */
        public BewonerAdresBLLProvider()
        {
            _dalProvider = new AdresBewonerDALProvider();
        }
        public BewonerAdresBLLProvider(AdressenListSettings settings) : this()
        {
            _settings = settings;
        }
        public BewonerAdresBLLProvider(bewoner bew) : this()
        {
            _settings = new AdressenListSettings() { Bewoner = bew };
        }

        /** METHODS */
        #region adressen methods
        public override void AddNewAdres(AdresWeergave adres)
        {
            _dalProvider.InsertAdres(adres);
        }
        public override void EditAdres(AdresWeergave adres)
        {
            _dalProvider.UpdateAdres(adres);
        }
        public void DeleteAdresAndContactpersonen(AdresWeergave adres)
        {
            _dalProvider.DeleteBewoneradresAndBewonercontactpersonen(adres);
        }
        public List<AdresWeergave> GetAllPersoonAdressenFromBewoner(bewoner bewonerId)
        {
            return _dalProvider.SelectAllAdresweergaveFromCategory(false, bewonerId);
        }
        public List<AdresWeergave> GetAllBedrijfAdressen()
        {
            return _dalProvider.SelectAllAdresweergaveFromCategory(true, null);
        }




        private static Dictionary<int, List<AdresWeergave>> _cachedAdresWeergaveList = new Dictionary<int, List<AdresWeergave>>();
  

        public static void ClearCache()
        {
            _cachedAdresWeergaveList.Clear();
        }
        private static bool IsCacheEmpty(int bewonerId)
        {

            if (_cachedAdresWeergaveList == null || _cachedAdresWeergaveList.Count == 0)
                return true;
            if (_cachedAdresWeergaveList.ContainsKey(bewonerId) == false)
                return true;

            return false;

        }



        public static void FillCache(bewoner bew)
        {

            if (bew != null)
            {
                ClearCache();
                BewonerAdresBLLProvider bll = new BewonerAdresBLLProvider(new AdressenListSettings() { Bewoner = bew });
                List<AdresWeergave> list = bll.GetAllAdressenFromSetttings(true);
                _cachedAdresWeergaveList.Add(bew.id, list);
            }
        }

        public static List<AdresWeergave> GetAdresFamilieOpOC  (bewoner bew, List<bewoner> bewonerListToSearch)
        {
            if (IsCacheEmpty(bew.id))
                FillCache(bew);

            List<AdresWeergave> returnValue = new List<AdresWeergave>();
            foreach (  AdresWeergave  w  in _cachedAdresWeergaveList[bew.id])
            {
                foreach (bewoner b in bewonerListToSearch)
                {
                    if (w.rijksregisternummer == b.rijksregisternr)
                        returnValue.Add(w);

                }
            }
            return returnValue;
        }

        public static AdresWeergave GetAdresFromCache (bewoner bew, int adresTypeId)
        {
         /*   if (IsCacheEmpty(bew.id))
                FillCache(bew);

            List<AdresWeergave> returnValue = new List<AdresWeergave>();
            foreach (AdresWeergave w in _cachedAdresWeergaveList[bew.id])
            {
                if (w.adrestype_id == adresTypeId)
                    return w;
            }*/
            return new AdresWeergave();
        }

        public List<AdresWeergave> GetAllAdressenFromSetttings(bool fromCache = false)
        {

           
                /* getting al the adresses */
                List<AdresWeergave> tempList = _dalProvider.SelectAllAdresweergavesFromSettings(_settings);
                if (_settings.Bewoner is bewoner bew)
                {
                    /* flagging all the contacts deemed as "MY CONTACT" */
                    List<int> contactpersonenForBewoner = _dalProvider.SelectAllContactpersonenFromBewonerid(bew.id); // id's of the contacts that livein has defined as "MY CONTACT"
                    foreach (AdresWeergave adres in tempList)
                    {
                        foreach (ContactpersoonWeergave contactpersoon in adres.Contactpersonen)
                        {
                            contactpersoon.HasPresentBewoner = contactpersonenForBewoner.Any(x => x == contactpersoon.id);
                        }

                        adres.Contactpersonen = new ObservableCollection<ContactpersoonWeergave>(adres.Contactpersonen.OrderBy(x => x.HasPresentBewoner).ToList());
                    }
                   
                 
                
                }

        
               return tempList;
        }
        #endregion
        #region adrestype methods
        public override List<adrestype> GetAllAdrestypes()
        {
            return _dalProvider.SelectAllAdrestypes(null, null);
        }
        public override List<adrestype> GetAllHoofdAdrestypes()
        {
            return _dalProvider.SelectAllAdrestypes(true, null);
        }
        public override List<adrestype> GetAllSubAdrestypes()
        {
            return _dalProvider.SelectAllAdrestypes(false, null);
        }
        public override List<adrestype> GetAllHoofdPersoonAdrestypes()
        {
            return _dalProvider.SelectAllAdrestypes(true, false);
        }
        public override List<adrestype> GetAllSubPersoonAdrestypes()
        {
            return _dalProvider.SelectAllAdrestypes(false, false);
        }
        public override List<adrestype> GetAllHoofdBedrijfAdrestypes()
        {
            return _dalProvider.SelectAllAdrestypes(true, true);
        }
        public override List<adrestype> GetAllSubBedrijfAdrestypes()
        {
            return _dalProvider.SelectAllAdrestypes(false, true);
        }
        #endregion
        #region contactpersonen methods
        public override void RemoveContactpersoonFromDB(ContactpersoonWeergave selectedContactpersoon)
        {
            _dalProvider.DeleteContactpersoon(selectedContactpersoon);
        }
        public void RemoveBewoneradresContactpersoon(ContactpersoonWeergave contactpersoon)
        {
            _dalProvider.DeleteBewonerContactpersoon(contactpersoon);
        }
        #endregion
    }
    public class CampusAdresBLLProvider : AdresBLLProvider
    {
        /** FIELDS */
        private AdresCampusDALProvider _dalProvider;

        /** CONSTRUCTORS */
        public CampusAdresBLLProvider()
        {
            _dalProvider = new AdresCampusDALProvider();
        }

        /** METHODS */
        #region adressen methods
        public override void AddNewAdres(AdresWeergave adres)
        {
            _dalProvider.InsertOrUpdateAdres(adres);
        }
        public override void EditAdres(AdresWeergave adres)
        {
            _dalProvider.InsertOrUpdateAdres(adres);
        }
        public AdresWeergave GetAdresWeergaveFromLocatieid(int locatieId)
        {
            return _dalProvider.SelectAdresweergavesFromOwnerId(locatieId).SingleOrDefault();
        }
        #endregion
        #region adrestype methods
        public override List<adrestype> GetAllAdrestypes()
        {
            return _dalProvider.SelectAllAdrestypes(null, null);
        }
        public override List<adrestype> GetAllHoofdAdrestypes()
        {
            return _dalProvider.SelectAllAdrestypes(true, null);
        }
        public override List<adrestype> GetAllSubAdrestypes()
        {
            return _dalProvider.SelectAllAdrestypes(false, null);
        }
        public override List<adrestype> GetAllHoofdPersoonAdrestypes()
        {
            return _dalProvider.SelectAllAdrestypes(true, false);
        }
        public override List<adrestype> GetAllSubPersoonAdrestypes()
        {
            return _dalProvider.SelectAllAdrestypes(false, false);
        }
        public override List<adrestype> GetAllHoofdBedrijfAdrestypes()
        {
            return _dalProvider.SelectAllAdrestypes(true, true);
        }
        public override List<adrestype> GetAllSubBedrijfAdrestypes()
        {
            return _dalProvider.SelectAllAdrestypes(false, true);
        }
        #endregion
        public locatie GetLocatieFromId(int id)
        {
            return _dalProvider.SelectLocatieFromId(id);
        }
        public override void RemoveContactpersoonFromDB(ContactpersoonWeergave selectedContactpersoon)
        {
            _dalProvider.DeleteContactpersoon(selectedContactpersoon);
        }
    }
    // STILL NEEDS TO BE DONE: a.t.m. just gives errors
    public class LeverancierAdresBLLProvider : AdresBLLProvider
    {
        /** FIELDS */
        private AdresLeverancierDALProvider _provider;

        /** CONSTRUCTORS */
        public LeverancierAdresBLLProvider()
        {
            _provider = new AdresLeverancierDALProvider();
        }

        /** METHODS */
        public override void AddNewAdres(AdresWeergave adres)
        {
            throw new NotImplementedException();
        }
        public override void EditAdres(AdresWeergave adres)
        {
            throw new NotImplementedException();
        }
        public override void RemoveContactpersoonFromDB(ContactpersoonWeergave selectedContactpersoon)
        {
            throw new NotImplementedException();
        }
        #region adrestype methods
        public override List<adrestype> GetAllAdrestypes()
        {
            throw new NotImplementedException();
        }
        public override List<adrestype> GetAllHoofdAdrestypes()
        {
            throw new NotImplementedException();
        }
        public override List<adrestype> GetAllHoofdBedrijfAdrestypes()
        {
            throw new NotImplementedException();
        }
        public override List<adrestype> GetAllHoofdPersoonAdrestypes()
        {
            throw new NotImplementedException();
        }
        public override List<adrestype> GetAllSubAdrestypes()
        {
            throw new NotImplementedException();
        }
        public override List<adrestype> GetAllSubBedrijfAdrestypes()
        {
            throw new NotImplementedException();
        }
        public override List<adrestype> GetAllSubPersoonAdrestypes()
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
