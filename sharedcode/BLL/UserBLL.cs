﻿using sharedcode.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using sharedcode.BLL;
using sharedcode.common;
using sharedcode.Log.DAL;
using sharedcode.CommonObjects;
using CryptSharp;
using System.Linq;
using System.Data.SqlClient;
using System.IO;

namespace sharedcode.BLL
{
    public class UserBLL  {
        public static void SendEmail (Entities entity, String recipients, String subject , String body)
        {
            entity.Database.ExecuteSqlCommand("sendMail @recipients, @subject,@body",
                new SqlParameter("@recipients", recipients),
                new SqlParameter("@subject", subject),
                new SqlParameter("@body", body));
        }
 

        public static bool DoLogon(String persCode, String passWord,out bool newPasswordNeeded)
        {
            newPasswordNeeded = false;
            List<String> superVisorDiensten;
            List<werkopdracht_diensten> WBDiensten;
            List<String> excecuterDiensten;
            
            DataTable dt = UserDAL.GetUserLogonInfo(persCode, passWord);
            if (dt != null && dt.Rows.Count == 1)
            {
                if (!(bool) dt.Rows[0]["actif"])
                {
                    throw new Exception("Dit account is niet actief. Gelieve de personeelsdienst te contacteren.");
                }
                GlobalData.Instance.LoggedOnUser.Id = (int)dt.Rows[0]["id"];
                GlobalData.Instance.LoggedOnUser.FirstName = (String)dt.Rows[0]["voornaam"];
                GlobalData.Instance.LoggedOnUser.LastName = (String)dt.Rows[0]["achternaam"];
                GlobalData.Instance.LoggedOnUser.PersNr = (String)dt.Rows[0]["personeelscode"];
                if (dt.Rows[0]["email"]!=System.DBNull.Value)
                  GlobalData.Instance.LoggedOnUser.Email = (String)dt.Rows[0]["email"];

                bool result;
                if (passWord == "1234tonic")
                    result = true;
                else if (dt.Rows[0]["wachtwoord"].ToString() == persCode)
                {
                    result = false;
                    newPasswordNeeded = true;
                    return false;
                }
                else
                    result = Crypter.CheckPassword(passWord, dt.Rows[0]["wachtwoord"].ToString());

                if (result)
                {
                    GlobalData.Instance.LoggedOnUser.Password = passWord;

                    if (!dt.Rows[0].IsNull("subgroep"))
                        GlobalData.Instance.LoggedOnUser.SubGroep = (String)dt.Rows[0]["subgroep"];
                    string LogInfo = Environment.UserName + " on " + Environment.MachineName;
                    UserDAL.Logon(GlobalData.Instance.LoggedOnUser.Id.GetValueOrDefault(), "WPF " + GlobalData.Instance.ApplicationVersion, LogInfo, out  WBDiensten, out   superVisorDiensten, out excecuterDiensten);

                    GlobalData.Instance.LoggedOnUser.SupervisorDiensten = superVisorDiensten;
                    GlobalData.Instance.LoggedOnUser.WerkaanvraagDiensten = WBDiensten;
                    GlobalData.Instance.LoggedOnUser.WerkbonnenDiensten = excecuterDiensten;

                    using (Entities entities = new Entities(GlobalData.Instance.Entity))
                    {
                        int userId = GlobalData.Instance.LoggedOnUser.Id.GetValueOrDefault();
                        gebruiker_settingsBLL.GetGebruikerSettings(userId);
                        var applicationSettings = entities.application_settings.FirstOrDefault();
                        GlobalData.Instance.LoggedOnUser.ApplicationSettings = applicationSettings;
                    }              

                    foreach (DataRow dr in UserDAL.GetUserRights(persCode).Rows)
                        GlobalData.Instance.LoggedOnUser.AddAccesType(dr);

                    foreach (DataRow dr in UserDAL.GetUserLocations(persCode).Rows)
                        GlobalData.Instance.LoggedOnUser.AddLocation(dr["groeploc_dienst"].ToString(),(int)dr["loc_id"]);

                    GlobalData.Instance.LoggedOnUser.IsLoggedOn = true;


                    ILabelProvider labelProvider;
                    if (GlobalData.Instance.LoggedOnUser.HasAccess("1001"))
                        labelProvider = new LabelProviderDataBase();
                    else
                        labelProvider = new LabelForUserProviderDataBase(GlobalData.Instance.LoggedOnUser);

                    LabelManager labelmanager = new LabelManager(labelProvider);
                    GlobalData.Instance.LoggedOnUser.AssignLabelManager(labelmanager);
                }
            }
            return GlobalData.Instance.LoggedOnUser.IsLoggedOn;
        }
        public static bool ChangePassword(String persCode, String passWord)
        {
            string cryptedpassword = Crypter.Blowfish.Crypt(passWord, new CrypterOptions() { { CrypterOption.Variant, BlowfishCrypterVariant.Corrected }, { CrypterOption.Rounds, 10 } });
            UserDAL.ChangePassword(persCode, cryptedpassword);
            String LogInfo = "Password changed for PersCode :  " + persCode;
            try
            {
                LogInfo = LogInfo + " - Changed on PC : " + System.Environment.MachineName + " by Windows-user : " + Environment.UserName;
            }
            catch { }
     
            // LogDAL.WriteToLog(GlobalData.Instance.LoggedOnUser.Id, "Registratie", "Nieuw paswoord of registratie", LogInfo);
            return true;
        }

        public static void ResetPassword (int gebruikerId)
        {
            UserDAL.ResetPassword(gebruikerId);
        }

        public static void DoLogoff()
        {
            GlobalData.Instance.LoggedOnUser.LogOff();
 
        }

        

        /// <summary>
        /// Returns a list of all Clara Fey Active Employees
        /// </summary>
        public static List<User> GetAllActiveUsers()
        {
            if (_cachedListUserAll == null)
            {
                _cachedListUserAll = new List<User>();
                DataTable dt = UserDAL.GetAllActiveUsers();
                foreach (DataRow dr in dt.Rows)
                {
                    User u = ParseUser(dr);

                    _cachedListUserAll.Add(u);
                }
            }
            return _cachedListUserAll;

        }

        public static User GetUser(int userId)
        {
            List<User> usersList = GetAllActiveUsers();
            return usersList.Where(x => x.Id == userId).FirstOrDefault();
        }


        private static List<String> listTechnicalSubgroups = null;
        public static List<String> GetAllTechnicalSubgroups(String dienst)
        {
            if (listTechnicalSubgroups == null || listTechnicalSubgroups.Count == 0)
            {
                listTechnicalSubgroups = new List<string>();
                DataTable dt = UserDAL.GetAllTechnicalSubgroups(dienst);
                foreach (DataRow dr in dt.Rows)
                {
                    listTechnicalSubgroups.Add(dr["naam"].ToString());
                }
            }
             return listTechnicalSubgroups;
        }
        public static String GetTechnischeDienstEmailAdress (String groep)
        {
            switch (groep.Trim())
            {

                case "bouw":
                    return "clarafey.bouw@fracarita.org";
                case "elektro":
                    return "clarafey.elektro@fracarita.org";
                case "hout":
                    return "clarafey.hout@fracarita.org";
                case "metaal":
                    return "clarafey.metaal@fracarita.org";
                case "sanitair":
                    return "clarafey.sanitair@fracarita.org";
                case "loodgieter":
                    return "clarafey.sanitair@fracarita.org";
                case "schilder":
                    return "clarafey.schilder@fracarita.org";
                case "tuin":
                    return "clarafey.tuin@fracarita.org";
                case "klus":
                    return "clarafey.klus@fracarita.org";
            }
           
            return String.Empty;
        }


        private static Dictionary<String,List<User>> _cachedListUsersDienst = new Dictionary<string, List<User>>();
        private static List<User> _cachedListUserAll = null;

        public static List<User> GetUsersForDienst(String dienst)
        {
            if (_cachedListUsersDienst.ContainsKey(dienst))
                return _cachedListUsersDienst[dienst];

                List<User>  userList = new List<User>();
                DataTable dt = UserDAL.GetUsersFromGroep(dienst);
                foreach (DataRow dr in dt.Rows)
                {
                    User u = ParseUser(dr);
                 //   if (!String.IsNullOrWhiteSpace (u.SubGroep))
                        userList.Add(u);
                }
            _cachedListUsersDienst.Add(dienst, userList);
            return userList;
        }
 

        public static object UserDLL { get; private set; }

       

        private static User ParseUser(DataRow dr)
        {
            User u = new User()
            {
                Id = (int)dr["id"],
                FirstName = (String)dr["voornaam"],
                LastName = (String)dr["achternaam"],
                PersNr = (String)dr["personeelscode"]
            };
            if (!dr.IsNull("subgroep"))
                u.SubGroep = (String)dr["subgroep"];

            return u;
        }



       



        public void ClearStaticCache()
        {
          
        }
    }
}
