﻿
using sharedcode.common;
using sharedcode.DAL;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
 
using System.Linq;
 
namespace sharedcode.BLL
{
    public partial class vw_campussen
    {
        public static List<vw_campussen> GetCampussen()
        {
            using (Entities entity = new Entities(sharedcode.common.GlobalData.Instance.Entity))
            {
                return entity.vw_campussen.ToList();
            }
        }
    }
    public class Location : MyNotifyPropertyChanged
    {
        /** FIELDS */
        private int? _id;
        public int? Id
        {
            get { return _id; }
            set
            {
                _id = value;
                NotifiyPropertyChanged(nameof(Id));
            }
        }
        private int? _parentId;
        public int? ParentId
        {
            get { return _parentId; }
            set
            {
                _parentId = value;
                NotifiyPropertyChanged(nameof(ParentId));
            }
        }

        private string _name;
        public string Name
        {
            get { return _name; }
            set
            {
                _name = value;

                NotifiyPropertyChanged(nameof(Name));
            }
        }
        private string _description;
        public string Description
        {
            get { return _description; }
            set
            {
                _description = value;

                NotifiyPropertyChanged(nameof(Description));
            }
        }
        private string _remark;
        public string Remark
        {
            get { return _remark; }
            set
            {
                _remark = value;

                NotifiyPropertyChanged(nameof(Description));
            }
        }
        private int _attachments;
        public int Attachments
        {
            get
            {
                return _attachments;
            }

            set
            {
                if (_attachments != value)
                {
                    _attachments = value;
                    NotifiyPropertyChanged(nameof(Attachments));
                    NotifiyPropertyChanged(nameof(HasAttachments));
                }
            }
        }

        private bool _isExpanded;
        public bool IsExpanded
        {
            get { return _isExpanded; }
            set
            {
                _isExpanded = value;
                NotifiyPropertyChanged(nameof(IsExpanded));
            }
        }
        private bool _isSelected;
        public bool IsSelected
        {
            get { return _isSelected; }
            set
            {
                _isSelected = value;
                NotifiyPropertyChanged(nameof(IsSelected));
            }
        }
        private bool _isFocusable;
        public bool IsFocusable
        {
            get { return _isFocusable; }
            set
            {
                _isFocusable = value;
                NotifiyPropertyChanged(nameof(IsFocusable));
            }
        }
        public bool HasAttachments
        {
            get
            {
                return _attachments>0;
            }
        }
        /// <summary>
        /// een campus is een treeitem op het bovenste niveau. Maw heeft geen parent.
        /// </summary>
        public bool IsCampus
        {
            get { return Parent == null; }
        }
        public bool Active { get; set; }

        public Location Parent { get; set; }
        public ObservableCollection<Location> Children { get; set; }

        /** CONSTRUCTORS */
        public Location()
        {
            Children = new ObservableCollection<Location>();
        }

        /** METHODS */
        public Location Clone()
        {
            Location location = new Location
            {
                _id = _id,
                Children = Children,
                Parent = Parent,
                ParentId = ParentId,
                _name = _name,
                _description = _description,
                _remark = _remark
            };
            return location;
        }
        public override string ToString()
        {
            return _name;
        }
        public string GetFullPath()
        {
            String fp = String.Empty;

            fp = AddNameToLocationString(this, fp);
            return fp;
        }
        private string AddNameToLocationString (Location loc, String s)
        {
            if (loc!=null)
            {
                if (s.Length > 0)
                    s = loc.Name + " - " + s;
                else s = loc.Name;
                s = AddNameToLocationString(loc.Parent, s);
            }
            return s;
        }
    }
    public class GebouwenBeheerLoader  
    {
        private static DataTable dtLocations = null;

        public static Dictionary<int, Location> FlatList { get; private set; } = new Dictionary<int, Location>();
        private static ObservableCollection<Location> _listLocation = new ObservableCollection<Location>();

        public static ObservableCollection<Location> ListLocation
        {
            get { return GebouwenBeheerLoader._listLocation; }
       
        }
        public static Location GetLocationById(int locId)
        {
            if (FlatList == null)
                Load();
            if (FlatList.ContainsKey(locId) && FlatList[locId] is Location loc)
                return loc;
            else
                // when werbon of aanvraag op inactieve locatie staat
                return ParseLocationRow(dtLocations.AsEnumerable().Where(x => (int)x["id"] == locId).SingleOrDefault());
        }
        public static void Load()
        {
            try
            {
                if (FlatList != null)
                    FlatList.Select(c => { c.Value.IsExpanded = false; c.Value.IsSelected = false; return c; }).ToList();
                if (dtLocations == null)
                {
                    dtLocations = GebouwenBeheerDAL.loadLocatieTree();
                    FlatList = new Dictionary<int, Location>();
                    _listLocation = new ObservableCollection<Location>();
                    List<DataRow> drToRemove = new List<DataRow>();
                    foreach (DataRow dr in dtLocations.Rows)
                    {
                        if (dr.IsNull("parent_id"))
                        {
                            Location loc = ParseLocationRow(dr);
                            FlatList.Add(loc.Id.Value, loc);
                            _listLocation.Add(loc);
                            Load_AddChildLocations(loc);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private static void Load_AddChildLocations(Location parent)
        {
            var filteredRows = dtLocations.AsEnumerable().Where(row => row.Field<int?>("parent_id") == parent.Id);
            if (filteredRows.Count() == 0)
                return;
            DataTable tblFiltered = filteredRows.CopyToDataTable();
            foreach (DataRow dr in tblFiltered.Rows)
            {
                Location loc = ParseLocationRow(dr);
                if (loc.Active)
                {
                    loc.Parent = parent;
                    parent.Children.Add(loc);
                    FlatList.Add(loc.Id.Value, loc);
                    Load_AddChildLocations(loc);
                }
            }
        }
        private static Location ParseLocationRow(DataRow dr)
        {
            Location location = new Location
            {
                Id = (int)dr["id"],
                Name = dr["naam"].ToString(),
                Active = (bool)dr["active"],
                Attachments = (int)dr["attachements"]
            };
            if (!dr.IsNull("opmerking"))
                location.Remark = dr["opmerking"].ToString();
            if (!dr.IsNull("beschrijving"))
                location.Description = dr["beschrijving"].ToString();
            if (dr.IsNull("parent_id"))
                location.ParentId = null;
            else
                location.ParentId = (int)dr["parent_id"];

            return location;
        }
        public static void ClearStaticCache()
        {
            dtLocations = null;
            if (FlatList != null)
            {
                FlatList.Clear();
                FlatList = null;
            }
            if (_listLocation != null)
            {
                _listLocation.Clear();
                _listLocation = null;
            }
        }
    }
    public enum GebouwenBeheerSelectionModeType
    {
        selectall,
        selectlowestonly,
        selectnone
    }
    public class GebouwenBeheerSettings 
    {
        public GebouwenBeheerSelectionModeType SelectionMode { get; set; }
        /// <summary>
        /// Wanneer deze filter is gezet zal enkel een bepaald deel getoond worden
        /// </summary>
        public List<int> LocatieIdFilter { get; set; }

        public GebouwenBeheerSettings()
        {
            LocatieIdFilter = new List<int>();
            SelectionMode = GebouwenBeheerSelectionModeType.selectall;
        }

        public void AddLocationsToFilter(List<int> locationIds)
        {
            if (locationIds.Count == 0)
            {
                //Wanneer locatiefilter wordt geactiveerd, maar de gebruiker geen locaties heeft, niets tonen !
                LocatieIdFilter.Add(-1);
            }
            else
                LocatieIdFilter = locationIds;
        }
    }
    public class GebouwenBeheerBLL : INotifyPropertyChanged
    {
        /** FIELDS */
        public event PropertyChangedEventHandler PropertyChanged;

        private ObservableCollection<Location> listAllLocations = new ObservableCollection<Location>();
        private ObservableCollection<Location> listFilteredLocations = new ObservableCollection<Location>();
        private GebouwenBeheerSettings _settings;
        public Dictionary <int,Location> flatList = new Dictionary <int,Location>();
        private bool _isFilterOn = false;
        public ObservableCollection<Location> Locations
        {
            get
            {
                if (_isFilterOn)
                    return listFilteredLocations;
                else return listAllLocations;
            }
        }
        public ObservableCollection<Location> AllActiveLocations
        {
            get
            {
                if (_isFilterOn)
                    return new ObservableCollection<Location>(listFilteredLocations.Where(x => x.Active));
                return new ObservableCollection<Location>(listAllLocations.Where(x => x.Active));
            }
        }

        /** CONSTRUCTOR */
        public GebouwenBeheerBLL(GebouwenBeheerSettings settings)
        {
            this._settings = settings;
            GebouwenBeheerLoader.Load();
            this.flatList = GebouwenBeheerLoader.FlatList;
            this.listAllLocations = GebouwenBeheerLoader.ListLocation;
            CheckFilter(settings);

            foreach (KeyValuePair<int, Location> kvp in this.flatList)
                SetSelectionMode4Item(kvp.Value, settings.SelectionMode);
        }

        /** METHODS */
        private void NotifiyPropertyChanged(string property)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(property));
        }
        private void SetSelectionMode4Item(Location loc, GebouwenBeheerSelectionModeType selectionMode)
        {
            switch (selectionMode)
            {
                case GebouwenBeheerSelectionModeType.selectall:
                    loc.IsFocusable = true;
                    break;
                case GebouwenBeheerSelectionModeType.selectlowestonly:
                    if (loc.Children == null || loc.Children.Count == 0)
                        loc.IsFocusable = true;
                    else
                        loc.IsFocusable = false;
                    break;
                case GebouwenBeheerSelectionModeType.selectnone:
                    loc.IsFocusable = false;
                    break;
               default:
                    loc.IsFocusable = true;
                    break;
            }

        }
        private void CheckFilter(GebouwenBeheerSettings settings)
        {
            _isFilterOn=false;
            if  (settings.LocatieIdFilter.Count()>0)
            {
                _isFilterOn=true;
                foreach (int loc in settings.LocatieIdFilter)
                    listFilteredLocations.Add(FindList(loc,this.listAllLocations));
            }
        }
        private Location FindList(int id,ObservableCollection<Location> hayStack)
        {
            foreach (Location loc in hayStack)
            {
                if (loc.Id == id)
                    return loc;
                Location loc2 = FindList(id, loc.Children);
                if (loc2 != null)
                    return loc2;

            }
            return null;
        }
        public void SelectLocation(int? id)
        {


            Location foundLocation = null;
            foreach (KeyValuePair<int,Location> kvp in flatList)
            {
                if (id != null && kvp.Value.Id == id)
                {
               
                    foundLocation = kvp.Value;
     
                    kvp.Value.IsSelected = true;
    


                }
                else
                {


                    kvp.Value.IsExpanded = false;
                    kvp.Value.IsSelected = false;
                }
            }
            if (foundLocation !=null)
            {

                 ExpandAllParents (foundLocation);

            }

        }
        public void ExpandAllParents (Location loc)
        {

            loc.IsExpanded= true;
            if (loc.Parent != null )
             ExpandAllParents (loc.Parent);

        }
        public void UpdateLocation(Location location, String newName, String newDescription, String newRemark, int? newParentId)
        {
            int? id = GebouwenBeheerDAL.SaveLocation(location.Id, newName, newDescription, newRemark, newParentId);
            if (id != null)
            {
                location.Id = id;
                location.ParentId = newParentId;
                location.Name = newName;
                location.Remark = newRemark;
                location.Description = newDescription;
            }
        }
        public void DeleteLocation(Location location)
        {
            if (location.Id == null || GebouwenBeheerDAL.DeleteLocation(location))
            {
                if (location.Parent == null)
                    this.listAllLocations.Remove(location);
                else
                {
                    location.Parent.Children.Remove(location);
                }
            }
        }
        public void AddChild(Location destination, String newName, String newDescription, String newRemark)
        {
            int? id = GebouwenBeheerDAL.SaveLocation(null, newName, newDescription, newRemark, destination.Id);
            if (id != null)
            {
                Location location = new Location
                {
                    Parent = destination,
                    ParentId = destination.Id,
                    Id = id,
                    Name = newName,
                    Description = newDescription,
                    Remark = newRemark
                };
                destination.Children.Add(location);
                destination.IsExpanded = true;
            }
        }
        public void AddCampus(String newName, String newDescription, String newRemark)
        {
            int? id = GebouwenBeheerDAL.SaveLocation(null, newName, newDescription, newRemark, null);
            if (id != null)
            {
                Location location = new Location();
                location.Parent = null;
                location.ParentId = null;
                location.Id = null;
                location.Name = newName;
                location.Description = newDescription;
                location.Remark = newRemark;
                this.listAllLocations.Add(location);
            }
        }
        public static void ClearStaticCache()
        {
            GebouwenBeheerLoader.ClearStaticCache();
        }
    }
}
