﻿using sharedcode.common;
using sharedcode.CommonObjects;
using sharedcode.DAL;
using sharedcode.WeergaveClasses;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sharedcode.BLL
{
    public class KilometerLeefgroepBewonerReport
    {
        public DateTime DatumVan { get; set; }
        public DateTime DatumTot { get; set; }
        public DateTime? AfsluitDatum { get; set; }
        public User Afsluiter { get; set; }

        public KilometerLeefgroepBewonerReport(DateTime DatumVan,DateTime DatumTot)
        {
            this.DatumTot = DatumTot;
            this.DatumVan = DatumVan;
        }
        public List<KilometerLeefgroepBewonerReportItem> reportItems { get; set; }
        public List<KilometerLeefgroepBewonerShortReportItem> reportItemsShort { get; set; }

        public List<KilometerLeefgroepBewonerShortCsvReportItem> reportItemsCSVShort { get; set; } = new List<KilometerLeefgroepBewonerShortCsvReportItem>();


        public void GenerateReport(List<KilometerItem> items, bool afsluiten,KilometerModeType mode)
        {
            if (afsluiten)
            {
                AfsluitDatum = DateTime.Now;
                Afsluiter = GlobalData.Instance.LoggedOnUser;
            }
            else
                AfsluitDatum = null;

            List<KilometerLeefgroepBewonerReportItem> dic = new List<KilometerLeefgroepBewonerReportItem>();
            KilometerItem idebug = null;
            try
            {
          
                foreach (KilometerItem i in items.Where(x=>x.Kilometers!=null))
                {
                    idebug = i;

         
                    if (mode == KilometerModeType.bewoners && i.Type.Code == "CLIENT" && i.Bewoners.Count > 0)
                    {
                        foreach (bewoner b in i.Bewoners)
                        {

                            if (b.rijksregisternr == "66112918322")
                            {

                            }
                            KilometerLeefgroepBewonerReportItem ni = new KilometerLeefgroepBewonerReportItem
                            {

                                Bedrag = i.Bewoners.Count == 1 ? i.PriceRounded.GetValueOrDefault() / 1 : i.PriceRounded.GetValueOrDefault() / 2,
                                Code = b.code,
                                Bestuurder = i.Bestuurder.Name,
                                Naam = b.AchternaamVoornaam,
                                Datum = i.Datum,
                                Reden = i.Reden,
                                Route = i.Bestemming,
                                Rijksregisternummmer = b.rijksregisternr,
                                Ondersteuningsvorm = b.ondersteuningsvorm
                            };
                            dic.Add(ni);
                        }
                    }
                    else if (mode == KilometerModeType.bewoners && i.Type.Code == "KAMP" && i.Kampen.Count > 0)
                    {
                        foreach (leefgroep b in i.Leefgroepen)
                        {
                            KilometerLeefgroepBewonerReportItem ni = new KilometerLeefgroepBewonerReportItem
                            {
                                Bedrag = i.PriceRounded.Value / i.Kampen.Count,
                                Code = b.code,
                                Bestuurder = i.Bestuurder.Name,
                                Naam = b.naam,
                                Datum = i.Datum,
                                Reden = i.Reden,
                                Route = i.Bestemming
                            };
                            dic.Add(ni);
                        }
                    }
                    else if (mode == KilometerModeType.boekhouding && i.Type.Code == "LFGR" && i.Leefgroepen.Count > 0)
                    {
                        foreach (leefgroep b in i.Leefgroepen)
                        {
                            KilometerLeefgroepBewonerReportItem ni = new KilometerLeefgroepBewonerReportItem
                            {
                                Bedrag = i.PriceRounded.Value / i.Leefgroepen.Count,
                                Code = b.code,
                                Bestuurder = i.Bestuurder.Name,
                                Naam = b.naam,
                                Datum = i.Datum,
                                Reden = i.Reden,
                                Route = i.Bestemming
                            };
                            dic.Add(ni);
                        }
                    }
                }
            }
            catch(Exception ex)
            {


            }

            List<KilometerLeefgroepBewonerReportItem> itemsSorted = dic.OrderBy(o => o.Naam).ToList();
            List<string> listUserKode = new List<string>();
            
            foreach (KilometerLeefgroepBewonerReportItem i in itemsSorted)
            {
                if (listUserKode.Contains(i.NaamCode) == false)
                    listUserKode.Add(i.NaamCode);
            }

            reportItems = itemsSorted;

            //*************************************** CREATE SHORT SUMMARY FROM PREVIOUS RESULT    *****************************************/
            reportItemsShort = new List<KilometerLeefgroepBewonerShortReportItem>();
            foreach (string userCode in listUserKode)
            {
                reportItemsShort.Add(new KilometerLeefgroepBewonerShortReportItem
                {
                    Naam = userCode,
                    Bedrag = dic.Where(o => o.NaamCode == userCode).Sum(x => x.Bedrag),
                    Datum = string.Join(",", dic.Where(o => o.NaamCode == userCode).Select(p => p.Datum.ToString("dd.MM.yyyy")))
                });
            }

            //***************************************   CSV *****************************************/
            foreach (KilometerLeefgroepBewonerReportItem i in itemsSorted)
            {
                //eerste dag van de afsluitmaand
                DateTime dtPrevMonth = DateTime.Now.AddMonths(-1);

                KilometerLeefgroepBewonerShortCsvReportItem csvItem = new KilometerLeefgroepBewonerShortCsvReportItem
                {
                    Naam = i.Naam,
                    Rijksregisternummer = i.Rijksregisternummmer,
                    Ondersteuningsvorms = i.Ondersteuningsvorm,
                    Datum = new DateTime(dtPrevMonth.Year, dtPrevMonth.Month, DateTime.DaysInMonth(dtPrevMonth.Year, dtPrevMonth.Month)),
               
                    Omschrijving = i.Datum, // de dag vd verplaatsing
                    Eenheden = 1,
                    Eenheidsprijs = Math.Round(i.Bedrag, 2, MidpointRounding.AwayFromZero),
                    Totaal = Math.Round(i.Bedrag, 2,MidpointRounding.AwayFromZero)
                };
                reportItemsCSVShort.Add(csvItem);
            }
        }
    }
    public class KilometerPersoneelsdienstReport
    {
        public DateTime DatumVan { get; set; }
        public DateTime DatumTot { get; set; }
        public DateTime? AfsluitDatum { get; set; }
        public User Afsluiter { get; set; }
        public List<KilometerPersoneelsdienstReportItem> reportItems { get;set; }

        public KilometerPersoneelsdienstReport(DateTime DatumVan, DateTime DatumTot)
        {
            this.DatumTot = DatumTot;
            this.DatumVan = DatumVan;
        }
       
        public void GenerateReport(List<KilometerItem> items,bool afsluiten)
        {
            if (afsluiten)
            {
                AfsluitDatum = DateTime.Now;
                Afsluiter = GlobalData.Instance.LoggedOnUser;
            }
            else
                AfsluitDatum = null;

            Dictionary<int, KilometerPersoneelsdienstReportItem> dic = new Dictionary<int, KilometerPersoneelsdienstReportItem>();

            List < KilometerItem > itemsSorted = items.Where(x=>x.Kilometers != null).OrderBy(o => o.Bestuurder.LastName).ToList();

            foreach (KilometerItem i in itemsSorted)
            {
                if (i.Kilometers.HasValue && i.PriceLessOmnium.HasValue && !i.GeslotenPersoneelsdientDatum.HasValue)
                {
                    if (dic.ContainsKey(i.Bestuurder.Id.GetValueOrDefault()))
                    {
                        dic[i.Bestuurder.Id.GetValueOrDefault()].Km += i.Kilometers.GetValueOrDefault();
                        dic[i.Bestuurder.Id.GetValueOrDefault()].Price += i.PriceLessOmnium.GetValueOrDefault();
                    }
                    else
                    {
                        dic.Add(i.Bestuurder.Id.GetValueOrDefault(), new
                        KilometerPersoneelsdienstReportItem(i.Bestuurder.PersNr, i.Bestuurder.LastName + " " + i.Bestuurder.FirstName, i.Omnium, i.Kilometers.GetValueOrDefault(), i.PriceLessOmnium.GetValueOrDefault()));
                    }
                }
            }
            reportItems =  dic.Values.ToList();
        }
    }
    public class KilometerBLLSettings
    {
        public int? FilterGebruikerId { get; set; }
        public DateTime DateFrom { get; set; }
        public DateTime DateTo { get; set; }
        public KilometerModeType Mode { get; set; }
        public string Type { get; set; }
        public int? LeefgroepId { get; set; }
        public int? BewonerId { get; set; }
        /// <summary>
        /// Afgerekend of gefactureerd afhankelijk van de Mode (boekhouding, personeelsdienst of bewoners)
        /// </summary>
        public bool? Closed { get; set; }

        public KilometerBLLSettings(KilometerModeType mode)
        {
            Mode = mode;
        }

        public void PrepareForClose(DateTime dtFrom, DateTime dtTo)
        {
            DateFrom = dtFrom;
            DateTo = dtTo;
            LeefgroepId = null;
            BewonerId = null;
            Closed = false;
        }
    }
    public class KilometerVergoedingBLL
    {
        /** FIELDS */
        private readonly KilometerBLLSettings _settings;
        private static List<KilometersType> _cachedKilometersTypes;
        private static List<KilometersWagen> _cachedKilometersWagens;
        private static List<string> _verplaatsingSuggestions;
        private static List<string> _redenSuggestions;
        public List<KilometerItem> KilometerList { get; set; }
        private List<KilometersType> _kilometersTypes;
        public List<KilometersType> KilometersTypes
        {
            get
            {
                if (_kilometersTypes == null)
                    _kilometersTypes = GetKilometersTypes();
                return _kilometersTypes;
            }
        }
        private List<KilometersWagen> _kilometersWagens;
        public List<KilometersWagen> KilometersWagens
        {
            get
            {
                if (_kilometersWagens == null)
                    _kilometersWagens = GetKilometersWagens();
                return _kilometersWagens;
            }
        }

        /** CONSTRUCTORS */
        public KilometerVergoedingBLL(KilometerBLLSettings settings)
        {
            _settings = settings;
        }

        /** METHODS */
        public KilometerItem FindKilometer(int id)
        {
            return KilometerList.Where(x => x.Id == id).FirstOrDefault();
        }
        public void CloseKilometers()
        {
            KilometerVergoedingDAL.CloseKilometers(_settings);
        }
        public void DeleteKilometers(int id)
        {
            KilometerVergoedingDAL.DeleteKilometers(id);
        }

        public void MarkAsNagekeken(int id)
        {
            KilometerVergoedingDAL.MarkAsNagekeken(id);
        }
        public void MarkAsNietNagekeken(int id)
        {
            KilometerVergoedingDAL.MarkAsNietNagekeken(id);
        }
        public void LoadKilometers()
        {
            KilometerVergoedingDAL.GetKilometers(_settings, out DataTable dtKilometers, out DataTable dtLeefgroepen, out DataTable dtBewoners, out DataTable dtKampen);
            
            KilometerList = new List<KilometerItem>();
            foreach (DataRow dr in dtKilometers.Rows)
            {
                KilometerList.Add(ParseKilometerItem(dr));
            }
            foreach (DataRow dr in dtLeefgroepen.Rows)
            {
                if (FindKilometer((int)dr["kmlfg_km_id"]) is KilometerItem item)
                    item.Leefgroepen.Add(new leefgroep()
                    {
                        id = (int)dr["id"],
                        code = dr["code"].ToString(),
                        naam = dr["naam"].ToString()
                    });
            }
            foreach (DataRow dr in dtBewoners.Rows)
            {
                if (FindKilometer((int)dr["kmbew_km_id"]) is KilometerItem item)
                    item.Bewoners.Add(new bewoner()
                    {
                        id = (int)dr["id"],
                        code = dr["code"].ToString(),
                        voornaam = dr["voornaam"].ToString(),
                        achternaam = dr["achternaam"].ToString(),
                        rijksregisternr = dr["rijksregisternr"].ToString(),
                        ondersteuningsvorm = dr["ondersteuningsvorm"].ToString()
                    });
            }
            foreach (DataRow dr in dtKampen.Rows)
            {
                if (FindKilometer((int)dr["kmkamp_km_id"]) is KilometerItem item)
                    item.Kampen.Add(new kampen()
                    {
                        id = (int)dr["id"],
                        nr = dr["nr"].ToString(),
                        bestemming = dr["bestemming"].ToString()
                    });
            }
        }
        private KilometerItem ParseKilometerItem(DataRow dr)
        {
            //nieuwe werkopdrachtItem maken
            KilometerItem item = new KilometerItem
            {
                Id = (int)dr["km_id"],
                DoNotify = false,
                Omnium = dr["km_omnium"].ToString() == "Y",
                Reden = dr["km_reden"].ToString(),
                Bestemming = dr["km_bestemming"].ToString(),
                Datum = (DateTime)dr["km_datum"],
                Type = new KilometersType
                {
                    Code = dr["kmt_code"].ToString(),
                    Naam = dr["kmt_naam"].ToString()
                },
                Wagen = new KilometersWagen
                {
                    Id = (int)dr["kmw_id"],
                 //   Naam = dr["kmw_naam"].ToString()
                },
                Bestuurder = new User
                {
                    Id = (int)dr["id"],
                    PersNr = dr["personeelscode"].ToString(),
                    FirstName = dr["voornaam"].ToString(),
                    LastName = dr["achternaam"].ToString()
                }
            };
            if (!dr.IsNull("km_kilometers"))
                item.Kilometers = (decimal)dr["km_kilometers"];
            if (!dr.IsNull("km_bedrag_per_km"))
                item.PricePerKilometer = (decimal)dr["km_bedrag_per_km"];
            if (!dr.IsNull("km_bedragomnium_per_km"))
                item.PriceOmniumPerKilometer = (decimal)dr["km_bedragomnium_per_km"];
            if (!dr.IsNull("km_bedrag"))
                item.Price = (decimal)dr["km_bedrag"];
            if (!dr.IsNull("km_bedrag_omnium"))
                item.PriceOmnium = (decimal)dr["km_bedrag_omnium"];
            if (!dr.IsNull("km_closed_persdienst_date"))
            {
                item.GeslotenPersoneelsdientDatum = (DateTime)dr["km_closed_persdienst_date"];
                item.GeslotenPersoneelsdientGebruikerId = (int)dr["km_closed_persdienst_user"];
            }
            if (!dr.IsNull("km_closed_boekh_date"))
            {
                item.GeslotenBoekhoudingDatum = (DateTime)dr["km_closed_boekh_date"];
                item.GeslotenBoekhoudingGebruikerId = (int)dr["km_closed_boekh_user"];
            }
            if (!dr.IsNull("km_closed_bewoners_date"))
            {
                item.GeslotenBewonersDatum = (DateTime)dr["km_closed_bewoners_date"];
                item.GeslotenBewonersGebruikerId = (int)dr["km_closed_bewoners_user"];
            }
            if (!dr.IsNull("km_checkedbybewoners_date"))
            {
                item.IsCheckedByBewonersAdministratie = true;
            }
            item.DoNotify = true;

            return item;
        }
        public static List<string> GetVerplaatsingSuggestions(int userId, int wagenId, String typeCode)
        {

                _verplaatsingSuggestions = KilometerVergoedingDAL.GetVerplaatsingSuggestions(userId, wagenId, typeCode);

            return _verplaatsingSuggestions;
        }
        public static List<string> GetRedenSuggestions(int userId, int wagenId, String typeCode)
        {
   
                _redenSuggestions = KilometerVergoedingDAL.GetRedenSuggestions(userId, wagenId, typeCode);
            return _redenSuggestions;
        } 
        public static List<KilometersType> GetKilometersTypes()
        {
            if (_cachedKilometersTypes == null)
            {
                _cachedKilometersTypes = new List<KilometersType>();
                foreach (DataRow dr in KilometerVergoedingDAL.LoadTypes().Rows)
                {
                    KilometersType item = new KilometersType();
                    if (!dr.IsNull("kmt_code"))
                        item.Code = dr["kmt_code"].ToString();
                    if (!dr.IsNull("kmt_naam"))
                        item.Naam = dr["kmt_naam"].ToString();

                    _cachedKilometersTypes.Add(item);
                }
            }
            return _cachedKilometersTypes;
        }
        public static List<KilometersWagen> GetKilometersWagens()
        {
            if (_cachedKilometersWagens == null)
            {
                _cachedKilometersWagens = new List<KilometersWagen>();
                foreach (DataRow dr in KilometerVergoedingDAL.LoadWagens().Rows)
                {
                    KilometersWagen item = new KilometersWagen();
                    if (dr.IsNull("kmw_id") == false)
                        item.Id = (int)dr["kmw_id"];
                    if (dr.IsNull("LocatieNaam") == false)
                        item.Campus.naam = (String)dr["LocatieNaam"];

                 /*   if (dr.IsNull("kmw_naam") == false)
                        item.Naam = dr["kmw_naam"].ToString();*/
                    if (dr.IsNull("Nummerplaat") == false)
                        item.Nummerplaat = dr["Nummerplaat"].ToString();
                    if (dr.IsNull("Merk") == false)
                        item.Merk = dr["Merk"].ToString();
                    if (dr.IsNull("Kleur") == false)
                        item.Kleur = dr["Kleur"].ToString();
                    if (dr.IsNull("Bouwjaar") == false)
                        item.Bouwjaar = dr["Bouwjaar"].ToString();
 

                    _cachedKilometersWagens.Add(item);
                }
            }

            return _cachedKilometersWagens;
        }
        public void CleanUp()
        {
            KilometerList.Clear();
            KilometerList = null;
        }
    }
}
