﻿
using sharedcode.common;
using sharedcode.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sharedcode.BLL
{
    [Serializable]
    public partial class bewoner: MyNotifyPropertyChanged
    {
        public Byte[] GetFoto()
        {
            if (bewoner_foto == null)
                return null;
            return bewoner_foto.FirstOrDefault()?.foto;
        }
        public void SetFoto(byte[] fotoBytes)
        {
            bewoner_foto foto = new bewoner_foto();
            foto.foto = fotoBytes;
            if (bewoner_foto == null)
                bewoner_foto = new List<bewoner_foto>();
            bewoner_foto.Clear();
            bewoner_foto.Add(foto);
            NotifiyPropertyChanged(nameof(defaultFoto));
        }
        public void RemoveFoto()
        {
            foto = null;
            bewoner_foto.Clear();
            NotifiyPropertyChanged(nameof(defaultFoto));
        }
        public Byte[] defaultFoto
        {
            get { return GetFoto(); }         
        }
        public String Leeftijd
        {
            get
            {
                if (this.geboortedatum.HasValue)
                    return geboortedatum.GetValueOrDefault().ToString("dd/MM/yyyy");
                return String.Empty;
             }
        }

        public String LeefgroepenText
        {
            get
            {
                String text = "";
                if (this.leefgroep != null && this.leefgroep.Count>0)
                {
                    foreach (leefgroep l in this.leefgroep)
                    {
                        if (String.IsNullOrEmpty(text) == false)
                            text = text + ", ";
                        text = text + l.naam;
                    }

                }
                return text;

            }
        }
        public String FormattedGeboorteDatum
        {

            get
            {
                if (geboortedatum.HasValue)
                    return geboortedatum.GetValueOrDefault().ToString("dd.MM.yyyy");
                else return "";
            }
        }
        public string VoornaamAchternaam
        {
            get { return voornaam + " " + achternaam; }
        }
        public string AchternaamVoornaam
        {
            get { return achternaam + " " + voornaam; }
        }
        public String TreeViewItemTitle
        {
            get
            {
                return this.VoornaamAchternaam;
            }
        }
        /// <summary>
        /// Op het moment zitten alle bewoners in 1 leefgroep, maar dat zou kunnen veranderen
        /// </summary>
        public String EersteLeefgroep
        {
            get
            {
                if (leefgroep != null && leefgroep.Count > 0)
                    return leefgroep.ElementAt(0).naam;
                return String.Empty;
            }
        }

        public override string ToString()
        {
            return VoornaamAchternaam;
        }
    }
}
