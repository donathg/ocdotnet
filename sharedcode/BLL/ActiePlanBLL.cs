﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using sharedcode.common;
using sharedcode.DAL;
using sharedcode.WeergaveClasses;

namespace sharedcode.BLL
{
    public class ActieplanFilter
    {
        public int BewonerId { get; set; }
        public String Category { get; set; }
        public bool IsAfgewerkt { get; set; }
    }
    public class ActieplanOpvolgingFilter
    {
        public int ActieplanId { get; set; }
    }
    public static class ActiePlanBLL
    {
        #region actieplan methods
        public static List<ActieplanWeergave> GetAllActieplanWeergaves()
        {
            return ActieplanDAL.SelectAllActieplannen(new ActieplanFilter());
        }
        public static List<ActieplanWeergave> GetAllActieplanWeergavesFromFilter(ActieplanFilter filter)
        {
            return ActieplanDAL.SelectAllActieplannen(filter);
        }
        public static List<ActieplanWeergave> GetAllActieplanWeergavesFromBewoner(bewoner bew)
        {
            return ActieplanDAL.SelectAllActieplannen(new ActieplanFilter() { BewonerId = bew.id });
        }
        public static List<ActieplanWeergave> GetAllAfgewerkteActieplanWeergavesFromBewoner(bewoner bew)
        {
            return ActieplanDAL.SelectAllActieplannen(new ActieplanFilter() { BewonerId = bew.id, IsAfgewerkt = true });
        }
        public static List<ActieplanWeergave> GetAllOpenActieplanWeergavesFromBewoner(bewoner bew)
        {
            return ActieplanDAL.SelectAllActieplannen(new ActieplanFilter() { BewonerId = bew.id, IsAfgewerkt = false });
        }
        public static List<ActieplanWeergave> GetAllActieplanWeergavesFromBewonerAndCategory(bewoner bew, String category)
        {
            return ActieplanDAL.SelectAllActieplannen(new ActieplanFilter() { BewonerId = bew.id, Category = category });
        }
        public static List<ActieplanWeergave> GetAllAfgewerkteActieplanWeergavesFromBewonerAndCategory(bewoner bew, String category)
        {
            return ActieplanDAL.SelectAllActieplannen(new ActieplanFilter() { BewonerId = bew.id, Category = category, IsAfgewerkt = true });
        }
        public static List<ActieplanWeergave> GetAllOpenActieplanWeergavesFromBewonerAndCategory(bewoner bew, String category)
        {
            return ActieplanDAL.SelectAllActieplannen(new ActieplanFilter() { BewonerId = bew.id, Category = category, IsAfgewerkt = false });
        }
        public static List<ActieplanWeergave> GetAllActieplanWeergavesFromCategory(String category)
        {
            return ActieplanDAL.SelectAllActieplannen(new ActieplanFilter() { Category = category });
        }
        public static List<ActieplanWeergave> GetAllAfgewerkteActieplanWeergavesFromCategory(String category)
        {
            return ActieplanDAL.SelectAllActieplannen(new ActieplanFilter() { Category = category, IsAfgewerkt = true });
        }
        public static List<ActieplanWeergave> GetAllOpenActieplanWeergavesFromCategory(String category)
        {
            return ActieplanDAL.SelectAllActieplannen(new ActieplanFilter() { Category = category, IsAfgewerkt = false });
        }
        public static ActieplanWeergave GetActieplanFromId(int id)
        {
            return ActieplanDAL.SelectActieplanFromId(id);
        }
        public static void AddActieplan(ActieplanWeergave ap)
        {
            ActieplanDAL.InsertActieplan(ap);
        }
        public static void ChangeActieplan(ActieplanWeergave ap)
        {
            ActieplanDAL.UpdateActieplan(ap);
        }
        public static void CloseActieplan(ActieplanWeergave ap)
        {
            ActieplanDAL.CloseActieplan(ap);
        }
        public static void DeleteActieplan(ActieplanWeergave ap)
        {
            ActieplanDAL.DeleteActieplan(ap);
        }
        #endregion
        #region actieplanopvolging methods
        public static List<ActieplanOpvolgingWeergave> GetAllActieplanOpvolgingen()
        {
            return ActieplanDAL.SelectAllActieplanOpvolgingen(new ActieplanOpvolgingFilter());
        }
        public static List<ActieplanOpvolgingWeergave> GetAllActieplanOpvolgingenFromActieplan(ActieplanWeergave ap)
        {
            return ActieplanDAL.SelectAllActieplanOpvolgingen(new ActieplanOpvolgingFilter() { ActieplanId = ap.id });
        }
        public static ActieplanOpvolgingWeergave GetActieplanOpvolgingFromId(int id)
        {
            return ActieplanDAL.SelectActieplanOpvolgingFromId(id);
        }
        public static void AddActieplanOpvolging(ActieplanOpvolgingWeergave opvolging)
        {
            ActieplanDAL.InsertActieplanOpvolging(opvolging);
        }
        public static void ChangeActieplanOpvolging(ActieplanOpvolgingWeergave opvolging)
        {
            ActieplanDAL.UpdateActieplanOpvolging(opvolging);
        }
        public static void DeleteActieplanOpvolging(ActieplanOpvolgingWeergave opvolging)
        {
            ActieplanDAL.DeleteActieplanOpvolging(opvolging);
        }
        #endregion
        #region actieplantypes methods
        public static List<actieplantypes> GetAllActieplantypes()
        {
            return ActieplanDAL.SelectAllActieplanTypes(null);
        }
        public static List<actieplantypes> GetActiveAllActieplantypes()
        {
            return ActieplanDAL.SelectAllActieplanTypes(true);
        }
        public static List<actieplantypes> GetInactiveAllActieplantypes()
        {
            return ActieplanDAL.SelectAllActieplanTypes(false);
        }
        public static actieplantypes GetActieplantypeFromId(int id)
        {
            return ActieplanDAL.SelectActieplanTypeFromId(id);
        }
        #endregion
    }
}
