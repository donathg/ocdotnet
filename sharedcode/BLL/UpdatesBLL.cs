﻿using sharedcode.common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;

namespace sharedcode.BLL
{
    public partial class maintenance_message
    {
        public string messageBodyFormatted
        {
            get
            {  
                   return  this.messageBody.Replace("{TIME}", this.timeShutDown.ToString("HH:mm"));
            }
        }
    }

    public class UpdatesBLL
    {
        public static maintenance_message GetMaintenanceMessage()
        {
            using (Entities entity = new Entities(GlobalData.Instance.Entity))
            {
                return entity.maintenance_message.Where(x => x.actif == true).FirstOrDefault();
            }
        }
    }

    public class GlobalShutDownTimer : IDisposable
    {
        /** FIELDS */
        private DispatcherTimer timer;
        public bool ShutDownInProgress { get; set; }
        public DateTime? TimeUpdateDetected { get; set; }
        private int secondsBetweenEachCheck = 450; //7.5 Minuten
        private maintenance_message _maintenanceMessage;
        public maintenance_message MaintenanceMessage
        {
            get
            {
                return _maintenanceMessage;
            }
            set
            {
                if (value != null)
                {
                    _maintenanceMessage = value;
                    IsUpdateAvailble = true;
                }
            }
        }
        public bool IsUpdateAvailble { get; private set; }

        /** EVENTS */
        public delegate void GlobalShutDownTimerUpdateAvailableHandler(bool isUpdateAvailable, maintenance_message updateManagerInfo, bool immidiateShutdown);
        public event GlobalShutDownTimerUpdateAvailableHandler globalShutDownTimerUpdateAvailable;

        private void DoTick(object sender, EventArgs e)
        {
            Check();
        }

        /** METHODS */
        public void Dispose()
        {
            StopTimer();
        }
        public void ShutDownApplicationHardWay(int inXSeconds)
        {
            Task.Factory.StartNew(() => Thread.Sleep(inXSeconds * 1000)).ContinueWith(
                (t) => {
                    ShutDownInProgress = true;
                    Environment.Exit(0);
                }, TaskScheduler.FromCurrentSynchronizationContext());
        }
        public void StartTimer(out bool immidiateShutdown)
        {
            immidiateShutdown = false;
            StopTimer();
            if (!CheckAndGetUpdates() && IsUpdateAvailble)
            {
                MessageBox.Show("Er is momenteel een updatemessage actief, maar jij mag verder werken.");
                IsUpdateAvailble = false;
            }
            else if (IsUpdateAvailble)
            {
                immidiateShutdown = DateTime.Now > MaintenanceMessage.timeShutDown.Subtract(new TimeSpan(0, 5, 0));

                if (!immidiateShutdown)
                    globalShutDownTimerUpdateAvailable?.Invoke(IsUpdateAvailble, MaintenanceMessage, immidiateShutdown);
            }
            timer = new DispatcherTimer();
            timer.Tick += DoTick;
            timer.Interval = new TimeSpan(0, 0, secondsBetweenEachCheck);
            timer.Start();
        }
        private void StopTimer()
        {
            if (timer != null)
            {
                timer.Stop();
                timer.Tick -= DoTick;
                timer = null;
            }
        }        
        public void Check()
        {
            if (CheckAndGetUpdates())
            {
                if (globalShutDownTimerUpdateAvailable != null && IsUpdateAvailble)
                {
                    bool immidiateShutdown = DateTime.Now >= MaintenanceMessage.timeShutDown.Subtract(new TimeSpan(0, 0, 0));
                    globalShutDownTimerUpdateAvailable(IsUpdateAvailble, MaintenanceMessage, immidiateShutdown);
                }
            }
        }
        private bool CheckAndGetUpdates()
        {
            MaintenanceMessage = UpdatesBLL.GetMaintenanceMessage();

            if (Environment.MachineName == "W118-CF0288" || Environment.MachineName == "W118-CF0206")
                return false;
            
            if (IsUpdateAvailble && TimeUpdateDetected == null)
                TimeUpdateDetected = DateTime.Now;

            return IsUpdateAvailble;
        }
    }
}
