﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using sharedcode.common;
using sharedcode.DAL;

namespace sharedcode.BLL
{

    public class myToelating: INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public int Id { get; set; }
        public int? TypeId { get; set; }
        public int BewonerId { get; set; }
        private String _title;
        private String _category;
        private String _header;
        private String _remark;
        public bool IsCustom { get; set; }
        public bool YesNo { get; set; }

        /// <summary>
        /// Elke record bevat categorie, maar wanneer deze dezelfde is als voorganger mag deze niet getoond worden.
        /// </summary>
        public string CategoryToDisplay { get; set; }
        public string Category
        {
            get { return _category; }
            set
            {
                _category = value;
                NotifiyPropertyChanged("Category");
            }
        }

        public string Title
        {
            get { return _title; }
            set
            {
                _title = value;
                NotifiyPropertyChanged("Title");
            }
        }
         public string Header
        {
            get { return _header; }
            set
            {
                _header = value;
                NotifiyPropertyChanged("Header");
            }
        }
        public string Remark
        {
            get { return _remark; }
            set
            {
                _remark = value;
                NotifiyPropertyChanged("Remark");

            }
        }

        private void NotifiyPropertyChanged(string property)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(property));
            }
        }
    }

    public class ToelatingenBLL
    {
        public int BewonerId { get; set; }
        public List<myToelating> Toelatingen { get; set; }

        public ToelatingenBLL(int bewonerId)
        {
            BewonerId = bewonerId;
            this.Toelatingen = this.GetToelatingen(bewonerId);
        }

        public List<myToelating> GetToelatingen(int bewonerId)
        {
            List<myToelating> results = new List<myToelating>();
            List<toelatingen> toelatingen = GetToelatingenDb(bewonerId);
            List<toelatingen_types> toelatingen_types = GetToelatingenTypesDb(bewonerId);

            foreach (toelatingen_types type in toelatingen_types)
            {
                toelatingen toelating = toelatingen.Where(x => x.typeid == type.id).FirstOrDefault();
                if (toelating == null)
                {
                    toelating = new toelatingen();
                    toelating.customToelating = null;
                    toelating.typeid = type.id;
                    toelating.yesno = false;
                    toelating.bewonerid = this.BewonerId;
                   

                }
                results.Add(ConvertToelating(type, toelating));

            }
            foreach (toelatingen t in toelatingen.Where(x=>x.typeid==null))
            {
                results.Add(ConvertToelating(null, t));
            }
            return results;
        }
        private myToelating ConvertToelating(toelatingen_types type, toelatingen toelating)
        {
            myToelating t = new myToelating();
            if (type !=null)
             t.TypeId =  type.id;
            t.BewonerId = this.BewonerId;
            t.Title = type == null ? toelating.customToelating : type.title;
            t.IsCustom = type == null;
            t.YesNo = toelating.yesno;
            t.Id = toelating.id;
            t.Remark = toelating.extrainfo;
            if (type != null)
            {
                t.Header = type.header;
                t.Category = type.category;
                
            }
            else
            {
                t.Category = "Cliëntspecifieke toelatingen";

            }
           
            return t;
        }
        public myToelating AddCustomToelating(String text)
        {
            toelatingen t = new toelatingen();
            t.typeid = null;
            t.bewonerid = this.BewonerId;
            t.customToelating = text;
            t.modificationdate = DateTime.Now;
            t.yesno = false;
            t.modifier = GlobalData.Instance.LoggedOnUser.Id.GetValueOrDefault();
            using (Entities entity = new Entities(sharedcode.common.GlobalData.Instance.Entity))
            {
                entity.toelatingen.Add(t);
                entity.SaveChanges();
                return ConvertToelating(null, t);
            }
        }
        public myToelating SaveCustomToelating(myToelating toelating)
        {
            using (Entities entity = new Entities(sharedcode.common.GlobalData.Instance.Entity))
            {
                toelatingen t = entity.toelatingen.Where(x => x.id == toelating.Id).FirstOrDefault();
                if (t == null)
                {
                    t = new toelatingen();
                 
                    entity.toelatingen.Add(t);
                }
                t.bewonerid = this.BewonerId;
                t.modifier = GlobalData.Instance.LoggedOnUser.Id.GetValueOrDefault();
                t.modificationdate = DateTime.Now;
               
                if (toelating.IsCustom)
                {
                    t.customToelating = toelating.Title;
                    t.typeid = null;
                   
                }
                else
                {
                    t.customToelating = String.Empty;
                    t.typeid = toelating.TypeId;
                }
                t.extrainfo = toelating.Remark;
                t.yesno = toelating.YesNo;
               
                entity.SaveChanges();
                return toelating;
            }
        }
        public void DeleteToelating(myToelating toelating)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                string sql = "DELETE FROM toelatingen WHERE id = @id";
                List<DbParam> paramList = new List<DbParam>()
                {
                    new DbParam() { Name = "@id", Value = toelating.Id, DbType = DbTypes.db_integer }
                };
                DbCommand comm = new DbCommand(DbCommandType.crud, sql, paramList.ToArray());

                try
                {
                    db.Excecute(comm);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
        private static toelatingen GetToelating(int id)
        {
            using (Entities entity = new Entities(sharedcode.common.GlobalData.Instance.Entity))
            {
                return entity.toelatingen.Where(x => x.id == id).FirstOrDefault();
            }
        }
        private static List<toelatingen> GetToelatingenDb(int bewonerId)
        {
            using (Entities entity = new Entities(sharedcode.common.GlobalData.Instance.Entity))
            {
                return entity.toelatingen.Where(x=>x.bewonerid== bewonerId).ToList();
            }
        }
        private static List<toelatingen_types> GetToelatingenTypesDb(int bewonerId)
        {
            using (Entities entity = new Entities(sharedcode.common.GlobalData.Instance.Entity))
            {
                return entity.toelatingen_types.Where(x => x.actif==true).ToList();
            }
        }
    }
}
