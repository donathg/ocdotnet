﻿using sharedcode.common;
using sharedcode.DAL;
using sharedcode.WeergaveClasses;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sharedcode.BLL
{
    /***** NEW VERSION *****/
    public class OvereenkomstenBLL
    {
        #region Overeenkomst_type methods
        public List<Overeenkomst_type> GetAllOvereenkomstTypes()
        {
            return OvereenkomstenDAL.SelectAllOvereenkomstTypes();
        }
        public Overeenkomst_type GetOvereenkomstTypeFromId(int id)
        {
            return OvereenkomstenDAL.SelectOvereenkomstTypeFromId(id);
        }
        public void AddOvereenkomstType(Overeenkomst_type type)
        {
            OvereenkomstenDAL.InsertOvereenkomstType(type);
        }
        public void ChangeOvereenkomstType(Overeenkomst_type type)
        {
            OvereenkomstenDAL.UpdateOvereenkomstType(type);
        }
        public void DeleteOvereenkomstType(Overeenkomst_type type)
        {
            OvereenkomstenDAL.DeleteOvereenkomstType(type);
        }
        #endregion
        #region Overeenkomst_status methods
        public List<Overeenkomst_status> GetAllOvereenkomstStatussen()
        {
            return OvereenkomstenDAL.SelectAllOvereenkomstStatussen();
        }
        public Overeenkomst_status GetOvereenkomstStatusFromId(int id)
        {
            return OvereenkomstenDAL.SelectOvereenkomstStatusFromId(id);
        }
        public Overeenkomst_status GetStartingStatusForOvereenkomst()
        {
            return OvereenkomstenDAL.SelectLowestPriorityStatus();
        }
        public void AddOvereenkomstType(Overeenkomst_status status)
        {
            OvereenkomstenDAL.InsertOvereenkomstStatus(status);
        }
        public void ChangeOvereenkomstType(Overeenkomst_status status)
        {
            OvereenkomstenDAL.UpdateOvereenkomstStatus(status);
        }
        public void DeleteOvereenkomstType(Overeenkomst_status status)
        {
            OvereenkomstenDAL.DeleteOvereenkomstStatus(status);
        }
        #endregion
        #region Overeenkomst methods
        public List<OvereenkomstWeergave> GetAllOvereenkomsten(OvereenkomstenSettings settings)
        {
            return OvereenkomstenDAL.GetAllOvereenkomsten(settings);
        }
        public OvereenkomstWeergave GetOvereenkomstFromId(int overeenkomstId)
        {
            return OvereenkomstenDAL.GetOvereenkomstFromId(overeenkomstId);
        }
        public void AddOvereenkomst(OvereenkomstWeergave overeenkomst)
        {
            OvereenkomstenDAL.InsertOvereenkomst(overeenkomst);
        }
        public void ChangeStatus(OvereenkomstWeergave overeenkomst)
        {
            OvereenkomstenDAL.UpdateStatus(overeenkomst);
        }
        public void RemoveOvereenkomst(OvereenkomstWeergave overeenkomst)
        {
            if (overeenkomst != null)
                OvereenkomstenDAL.DeleteOvereenkomst(overeenkomst);
            else throw new Exception("Er is geen overeenkomst geselecteerd");
        }
        #endregion

        //  EMAIL VERZENDEN BIJ NIET GETEKENDE OVEREENKOMST 

        //            if (status == "NG")
        //            { 
        //                if (!String.IsNullOrWhiteSpace(GlobalData.Instance.LoggedOnUser.Email))
        //                {
        //                    var bewonerInfo = entity.bewoner.Where(x => x.id == bewonerId).FirstOrDefault();
        //                    String body = String.Format("{0}\n{1}\n\n\n\n\n(Dit is een automatisch bericht, antwoorden op CF_no-reply@fracarita.org worden niet gelezen)", overeenkomst, bewonerInfo.AchternaamVoornaam);
        //                    UserBLL.SendEmail(entity, GlobalData.Instance.LoggedOnUser.Email, "niet getekende overeenkomst", body);
        //                }
        //            }
    }

    public class OvereenkomstenSettings
    {
        public int? BewonerId;
        public Overeenkomst_type Type;
        public List<Overeenkomst_status> Statussen;
    }
}
