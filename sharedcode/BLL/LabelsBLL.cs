﻿using sharedcode.common;
using sharedcode.CommonObjects;
using sharedcode.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using sharedcode.WeergaveClasses;

namespace sharedcode.BLL
{
    public interface ILabelProvider
    {
        List<labels> Load();
    }
    public class LabelProviderDataBase : ILabelProvider
    {
        List<labels> labels=null;

        public List<labels> Load()
        {
            if (labels==null)
                labels =  LabelsDAL.GetAllLabels();
            return labels;
        }
        public List<labels> Load(OvereenkomstWeergave overeenkomst)
        {
            if (labels == null)
            {
                if (overeenkomst.TypeText.Contains("Persoonlijke Handleiding"))
                    labels = LabelsDAL.GetAllSubLabelsFromLabelid(54);
                else
                    labels = LabelsDAL.GetAllSubLabelsFromLabelid(34);
            }
            return labels;
        }
    }
    public class LabelForUserProviderDataBase : ILabelProvider
    {
        List<labels> labels = null;
        public User User { get; set; }

        public LabelForUserProviderDataBase(User user)
        {
            this.User = user;
        }

        public List<labels> Load()
        {
            if (labels == null)
                labels = LabelsDAL.GetUserLabels(User.Id.GetValueOrDefault());
            return labels;
        }
     
    }
    public class LabelProviderEmpty : ILabelProvider
    {

        public List<labels> labels { get; set; }

        public List<labels> Load()
        {
            if (labels == null)
                labels = LabelsDAL.GetAllLabels();
            return labels;
        }
        public void InjectLabels(labels labels)
        {
            if (this.labels == null)
                this.labels = new List<labels>();
            this.labels.Add(labels);

        }
        public void InjectLabels(List<labels> labels)
        {
            foreach (labels l in labels)
                InjectLabels(l);
        }
    }

    public class LabelManager
    {
        /** FIELDS */
        private List<labels> _labels;
        public ILabelProvider LabelProvider { get; }

        /** CONSTRUCTORS */
        public LabelManager(ILabelProvider provider)
        {
            this.LabelProvider = provider;
            _labels = provider.Load();
        }

        /** METHODS */
        public bool HasLabel(int id)
        {
            return this._labels.Any(x => x.id == id);
        }
        public void ClearLabels()
        {
            _labels.Clear();
        }
        public List<labels> GetAllLabels()
        {
            return _labels;
        }
        public List<labels> GetAllActiveLabels()
        {
            return _labels.Where(x => x.active == true).ToList();
        }
        public List<labels> GetAllLabels(List<labels> filter)
        {
            List<int> labelIdsFilter = filter.Select(x => x.id).ToList();
            return _labels.Where(x=> labelIdsFilter.Contains(x.id)).ToList();
        }
        public labels GetLabelByIdFromSavedList(int id)
        {
            return _labels.Where(x => x.id == id).SingleOrDefault();
        }
        public bool HasAccessToLabel(int labelId)
        {
            return _labels.Any(x => x.id == labelId);
        }

        public static labels GetLabelFromOvereenkomst(OvereenkomstWeergave selectedOvereenkomst)
        {
            return new LabelManager(new LabelProviderDataBase())._labels.Find(x => x.name.ToLower() == selectedOvereenkomst.TypeText.ToLower());
        }
        public static labels GetLabelById(int id)
        {
            return new LabelManager(new LabelProviderDataBase())._labels.Find(x => x.id == id);
        }
        public static labels GetLabelByIdFromUserLabels(int id)
        {
            return new LabelManager(new LabelProviderDataBase())._labels.Find(x => x.id == id);
        }
        public static List<labels> GetChildLables(int labelId)
        {
            List<labels> childList = new List<labels>();
            foreach (labels l in new LabelManager(new LabelProviderDataBase())._labels)
            {
                if (l.parentid == labelId && l.active == true)
                    childList.Add(l);
            }
            return childList;
        }
        public static List<labels> GetAllLabelFromDagboek(dagboek dagBoek)
        {
            List<labels> labelList = new List<labels>();
            using (Entities entity = new Entities(GlobalData.Instance.Entity))
            {
                foreach (dagboek_labels dagboekLabel in entity.dagboek_labels.Where(x => x.dagboekid == dagBoek.id))
                {
                    labelList.Add(entity.labels.Where(x => x.id == dagboekLabel.labelid).SingleOrDefault());
                }
            }
            return labelList;
        }
        public static List<labels> GetAllLabelFromBijlage(FileItem fileItem)
        {
            List<labels> labelList = new List<labels>();
            using (Entities entity = new Entities(GlobalData.Instance.Entity))
            {
                foreach (cd_files_labels bijlageLabel in entity.cd_files_labels.Where(x => x.fileid == fileItem.Id))
                {
                    labelList.Add(entity.labels.Where(x => x.id == bijlageLabel.labelid).SingleOrDefault());
                }
            }
            return labelList;
        }
    }

    public interface ILabelConnectionProvider
    {
        String TableName { get; }
        String ForeinKeyName { get; }
    }

    public interface ILabelConnectionDataProvider  
    {
        List<labels> GetSelectedLabels(int id);
        List<labels> GetAllExistingLabels();
        List<labels> GetAllPossibleLabelsForUser();

        void SaveLabels(Entities entity, int foreignKeyId, List<labels> labels);
    }

    public class LabelConnectionProviderDagboek : ILabelConnectionDataProvider
    {
        List<labels> allLabels = null;
        List<labels> allUserLabels = null;
        User user = null;

        public LabelConnectionProviderDagboek(User user)
        {
            this.user = user;
            LabelProviderDataBase provider = new LabelProviderDataBase();
            List<labels> labelList = provider.Load();
            allLabels = labelList.Where(x => x.dagboek == true).ToList();

            LabelForUserProviderDataBase provider2 = new LabelForUserProviderDataBase(user);
            List<labels> labelList2 = provider.Load();
            allUserLabels = labelList2.Where(x => x.dagboek == true).ToList();
        }
        public LabelConnectionProviderDagboek(User user, List<labels> allLabels,List<labels> allUserLabels)
        {
            this.user = user;
            this.allLabels = allLabels.Where(x => x.dagboek == true).ToList();
            this.allUserLabels = allUserLabels.Where(x => x.dagboek == true).ToList();
        }


        /// <summary>
        /// Alle Labels die bestaan
        /// </summary>
        /// <returns></returns>
        public List<labels> GetAllExistingLabels()
        {
            return allLabels;
        }
        /// <summary>
        /// Alle Labels waartoe de gebruiker rechten heeft
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public List<labels> GetAllPossibleLabelsForUser()
        {
            return allUserLabels;
        }

        /// <summary>
        /// Alle labels die aan het dagboek zijn gekoppeld
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public List<labels> GetSelectedLabels(int id)
        {
            using (Entities entity = new Entities(GlobalData.Instance.Entity))
            {
                List<labels> resultList = new List<labels>();
                var dls = entity.dagboek_labels.Where(x => x.dagboekid == id);
                foreach (dagboek_labels dl in dls)
                {
                    resultList.Add(dl.labels);
                }
               
                return resultList;
            }

        }
        public void SaveLabels(Entities entity, int foreignKeyId, List<labels> labels)
        {
            int userId = GlobalData.Instance.LoggedOnUser.Id.Value;
            List<dagboek_labels> previousLabelsDagboek = entity.dagboek_labels
                                                            .Where(x => x.dagboekid == foreignKeyId)
                                                            .ToList();
            List<dagboek_labels> previousLabelsDagboekAndUser = new List<dagboek_labels>();

            foreach (dagboek_labels dagboekLabel in previousLabelsDagboek)
            {
                if (GlobalData.Instance.LoggedOnUser.LabelManager.GetAllActiveLabels().Any(x => x.id == dagboekLabel.labelid))
                {
                    previousLabelsDagboekAndUser.Add(dagboekLabel);
                }
            }

            foreach (labels label in labels)
            {
                if (!previousLabelsDagboekAndUser.Any(x => x.labelid == label.id))
                {
                    entity.dagboek_labels.Add(new dagboek_labels()
                    {
                        dagboekid = foreignKeyId,
                        labelid = label.id,
                        modificationdate = DateTime.Now,
                        modifier = userId
                    });
                }
                else
                {
                    previousLabelsDagboekAndUser.RemoveAll(x => x.labelid == label.id);
                }
            }

            foreach (dagboek_labels dagboekLabel in previousLabelsDagboekAndUser)
            {
                entity.dagboek_labels.Remove(dagboekLabel);
            }
        }
    }
    public class LabelConnectionProviderContactverslag : ILabelConnectionDataProvider
    {
        List<labels> allLabels = null;
        List<labels> allUserLabels = null;
        User user = null;

        public LabelConnectionProviderContactverslag(User user)
        {
            this.user = user;
            LabelProviderDataBase provider = new LabelProviderDataBase();
            List<labels> labelList = provider.Load();
            allLabels = labelList.Where(x => x.contactverslagen == true).ToList();

            LabelForUserProviderDataBase provider2 = new LabelForUserProviderDataBase(user);
            List<labels> labelList2 = provider.Load();
            allUserLabels = labelList2.Where(x => x.contactverslagen == true).ToList();
        }
        public LabelConnectionProviderContactverslag(User user, List<labels> allLabels, List<labels> allUserLabels)
        {
            this.user = user;
            this.allLabels = allLabels.Where(x => x.contactverslagen == true).ToList();
            this.allUserLabels = allUserLabels.Where(x => x.contactverslagen == true).ToList();
        }

        public List<labels> GetAllExistingLabels()
        {
            return allLabels;
        }
        public List<labels> GetAllPossibleLabelsForUser()
        {
            return allUserLabels;
        }
        public List<labels> GetSelectedLabels(int id)
        {
            using (Entities entity = new Entities(GlobalData.Instance.Entity))
            {
                List<labels> resultList = new List<labels>();
                var dls = entity.Contactverslag_Labels.Where(x => x.contactverslag_id == id);
                foreach (Contactverslag_Labels dl in dls)
                {
                    resultList.Add(dl.labels);
                }

                return resultList;
            }
        }
        public void SaveLabels(Entities entity, int foreignKeyId, List<labels> labels)
        {
            int userId = GlobalData.Instance.LoggedOnUser.Id.Value;
            List<Contactverslag_Labels> previousLabelsUser = entity.Contactverslag_Labels
                                                            .Include("gebruiker")
                                                            .Where(x => x.gebruiker.id == userId && x.contactverslag_id == foreignKeyId)
                                                            .ToList();
            foreach (labels label in labels)
            {
                if (!previousLabelsUser.Any(x => x.label_id == label.id))
                {
                    entity.Contactverslag_Labels.Add(new Contactverslag_Labels()
                    {
                        contactverslag_id = foreignKeyId,
                        label_id = label.id,
                        modification_date = DateTime.Now,
                        modifier = userId
                    });
                }
                else
                    previousLabelsUser.RemoveAll(x => x.label_id == label.id);
            }

            foreach (Contactverslag_Labels contactverslagLabel in previousLabelsUser)
            {
                entity.Contactverslag_Labels.Remove(contactverslagLabel);
            }
        }
    }
    public class LabelConnectionProviderBijlages : ILabelConnectionDataProvider
    {
        List<labels> allLabels = null;
        List<labels> allUserLabels = null;
        User user = null;

        public LabelConnectionProviderBijlages(User user)
        {
            this.user = user;
            LabelProviderDataBase provider = new LabelProviderDataBase();
            List<labels> labelList = provider.Load();
            allLabels = labelList.Where(x => x.bijlagen == true).ToList();

            LabelForUserProviderDataBase provider2 = new LabelForUserProviderDataBase(user);
            List<labels> labelList2 = provider.Load();
            allUserLabels = labelList2.Where(x => x.bijlagen == true).ToList();
        }
        public LabelConnectionProviderBijlages(User user, List<labels> allLabels, List<labels> allUserLabels)
        {
            this.user = user;
            this.allLabels = allLabels.Where(x => x.bijlagen == true).ToList();
            this.allUserLabels = allUserLabels.Where(x => x.bijlagen == true).ToList();
        }

        /// <summary>
        /// Alle Labels die bestaan
        /// </summary>
        /// <returns></returns>
        public List<labels> GetAllExistingLabels()
        {
            return allLabels;
        }
        /// <summary>
        /// Alle Labels waartoe de gebruiker rechten heeft
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public List<labels> GetAllPossibleLabelsForUser()
        {
            return allUserLabels;
        }
        /// <summary>
        /// Alle labels die aan het dagboek zijn gekoppeld
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public List<labels> GetSelectedLabels(int id)
        {
            using (Entities entity = new Entities(GlobalData.Instance.Entity))
            {
                List<labels> resultList = new List<labels>();
                var dls = entity.cd_files_labels.Where(x => x.fileid == id);
                foreach (cd_files_labels dl in dls)
                {
                    resultList.Add(dl.labels);
                }

                return resultList;
            }

        }
        public void SaveLabels(Entities entity, int foreignKeyId, List<labels> labels)
        {
            List<int> idsUserLabels = this.allUserLabels.Select(x => x.id).ToList();
            //enkel de labels die de user mag zien verwijderen. Het kan zijn dat er nog labels van andere aanhangen
            entity.cd_files_labels.RemoveRange(entity.cd_files_labels.Where(x => x.fileid == foreignKeyId && idsUserLabels.Contains(x.labelid)));
            List<int> labelsToSave = new List<int>();
            foreach (labels l in labels)
            {
                if (!labelsToSave.Contains(l.id))
                    labelsToSave.Add(l.id);
            }
            foreach (int i in labelsToSave)
            {
                cd_files_labels dl = new cd_files_labels
                {
                    fileid = foreignKeyId,
                    labelid = i,
                    modificationdate = DateTime.Now,
                    modifier = GlobalData.Instance.LoggedOnUser.Id.GetValueOrDefault()
                };
                entity.cd_files_labels.Add(dl);
            }
        }
    }
    public class LabelConnectionManager 
    {
        ILabelConnectionDataProvider labelConnectionProvider;
       // ILabelProvider labelProvider;
        List<labels> _allExistingLabels;
        public List<labels> AllExistingLabels { get { return _allExistingLabels; } }
        List<labels> _allUserLabels;
        public List<labels> AllUserLabels { get { return _allUserLabels; } }
        public List<labels> SelectedLabels { get; set; }
        int? foreignKeyId;

        public LabelConnectionManager(User user,ILabelConnectionDataProvider labelConnectionProvider)
        {
        //    this.labelProvider = labelProvider;
            this.labelConnectionProvider = labelConnectionProvider;
            _allExistingLabels = labelConnectionProvider.GetAllExistingLabels();
            _allUserLabels = labelConnectionProvider.GetAllPossibleLabelsForUser();

        }

        public void SaveSelectedLabels(Entities entity)
        {
            if (foreignKeyId.HasValue && SelectedLabels != null && SelectedLabels.Count > 0)
            {
                if (entity == null)
                    entity = new Entities(GlobalData.Instance.Entity);
                labelConnectionProvider.SaveLabels(entity, foreignKeyId.GetValueOrDefault(), SelectedLabels);
                entity.SaveChanges();
            }
        }
        public void SaveSelectedLabels(Entities entity, int foreignKeyId)
        {
            if (entity == null)
                entity = new Entities(GlobalData.Instance.Entity);
            if (SelectedLabels != null && SelectedLabels.Count > 0)
                labelConnectionProvider.SaveLabels(entity, foreignKeyId, SelectedLabels);
            entity.SaveChanges();
        }

        public void LoadSelectedLabels (int foreignKeyId)
        {
            this.foreignKeyId = foreignKeyId;
            SelectedLabels = labelConnectionProvider.GetSelectedLabels(foreignKeyId);
        }
    }
}
 
 
