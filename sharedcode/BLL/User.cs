﻿using sharedcode.DAL;
using sharedcode.BLL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using sharedcode.common;
using System.Collections;
using System.Data;

namespace sharedcode.CommonObjects
{
    public enum AccessType
    {
        none=0,
        access=1
    }
    public class AccessCode
    {
        public string Groep { get; set; }
        public bool FromOrbis { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public string Module { get; set; }
        public module ModuleObject { get; set; }
        public AccessType AccesType { get; set; }

        public override string ToString()
        {
            return AccesType.ToString() + " " + Code + " - " + Description + " - " + ModuleObject.descr;
        }
    }
    [Serializable]
    public class User
    {
        public int? Id { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string Name
        {
            get { return FirstName + " " + LastName; }
        }
        public string PersNr { get; set; }
        /// <summary>
        /// komt uit Orbis, veld grp. Kan een subgroep bevatten.
        /// voorbeeld Technische Dienst : schilder, bouw, klus,...
        /// </summary>
        public string SubGroep { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }

        public override string ToString()
        {
            if (!string.IsNullOrWhiteSpace(Name))
                return Name;
            else
                return string.Empty;
        }
        public User Clone()
        {
            return new User
            {
                Id = Id,
                LastName = LastName.Trim(),
                FirstName = FirstName.Trim(),
                SubGroep = SubGroep
            };
        }
    }
    public class LoggedOnUser: User, INotifyPropertyChanged
    {
        /** FIELDS */
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// The loggedon user is supervisor for this diensten (IT, TECHD,....)
        /// </summary>
        public List<String> SupervisorDiensten { get; set; }
        public List<werkopdracht_diensten>  WerkaanvraagDiensten { get; set; }

        public werkopdracht_diensten GetDienstByCode(String dienstCode)
        {
            return WerkaanvraagDiensten.Where(x => x.wodienst_code == dienstCode).FirstOrDefault();
        }

        public List<String> WerkbonnenDiensten { get; set; }


        public LabelManager LabelManager { get; set; }
        public gebruiker_settings Settings { get; set; }
        public application_settings ApplicationSettings { get; set; }
        Dictionary<String, AccessCode> AccesCodes = new Dictionary<string, AccessCode>();
        public Dictionary<string, List<int>> Locations { get; set; }
        private bool _isLoggedOn;
        public bool IsLoggedOn
        {
            get
            {
                return _isLoggedOn;
            }
            set
            {
                _isLoggedOn = value;
                NotifiyPropertyChanged(nameof(IsLoggedOn));
            }
        }

        /** CONSTRUCTORS */
        public LoggedOnUser()
        {
            Locations = new Dictionary<string, List<int>>();
         
            Settings = new gebruiker_settings();
            ApplicationSettings = new application_settings();
        }

        /** METHODS */
        public void NotifiyPropertyChanged(string property)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(property));
        }
    /*    public bool IsPowerUser(String code = "")
        {
            if (IsLoggedOn)
            {
                if (PersNr == "20376" || PersNr == "20513" ||  PersNr == "20378")
                    return true;
                //Supervisor Clientdossier ?
                else if (!String.IsNullOrWhiteSpace(code) && Int32.Parse(code) >= 1000 && Int32.Parse(code) < 2000 && this.GetAccesTypeNoPowerUserCheck("1001") == AccessType.access)
                    return true;
            }
            return false;
        }*/
       
        public List<int> GetLocationsForDienst(String dienst)
        {
            if (Locations == null || !Locations.ContainsKey(dienst))
                return new List<int>();

            return Locations[dienst];
        }
        /// <summary>
        /// Kijkt na af de gebruiker rechten heeft binnen een hoofdgroep. Bijvoorbeel 4(00) voor kalender
        /// </summary>
        /// <param name="firstNum"></param>
        /// <returns></returns>
        public bool HasAccessToGroup(int firstNum, int lastNum)
        {

           
            if (AccesCodes.ContainsKey("1"))
                return  true;


            if (firstNum >= 1000 && firstNum < 2000)//Clientdossier
            {
                if (AccesCodes.ContainsKey("1001"))
                    return true;
            }

            foreach (KeyValuePair<string,AccessCode> kvp in AccesCodes)
            {
                int k = Int32.Parse(kvp.Key.ToString());
                if (k >= firstNum && k<= lastNum && kvp.Value.AccesType != AccessType.none)
                    return true;

            }
            return false;
        }
        public void AssignLabelManager (LabelManager manager)
        {

            this.LabelManager = manager;
         }
        public bool HasAccess(String code)
        {
            return GetAccesType(code) != AccessType.none;
        }
        public AccessType GetAccesType(String code)
        {

            if (String.IsNullOrWhiteSpace(code))
                return AccessType.none;

            int iCode = Int32.Parse(code);



        
             if (AccesCodes.ContainsKey("1"))
                return AccessType.access;
                 

            if (iCode >= 1000 && iCode < 2000)//Clientdossier
            {
                if (AccesCodes.ContainsKey("1001"))
                    return AccessType.access;
            }


            if (AccesCodes.ContainsKey(code))
                return AccesCodes[code].AccesType;
            return AccessType.none;//geen rechten
        }
        
        public void AddLocation (String dienst, int locationId)
        {
            if (!Locations.ContainsKey(dienst))
                Locations.Add(dienst, new List<int>());

            if (!Locations[dienst].Contains(locationId))
                Locations[dienst].Add(locationId);
        }
        public List<String> GetUserFunctieGroepen(bool onlyOrbis=false)
        {
            List<String> groepen = new List<string>(); 

            foreach (KeyValuePair<string, AccessCode> kvp in AccesCodes)
            {
                if (onlyOrbis && kvp.Value.FromOrbis == false)
                    continue;
              
                if (groepen.Contains(kvp.Value.Groep) == false)
                    groepen.Add(kvp.Value.Groep);

            }
            return groepen;
        }
        public bool IsUserInFunctieGroep(String groep)
        {
            foreach (KeyValuePair<string, AccessCode> kvp in AccesCodes)
            {
                if (kvp.Value.Groep==groep)
                    return true;

            }
            return false;

        }
        public void AddAccesType(DataRow row)
        {
            if (row["func_code"].ToString() is string code && !AccesCodes.ContainsKey(code))
            {
                AccessCode accessCode = new AccessCode()
                {
                    FromOrbis = row["groep_orbis"].ToString() == "Y",
                    Groep = row["groepgeb_groep_code"].ToString(),
                    Code = code,
                    Description = row["func_naam"].ToString(),
                    Module = row["func_cat"].ToString(),
                    AccesType = row["groepfunc_recht"].ToString() == "schrijven" ? AccessType.access : AccessType.none
                };
                accessCode.ModuleObject = new module()
                {
                    code = row["code"].ToString(),
                    descr = row["descr"].ToString()
                };

                AccesCodes.Add(code, accessCode);
            }
        }
        public void LogOff()
        {
            if (IsLoggedOn)
            {
                UserDAL.Logoff(GlobalData.Instance.LoggedOnUser.Id.GetValueOrDefault());
                this.IsLoggedOn = false;
                this.AccesCodes.Clear();
                this.Locations.Clear();
                this.Settings = null;
            }
            ProjectMethods.ClearCache();
        }
    }
}
