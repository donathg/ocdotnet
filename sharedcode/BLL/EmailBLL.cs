﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sharedcode.BLL
{
    public class EmailBLL
    {


        public static void SendEmailViaOutlook(List<String> emailAdressen, String subject, String body, List<string> attachementFilename)
        {
            Microsoft.Office.Interop.Outlook.Application oApp = new Microsoft.Office.Interop.Outlook.Application();
            Microsoft.Office.Interop.Outlook.MailItem oMsg = (Microsoft.Office.Interop.Outlook.MailItem)oApp.CreateItem(Microsoft.Office.Interop.Outlook.OlItemType.olMailItem);

            oMsg.Subject = subject;
            oMsg.BodyFormat = Microsoft.Office.Interop.Outlook.OlBodyFormat.olFormatHTML;

            if (emailAdressen != null)
            {
                foreach (String email in emailAdressen)
                    oMsg.To = email + ";";
            }
            oMsg.HTMLBody = body;
            foreach (String f in attachementFilename)
                oMsg.Attachments.Add(Convert.ToString(f), Microsoft.Office.Interop.Outlook.OlAttachmentType.olByValue, Type.Missing, Type.Missing);
            oMsg.Display(false); //In order to displ

        }

    }
}
