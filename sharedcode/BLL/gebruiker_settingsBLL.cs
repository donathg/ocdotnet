﻿using sharedcode.common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sharedcode.BLL
{
    public partial class gebruiker_settingsBLL
    {
        /**
         * comment
         */
        /***** GET EEN GEBRUIKER_SETTINGS *****/
        public static gebruiker_settings GetGebruikerSettings(int gebruikersId)
        {
            using (Entities entity = new Entities(GlobalData.Instance.Entity))
            {
                gebruiker_settings settings = entity.gebruiker_settings.FirstOrDefault(x => x.gebruikerid == gebruikersId);
                if (settings == null)
                    settings = CreateDefaultSettingsForGebruiker(gebruikersId);

                GlobalData.Instance.LoggedOnUser.Settings = settings;
                return settings;
            }
        }
        /***** AANPASSEN BROWSER GEBRUIKER_SETTINGS *****/
        public static void UpdateBrowser(int gebruikersId, String browserPath)
        {
            using (Entities entity = new Entities(GlobalData.Instance.Entity))
            {
                gebruiker_settings dbSetting = entity.gebruiker_settings.FirstOrDefault(x => x.gebruikerid == gebruikersId);
                dbSetting.browserpath = browserPath;

                entity.SaveChanges();
                GlobalData.Instance.LoggedOnUser.Settings = dbSetting;
            }
        }
        /***** AANPASSEN POSITIE TOEVOEGEN BLOK BIJ CLIENTDOSSIER *****/
        public static void UpdateDagboekInvoerPositie(int gebruikersId, String positie)
        {
            if (positie != "Top" && positie != "Bottom")
            {
                throw new Exception("postie onbekend");
            }
            using (Entities entity = new Entities(GlobalData.Instance.Entity))
            {
                gebruiker_settings settings = entity.gebruiker_settings.FirstOrDefault(x => x.gebruikerid == gebruikersId);
                settings.dagboek_invoerpositie = positie;

                entity.SaveChanges();
                GlobalData.Instance.LoggedOnUser.Settings = settings;
            }
        }
        /***** TOEVOEGEN DEFAULT SETTINGS AAN GEBRUIKER_SETTINGS TABEL *****/
        private static gebruiker_settings CreateDefaultSettingsForGebruiker(int gebruikerId)
        {
            gebruiker_settings settings = new gebruiker_settings();
            using (Entities entity = new Entities(GlobalData.Instance.Entity))
            {
                settings.gebruikerid = gebruikerId;
                settings.dagboek_invoerpositie = "Top";

                entity.gebruiker_settings.Add(settings);
                entity.SaveChanges();
            }
            return settings;
        }
        /***** VERWIJDERT ALLE GEBRUIKER SETTINGS VAN dbo.wpflayout *****/
        public static void DeleteAllInputsFromUser(int userId)
        {
            using (Entities entity = new Entities(GlobalData.Instance.Entity))
            {
                foreach (wpflayout item in entity.wpflayout.Where(x => x.lay_geb_id == userId))
                {
                    entity.wpflayout.Remove(item);
                }

                entity.SaveChanges();
            }
        }
    }
}
