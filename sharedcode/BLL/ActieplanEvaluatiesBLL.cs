﻿using sharedcode.common;
using sharedcode.DAL;
using sharedcode.WeergaveClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sharedcode.BLL
{
    public static class ActieplanEvaluatiesBLL
    {
        #region Actieplan_Resultaat methods
        public static List<Actieplan_Resultaat> GetAllActieplanResultaten()
        {
            return ActieplanEvaluatiesDAL.SelectAllActieplanResultaten();
        }
        public static Actieplan_Resultaat GetActieplanResultaatFromId(int id)
        {
            return ActieplanEvaluatiesDAL.SelectActieplanResultaatFromId(id);
        }
        #endregion
        #region Actieplan_Evaluaties methods
        public static ActieplanEvaluatiesWeergave GetEindevaluatieFromActieplanId(int actieplanId)
        {
            return ActieplanEvaluatiesDAL.SelectEindEvaluatieFromActieplanId(actieplanId);
        }
        public static List<ActieplanEvaluatiesWeergave> GetAllEindevaluaties(int bewonerId)
        {
            return ActieplanEvaluatiesDAL.SelectAllEvaluatiesFromBewoner(ActieplanEvaluatieVersies.EINDEVALUATIE, bewonerId);
        }
        public static List<ActieplanEvaluatiesWeergave> GetAllPeriodiekeEvaluaties(int bewonerId)
        {
            return ActieplanEvaluatiesDAL.SelectAllEvaluatiesFromBewoner(ActieplanEvaluatieVersies.PERIODIEKE_EVALUATIE, bewonerId);
        }
        public static ActieplanEvaluatiesWeergave GetActieplanEvaluatieFromId(int id)
        {
            return ActieplanEvaluatiesDAL.SelectActieplanEvaluatieFromId(id);
        }
        public static void AddActieplanEvaluatie(ActieplanEvaluatiesWeergave evaluatie)
        {
            ActieplanEvaluatiesDAL.InsertEvaluatie(evaluatie);
        }
        public static void ModifyActieplanEvaluatie(ActieplanEvaluatiesWeergave evaluatie)
        {
            ActieplanEvaluatiesDAL.UpdateEvaluatie(evaluatie);
        }
        public static void RemoveActieplanEvaluatie(ActieplanEvaluatiesWeergave evaluatie)
        {
            ActieplanEvaluatiesDAL.DeleteEvaluatie(evaluatie);
        }
        #endregion
    }
}
