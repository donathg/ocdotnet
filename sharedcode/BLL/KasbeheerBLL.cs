﻿using sharedcode.common;
using sharedcode.DAL;
using sharedcode.WeergaveClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sharedcode.BLL
{
    public class KasbeheerExportFilter: MyNotifyPropertyChanged
    {
        public DateTime DateFrom { get; set; }
        public DateTime DateTo { get; set; }
        private bool? _isClosed;
        public bool? IsClosed
        {
            get
            {
                return _isClosed;
            }
            set
            {
                _isClosed = value;

                NotifiyPropertyChanged(nameof(IsClosed));
                NotifiyPropertyChanged(nameof(ClosedText));
            }
        }
        public string ClosedText
        {
            get
            {
                if (!IsClosed.HasValue)
                    return "afgesloten en niet afgesloten";
                else if (IsClosed.Value)
                    return "afgesloten";
                else
                    return "niet afgesloten";
            }
        }
        public int LeefgroepId { get; set; }
    }
    public class KasBeheerExportBLL
    {
        public List<KasBeheerExportWeergave> GetAllKasbeheerItemsFromFilter(KasbeheerExportFilter filter)
        {
            return KasbeheerExportDAL.SelectAllKasbeheerItemsFromFilter(filter);
        }
        public List<KasBeheerExportWeergave> GetAllClientKasbeheerItemsFromLeefgroepid(int leefgroepId)
        {
            return KasbeheerExportDAL.SelectAllLastClosedClientKasbeheerItemsFromLeefgroep(leefgroepId);
        }
        public void CloseKasbeheerItems(List<KasBeheerExportWeergave> filteredItems)
        {
            KasbeheerExportDAL.UpdateKasbeheerStateToClosed(filteredItems);
        }
    }
}
