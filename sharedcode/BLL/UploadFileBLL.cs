﻿using sharedcode.common;
using sharedcode.DAL;
using sharedcode.WeergaveClasses;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace sharedcode.BLL
{
    public class BijlagenBLLManager
    {
        /** FIELDS */
        private AbstractBijlagenBLLProvider _provider;

        /** CONSTRUCTOR */
        public BijlagenBLLManager(AbstractBijlagenBLLProvider provider)
        {
            _provider = provider;
        }

        /** METHODS */
        public List<file_cats> GetAllFileCatsFromModule()
        {
            return _provider.GetAllFileCatsFromModule();
        }
        /// <param name="id">necessary</param>
        /// <param name="category">if null not filtered on category</param>
        /// <returns></returns>
        public List<FileItem> GetAllFilesFilteredOnOwnerIdAndCategory(int id, file_cats category)
        {
            return _provider.GetAllFilesFilteredOnOwnerIdAndCategory(id, category);
        }
        public void SaveFileItemDescription(FileItem file)
        {
            _provider.SaveFileItemDescription(file);
        }
        public void DownloadFile(FileItem file, String fileName)
        {
            _provider.DownloadFile(file, fileName);
        }
        public Byte[] GetFileDataFromFileItem(FileItem file)
        {
            return _provider.GetFileDataFromFileItem(file);
        }
        public void UploadSaveBijlageData(SaveBijlageData bijlageData)
        {
            _provider.UploadSaveBijlageData(bijlageData);
        }
        public void UploadFiles(List<SaveBijlageData> fileList)
        {
            _provider.UploadSaveBijlageData(fileList);
        }
        public void DeleteBijlage(FileItem file)
        {
            _provider.DeleteBijlage(file);
        }
        public file_cats GetFileCategoryFromCode(string code)
        {
            return _provider.GetFileCatFromCode(code);
        }
        public static void RemoveTempFolder()
        {
            AbstractBijlagenBLLProvider.DeleteContentsFromTempFolder();
        }
        public void UpdateFileData(FileItem file)
        {
            _provider.UpdateFileData(file);
        }
    }

    public abstract class AbstractBijlagenBLLProvider
    {
        /** FIELDS */
        protected BijlagenDALManager dal;
        protected string module;
        private List<file_cats> _fileCats;

        public bool ShowCategory { get; set; }

        /** METHODS */
        protected void LoadFileCats()
        {
            _fileCats = dal.SelectAllFilecategories();
        }
        protected byte[] GetLocalFileData(string fileName)
        {
            FileInfo fileInfo = new FileInfo(fileName);
            FileStream fileStream = new FileStream(fileInfo.FullName, FileMode.Open, FileAccess.Read);
            BinaryReader binaryReader = new BinaryReader(fileStream);

            byte[] fileData = binaryReader.ReadBytes((int)fileStream.Length);

            binaryReader.Close();
            fileStream.Close();

            return fileData;
        }
        protected void CreateTempFolder()
        {
            String tempFolder = "C:/Temp Clara Fey App/";
            if (!Directory.Exists(tempFolder))
                Directory.CreateDirectory(tempFolder);

            tempFolder += GlobalData.Instance.LoggedOnUser.Id + "/";
            if (!Directory.Exists(tempFolder))
                Directory.CreateDirectory(tempFolder);
        }

        public static void DeleteContentsFromTempFolder()
        {
            if (GlobalData.Instance.LoggedOnUser.Id is int id)
            {
                String MyFolder = "C:/Temp Clara Fey App/" + id;
                if (Directory.Exists(MyFolder))
                {
                    DirectoryInfo di = new DirectoryInfo(MyFolder);
                    foreach (FileInfo file in di.EnumerateFiles()) // delete all files
                        file.Delete();

                    foreach (DirectoryInfo dir in di.EnumerateDirectories()) // delete all directories
                        dir.Delete(true);
                }
            }
        }
        public List<file_cats> GetAllFileCatsFromModule()
        {
            return _fileCats.Where(x => x.module == module).OrderBy(x => x.name).ToList();
        }
        public file_cats GetFileCatFromCode(String code)
        {
            return _fileCats.SingleOrDefault(x => x.code == code);
        }
        public Byte[] GetFileDataFromFileItem(FileItem file)
        {
            return dal.SelectFileDataFromFileItem(file);
        }
        public void DownloadFile(FileItem file, String locatie)
        {
            FileStream objFileStream = new FileStream(locatie, FileMode.Create, FileAccess.Write);
            objFileStream.Write(file.FileData, 0, file.FileData.Length);
            objFileStream.Close();
        }
        public void SaveFileItemDescription(FileItem file)
        {
            dal.UpdateFileDescription(file);
        }
        public void UploadSaveBijlageData(List<SaveBijlageData> bijlageDataList)
        {
            foreach (SaveBijlageData bijlageData in bijlageDataList)
            {
                UploadSaveBijlageData(bijlageData);
            }
        }
        public void UploadSaveBijlageData(SaveBijlageData bijlageData)
        {
            if (bijlageData.BijlageType == BijlageType.internalWord)
                bijlageData.FileName = "interne_notie.dtrtf";
            else if (bijlageData.BijlageType == BijlageType.internalExcel)
                bijlageData.FileName = "intern_excel.dtxls";

            if (bijlageData.FileData == null || bijlageData.FileData.Length == 0)
                bijlageData.FileData = GetLocalFileData(bijlageData.FileName);

            int lastSlashIndex = bijlageData.FileName.LastIndexOf("\\");
            string fullFileName = bijlageData.FileName.Substring(lastSlashIndex + 1);
            int indexPointFullFileName = fullFileName.LastIndexOf('.');
            FileItem item = new FileItem
            {
                Creator = GlobalData.Instance.LoggedOnUser.Id.Value,
                CreationDate = DateTime.Now,
                ObjectId = bijlageData.ObjectId,
                FileName = fullFileName.Substring(0, indexPointFullFileName),
                FileExtension = fullFileName.Substring(indexPointFullFileName + 1),
                FileData = bijlageData.FileData
            };

            if (bijlageData.LabelList != null && bijlageData.LabelList.Count != 0)
                item.Labels = new ObservableCollection<labels>(bijlageData.LabelList);
            if (!String.IsNullOrWhiteSpace(bijlageData.Category.code))
                item.Category = bijlageData.Category;

            dal.InsertFile(item);
        }
        public List<FileItem> GetAllFilesFilteredOnOwnerIdAndCategory(int Id, file_cats category)
        {
            return dal.SelectAllFiles(Id, category);
        }
        public void DeleteBijlage(FileItem file)
        {
            dal.DeleteBijlage(file);
        }
        internal void UpdateFileData(FileItem file)
        {
            dal.UpdateFileData(file);
        }

        // unused methods
        public List<file_cats> GetAllFileCats()
        {
            return _fileCats;
        }
    }
    public class BijlagenBLLProviderPersoneel : AbstractBijlagenBLLProvider
    {
        /** CONSTRUCTOR */
        public BijlagenBLLProviderPersoneel()
        {
            base.module = "PERSONEEL"; //zie database tabel module
            base.dal = new BijlagenDALManager(new BijlagenDalProviderPersoneel());
            base.LoadFileCats();
            base.CreateTempFolder();
            base.ShowCategory = true;
        }
    }
    public class BijlagenBLLProviderClientdossier: AbstractBijlagenBLLProvider
    {
        /** CONSTRUCTOR */
        public BijlagenBLLProviderClientdossier()
        {
            base.module = "CD";
            base.dal = new BijlagenDALManager(new BijlagenDalProviderClientdossier());
            base.LoadFileCats();
            base.CreateTempFolder();
            base.ShowCategory = false;
        }

        /** METHODS */
        public List<FileItem> GetAllFilesFilteredOnOwnerIdAndLabellist(int id, List<labels> labelList)
        {
            if (base.dal.GetProvider() is BijlagenDalProviderClientdossier dalClient)
                return dalClient.SelectAllFilesFromBewonerAndLabellist(id, labelList);
            else
                return null;
        }
    }
    public class BijlagenBLLProviderWerkbonnen : AbstractBijlagenBLLProvider
    {
        /** CONSTRUCTOR */
        public BijlagenBLLProviderWerkbonnen()
        {
            base.module = "WERKBON";
            base.dal = new BijlagenDALManager(new BijlagenDalProviderWerkbonnen());
            base.LoadFileCats();
            base.CreateTempFolder();
        }
    }
    public class BijlagenBLLProviderLocaties : AbstractBijlagenBLLProvider
    {
        /** CONSTRUCTOR */
        public BijlagenBLLProviderLocaties()
        {
            base.module = "LOCATIE";
            base.dal = new BijlagenDALManager(new BijlagenDalProviderLocatie());
            base.LoadFileCats();
            base.CreateTempFolder();
        }
    }
    public class BijlagenBLLProviderInventaris : AbstractBijlagenBLLProvider
    {
        /** CONSTRUCTOR */
        public BijlagenBLLProviderInventaris()
        {
            base.module = "INVENTARIS";
            base.dal = new BijlagenDALManager(new BijlagenDalProviderInventaris());
            base.LoadFileCats();
            base.CreateTempFolder();
        }
    }
    public class BijlagenBLLProviderClientdossierOvereenkomst : AbstractBijlagenBLLProvider
    {
        public OvereenkomstWeergave Overeenkomst;

        public BijlagenBLLProviderClientdossierOvereenkomst(OvereenkomstWeergave overeenkomst)
        {
            base.ShowCategory = true;
            base.module = "OVEREENKOMST";
            base.dal = new BijlagenDALManager(new BijlagenDalProviderClientdossierOvereenkomsten(overeenkomst.bewoner_id));
            base.LoadFileCats();
            Overeenkomst = overeenkomst;
        }
    }

    /***** DATA TRANSFER & REPRESENTATION CLASSES *****/
    public class FileItem : MyNotifyPropertyChanged
    {
        /** FIELDS */
        public int Id { get; set; }
        public int Creator { get; set; }
        public DateTime CreationDate { get; set; }
        public int ObjectId { get; set; }
        public string FileName { get; set; }
        public string FileExtension { get; set; }
        public byte[] FileData { get; set; }
        public file_cats Category { get; set; }
        public string CategoryName
        {
            get
            {
                if (Category != null)
                {
                    return Category.code;
                }
                return null;
            }
        }
        private String _description;
        public string Description
        {
            get
            {
                return _description;
            }
            set
            {
                _description = value;
                IsModified = true;
            }
        }
        public string CreatorName { get; set; }
        public string Rowguid { get; set; }
        public ObservableCollection<labels> Labels { get; set; }
        public ObservableCollection<labels> VisibleLabels { get; set; }
        private string _labelsKommaSeperated;
        public string LabelsKommaSeperated
        {
            get
            {
                RefreshLabelsKommaSeperated();
                return _labelsKommaSeperated;
            }
            set
            {
                _labelsKommaSeperated = value;
                NotifiyPropertyChanged(nameof(LabelsKommaSeperated));
            }
        }
        public bool IsModified { get; set; }
        public bool CanEditDescription
        {
            get
            {
                if (Category != null)
                    return Category.module != "OVEREENKOMST";
                else
                    return true;
            }
        }

        /** METHODS */
        private void RefreshLabelsKommaSeperated()
        {
            Labels = new ObservableCollection<labels>(LabelManager.GetAllLabelFromBijlage(this));
            _labelsKommaSeperated = String.Empty;
            foreach (labels label in Labels)
            {
                if (!string.IsNullOrWhiteSpace(_labelsKommaSeperated))
                {
                    _labelsKommaSeperated += ", ";
                }
                _labelsKommaSeperated += label.name;
            }
        }
    }
    public enum BijlageType
    {
        external, 
        internalWord,
        internalExcel
    }

    public class SaveBijlageData
    {
        public String Descr { get; set; }
        public String FileName { get; set; }
        public Byte[] FileData { get; set; }
        public int ObjectId { get; set; } // bewonerId, inventarisId, werkbonId, ...
        public int OvereenkomstId { get; set; }
        public file_cats Category { get; set; }
        public List<labels> LabelList { get; set; }

        public BijlageType BijlageType { get; set; } = BijlageType.external;
    }
}
