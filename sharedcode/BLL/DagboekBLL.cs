﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using sharedcode.common;
using System.ComponentModel;
using sharedcode.DAL;
using System.Windows.Media.Imaging;
using System.Collections.ObjectModel;
using sharedcode.CommonObjects;

namespace sharedcode.BLL
{
    public class DagboekWeergave : MyNotifyPropertyChanged
    {
        /** FIELDS */
        public Byte[] ImageInBytes { get; set; }
        #region DagboekUC Weergave
        public BitmapImage ImageBewoner { get; set; }
        public String FotoDescr { get; set; }
        public String CreationdateIntelligentText { get; set; }
        public String Message { get; set; }
        public String LabelsKommaSeperated { get; set; }
        public String ToolTipFavorietenSter
        {
            get
            {
                if (_isFavoriet)
                    return "Klik om dit dagboekitem uit de favorieten te verwijderen";
                else
                    return "Klik om dit dagboekitem aan de favorieten toe te voegen";
            }
        }
        public DateTime DateEvent
        {
            get
            {
                if (dagboekDB.datum.HasValue)
                {
                    DateTime datum = dagboekDB.datum.Value;
                    return dagboekDB.datum.Value; //datum.Day + "\n" + GetMonthString(datum.Month) + "\n" + datum.Year;
                }
                return dagboekDB.creationdate;
            }
        }
        private Boolean _isFavoriet;
        public Boolean IsFavoriet
        {
            get
            {
                return _isFavoriet;
            }
            set
            {
                _isFavoriet = value;

                NotifiyPropertyChanged(nameof(IsFavoriet));
            }
        }
        public Boolean IsBewonerBericht { get; set; }
        public Boolean CanEditDelete
        {
            get
            {
                LoggedOnUser user = GlobalData.Instance.LoggedOnUser;
                return user.HasAccess("1001") || (user.Id.HasValue && user.Id.Value == dagboekDB.creatorId);
            }
        }
        #endregion
        #region Saved Data
        public int DagboekId;
        public dagboek dagboekDB;
        private ObservableCollection<labels> _dagboekLabels;
        public ObservableCollection<labels> DagboekLabels
        {
            get
            {
                return _dagboekLabels;
            }
            set
            {
                _dagboekLabels = value;

                InitLabelsKommaSeperated();

                NotifiyPropertyChanged(nameof(LabelsKommaSeperated));
                NotifiyPropertyChanged(nameof(DagboekLabels));
            }
        }
        #endregion

        /** CONSTRUCTOR */
        public DagboekWeergave(dagboek dagBoek)
        {
            dagboekDB = dagBoek;
            DagboekId = dagBoek.id;
            Message = dagBoek.descr;
            IsBewonerBericht = dagBoek.bewonerId.HasValue;
            DagboekLabels = new ObservableCollection<labels>();
        }

        /** METHODS */
        public void InitLabelsKommaSeperated()
        {
            LabelsKommaSeperated = null;
            foreach (labels label in _dagboekLabels)
            {
                if (!String.IsNullOrWhiteSpace(LabelsKommaSeperated))
                    LabelsKommaSeperated += ", ";
                LabelsKommaSeperated += label.name;
            }
        }
        private String GetMonthString(int monthNumber)
        {
            switch (monthNumber)
            {
                case 1: return "Jan";
                case 2: return "Feb";
                case 3: return "Maart";
                case 4: return "April";
                case 5: return "Mei";
                case 6: return "Juni";
                case 7: return "Juli";
                case 8: return "Aug";
                case 9: return "Sep";
                case 10: return "Okt";
                case 11: return "Nov";
                case 12: return "Dec";
                default: return "";
            }
        }
    }
    // Klasse die alle zoekfilters bijhoudt van dagboek
    public class DagBoekFilter: MyNotifyPropertyChanged
    {
        public bewoner Bewoner { get; set; }
        public gebruiker GekozenGebruiker { get; set; }
        public leefgroep GekozenLeefgroep { get; set; }
        public String NaamBewonerOfLeefgroep
        {
            get
            {
                if (GekozenLeefgroep != null)
                    return GekozenLeefgroep.naam;
                else if (Bewoner != null)
                    return Bewoner.VoornaamAchternaam;
                else
                    return null;
            }
        }
        public DateTime VanDatum { get; set; }
        public DateTime TotDatum { get; set; }
        public String Bericht { get; set; }
        public List<labels> Labels { get; set; }

        public DagBoekFilter()
        {
            Labels = new List<labels>();
        }
        
        public void Clear()
        {
            Bewoner = null;
            GekozenGebruiker = null;
            GekozenLeefgroep = null;
            Bericht = String.Empty;
            Labels = new List<labels>();
        }
        public string GetLabelIds()
        {
            string labelIds = "";
            foreach (labels label in Labels)
            {
                if (!string.IsNullOrWhiteSpace(labelIds))
                    labelIds += ", ";
                labelIds += label.id;
            }

            return labelIds;
        }
    }

    public class DagboekBLL
    {
        /** FIELDS */
        private DagboekDALManager _dal;
        private bool _isFavorietenView;

        /** CONSTRUCTOR */
        /// <summary>Automatically sets the view to AllDagboekItems</summary>
        public DagboekBLL()
        {
            _dal = new DagboekDALManager(new DagboekDALProviderAll());
            _isFavorietenView = false;
        }

        /** METHODS */
        public void SwitchViews()
        {
            if (_isFavorietenView)
            {
                _dal = new DagboekDALManager(new DagboekDALProviderAll());
                _isFavorietenView = false;
            }
            else
            {
                _dal = new DagboekDALManager(new DagboekDALProviderFavorieten());
                _isFavorietenView = true;
            }
        }

        public List<DagboekWeergave> GetAllDagboekWeergaves(DagBoekFilter filter)
        {
            return _dal.GetAllDagboekWeergaves(filter);
        }
        public void InsertDagboek(DagboekWeergave dagBoek)
        {
            _dal.InsertDagboek(dagBoek);
        }
        public void RemoveDagboekFromFavorieten(DagboekWeergave dagBoek)
        {
            _dal.DeleteDagboekFavoriet(dagBoek);
        }
        public void InsertDagboekFavoriet(DagboekWeergave dagBoek)
        {
            _dal.InsertDagboekFavoriet(dagBoek);
        }
        public void RemoveDagboek(DagboekWeergave dagBoek)
        {
            _dal.DeleteDagboek(dagBoek);
        }
        public void ChangeDagboekItem(DagboekWeergave dagBoek)
        {
            _dal.UpdateDagboek(dagBoek);
        }
    }
}
