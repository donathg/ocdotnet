﻿using sharedcode.common;
using sharedcode.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sharedcode.BLL
{


   


    public partial class vw_gebruikergroep
    {

        public String Naam
        {
            get
            {
                return voornaam + " " + achternaam;
            }

        }

        public String VoornaamAchternaam
        {

            get
            {

                return voornaam + " " + achternaam;
            }

        }
        public String AchternaamVoornaam
        {

            get
            {

                return achternaam + " " + voornaam;
            }

        }
        public String Voornaam
        {

            get
            {

                return voornaam;
            }

        }
        public String Achternaam
        {
            get { return achternaam; }

        }
        public String Code
        {

            get { return personeelscode; }

        }

    }




    public partial class gebruiker
    {

        public Byte[] GetFoto()
        {
            if (this.gebruiker_foto == null)
                return null;
            return this.gebruiker_foto.FirstOrDefault()?.foto;
        }
        public void SetFoto(Byte[] fotoBytes)
        {

            gebruiker_foto foto = new gebruiker_foto();
            foto.foto = fotoBytes;
            if (this.gebruiker_foto == null)
                this.gebruiker_foto = new List<gebruiker_foto>();
            this.gebruiker_foto.Clear();
            this.gebruiker_foto.Add(foto);
        }
        public Byte[] defaultFoto
        {
            get { return GetFoto(); }

        }



        public String Code
        {

            get { return personeelscode; }

        }
        public String Voornaam
        {

            get
            {

                return voornaam;
            }

        }
        public String Achternaam
        {

            get
            {

                return achternaam;
            }

        }
        public String Naam
        {

            get
            {

                return voornaam + " " + achternaam;
            }

        }
        public String VoornaamAchternaam
        {

            get
            {

                return voornaam + " " + achternaam;
            }

        }
        public String AchternaamVoornaam
        {

            get
            {

                return achternaam + " " + voornaam;
            }

        }

    }
    public class GebruikerBLL
    {


        public static gebruiker GetGebruiker (int id)
        {
            using (Entities entity = new Entities(sharedcode.common.GlobalData.Instance.Entity))
            {
                return entity.gebruiker.Where (x=>x.id==id).FirstOrDefault();
                
                
            }
        }


        public static List<vw_gebruikergroep> GetGebruikers()
        {
            using (Entities entity = new Entities(sharedcode.common.GlobalData.Instance.Entity))
            {
                return entity.vw_gebruikergroep.ToList();
            }
        }

        private static List<gebruiker> _allGebruikers = null;
        public static List<gebruiker> GetAllGebruikers()
        {
            if (_allGebruikers == null)
            {
                using (Entities entity = new Entities(sharedcode.common.GlobalData.Instance.Entity))
                {
                    _allGebruikers =  entity.gebruiker.Where(x => x.voornaam != "system").OrderBy(x => x.achternaam).ToList();
                }
            }
            return _allGebruikers;
        }


        private static List<gebruiker> _allGebruikersModulePersoneel = null;
        public static List<gebruiker> GetAllGebruikersModulePersoneel()
        {
            if (_allGebruikersModulePersoneel == null)
            {
                using (Entities entity = new Entities(sharedcode.common.GlobalData.Instance.Entity))
                {
                    _allGebruikersModulePersoneel = entity.gebruiker.Include("gebruiker_foto").Where(x => x.voornaam != "system").OrderBy(x => x.achternaam).ToList();
                }
            }
            return _allGebruikersModulePersoneel;
        }


        public static DataTable GetTelefoonBoekItems()
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                string SQL = @"select gebruiker_foto.foto foto, gebruiker.*
                            from gebruiker left outer join gebruiker_foto on gebruiker.id = gebruiker_foto.gebruikerid
                            where actif= 1 and (gebruiker.personeelscode in (select VPERSCOD from vw_gebruikergroep_orbis where VSUBDNSCOD not in ('GEDET', 'CONVB') and isFromOrbis=1 ) or isFromOrbis=0)
                            
order by voornaam";

                return db.GetDataTable(SQL);
            }
        }
        public static void SavePicture(int gebruikerId, Byte[] image)
        {
            if (image.Count() > 300000)
                throw new Exception("Gelieve enkel foto's toe te voegen kleiner dan 100 Kilobytes");

            using (Entities e = new Entities(GlobalData.Instance.Entity))
            {
                gebruiker_foto b = (from p in e.gebruiker_foto where p.gebruikerId == gebruikerId select p).FirstOrDefault();
                if (b != null)
                {
                    b.foto = image;

                }
                else
                {
                    e.gebruiker_foto.Add(new gebruiker_foto { gebruikerId = gebruikerId, foto = image });

                }
                e.SaveChanges();
            }
        }

        public static void SaveGebruiker(gebruiker gebruiker)
        {
            using (Entities e = new Entities(sharedcode.common.GlobalData.Instance.Entity))
            {
                gebruiker b = (from p in e.gebruiker where p.id == gebruiker.id select p).FirstOrDefault();
                if (b != null)
                {


                    if (!String.IsNullOrWhiteSpace(gebruiker.email))
                    {
                        try
                        {
                            var addr = new System.Net.Mail.MailAddress(gebruiker.email);
                        }
                        catch(Exception)
                        {

                            throw new Exception("Gelieve een geldig emailadres in te geven");
                        }
                    }
                    if (String.IsNullOrWhiteSpace(gebruiker.voornaam))
                        throw new Exception("Gelieve een voornaam in te geven");
                    if (String.IsNullOrWhiteSpace(gebruiker.achternaam))
                        throw new Exception("Gelieve een achternaam in te geven");


                    b.voornaam = gebruiker.voornaam;
                    b.achternaam = gebruiker.achternaam;
                    b.email = gebruiker.email;
                    b.telefoon = gebruiker.telefoon;
                    b.gsm = gebruiker.gsm;
                    b.werkplaats = gebruiker.werkplaats;
                    b.telefoon_intern = gebruiker.telefoon_intern;
                    b.telefoon_intern_groep = gebruiker.telefoon_intern_groep;
                    b.functiebeschrijving = gebruiker.functiebeschrijving;
                    e.SaveChanges();
                }
            }
        }
    }
    
}
