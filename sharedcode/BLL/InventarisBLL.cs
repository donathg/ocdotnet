﻿using sharedcode.common;
using sharedcode.DAL;
using sharedcode.WeergaveClasses;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
 

namespace sharedcode.BLL
{
    public class InventarisSettings
    {
        /// <summary>
        /// null : alle locaties
        /// </summary>
        public int? LocatieId { get; set; } = null;
        /// <summary>
        /// null : beide
        /// true : enkel uitDienst
        /// false: enkel niet uit-dienst
        /// </summary>
        public bool? LoadUitdienst { get; set; } = null;
        /// <summary>
        /// null : beide
        /// true : enkel Geinventariseerd
        /// false: enkel niet Geinventariseerd
        /// </summary>
        public bool? Geinventariseerd { get; set; } = null;
        public bool AddEmptyRecord { get; set; }
        /// bepaalt automatisch of je de infoIT-tabel mee moet ophalen in de databank
        public bool HasITRights
        {
            get
            {
                return GlobalData.Instance.LoggedOnUser.HasAccess("202")  || GlobalData.Instance.LoggedOnUser.HasAccess("201");
            }
        }
        /// <summary>
        /// IT, PAV, GK
        /// </summary>
        public List<ToestelCategorieType> ToestelCategorien { get; set; }
        public List<InventarisStatusType> StatusTypes { get; set; }
    }
    public class InventarisBLL : IDisposable/*, IClaraFeyCleanup*/
    {
        /** FIELDS */
        private readonly InventarisSettings _settings;
        private List<InventarisWeergave> _inventarisItems;
        private List<LeverancierItem> _leveranciers;
        private List<toestel> _toestellen;
        private List<merk> _merken;

        /** CONSTRUCTORS */
        public InventarisBLL(InventarisSettings settings)
        {
            _settings = settings;
        }

        /** METHODS */
        #region Leveranciers
        public void ReloadLeveranciersFromDB()
        {
            _leveranciers = null;
        }
        public List<LeverancierItem> GetAllLeveranciers()
        {
            if (_leveranciers == null)
                _leveranciers = InventarisDAL.SelectAllLeveranciers();

            return _leveranciers;
        }
        public LeverancierItem GetLeverancierById(int id)
        {
            return this.GetAllLeveranciers().First(i => i.Id == id);
        }
        #endregion
        #region Merken
        public void ReloadMerkenFromDB()
        {
            _merken = null;
        }
        public List<merk> GetAllMerken()
        {
            if (_merken == null)
                _merken = InventarisDALRefactor.SelectAllMerken();

            return _merken;
        }
        public merk GetMerkById(int id)
        {
            return GetAllMerken().First(x => x.id == id);
        }
        #endregion
        #region Toestellen
        public void ReloadToestellenFromDB()
        {
            _toestellen = null;
        }
        public List<toestel> GetAllToestellen()
        {
            if (_toestellen == null)
                _toestellen = InventarisDALRefactor.SelectAllToestellen();
            return _toestellen;
        }
        public toestel GetToestelById(int id)
        {
            return GetAllToestellen().First(x => x.id == id);
        }
        public static List<string> GetAllToestelCategorieTypes()
        {
            List<string> catList = new List<string>();
            foreach (ToestelCategorieType c in Enum.GetValues(typeof(ToestelCategorieType)))
            {
                catList.Add(c.ToString());
            }
            return catList;
        }
        #endregion
        #region Inventarisitems
        public void ReloadInventarisItemsFromDB()
        {
            _inventarisItems = null;
        }
        public List<InventarisWeergave> GetAllInventarisItems()
        {
            if (_inventarisItems == null || _inventarisItems.Count == 0)
                _inventarisItems = InventarisDALRefactor.SelectAllInventarisItemsFromSettings(_settings);

            return _inventarisItems;
        }
        public InventarisWeergave GetInventarisItemFromId(int id)
        {
            return GetAllInventarisItems().First(x => x.id == id);
        }
        #endregion
        #region Locatie Objecten
        public static List<objecttype> GetAllObjecttypes()
        {
            return InventarisDALRefactor.SelectAllObjecttypes();
        }
        public static objecttype GetObjecttypeFromid(int id)
        {
            return InventarisDALRefactor.SelectObjecttypeFromId(id);
        }
        public static List<LocatieobjectWeergave> GetAllObjectenMetLocatiesFromObjecttypeId(int id)
        {
            return InventarisDALRefactor.SelectAllObjectenMetLocatie(id);
        }
        #endregion

        public void ChangeLocation (int? LocatieId)
        {
            _settings.LocatieId = LocatieId;
            _inventarisItems = new List<InventarisWeergave>();
        }
        public void SetLocation(InventarisWeergave inv, Location loc)
        {
            inv.locatie_id = loc.Id;
            inv.LocatiePath = loc.GetFullPath();
        }
        public List<InventarisPrintItem> ConvertToPrintItems(List<InventarisWeergave> inventarisItems)
        {
            List<InventarisPrintItem> printList = new List<InventarisPrintItem>();
            foreach (InventarisWeergave i in inventarisItems)
            {
                InventarisPrintItem p = new InventarisPrintItem()
                {
                    Nummer = i.nummer_263,
                    Omschrijving = i.productOmschrijving
                };
                if (i.leverancier_id.HasValue)
                {
                    var leverancier = this.GetAllLeveranciers().First(x => x.Id == i.leverancier_id);
                    p.Leverancier = leverancier.Naam;
                }
                if (i.merk_id.HasValue)
                {
                    var merk = this.GetAllMerken().First(x => x.id == i.merk_id);
                    p.Merk = merk.naam;
                }
                if (i.toestel_id.HasValue)
                {
                    var toestel = this.GetAllToestellen().First(x => x.id == i.toestel_id);
                    p.Toestel = toestel.naam;
                    p.Categorie = toestel.category;
                }
                p.LocatiePath = i.LocatiePath;

                printList.Add(p);
            }
            return printList;

        }
        public void Save()
        {
            var modifiedInventarisItems = from inv in this._inventarisItems where inv.IsModified == true select inv;
            List<InventarisWeergave> items = modifiedInventarisItems.ToList();
            if (items.Count > 0)
            {
                try
                {
                    InventarisDALRefactor.SaveInventarisProcedure(items);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
        public  void CleanUp()
        {

            if (_inventarisItems != null)
            {
                _inventarisItems.Clear();
                _inventarisItems=null;

            }
             if (_leveranciers != null)
            {
                _leveranciers.Clear();
                _leveranciers=null;

            }
             if (_toestellen != null)
            {
                _toestellen.Clear();
                _toestellen=null;

            }
             if (_merken != null)
            {
                _merken.Clear();
                _merken=null;

            }
            
        }
        public void Dispose()
        {
            CleanUp();
        }
    }
    public partial class objecttype
    {
        public string GlyphLocation
        {
            get
            {
                if (this.id == 1) /* ASBEST */
                    return "pack://application:,,,/images/asbest.png";
                else if (this.id == 2) /* SLEUTEL */
                    return "pack://application:,,,/images/sleutel.png";
                else if (this.id == 3) /* BRANDBLUSSER */
                    return "pack://application:,,,/images/brandblusser.jpg";
                else if (this.id == 4) /* DATARACK */
                    return "pack://application:,,,/images/rack.png";
                else
                    return "pack://application:,,,/images/excel.png";
            }
        } 
    }
}
