﻿using sharedcode.BLL;
using sharedcode.common;
using sharedcode.CommonObjects;
using sharedcode.DAL;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sharedcode.WeergaveClasses
{
    public enum KilometerModeType
    {
        ingave, //ingeven van eigen kilometers
        boekhouding, //overzicht type leefgroep met mogelijkheid afsluiten
        bewoners, //overzicht type leefgroep met mogelijkheid afsluiten
        personeelsdienst, //overzicht eigen wagen met mogelijkheid afsluiten
        overzicht //volledig overzicht , alle types alle wagens
    }

    public class KilometerLeefgroepBewonerShortReportItem
    {
        public string Naam { get; set; }
        public string Datum { get; set; }
        public decimal Bedrag { get; set; }
    }
    public class KilometerLeefgroepBewonerShortCsvReportItem
    {
        public string Naam { get; set; }
        public string Rijksregisternummer { get; set; }

        public string Ondersteuningsvorms { get; set; }

        public DateTime Datum { get; set; }
        public string DatumString
        {
            get
            {

                return Datum.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
            }
        }
        public string FinancielbewegingsCode
        {
            get
            {
                if (this.Rijksregisternummer== "13011711473")
                {
                    int i = 0;
                }

                return Ondersteuningsvorms == "MFC" ? "VER" : "VEP";


            }
        }
        public DateTime Omschrijving { get; set; }
        public int Eenheden { get; set; } = 1;
        public decimal Eenheidsprijs { get; set; }
        public decimal Totaal { get; set; }

  
    }
    public class KilometerLeefgroepBewonerReportItem
    {
        public string Rijksregisternummmer { get; set; }

        public string Ondersteuningsvorm { get; set; }
        public string Code { get; set; }
        public string Naam { get; set; }
        public string Bestuurder { get; set; }
        public string Route { get; set; }
        public string Reden { get; set; }
        public decimal Bedrag { get; set; }
        public DateTime Datum { get; set; }
        /// <summary>
        /// Enkel voor reportgrouepring te sorterren
        /// </summary>
        public string NaamCode
        {
            get
            {
                return Naam + Code;
            }
        }
    }
    public class KilometerPersoneelsdienstReportItem
    {
        public string Persnr { get; set; }
        public string Naam { get; set; }
        public bool Omnium { get; set; }
        public decimal Km { get; set; }
        public decimal Price { get; set; }

        public KilometerPersoneelsdienstReportItem(string persNr, string naam, bool omnium, decimal km, decimal price)
        {
            Naam = naam;
            Omnium = omnium;
            Km = km;
            Price = price;
            Persnr = persNr;
        }
    }

    [Serializable]
    public class KilometerItem : MyNotifyPropertyChanged
    {
        /** FIELDS */
  
        public bool HasChilds
        {
            get
            {
                return Type.Code == "LFGR" || Type.Code == "CLIENT" || Type.Code == "KAMP" || Type.Code == "IRO";
            }

        }
       
        public bool DoNotify { get; set; } = true;

      
        public int? Id { get; set; }

    
        public string Bestemming { get; set; }
        public string Reden { get; set; }
        private bool _omnium;
        public bool Omnium
        {
            get
            {
                return _omnium;
            }

            set
            {
                _omnium = value;
                NotifiyPropertyChanged(nameof(Omnium));
            }
        }
        private int? _beginstandKilometer;
        public int? BeginstandKilometer
        {
            get
            {
                return _beginstandKilometer;
            }

            set
            {
                if (value != _beginstandKilometer)
                {
                    if (value == null)
                        _kilometers = null;
                    _beginstandKilometer = value;
                    NotifiyPropertyChanged(nameof(BeginstandKilometer));
                    NotifiyPropertyChanged(nameof(Kilometers));
                }
            }
        }
        private int? _eindstandKilometer;
        public int? EindstandKilometer
        {
            get
            {
                return _eindstandKilometer;
            }

            set
            {
                if (value != _eindstandKilometer)
                {
                    if (value == null)
                        _kilometers = null;
                    _eindstandKilometer = value;
                    NotifiyPropertyChanged(nameof(EindstandKilometer));
                    NotifiyPropertyChanged(nameof(Kilometers));
                }

            }
        }
        private decimal? _kilometers;
        public decimal? Kilometers
        {
            get
            {
                if (BeginstandKilometer != null || EindstandKilometer != null)//eigen wagen
                {
                    if (BeginstandKilometer == null || EindstandKilometer == null)
                        return null;
                    else
                    {
                        if ((decimal)(EindstandKilometer.GetValueOrDefault() - BeginstandKilometer.GetValueOrDefault()) < 0)
                            return null;
                        else
                            return EindstandKilometer.GetValueOrDefault() - BeginstandKilometer.GetValueOrDefault();
                    }
                }
                else
                    return _kilometers;
            }

            set
            {
                _kilometers = value;
            }
        }

        private bool _IsCheckedByBewonersAdministratie { get; set; }
        public bool  IsCheckedByBewonersAdministratie { get { return _IsCheckedByBewonersAdministratie; } set { _IsCheckedByBewonersAdministratie = value; NotifiyPropertyChanged(nameof(IsCheckedByBewonersAdministratie)); } }
        public decimal PricePerKilometer { get; set; }
        public decimal PriceOmniumPerKilometer { get; set; }
        public KilometersType Type { get; set; }
        public KilometersWagen Wagen { get; set; }
        public decimal? PriceOmnium { get; set; }
        public DateTime Datum { get; set; }
        /// <summary>
        /// De gebruiker die gereden heeft en eventueel het bedrag ontvangt
        /// </summary>
        public User Bestuurder { get; set; }
        public decimal? Price { 
            get; 
            set;
        }
        public List<BewonersLeefgroepenDetail> BewonersLeefgroepenDetail
        {
            get
            {
                List<BewonersLeefgroepenDetail> list = new List<BewonersLeefgroepenDetail>();

                if (Leefgroepen.Count > 0)
                {
                    foreach (leefgroep lfg in this.Leefgroepen)
                    {
                        list.Add(new BewonersLeefgroepenDetail()
                        {
                            Code = lfg.code,
                            Name = lfg.naam,
                            Price = PriceRounded.GetValueOrDefault() / Leefgroepen.Count,
                            Type = "LFGR"
                        });
                    }
                }
                if (Bewoners.Count > 0)
                {
                    foreach (bewoner bew in Bewoners)
                    {
                        list.Add(new BewonersLeefgroepenDetail()
                        {
                            Code = bew.code,
                            Name = bew.VoornaamAchternaam,
                            Price = Bewoners.Count==1 ? PriceRounded.GetValueOrDefault() / 1 : PriceRounded.GetValueOrDefault() /2,
                            Rijksregisternr = bew.rijksregisternr,
                            Type = Type.Code
                        });
                    }
                }
                if (Kampen.Count > 0)
                {
                    foreach (kampen k in Kampen)
                    {
                        list.Add(new BewonersLeefgroepenDetail()
                        {
                            Code = k.nr,
                            Name = k.bestemming,
                            Price = PriceRounded.GetValueOrDefault() / Kampen.Count,
                            Type = "KAMP"
                        });
                    }
                }

                return list;
            }
        }
        public decimal? PriceRounded
        {
            get
            {
                if (Price.HasValue)
                    return Math.Round(Price.GetValueOrDefault(), 2, MidpointRounding.AwayFromZero);
                else return null;
            }
        }
        public decimal? PriceOmniumRounded
        {
            get
            {
                if (PriceOmnium.HasValue)
                    return Math.Round(PriceOmnium.GetValueOrDefault(), 2, MidpointRounding.AwayFromZero);
                else return null;
            }
        }
        public decimal? PriceLessOmnium
        {
            get
            {
                if (Price.HasValue)
                    return PriceRounded.GetValueOrDefault() - PriceOmniumRounded.GetValueOrDefault();
                return null;
            }
        }
        public List<kampen> Kampen { get; set; }
        public List<leefgroep> Leefgroepen { get; set; }
        public List<bewoner> Bewoners { get; set; }
        public bool IsGeslotenPersoneelsdienst
        {
            get
            {
                return GeslotenPersoneelsdientDatum.HasValue;
            }
        }
        public DateTime? GeslotenPersoneelsdientDatum { get; set; }
        public int? GeslotenPersoneelsdientGebruikerId { get; set; }
        public bool IsGeslotenBewoners
        {
            get
            {
                return GeslotenBewonersDatum.HasValue;
            }
        }
        public DateTime? GeslotenBewonersDatum { get; set; }
        public int? GeslotenBewonersGebruikerId { get; set; }
        public bool IsGeslotenBoekhouding
        {
            get
            {
                return GeslotenBoekhoudingDatum.HasValue;
            }
        }
        public DateTime? GeslotenBoekhoudingDatum { get; set; }
        public int? GeslotenBoekhoudingGebruikerId { get; set; }

        /** CONSTRUCTORS */
        public KilometerItem()
        {
            Type = new KilometersType();
            Wagen = new KilometersWagen();
            Bestuurder = new User();
            Leefgroepen = new List<leefgroep>();
            Kampen = new List<kampen>();
            Bewoners = new List<bewoner>();
            Datum = DateTime.Now;
        }

        /** METHODS */
        public void AddBewoner(bewoner bew)
        {
            if (!Bewoners.Contains(bew))
                Bewoners.Add(bew);
        }
        public void DeleteBewoner(int bewonerId)
        {
            Bewoners.RemoveAll(x => x.id == bewonerId);
        }
        public void DeleteLeefgroep(int leefgroepId)
        {
            Leefgroepen.RemoveAll(x => x.id == leefgroepId);
        }
        public void AddLeefgroep(leefgroep leef)
        {
            if (!Leefgroepen.Contains(leef))
                Leefgroepen.Add(leef);
        }
        public void DeleteKamp(int kampId)
        {
            Kampen.RemoveAll(x => x.id == kampId);
        }
        public void AddKamp(kampen kamp)
        {
            if (!Kampen.Contains(kamp))
                Kampen.Add(kamp);
        }
        public void Save()
        {
            KilometerVergoedingDAL.SaveKilometers(this);
        }
       
    }
    /* public enum KilometerType
     {
         OC, VTO, CLIENT, LFGR,IRO,KAMP
     }*/

        [Serializable]
    public class KilometersType
    {
        public string Code { get; set; }
        public string Naam { get; set; }
    }
    [Serializable]
    public class KilometersWagen
    {

        public bool IsWagenOfFiets { get
            {

                if (Id == 1)
                    return true;// "Eigen wagen";
                if (Id == 5)
                    return true;//  "Eigen fiets";
                return false;


            }
        }

        public int? Id { get; set; }
        public string Naam { get
            {

                if (Id == null)
                    return "";
                if (Id == 1)
                    return "Eigen wagen";
                if (Id == 5)
                    return "Eigen fiets";
                else
                    return $"{Nummerplaat} {Merk} {Campus.naam}";

            }
            }
        public locatie Campus { get; set; } = new locatie();
        public string Nummerplaat { get; set; }
        public string Merk { get; set; }
        public string Kleur { get; set; }
        public string Bouwjaar { get; set; }

    }

    [Serializable]
    public class BewonersLeefgroepenDetail
    {
        public string Code { get; set; }
        public string Rijksregisternr { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public string Type { get; set; }
    }
}
