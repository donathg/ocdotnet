﻿using sharedcode.BLL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sharedcode.WeergaveClasses
{
    public class ActieplanOpvolgingWeergave: actieplanopvolging
    {
        public string AangemaaktDoor { get; set; }
        public string AangemaaktDoorNaamEnDatum
        {
            get
            {
                return creationDate.ToShortDateString() + ", " + AangemaaktDoor;
            }
        }
        public bool IsEditable
        {
            get
            {
                return actieplan.afgewerktdoor == null;                
            }
        }
    }
    public class ActieplanWeergave: actieplan, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public string AangemaaktDoor { get; set; }
        public string VerandertDoor { get; set; }
        public string AfgewertkDoor { get; set; }
        public string TypesString
        {
            get
            {
                string tempString = "";
                Actieplantypes.ForEach(x => tempString += x.name + ", ");
                return tempString.Remove(tempString.Count() - 2);
            }
        }
        public List<actieplantypes> Actieplantypes { get; set; }
        public List<ActieplanOpvolgingWeergave> ActieplanOpvolgingen { get; set; }
        public bool IsAfgewerkt
        {
            get
            {
                return afgewerktdoor != null && afgewerktdoor != 0;
            }
        }

        private void NotifiyPropertyChanged(string property)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(property));
        }
    }
    public class ActieplanReportModel
    {
        public bewoner Bewoner { get; set; }
        public ActieplanWeergave Actieplan { get; set; }
        public List<actieplanopvolging> Opvolgingen { get; set; }

        public void CreateReport(bewoner bew, ActieplanWeergave actieplan, List<actieplanopvolging> opvolgingen)
        {
            this.Bewoner = bew;
            this.Actieplan = actieplan;
            this.Opvolgingen = opvolgingen;
        }
    }
    public class ActieplanEvaluatiesWeergave: Actieplan_Evaluaties
    {
        public string CreatorName { get; set; }
        public List<actieplantypes> Actieplantypes { get; set; }
        public string TypesString
        {
            get
            {
                String tempString = "";
                Actieplantypes.ForEach(x => tempString += x.name + ", ");
                return tempString.Remove(tempString.Count() - 2);
            }
        }
    }
}
