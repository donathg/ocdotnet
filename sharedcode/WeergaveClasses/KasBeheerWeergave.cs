﻿using sharedcode.BLL;
using sharedcode.common;
using sharedcode.CommonObjects;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sharedcode.WeergaveClasses
{
    public class KasBeheerExportWeergave
    {
        public int Id { get; set; }
        public int ParentId { get; set; }
        public string LeefgroepNaam { get; set; }
        public bewoner Bewoner { get; set; }
        public decimal Bedrag { get; set; }
        public string TypeVerrichting
        {
            get
            {
                if ((IsClientTransactie && Bedrag <= 0) || (!IsClientTransactie && Bedrag >= 0))
                    return "Inkomsten";
                else
                    return "Uitgaven";
            }
        }
        public string Omschrijving { get; set; }
        public string TransactieCode { get; set; }
        public DateTime AangemaaktOp { get; set; }
        public string AangemaaktDoor { get; set; }
        public bool IsAfgesloten { get; set; }
        public DateTime? AfgeslotenOp { get; set; }
        public DateTime TransactieDatum { get; set; }
        public bool IsClientTransactie
        {
            get
            {
                return Bewoner != null && !string.IsNullOrWhiteSpace(Bewoner.voornaam);
            }
        }
        public decimal BedragOverzichtsweergave
        {
            get
            {
                if (IsClientTransactie)
                    return -Bedrag;
                else
                    return Bedrag;
            }
        }
    }
    public class KasbeheerExportReportWeergave
    {
        /** FIELDS */
        public string Rijksregisternummer { get; private set; }
        public DateTime Datum { get; private set; }
        public string DatumString
        {
            get
            {
                return Datum.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
            }
        }
        public string FinancieleBewegingscode { get; private set; }
        public string Omschrijving { get; private set; }
        public int Aantal { get; } = 1;
        public decimal Bedrag { get; private set; }
        public decimal Totaal
        {
            get
            {
                return Aantal * Bedrag;
            }
        }


        /** CONSTRCTORS */
        public KasbeheerExportReportWeergave(KasBeheerExportWeergave exportWeergave)
        {
            int thisyear = DateTime.Today.Year;
            int monthOfClosing = DateTime.Today.Month - 1;

            Rijksregisternummer = exportWeergave.Bewoner.rijksregisternr;
            Datum = DateTime.Parse($"{thisyear}/{monthOfClosing}/{DateTime.DaysInMonth(thisyear, monthOfClosing)}");
            Omschrijving = $"{exportWeergave.TransactieDatum.ToShortDateString()} - {exportWeergave.Omschrijving}";
            Bedrag = exportWeergave.Bedrag;
            FinancieleBewegingscode = exportWeergave.TransactieCode;
        }
    }
}
