﻿using sharedcode.BLL;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sharedcode.WeergaveClasses
{
    public class AdresWeergave : Adressen
    {
        public String Fullname
        {
            get
            {
                return (voornaam + " " + naam).Trim();
            }
        }
        public String Fulladres
        {
            get
            {
                String adres = "";

                if (!String.IsNullOrWhiteSpace(straat))
                    adres += straat;
                if (!String.IsNullOrWhiteSpace(huisnr))
                    adres += " " + huisnr;
                if (String.IsNullOrWhiteSpace(busnr))
                    adres += busnr;
                if (!String.IsNullOrWhiteSpace(adres))
                    adres += ", ";
                if (!String.IsNullOrWhiteSpace(postcode))
                    adres += postcode;
                if (!String.IsNullOrWhiteSpace(gemeente))
                    adres += " " + gemeente;

                return (adres + ", " + LandNaam).Trim();
            }
        }
        public int BewoneradresId { get; set; }
        public String LandNaam { get; set; }
        public adrestype Type { get; set; }
        public int OwnerId { get; set; }
        public String Aansluitingsnummer { get; set; }
        public String Opmerking { get; set; }
        public ObservableCollection<ContactpersoonWeergave> Contactpersonen { get; set; }

        public bool MemberOfClaraFey { get; set; } = false;
        public bool HasChilderen
        {
            get
            {
                return Contactpersonen != null && Contactpersonen.Count != 0;
            }
        }
    }
    public class ContactpersoonWeergave : Adres_contactpersonen, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public String Fullname
        {
            get
            {
                return (voornaam + " " + naam).Trim();
            }
        }
        public bool HasPresentBewoner { get; set; }
        public adrestype Type { get; set; }
        public int BewoneradresId { get; set; }

        public void NotifiyPropertyChanged(string property)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(property));
        }
    }
}
