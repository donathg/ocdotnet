﻿using sharedcode.BLL;
using sharedcode.Caches;
using sharedcode.common;
using sharedcode.CommonObjects;
using sharedcode.DAL;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace sharedcode.WeergaveClasses
{
    public class ContactverslagWeergave : MyNotifyPropertyChanged
    {
        /** FIELDS */
        #region ContactverslagUC Weergave
        public int ContactverslagId { get; private set; }
        public string LabelsKommaSeperated { get; private set; }
        public string FotoDescr { get; private set; }
        public byte[] ImageInBytes { get; private set; }
        public BitmapImage ImageBewoner { get; private set; }
        public string Aanwezigen { get; set; }
        public string Afspraken { get; set; }
        public DateTime Date { get; set; }
        public DateTime CreationDate { get; private set; }
        public string SoortBegeleidingNaam { get; private set; }
        public string CreationdateIntelligentText { get; set; }
        public bool CanEditDelete
        {
            get
            {
                LoggedOnUser user = GlobalData.Instance.LoggedOnUser;
                return user.HasAccess("1001") || (user.Id.HasValue && user.Id.Value == ContactverslagDB.creator);
            }
        }
        public string Verslag { get; set; }
        public string Title { get; set; }
        #endregion
        #region Saved Data
        private Contactverslag _contactverslagDB;
        public Contactverslag ContactverslagDB
        {
            get
            {
                return _contactverslagDB;
            }
            set
            {
                _contactverslagDB = value;
                ContactverslagId = _contactverslagDB.id;
                Aanwezigen = _contactverslagDB.aanwezigen;
                Afspraken = _contactverslagDB.afspraken;
                Date = _contactverslagDB.datum;
                CreationDate = _contactverslagDB.creation_date;
                Verslag = _contactverslagDB.verslag;
                Title = _contactverslagDB.title;

                NotifiyPropertyChanged(nameof(ContactverslagDB));
                NotifiyPropertyChanged(nameof(ContactverslagId));
                NotifiyPropertyChanged(nameof(Aanwezigen));
                NotifiyPropertyChanged(nameof(Afspraken));
                NotifiyPropertyChanged(nameof(Date));
                NotifiyPropertyChanged(nameof(CreationDate));
                NotifiyPropertyChanged(nameof(Verslag));
                NotifiyPropertyChanged(nameof(Title));
            }
        }
        private bewoner _bewoner;
        public bewoner Bewoner
        {
            get
            {
                return _bewoner;
            }
            set
            {
                _bewoner = value;
                if (_bewoner != null)
                {
                    FotoDescr = _bewoner.VoornaamAchternaam;
                    ImageInBytes = _bewoner.GetFoto();
                    ImageBewoner = GlobalSharedMethods.DecodePhoto(ImageInBytes);

                    NotifiyPropertyChanged(nameof(FotoDescr));
                    NotifiyPropertyChanged(nameof(ImageInBytes));
                    NotifiyPropertyChanged(nameof(ImageBewoner));
                }
                NotifiyPropertyChanged(nameof(Bewoner));
            }
        }
        private Contactverslag_SoortBegeleiding _soortBegeleiding;
        public Contactverslag_SoortBegeleiding SoortBegeleiding
        {
            get
            {
                return _soortBegeleiding;
            }
            set
            {
                _soortBegeleiding = value;
                if (value != null)
                {
                    SoortBegeleidingNaam = _soortBegeleiding.naam;
                    NotifiyPropertyChanged(nameof(SoortBegeleidingNaam));
                }

                NotifiyPropertyChanged(nameof(SoortBegeleiding));
            }
        }
        private ObservableCollection<labels> _contactverslagLabels;
        public ObservableCollection<labels> ContactverslagLabels
        {
            get
            {
                return _contactverslagLabels;
            }
            set
            {
                _contactverslagLabels = value;

                NotifiyPropertyChanged(nameof(ContactverslagLabels));
            }
        }
        #endregion

        /** CONSTRUCTOR */
        public ContactverslagWeergave(Contactverslag contactverslag)
        {
            ContactverslagDAL dal = new ContactverslagDAL();

            ContactverslagDB = contactverslag;
            SoortBegeleiding = dal.SelectSoortBegeleidingFromId(contactverslag.soortBegeleiding_id);
            ContactverslagLabels = new ObservableCollection<labels>();
            Bewoner = BewonersListCache.Instance.GetBewoner(contactverslag.bewoner_id);
            if (Date == DateTime.MinValue)
                Date = DateTime.Today;
        }
        public void InitLabelsKommaSeperated()
        {
            if (_contactverslagLabels != null || _contactverslagLabels.Count != 0)
            {
                String tempString = null;
                foreach (labels label in _contactverslagLabels)
                {
                    if (!String.IsNullOrWhiteSpace(tempString))
                        tempString += ", ";
                    tempString += label.name;
                }
                LabelsKommaSeperated = tempString;
            }
            else
                LabelsKommaSeperated = "";

            NotifiyPropertyChanged(nameof(LabelsKommaSeperated));
        }
    }
}
