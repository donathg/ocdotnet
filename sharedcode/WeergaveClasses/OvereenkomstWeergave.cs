﻿using sharedcode.BLL;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sharedcode.WeergaveClasses
{
    public class OvereenkomstWeergave : Cd_Overeenkomst
    {
        public int AantalBijlagen { get; set; }
        public string CreatorNaam { get; set; }
        public string StatusText
        {
            get
            {
                return Status.naam;
            }
        }
        public string TypeText
        {
            get
            {
                return Type.naam;
            }
        }
        public string Naam
        {
            get
            {
                return Type.naam;
            }
        }
        public string BewonerNaam { get; set; }
        public Overeenkomst_status Status { get; set; }
        public Overeenkomst_type Type { get; set; }
        public ObservableCollection<FileItem> Files { get; set; }
    }
}
