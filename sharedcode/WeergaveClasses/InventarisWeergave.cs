﻿using System;
using System.ComponentModel;
using System.Net;
using DevExpress.Xpo;
using sharedcode.BLL;
using sharedcode.common;

namespace sharedcode.WeergaveClasses
{
    public enum ToestelCategorieType
    {
        IT, //Informatica
        PAV, //paviljoen
        TD, //Technische Dienst 
        GK //Grootkeuken 
    }
    public enum InventarisStatusType
    {
        Actief,
        Afgeschreven,
        Afgeboekt,
        Verkocht
    }
    public class InventarisWeergave : inventaris, INotifyPropertyChanged
    {
        /** FIELDS */
        public event PropertyChangedEventHandler PropertyChanged;

        // inventaris-tabel weergave velden
        public string ProductOmschrijvingText
        {
            get
            {
                return productOmschrijving;
            }
            set
            {
                if (productOmschrijving != value)
                {
                    productOmschrijving = value;
                    IsModified = true;
                    NotifiyPropertyChanged(nameof(ProductOmschrijvingText));
                }
            }
        }
        public string SerieNummerText
        {
            get
            {
                return serieNummer;
            }
            set
            {
                if (serieNummer != value)
                {
                    serieNummer = value;
                    IsModified = true;
                    NotifiyPropertyChanged(nameof(SerieNummerText));
                }
            }
        }
        public int? ToestelId
        {
            get
            {
                return toestel_id;
            }
            set
            {
                if (toestel_id != value)
                {
                    toestel_id = value;
                    IsModified = true;
                    NotifiyPropertyChanged(nameof(ToestelId));
                }
            }
        }
        public int? MerkId
        {
            get
            {
                return merk_id;
            }
            set
            {
                if (merk_id != value)
                {
                    merk_id = value;
                    IsModified = true;
                    NotifiyPropertyChanged(nameof(MerkId));
                }
            }
        }
        public int? LocatieId
        {
            get
            {
                return locatie_id;
            }
            set
            {
                if (locatie_id != value)
                {
                    locatie_id = value;
                    IsModified = true;
                    NotifiyPropertyChanged(nameof(LocatieId));
                    NotifiyPropertyChanged(nameof(LocatiePath));
                }
            }
        }
        public int? LeverancierId
        {
            get
            {
                return leverancier_id;
            }
            set
            {
                if (leverancier_id != value)
                {
                    leverancier_id = value;
                    IsModified = true;
                    NotifiyPropertyChanged(nameof(LeverancierId));
                }
            }
        }
        public string ProductCodeText
        {
            get
            {
                return productCode;
            }
            set
            {
                if (productCode != value)
                {
                    productCode = value;
                    IsModified = true;
                    NotifiyPropertyChanged(nameof(ProductCodeText));
                }
            }
        }
        public string BestelbonText
        {
            get
            {
                return bestelbon;
            }
            set
            {
                if (bestelbon != value)
                {
                    bestelbon = value;
                    IsModified = true;
                    NotifiyPropertyChanged(nameof(BestelbonText));
                }
            }
        }
        public string LabelText
        {
            get
            {
                return label;
            }
            set
            {
                if (label != value)
                {
                    label = value;
                    IsModified = true;
                    NotifiyPropertyChanged(nameof(LabelText));
                }
            }
        }
        public string OpmerkingText
        {
            get
            {
                return opmerking;
            }
            set
            {
                if (opmerking != value)
                {
                    opmerking = value;
                    IsModified = true;
                    NotifiyPropertyChanged(nameof(OpmerkingText));
                }
            }
        }
        public DateTime? UitDienstDatum
        {
            get
            {
                return uitDienst;
            }
            set
            {
                if (uitDienst != value)
                {
                    uitDienst = value;
                    IsModified = true;
                    NotifiyPropertyChanged(nameof(UitDienstDatum));
                }
            }
        }
        public DateTime? AankoopDatum
        {
            get
            {
                return aankoopDatum;
            }
            set
            {
                if (aankoopDatum != value)
                {
                    aankoopDatum = value;
                    IsModified = true;
                    NotifiyPropertyChanged(nameof(AankoopDatum));
                }
            }
        }

        // infoIT-tabel weergave velden
        public string MacAdresText
        {
            get
            {
                if (infoIT != null)
                    return infoIT.macAdres;
                else return null;
            }
            set
            {
                if (infoIT.macAdres != value)
                {
                    infoIT.macAdres = value;
                    IsModified = true;
                    NotifiyPropertyChanged(nameof(MacAdresText));
                }
            }
        }
        public string IpAdresText
        {
            get
            {
                if (infoIT != null)
                    return infoIT.ipAdres;
                else return null;
            }
            set
            {
                if (infoIT.ipAdres != value)
                {
                    infoIT.ipAdres = value;
                    IsModified = true;
                    NotifiyPropertyChanged(nameof(IpAdresText));
                }
            }
        }
        public string DeviceLoginText
        {
            get
            {
                if (infoIT != null)
                    return infoIT.deviceLogin;
                else return null;
            }
            set
            {
                if (infoIT.deviceLogin != value)
                {
                    infoIT.deviceLogin = value;
                    IsModified = true;
                    NotifiyPropertyChanged(nameof(DeviceLoginText));
                }
            }
        }
        public string DeviceWachtwoordText
        {
            get
            {
                if (infoIT != null)
                    return infoIT.deviceWachtwoord;
                else return null;
            }
            set
            {
                if (infoIT.deviceWachtwoord != value)
                {
                    infoIT.deviceWachtwoord = value;
                    IsModified = true;
                    NotifiyPropertyChanged(nameof(DeviceWachtwoordText));
                }
            }
        }
        public int? DatarackId
        {
            get
            {
                if (infoIT != null)
                    return infoIT.datarack_id;
                else return null;
            }
            set
            {
                if (infoIT.datarack_id != value)
                {
                    if (value == -1)
                        infoIT.datarack_id = null;
                    else
                        infoIT.datarack_id = value;

                    IsModified = true;
                    NotifiyPropertyChanged(nameof(DatarackId));
                }
            }
        }
        public bool IpHasDHCP
        {
            get
            {
                if (infoIT != null)
                    return infoIT.ipHasDHCP;
                else return false;
            }
            set
            {
                if (infoIT.ipHasDHCP != value)
                {
                    infoIT.ipHasDHCP = value;
                    IsModified = true;
                    NotifiyPropertyChanged(nameof(IpHasDHCP));
                }
            }
        }
        public string PatchPaneelText
        {
            get
            {
                if (infoIT != null)
                    return infoIT.patchPaneel;
                else return null;
            }
            set
            {
                if (infoIT.patchPaneel != value)
                {
                    infoIT.patchPaneel = value;
                    IsModified = true;
                    NotifiyPropertyChanged(nameof(PatchPaneelText));
                }
            }
        }
        public string DeviceURLText
        {
            get
            {
                if (infoIT != null)
                    return infoIT.deviceURL;
                else return null;
            }
            set
            {
                if (infoIT.deviceURL != value)
                {
                    infoIT.deviceURL = value;
                    IsModified = true;
                    NotifiyPropertyChanged(nameof(DeviceURLText));
                }
            }
        }
        public string SwitchpoortText
        {
            get
            {
                if (infoIT != null)
                    return infoIT.switchPoort;
                else return null;
            }
            set
            {
                if (infoIT.switchPoort != value)
                {
                    infoIT.switchPoort = value;
                    IsModified = true;
                    NotifiyPropertyChanged(nameof(SwitchpoortText));
                }
            }
        }
        public string SwitchIpadresText
        {
            get
            {
                if (infoIT != null)
                    return infoIT.switchIpadres;
                else return null;
            }
            set
            {
                if (infoIT.switchIpadres != value)
                {
                    infoIT.switchIpadres = value;
                    IsModified = true;
                    NotifiyPropertyChanged(nameof(SwitchIpadresText));
                }
            }
        }
        public string WifiMacadresText
        {
            get
            {
                if (infoIT != null)
                    return infoIT.wifiMacadres;
                else return null;
            }
            set
            {
                if (infoIT.wifiMacadres != value)
                {
                    infoIT.wifiMacadres = value;
                    IsModified = true;
                    NotifiyPropertyChanged(nameof(WifiMacadresText));
                }
            }
        }
        public int? VlanText
        {
            get
            {
                if (infoIT != null)
                    return infoIT.vlan;
                else return null;
            }
            set
            {
                if (infoIT.vlan != value)
                {
                    infoIT.vlan = value;
                    IsModified = true;
                    NotifiyPropertyChanged(nameof(VlanText));
                }
            }
        }

        // extra info velden & opties
        private int? aantalbijlagen;
        public int? Aantalbijlagen
        {
            get
            {
                return aantalbijlagen;
            }

            set
            {

                if (aantalbijlagen != value.GetValueOrDefault())
                {
                    aantalbijlagen = value;
                    NotifiyPropertyChanged(nameof(Aantalbijlagen));
                }
            }
        }
        public bool DoNotify { get; set; }
        private bool _isModified;
        public bool IsModified
        {
            get { return _isModified; }
            set
            {
                if (_isModified != value)
                {
                    _isModified = value;
                    NotifiyPropertyChanged(nameof(IsModified));
                    NotifiyPropertyChanged(nameof(IsGeinventariseerd));
                }
            }
        }
        /// <summary>
        /// De boekhouditems komen uit de Boekhoudtabel uit Orbis
        /// Wanneer onze inventaris-tabel een id heeft, is het item geinventariseerd
        /// item is pas volledig geinventariseerd wanneer min. Locatie,Merk, Toestel en Leverancier zijn ingevuld.
        /// </summary>
        public bool IsGeinventariseerd
        {
            get
            {
                return id != 0 && locatie_id.HasValue && merk_id.HasValue && leverancier_id.HasValue && toestel_id.HasValue;
            }
        }
        public string StatusText
        {
            get
            {
                return StatusType.ToString();
            }
        }
        private string _locatiePath;
        public string LocatiePath
        {
            get { return _locatiePath; }
            set
            {
                if (_locatiePath != value)
                {
                    _locatiePath = value;
                    IsModified = true;
                    NotifiyPropertyChanged(nameof(LocatiePath));
                }
            }
        }
        public InventarisStatusType StatusType { get; set; }

        /** METHODS */
        public void NotifiyPropertyChanged(string property)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(property));
        }
        public void SetStatus(string code)
        {
            switch (code)
            {
                case "A":
                    StatusType = InventarisStatusType.Actief;
                    break;
                case "S":
                    StatusType = InventarisStatusType.Verkocht;
                    break;
                case "W":
                    StatusType = InventarisStatusType.Afgeboekt;
                    break;
                case "C":
                    StatusType = InventarisStatusType.Afgeschreven;
                    break;
            }
            status = code;
        }
    }
    public class LocatieobjectWeergave
    {
        public int Id { get; set; }
        public string Naam { get; set; }
        public string Opmerking { get; set; }
        public string FullLocatiePath { get; set; }
        public string Hoeveelheid { get; set; }
        public string Eigenaar { get; set; }
    }
    public class InventarisPrintItem
    {
        public string Nummer { get; set; }
        public string Omschrijving { get; set; }
        public string Leverancier { get; set; }
        public string Merk { get; set; }
        public string Toestel { get; set; }
        public string Categorie { get; set; }
        public string LocatiePath { get; set; }
    }
    /// <summary>
    /// Item enkel voor keuzelijsten of overzicht zonder verdere functionaliteit
    /// </summary>
    public class InventarisOverviewItem
    {
        public int? Id { get; set; } = null;
        public string Nummer { get; set; }
        public string Naam { get; set; }
        public string Leverancier { get; set; }
        public string Merk { get; set; }
        public string Toestel { get; set; }
        public DateTime AankoopDatum { get; set; }
    }
    public class LeverancierItem
    {
        public int Id { get; set; }
        public string Naam { get; set; }

        public leverancier ConvertToLeverancierDB()
        {
            return new leverancier()
            {
                lev_id = Id,
                lev_naam = Naam
            };
        }
    }
}