﻿using sharedcode.DatabaseParsers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sharedcode.WeergaveClasses
{
    public class OrbisadresWeergave
    {
        [DatabaseField("Straat")]
        public string Straat { get; set; }
        [DatabaseField("Nummer")]
        public string Huisnummer { get; set; }
        [DatabaseField("Bus")]
        public string Busnummer { get; set; }
        [DatabaseField("Postcode")]
        public string Postcode { get; set; }
        [DatabaseField("GemeenteNaam")]
        public string Woonplaats { get; set; }
        [DatabaseField("Land")]
        public string Land { get; set; }
    }
    public class DomicilieadresWeergave: OrbisadresWeergave
    {
        // Id FROM OrbisNet.Adressen.RelatieType TABLE
        public static string RelatietypeIds { get { return "24"; } }
        // Id FROM OrbisNet.Adressen.ContactGegevensType TABLE
        public static string ContacttypeIds { get { return "73"; } }
        [DatabaseField("Voornaam")]
        public string Voornaam { get; set; }
        [DatabaseField("Naam")]
        public string Naam { get; set; }
        [DatabaseField("Rijksregisternummer")]
        public string Rijksregisternummer { get; set; }
    }
    public class BewindvoerderadresWeergave : OrbisadresWeergave
    {
        // Id FROM OrbisNet.Adressen.RelatieType TABLE
        public static string RelatietypeIds { get { return "9"; } }
        // Id FROM OrbisNet.Adressen.ContactGegevensType TABLE
        public static string ContacttypeIds { get { return "7"; } }
        [DatabaseField("NaamBewindvoerder")]
        public string Naam { get; set; }
        [DatabaseField("JuridischStatuut")]
        public string Type { get; set; }
    }
    public class OuderadresWeergave: OrbisadresWeergave
    {
        // Id FROM OrbisNet.Adressen.RelatieType TABLE
        public static string RelatietypeIds { get { return "1,2,13,14,15,39,48"; } }
        // Id FROM OrbisNet.Adressen.ContactGegevensType TABLE
        public static string ContacttypeIds { get { return "3,4,5"; } }
        [DatabaseField("Omschrijving")]
        public string Naam { get; set; }
    }
    public class FacturatieadresWeergave: OrbisadresWeergave
    {
        // Id FROM OrbisNet.Adressen.RelatieType TABLE
        public static string RelatietypeIds { get { return "12,45"; } }
        // Id FROM OrbisNet.Adressen.ContactGegevensType TABLE
        public static string ContacttypeIds { get { return "74"; } }
        [DatabaseField("Omschrijving")]
        public string Naam { get; set; }
        [DatabaseField("Rijksregisternummer")]
        public string Rijksregisternummer { get; set; }
        [DatabaseField("EmailAdres")]
        public string Email { get; set; }
    }
    public class IbanFacturatieAdresWeergave
    {
        [DatabaseField("iban")]
        public string Ibannummer { get; set; }
    }
    public class KinderbijslagadresWeergave: OrbisadresWeergave
    {
        // Id FROM OrbisNet.Adressen.RelatieType TABLE
        public static string RelatietypeIds { get { return "27"; } }
        // Id FROM OrbisNet.Adressen.ContactGegevensType TABLE
        public static string ContacttypeIds { get { return "14"; } }
        [DatabaseField("Omschrijving")]
        public string Naam { get; set; }
    }

    public class ToestemmingenWeergave
    {
        [DatabaseField("beeldmateriaalExterneCommunicatie")]
        public bool BeeldmateriaalExterneCommunicatie { get; set; }
        [DatabaseField("beeldmateriaalActiviteitenClaraFey")]
        public bool BeeldmateriaalActiviteitenClaraFey { get; set; }
    }
    public class BWaardeWeergave
    {
        [DatabaseField("BWaarde")]
        public string BWaarde { get; set; }
    }
    public class PWaardeWeergave
    {
        [DatabaseField("PWaarde")]
        public string PWaarde { get; set; }
    }
    public class PunterPerJaarWeergave
    {
        [DatabaseField("PuntenEnData")]
        public string LijstPuntenEnPeriode { get; set; }
    }
}
