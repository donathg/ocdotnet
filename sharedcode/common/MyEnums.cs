﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sharedcode.CommonObjects
{
    public enum itemChangedMode
    {
        none,
        modified,
        added,
        deleted

    }
    public enum HistoryType
    {
        active,
        history,
        both

    }
}
