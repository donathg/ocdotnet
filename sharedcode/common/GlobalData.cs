﻿using sharedcode.BLL;
using sharedcode.CommonObjects;
using System;
using System.Data.Entity.Core.EntityClient;
using System.Data.SqlClient;
using System.Threading;


namespace sharedcode.common
{
    public enum ClaraFeyDatabase
    {
        produktie,
        test
    }

    public sealed class GlobalData
    {
        /** FIELDS */
#if DEBUG
        public String BlazorAppUrl { get; set; } = "http://localhost:50651";
#else
    
       public  String BlazorAppUrl {get;set;} = "http://webapp.clarafey.fracarita.org";
#endif
        public GlobalShutDownTimer GlobalShutDownTimer { get; set; }
        public string Entity { get; private set; }
        private static Mutex M1 = new Mutex(false);
        private const String ConnectStringClaraFeyProduktie = "Data Source=10.118.0.10;Initial Catalog=ClaraFey;Integrated Security=false;User Id=ClaraFeyApp;Password=Brecht2960$;";
        // private const String ConnectStringClaraFeyTest = "Data Source=10.118.0.10;Initial Catalog=ClaraFey_DEV;Integrated Security=false;User Id=ClaraFeyApp;Password=Brecht2960$;";
        private const String ConnectStringClaraFeyTest = "Data Source = (localdb)\\MyLocalDB; Initial Catalog = ClaraFeyLocalDB; Integrated Security = true";
        private const String EntityProduktie = "ClaraFeyEntities"; //zie App.config , voor entity framework
        private const String EntityTest = "ClaraFeyTestEntities"; //zie App.config , voor entity framework
        public String ApplicationVersion = "3.0.7";
        public LoggedOnUser LoggedOnUser { get; set; }
        public string GetOrbisSchema
        {
            get
            {
                return "262";
            }
        }
        private static volatile GlobalData _instance;
        /// <summary>
        /// Singleton implementation.
        /// Advantage : data is accessible from any thread.
        /// </summary>
        public static GlobalData Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (syncRoot)
                    {
                        if (_instance == null)
                            _instance = new GlobalData();
                    }
                }
                return _instance;
            }
        }
        private static object syncRoot = new object();
        public string ConnectionStringEF { get; set; } = "";
        public string ConnectionStringADONET { get; set; } = "";

        /** CONSTRUCTORS */
        private GlobalData()
        {
            LoggedOnUser = new LoggedOnUser();
            GlobalShutDownTimer = new GlobalShutDownTimer();
        }

        /** METHODS */
        /// <summary>
        /// WEB
        /// </summary>
        /// <param name="connectString"></param>
        public void CreateConnectionStringEF(string connectString)
        {
            ConnectionStringEF = connectString;
        }
        public void SetConnectionStringEF(string serverName, string databaseName, string userId, string password, bool IntegratedSecurity, bool TrustServerCertificate, bool Encrypt)
        {
            // Set the properties for the data source.
            SqlConnectionStringBuilder sqlBuilder = new SqlConnectionStringBuilder
            {
                DataSource = serverName,
                InitialCatalog = databaseName,
                Password = password,
                UserID = userId,
                IntegratedSecurity = IntegratedSecurity,
                Encrypt = Encrypt,
                TrustServerCertificate = TrustServerCertificate
            };

            // Initialize the EntityConnectionStringBuilder.
            EntityConnectionStringBuilder entityBuilder = new EntityConnectionStringBuilder
            {
                //Set the provider name.
                Provider = "System.Data.SqlClient",
                // Build the SqlConnection connection string + Set the provider-specific connection string.
                ProviderConnectionString = sqlBuilder.ToString(),
                // Set the Metadata location.
                Metadata = @"res://*/models.Model1.csdl|res://*/models.Model1.ssdl|res://*/models.Model1.msl"
            };

            ConnectionStringEF = entityBuilder.ConnectionString;
        }
        public void ChangeDataBase(ClaraFeyDatabase db)
        {
            switch (db)
            {
                case ClaraFeyDatabase.produktie:
                    ConnectionStringADONET = ConnectStringClaraFeyProduktie;
                    Entity = EntityProduktie;
                    break;

                case ClaraFeyDatabase.test:
                    ConnectionStringADONET = ConnectStringClaraFeyTest;
                    Entity = EntityTest;
                    break;
            }
        }
        public ClaraFeyDatabase GetConnectedDatabaseType()
        {
            if (ConnectionStringADONET == ConnectStringClaraFeyProduktie)
                return ClaraFeyDatabase.produktie;
            else
                return ClaraFeyDatabase.test;
        }
    }
}
