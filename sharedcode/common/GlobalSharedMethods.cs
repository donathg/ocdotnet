﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace sharedcode.common
{
    public class GlobalSharedMethods
    {

        public static T DeepClone<T>(T obj)
        {
            using (var ms = new MemoryStream())
            {
                var formatter = new BinaryFormatter();
                formatter.Serialize(ms, obj);
                ms.Position = 0;

                return (T)formatter.Deserialize(ms);
            }
        }
        public static Byte[] JpgFileToByteArray(String path)
        {
            if (String.IsNullOrWhiteSpace(path) == false)
            {
                Uri uri = new Uri(path);
                BitmapFrame f = BitmapFrame.Create(uri);
                if (f != null)
                {
                    JpegBitmapEncoder encoder = new JpegBitmapEncoder()
                    {
                        QualityLevel = 100
                    };
                    using (MemoryStream stream = new MemoryStream())
                    {
                        encoder.Frames.Add(BitmapFrame.Create(f));
                        encoder.Save(stream);
                        byte[] bit = stream.ToArray();
                        stream.Close();
                        return bit;
                    }
                }
            }
            return null;
        }

        internal static string DateTimeToIntelligentText(DateTime creationdate)
        {
            const int SECOND = 1;
            const int MINUTE = 60 * SECOND;
            const int HOUR = 60 * MINUTE;
          

            var ts = new TimeSpan(DateTime.Now.Ticks - creationdate.Ticks);
            double delta = Math.Abs(ts.TotalSeconds);

            if (delta < 1 * MINUTE)
                return ts.Seconds == 1 ? "1 seconde geleden" : ts.Seconds + " seconden geleden";

            if (delta < 2 * MINUTE)
                return "1 minuut geleden";

            if (delta < 45 * MINUTE)
                return ts.Minutes + " minuten geleden";

            if (delta < 90 * MINUTE)
                return "een uur geleden";

            if (delta < 24 * HOUR)
                return ts.Hours + " uren geleden";

            if (delta < 48 * HOUR)
                return "gisteren";

            if (delta < 96 * HOUR)
                return "eergisteren";


            return creationdate.ToShortDateString();
        }

        public static string GetDownloadPath(String subFolder)
        {
            String path = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            path = path + @"\ClaraFeyApplicatie\" + subFolder;
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            return path;
        }

        public static byte[] getJPGFromImageControl(BitmapImage imageC)
        {
            MemoryStream memStream = new MemoryStream();
            JpegBitmapEncoder encoder = new JpegBitmapEncoder();
            encoder.Frames.Add(BitmapFrame.Create(imageC));
            encoder.Save(memStream);
            return memStream.ToArray();
        }
        public static BitmapImage DecodePhoto(byte[] byteVal)
        {
            if (byteVal == null) return null;
            MemoryStream strmImg = new MemoryStream(byteVal);
            BitmapImage myBitmapImage = new BitmapImage();
            myBitmapImage.BeginInit();
            myBitmapImage.StreamSource = strmImg;
            myBitmapImage.EndInit();
            return myBitmapImage;
        }



        public static ImageSource ByteToImage(byte[] imageData)
        {
            BitmapImage biImg = new BitmapImage();
            MemoryStream ms = new MemoryStream(imageData);
            biImg.BeginInit();
            biImg.StreamSource = ms;
            biImg.EndInit();

            ImageSource imgSrc = biImg as ImageSource;

            return imgSrc;
        }

        public static Byte[] ConvertFileToByteArray(String filePath)
        {
            FileInfo fi = new FileInfo(filePath);
            FileStream fs = new FileStream(fi.FullName, FileMode.Open, FileAccess.Read);
            BinaryReader rdr = new BinaryReader(fs);
            byte[] fileData = rdr.ReadBytes((int)fs.Length);
            rdr.Close();
            fs.Close();
            return fileData;


        }
        public static List<string> GetAllProperties(object obj)
        {
            List<string> propertiesList = new List<string>();
            Type typeSource = obj.GetType();
            PropertyInfo[] propertyInfo = typeSource.GetProperties(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
            foreach (PropertyInfo property in propertyInfo)
            {
                propertiesList.Add(property.Name);
            }
            return propertiesList;
        }
        public static void CopyPublicProperties(object objSource, object objTarget)
        {
            //step : 1 Get the type of source object and create a new instance of that type
            Type typeSource = objSource.GetType();
            //Step2 : Get all the properties of source object type
            PropertyInfo[] propertyInfo = typeSource.GetProperties(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
            //Step : 3 Assign all source property to taget object 's properties
            foreach (PropertyInfo property in propertyInfo)
            {
                //Check whether property can be written to
                if (property.CanWrite)
                {
                    //Step : 4 check whether property type is value type, enum or string type
                    if (property.PropertyType.IsValueType || property.PropertyType.IsEnum || property.PropertyType.Equals(typeof(String)))
                    {
                        property.SetValue(objTarget, property.GetValue(objSource, null), null);
                    }

                    //else property type is object/complex types, so need to recursively call this method until the end of the tree is reached
                    /* else
                       {
                           object objPropertyValue = property.GetValue(objSource, null);
                           if (objPropertyValue == null)
                           {
                               property.SetValue(objTarget, null, null);
                           }
                           else
                           {
                               CloneObject(objPropertyValue, objTarget);
                               property.SetValue(objTarget, property.GetValue(objSource, null), null);
                           }
                       } */

                }

            }
        }
        public static T CreateItem<T>(DataRow row)
        {
            T obj = default(T);
            if (row != null)
            {
                obj = Activator.CreateInstance<T>();

                foreach (DataColumn column in row.Table.Columns)
                {
                    PropertyInfo prop = obj.GetType().GetProperty(column.ColumnName);
                    try
                    {
                        if (prop != null && row[column] != DBNull.Value)
                        {
                        
                        
                        object value = row[column.ColumnName];
                        prop.SetValue(obj, value, null);
                        }
                    }
                    catch
                    {
                        // You can log something here
                        throw;
                    }
                }
            }

            return obj;
        }
        public static int BooleantoBitConverter(bool value)
        {
            int bitValue = 0;
            if (value)
                bitValue = 1;

            return bitValue;
        }

        public static String EncodeStringToBase64 (String s)
        {
            byte[] encodedBytes = System.Text.Encoding.Unicode.GetBytes(s);
          return Convert.ToBase64String(encodedBytes);

        }
        public static String DecodeBase64ToString(String s)
        {
            byte[] decodedBytes = Convert.FromBase64String(s);
            string decodedTxt = System.Text.Encoding.UTF8.GetString(decodedBytes);
            return  System.Text.Encoding.Unicode.GetString(decodedBytes);
            
        }
    }
}
