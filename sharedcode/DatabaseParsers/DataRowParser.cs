﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Reflection;
using System.Linq;

namespace sharedcode.DatabaseParsers
{
    public static class DatabaseParser
    {
        public static List<T> DataTableParse<T>(this DataTable dt) where T : new()
        {
            List<T> list_of_t = new List<T>();
            foreach (DataRow row in dt.Rows)
            {
                T t = new T();
                ((Object)t).DataRowParse(row);
                list_of_t.Add(t);
            }
            return list_of_t;
        }
        public static void DataRowParse(this object oa, DataRow r)
        {
            // Use reflection to fill the ReportOA object, 
            // for this the Names in the Datarow should match the names in the ReportOA object !
            Type testType = null;
            bool nullable = false;


            PropertyInfo[] properties = oa.GetType().GetProperties();
            foreach (PropertyInfo pi in properties)
            {
                if (!ContainsDatabaseFieldAttribute(pi)) continue;          // Only load database fields !

                string dataBaseFieldName = GetRealDatabaseFieldName(pi).ToUpper();
                if (dataBaseFieldName == "") dataBaseFieldName = pi.Name.ToUpper();

                if (!r.Table.Columns.Contains(dataBaseFieldName)) continue; // only fill fields if they exist in the datarow !


                // Debug Entry in case of property conversion problems
                //if (dataBaseFieldName == "PGV_AKTIVE") Debugger.Break();

                try
                {
                    // GetUnderlyingType is returning null when the type is not Nullable<>
                    if (Nullable.GetUnderlyingType(pi.PropertyType) == null)
                    {
                        nullable = false;
                        testType = pi.PropertyType;
                    }
                    else
                    {
                        nullable = true;
                        testType = Nullable.GetUnderlyingType(pi.PropertyType);
                    }



                    // Debug :  Here you can check the type of the current entry
                    // var x = r[dataBaseFieldName];

                    switch (Type.GetTypeCode(testType))
                    {
                        // Add to this case if you need other types to be parsed.
                        case TypeCode.Int16:
                        case TypeCode.Int32:
                        case TypeCode.Int64:
                            if (nullable)
                            {
                                object typeTest = r[dataBaseFieldName];
                                if (typeTest.GetType().Name.ToLower() == "dbnull")
                                {
                                    SetPropertyValue(oa, pi.Name, null);
                                }
                                else
                                {
                                    if (typeTest.GetType().Name.ToLower() == "decimal")
                                    {

                                        SetPropertyValue(oa, pi.Name, (int?)(decimal?)r[dataBaseFieldName]);
                                    }
                                    else
                                    {
                                        SetPropertyValue(oa, pi.Name, (int?)r[dataBaseFieldName]);
                                    }
                                }
                            }
                            else
                            {
                                object typeTest = r[dataBaseFieldName];
                                if (typeTest.GetType().Name.ToLower() == "decimal")
                                {
                                    SetPropertyValue(oa, pi.Name, (int)(decimal)r[dataBaseFieldName]);
                                }
                                else
                                {
                                    SetPropertyValue(oa, pi.Name, (int)r[dataBaseFieldName]);
                                }
                            }
                            break;
                        case TypeCode.String:
                            SetPropertyValue(oa, pi.Name, r[dataBaseFieldName].ToString());
                            break;
                        case TypeCode.DateTime:
                            if (nullable)
                            {
                                object typeTest = r[dataBaseFieldName];
                                if (typeTest.GetType().Name.ToLower() == "dbnull")
                                {
                                    SetPropertyValue(oa, pi.Name, null);
                                }
                                else
                                {
                                    SetPropertyValue(oa, pi.Name, (DateTime?)r[dataBaseFieldName]);
                                }
                            }
                            else
                            {
                                SetPropertyValue(oa, pi.Name, (DateTime)r[dataBaseFieldName]);
                            }
                            break;
                        case TypeCode.Decimal:

                            if (nullable)
                            {
                                object typeTest = r[dataBaseFieldName];
                                if (typeTest.GetType().Name.ToLower() == "dbnull")
                                {
                                    SetPropertyValue(oa, pi.Name, null);
                                }
                                else
                                {
                                    var databasevalue = r[dataBaseFieldName];
                                    if (databasevalue == null)
                                    {
                                        SetPropertyValue(oa, pi.Name, null);
                                    }
                                    else
                                    {
                                        SetPropertyValue(oa, pi.Name, GetDecimalValue(databasevalue));
                                    }
                                }
                            }
                            else
                            {
                                SetPropertyValue(oa, pi.Name, GetDecimalValue(r[dataBaseFieldName]));
                            }
                            break;

                        case TypeCode.Double:

                            if (nullable)
                            {
                                object typeTest = r[dataBaseFieldName];
                                if (typeTest.GetType().Name.ToLower() == "dbnull")
                                {
                                    SetPropertyValue(oa, pi.Name, null);
                                }
                                else
                                {
                                    var databasevalue = r[dataBaseFieldName];
                                    if (databasevalue == null)
                                    {
                                        SetPropertyValue(oa, pi.Name, null);
                                    }
                                    else
                                    {
                                        SetPropertyValue(oa, pi.Name, GetDoubleValue(databasevalue));
                                    }
                                }
                            }
                            else
                            {
                                SetPropertyValue(oa, pi.Name, GetDecimalValue(r[dataBaseFieldName]));
                            }
                            break;
                        case TypeCode.Boolean:
                            SetPropertyValue(oa, pi.Name, r[dataBaseFieldName].ToString().Substring(0, 1).ToUpper() == "Y");
                            break;

                        default:
                            throw new Exception($"DataRowParse on {oa.GetType().ToString()} can not parse: {pi.Name} type {pi.PropertyType.ToString()} is not defined.");
                    }
                }
                catch (Exception e)
                {
                    throw new Exception($"DataRowParse on {oa.GetType().ToString()} can not parse: {pi.Name} error: {e.ToString()} ");
                }
            }
        }
        public static void SetPropertyValue(this object obj, string propName, object value)
        {
            obj.GetType().GetProperty(propName).SetValue(obj, value, null);
        }
        public static object GetPropertyValue(this object obj, string propName)
        {
            return obj.GetType().GetProperty(propName).GetValue(obj, null);
        }
        public static Int64 GetLongValue(object obj)
        {
            if (obj is Int64)
                return (Int64)obj;
            if (obj is double)
                return (Int64)(double)obj;
            if (obj is short)
                return (Int64)(short)obj;
            if (obj is decimal)
                return (Int64)(decimal)obj;
            if (obj is float)
                return (Int64)(float)obj;
            if (obj is char)
                return (Int64)(char)obj;
            if (obj is string)
            {
                Int64 value;
                if (Int64.TryParse((string)obj, out value))
                {
                    return value;
                }
                else throw new ArgumentException("Do not know how to make an integer from " + (string)obj);
            }
            else throw new ArgumentException("Do not know how to make an integer from " + obj.GetType());
        }
        public static int GetIntValue(object obj)
        {
            if (obj is int)
                return (int)obj;
            if (obj is double)
                return (int)(double)obj;
            if (obj is short)
                return (int)(short)obj;
            if (obj is decimal)
                return (int)(decimal)obj;
            if (obj is float)
                return (int)(float)obj;
            if (obj is char)
                return (int)(char)obj;
            if (obj is string)
            {
                int value;
                if (int.TryParse((string)obj, out value))
                {
                    return value;
                }
                else throw new ArgumentException("Do not know how to make an integer from " + (string)obj);
            }
            else throw new ArgumentException("Do not know how to make an integer from " + obj.GetType());
        }
        public static decimal GetDecimalValue(object obj)
        {
            if (obj is byte)
                return (decimal)(byte)obj;
            if (obj is short)
                return (decimal)(short)obj;
            if (obj is int)
                return (decimal)(int)obj;
            if (obj is long)
                return (decimal)(long)obj;
            if (obj is Byte)
                return (decimal)(Byte)obj;
            if (obj is Int16)
                return (decimal)(Int16)obj;
            if (obj is Int32)
                return (decimal)(Int32)obj;
            if (obj is Int64)
                return (decimal)(Int64)obj;
            if (obj is float)
                return (decimal)(float)obj;
            if (obj is double)
                return (decimal)(double)obj;
            if (obj is Double)
                return (decimal)(Double)obj;
            if (obj is decimal)
                return (decimal)obj;
            if (obj is Decimal)
                return (Decimal)obj;


            if (obj is string || obj is String)
            {
                decimal value;
                if (decimal.TryParse((string)obj, out value))
                {
                    return value;
                }
                else throw new ArgumentException("Do not know how to make an decimal from " + (string)obj);
            }
            else throw new ArgumentException("Do not know how to make an decimal from " + obj.GetType());
        }
        public static double GetDoubleValue(object obj)
        {
            if (obj is byte)
                return (double)(byte)obj;
            if (obj is short)
                return (double)(short)obj;
            if (obj is int)
                return (double)(int)obj;
            if (obj is long)
                return (double)(long)obj;
            if (obj is Byte)
                return (double)(Byte)obj;
            if (obj is Int16)
                return (double)(Int16)obj;
            if (obj is Int32)
                return (double)(Int32)obj;
            if (obj is Int64)
                return (double)(Int64)obj;
            if (obj is float)
                return (double)(float)obj;
            if (obj is double)
                return (double)obj;
            if (obj is Double)
                return (double)obj;
            if (obj is decimal)
                return decimal.ToDouble((decimal)obj);
            if (obj is Decimal)
                return decimal.ToDouble((Decimal)obj);
            if (obj is string || obj is String)
            {
                double value;
                if (double.TryParse((string)obj, out value))
                {
                    return value;
                }
                else throw new ArgumentException("Do not know how to make a double from " + (string)obj);
            }
            else throw new ArgumentException("Do not know how to make a double from " + obj.GetType());
        }
        public static bool ContainsDatabaseFieldAttribute(PropertyInfo pi)
        {
            if (pi.GetCustomAttributes(false).Any(a => a.GetType() == typeof(DatabaseFieldAttribute)) == true)
                return true;
            else
                return false;
        }
        public static string GetRealDatabaseFieldName(PropertyInfo pi)
        {
            foreach (DatabaseFieldAttribute x in pi.GetCustomAttributes(typeof(DatabaseFieldAttribute), false))
            {
                if (!string.IsNullOrWhiteSpace(x.DatabaseFieldName)) return x.DatabaseFieldName;
            }
            return "";
        }
    }
    public class DatabaseFieldAttribute : Attribute
    {
        public DatabaseFieldAttribute(String name)
        {

            this.DatabaseFieldName = name;

        }
        public String DatabaseFieldName { get; set; }
    }
}
