﻿using sharedcode.BLL;
using sharedcode.common;
using sharedcode.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sharedcode.Caches
{
    public sealed class BewonersListCache: MyNotifyPropertyChanged
    {
        /** FIELDS */
        private static BewonersListCache _instance;
        public static BewonersListCache Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new BewonersListCache();

                return _instance;
            }
        }
        public List<bewoner> AllBewonersList { get; private set; }
 

        /** CONSTRUCTORS */
        private BewonersListCache()
        {
            AllBewonersList = BewonersDAL.SelectAllBewoners();
        }

        /** METHODS */
        public List<bewoner> GetMyBewoners(int usrId)
        {
            List<String> groepen = GlobalData.Instance.LoggedOnUser.GetUserFunctieGroepen();
            List<String> aanvaardeGroepen = new List<string>()
            {
                "COBJ",
                "CODS",
                "COFB",
                "COHC",
                "COHH",
                "COST",
                "COVD",
                "COVW",
                "DIRME",
                "KKDIR",
                "KKINF",
                "ORTH",
            };

            if (GlobalData.Instance.LoggedOnUser.HasAccess("1001") || GlobalData.Instance.LoggedOnUser.HasAccess("1002") || groepen.Any(x => aanvaardeGroepen.Contains(x.Trim())))
                return AllBewonersList;

            List<bewoner> listBewoners = new List<bewoner>();
            List<int> bewIds = BewonersDAL.SelectMyBewonerIds(usrId);
            foreach (bewoner b in AllBewonersList)
            {
                if (bewIds.Contains(b.id))
                    listBewoners.Add(b);
            }
            return listBewoners;
        }
        public bewoner GetBewoner(int bewonerId)
        {
            if (AllBewonersList != null)
                return AllBewonersList.Where(x => x.id == bewonerId).FirstOrDefault();
            return null;
        }
        public void SavePicture(bewoner_foto pic)
        {
            if (pic.foto.Count() > 300000)
                throw new Exception("Gelieve enkel foto's toe te voegen kleiner dan 100 Kilobytes");
            BewonersDAL.InsertPicture(pic);
        }
        public void DeletePicture(bewoner_foto pic, int bewonerId)
        {
            BewonersDAL.DeletePicture(pic);

            AllBewonersList.Where(x => x.id == bewonerId).SingleOrDefault().bewoner_foto.Clear();
        }
        public int GetNumberOfAttachements(int bewonerId, int labelId)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                String sql = @" SELECT count(*)
                                FROM cd_files AS f, cd_files_labels AS fl
                                WHERE f.id = fl.fileid
                                AND f.bewonerid = @bewonerId
                                AND fl.labelid = @labelId";
                List<DbParam> paramList = new List<DbParam>()
                {
                    new DbParam() { Name = "@bewonerId", Value = bewonerId, DbType = DbTypes.db_integer },
                    new DbParam() { Name = "@labelId", Value = labelId, DbType = DbTypes.db_integer }
                };
                DbCommand comm = new DbCommand(DbCommandType.crud, sql, paramList.ToArray());
                return (int)db.ExcecuteScalar(comm);
            }
        }
        public void EmptyCache()
        {
            AllBewonersList.Clear();
            AllBewonersList = null;
        }
    }
}
