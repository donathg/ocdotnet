﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using sharedcode.BLL;
using sharedcode.common;
using System.Data;
using System.Data.SqlClient;
using sharedcode.WeergaveClasses;

namespace sharedcode.DAL
{
    public class ContactverslagDAL
    {
        #region Contactverslag_SoortBegeleiding Methods
        private List<Contactverslag_SoortBegeleiding> ConvertDatatableToContactverslagSoortBegeleiding(DataTable dt)
        {
            List<Contactverslag_SoortBegeleiding> tempList = new List<Contactverslag_SoortBegeleiding>();
            foreach (DataRow row in dt.Rows)
            {
                Contactverslag_SoortBegeleiding soortBegeleiding = new Contactverslag_SoortBegeleiding()
                {
                    id = (int)row["id"],
                    naam = row["naam"].ToString(),
                    descr = row["descr"].ToString(),
                    active = (Boolean)row["active"]
                };
                if (row["descr"].ToString() is String descr)
                    soortBegeleiding.descr = descr;
                tempList.Add(soortBegeleiding);
            }
            return tempList;
        }

        /// <summary>
        /// Get all Contactverslag_SoortBegeleiding
        /// </summary>
        /// <param name="onlyActive">
        /// true: gets only active Contactverslag_SoortBegeleiding.
        /// false: gets active and inactive Contactverslag_SoortBegeleiding.
        /// </param>
        /// <returns></returns>
        public List<Contactverslag_SoortBegeleiding> SelectAllSoortBegeleidingen(Boolean onlyActive)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                String sql = @"SELECT * FROM Contactverslag_SoortBegeleiding";
                if (onlyActive)
                    sql += @" WHERE active = 1";

                DataTable dt = db.GetDataTable(sql);

                return ConvertDatatableToContactverslagSoortBegeleiding(dt);
            }
        }
        public Contactverslag_SoortBegeleiding SelectSoortBegeleidingFromId(int id)
        {
            using (DatabaseCentral db  = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                List<DbParam> paramList = new List<DbParam>()
                {
                    new DbParam() { Name = "@id", Value = id, DbType = DbTypes.db_integer }
                };
                String sql = @"SELECT * FROM Contactverslag_SoortBegeleiding
                               WHERE id=@id";

                DataTable dt = db.GetDataTable(sql, paramList.ToArray());

                return ConvertDatatableToContactverslagSoortBegeleiding(dt).SingleOrDefault();
            }
        }
        public void UpdateSoortBegeleiding(Contactverslag_SoortBegeleiding soortBegeleiding)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                List<DbParam> paramList = new List<DbParam>()
                {
                    new DbParam() { Name = "@id", Value = soortBegeleiding.id, DbType = DbTypes.db_integer },
                    new DbParam() { Name = "@naam", Value = soortBegeleiding.naam, DbType = DbTypes.db_string },
                    new DbParam() { Name = "@descr", Value = soortBegeleiding.descr, DbType = DbTypes.db_string },
                    new DbParam() { Name = "@active", Value = soortBegeleiding.active.CompareTo(true), DbType = DbTypes.db_integer }
                };
                String sql = @"UPDATE Contactverslag_SoortBegeleiding
                               SET active=@active, naam=@naam, descr=@descr
                               WHERE id=@id";

                DbCommand comm = new DbCommand(DbCommandType.crud, sql, paramList.ToArray());

                db.Excecute(comm);
            }
        }
        public void InsertSoortBegeleiding(Contactverslag_SoortBegeleiding soortBegeleiding)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                List<DbParam> paramList = new List<DbParam>()
                {
                    new DbParam() { Name = "@naam", Value = soortBegeleiding.naam, DbType = DbTypes.db_string },
                    new DbParam() { Name = "@descr", Value = soortBegeleiding.descr, DbType = DbTypes.db_string },
                    new DbParam() { Name = "@active", Value = soortBegeleiding.active, DbType = DbTypes.db_string },
                };
                String sql = @"INSERT INTO Contactverslag_SoortBegeleiding (naam, descr, active)
                               VALUES (@naam, @descr, @ctive)";

                DbCommand comm = new DbCommand(DbCommandType.crud, sql, paramList.ToArray());

                db.Excecute(comm);
            }
        }
        #endregion
        #region Contactverslag Methods
        private List<ContactverslagWeergave> ConvertDatatableToContactverslagWeergave(DataTable dt)
        {
            List<ContactverslagWeergave> tempList = new List<ContactverslagWeergave>();
            foreach (DataRow row in dt.Rows)
            {
                // Checks if tempList contains this ContactverslagWeergave
                if (tempList.Find(x => x.ContactverslagId == (int)row["id"]) is ContactverslagWeergave weergave)
                    weergave.ContactverslagLabels.Add(LabelsDAL.CreateNewLabelFromDataRow(row));
                else
                    tempList.Add(CreateNewContactverslagWeergaveFromDataRow(row));
            }
            tempList.ForEach(x => x.InitLabelsKommaSeperated());

            return tempList;
        }
        private ContactverslagWeergave CreateNewContactverslagWeergaveFromDataRow(DataRow row)
        {
            ContactverslagWeergave weergave = new ContactverslagWeergave(
                new Contactverslag()
                {
                    id = (int)row["id"],

                    bewoner_id = (int)row["bewoner_id"],
                    soortBegeleiding_id = (int)row["soortBegeleiding_id"],

                    afspraken = row["afspraken"].ToString(),
                    aanwezigen = row["aanwezigen"].ToString(),
                    datum = (DateTime)row["datum"],
                    creator = (int)row["creator"],
                    creation_date = (DateTime)row["creation_date"],
                    modifier = (int)row["modifier"],
                    modification_date = (DateTime)row["modification_date"],
                    verslag = row["verslag"].ToString(),
                    title = row["title"].ToString()
                })
            {
                CreationdateIntelligentText = row["username"].ToString() + " " + ((DateTime)row["datum"]).ToShortDateString()
            };

            weergave.ContactverslagLabels.Add(LabelsDAL.CreateNewLabelFromDataRow(row));

            return weergave;
        }

        public List<ContactverslagWeergave> SelectAllContactverslagen(ContactverslagFilter filter)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                try
                {
                    List<DbParam> paramlist = new List<DbParam>
                    {
                        new DbParam() { Name = "@vanDatum", Value = filter.VanDatum, DbType = DbTypes.db_datetime },
                        new DbParam() { Name = "@totDatum", Value = filter.TotDatum, DbType = DbTypes.db_datetime },
                        new DbParam() { Name = "@bewonerid", Value = filter.GekozenBewoner.id, DbType = DbTypes.db_integer }
                    };
                    string sql = @"SELECT DISTINCT abc.*, l.id as label_id, l.parentid as label_parentid, l.name as label_name, l.descr as label_descr, 
l.module as label_module, l.active as label_active, l.dagboek as label_dagboek, l.bijlagen as label_bijlagen, l.deadlinedate as label_deadlinedate
                               FROM (
                                    SELECT c.*, (g.voornaam + ' ' + g.achternaam) AS username
                                    FROM Contactverslag AS c, gebruiker AS g
                                    WHERE c.active = 1 
                                    AND g.id = c.creator
                                    AND c.datum >= @vanDatum AND c.datum <= @totDatum
                                    AND c.bewoner_id = @bewonerid";
                    if (!String.IsNullOrWhiteSpace(filter.GekozenAfspraak))
                    {
                        sql += @"       AND c.afspraken LIKE @afspraken";
                        paramlist.Add(new DbParam() { Name = "@afspraken", Value = '%' + filter.GekozenAfspraak + '%', DbType = DbTypes.db_string });
                    }
                    if (filter.GekozenSoortBegeleiding != null && filter.GekozenSoortBegeleiding.id != 0)
                    {
                        sql += @"       AND c.soortBegeleiding_id = @soortbegeleidingid";
                        paramlist.Add(new DbParam() { Name = "@soortbegeleidingid", Value = filter.GekozenSoortBegeleiding.id, DbType = DbTypes.db_integer });
                    }
                    if (filter.GekozenGebruiker != null && filter.GekozenGebruiker.id != 0)
                    {
                        sql += @"       AND c.creator = @creatorId";
                        paramlist.Add(new DbParam() { Name = "@creatorId", Value = filter.GekozenGebruiker.id, DbType = DbTypes.db_integer });
                    }
                    sql += @"      ) AS abc, Contactverslag_Labels AS c_l, labels AS l
                               WHERE abc.id = c_l.contactverslag_id
                               AND c_l.label_id = l.id";
                    if (!string.IsNullOrEmpty(filter.GetLabelIds()))
                        sql += @" AND l.id IN(" + filter.GetLabelIds() + @")";
                    else
                        return new List<ContactverslagWeergave>();
                    sql += @"   ORDER BY abc.creation_date DESC";

                    DataTable dt = db.GetDataTable(sql, paramlist.ToArray());

                    return ConvertDatatableToContactverslagWeergave(dt);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
        public ContactverslagWeergave SelectContactverslagFromId(int id)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                List<DbParam> paramList = new List<DbParam>()
                {
                    new DbParam() { Name = "@id", Value = id, DbType = DbTypes.db_integer }
                };
                String sql = @"SELECT c.*, l.id AS label_id, l.name AS label_naam, l.descr AS label_descr, l.module AS label_module, l.active AS label_active,
l.dagboek AS label_dagboek, l.bijlagen AS label_bijlagen, l.deadlinedate AS label_deadlinedate, l.parentid AS label_parentid
                               FROM Contactverslag AS c, Contactverslag_Labels AS c_l, labels AS l
                               WHERE c.ative = 1
                               AND c.id = c_l.contactverslag_id
                               AND c_l.label_id = l.id
                               AND id=@id";

                DataTable dt = db.GetDataTable(sql, paramList.ToArray());

                return ConvertDatatableToContactverslagWeergave(dt).SingleOrDefault();
            }
        }
        public void InsertContactverslag(ContactverslagWeergave contactverslag)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                DateTime creationDate = DateTime.Now;
                int creatorId = GlobalData.Instance.LoggedOnUser.Id.Value;
                
                try
                {
                    db.BeginTransaction();

                    #region Add Contactverslag to DB
                    List<DbParam> paramListContactverslag = new List<DbParam>()
                    {
                        new DbParam() { Name = "@bewonerid", Value = contactverslag.Bewoner.id, DbType = DbTypes.db_integer },
                        new DbParam() { Name = "@soortBegeleidingid", Value = contactverslag.SoortBegeleiding.id, DbType = DbTypes.db_integer },
                        new DbParam() { Name = "@afspraken", Value = contactverslag.Afspraken, DbType = DbTypes.db_string },
                        new DbParam() { Name = "@aanwezigen", Value = contactverslag.Aanwezigen, DbType = DbTypes.db_string },
                        new DbParam() { Name = "@datum", Value = contactverslag.Date, DbType = DbTypes.db_date },
                        new DbParam() { Name = "@creator", Value = creatorId, DbType = DbTypes.db_integer },
                        new DbParam() { Name = "@creationdate", Value = creationDate, DbType = DbTypes.db_datetime },
                        new DbParam() { Name = "@modifier", Value = creatorId, DbType = DbTypes.db_integer },
                        new DbParam() { Name = "@modificationdate", Value = creationDate, DbType = DbTypes.db_datetime },
                        new DbParam() { Name = "@verslag", Value = contactverslag.Verslag, DbType = DbTypes.db_string },
                        new DbParam() { Name = "@title", Value = contactverslag.Title, DbType = DbTypes.db_string }
                    };
                    String sqlContactverslag = @"INSERT INTO Contactverslag (bewoner_id, soortBegeleiding_id, afspraken, aanwezigen, datum, creator, 
creation_date, modifier, modification_date, verslag, title)
                                                VALUES (@bewonerid, @soortBegeleidingid, @afspraken, @aanwezigen, @datum, @creator, @creationdate,
@modifier, @modificationdate, @verslag, @title)";

                    DbCommand commContactverslag = new DbCommand(DbCommandType.crud, sqlContactverslag, paramListContactverslag.ToArray());
                    #endregion
                    int contactverslagId = Convert.ToInt32(db.ExcecuteScalar(commContactverslag));
                    #region Add Labels To DB
                    List<DbCommand> commListLabels = new List<DbCommand>();
                    if (contactverslag.ContactverslagLabels.Count != 0)
                    {
                        foreach (labels label in contactverslag.ContactverslagLabels)
                        {
                            List<DbParam> paramListLabels = new List<DbParam>()
                            {
                                new DbParam() { Name = "@contactverslagid", Value = contactverslagId, DbType = DbTypes.db_integer },
                                new DbParam() { Name = "@labelid", Value = label.id, DbType = DbTypes.db_integer },
                                new DbParam() { Name = "@modifier", Value = creatorId, DbType = DbTypes.db_integer },
                                new DbParam() { Name = "@modificationdate", Value = creationDate, DbType = DbTypes.db_datetime }
                            };
                            String sqlLabel = @"INSERT INTO Contactverslag_Labels (contactverslag_id, label_id, modifier, modification_date) 
                                            VALUES (@contactverslagid, @labelid, @modifier, @modificationdate)";

                            commListLabels.Add(new DbCommand(DbCommandType.crud, sqlLabel, paramListLabels.ToArray()));
                        }
                    }
                    else
                        throw new Exception("Er zijn geen labels");
                    #endregion
                    db.Excecute(commListLabels);

                    db.CommitTransaction();
                }
                catch (Exception ex)
                {
                    db.RollbackTransaction();
                    throw ex;
                }
            }
        }
        public void UpdateContactverslag(ContactverslagWeergave contactverslag)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                List<DbParam> paramList = new List<DbParam>()
                {
                    new DbParam() { Name = "@id", Value = contactverslag.ContactverslagId, DbType=DbTypes.db_integer },
                    new DbParam() { Name = "@bewonerid", Value = contactverslag.Bewoner.id, DbType=DbTypes.db_integer },
                    new DbParam() { Name = "@soortbegeleidingid", Value = contactverslag.SoortBegeleiding.id, DbType=DbTypes.db_integer },
                    new DbParam() { Name = "@afspraken", Value = contactverslag.Afspraken, DbType=DbTypes.db_string },
                    new DbParam() { Name = "@aanwezigen", Value = contactverslag.Aanwezigen, DbType=DbTypes.db_string },
                    new DbParam() { Name = "@datum", Value = contactverslag.Date, DbType=DbTypes.db_date },
                    new DbParam() { Name = "@modifier", Value = GlobalData.Instance.LoggedOnUser.Id.Value, DbType=DbTypes.db_integer },
                    new DbParam() { Name = "@modificationdate", Value = DateTime.Today, DbType=DbTypes.db_datetime },
                    new DbParam() { Name = "@verslag", Value = contactverslag.Verslag, DbType = DbTypes.db_string },
                    new DbParam() { Name = "@title", Value = contactverslag.Title, DbType = DbTypes.db_string }
                };
                String sql = @"UPDATE Contactverslag
                               SET bewoner_id=@bewonerid, soortBegeleiding_id=@soortbegeleidingid, afspraken=@afspraken, 
aanwezigen=@aanwezigen, datum=@datum, modifier=@modifier, modification_date=@modificationdate, verslag=@verslag, title = @title
                               WHERE id=@id";

                DbCommand comm = new DbCommand(DbCommandType.crud, sql, paramList.ToArray());

                db.Excecute(comm);
            }
        }
        public void DeleteContactverslag(ContactverslagWeergave contactverslag)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                List<DbParam> paramList = new List<DbParam>()
                {
                    new DbParam() { Name = "@id", Value = contactverslag.ContactverslagId, DbType = DbTypes.db_integer}
                };
                string sqlLabels = @"DELETE FROM Contactverslag_Labels WHERE contactverslag_id=@id";
                string sqlContactverslag = @"UPDATE Contactverslag SET active = 0 WHERE id = @id";

                List<DbCommand> commList = new List<DbCommand>()
                {
                    new DbCommand(DbCommandType.crud, sqlLabels, paramList.ToArray()),
                    new DbCommand(DbCommandType.crud, sqlContactverslag, paramList.ToArray())
                };

                try
                {
                    db.Excecute(commList);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
        #endregion
    }
}
