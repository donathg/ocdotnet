﻿

 
using sharedcode.common;
using sharedcode.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sharedcode.DAL
{
    public class WpfLayoutDAL
    {
        public static void SaveLayout (Stream s, String controlCode, int userId, string pcname)
        { 
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                DbCommand commd = new DbCommand(DbCommandType.crud)
                {
                    Sql = "delete from  wpflayout where lay_code=@lay_code and lay_geb_id =@lay_geb_id and lay_pcname=@lay_pcname"
                };
                commd.Params.Add(new DbParam() { Name = "@lay_code", Value = controlCode });
                commd.Params.Add(new DbParam() { Name = "@lay_geb_id", Value = userId });
                commd.Params.Add(new DbParam() { Name = "@lay_pcname", Value = pcname });

                DbCommand comm = new DbCommand(DbCommandType.crud)
                {
                    Sql = "insert into wpflayout (lay_code,lay_geb_id,lay_pcname,lay_data) values (@lay_code,@lay_geb_id,@lay_pcname,@lay_data)"
                };
                comm.Params.Add(new DbParam() { Name = "@lay_code",  Value = controlCode });
                comm.Params.Add(new DbParam() { Name = "@lay_geb_id", Value = userId });
                comm.Params.Add(new DbParam() { Name = "@lay_pcname", Value = pcname });
                comm.Params.Add(new DbParam() { Name = "@lay_data", Value = s , DbType = DbTypes.db_varbinary});

                List<DbCommand> dbCommandList = new List<DbCommand>
                {
                    commd,
                    comm
                };

                try
                {
                    db.Excecute(dbCommandList);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

        }
        public static MemoryStream LoadLayout(String controlCode, int userId, string pcname)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                DbCommand comm = new DbCommand(DbCommandType.crud)
                {
                    Sql = "select lay_data from  wpflayout where lay_code=@lay_code and lay_geb_id =@lay_geb_id and lay_pcname=@lay_pcname"
                };
                comm.Params.Add(new DbParam() { Name = "@lay_code", Value = controlCode });
                comm.Params.Add(new DbParam() { Name = "@lay_geb_id", Value = userId });
                comm.Params.Add(new DbParam() { Name = "@lay_pcname", Value = pcname });

               DataTable dt = db.GetDataTable(comm.Sql, comm.Params.ToArray());
                if (dt !=null && dt.Rows.Count == 1)
                {
                    MemoryStream stream = new MemoryStream((byte[])dt.Rows[0][0]);
                    return stream;
                }
                return null;
            }

        }
        public static MemoryStream LoadHelp(String screenCode)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                DbCommand comm = new DbCommand(DbCommandType.crud)
                {
                    Sql = "select data from  help where screencode=@code"
                };
                comm.Params.Add(new DbParam() { Name = "@code", Value = screenCode });
 

                DataTable dt = db.GetDataTable(comm.Sql, comm.Params.ToArray());
                if (dt != null && dt.Rows.Count == 1)
                {
                    MemoryStream stream = new MemoryStream((byte[])dt.Rows[0][0]);
                    return stream;
                }
                return null;
            }

        }
        public static void SaveHelp(Stream s, String screenCode, int userId)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                DbCommand commd = new DbCommand(DbCommandType.crud)
                {
                    Sql = "delete from  help where screencode=@code"
                };
                commd.Params.Add(new DbParam() { Name = "@code", Value = screenCode });


                DbCommand comm = new DbCommand(DbCommandType.crud)
                {
                    Sql = "insert into help (screencode,modifier,modificationdate,data) values (@code,@userId,@date,@data)"
                };
                comm.Params.Add(new DbParam() { Name = "@code", Value = screenCode });
                comm.Params.Add(new DbParam() { Name = "@userId", Value = userId });
                comm.Params.Add(new DbParam() { Name = "@date", Value = DateTime.Now });
                comm.Params.Add(new DbParam() { Name = "@data", Value = s, DbType = DbTypes.db_varbinary });

                List<DbCommand> dbCommandList = new List<DbCommand>
                {
                    commd,
                    comm
                };

                try
                {
                    db.Excecute(dbCommandList);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

        }
    }
}
