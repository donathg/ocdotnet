﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using sharedcode.BLL;
using sharedcode.common;

namespace sharedcode.DAL
{
    public class LandenDAL
    {
        private static List<country> ConvertDatatableToCountrylist(DataTable dt)
        {
            List<country> tempList = new List<country>();
            foreach (DataRow row in dt.Rows)
            {
                tempList.Add(new country()
                {
                    id = (int)row["id"],
                    code = row["code"].ToString(),
                    name = row["name"].ToString() 
                });
            }
            return tempList;
        }

        public static List<country> SelectAllCountries()
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                String sql = @"SELECT * FROM country";
                DataTable dt = db.GetDataTable(sql);

                return ConvertDatatableToCountrylist(dt);
            }
        }
        public static country SelectCountryFromId(int id)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                List<DbParam> paramList = new List<DbParam>()
                {
                    new DbParam() { Name = "@id", Value = id, DbType = DbTypes.db_integer }
                };
                String sql = @"SELECT * FROM country WHERE id = @id";
                DataTable dt = db.GetDataTable(sql, paramList.ToArray());

                return ConvertDatatableToCountrylist(dt).SingleOrDefault();
            }
        }
    }
}
