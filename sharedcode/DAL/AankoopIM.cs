﻿using sharedcode.BLL;
using sharedcode.common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sharedcode.DAL
{
    public static class AankoopIM_DAL
    {

        public static List<DateTime> GetAfsluitDatums()
        {

            List<DateTime> list = new List<DateTime>();
            String sql = @"select distinct ClosedDate from aankoop.incontinentiemateriaal where ClosedDate is not null";
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                DataTable dt = db.GetDataTable(sql);
                foreach (DataRow row in dt.Rows)
                {
                    list.Add((DateTime)row["ClosedDate"]);
                }

            }
            return list;
        }

        public static List<AankoopIM> GetAankoopIM(DateTime? aflsuitDatum)
        {

            List<DbParam> paramList = null;

            List<AankoopIM> list = new List<AankoopIM>();
            String sql = "";

            if (aflsuitDatum.HasValue)
            {
                paramList = new List<DbParam>()
                { 
                    new DbParam() { Name = "@closingDate", Value = aflsuitDatum, DbType = DbTypes.db_datetime },
                };
                 sql = @"select aankoop.incontinentiemateriaal.*, bewoner.*, dbo.getAfleverplaatsBestelling(bewoner.id) afleverplaats, dbo.getAfleverplaatsLeefgroepBestelling(bewoner.id) leefgroep from aankoop.incontinentiemateriaal,bewoner
where bewoner.id = aankoop.incontinentiemateriaal.BewonerId  and ClosedDate=@closingDate order by leefgroep,voornaam";
            }
            else
            {
                sql = @"select aankoop.incontinentiemateriaal.*, bewoner.*, dbo.getAfleverplaatsBestelling(bewoner.id)  afleverplaats, dbo.getAfleverplaatsLeefgroepBestelling(bewoner.id) leefgroep from aankoop.incontinentiemateriaal,bewoner
where bewoner.id = aankoop.incontinentiemateriaal.BewonerId  and ClosedDate is null order by leefgroep,voornaam";

            }
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                DataTable dt;
                if (paramList == null)
                   dt  = db.GetDataTable(sql);
                else
                    dt = db.GetDataTable(sql, paramList.ToArray());

                foreach (DataRow row in dt.Rows)
                {
                    AankoopIM a = new AankoopIM();
                    a.Id = (int)row["id"];
                    a.ArtikelId = (int)row["artikelid"];
                    a.ClientId = (int)row["bewonerid"];
                    a.ClientRijksregisternummer = (String)row["rijksregisternr"];
                    a.ClientFirstName = (String)row["voornaam"];
                    a.ClientLastName = (String)row["achternaam"];
                    a.ClientOndersteuningsvorm = (String)row["ondersteuningsvorm"];
                    a.Aantal = (int)row["aantal"];
                    if(!row.IsNull("closeddate"))
                        a.ClosedDate = (DateTime?)row["closeddate"];
                    a.ArtikelPrijs = (double)(decimal)row["artikelprijs"];
                    a.LeefgroepInfo = (String)row["leefgroepinfo"];
                    a.ArtikelInfo = (String)row["artikelinfo"];
                    a.Afleverplaats = (String)row["afleverplaats"];
                    a.MainLeefgroep = (String)row["leefgroep"]; 
                    FillArtikelInfo(a);
                    list.Add(a);
                }
            }
            return list;
        }

        public static void Afsluiten(int userId)
        {
            DateTime closingDate = DateTime.Now;
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                List<DbParam> paramList = new List<DbParam>()
                { new DbParam() { Name = "@closingDate", Value = closingDate, DbType = DbTypes.db_datetime },
                    new DbParam() { Name = "@userId", Value = userId, DbType = DbTypes.db_integer },
                 
                };
                String sql = @"UPDATE aankoop.incontinentiemateriaal
                               SET ClosedDate=@closingDate, Closer=@userId
                               WHERE ClosedDate is null and dbo.getAfleverplaatsBestelling( aankoop.incontinentiemateriaal.BewonerId) is not null";

                DbCommand comm = new DbCommand(DbCommandType.crud, sql, paramList.ToArray());

                db.Excecute(comm);
            }
        }

        private static void FillArtikelInfo(AankoopIM a)
        {
            var x =  a.ArtikelInfo.Split(new string[] { "@@" }, StringSplitOptions.None);
            a.ArtikelNaam = x[0];
            a.ArtikelNr = x[1];
            a.ArtikelMaat = x[2];
            a.ArtikelKleur = x[3];
        }

     
    }
}
