﻿using sharedcode.common;
using sharedcode.WeergaveClasses;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sharedcode.DAL
{
    public class IdoDAL
    {
        public static List<DomicilieadresWeergave> SelectDomicilieAdresFromBewonerRijksregisternummer(string rijksregisternummer)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {

                DataTable dtDirect = db.GetDataTable(@"SELECT * FROM  vw_DomicilieAdressenOrbisNet where Rijksregisternummer = @rijksregisternummer",
                    new DbParam() { Name = "@rijksregisternummer", Value = rijksregisternummer, DbType = DbTypes.db_auto });

                return DatabaseParsers.DatabaseParser.DataTableParse<DomicilieadresWeergave>(dtDirect);
            }
        }
        public static List<BewindvoerderadresWeergave> SelectBewindvoerderAdresFromBewonerRijksregisternummer(string rijksregisternummer)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                DataTable dtDirect = db.GetDataTable(@"SELECT * FROM  vw_BewindvoerderOrbisNet where Rijksregisternummer = @rijksregisternummer",
                    new DbParam() { Name = "@rijksregisternummer", Value = rijksregisternummer, DbType = DbTypes.db_auto });

                return DatabaseParsers.DatabaseParser.DataTableParse<BewindvoerderadresWeergave>(dtDirect);
            }
        }
        public static List<OuderadresWeergave> SelectOuderAdresFromBewonerRijksregisternummer(string rijksregisternummer)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                List<DbParam> paramList = new List<DbParam>()
                {
                    new DbParam() { Name = "@rijksregisterNummer", Value = rijksregisternummer, DbType = DbTypes.db_auto },
                    new DbParam() { Name = "@RelatieIds", Value = OuderadresWeergave.RelatietypeIds, DbType = DbTypes.db_string },
                    new DbParam() { Name = "@ContactTypeIds", Value = OuderadresWeergave.ContacttypeIds, DbType = DbTypes.db_string },
                    new DbParam() { Name = "@tempTable", Direction= DbParamDirection.return_value }
                };
                DataTable dt = db.GetDataTable("SELECT * FROM GetAdresFromOrbisNetByIds(@rijksregisterNummer,@RelatieIds,@ContactTypeIds)", paramList.ToArray());

                return DatabaseParsers.DatabaseParser.DataTableParse<OuderadresWeergave>(dt);
            }
        }
        public static List<KinderbijslagadresWeergave> SelectKinderbijslagAdresFromBewonerRijksregisternummer(string rijksregisternummer)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                List<DbParam> paramList = new List<DbParam>()
                {
                    new DbParam() { Name = "@rijksregisterNummer", Value = rijksregisternummer, DbType = DbTypes.db_auto },
                    new DbParam() { Name = "@RelatieIds", Value = KinderbijslagadresWeergave.RelatietypeIds, DbType = DbTypes.db_string },
                    new DbParam() { Name = "@ContactTypeIds", Value = KinderbijslagadresWeergave.ContacttypeIds, DbType = DbTypes.db_string },
                    new DbParam() { Name = "@tempTable", Direction= DbParamDirection.return_value }
                };
                DataTable dt = db.GetDataTable("SELECT * FROM GetAdresFromOrbisNetByIds(@rijksregisterNummer,@RelatieIds,@ContactTypeIds)", paramList.ToArray());

                return DatabaseParsers.DatabaseParser.DataTableParse<KinderbijslagadresWeergave>(dt);
            }
        }
        public static List<FacturatieadresWeergave> SelectFacturatieAdresAndEmailFromBewonerRijksregisternummer(string rijksregisternummer)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                string sql = @" SELECT DISTINCT * 
                                FROM GetAdresFromOrbisNetByIds(@rijksregisterNummer,@RelatieIds,@ContactTypeIds)";
                List<DbParam> paramList = new List<DbParam>()
                {
                    new DbParam() { Name = "@rijksregisterNummer", Value = rijksregisternummer, DbType = DbTypes.db_auto },
                    new DbParam() { Name = "@RelatieIds", Value = FacturatieadresWeergave.RelatietypeIds, DbType = DbTypes.db_string },
                    new DbParam() { Name = "@ContactTypeIds", Value = FacturatieadresWeergave.ContacttypeIds, DbType = DbTypes.db_string },
                    new DbParam() { Name = "@tempTable", Direction= DbParamDirection.return_value }
                };
                DataTable dt = db.GetDataTable(sql, paramList.ToArray());

                return DatabaseParsers.DatabaseParser.DataTableParse<FacturatieadresWeergave>(dt);
            }
        }
        public static List<IbanFacturatieAdresWeergave> SelectIbanFacturatieadresFromBewonerRijksregisternummer (string rijksregisternummer)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                string sql = @"SELECT REPLACE(r.IBAN,' ','-') as iban
                    FROM OrbisNet.Clienten.ClientGegevens AS cg
                        LEFT OUTER JOIN OrbisNet.Adressen.Rechtssubject AS rs ON cg.OnderwerpId = rs.OnderwerpId
                        LEFT OUTER JOIN OrbisNet.Adressen.RekeningenPerRechtssubject AS rprs ON rs.id = rprs.RechtssubjectId
                        LEFT OUTER JOIN OrbisNet.Adressen.Rekening AS r ON r.Id = rprs.RekeningId
                        LEFT OUTER JOIN OrbisNet.Adressen.Relatie AS rel ON rel.id = rprs.RelatieId
                    WHERE rel.RelatieTypesId = 12
                        AND cg.Rijksregisternummer = @rijksregisterNummer
                        AND rs.Id in (
                            SELECT rprs.RechtssubjectId
                            FROM OrbisNet.Adressen.RekeningenPerRechtssubject AS rprs
                                LEFT OUTER JOIN OrbisNet.Adressen.Relatie AS rel ON rel.id = rprs.RelatieId
                            WHERE rel.RelatieTypesId = 12
                            GROUP BY rprs.RechtssubjectId
                            HAVING COUNT(rprs.RechtssubjectId) = 1
                        )
                    ";
                List<DbParam> paramList = new List<DbParam>()
                {
                    new DbParam() { Name = "@rijksregisterNummer", Value = rijksregisternummer, DbType = DbTypes.db_auto }
                };
                DataTable dt = db.GetDataTable(sql, paramList.ToArray());

                return DatabaseParsers.DatabaseParser.DataTableParse<IbanFacturatieAdresWeergave>(dt);
            }
        }
        public static List<BWaardeWeergave> SelectBWaardeFromBewonerRijksregisternummer(string rijksregisternummer)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                string sql = @"SELECT DISTINCT BWaarde
                                  FROM vw_BPWaardenOrbisNet
                                  WHERE Rijksregisternummer = @rijksregisterNummer";
                List<DbParam> paramList = new List<DbParam>()
                {
                    new DbParam() { Name = "@rijksregisterNummer", Value = rijksregisternummer, DbType = DbTypes.db_auto }
                };
                DataTable dt = db.GetDataTable(sql, paramList.ToArray());

                return DatabaseParsers.DatabaseParser.DataTableParse<BWaardeWeergave>(dt);
            }
        }
        public static List<PWaardeWeergave> SelectPWaardeFromBewonerRijksregisternummer(string rijksregisternummer)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                string sql = @"SELECT DISTINCT PWaarde
                                  FROM vw_BPWaardenOrbisNet
                                  WHERE Rijksregisternummer = @rijksregisterNummer";
                List<DbParam> paramList = new List<DbParam>()
                {
                    new DbParam() { Name = "@rijksregisterNummer", Value = rijksregisternummer, DbType = DbTypes.db_auto }
                };
                DataTable dt = db.GetDataTable(sql, paramList.ToArray());

                return DatabaseParsers.DatabaseParser.DataTableParse<PWaardeWeergave>(dt);
            }
        }
        public static List<PunterPerJaarWeergave> SelectPuntenPerJaarFromBewonerRijksregisternummer(string rijksregisternummer)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                string sql = @"SELECT distinct 
	                                STUFF(
		                                (SELECT 
			                                CASE
				                                WHEN bp2.GeldigTot is not null
				                                THEN CHAR(10) + CONVERT(varchar,bp2.GeldigVan,103) + ' - ' + CONVERT(varchar,bp2.GeldigTot,103) + ' = ' + CAST(bp2.ZorggebondenPunten AS varchar(max)) + ' punten.'
				                                ELSE CHAR(10) + CONVERT(varchar,bp2.GeldigVan,103) + ' - 31/12/9999 = ' + CAST(bp2.ZorggebondenPunten AS varchar(max)) + ' punten.'
			                                END
		                                FROM vw_BPWaardenOrbisNet AS bp2 WHERE bp2.Rijksregisternummer = bp.Rijksregisternummer FOR XML PATH('')),1,1,''
	                                ) as 'PuntenEnData', Rijksregisternummer
                               FROM vw_BPWaardenOrbisNet as bp
                               WHERE Rijksregisternummer = @rijksregisterNummer";
                List<DbParam> paramList = new List<DbParam>()
                {
                    new DbParam() { Name = "@rijksregisterNummer", Value = rijksregisternummer, DbType = DbTypes.db_auto }
                };
                DataTable dt = db.GetDataTable(sql, paramList.ToArray());

                return DatabaseParsers.DatabaseParser.DataTableParse<PunterPerJaarWeergave>(dt);
            }
        }
    }
}
