﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using sharedcode.common;

namespace sharedcode.DAL
{
    public class beeldvormingDAL
    {
        public static void CreateNewVersion(int bewonerId,DateTime expirationDate)
        {
            DbCommand command = new DbCommand(DbCommandType.procedure, "[cdCreateNewVersionBeeldvorming]"
                , new DbParam() {Name = "@retval", Direction = DbParamDirection.return_value}
                , new DbParam() {Name = "@bewonerId", Value = bewonerId }
                , new DbParam() {Name = "@gebruikerId", Value = GlobalData.Instance.LoggedOnUser.Id.GetValueOrDefault() }
                , new DbParam() {Name = "@expirationDate", Value = expirationDate.Date });
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                try
                {
                    db.Excecute(command);
                }
                catch
                {
                    throw;
                }
            }
        }
    }
}
