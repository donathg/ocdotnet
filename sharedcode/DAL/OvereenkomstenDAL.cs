﻿using sharedcode.BLL;
using sharedcode.common;
using sharedcode.WeergaveClasses;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sharedcode.DAL
{
    public static class OvereenkomstenDAL
    {
        #region Overeenkomst_type methods
        private static List<Overeenkomst_type> ConvertDatatableToOvereenkomstTypesList(DataTable dt)
        {
            List<Overeenkomst_type> tempList = new List<Overeenkomst_type>();
            foreach (DataRow row in dt.Rows)
            {
                Overeenkomst_type type = new Overeenkomst_type()
                {
                    id = (int)row["id"],
                    naam = row["naam"].ToString(),
                    descr = row["descr"].ToString(),
                    active = (bool)row["active"]
                };
                if (row["descr"] is string descr)
                    type.descr = descr;

                tempList.Add(type);
            }
            return tempList;
        }

        public static List<Overeenkomst_type> SelectAllOvereenkomstTypes()
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                String sql = @"SELECT * FROM Overeenkomst_type";
                DataTable dt = db.GetDataTable(sql);

                return ConvertDatatableToOvereenkomstTypesList(dt);
            }
        }
        public static Overeenkomst_type SelectOvereenkomstTypeFromId(int id)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                List<DbParam> paramList = new List<DbParam>()
                {
                    new DbParam() { Name = "@id", Value = id, DbType = DbTypes.db_integer }
                };
                String sql = @"SELECT * FROM Overeenkomst_type WHERE id = @id";
                DataTable dt = db.GetDataTable(sql, paramList.ToArray());

                return ConvertDatatableToOvereenkomstTypesList(dt).SingleOrDefault();
            }
        }
        public static void InsertOvereenkomstType(Overeenkomst_type type)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                List<DbParam> paramList = new List<DbParam>()
                {
                    new DbParam() { Name = "@naam", Value = type.naam, DbType = DbTypes.db_string },
                    new DbParam() { Name = "@descr", Value = type.descr, DbType = DbTypes.db_string },
                    new DbParam() { Name = "@active", Value = GlobalSharedMethods.BooleantoBitConverter(type.active), DbType = DbTypes.db_auto }
                };
                String sql = @"INSERT INTO Overeenkomst_type (naam, descr, active) VALUES (@naam, @descr, @active)";
                DbCommand comm = new DbCommand(DbCommandType.crud, sql, paramList.ToArray());

                try
                {
                    db.Excecute(comm);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
        public static void UpdateOvereenkomstType(Overeenkomst_type type)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                List<DbParam> paramList = new List<DbParam>()
                {
                    new DbParam() { Name = "@naam", Value = type.naam, DbType = DbTypes.db_string },
                    new DbParam() { Name = "@descr", Value = type.descr, DbType = DbTypes.db_string },
                    new DbParam() { Name = "@active", Value = GlobalSharedMethods.BooleantoBitConverter(type.active), DbType = DbTypes.db_auto },
                    new DbParam() { Name = "@id", Value = type.id, DbType = DbTypes.db_integer }
                };
                String sql = @"UPDATE Overeenkomst_type SET naam = @naam, descr = @descr, active = @active WHERE id = @id";
                DbCommand comm = new DbCommand(DbCommandType.crud, sql, paramList.ToArray());

                try
                {
                    db.Excecute(comm);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
        public static void DeleteOvereenkomstType(Overeenkomst_type type)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                List<DbParam> paramList = new List<DbParam>()
                {
                    new DbParam() { Name = "@id", Value = type.id, DbType = DbTypes.db_integer }
                };
                String sql = @"DELETE FROM Overeenkomst_type WHERE id = @id";
                DbCommand comm = new DbCommand(DbCommandType.crud, sql, paramList.ToArray());

                try
                {
                    db.Excecute(comm);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
        #endregion
        #region Overeenkomst_status methods
        private static List<Overeenkomst_status> ConvertDatatableToOvereenkomstStatusList(DataTable dt)
        {
            List<Overeenkomst_status> tempList = new List<Overeenkomst_status>();
            foreach (DataRow row in dt.Rows)
            {
                Overeenkomst_status type = new Overeenkomst_status()
                {
                    id = (int)row["id"],
                    naam = row["naam"].ToString(),
                    descr = row["descr"].ToString(),
                    active = (bool)row["active"],
                    prioriteit = (int)row["prioriteit"]
                };
                if (row["descr"] is string descr)
                    type.descr = descr;

                tempList.Add(type);
            }
            return tempList;
        }

        public static List<Overeenkomst_status> SelectAllOvereenkomstStatussen()
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                String sql = @"SELECT * FROM Overeenkomst_status";
                DataTable dt = db.GetDataTable(sql);

                return ConvertDatatableToOvereenkomstStatusList(dt);
            }
        }
        public static Overeenkomst_status SelectOvereenkomstStatusFromId(int id)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                List<DbParam> paramList = new List<DbParam>()
                {
                    new DbParam() { Name = "@id", Value = id, DbType = DbTypes.db_integer }
                };
                String sql = @"SELECT * FROM Overeenkomst_status WHERE id = @id";
                DataTable dt = db.GetDataTable(sql, paramList.ToArray());

                return ConvertDatatableToOvereenkomstStatusList(dt).SingleOrDefault();
            }
        }
        public static Overeenkomst_status SelectLowestPriorityStatus()
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                String sql = @"SELECT * FROM Overeenkomst_status WHERE prioriteit = (SELECT MIN(prioriteit) FROM Overeenkomst_status) AND active = 1";
                DataTable dt = db.GetDataTable(sql);

                return ConvertDatatableToOvereenkomstStatusList(dt).SingleOrDefault();
            }
        }
        public static void InsertOvereenkomstStatus(Overeenkomst_status type)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                List<DbParam> paramList = new List<DbParam>()
                {
                    new DbParam() { Name = "@naam", Value = type.naam, DbType = DbTypes.db_string },
                    new DbParam() { Name = "@descr", Value = type.descr, DbType = DbTypes.db_string },
                    new DbParam() { Name = "@active", Value = GlobalSharedMethods.BooleantoBitConverter(type.active), DbType = DbTypes.db_auto }
                };
                String sql = @"INSERT INTO Overeenkomst_status (naam, descr, active) VALUES (@naam, @descr, @active)";
                DbCommand comm = new DbCommand(DbCommandType.crud, sql, paramList.ToArray());

                try
                {
                    db.Excecute(comm);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
        public static void UpdateOvereenkomstStatus(Overeenkomst_status type)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                List<DbParam> paramList = new List<DbParam>()
                {
                    new DbParam() { Name = "@naam", Value = type.naam, DbType = DbTypes.db_string },
                    new DbParam() { Name = "@descr", Value = type.descr, DbType = DbTypes.db_string },
                    new DbParam() { Name = "@active", Value = GlobalSharedMethods.BooleantoBitConverter(type.active), DbType = DbTypes.db_auto },
                    new DbParam() { Name = "@id", Value = type.id, DbType = DbTypes.db_integer }
                };
                String sql = @"UPDATE Overeenkomst_status SET naam = @naam, descr = @descr, active = @active WHERE id = @id";
                DbCommand comm = new DbCommand(DbCommandType.crud, sql, paramList.ToArray());

                try
                {
                    db.Excecute(comm);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
        public static void DeleteOvereenkomstStatus(Overeenkomst_status type)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                List<DbParam> paramList = new List<DbParam>()
                {
                    new DbParam() { Name = "@id", Value = type.id, DbType = DbTypes.db_integer }
                };
                String sql = @"DELETE FROM Overeenkomst_status WHERE id = @id";
                DbCommand comm = new DbCommand(DbCommandType.crud, sql, paramList.ToArray());

                try
                {
                    db.Excecute(comm);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
        #endregion
        #region Overeenkomst methods
        private static List<OvereenkomstWeergave> ConvertDatatableToOvereenkomstWeergave(DataTable dt)
        {
            List<OvereenkomstWeergave> tempList = new List<OvereenkomstWeergave>();
            foreach (DataRow row in dt.Rows)
            {
                OvereenkomstWeergave weergave = new OvereenkomstWeergave()
                {
                    id = (int)row["id"],
                    bewoner_id = (int)row["bewoner_id"],
                    status_id = (int)row["status_id"],
                    vanDatum = (DateTime)row["vanDatum"],
                    AantalBijlagen = (int)row["AantalBijlagen"],
                    creator = (int)row["creator"],
                    CreatorNaam = row["CreatorNaam"].ToString(),
                    creation_date = (DateTime)row["creation_date"],
                    BewonerNaam = row["BewonerNaam"].ToString()
                };
                if (row["totDatum"] is DateTime totDatum)
                    weergave.totDatum = totDatum;

                Overeenkomst_status statusWeergave = new Overeenkomst_status()
                {
                    id = (int)row["status_id"],
                    naam = row["status_naam"].ToString(),
                    active = (bool)row["status_active"]
                };
                if (row["status_descr"] is string descr)
                    statusWeergave.descr = descr;
                weergave.Status = statusWeergave;

                Overeenkomst_type typeWeergave = new Overeenkomst_type()
                {
                    id = (int)row["type_id"],
                    naam = row["type_naam"].ToString(),
                    active = (bool)row["type_active"]
                };
                if (row["type_descr"] is string typeDescr)
                    typeWeergave.descr = typeDescr;
                weergave.Type = typeWeergave;

                tempList.Add(weergave);
            }
            return tempList;
        }

        public static List<OvereenkomstWeergave> GetAllOvereenkomsten(OvereenkomstenSettings settings)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                List<DbParam> paramList = new List<DbParam>();
                String sql = @"SELECT o.*, o_l.naam AS status_naam, o_l.descr AS status_descr, o_l.active AS status_active, 
o_t.naam AS type_naam, o_t.descr AS type_descr, o_t.active AS type_active, (g.voornaam + ' ' + g.achternaam) AS CreatorNaam,
(SELECT COUNT(f.id) FROM cd_files AS f WHERE o.id = f.overeenkomst_id) AS AantalBijlagen, (b.voornaam + ' ' + b.achternaam) AS BewonerNaam
                               FROM Cd_Overeenkomst AS o, Overeenkomst_status AS o_l, gebruiker AS g, Overeenkomst_type AS o_t, bewoner AS b
                               WHERE o.status_id = o_l.id AND o.creator = g.id AND o.type_id = o_t.id
                               AND b.id = o.bewoner_id";
                if (settings.BewonerId is int bewonerId)
                {
                    paramList.Add(new DbParam() { Name = "@bewonerId", Value = bewonerId, DbType = DbTypes.db_integer });
                    sql += @"  AND o.bewoner_id = @bewonerId";
                }
                if (settings.Statussen != null && settings.Statussen.Where(x => x.id != 0).ToList() is List<Overeenkomst_status> statussen && statussen.Count != 0)
                {
                    string statusIds = "";
                    foreach (Overeenkomst_status status in statussen)
                    {
                        if (!string.IsNullOrWhiteSpace(statusIds))
                            statusIds += ", ";
                        statusIds += status.id;
                    }
                    sql += @"  AND o.status_id in (" + statusIds  + ")";
                }
                if (settings.Type is Overeenkomst_type type && type.id != 0)
                {
                    paramList.Add(new DbParam() { Name = "@typeId", Value = type.id, DbType = DbTypes.db_integer });
                    sql += @"  AND o.type_id = @typeId";
                }
                sql += @"      ORDER BY o.vanDatum DESC";  
                DataTable dt = db.GetDataTable(sql, paramList.ToArray());

                return ConvertDatatableToOvereenkomstWeergave(dt);
            }
        }
        public static OvereenkomstWeergave GetOvereenkomstFromId(int id)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                List<DbParam> paramList = new List<DbParam>()
                {
                    new DbParam() { Name = "@id", Value = id, DbType = DbTypes.db_integer }
                };
                String sql = @"SELECT o.*, 
o_l.naam AS status_naam, o_l.descr AS status_descr, o_l.active AS status_active, 
o_t.naam AS type_naam, o_t.descr AS type_descr, o_t.active AS type_active,
(g.voornaam + ' ' + g.achternaam) AS CreatorNaam,
(SELECT COUNT(f.id) FROM cd_files AS f WHERE o.id = f.overeenkomst_id) AS AantalBijlagen
                               FROM Cd_Overeenkomst AS o, Overeenkomst_status AS o_l, gebruiker AS g, Overeenkomst_type AS o_t
                               WHERE o.status_id = o_l.id AND o.creator = g.id AND o.type_id = o_t.id
                               AND o.id = @id";
                DataTable dt = db.GetDataTable(sql, paramList.ToArray());

                return ConvertDatatableToOvereenkomstWeergave(dt).SingleOrDefault();
            }
        }
        public static void InsertOvereenkomst(OvereenkomstWeergave overeenkomst)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                List<DbParam> paramListOvereenkomst = new List<DbParam>()
                    {
                        new DbParam() { Name = "@bewonerid", Value = overeenkomst.bewoner_id, DbType = DbTypes.db_integer },
                        new DbParam() { Name = "@vanDatum", Value = overeenkomst.vanDatum, DbType = DbTypes.db_date },
                        new DbParam() { Name = "@totDatum", Value = overeenkomst.totDatum, DbType = DbTypes.db_date },
                        new DbParam() { Name = "@typeId", Value = overeenkomst.type_id, DbType = DbTypes.db_integer },
                        new DbParam() { Name = "@status", Value = overeenkomst.status_id, DbType = DbTypes.db_integer },
                        new DbParam() { Name = "@creator", Value = GlobalData.Instance.LoggedOnUser.Id.Value, DbType = DbTypes.db_integer },
                        new DbParam() { Name = "@creationDate", Value = DateTime.Now, DbType = DbTypes.db_datetime }
                    };
                String sqlOvereenkomst = @"INSERT INTO Cd_Overeenkomst (bewoner_id, type_id, vanDatum, totDatum, status_id, creator, creation_date)
                                               VALUES (@bewonerId, @typeId, @vanDatum, @totDatum, @status, @creator, @creationDate)";

                try
                {
                    db.Excecute(new DbCommand(DbCommandType.crud, sqlOvereenkomst, paramListOvereenkomst.ToArray()));
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
        public static void UpdateStatus(OvereenkomstWeergave overeenkomst)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                List<DbParam> paramList = new List<DbParam>()
                {
                    new DbParam() { Name = "@id", Value = overeenkomst.id, DbType = DbTypes.db_integer },
                    new DbParam() { Name = "@status", Value = overeenkomst.status_id, DbType = DbTypes.db_integer }
                };
                String sql = @"UPDATE Cd_Overeenkomst
                               SET status_id = @status
                               WHERE id = @id";
                DbCommand comm = new DbCommand(DbCommandType.crud, sql, paramList.ToArray());

                try
                {
                    db.Excecute(comm);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
        public static void DeleteOvereenkomst(OvereenkomstWeergave overeenkomst)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                List<DbParam> paramList = new List<DbParam>()
                {
                    new DbParam() { Name = "@id", Value = overeenkomst.id, DbType = DbTypes.db_integer }
                };
                String sql = @"DELETE FROM Cd_Overeenkomst WHERE id = @id";
                DbCommand comm = new DbCommand(DbCommandType.crud, sql, paramList.ToArray());

                try
                {
                    db.Excecute(comm);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
        #endregion
    }
}
