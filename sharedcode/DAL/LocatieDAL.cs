﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using sharedcode.BLL;
using sharedcode.common;

namespace sharedcode.DAL
{
    public class LocatieDAL
    {
        private static List<locatie> ConvertDatatableToLocatieList(DataTable dt)
        {
            List<locatie> locatieList = new List<locatie>();
            foreach (DataRow row in dt.Rows)
            {
                locatie loc = new locatie()
                {
                    id = (int)row["id"],
                    naam = row["naam"].ToString(),
                    active = (bool)row["active"]
                };
                if (row["parent_id"] is int parentId)
                    loc.parent_id = parentId;
                if (row["beschrijving"] is String descr)
                    loc.beschrijving = descr;
                if (row["opmerking"] is String opmerking)
                    loc.opmerking = opmerking;

                locatieList.Add(loc);
            }
            return locatieList;
        }
        
        public static List<locatie> SelectAllSublocatiesFromLocatie(locatie loc)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                List<DbParam> paramList = new List<DbParam>()
                {
                    new DbParam() { Name = "@parentId", Value = loc.id, DbType = DbTypes.db_integer }
                };
                String sql = @"WITH EntityChildren AS
                               (
                                    SELECT * FROM locatie WHERE parent_id = @parentId

                                    UNION ALL

                                	SELECT l.* FROM locatie l INNER JOIN EntityChildren l2 ON l.parent_id = l2.id
                               )
                               SELECT * from EntityChildren";
                DataTable dt = db.GetDataTable(sql, paramList.ToArray());

                return ConvertDatatableToLocatieList(dt);
            }
        }
        public static List<locatie> SelectAllLocaties(bool? active)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                List<DbParam> paramList = new List<DbParam>();
                String sql = @"SELECT * FROM locatie";
                if (active != null)
                {
                    paramList.Add(new DbParam() { Name = "@active", Value = GlobalSharedMethods.BooleantoBitConverter(active.Value), DbType = DbTypes.db_auto });
                    sql += "WHERE active = @active";
                }
                DataTable dt = db.GetDataTable(sql, paramList.ToArray());

                return ConvertDatatableToLocatieList(dt);
            }
        }
        public static void InsertLocatie(locatie loc)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                List<DbParam> paramList = new List<DbParam>()
                {
                    new DbParam() { Name = "@parentId", Value = loc.parent_id, DbType = DbTypes.db_integer },
                    new DbParam() { Name = "@naam", Value = loc.naam, DbType = DbTypes.db_string },
                    new DbParam() { Name = "@beschrijving", Value = loc.beschrijving, DbType = DbTypes.db_string },
                    new DbParam() { Name = "@opmerking", Value = loc.opmerking, DbType = DbTypes.db_string },
                    new DbParam() { Name = "@active", Value = GlobalSharedMethods.BooleantoBitConverter(loc.active), DbType = DbTypes.db_auto }
                };
                String sql = @"INSERT INTO locatie(parent_id, naam, beschrijving, opmerking, active)
                               VALUES(@parentId, @naam, @beschrijving, @opmerking, @active)";
                DbCommand comm = new DbCommand(DbCommandType.crud, sql, paramList.ToArray());

                try
                {
                    db.Excecute(comm);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
        public static void UpdateLocatie(locatie loc)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                List<DbParam> paramList = new List<DbParam>()
                {
                    new DbParam() { Name = "@id", Value = loc.id, DbType = DbTypes.db_integer },
                    new DbParam() { Name = "@parentId", Value = loc.parent_id, DbType = DbTypes.db_integer },
                    new DbParam() { Name = "@naam", Value = loc.naam, DbType = DbTypes.db_string },
                    new DbParam() { Name = "@beschrijving", Value = loc.beschrijving, DbType = DbTypes.db_string },
                    new DbParam() { Name = "@opmerking", Value = loc.opmerking, DbType = DbTypes.db_string }
                };
                String sql = @"UPDATE locatie
                               SET parent_id = @parentId, naam = @naam, beschrijving = @beschrijving, opmerking = @opmerking
                               WHERE id = @id";
                DbCommand comm = new DbCommand(DbCommandType.crud, sql, paramList.ToArray());

                try
                {
                    db.Excecute(comm);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
        public static void DeleteLocatieAndSublocaties(locatie loc)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                List<DbParam> paramList = new List<DbParam>()
                {
                    new DbParam() { Name = "@id", Value = loc.id, DbType = DbTypes.db_integer }
                };
                String sql = @"WITH EntityChildren AS
                               (
                                    SELECT * FROM locatie WHERE id = @id

                                    UNION ALL

                                    SELECT l.* FROM locatie l INNER JOIN EntityChildren l2 ON l.parent_id = l2.id
                               )
                               DELETE FROM locatie WHERE id IN ( SELECT id from EntityChildren )";
                DbCommand comm = new DbCommand(DbCommandType.crud, sql, paramList.ToArray());

                try
                {
                    db.Excecute(comm);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
        public static void SetActiveStatus(locatie loc)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                List<DbParam> paramList = new List<DbParam>()
                {
                    new DbParam() { Name = "@id", Value = loc.id, DbType = DbTypes.db_integer },
                    new DbParam() { Name = "@active", Value = GlobalSharedMethods.BooleantoBitConverter(loc.active), DbType = DbTypes.db_auto }
                };
                String sql = @"WITH EntityChildren AS
                               (
                                    SELECT * FROM locatie WHERE id = @id

                                    UNION ALL

                                    SELECT l.* FROM locatie l INNER JOIN EntityChildren l2 ON l.parent_id = l2.id
                               )
                               UPDATE locatie
                               SET active = @active
                               WHERE id IN ( SELECT id from EntityChildren )";
                DbCommand comm = new DbCommand(DbCommandType.crud, sql, paramList.ToArray());

                try
                {
                    db.Excecute(comm);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
    }
}
