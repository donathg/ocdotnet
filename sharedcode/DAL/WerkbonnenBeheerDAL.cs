﻿
using sharedcode.BLL;
using sharedcode.common;
using sharedcode.CommonObjects;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sharedcode.DAL
{
    public class WerkbonnenBeheerDAL
    {
        /// <param name="settings"></param>
        /// <param name="afterModificationDate"> enkel wb met een modificatidatum groter dan deze parameter (refresh)</param>
        /// <returns></returns>
        public static DataTable LoadWerkopdrachten(WerkbonnenBeheerSettings2 settings, DateTime? afterModificationDate, HistoryType histtype)
        {
            List<DbParam> parameters = new List<DbParam>();

            String sql = @" SELECT creator.telefoon, creator.telefoon_intern, creator.telefoon_intern_groep,readdate, wo.wo_wodienst_code, wo.wo_parent_id, wo.wo_id, i.id, 
	                            i.nummer_263, i.productOmschrijving, wo.wo_titel, wo.wo_beschrijving, wo.wo_wop_id, wo.wo_loc_id, wo_s_h.wosh_wos_id, wo_s_h.wosh_opmerking,
	                            wo.wo_wot_id, wo.wo_geb_id_creator, wo.wo_creatiedatum, wo.wo_begindatum, wo.wo_einddatum, creator.voornaam + ' ' + creator.achternaam AS creator,
	                            dbo.getOrbisGroepGebruiker(creator.id , GetDate()) AS groepgeb_groep_code, modifier.voornaam + ' ' + modifier.achternaam AS modifier, wo_s_h.wosh_datum, 
	                            statusgebruiker.voornaam + ' ' + statusgebruiker.achternaam AS statusgebruiker, wo.wo_modificatiedatum, 
	                            (
		                            SELECT CAST(wogeb_geb_id AS VARCHAR) + '@' + g.voornaam + '@' + g.achternaam + ',' AS 'data()'
		                            FROM werkopdracht_gebruikers, gebruiker AS g 
		                            WHERE wogeb_wo_id = wo.wo_id and wogeb_geb_id = g.id FOR XML PATH('')
	                            ) AS gebruikers,
	                            (
		                            SELECT werkopdracht_groepen.wogroep_geb_subgroep + ',' AS 'data()' 
		                            FROM werkopdracht_groepen 
		                            WHERE werkopdracht_groepen.wogroep_wo_id = wo.wo_id FOR XML PATH('')
	                            ) as gebruikersgroepen,
	                            (SELECT count(*) from werkopdracht_files where werkopdracht_files.wo_id =wo.wo_id) as aantalbijlagen,
	                            (SELECT count(*) from locatie_objecten where locatie_objecten.loc_id =wo.wo_loc_id and locatie_objecten.objecttype_id = 1) as aantalAsbest
                            FROM werkopdracht AS wo
	                            LEFT JOIN gebruiker AS modifier on modifier.id = wo.wo_geb_id_modificator
	                            LEFT JOIN gebruiker AS creator on creator.id = wo.wo_geb_id_creator
	                            LEFT JOIN werkopdracht_read AS wo_r on wo_r.wo_id = wo.wo_id and wo_r.reader = " + GlobalData.Instance.LoggedOnUser.Id.GetValueOrDefault() + @"
	                            LEFT JOIN werkopdracht AS wo2 on wo.wo_parent_id = wo2.wo_id
	                            LEFT JOIN werkopdracht_status_historiek AS sh2 on wo2.wo_wosh_id = sh2.wosh_id
	                            LEFT JOIN inventaris.inventaris AS i on wo.wo_inv_id = i.id
	                            ,werkopdracht_status_historiek AS wo_s_h, gebruiker AS statusgebruiker
                            WHERE wo.wo_wosh_id = wo_s_h.wosh_id AND wo.wo_geb_id_creator = creator.id AND statusgebruiker.id = wo_s_h.wosh_geb_id";

            String locGroepFilter = String.Empty;
            if (settings.LocationFilter.Count() > 0 && settings.LocationFilter[0] != -1) //vooral voor WA filteren op Groep, enkel WA's van Locatie Id98 en daaronder
            {
                String kommaSepString = String.Empty;
                for (int i = 0; i < settings.LocationFilter.Count(); i++)
                {
                    if (i > 0)
                        kommaSepString += ",";
                    kommaSepString += settings.LocationFilter[i];
                }
                locGroepFilter += String.Format(" and wo.wo_loc_id in (select id from dbo.GetLocationTree('{0}')) ", kommaSepString);
            }
            else
            {
                if (settings.GroepFilter.Count() > 0) //vooral voor WA filteren op Groep, enkel WA's van Boekhouding
                {
                    locGroepFilter += @" and (
                           ( wo.wo_parent_id is null and  @groep in (select r2.groepgeb_groep_code from  rechten_groepengebruikers r2,werkopdracht w2 where r2.groepgeb_geb_id = w2.wo_geb_id_creator and w2.wo_id = wo.wo_id)) 
                              or 
                           (wo.wo_parent_id is not null and @groep2 in (select r2.groepgeb_groep_code from  rechten_groepengebruikers r2,werkopdracht w2 where r2.groepgeb_geb_id = w2.wo_geb_id_creator and w2.wo_id = wo.wo_parent_id)  )
                          )";
                    parameters.Add(new DbParam() { Name = "@groep", Value = settings.GroepFilter[0] });
                    parameters.Add(new DbParam() { Name = "@groep2", Value = settings.GroepFilter[0] });
                }
            }

            String sqlModdate = String.Empty;
            if (afterModificationDate != null)
            {
                sqlModdate = " and wo.wo_modificatiedatum > @minDate";
                parameters.Add(new DbParam() { Name = "@minDate", Value = afterModificationDate });
            }

            String sqlMain = String.Empty;
            String SqlSub = String.Empty;

            if (settings.UserType == WerkopdrachtUserType.supervisor)
            {
                sqlMain = " and (wo.wo_wodienst_code = '"+ settings.Dienst + "') ";
            }
            else
            {
                if (settings.woType == WerkopdrachtType.Werkaanvraag)
                {
                    sqlMain = String.Format(" {0} ", locGroepFilter);
                }
                else if (settings.woType == WerkopdrachtType.Werkbon)
                {
                    if (settings.UserType == WerkopdrachtUserType.excecuter)
                    {
                        if (settings.SubGroepMijnWBFilter.Count() == 1 || settings.UserMijnWBFilter.Count() == 1)//momenteel kunnen gebruikers maar tot 1 subgroep behoren
                        {
                            //techniekers mogen enkel werkbonnen zien wanneer geen begindatum is ingeven of de begindatum binnen een maand ligt
                            SqlSub += " and (wo.wo_begindatum  is null or wo.wo_begindatum <= DATEADD(day,31,GETDATE())) ";
                            //voor de werkbonnen
                            SqlSub += " and ((  wo.wo_parent_id is not null and @userid in (select wogeb_geb_id from werkopdracht_gebruikers where wogeb_wo_id = wo.wo_id))";
                            SqlSub += "  or  ( wo.wo_parent_id is not null and @subgroep in (select wogroep_geb_subgroep from werkopdracht_groepen where wogroep_wo_id = wo.wo_id))";
                            parameters.Add(new DbParam() { Name = "@userid", Value = settings.UserMijnWBFilter[0] });
                            parameters.Add(new DbParam() { Name = "@subgroep", Value = settings.SubGroepMijnWBFilter.Count == 1 ? settings.SubGroepMijnWBFilter[0] : "" });
                            //zelfde voor de Werkaanvragen, die moeten ook mee
                            SqlSub += " or (  wo.wo_parent_id is null and @userid2 in (select wogeb_geb_id from werkopdracht_gebruikers,werkopdracht w2 where w2.wo_parent_id = wo.wo_id and  wogeb_wo_id = w2.wo_id))";
                            SqlSub += "  or  ( wo.wo_parent_id is  null and @subgroep2 in (select wogroep_geb_subgroep from werkopdracht_groepen,werkopdracht w2 where w2.wo_parent_id = wo.wo_id and  wogroep_wo_id = w2.wo_id)))";
                            parameters.Add(new DbParam() { Name = "@userid2", Value = settings.UserMijnWBFilter[0] });
                            parameters.Add(new DbParam() { Name = "@subgroep2", Value = settings.SubGroepMijnWBFilter.Count == 1 ? settings.SubGroepMijnWBFilter[0] : "" });
                        }
                    }
                    else
                    {
                        //niks zien : op werkbon niveau mogen enkel supervisors of techniekers bonnen zien
                        sqlMain = String.Format(" and 0=1");
                    }
                }
            }

            String sqlHist = String.Empty;
            String sqlDienst = String.Empty;
            if (histtype == HistoryType.active)
                sqlHist = " and (wo_s_h.wosh_wos_id not in (9,10) or sh2.wosh_wos_id not in (9,10)) "; // enkel WA die niet gesloten zijn
            if (histtype == HistoryType.history)
                sqlHist = " and (wo_s_h.wosh_wos_id in (9,10) or sh2.wosh_wos_id in (9,10)) ";
            if (!String.IsNullOrWhiteSpace(settings.Dienst))
            {
                sqlDienst = " and wo.wo_wodienst_code = '" + settings.Dienst +  "'";

            }
            sql = sql + sqlMain + sqlModdate + sqlHist + sqlDienst + SqlSub;
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                return db.GetDataTable(sql, parameters.ToArray());
            }
        }
        public static DataTable GetEmailList(int userId)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                String Sql = @"SELECT dbo.getWerkopdrachtEmail(id) AS wo_email, email, id, voornaam, achternaam " +
                              "FROM vw_gebruikergroep WHERE groep_code IN (SELECT g2.groep_code FROM vw_gebruikergroep g2 WHERE g2.id = @userId)";
                return db.GetDataTable(Sql, new DbParam() { Name = "@userId", Value = userId });
            }
        }
        public static DataTable LoadPrioriteiten()
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                return db.GetDataTable(@"select * from werkopdracht_prioriteit order by wop_ordernum asc");
            }
        }
        public static DataTable GetStatusHistoriek(int woId)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                String sql = @"SELECT g.achternaam, g.voornaam, wosh_datum, wos_naam, wosh_opmerking " +
                              "FROM werkopdracht_status_historiek, werkopdracht_status, gebruiker AS g " +
                              "WHERE wosh_wos_id = wos_id AND g.id = wosh_geb_id AND wosh_wo_id = @wo_id " +
                              "ORDER BY wosh_datum desc";
                return db.GetDataTable(sql, new DbParam() { Name = "@wo_id", Value = woId });
            }
        }
        public static DataTable LoadTypes()
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                return db.GetDataTable(@"select * from werkopdracht_type order by wot_ordernum asc");
            }
        }
        public static DataTable LoadStatussen()
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                return db.GetDataTable(@"select * from werkopdracht_status");
            }
        }
        public static DataTable LoadDiensten()
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                return db.GetDataTable(@"select * from werkopdracht_diensten");
            }
        }
        public static void MarkReadByUser (int id, int userId)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                List<DbCommand> commandList = new List<DbCommand>();

                DbCommand comm = new DbCommand(DbCommandType.crud)
                {
                    Sql = "delete from werkopdracht_read where wo_id=@id and reader=@userid"
                };
                comm.Params.Add(new DbParam() { Name = "@id", Value = id }); 
                comm.Params.Add(new DbParam() { Name = "@userid", Value = userId });
                commandList.Add(comm);

                DbCommand comm2 = new DbCommand(DbCommandType.crud)
                {
                    Sql = "insert into werkopdracht_read (wo_id,reader,readdate) values (@id,@userid,GetDate())"
                };
                comm2.Params.Add(new DbParam() { Name = "@id", Value = id });
                comm2.Params.Add(new DbParam() { Name = "@userid", Value = userId });
                commandList.Add(comm2);

                try
                {
                    db.Excecute(commandList);
                }
                catch
                {
                    throw;
                }
            }
        }
        public static void SaveStatus(WerkopdrachtItem w, out int newWerkaanvraagStatusId)
        {
            DbParam[] paramList = {
                new DbParam() { Name = "@wo_id", Value = w.Id },
                new DbParam() { Name = "@wos_id", Value = w.StatusId },
                new DbParam() { Name = "@geb_id", Value = GlobalData.Instance.LoggedOnUser.Id },
                new DbParam() { Name = "@opmerking", Value = w.StatusOpmerking },
                new DbParam() { Name = "@o_wa_status_id", Direction = DbParamDirection.output, Size = 4, DbType = DbTypes.db_integer }
            };
            DbCommand command = new DbCommand(DbCommandType.procedure, "[saveWerkOpdrachtStatus]", paramList);

            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                try
                {
                    db.Excecute(command);
                    newWerkaanvraagStatusId = (int)command.Results["@o_wa_status_id"];
                }
                catch
                {
                    throw;
                }
            }
        }
        public static bool SaveWerkopdrachten(List<WerkopdrachtItem> modifiedItems)
        {
            List<DbCommand> commands = new List<DbCommand>();
            foreach (WerkopdrachtItem w in modifiedItems)
            {
                int? inventrarisId = null;
                if (w.InventarisId.GetValueOrDefault() != 0)
                    inventrarisId = w.InventarisId;

                DbParam[] paramList = {
                    new DbParam() { Name = "@retval", Direction = DbParamDirection.return_value },
                    new DbParam() { Name = "@id", Value = w.Id },
                    new DbParam() { Name = "@parentid", Value = w.Parent_id },
                    new DbParam() { Name = "@dienst", Value = w.Dienst },
                    new DbParam() { Name = "@gebruikerid", Value = GlobalData.Instance.LoggedOnUser.Id },
                    new DbParam() { Name = "@wot_id", Value = w.TypeId },
                    new DbParam() { Name = "@loc_id", Value = w.LocatieId },
                    new DbParam() { Name = "@titel", Value = w.Titel },
                    new DbParam() { Name = "@beschrijving", Value = w.Beschrijving },
                    new DbParam() { Name = "@prio_id", Value = w.PrioriteitId },
                    new DbParam() { Name = "@begindatum", Value = w.Begindatum },
                    new DbParam() { Name = "@einddatum", Value = w.Einddatum },
                    new DbParam() { Name = "@inv_id", Value = inventrarisId },
                    new DbParam() { Name = "@gebruikers", Value = w.MedewerkersIdString },
                    new DbParam() { Name = "@groepen", Value = w.GroepenString }
                };
                DbCommand com = new DbCommand(DbCommandType.procedure, "[saveWerkopdracht]", paramList);
                commands.Add(com);
            }

            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                try
                {
                    db.Excecute(commands);
                    int i = 0;
                    foreach (WerkopdrachtItem item in modifiedItems)
                    {
                        item.Id = (int)commands[i++].Results["@retval"];
                        item.IsModified = false;
                    }
                    return true;
                }
                catch (Exception ex)
                {
                    throw;
                }
            }
        }
    }
}
