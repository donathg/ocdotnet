﻿
using sharedcode.BLL;
using sharedcode.common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sharedcode.DAL
{
    public class UserDAL
    {
        public static DataTable GetUserLogonInfo(String persNr, String passWord)
        {
            /*String sql = @"SELECT DISTINCT p.VPERSCOD, p.VPERSNAAM, p.VPERSVRNM, g.id, g.wachtwoord, g.voornaam, g.achternaam, g.personeelscode, g.subgroep, g.email
                           FROM orbis.dbo.BPRPERSO p 
                           JOIN orbis.dbo.BPRHCONT c ON p.VPERSCOD = c.VPERSCOD
                           JOIN  gebruiker AS g ON p.VPERSCOD = g.personeelscode
                           WHERE ((GETDATE() between c.VHCONTRDATB and c.VHCONTRDATE or c.VHCONTRDATE is null)) 
                           AND p.VPERSCOD = @code";*/
            String sql = @"SELECT id, wachtwoord, voornaam, achternaam, personeelscode, subgroep, email, actif
                           FROM gebruiker
                           WHERE personeelscode = @code";

            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                return db.GetDataTable(sql, new DbParam() { Name = "@code", Value = persNr });
            }
        }
        public static void ResetPassword (int gebruikerId)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                DbCommand comm = new DbCommand(DbCommandType.crud)
                {
                    Sql = "UPDATE gebruiker SET wachtwoord = personeelscode WHERE id = @id"
                };
                comm.Params.Add(new DbParam() { Name = "@id", Value = gebruikerId });
                
                try
                {
                    db.Excecute(comm);                        
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
        /// <summary>
        /// Deze methode roept procedure aan die de gebruiker toevoegd in de online user tabel
        /// </summary>
        /// <param name="gebruikerId"></param>
        /// <param name="appInfo"></param>
        /// <param name="pcInfo"></param>
        public static void Logon(int gebruikerId, string appInfo, string pcInfo, out List<werkopdracht_diensten> WBDiensten, out List<string> superVisorDiensten, out List<string> excecuterDiensten)
        {
            DbCommand command = new DbCommand
            (
                DbCommandType.procedure,
                "[logon]",
                new DbParam() { Name = "@gebruikerid", Value = gebruikerId },
                new DbParam() { Name = "@appinfo", Value = appInfo },
                new DbParam() { Name = "@pcinfo", Value = pcInfo },
                new DbParam() { Name = "@o_supervisorWBDiensten", Direction = DbParamDirection.output, DbType= DbTypes.db_string, Size=100},
                new DbParam() { Name = "@o_excecuterWBDiensten", Direction = DbParamDirection.output, DbType = DbTypes.db_string, Size = 100 });


            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                try
                {
                    db.Excecute(command);

                    superVisorDiensten = command.Results["@o_supervisorWBDiensten"].ToString().Split(',').ToList();
                    excecuterDiensten = command.Results["@o_excecuterWBDiensten"].ToString().Split(',').ToList();

                    //Clean up empty entries
                    if (superVisorDiensten.Count == 1 && string.IsNullOrWhiteSpace(superVisorDiensten[0]))
                        superVisorDiensten.Clear();
                    if (excecuterDiensten.Count == 1 && string.IsNullOrWhiteSpace(excecuterDiensten[0]))
                        excecuterDiensten.Clear();
                    
                    using (Entities entity = new Entities(GlobalData.Instance.Entity))
                    {
                        WBDiensten =  entity.werkopdracht_diensten.ToList();                       
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
        public static void ChangePassword(string persNr, String paswoord)
        {
            DbCommand command = new DbCommand
            (
                DbCommandType.procedure,
                "[paswoordWijzigen]",
                new DbParam() { Name = "@persNr", Value = persNr },
                new DbParam() { Name = "@paswoord", Value = paswoord }
            );
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                try
                {
                    db.Excecute(command);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
        public static void Logoff(int gebruikerId)
        {
            DbCommand command = new DbCommand(DbCommandType.procedure, "[logoff]", new DbParam() { Name = "@gebruikerid", Value = gebruikerId });

            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                try
                {
                    db.Excecute(command);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
        public static DataTable GetUserRights(string persNr)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                string sql = @"SELECT groepgeb_groep_code, func_code, func_naam, func_cat, groepfunc_recht,groep_orbis, m.code, m.descr
                            FROM rechten_functie, rechten_groepenfuncties, gebruiker AS g, rechten_groepengebruikers,rechten_groep, module AS m
                            WHERE rechten_functie.func_code = rechten_groepenfuncties.groepfunc_func_code
                            AND g.id = rechten_groepengebruikers.groepgeb_geb_id
                            AND rechten_groepengebruikers.groepgeb_groep_code = rechten_groepenfuncties.groepfunc_groep_code
							AND groepgeb_groep_code = rechten_groep.groep_code 
							AND m.code = rechten_functie.module_code
                            AND g.personeelscode = @code";
                return db.GetDataTable(sql, new DbParam() { Name = "@code", Value = persNr });
            }
        }
        public static DataTable GetUserLocations(string persNr)
        {
           String  sql = @"SELECT groep_code, groep_naam, l.id AS loc_id, l.naam AS loc_naam, groeploc_dienst 
                           FROM vw_gebruikergroep, rechten_groepenlocaties, locatie AS l
                           WHERE groep_code = groeploc_groep_code AND l.id = groeploc_loc_id AND personeelscode = @code";
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                return db.GetDataTable(sql, new DbParam() { Name = "@code", Value = persNr });
            }
        }
        public static DataTable GetAllActiveUsers(params string[] groepcode)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                String sql = @"SELECT * FROM gebruiker WHERE personeelscode IN (SELECT VPERSCOD FROM vw_gebruikergroep_orbis WHERE VSUBDNSCOD NOT IN ('GEDET', 'CONVB'))";
                return db.GetDataTable(sql);
            }
        }
        public static DataTable  GetAllTechnicalSubgroups(String dienst)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                return db.GetDataTable("SELECT naam FROM werkopdracht_subgroepdefinities where dienst = @dienst ORDER BY naam ASC", new DbParam() { Name = "@dienst", Value = dienst });
            }
        }
        public static DataTable GetUsersFromGroep(string dienst)
        {
          
           
            String sql = @"SELECT * FROM vw_gebruikergroepall,werkopdracht_uitvoerder WHERE 
  groepgeb_groep_code = werkopdracht_uitvoerder.groep_code and werkopdracht_uitvoerder.dienst = @dienst";

            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                return db.GetDataTable(sql, new DbParam(){ Name = "@dienst", Value = dienst } );
            }
        }
    }
}
