﻿
using sharedcode.BLL;
using sharedcode.common;
using sharedcode.DAL;
using sharedcode.WeergaveClasses;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace sharedcode.DAL
{
    public class InventarisDAL
    {
        #region Leveranciers
        private static List<LeverancierItem> ConvertDatatableToListLeveranciers(DataTable dt)
        {
            List<LeverancierItem> tempList = new List<LeverancierItem>();
            foreach (DataRow dr in dt.Rows)
            {
                tempList.Add(new LeverancierItem()
                {
                    Id = (int)dr["lev_id"],
                    Naam = dr["lev_naam"].ToString()
                });
            }
            return tempList;
        }

        public static List<LeverancierItem> SelectAllLeveranciers()
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                string sql = @"SELECT * FROM leverancier ORDER BY lev_naam";
                DataTable dt = db.GetDataTable(sql);

                return ConvertDatatableToListLeveranciers(dt);
            }
        }
        public static LeverancierItem SelectLeverancierFromId(int id)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                string sql = @"SELECT * FROM leverancier WHERE lev_id = @id ORDER BY lev_naam";
                List<DbParam> paramList = new List<DbParam>
                {
                    new DbParam { Name = "@id", Value = id, DbType = DbTypes.db_integer }
                };

                DataTable dt = db.GetDataTable(sql, paramList.ToArray());

                return ConvertDatatableToListLeveranciers(dt).FirstOrDefault();
            }
        }
        #endregion
    }
    public class InventarisDALRefactor
    {
        #region Merken
        private static List<merk> ConvertDatatableToListMerken(DataTable dt)
        {
            List<merk> tempList = new List<merk>();
            foreach (DataRow dr in dt.Rows)
            {
                tempList.Add(new merk()
                {
                    id = (int)dr["id"],
                    naam = dr["naam"].ToString()
                });
            }
            return tempList;
        }

        public static List<merk> SelectAllMerken()
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                string sql = @"SELECT * FROM inventaris.merk ORDER BY naam";
                DataTable dt = db.GetDataTable(sql);

                return ConvertDatatableToListMerken(dt);
            }
        }
        public static merk SelectMerkFromId(int id)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                string sql = @"SELECT * FROM inventaris.merk WHERE id = @id ORDER BY naam";
                List<DbParam> paramList = new List<DbParam>
                {
                    new DbParam { Name = "@id", Value = id, DbType = DbTypes.db_integer }
                };

                DataTable dt = db.GetDataTable(sql, paramList.ToArray());

                return ConvertDatatableToListMerken(dt).FirstOrDefault();
            }
        }
        #endregion
        #region Toestellen
        private static List<toestel> ConvertDatatableToListToestellen(DataTable dt)
        {
            List<toestel> tempList = new List<toestel>();
            foreach (DataRow dr in dt.Rows)
            {
                tempList.Add(new toestel()
                {
                    id = (int)dr["id"],
                    naam = dr["naam"].ToString(),
                    category = dr["category"].ToString(),
                });
            }
            return tempList;
        }
        public static List<toestel> SelectAllToestellen()
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                string sql = @"SELECT * 
                               FROM inventaris.toestel 
                               ORDER BY category, CASE WHEN naam = 'andere' THEN 1 ELSE 0 END, naam";
                DataTable dt = db.GetDataTable(sql);

                return ConvertDatatableToListToestellen(dt);
            }
        }
        public static toestel SelectToestelFromId(int id)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                string sql = @"SELECT * 
                               FROM inventaris.toestel 
                               WHERE id = @id
                               ORDER BY category, CASE WHEN naam = 'andere' THEN 1 ELSE 0 END, naam";
                List<DbParam> paramList = new List<DbParam>
                {
                    new DbParam { Name = "@id", Value = id, DbType = DbTypes.db_integer }
                };

                DataTable dt = db.GetDataTable(sql, paramList.ToArray());

                return ConvertDatatableToListToestellen(dt).FirstOrDefault();
            }
        }
        #endregion
        #region InventarisWeergaves
        private static List<InventarisWeergave> ConvertDatatableToListInventarisItems(DataTable dt)
        {
            List<InventarisWeergave> tempList = new List<InventarisWeergave>();
            foreach (DataRow row in dt.Rows)
            {
                InventarisWeergave weergave = new InventarisWeergave()
                {
                    DoNotify = false,
                    id = (int)row["id"],
                    productOmschrijving = row["productOmschrijving"].ToString(),
                    creator = (int)row["creator"],
                    creationDate = (DateTime)row["creationDate"],
                    modifier = (int)row["modifier"],
                    modificationDate = (DateTime)row["modificationDate"],
                    actief = (bool)row["actief"],
                    Aantalbijlagen = (int)row["aantalBijlagen"]
                };
                //if (!row.IsNull("toestel_id") && row["toestel_id"] is int toestelId)
                //{
                //    weergave.toestel_id = toestelId;
                //    weergave.toestel = new toestel
                //    {
                //        id = toestelId,
                //        naam = row["toestelNaam"].ToString()
                //    };
                //}
                //if (!row.IsNull("merk_id") && row["merk_id"] is int merkId)
                //{
                //    weergave.merk_id = merkId;
                //    weergave.merk = new merk
                //    {
                //        id = merkId,
                //        naam = row["merkNaam"].ToString()
                //    };
                //}
                //if (!row.IsNull("locatie_id") && row["locatie_id"] is int locatieId)
                //{
                //    weergave.locatie_id = locatieId;
                //    weergave.LocatiePath = GebouwenBeheerLoader.GetLocationById(locatieId).GetFullPath();
                //}
                //if (!row.IsNull("leverancier_id") && row["leverancier_id"] is int leverancierId)
                //{
                //    weergave.leverancier_id = leverancierId;
                //    weergave.leverancier = new leverancier
                //    {
                //        lev_id = leverancierId,
                //        lev_naam = row["leverancierNaam"].ToString()
                //    };
                //}
                if (!row.IsNull("infoIT_id") && row["infoIT_id"] is int infoITId)
                {
                    weergave.infoIT_id = infoITId;
                    infoIT tempInfoIT = new infoIT()
                    {
                        id = infoITId,
                        ipHasDHCP = (bool)row["ipHasDHCP"]
                    };
                    if (!row.IsNull("datarack_id") && row["datarack_id"] is int datarackId)
                    {
                        tempInfoIT.datarack_id = datarackId;
                    }
                    if (!row.IsNull("deviceLogin") && row["deviceLogin"] is string deviceLogin)
                    {
                        tempInfoIT.deviceLogin = deviceLogin;
                    }
                    if (!row.IsNull("deviceURL") && row["deviceURL"] is string deviceURL)
                    {
                        tempInfoIT.deviceURL = deviceURL;
                    }
                    if (!row.IsNull("deviceWachtwoord") && row["deviceWachtwoord"] is string deviceWachtwoord)
                    {
                        tempInfoIT.deviceWachtwoord = deviceWachtwoord;
                    }
                    if (!row.IsNull("ipAdres") && row["ipAdres"] is string ipAdres)
                    {
                        tempInfoIT.ipAdres = ipAdres;
                    }
                    if (!row.IsNull("macAdres") && row["macAdres"] is string macAdres)
                    {
                        tempInfoIT.macAdres = macAdres;
                    }
                    if (!row.IsNull("patchPaneel") && row["patchPaneel"] is string patchPaneel)
                    {
                        tempInfoIT.patchPaneel = patchPaneel;
                    }
                    if (!row.IsNull("switchIpadres") && row["switchIpadres"] is string switchIpadres)
                    {
                        tempInfoIT.switchIpadres = switchIpadres;
                    }
                    if (!row.IsNull("switchPoort") && row["switchPoort"] is string switchPoort)
                    {
                        tempInfoIT.switchPoort = switchPoort;
                    }
                    if (!row.IsNull("vlan") && row["vlan"] is int vlan)
                    {
                        tempInfoIT.vlan = vlan;
                    }
                    if (!row.IsNull("wifiMacadres") && row["wifiMacadres"] is string wifiMacadres)
                    {
                        tempInfoIT.wifiMacadres = wifiMacadres;
                    }

                   // weergave.infoIT = tempInfoIT;
                }
                if (!row.IsNull("nummer_263") && row["nummer_263"] is string nummer263)
                {
                    weergave.nummer_263 = nummer263;
                }
                if (!row.IsNull("label") && row["label"] is string label)
                {
                    weergave.label = label;
                }
                if (!row.IsNull("productCode") && row["productCode"] is string productCode)
                {
                    weergave.productCode = productCode;
                }
                if (!row.IsNull("serieNummer") && row["serieNummer"] is string serieNummer)
                {
                    weergave.serieNummer = serieNummer;
                }
                if (!row.IsNull("bestelbon") && row["bestelbon"] is string bestelbon)
                {
                    weergave.bestelbon = bestelbon;
                }
                if (!row.IsNull("uitDienst") && row["uitDienst"] is DateTime uitDienst)
                {
                    weergave.uitDienst = uitDienst;
                }
                if (!row.IsNull("aankoopDatum") && row["aankoopDatum"] is DateTime aankoopDatum)
                {
                    weergave.aankoopDatum = aankoopDatum;
                }
                if (!row.IsNull("opmerking") && row["opmerking"] is string opmerking)
                {
                    weergave.opmerking = opmerking;
                }
                if (!row.IsNull("investering") && row["investering"] is decimal investering)
                {
                    weergave.investering = investering;
                }
                if (!row.IsNull("groep") && row["groep"] is string groep)
                {
                    weergave.groep = groep;
                }
                if (!row.IsNull("status") && row["status"] is string status)
                {
                    weergave.SetStatus(row["status"].ToString());
                }

                weergave.DoNotify = true;
                weergave.IsModified = false;

                tempList.Add(weergave);
            }

            return tempList;
        }

        public static List<InventarisWeergave> SelectAllInventarisItemsFromSettings(InventarisSettings settings)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                List<DbParam> paramList = new List<DbParam>();
                string sql = @" SELECT i.*, (SELECT count(*) from inventaris_files where inventaris_files.inv_id = i.id) as aantalBijlagen, t.naam AS toestelNaam, m.naam AS merkNaam, 
l.lev_naam As leverancierNaam,it.datarack_id, it.deviceLogin, it.deviceURL, it.deviceWachtwoord, it.ipAdres, it.ipHasDHCP, it.macAdres, it.patchPaneel, it.switchIpadres, it.switchPoort, it.vlan, it.wifiMacadres
                               FROM inventaris.inventaris AS i
                                    LEFT OUTER JOIN inventaris.infoIT as it ON i.infoIT_id = it.id
                                    LEFT OUTER JOIN dbo.leverancier AS l ON l.lev_id = i.leverancier_id
                                    LEFT OUTER JOIN inventaris.merk AS m ON m.id = i.merk_id
                                    LEFT OUTER JOIN inventaris.toestel AS t ON t.id = i.toestel_id
                                WHERE 1 = 1";
                if (settings.LocatieId.HasValue)
                {
                    sql += "    AND locatie_id = @locatieId";
                    paramList.Add(new DbParam() { Name = "@locatieId", Value = settings.LocatieId.Value });
                }
                if (settings.Geinventariseerd.HasValue)
                {
                    if (settings.Geinventariseerd.GetValueOrDefault())
                        sql += " AND i.id is not null";
                    else
                        sql += " AND i.id is null";
                }
                if (settings.LoadUitdienst.HasValue)
                {
                    if (settings.LoadUitdienst.GetValueOrDefault())
                        sql += " AND uitDienst is not null";
                    else
                        sql += " AND uitDienst is null";
                }
                if (settings.ToestelCategorien != null && settings.ToestelCategorien.Count > 0)
                {
                    string kommaSepString = string.Empty;
                    foreach (ToestelCategorieType type in settings.ToestelCategorien)
                    {
                        kommaSepString += "'" + type.ToString() + "',";
                    }
                    kommaSepString = kommaSepString.Remove(kommaSepString.Length - 1);

                    sql += $"    AND category in ({kommaSepString}) ";
                }
                if (settings.StatusTypes != null && settings.StatusTypes.Count > 0)
                {
                    string kommaSepString2 = string.Empty;
                    foreach (InventarisStatusType statusType in settings.StatusTypes)
                    {
                        string tempString = string.Empty;
                        switch (statusType)
                        {
                            case InventarisStatusType.Actief:
                                tempString = "A";
                                break;
                            case InventarisStatusType.Verkocht:
                                tempString = "S";
                                break;
                            case InventarisStatusType.Afgeschreven:
                                tempString = "C";
                                break;
                            case InventarisStatusType.Afgeboekt:
                                tempString = "W";
                                break;
                        }
                        kommaSepString2 += $"'{tempString}',";
                    }
                    kommaSepString2 = kommaSepString2.Remove(kommaSepString2.Length - 1);

                    sql += "    AND status in (" + kommaSepString2 + ") ";
                }

                DataTable dt = db.GetDataTable(sql, paramList.ToArray());

                List<InventarisWeergave> tempList = ConvertDatatableToListInventarisItems(dt);
                if (settings.AddEmptyRecord)
                    tempList.Insert(0, new InventarisWeergave() { id = 0, productOmschrijving = "Niet van toepassing" });

                return tempList;
            }
        }
        public static void SaveInventarisProcedure(List<InventarisWeergave> modifiedInventarisItems)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                try
                {
                    List<DbCommand> commands = new List<DbCommand>();
                    foreach (InventarisWeergave inv in modifiedInventarisItems)
                    {
                        List<DbParam> paramList = new List<DbParam>()
                        {
                            new DbParam() { Name = "@retval", Direction = DbParamDirection.return_value },
                            new DbParam() { Name = "@id", Value = inv.id, DbType = DbTypes.db_integer },
                            new DbParam() { Name = "@nummer263", Value = inv.nummer_263, DbType = DbTypes.db_string },
                            new DbParam() { Name = "@label", Value = inv.label, DbType = DbTypes.db_string },
                            new DbParam() { Name = "@tst_id", Value = inv.toestel_id, DbType = DbTypes.db_integer },
                            new DbParam() { Name = "@mer_id", Value = inv.merk_id, DbType = DbTypes.db_integer },
                            new DbParam() { Name = "@productcode", Value = inv.productCode, DbType = DbTypes.db_string },
                            new DbParam() { Name = "@produktomschrijving", Value = inv.productOmschrijving, DbType = DbTypes.db_string },
                            new DbParam() { Name = "@loc_id", Value = inv.locatie_id, DbType = DbTypes.db_integer },
                            new DbParam() { Name = "@serienummer", Value = inv.serieNummer, DbType = DbTypes.db_string },
                            new DbParam() { Name = "@bestelbon", Value = inv.bestelbon, DbType = DbTypes.db_string },
                            new DbParam() { Name = "@uitdienst", Value = inv.uitDienst, DbType = DbTypes.db_date },
                            new DbParam() { Name = "@lev_id", Value = inv.leverancier_id, DbType = DbTypes.db_integer },
                            new DbParam() { Name = "@aankoopdatum", Value = inv.aankoopDatum, DbType = DbTypes.db_date },
                            new DbParam() { Name = "@gebruikerid", Value = GlobalData.Instance.LoggedOnUser.Id.Value, DbType = DbTypes.db_integer },
                            new DbParam() { Name = "@opmerking", Value = inv.opmerking, DbType = DbTypes.db_string }
                        };
                        if (inv.infoIT != null)
                        {
                            paramList.Add(new DbParam() { Name = "@macadres", Value = inv.infoIT.macAdres, DbType = DbTypes.db_string });
                            paramList.Add(new DbParam() { Name = "@ipadres", Value = inv.infoIT.ipAdres, DbType = DbTypes.db_string });
                            paramList.Add(new DbParam() { Name = "@devicelogin", Value = inv.infoIT.deviceLogin, DbType = DbTypes.db_string });
                            paramList.Add(new DbParam() { Name = "@devicewachtwoord", Value = inv.infoIT.deviceWachtwoord, DbType = DbTypes.db_string });
                            paramList.Add(new DbParam() { Name = "@datarackId", Value = inv.infoIT.datarack_id, DbType = DbTypes.db_string });
                            paramList.Add(new DbParam() { Name = "@ipHasDCHP", Value = inv.infoIT.ipHasDHCP, DbType = DbTypes.db_auto });
                            paramList.Add(new DbParam() { Name = "@patchPaneel", Value = inv.infoIT.patchPaneel, DbType = DbTypes.db_string });
                            paramList.Add(new DbParam() { Name = "@deviceURL", Value = inv.infoIT.deviceURL, DbType = DbTypes.db_string });
                            paramList.Add(new DbParam() { Name = "@switchPoort", Value = inv.infoIT.switchPoort, DbType = DbTypes.db_string });
                            paramList.Add(new DbParam() { Name = "@switchIpadres", Value = inv.infoIT.switchIpadres, DbType = DbTypes.db_string });
                            paramList.Add(new DbParam() { Name = "@wifiMacadres", Value = inv.infoIT.wifiMacadres, DbType = DbTypes.db_string });
                            paramList.Add(new DbParam() { Name = "@vlan", Value = inv.infoIT.vlan, DbType = DbTypes.db_integer });
                        }

                        DbCommand com = new DbCommand(DbCommandType.procedure, "saveInventaris2", paramList.ToArray());
                        commands.Add(com);
                    }

                    db.Excecute(commands);

                    int i = 0;
                    foreach (InventarisWeergave item in modifiedInventarisItems)
                    {
                        item.id = (int)commands[i++].Results["@retval"];
                        item.IsModified = false;
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
        #endregion
        #region DatarackWeergaves
        private static List<LocatieobjectWeergave> ConvertDatatableToListDataracks(DataTable dt)
        {
            List<LocatieobjectWeergave> tempList = new List<LocatieobjectWeergave>();
            foreach (DataRow row in dt.Rows)
            {
                LocatieobjectWeergave obj = new LocatieobjectWeergave
                {
                    Id = (int)row["objectId"],
                    FullLocatiePath = row["objectLocatie"].ToString()
                };
                if (!row.IsNull("objectSerienummer") && row["objectSerienummer"] is string naam)
                {
                    obj.Naam = naam;
                }
                if (!row.IsNull("objectOpmerking") && row["objectOpmerking"] is string opmerking)
                {
                    obj.Opmerking = opmerking;
                }
                if (!row.IsNull("objectHoeveelheid") && row["objectHoeveelheid"] is string hoeveelheid)
                {
                    obj.Hoeveelheid = hoeveelheid;
                }
                if (!row.IsNull("objectEigenaar") && row["objectEigenaar"] is string eigenaar)
                {
                    obj.Eigenaar = eigenaar;
                }

                tempList.Add(obj);
            }

            return tempList;
        }
        public static List<LocatieobjectWeergave> SelectAllObjectenMetLocatie(int typeId)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                string sql = @" WITH items AS 
                                (
                                   SELECT l.id, l.parent_id, 0 AS level, CONVERT(VARCHAR(MAX), l.naam) AS path
                                   FROM locatie as l
                                   WHERE l.parent_id IS NULL

                                   UNION ALL

                                   SELECT l2.id, l2.parent_id, items.level + 1, path + ' > ' + l2.naam
                                   FROM locatie AS l2 
		                                INNER JOIN items ON items.id = l2.parent_id
                                )

                                SELECT lo.id AS objectId, lo.serienummer AS objectSerienummer, lo.opmerking AS objectOpmerking, lo.hoeveelheid AS objectHoeveelheid, lo.eigenaar AS objectEigenaar,
                                    i.path AS objectLocatie
                                FROM items AS i LEFT OUTER JOIN locatie_objecten AS lo ON i.id = lo.loc_id
                                WHERE lo.objecttype_id = @typeId
                                ORDER BY path";
                List<DbParam> paramList = new List<DbParam>
                {
                    new DbParam { Name = "@typeId", Value = typeId, DbType = DbTypes.db_integer }
                };

                DataTable dt = db.GetDataTable(sql, paramList.ToArray());

                return ConvertDatatableToListDataracks(dt);
            }
        }
        #endregion
        #region Locatie Objecten
        private static List<objecttype> ConvertDatatableToListObjecttypes(DataTable dt)
        {
            List<objecttype> tempList = new List<objecttype>();
            foreach (DataRow row in dt.Rows)
            {
                objecttype type = new objecttype
                {
                    id = (int)row["id"],
                    naam = row["naam"].ToString()
                };
                if (!row.IsNull("beschrijving"))
                    type.beschrijving = row["beschrijving"].ToString();

                tempList.Add(type);
            }

            return tempList;
        }

        public static List<objecttype> SelectAllObjecttypes()
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                string sql = @"SELECT * FROM objecttype";
                DataTable dt = db.GetDataTable(sql);

                return ConvertDatatableToListObjecttypes(dt);
            }
        }
        public static objecttype SelectObjecttypeFromId(int id)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                string sql = @"SELECT * FROM objecttype WHERE id = @id";
                List<DbParam> paramList = new List<DbParam>
                {
                    new DbParam { Name = "@id", Value = id, DbType = DbTypes.db_integer }
                };
                DataTable dt = db.GetDataTable(sql);

                return ConvertDatatableToListObjecttypes(dt).SingleOrDefault();
            }
        }
        #endregion
    }
}
