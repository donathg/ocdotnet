﻿using sharedcode.BLL;
using sharedcode.common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sharedcode.DAL
{
    public class LabelsDAL
    {
        private static List<labels> _allLabels = null;

        private static List<labels> ConvertDatatableToListLabels(DataTable dt)
        {
            List<labels> tempList = new List<labels>();

            foreach (DataRow row in dt.Rows)
            {
                labels label = new labels()
                {
                    id = (int)row["id"],
                    name = row["name"].ToString(),
                    module = row["module"].ToString(),
                    active = (bool)row["active"],
                    dagboek = (bool)row["dagboek"],
                    bijlagen = (bool)row["bijlagen"],
                    deadlinedate = (bool)row["deadlinedate"]
                };

                if (row["parentid"] is int parentId)
                    label.parentid = parentId;
                if (row["descr"] is string descr)
                    label.descr = descr;

                tempList.Add(label);
            }

            return tempList;
        }

        public static List<labels> GetAllLabels()
        {
            if (_allLabels == null)
            {
                using (Entities entity = new Entities(GlobalData.Instance.Entity))
                {
                    _allLabels = entity.labels.OrderBy(x => x.name).ToList();
                }
            }
            return _allLabels;
        }
        /// <summary>
        /// deze routine indien mogelijk niet gebruiken , maar wenden tot GlobalData.Instance.LoggedOnUser.Labels
        /// </summary>
        /// <param name="gebruikerId"></param>
        /// <returns></returns>
        public static List<labels> GetUserLabels(int userId)
        {
            List<labels> labels = new List<BLL.labels>();
            String sql = @"SELECT labels.* FROM rechten_groepenlabels, labels, gebruiker,rechten_groepengebruikers
                           WHERE rechten_groepenlabels.labelid = labels.id
                           AND rechten_groepengebruikers.groepgeb_geb_id = gebruiker.id
                           AND rechten_groepengebruikers.groepgeb_groep_code = rechten_groepenlabels.groepcode
                           AND gebruiker.id = @id";

            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                DataTable dt = db.GetDataTable(sql, new DbParam() { Name = "@id", Value = userId });
                foreach (DataRow dr in dt.Rows)
                {
                    labels l = (labels)GlobalSharedMethods.CreateItem<labels>(dr);
                    labels.Add(l);
                }
            }
            return labels;
        }
        /// <summary>
        ///     <para>Vormt datarow gekregen van DatabaseCentral om naar een labels-object.</para>
        ///     <para>Namen van de kolommen:</para>
        ///     <para>- label_id, label_name, label_descr, label_module, label_active, label_dagboek, label_bijlagen, label_deadlinedate, label_parentid</para>
        /// </summary>
        /// <param name="row">de datarow die omgevormt moet worden</param>
        /// <returns>labels_object</returns>
        public static labels CreateNewLabelFromDataRow(DataRow row)
        {
            labels label = new labels()
            {
                id = (int)row["label_id"],
                name = row["label_name"].ToString(),
                descr = row["label_descr"].ToString(),
                module = row["label_module"].ToString(),
                active = (bool)row["label_active"],
                dagboek = (bool)row["label_dagboek"],
                bijlagen = (bool)row["label_bijlagen"],
                deadlinedate = (bool)row["label_deadlinedate"]
            };
            if (row["label_parentid"] is int parentId)
                label.parentid = parentId;

            return label;
        }
        public static List<labels> GetAllSubLabelsFromLabelid(int id)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                List<DbParam> paramList = new List<DbParam>()
                {
                    new DbParam() { Name = "@id", Value = id, DbType = DbTypes.db_integer }
                };
                String sql = @"WITH EntityChildren AS
                               (
                                    SELECT * FROM labels WHERE id = @id

                                    UNION ALL

                                    SELECT l.* FROM labels l INNER JOIN EntityChildren l2 ON l.parentid = l2.id
                               )
                               SELECT * from EntityChildren WHERE active = 1";
                DataTable dt = db.GetDataTable(sql, paramList.ToArray());

                return ConvertDatatableToListLabels(dt);
            }
        }
    }

}
