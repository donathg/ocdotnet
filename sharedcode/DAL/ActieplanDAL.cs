﻿
using sharedcode.BLL;
using sharedcode.common;
using sharedcode.WeergaveClasses;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sharedcode.DAL
{
    public static class ActieplanDAL
    {
        #region Actieplan methods 
        private static List<ActieplanWeergave> ConvertDatatableToActieplanList(DataTable dt)
        {
            List<ActieplanWeergave> tempList = new List<ActieplanWeergave>();

            foreach (DataRow row in dt.Rows)
            {
                int actieplanId = (int)row["id"];
                ActieplanWeergave ap = tempList.Where(x => x.id == actieplanId).FirstOrDefault();
                
                if (ap == null)
                {
                    ap = new ActieplanWeergave()
                    {
                        id = actieplanId,
                        title = row["title"].ToString(),
                        deadline = (DateTime)row["deadline"],
                        category = row["category"].ToString(),
                        bewonerid = (int)row["bewonerid"],
                        creator_id = (int)row["creator_id"],
                        creationDate = (DateTime)row["creationDate"],
                        AangemaaktDoor = row["createdByName"].ToString(),

                        Actieplantypes = new List<actieplantypes>()
                    };
                    if (row["modifier"] is int mod)
                    {
                        ap.modifier = mod;
                        ap.VerandertDoor = row["modifiedByName"].ToString();
                    }
                    if (row["modificationdate"] is DateTime modDate)
                        ap.modificationdate = modDate;
                    if (row["afgewerktdoor"] is int fin)
                    {
                        ap.afgewerktdoor = fin;
                        ap.AfgewertkDoor = row["afgewerktByName"].ToString();
                    }
                    if (row["afgwerktdate"] is DateTime finDate)
                        ap.afgwerktdate = finDate;

                    tempList.Add(ap);
                }

                actieplantypes type = new actieplantypes()
                {
                    id = (int)row["typeId"],
                    name = row["typeName"].ToString(),
                    actif = true
                };
                ap.Actieplantypes.Add(type);
            }

            return tempList;
        }

        public static List<ActieplanWeergave> SelectAllActieplannen(ActieplanFilter filter)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                try
                {
                    List<DbParam> paramList = new List<DbParam>();
                    String sql = @" SELECT ap.*, g_af.voornaam + ' ' + g_af.achternaam AS 'afgewerktByName', g_mod.voornaam + ' ' + g_mod.achternaam AS 'modifiedByName', 
g_crea.voornaam + ' ' + g_crea.achternaam AS 'createdByName', apt.id AS 'typeId', apt.name AS 'typeName'
                                    FROM gebruiker AS g_crea, Actieplan_actieplantypes AS ap_apt, actieplantypes as apt, actieplan AS ap
                                    LEFT JOIN gebruiker AS g_af ON ap.afgewerktdoor = g_af.id
                                    LEFT JOIN gebruiker AS g_mod ON ap.modifier = g_mod.id
                                    WHERE ap.creator_id = g_crea.id
                                    AND ap.id = ap_apt.actieplan_id
                                    AND ap_apt.actieplantypes_id = apt.id
                                    AND apt.actif = 1";
                    if (!String.IsNullOrWhiteSpace(filter.Category))
                    {
                        paramList.Add(new DbParam() { Name = "@category", Value = filter.Category, DbType = DbTypes.db_string });
                        sql += "    AND ap.category = @category";
                    }
                    if (filter.BewonerId != 0)
                    {
                        paramList.Add(new DbParam() { Name = "@bewonerId", Value = filter.BewonerId, DbType = DbTypes.db_integer });
                        sql += "    AND ap.bewonerid = @bewonerId";
                    }
                    if (filter.IsAfgewerkt)
                        sql += "    AND ap.afgewerktdoor is not null";
                    else
                        sql += "    AND ap.afgewerktdoor is null";
                    sql += "        ORDER BY creationDate DESC";
                    DataTable dt = db.GetDataTable(sql, paramList.ToArray());

                    return ConvertDatatableToActieplanList(dt);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
        public static ActieplanWeergave SelectActieplanFromId(int id)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                try
                {
                    List<DbParam> paramList = new List<DbParam>()
                    {
                        new DbParam() { Name = "@id", Value = id, DbType = DbTypes.db_integer }
                    };
                    String sql = @" SELECT ap.*, g_af.voornaam + ' ' + g_af.achternaam AS 'afgewerktByName', g_mod.voornaam + ' ' + g_mod.achternaam AS 'modifiedByName', 
g_crea.voornaam + ' ' + g_crea.achternaam AS 'createdByName', apt.id AS 'typeId', apt.name AS 'typeName'
                                FROM gebruiker AS g_crea, Actieplan_actieplantypes AS ap_apt, actieplantypes as apt, actieplan AS ap
                                LEFT JOIN gebruiker AS g_af ON ap.afgewerktdoor = g_af.id
                                LEFT JOIN gebruiker AS g_mod ON ap.modifier = g_mod.id
                                WHERE ap.creator_id = g_crea.id
                                AND ap.id = ap_apt.actieplan_id
                                AND ap_apt.actieplantypes_id = apt.id
                                AND apt.actif = 1
                                AND ap.id = @id";
                    DataTable dt = db.GetDataTable(sql, paramList.ToArray());

                    return ConvertDatatableToActieplanList(dt).FirstOrDefault();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
        public static void InsertActieplan(ActieplanWeergave actieplan)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                DateTime huidigeTijd = DateTime.Now;
                int userId = GlobalData.Instance.LoggedOnUser.Id.Value; 

                try
                {
                    db.BeginTransaction();

                    List<DbParam> paramListMain = new List<DbParam>()
                    {
                        new DbParam() { Name = "@bewonerId", Value = actieplan.bewonerid, DbType = DbTypes.db_integer },
                        new DbParam() { Name = "@category", Value = actieplan.category, DbType = DbTypes.db_string },
                        new DbParam() { Name = "@title", Value = actieplan.title, DbType = DbTypes.db_string },
                        new DbParam() { Name = "@deadline", Value = actieplan.deadline, DbType = DbTypes.db_date },
                        new DbParam() { Name = "@creatorId", Value = userId, DbType = DbTypes.db_integer },
                        new DbParam() { Name = "@creationDate", Value = huidigeTijd, DbType = DbTypes.db_datetime }
                    };
                    String sqlMain = @" INSERT INTO actieplan (bewonerid, category, title, deadline, creator_id, creationDate)
                                    VALUES (@bewonerId, @category, @title, @deadline, @creatorId, @creationDate)";
                    DbCommand commMain = new DbCommand(DbCommandType.crud, sqlMain, paramListMain.ToArray());
                    int actieplanId = Convert.ToInt32(db.ExcecuteScalar(commMain));

                    List<DbCommand> commListTypes = new List<DbCommand>();
                    foreach (actieplantypes type in actieplan.Actieplantypes)
                    {
                        List<DbParam> paramListTypes = new List<DbParam>()
                        {
                            new DbParam() { Name = "@actieplanId", Value = actieplanId, DbType = DbTypes.db_integer },
                            new DbParam() { Name = "@ActieplanTypeId", Value = type.id, DbType = DbTypes.db_integer },
                            new DbParam() { Name = "@ToegevoegdOp", Value = huidigeTijd, DbType = DbTypes.db_datetime },
                            new DbParam() { Name = "@ToegevoegdDoor", Value = userId, DbType = DbTypes.db_integer }
                        };
                        String sqlTypes = @"INSERT INTO Actieplan_actieplantypes (actieplan_id, actieplantypes_id, toegevoegdOp, toegevoegdDoor)
                                            VALUES (@actieplanId, @ActieplanTypeId, @ToegevoegdOp, @ToegevoegdDoor)";
                        commListTypes.Add(new DbCommand(DbCommandType.crud, sqlTypes, paramListTypes.ToArray()));
                    }
                    db.Excecute(commListTypes);
                    db.CommitTransaction();
                }
                catch (Exception ex)
                {
                    db.RollbackTransaction();
                    throw ex;
                }
            }
        }
        public static void UpdateActieplan(ActieplanWeergave ap)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                int userId = GlobalData.Instance.LoggedOnUser.Id.Value;
                DateTime now = DateTime.Now;

                try
                {
                    db.BeginTransaction();

                    #region update actieplan
                    List<DbParam> paramList = new List<DbParam>()
                    {
                        new DbParam() { Name = "@id", Value = ap.id, DbType = DbTypes.db_integer },
                        new DbParam() { Name = "@title", Value = ap.title, DbType = DbTypes.db_string },
                        new DbParam() { Name = "@deadline", Value = ap.deadline, DbType = DbTypes.db_date },
                        new DbParam() { Name = "@modifierId", Value = userId, DbType = DbTypes.db_integer },
                        new DbParam() { Name = "@modificationDate", Value = now, DbType = DbTypes.db_datetime }
                    };
                    String sql = @" UPDATE actieplan
                                    SET title = @title, deadline = @deadline, modifier = @modifierId, modificationdate = @modificationDate 
                                    WHERE id = @id";
                    DbCommand comm = new DbCommand(DbCommandType.crud, sql, paramList.ToArray());

                    db.Excecute(comm);
                    #endregion

                    #region Delete all types van dat actieplan uit de tussentabel
                    String sqlDelete = @"DELETE FROM Actieplan_actieplantypes WHERE actieplan_id = @actieplanId;";
                    List<DbParam> paramListDelete = new List<DbParam>()
                    {
                        new DbParam() { Name = "@actieplanId", Value = ap.id, DbType = DbTypes.db_integer }
                    };
                    DbCommand commDelete = new DbCommand(DbCommandType.crud, sqlDelete, paramListDelete.ToArray());

                    db.Excecute(commDelete);
                    #endregion

                    #region Insert new types into tussentabel

                    foreach (actieplantypes type in ap.Actieplantypes)
                    {
                        String sqlInsert = @"   INSERT INTO Actieplan_actieplantypes (actieplan_id, actieplantypes_id, toegevoegdOp, toegevoegdDoor)
                                                VALUES (@actieplanId, @typeId, @creationDate, @creatorId)";
                        List<DbParam> paramListInsert = new List<DbParam>()
                        {
                            new DbParam() { Name = "@actieplanId", Value = ap.id, DbType = DbTypes.db_integer },
                            new DbParam() { Name = "@typeId", Value = type.id, DbType = DbTypes.db_integer },
                            new DbParam() { Name = "@creationDate", Value = now, DbType = DbTypes.db_datetime },
                            new DbParam() { Name = "@creatorId", Value = userId, DbType = DbTypes.db_integer }
                        };
                        DbCommand commInsert = new DbCommand(DbCommandType.crud, sqlInsert, paramListInsert.ToArray());

                        db.Excecute(commInsert);
                    }
                    #endregion

                    db.CommitTransaction();
                }
                catch (Exception ex)
                {
                    db.RollbackTransaction();
                    throw ex;
                }
            }
        }
        public static void CloseActieplan(ActieplanWeergave ap)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                try
                {
                    String sql = @" UPDATE actieplan SET afgewerktdoor = @afgewerktDoor, afgwerktdate = @afgewerktOp WHERE id = @id";
                    List<DbParam> paramList = new List<DbParam>()
                        {
                            new DbParam() { Name = "@id", Value = ap.id, DbType = DbTypes.db_integer },
                            new DbParam() { Name = "@afgewerktDoor", Value = GlobalData.Instance.LoggedOnUser.Id.Value, DbType = DbTypes.db_integer },
                            new DbParam() { Name = "@afgewerktOp", Value = DateTime.Now, DbType = DbTypes.db_datetime }
                        };
                    DbCommand comm = new DbCommand(DbCommandType.crud, sql, paramList.ToArray());
                    db.Excecute(comm);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
        public static void DeleteActieplan(ActieplanWeergave selectedActieplan)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                List<DbParam> paramList = new List<DbParam>()
                {
                    new DbParam() { Name = "@id", Value = selectedActieplan.id, DbType = DbTypes.db_integer }
                };
                List<DbCommand> commList = new List<DbCommand>();

                String sqlOpvolging = @" DELETE FROM actieplanopvolging WHERE actieplanid = @id";
                DbCommand commOpvolging = new DbCommand(DbCommandType.crud, sqlOpvolging, paramList.ToArray());
                commList.Add(commOpvolging);

                String sqlactieplan = @"DELETE FROM actieplan WHERE id = @id";
                DbCommand commActieplan = new DbCommand(DbCommandType.crud, sqlactieplan, paramList.ToArray());
                commList.Add(commActieplan);

                try
                {
                    db.Excecute(commList);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
        #endregion
        #region ActieplanOpvolging methods
        private static List<ActieplanOpvolgingWeergave> ConvertDatatableToActieplanOpvolging(DataTable dt)
        {
            List<ActieplanOpvolgingWeergave> tempList = new List<ActieplanOpvolgingWeergave>();

            foreach (DataRow row in dt.Rows)
            {
                ActieplanOpvolgingWeergave opvolging = new ActieplanOpvolgingWeergave()
                {
                    id = (int)row["id"],
                    actieplanid = (int)row["actieplanid"],
                    watlooptgoed = row["watlooptgoed"].ToString(),
                    waaraanwerken = row["waaraanwerken"].ToString(),
                    hoeaanpakken = row["hoeaanpakken"].ToString(),
                    watIsGedaan = row["watIsGedaan"].ToString(),
                    creator_id = (int)row["creator_id"],
                    creationDate = (DateTime)row["creationDate"],
                    AangemaaktDoor = row["createdByName"].ToString()
                };
                if (row["modifier"] is int mod)
                    opvolging.modifier = mod;
                if (row["modificationdate"] is DateTime modDate)
                    opvolging.modificationdate = modDate;

                tempList.Add(opvolging);
            }

            return tempList;
        }

        public static List<ActieplanOpvolgingWeergave> SelectAllActieplanOpvolgingen(ActieplanOpvolgingFilter filter)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                try
                {
                    List<DbParam> paramList = new List<DbParam>();
                    String sql = @" SELECT apo.*, g_crea.voornaam + ' ' + g_crea.achternaam AS 'createdByName', g_mod.voornaam + ' ' + g_mod.achternaam AS 'modifiedByName'
                                FROM actieplanopvolging AS apo LEFT JOIN gebruiker AS g_mod ON g_mod.id = apo.modifier, gebruiker AS g_crea
                                WHERE apo.creator_id = g_crea.id";
                    if (filter.ActieplanId != 0)
                    {
                        paramList.Add(new DbParam() { Name = "@actieplanId", Value = filter.ActieplanId, DbType = DbTypes.db_integer });
                        sql += @"   AND apo.actieplanid = @actieplanId";
                    }

                    DataTable dt = db.GetDataTable(sql, paramList.ToArray());

                    return ConvertDatatableToActieplanOpvolging(dt);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
        public static ActieplanOpvolgingWeergave SelectActieplanOpvolgingFromId(int id)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                try
                {
                    String sql = @" SELECT apo.*, g_crea.voornaam + ' ' + g_crea.achternaam AS 'createdByName', g_mod.voornaam + ' ' + g_mod.achternaam AS 'modifiedByName'
                                FROM actieplanopvolging AS apo LEFT JOIN gebruiker AS g_mod ON g_mod.id = apo.modifier, gebruiker AS g_crea
                                WHERE apo.creator_id = g_crea.id
                                AND id = @id";
                    List<DbParam> paramList = new List<DbParam>()
                    {
                        new DbParam() { Name = "@id", Value = id, DbType = DbTypes.db_integer }
                    };

                    DataTable dt = db.GetDataTable(sql, paramList.ToArray());

                    return ConvertDatatableToActieplanOpvolging(dt).SingleOrDefault();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
        public static void InsertActieplanOpvolging(actieplanopvolging opvolging)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                try
                {
                    String sql = @" INSERT INTO actieplanopvolging(actieplanid, watlooptgoed, waaraanwerken, hoeaanpakken, watIsGedaan, creator_id, creationDate)
                                    VALUES (@actieplanId, @watLoopGoed, @waaraanWerken, @hoeAanpakken, @watIsGedaan, @creatorId, @creationDate)";
                    List<DbParam> paramList = new List<DbParam>()
                    {
                        new DbParam() { Name = "@actieplanId", Value = opvolging.actieplanid, DbType = DbTypes.db_integer },
                        new DbParam() { Name = "@watLoopGoed", Value = opvolging.watlooptgoed, DbType = DbTypes.db_string },
                        new DbParam() { Name = "@waaraanWerken", Value = opvolging.waaraanwerken, DbType = DbTypes.db_string },
                        new DbParam() { Name = "@hoeAanpakken", Value = opvolging.hoeaanpakken, DbType = DbTypes.db_string },
                        new DbParam() { Name = "@watIsGedaan", Value = opvolging.watIsGedaan, DbType = DbTypes.db_string },
                        new DbParam() { Name = "@creatorId", Value = GlobalData.Instance.LoggedOnUser.Id.Value, DbType = DbTypes.db_integer },
                        new DbParam() { Name = "@creationDate", Value = DateTime.Now, DbType = DbTypes.db_datetime }
                    };
                    DbCommand comm = new DbCommand(DbCommandType.crud, sql, paramList.ToArray());

                    db.Excecute(comm);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
        public static void UpdateActieplanOpvolging(actieplanopvolging opvolging)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                try
                {
                    String sql = @" UPDATE actieplanopvolging
                                    SET watlooptgoed = @watLooptGoed, waaraanwerken = @waaraanWerken, watIsGedaan = @watIsGedaan, hoeaanpakken = @hoeAanpakken, modifier = @modifier, 
modificationdate = @modificationDate
                                    WHERE id = @id";
                    List<DbParam> paramList = new List<DbParam>()
                    {
                        new DbParam() { Name = "@id", Value = opvolging.id, DbType = DbTypes.db_integer },
                        new DbParam() { Name = "@watLooptGoed", Value = opvolging.watlooptgoed, DbType = DbTypes.db_string },
                        new DbParam() { Name = "@waaraanWerken", Value = opvolging.waaraanwerken, DbType = DbTypes.db_string },
                        new DbParam() { Name = "@hoeAanpakken", Value = opvolging.hoeaanpakken, DbType = DbTypes.db_string },
                        new DbParam() { Name = "@watIsGedaan", Value = opvolging.watIsGedaan, DbType = DbTypes.db_string },
                        new DbParam() { Name = "@modifier", Value = GlobalData.Instance.LoggedOnUser.Id.Value, DbType = DbTypes.db_integer },
                        new DbParam() { Name = "@modificationDate", Value = DateTime.Now, DbType = DbTypes.db_datetime }
                    };
                    DbCommand comm = new DbCommand(DbCommandType.crud, sql, paramList.ToArray());

                    db.Excecute(comm);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
        public static void DeleteActieplanOpvolging(actieplanopvolging opvolging)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                try
                {
                    String sql = @"DELETE FROM actieplanopvolging WHERE id = @id";
                    List<DbParam> paramList = new List<DbParam>()
                    {
                        new DbParam() { Name = "@id", Value = opvolging.id, DbType = DbTypes.db_integer }
                    };
                    DbCommand comm = new DbCommand(DbCommandType.crud, sql, paramList.ToArray());

                    db.Excecute(comm);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
        #endregion
        #region  actieplantypes methods
        private static List<actieplantypes> ConvertDatatableToActieplanTypesList(DataTable dt)
        {
            List<actieplantypes> tempList = new List<actieplantypes>();
            foreach (DataRow row in dt.Rows)
            {
                tempList.Add(new actieplantypes()
                {
                    id = (int)row["id"],
                    name = row["name"].ToString(),
                    actif = (bool)row["actif"]
                });
            }

            return tempList;
        }

        public static List<actieplantypes> SelectAllActieplanTypes(bool? isActief)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                try
                {
                    String sql = @"SELECT * FROM actieplantypes";
                    if (isActief.HasValue)
                    {
                        sql += " WHERE actif = ";

                        if (isActief.Value)
                            sql += "1";
                        else
                            sql += "0";
                    }
                    DataTable dt = db.GetDataTable(sql);

                    return ConvertDatatableToActieplanTypesList(dt);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
        public static actieplantypes SelectActieplanTypeFromId(int id)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                try
                {
                    String sql = @"SELECT * FROM actieplantypes WHERE id = @id";
                    List<DbParam> paramList = new List<DbParam>()
                    {
                        new DbParam() { Name = "@id", Value = id, DbType = DbTypes.db_integer }
                    };
                    DataTable dt = db.GetDataTable(sql, paramList.ToArray());

                    return ConvertDatatableToActieplanTypesList(dt).SingleOrDefault();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
        #endregion
    }
}
