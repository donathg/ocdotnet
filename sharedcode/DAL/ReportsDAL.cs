﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using sharedcode.common;

namespace sharedcode.DAL
{
    public class ReportsDAL
    {


        public static DataTable GetTijdsRegistratieRapport()
        {
            String SQL =
                @"SELECT        'RTH' AS Afdeling, VDATUM Datum, VBEWOMS Bewoner, CASE WHEN vbegeleiding = '1' THEN 'eigen begeleiding' ELSE '' END AS begeleiding, 
                         CASE WHEN vtype = '0' THEN 'ambulant' ELSE 'mobiel' END AS 'Type', 
                         CASE WHEN vaanrekenen = 1 THEN 'aanrekenen' ELSE 'niet aanrekenen' END AS aanrekenen, vuurvan AS van, vuurtot AS tot, vuurtot - vuurvan AS tijd, 
                         vmemo AS memo, vbegeleider, BBWBEWON.VBEWNAAM, BBWBEWON.VBEWVRNM
FROM            orbis.dbo.BRTHBGL LEFT JOIN
                         orbis.dbo.BBWBEWON ON BBWBEWON.VBEWNRRYKS = BRTHBGL.VBEWNRRYKS JOIN
                         orbis.dbo.BBWHMULE ON BBWBEWON.VBEWCOD = BBWHMULE.VBEWCOD AND GETDATE() BETWEEN VHMUTDATB AND ISNULL(VHMUTDATE, GETDATE() + 1)
UNION
SELECT        'MFC' AS Afdeling, VDATUM Datum, VBEWOMS Bewoner, '' AS begeleiding, CASE WHEN vtype = '0' THEN 'ambulant' ELSE 'mobiel' END AS 'Type', 
                         CASE WHEN vaanrekenen = 1 THEN 'aanrekenen' ELSE 'niet aanrekenen' END AS aanrekenen, vuurvan AS van, vuurtot AS tot, vuurtot - vuurvan AS tijd, 
                         vmemo AS memo, vbegeleider, BBWBEWON.VBEWNAAM, BBWBEWON.VBEWVRNM
FROM            orbis.dbo.BMFCREGD LEFT JOIN
                         orbis.dbo.BBWBEWON ON BBWBEWON.VBEWNRRYKS = BMFCREGD.VBEWNRRYKS JOIN
                         orbis.dbo.BBWHMULE ON BBWBEWON.VBEWCOD = BBWHMULE.VBEWCOD AND GETDATE() BETWEEN VHMUTDATB AND ISNULL(VHMUTDATE, GETDATE() + 1)
UNION
SELECT        'FAM' AS Afdeling, VDATUM Datum, VBEWOMS, '' AS begeleiding, CASE WHEN vtype = '0' THEN 'ambulant' ELSE 'mobiel' END AS 'Type', '' AS aanrekenen, 
                         vuurvan AS van, vuurtot AS tot, vuurtot - vuurvan AS tijd, vmemo AS memo, vbegeleider, '', ''
FROM            orbis.dbo.BFAMBGL LEFT JOIN
                         orbis.dbo.BBWBEWON ON BBWBEWON.VBEWNRRYKS = BFAMBGL.VBEWNRRYKS JOIN
                         orbis.dbo.BBWHMULE ON BBWBEWON.VBEWCOD = BBWHMULE.VBEWCOD AND GETDATE() BETWEEN VHMUTDATB AND ISNULL(VHMUTDATE, GETDATE() + 1)";

            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                return db.GetDataTable(SQL);
            }

        }
    }
}
