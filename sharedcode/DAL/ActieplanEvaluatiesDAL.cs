﻿using sharedcode.BLL;
using sharedcode.common;
using sharedcode.WeergaveClasses;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sharedcode.DAL
{
    public enum ActieplanEvaluatieVersies
    {
        EINDEVALUATIE,
        PERIODIEKE_EVALUATIE
    }

    public static class ActieplanEvaluatiesDAL
    {
        #region Actieplan_Resultaat methods
        private static List<Actieplan_Resultaat> ConvertDatatableToActieplanResultaat(DataTable dt)
        {
            List<Actieplan_Resultaat> resultatenList = new List<Actieplan_Resultaat>();
            foreach (DataRow row in dt.Rows)
            {
                resultatenList.Add(new Actieplan_Resultaat()
                {
                    id = (int)row["id"],
                    naam = row["naam"].ToString(),
                    beschrijving = row["beschrijving"].ToString()
                });
            }
            return resultatenList;
        }

        public static List<Actieplan_Resultaat> SelectAllActieplanResultaten()
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                String sql = @"SELECT * FROM Actieplan_Resultaat";

                DataTable dt = db.GetDataTable(sql);

                return ConvertDatatableToActieplanResultaat(dt);
            }
        }
        public static Actieplan_Resultaat SelectActieplanResultaatFromId(int id)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                List<DbParam> paramList = new List<DbParam>()
                {
                    new DbParam() { Name = "@id", Value = id, DbType = DbTypes.db_integer}
                };
                String sql = @"SELECT * FROM Actieplan_Resultaat WHERE id= @id";

                DataTable dt = db.GetDataTable(sql, paramList.ToArray());

                return ConvertDatatableToActieplanResultaat(dt).SingleOrDefault();
            }
        }
        #endregion
        #region Actieplan_Evaluaties methods
        private static List<ActieplanEvaluatiesWeergave> ConvertDatatableToActieplanEvaluaties(DataTable dt)
        {
            List<ActieplanEvaluatiesWeergave> tempList = new List<ActieplanEvaluatiesWeergave>();
            foreach (DataRow row in dt.Rows)
            {
                int actieplanId = (int)row["id"];
                ActieplanEvaluatiesWeergave eval = tempList.Where(x => x.id == actieplanId).FirstOrDefault();

                if (eval == null)
                {
                    eval = new ActieplanEvaluatiesWeergave()
                    {
                        id = (int)row["id"],
                        bewoner_id = (int)row["bewoner_id"],
                        resultaat_id = (int)row["resultaat_id"],
                        titel = row["titel"].ToString(),
                        hoeVerliepHet = row["hoeVerliepHet"].ToString(),
                        datum_evaluatie = (DateTime)row["datum_evaluatie"],
                        creator = (int)row["creator"],
                        CreatorName = row["creator_name"].ToString(),
                        creation_date = (DateTime)row["creation_date"],
                        modifier = (int)row["modifier"],
                        modification_date = (DateTime)row["modification_date"],
                        Actieplantypes = new List<actieplantypes>()
                    };
                    if (row["actieplan_id"] is int ActieplanId)
                        eval.actieplan_id = ActieplanId;
                    if (row["start_datum"] is DateTime start)
                        eval.start_datum = start;
                    if (row["eind_datum"] is DateTime eind)
                        eval.eind_datum = eind;

                    tempList.Add(eval);
                }

                actieplantypes type = new actieplantypes()
                {
                    id = (int)row["typeId"],
                    name = row["typeName"].ToString(),
                    actif = true
                };
                eval.Actieplantypes.Add(type);
            }
            return tempList;
        }


        public static List<ActieplanEvaluatiesWeergave> SelectAllEvaluatiesFromBewoner(ActieplanEvaluatieVersies versie, int bewonerId)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                List<DbParam> paramList = new List<DbParam>()
                {
                    new DbParam() { Name = "@id", Value = bewonerId, DbType = DbTypes.db_integer }
                };
                String sql = @"SELECT e.*,(g.voornaam + ' ' + g.achternaam) AS creator_name, apt.id AS typeId, apt.name AS typeName
                               FROM Actieplan_Evaluaties AS e, gebruiker As g, ActieplanEvaluaties_actieplantypes AS e_apt, actieplantypes as apt
                               WHERE g.id = e.creator 
                               AND e.id = e_apt.actieplanEvaluatie_id
                               AND e_apt.actieplantype_id = apt.id
                               AND apt.actif = 1
							   AND e.actieplan_id IS ";

                if (versie != ActieplanEvaluatieVersies.PERIODIEKE_EVALUATIE)
                    sql += "   NOT ";

                sql += "       NULL AND bewoner_id=@id ORDER BY e.datum_evaluatie DESC";

                DataTable dt = db.GetDataTable(sql, paramList.ToArray());
                return ConvertDatatableToActieplanEvaluaties(dt);
            }
        }
        public static ActieplanEvaluatiesWeergave SelectEindEvaluatieFromActieplanId(int actieplanId)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                List<DbParam> paramList = new List<DbParam>()
                {
                    new DbParam() { Name = "@actieplanId", Value = actieplanId, DbType = DbTypes.db_integer }
                };
                String sql = @"SELECT e.*,(g.voornaam + ' ' + g.achternaam) AS creator_name, apt.id AS typeId, apt.name AS typeName
                               FROM Actieplan_Evaluaties AS e, gebruiker As g, ActieplanEvaluaties_actieplantypes AS e_apt, actieplantypes as apt
                               WHERE g.id = e.creator 
                               AND e.id = e_apt.actieplanEvaluatie_id
                               AND e_apt.actieplantype_id = apt.id
                               AND apt.actif = 1
							   AND e.actieplan_id = @actieplanId
                               ORDER BY e.datum_evaluatie DESC";

                DataTable dt = db.GetDataTable(sql, paramList.ToArray());
                return ConvertDatatableToActieplanEvaluaties(dt).SingleOrDefault();
            }
        }
        public static ActieplanEvaluatiesWeergave SelectActieplanEvaluatieFromId(int id)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                List<DbParam> paramList = new List<DbParam>()
                {
                    new DbParam() { Name = "@id", Value = id, DbType = DbTypes.db_integer }
                };
                String sql = @"SELECT e.*,(g.voornaam + ' ' + g.achternaam) AS creator_name FROM Actieplan_Evaluaties AS e, gebruiker As g WHERE g.id = e.creator AND id=@id";

                DataTable dt = db.GetDataTable(sql, paramList.ToArray());
                return ConvertDatatableToActieplanEvaluaties(dt).SingleOrDefault();
            }
        }
        public static void InsertEvaluatie(ActieplanEvaluatiesWeergave evaluatie)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                DateTime huidigeTijd = DateTime.Now;
                int userId = GlobalData.Instance.LoggedOnUser.Id.Value;
                if (evaluatie.actieplan_id.HasValue)
                    evaluatie.titel = ActiePlanBLL.GetActieplanFromId(evaluatie.actieplan_id.Value).title + " ";
                else
                    evaluatie.titel = "Periodieke Evaluatie op ";
                evaluatie.titel += evaluatie.datum_evaluatie.ToShortDateString();

                try
                {
                    db.BeginTransaction();

                    List<DbParam> paramList = new List<DbParam>()
                    {
                        new DbParam() { Name = "@actieplan_id", Value = evaluatie.actieplan_id, DbType = DbTypes.db_integer},
                        new DbParam() { Name = "@bewoner_id", Value = evaluatie.bewoner_id, DbType = DbTypes.db_integer},
                        new DbParam() { Name = "@resultaat_id", Value = evaluatie.resultaat_id, DbType = DbTypes.db_integer},
                        new DbParam() { Name = "@titel", Value = evaluatie.titel, DbType = DbTypes.db_string},
                        new DbParam() { Name = "@hoeVerliepHet", Value = evaluatie.hoeVerliepHet, DbType = DbTypes.db_string},
                        new DbParam() { Name = "@datum_evaluatie", Value = evaluatie.datum_evaluatie, DbType = DbTypes.db_date},
                        new DbParam() { Name = "@start_datum", Value = evaluatie.start_datum, DbType = DbTypes.db_date},
                        new DbParam() { Name = "@eind_datum", Value = evaluatie.eind_datum, DbType = DbTypes.db_date},
                        new DbParam() { Name = "@creator", Value = userId, DbType = DbTypes.db_integer},
                        new DbParam() { Name = "@creation_date", Value = huidigeTijd, DbType = DbTypes.db_datetime},
                        new DbParam() { Name = "@modifier", Value = userId, DbType = DbTypes.db_integer},
                        new DbParam() { Name = "@modification_date", Value = huidigeTijd, DbType = DbTypes.db_datetime},
                    };
                    String sql = @"INSERT INTO Actieplan_Evaluaties(actieplan_id, bewoner_id, resultaat_id, titel, hoeVerliepHet, datum_evaluatie, 
start_datum, eind_datum, creator, creation_date, modifier, modification_date)
                                   VALUES(@actieplan_id, @bewoner_id, @resultaat_id, @titel, @hoeVerliepHet, @datum_evaluatie, @start_datum, 
@eind_datum, @creator, @creation_date, @modifier, @modification_date)";
                    DbCommand commMain = new DbCommand(DbCommandType.crud, sql, paramList.ToArray());
                    int actieplanId = Convert.ToInt32(db.ExcecuteScalar(commMain));

                    List<DbCommand> commListTypes = new List<DbCommand>();
                    foreach (actieplantypes type in evaluatie.Actieplantypes)
                    {
                        List<DbParam> paramListTypes = new List<DbParam>()
                        {
                            new DbParam() { Name = "@actieplanId", Value = actieplanId, DbType = DbTypes.db_integer },
                            new DbParam() { Name = "@ActieplanTypeId", Value = type.id, DbType = DbTypes.db_integer },
                            new DbParam() { Name = "@CreationDate", Value = huidigeTijd, DbType = DbTypes.db_datetime },
                            new DbParam() { Name = "@CreatorId", Value = userId, DbType = DbTypes.db_integer }
                        };
                        String sqlTypes = @"INSERT INTO ActieplanEvaluaties_actieplantypes (actieplanEvaluatie_id, actieplantype_id, creator_id, creationDate)
                                            VALUES (@actieplanId, @ActieplanTypeId, @CreatorId, @CreationDate)";
                        commListTypes.Add(new DbCommand(DbCommandType.crud, sqlTypes, paramListTypes.ToArray()));
                    }
                    db.Excecute(commListTypes);
                    db.CommitTransaction();
                }
                catch (Exception ex)
                {
                    db.RollbackTransaction();
                    throw ex;
                }
            }
        }
        public static void UpdateEvaluatie(ActieplanEvaluatiesWeergave evaluatie)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                int userId = GlobalData.Instance.LoggedOnUser.Id.Value;
                DateTime now = DateTime.Now;
                if (evaluatie.actieplan_id.HasValue)
                    evaluatie.titel = ActiePlanBLL.GetActieplanFromId(evaluatie.actieplan_id.Value).title + " ";
                else
                    evaluatie.titel = "Periodieke Evaluatie op ";
                evaluatie.titel += evaluatie.datum_evaluatie.ToShortDateString();

                try
                {
                    db.BeginTransaction();

                    #region update Actieplan_Evaluaties
                    List<DbParam> paramList = new List<DbParam>()
                    {
                        new DbParam() { Name = "@id", Value = evaluatie.id, DbType = DbTypes.db_integer },
                        new DbParam() { Name = "@resultaat_id", Value = evaluatie.resultaat_id, DbType = DbTypes.db_integer },
                        new DbParam() { Name = "@titel", Value = evaluatie.titel, DbType = DbTypes.db_string },
                        new DbParam() { Name = "@hoeVerliepHet", Value = evaluatie.hoeVerliepHet, DbType = DbTypes.db_string },
                        new DbParam() { Name = "@datum_evaluatie", Value = evaluatie.datum_evaluatie, DbType = DbTypes.db_date },
                        new DbParam() { Name = "@start_datum", Value = evaluatie.start_datum, DbType = DbTypes.db_date },
                        new DbParam() { Name = "@eind_datum", Value = evaluatie.eind_datum, DbType = DbTypes.db_date },
                        new DbParam() { Name = "@modifier", Value = userId, DbType = DbTypes.db_integer },
                        new DbParam() { Name = "@modification_date", Value = now, DbType = DbTypes.db_datetime },
                    };
                    String sql = @"UPDATE Actieplan_Evaluaties 
                               SET resultaat_id=@resultaat_id, titel=@titel, hoeVerliepHet=@hoeVerliepHet, datum_evaluatie=@datum_evaluatie,
start_datum=@start_datum, eind_datum=@eind_datum, modifier=@modifier, modification_date=@modification_date 
                               WHERE id=@id";
                    DbCommand comm = new DbCommand(DbCommandType.crud, sql, paramList.ToArray());

                    db.Excecute(comm);
                    #endregion

                    #region Delete all types van dat Actieplan_Evaluatie uit de tussentabel
                    String sqlDelete = @"DELETE FROM ActieplanEvaluaties_actieplantypes WHERE actieplanEvaluatie_id = @actieplanId;";
                    List<DbParam> paramListDelete = new List<DbParam>()
                    {
                        new DbParam() { Name = "@actieplanId", Value = evaluatie.id, DbType = DbTypes.db_integer }
                    };
                    DbCommand commDelete = new DbCommand(DbCommandType.crud, sqlDelete, paramListDelete.ToArray());

                    db.Excecute(commDelete);
                    #endregion

                    #region Insert new types into tussentabel
                    foreach (actieplantypes type in evaluatie.Actieplantypes)
                    {
                        String sqlInsert = @"   INSERT INTO ActieplanEvaluaties_actieplantypes (actieplanEvaluatie_id, actieplantype_id, creator_id, creationDate)
                                                VALUES (@actieplanEvaluatieId, @typeId, @creatorId, @creationDate)";
                        List<DbParam> paramListInsert = new List<DbParam>()
                        {
                            new DbParam() { Name = "@actieplanEvaluatieId", Value = evaluatie.id, DbType = DbTypes.db_integer },
                            new DbParam() { Name = "@typeId", Value = type.id, DbType = DbTypes.db_integer },
                            new DbParam() { Name = "@creatorId", Value = userId, DbType = DbTypes.db_integer },
                            new DbParam() { Name = "@creationDate", Value = now, DbType = DbTypes.db_datetime }
                        };
                        DbCommand commInsert = new DbCommand(DbCommandType.crud, sqlInsert, paramListInsert.ToArray());

                        db.Excecute(commInsert);
                    }
                    #endregion

                    db.CommitTransaction();
                }
                catch (Exception ex)
                {
                    db.RollbackTransaction();
                    throw ex;
                }
            }
        }
        public static void DeleteEvaluatie(ActieplanEvaluatiesWeergave evaluatie)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                try
                {
                    db.BeginTransaction();
                    List<DbParam> paramList = new List<DbParam>()
                    {
                        new DbParam() { Name = "@id", Value = evaluatie.id, DbType = DbTypes.db_integer}
                    };
                    String sqlTypes = @"DELETE FROM ActieplanEvaluaties_actieplantypes WHERE actieplanEvaluatie_id = @id";
                    DbCommand commTypes = new DbCommand(DbCommandType.crud, sqlTypes, paramList.ToArray());
                    db.Excecute(commTypes);

                    String sql = @"DELETE FROM Actieplan_Evaluaties WHERE id = @id";
                    DbCommand comm = new DbCommand(DbCommandType.crud, sql, paramList.ToArray());
                    db.Excecute(comm);

                    db.CommitTransaction();
                }
                catch (Exception ex)
                {
                    db.RollbackTransaction();
                    throw ex;
                }
            }
        }
        #endregion
    }
}
