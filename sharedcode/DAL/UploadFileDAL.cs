﻿using sharedcode.BLL;
using sharedcode.common;
using sharedcode.DAL;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace sharedcode.DAL
{
    public class BijlagenDALManager
    {
        /** FIELDS */
        private AbstractBijlagenDALProvider _provider;

        /** CONSTRUCTOR */
        public BijlagenDALManager(AbstractBijlagenDALProvider provider)
        {
            _provider = provider;
        }

        /** METHODS */
        public List<file_cats> SelectAllFilecategories()
        {
            return _provider.SelectAllFilecategories();
        }
        public Byte[] SelectFileDataFromFileItem(FileItem file)
        {
            return _provider.SelectFileDataFromFileItem(file);
        }
        public void UpdateFileData(FileItem file)
        {
            _provider.UpdateFileData(file);
        }
        public void UpdateFileDescription(FileItem file)
        {
            _provider.UpdateFileDescription(file);
        }
        public void InsertFile(FileItem file)
        {
            _provider.InsertFile(file);
        }
        public List<FileItem> SelectAllFiles(int ownerId, file_cats category)
        {
            return _provider.SelectAllFiles(ownerId, category);
        }
        public void DeleteBijlage(FileItem file)
        {
            _provider.DeleteBijlage(file);
        }
        public AbstractBijlagenDALProvider GetProvider()
        {
            return _provider;
        }
    }

    public abstract class AbstractBijlagenDALProvider
    {
        protected List<FileItem> ConvertDatatableToListFileItem(DataTable dt, int objectId, bool hasLabels)
        {
            List<FileItem> filesList = new List<FileItem>();

            foreach (DataRow row in dt.Rows)
            {
                FileItem item = new FileItem()
                {
                    Id = (int)row["id"],
                    Creator = (int)row["creator"],
                    CreationDate = (DateTime)row["creationdate"],
                    ObjectId = objectId,
                    FileName = row["filename"].ToString(),
                    FileExtension = row["fExtension"].ToString(),
                    Description = row["description"].ToString().Trim(),
                    CreatorName = row["creator_voornaam"].ToString() + " " + row["creator_achternaam"].ToString(),
                    IsModified = false,
                    Labels = new ObservableCollection<labels>(),
                    VisibleLabels = new ObservableCollection<labels>(),
                    LabelsKommaSeperated = ""
                };

                if (!string.IsNullOrWhiteSpace(row["categoryCode"].ToString()))
                {
                    file_cats category = new file_cats()
                    {
                        code = row["categoryCode"].ToString().Trim()
                    };

                    if (!string.IsNullOrWhiteSpace(row["categoryName"].ToString()))
                        category.name = row["categoryName"].ToString().Trim();
                    if (!string.IsNullOrWhiteSpace(row["categoryModule"].ToString()))
                        category.module = row["categoryModule"].ToString().Trim();

                    item.Category = category;
                }

                if (hasLabels)
                {
                    labels label = new labels()
                    {
                        id = (int)row["LabelId"],
                        name = row["LabelNaam"].ToString()
                    };

                    item.Labels.Add(label);
                    LabelManager.GetChildLables(label.id).ForEach(item.Labels.Add);

                    if (GlobalData.Instance.LoggedOnUser.LabelManager.HasLabel(label.id))
                    {
                        item.LabelsKommaSeperated += label.name + ", ";
                        item.VisibleLabels.Add(label);
                        LabelManager.GetChildLables(label.id).ForEach(item.Labels.Add);
                    }
                }

                filesList.Add(item);
            }

            return filesList;
        }

        public virtual List<file_cats> SelectAllFilecategories()
        {
            using (Entities entity = new Entities(GlobalData.Instance.Entity))
            {
                return entity.file_cats.ToList();
            }
        }

        protected file_cats SelectFilecategorieFromCode(String code)
        {
            using (Entities entity = new Entities(GlobalData.Instance.Entity))
            {
                return entity.file_cats.Where(x => x.code == code).SingleOrDefault();
            }
        }

        public abstract List<FileItem> SelectAllFiles(int ownerId, file_cats category);
        public abstract Byte[] SelectFileDataFromFileItem(FileItem file);
        public abstract void UpdateFileData(FileItem file);
        public abstract void UpdateFileDescription(FileItem file);
        public abstract void InsertFile(FileItem file);
        public abstract void DeleteBijlage(FileItem file);
    }
    public class BijlagenDalProviderPersoneel : AbstractBijlagenDALProvider
    {
        public override List<FileItem> SelectAllFiles(int gebruikerId, file_cats category)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                List<DbParam> paramList = new List<DbParam>();

                String sql = @"SELECT personeel_files.id, fName as filename, fExtension, description, creator, creationdate, g.voornaam AS creator_voornaam, g.achternaam AS creator_achternaam, file_cats.code AS categoryCode,
file_cats.name categoryName, file_cats.module AS categoryModule
                               FROM  gebruiker AS g, personeel_files
							        LEFT JOIN file_cats on file_cats.code = personeel_files.category
                               WHERE g.id = creator AND gebruikerId = @gebruikerId";
                if (category != null)
                {
                    sql = sql + " AND category = @code";
                    paramList.Add(new DbParam() { Name = "@code", Value = category.code });
                }
                sql = sql + " ORDER BY creationdate desc";
                paramList.Add(new DbParam() { Name = "@gebruikerId", Value = gebruikerId });

                try
                {

                    DataTable dt = db.GetDataTable(sql, paramList.ToArray());

                    return ConvertDatatableToListFileItem(dt, gebruikerId, false);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
        public override byte[] SelectFileDataFromFileItem(FileItem file)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                String sql = @"Select * From personeel_files Where id = @id";
                DataTable dt = db.GetDataTable(sql, new DbParam() { Name = "@id", Value = file.Id });

                return (Byte[])dt.Rows[0]["filedata"];
            }
        }
        public override void UpdateFileDescription(FileItem file)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                DbCommand comm = new DbCommand(DbCommandType.crud)
                {
                    Sql = @"update personeel_files set description = @description where id = @id"
                };
                comm.Params.Add(new DbParam() { Name = "@description", Value = file.Description, DbType = DbTypes.db_string });
                comm.Params.Add(new DbParam() { Name = "@id", Value = file.Id, DbType = DbTypes.db_integer });

                try
                {
                    db.Excecute(comm);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
        public override void UpdateFileData(FileItem file)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                DbCommand comm = new DbCommand(DbCommandType.crud)
                {
                    Sql = "update personeel_files set filedata = @filedata where id=@id"
                };
                comm.Params.Add(new DbParam() { Name = "@filedata", Value = file.FileData, DbType = DbTypes.db_varbinary });
                comm.Params.Add(new DbParam() { Name = "@id", Value = file.Id, DbType = DbTypes.db_integer });

                try
                {
                    db.Excecute(comm);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
        public override void InsertFile(FileItem file)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                string sql = @"INSERT INTO personeel_files(creator, creationdate, gebruikerid, fName, fExtension, filedata, description, category)
                               VALUES (@creator, @creationdate, @geb_id, @filename, @fileExtension, @filedata, @description, @category)";
                List<DbParam> paramList = new List<DbParam>()
                {
                    new DbParam() { Name = "@creator", Value = file.Creator, DbType = DbTypes.db_integer },
                    new DbParam() { Name = "@creationdate", Value = file.CreationDate, DbType = DbTypes.db_datetime },
                    new DbParam() { Name = "@geb_id", Value = file.ObjectId, DbType = DbTypes.db_integer },
                    new DbParam() { Name = "@filename", Value = file.FileName, DbType = DbTypes.db_string },
                    new DbParam() { Name = "@fileExtension", Value = file.FileExtension, DbType = DbTypes.db_string },
                    new DbParam() { Name = "@filedata", Value = file.FileData, DbType = DbTypes.db_varbinary },
                    new DbParam() { Name = "@description", Value = file.Description, DbType = DbTypes.db_string },
                    new DbParam() { Name = "@category", Value = file.Category.code, DbType = DbTypes.db_string }
                };

                DbCommand commFile = new DbCommand(DbCommandType.crud, sql, paramList.ToArray());

                try
                {
                    db.Excecute(commFile);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
        public override void DeleteBijlage(FileItem file)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                DbCommand commFile = new DbCommand(DbCommandType.crud)
                {
                    Sql = "delete from personeel_files where id = @id"
                };
                commFile.Params.Add(new DbParam() { Name = "@id", Value = file.Id, DbType = DbTypes.db_integer });

                try
                {
                    db.Excecute(commFile);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
    }
    public class BijlagenDalProviderClientdossier : AbstractBijlagenDALProvider
    {
        public List<FileItem> SelectAllFilesFromBewonerAndLabellist(int bewonerId, List<labels> labelList)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                List<DbParam> paramList = new List<DbParam>()
                {
                    new DbParam() { Name = "@bewonerid", Value = bewonerId }
                };

                string sql = @"SELECT f.id, f.creator, f.creationdate, f.bewonerid, f.fName as filename, f.fExtension, file_cats.code AS categoryCode, file_cats.name AS categoryName, f.overeenkomst_id, 
g.voornaam AS creator_voornaam, g.achternaam AS creator_achternaam, l.id as LabelId, l.name as LabelNaam, file_cats.module AS categoryModule,
                                    CASE
                                        WHEN f.overeenkomst_id is null 
		                                THEN f.description
		                                ELSE f.description +  ' ( Overeenkomst loopt van ' + LEFT(CONVERT(VARCHAR,  o.vanDatum, 120), 10) + ' tot ' + 
			                                CASE 
				                                WHEN o.totDatum is not null 
				                                THEN LEFT(CONVERT(VARCHAR, o.totDatum, 120), 10) 
				                                ELSE '...'
			                                END
			                                + ' )' 
	                                END description
                               FROM cd_files AS f 
                                    LEFT JOIN cd_files_labels on cd_files_labels.fileid = f.id 
                                    LEFT JOIN Cd_Overeenkomst AS o ON o.id = f.overeenkomst_id 
                                    LEFT JOIN file_cats on file_cats.code = f.category
                                    LEFT JOIN Overeenkomst_status AS s ON o.status_id = s.id, gebruiker AS g, labels AS l 
                               WHERE g.id = f.creator
                                    AND f.bewonerid = @bewonerid
                                    AND l.id = cd_files_labels.labelid";
                if (labelList == null || labelList.Count == 0)
                {
                    labelList = GlobalData.Instance.LoggedOnUser.LabelManager.GetAllActiveLabels();
                }
                string labelIds = "";
                foreach (labels label in labelList.Where(x => x.bijlagen == true).ToList())
                {
                    if (!string.IsNullOrWhiteSpace(labelIds))
                        labelIds += ", ";
                    labelIds += label.id;
                }
                if (!string.IsNullOrWhiteSpace(labelIds))
                    sql += @"   AND (l.id IN (" + labelIds + @") OR l.parentid IN(" + labelIds + @"))";
                else
                    return new List<FileItem>();

                sql += @"       ORDER BY creationdate desc";

                try
                {
                    DataTable dt = db.GetDataTable(sql, paramList.ToArray());
                    List<FileItem> filesList = ConvertDatatableToListFileItem(dt, bewonerId, true);

                    List<FileItem> finalList = new List<FileItem>();
                    foreach (labels userLabel in GlobalData.Instance.LoggedOnUser.LabelManager.GetAllActiveLabels())
                    {
                        foreach (FileItem gevondenItem in filesList.FindAll(x => x.VisibleLabels.Any(y => y.id == userLabel.id)))
                        {
                            if (gevondenItem != null && !finalList.Contains(gevondenItem))
                            {
                                gevondenItem.LabelsKommaSeperated = gevondenItem.LabelsKommaSeperated.Remove(gevondenItem.LabelsKommaSeperated.Count() - 2);
                                finalList.Add(gevondenItem);
                            }
                        }
                    }
                    return finalList;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
        public override List<FileItem> SelectAllFiles(int bewonerId, file_cats category)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                List<DbParam> paramList = new List<DbParam>();

                string sql = @"SELECT f.id, f.creator, f.creationdate, f.bewonerid, f.fName as filename, f.fExtension, file_cats.code AS categoryCode, file_cats.name AS categoryName, f.overeenkomst_id, 
g.voornaam AS creator_voornaam, g.achternaam AS creator_achternaam, l.id as LabelId, l.name as LabelNaam, file_cats.module AS categoryModule,
                                    CASE
                                        WHEN f.overeenkomst_id is null 
		                                THEN f.description
		                                ELSE f.description +  ' ( Overeenkomst loopt van ' + LEFT(CONVERT(VARCHAR,  o.vanDatum, 120), 10) + ' tot ' + 
			                                CASE 
				                                WHEN o.totDatum is not null 
				                                THEN LEFT(CONVERT(VARCHAR, o.totDatum, 120), 10) 
				                                ELSE '...'
			                                END
			                                + ' )' 
	                                END description
                               FROM cd_files AS f 
                                    LEFT JOIN cd_files_labels on cd_files_labels.fileid = f.id 
                                    LEFT JOIN Cd_Overeenkomst AS o ON o.id = f.overeenkomst_id 
                                    LEFT JOIN file_cats on file_cats.code = f.category
                                    LEFT JOIN Overeenkomst_status AS s ON o.status_id = s.id, gebruiker AS g, labels AS l 
                               WHERE g.id = f.creator AND f.bewonerid = @bewonerid AND l.id = cd_files_labels.labelid";
                if (category != null)
                {
                    sql = sql + " AND category = @code";
                    paramList.Add(new DbParam() { Name = "@code", Value = category.code });
                }
                sql += @"       ORDER BY creationdate desc";
                paramList.Add(new DbParam() { Name = "@bewonerid", Value = bewonerId });

                try
                {
                    List<FileItem> filesList = ConvertDatatableToListFileItem(db.GetDataTable(sql, paramList.ToArray()), bewonerId, true);

                    List<FileItem> finalList = new List<FileItem>();
                    foreach (labels userLabel in GlobalData.Instance.LoggedOnUser.LabelManager.GetAllActiveLabels())
                    {
                        foreach (FileItem gevondenItem in filesList.FindAll(x => x.VisibleLabels.Any(y => y.id == userLabel.id)))
                        {
                            if (gevondenItem != null && !finalList.Contains(gevondenItem))
                            {
                                gevondenItem.LabelsKommaSeperated = gevondenItem.LabelsKommaSeperated.Remove(gevondenItem.LabelsKommaSeperated.Count() - 2);
                                finalList.Add(gevondenItem);
                            }
                        }
                    }
                    return finalList;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
        public override Byte[] SelectFileDataFromFileItem(FileItem file)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                String sql = @"Select * From cd_files Where id = @id";
                DataTable dt = db.GetDataTable(sql, new DbParam() { Name = "@id", Value = file.Id });

                return (Byte[])dt.Rows[0]["filedata"];
            }
        }
        public override void UpdateFileDescription(FileItem file)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                DbCommand comm = new DbCommand(DbCommandType.crud)
                {
                    Sql = @"update cd_files set description=@description where id=@id"
                };
                comm.Params.Add(new DbParam() { Name = "@description", Value = file.Description, DbType = DbTypes.db_string });
                comm.Params.Add(new DbParam() { Name = "@id", Value = file.Id, DbType = DbTypes.db_integer });

                try
                {
                    db.Excecute(comm);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
        public override void UpdateFileData(FileItem file)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                DbCommand comm = new DbCommand(DbCommandType.crud)
                {
                    Sql = "update cd_files set filedata = @filedata where id=@id"
                };
                comm.Params.Add(new DbParam() { Name = "@filedata", Value = file.FileData, DbType = DbTypes.db_varbinary });
                comm.Params.Add(new DbParam() { Name = "@id", Value = file.Id });

                try
                {
                    db.Excecute(comm);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
        public override void InsertFile(FileItem file)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                List<DbCommand> commList = new List<DbCommand>();

                try
                {
                    db.BeginTransaction();

                    List<DbParam> paramListFile = new List<DbParam>()
                    {
                        new DbParam() { Name = "@creator", Value = file.Creator, DbType = DbTypes.db_integer },
                        new DbParam() { Name = "@creationdate", Value = file.CreationDate, DbType = DbTypes.db_datetime },
                        new DbParam() { Name = "@bewonerid", Value = file.ObjectId, DbType = DbTypes.db_integer },
                        new DbParam() { Name = "@filename", Value = file.FileName, DbType = DbTypes.db_string },
                        new DbParam() { Name = "@fileExtension", Value = file.FileExtension, DbType = DbTypes.db_string },
                        new DbParam() { Name = "@filedata", Value = file.FileData, DbType = DbTypes.db_varbinary },
                        new DbParam() { Name = "@description", Value = file.Description, DbType = DbTypes.db_string },
                        new DbParam() { Name = "@category", Value = file.Category.code, DbType = DbTypes.db_string }
                    };
                    string sqlFile = @"INSERT INTO cd_files(creator, creationdate, bewonerid, fName, fExtension, filedata, description, category)
                                       VALUES (@creator, @creationdate, @bewonerid, @filename, @fileExtension, @filedata, @description, @category)";
                    DbCommand commFile = new DbCommand(DbCommandType.crud, sqlFile, paramListFile.ToArray());

                    int fileId = Convert.ToInt32(db.ExcecuteScalar(commFile));
                    
                    if (file.Labels != null || file.Labels.Count != 0)
                    {
                        foreach (labels label in file.Labels)
                        {
                            List<DbParam> paramListLabel = new List<DbParam>()
                            {
                                new DbParam() { Name = "@fileid", Value = fileId, DbType = DbTypes.db_integer },
                                new DbParam() { Name = "@labelid", Value = label.id, DbType = DbTypes.db_integer },
                                new DbParam() { Name = "@modifier", Value = file.Creator, DbType = DbTypes.db_integer },
                                new DbParam() { Name = "@modificationdate", Value = DateTime.Today, DbType = DbTypes.db_date }
                            };
                            String sqlLabel = @"insert into cd_files_labels(fileid, labelid, modifier, modificationdate)
                                                values (@fileid, @labelid, @modifier, @modificationdate)";
                            commList.Add(new DbCommand(DbCommandType.crud, sqlLabel, paramListLabel.ToArray()));
                        }
                    }
                    else
                        throw new Exception("Er zijn geen labels ingegeven voor dit bestand!");

                    db.Excecute(commList);
                    db.CommitTransaction();
                }
                catch (Exception ex)
                {
                    db.RollbackTransaction();
                    throw ex;
                }
            }
        }
        public override void DeleteBijlage(FileItem file)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                List<DbCommand> commandList = new List<DbCommand>();

                foreach (labels label in file.VisibleLabels)
                {
                    DbCommand commLabels = new DbCommand(DbCommandType.crud)
                    {
                        Sql = "delete from cd_files_labels where fileid = @id"
                    };
                    commLabels.Params.Add(new DbParam() { Name = "@id", Value = file.Id, DbType = DbTypes.db_integer });

                    commandList.Add(commLabels);
                }

                if (file.VisibleLabels.Count == file.Labels.Count)
                {
                    DbCommand commFile = new DbCommand(DbCommandType.crud)
                    {
                        Sql = "delete from cd_files where id = @id"
                    };
                    commFile.Params.Add(new DbParam() { Name = "@id", Value = file.Id, DbType = DbTypes.db_integer });

                    commandList.Add(commFile);
                }

                try
                {
                    db.Excecute(commandList);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
    }
    public class BijlagenDalProviderWerkbonnen : AbstractBijlagenDALProvider
    {
        public override List<FileItem> SelectAllFiles(int werkopdractId, file_cats category)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                List<DbParam> paramList = new List<DbParam>();

                String sql = @"SELECT werkopdracht_files.id, fName as filename, fExtension, description, creator, creationdate, g.voornaam AS creator_voornaam, g.achternaam AS creator_achternaam,
file_cats.code AS categoryCode, file_cats.name AS categoryName, file_cats.module AS categoryModule
                               FROM gebruiker AS g,  werkopdracht_files
                                    LEFT OUTER JOIN file_cats on file_cats.code = werkopdracht_files.category
                               WHERE g.id = creator AND wo_id = @werkopdractid";
                if (category != null)
                {
                    sql = sql + " AND category = @code";
                    paramList.Add(new DbParam() { Name = "@code", Value = category.code });
                }
                sql = sql + " ORDER BY creationdate desc";
                paramList.Add(new DbParam() { Name = "@werkopdractid", Value = werkopdractId });

                try
                {
                    DataTable dt = db.GetDataTable(sql, paramList.ToArray());

                    return ConvertDatatableToListFileItem(dt, werkopdractId, false);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
        public override Byte[] SelectFileDataFromFileItem(FileItem file)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                String sql = @"Select * From werkopdracht_files Where id = @id";
                DataTable dt = db.GetDataTable(sql, new DbParam() { Name = "@id", Value = file.Id });

                return (Byte[])dt.Rows[0]["filedata"];
            }
        }
        public override void UpdateFileDescription(FileItem file)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                DbCommand comm = new DbCommand(DbCommandType.crud)
                {
                    Sql = @"update werkopdracht_files set description=@description where id=@id"
                };
                comm.Params.Add(new DbParam() { Name = "@description", Value = file.Description, DbType = DbTypes.db_string });
                comm.Params.Add(new DbParam() { Name = "@id", Value = file.Id, DbType = DbTypes.db_integer });

                try
                {
                    db.Excecute(comm);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
        public override void UpdateFileData(FileItem file)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                DbCommand comm = new DbCommand(DbCommandType.crud)
                {
                    Sql = "update werkopdracht_files set filedata = @filedata where id=@id"
                };
                comm.Params.Add(new DbParam() { Name = "@filedata", Value = file.FileData, DbType = DbTypes.db_varbinary });
                comm.Params.Add(new DbParam() { Name = "@id", Value = file.Id, DbType = DbTypes.db_integer });

                try
                {
                    db.Excecute(comm);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
        public override void InsertFile(FileItem file)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                if (file.Category == null)
                    file.Category = new file_cats() { code = "WERKAANVRAAG" };
                else if (string.IsNullOrWhiteSpace(file.Category.code))
                    file.Category.code = "WERKAANVRAAG";

                string sql = @"INSERT INTO werkopdracht_files(creator, creationdate, wo_id, fName, fExtension, filedata, description, category) 
                               VALUES (@creator, @creationdate, @wo_id, @filename, @fileExtension, @filedata, @description, @category)";
                List<DbParam> paramList = new List<DbParam>()
                {
                    new DbParam() { Name = "@creator", Value = file.Creator, DbType = DbTypes.db_integer },
                    new DbParam() { Name = "@creationdate", Value = file.CreationDate, DbType = DbTypes.db_datetime },
                    new DbParam() { Name = "@wo_id", Value = file.ObjectId, DbType = DbTypes.db_integer },
                    new DbParam() { Name = "@filename", Value = file.FileName, DbType = DbTypes.db_string },
                    new DbParam() { Name = "@fileExtension", Value = file.FileExtension, DbType = DbTypes.db_string },
                    new DbParam() { Name = "@filedata", Value = file.FileData, DbType = DbTypes.db_varbinary },
                    new DbParam() { Name = "@description", Value = file.Description, DbType = DbTypes.db_string },
                    new DbParam() { Name = "@category", Value = file.Category.code, DbType = DbTypes.db_string }
                };

                DbCommand commFile = new DbCommand(DbCommandType.crud, sql, paramList.ToArray());

                try
                {
                    db.Excecute(commFile);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
        public override void DeleteBijlage(FileItem file)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                DbCommand commFile = new DbCommand(DbCommandType.crud)
                {
                    Sql = "delete from werkopdracht_files where id = @id"
                };
                commFile.Params.Add(new DbParam() { Name = "@id", Value = file.Id, DbType = DbTypes.db_integer });

                try
                {
                    db.Excecute(commFile);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
    }
    public class BijlagenDalProviderLocatie : AbstractBijlagenDALProvider
    {
        public override List<FileItem> SelectAllFiles(int locatieId, file_cats category)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                List<DbParam> paramList = new List<DbParam>();

                String sql = @"SELECT locatie_files.id, fName as filename, fExtension, description, creator, creationdate, g.voornaam AS creator_voornaam, g.achternaam AS creator_achternaam,
file_cats.code AS categoryCode, file_cats.name AS categoryName, file_cats.module AS categoryModule
                               FROM gebruiker AS g, locatie_files
                                    LEFT OUTER JOIN file_cats on file_cats.code = locatie_files.category
                               WHERE g.id = creator AND loc_id = @locatieid";
                if (category != null)
                {
                    sql = sql + " AND category = @code";
                    paramList.Add(new DbParam() { Name = "@code", Value = category.code });
                }
                sql = sql + " ORDER BY creationdate desc";
                paramList.Add(new DbParam() { Name = "@locatieid", Value = locatieId });

                try
                {
                    DataTable dt =  db.GetDataTable(sql, paramList.ToArray());

                    return ConvertDatatableToListFileItem(dt, locatieId, false);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
        public override Byte[] SelectFileDataFromFileItem(FileItem file)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                String sql = @"Select * From locatie_files Where id = @id";
                DataTable dt = db.GetDataTable(sql, new DbParam() { Name = "@id", Value = file.Id });

                return (Byte[])dt.Rows[0]["filedata"];
            }
        }
        public override void UpdateFileDescription(FileItem file)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                DbCommand comm = new DbCommand(DbCommandType.crud)
                {
                    Sql = @"update locatie_files set description=@description where id=@id"
                };
                comm.Params.Add(new DbParam() { Name = "@description", Value = file.Description, DbType = DbTypes.db_string });
                comm.Params.Add(new DbParam() { Name = "@id", Value = file.Id, DbType = DbTypes.db_integer });

                try
                {
                    db.Excecute(comm);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
        public override void UpdateFileData(FileItem file)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                DbCommand comm = new DbCommand(DbCommandType.crud)
                {
                    Sql = "update locatie_files set filedata = @filedata where id=@id"
                };
                comm.Params.Add(new DbParam() { Name = "@filedata", Value = file.FileData, DbType = DbTypes.db_varbinary });
                comm.Params.Add(new DbParam() { Name = "@id", Value = file.Id, DbType = DbTypes.db_integer });

                try
                {
                    db.Excecute(comm);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
        public override void InsertFile(FileItem file)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                if (file.Category == null)
                    file.Category = new file_cats() { code = "LOCATIE_INVENTARIS" };
                else if (string.IsNullOrWhiteSpace(file.Category.code))
                    file.Category.code = "LOCATIE_INVENTARIS";

                string sql = @"INSERT INTO locatie_files(creator, creationdate, loc_id, fName, fExtension, filedata, description, category) 
                               VALUES (@creator, @creationdate, @loc_id, @filename, @fileExtension, @filedata, @description, @category)";
                List<DbParam> paramList = new List<DbParam>()
                {
                    new DbParam() { Name = "@creator", Value = file.Creator, DbType = DbTypes.db_integer },
                    new DbParam() { Name = "@creationdate", Value = file.CreationDate, DbType = DbTypes.db_datetime },
                    new DbParam() { Name = "@loc_id", Value = file.ObjectId, DbType = DbTypes.db_integer },
                    new DbParam() { Name = "@filename", Value = file.FileName, DbType = DbTypes.db_string },
                    new DbParam() { Name = "@fileExtension", Value = file.FileExtension, DbType = DbTypes.db_string },
                    new DbParam() { Name = "@filedata", Value = file.FileData, DbType = DbTypes.db_varbinary },
                    new DbParam() { Name = "@description", Value = file.Description, DbType = DbTypes.db_string },
                    new DbParam() { Name = "@category", Value = file.Category.code, DbType = DbTypes.db_string }
                };

                DbCommand commFile = new DbCommand(DbCommandType.crud, sql, paramList.ToArray());

                try
                {
                    db.Excecute(commFile);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
        public override void DeleteBijlage(FileItem file)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                DbCommand commFile = new DbCommand(DbCommandType.crud)
                {
                    Sql = "delete from locatie_files where id = @id"
                };
                commFile.Params.Add(new DbParam() { Name = "@id", Value = file.Id, DbType = DbTypes.db_integer });

                try
                {
                    db.Excecute(commFile);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
    }
    public class BijlagenDalProviderInventaris : AbstractBijlagenDALProvider
    {
        public override List<FileItem> SelectAllFiles(int inventarisId, file_cats category)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                List<DbParam> paramList = new List<DbParam>();
                
                string sql = @"SELECT inventaris_files.id, fName as filename, fExtension, description, creator, creationdate, g.voornaam AS creator_voornaam, g.achternaam AS creator_achternaam,
file_cats.code AS categoryCode, file_cats.name AS categoryName, file_cats.module AS categoryModule
                               FROM gebruiker AS g, inventaris_files
                                    LEFT OUTER JOIN file_cats on file_cats.code = inventaris_files.category
                               WHERE g.id = creator AND inv_id = @inventarisid";
                if (category != null)
                {
                    sql = sql + " AND category = @code";
                    paramList.Add(new DbParam() { Name = "@code", Value = category.code });
                }
                sql = sql + " ORDER BY creationdate desc";
                paramList.Add(new DbParam() { Name = "@inventarisid", Value = inventarisId });

                try
                {
                    DataTable dt = db.GetDataTable(sql, paramList.ToArray());

                    return ConvertDatatableToListFileItem(dt, inventarisId, false);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
        public override byte[] SelectFileDataFromFileItem(FileItem file)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                String sql = @"Select * From inventaris_files Where id = @id";
                DataTable dt = db.GetDataTable(sql, new DbParam() { Name = "@id", Value = file.Id });

                return (Byte[])dt.Rows[0]["filedata"];
            }
        }
        public override void UpdateFileDescription(FileItem file)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                DbCommand comm = new DbCommand(DbCommandType.crud)
                {
                    Sql = @"update inventaris_files set description = @description where id = @id"
                };
                comm.Params.Add(new DbParam() { Name = "@description", Value = file.Description, DbType = DbTypes.db_string });
                comm.Params.Add(new DbParam() { Name = "@id", Value = file.Id, DbType = DbTypes.db_integer });

                try
                {
                    db.Excecute(comm);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
        public override void UpdateFileData(FileItem file)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                DbCommand comm = new DbCommand(DbCommandType.crud)
                {
                    Sql = "update inventaris_files set filedata = @filedata where id=@id"
                };
                comm.Params.Add(new DbParam() { Name = "@filedata", Value = file.FileData, DbType = DbTypes.db_varbinary });
                comm.Params.Add(new DbParam() { Name = "@id", Value = file.Id, DbType = DbTypes.db_integer });

                try
                {
                    db.Excecute(comm);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
        public override void InsertFile(FileItem file)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                if (file.Category == null)
                    file.Category = new file_cats() { code = "INVENTARIS" };
                else if (string.IsNullOrWhiteSpace(file.Category.code))
                    file.Category.code = "INVENTARIS";

                string sql = @"INSERT INTO inventaris_files(creator, creationdate, inv_id, fName, fExtension, filedata, description, category) 
                               VALUES (@creator, @creationdate, @inv_id, @filename, @fileExtension, @filedata, @description, @category)";
                List<DbParam> paramList = new List<DbParam>()
                {
                    new DbParam() { Name = "@creator", Value = file.Creator, DbType = DbTypes.db_integer },
                    new DbParam() { Name = "@creationdate", Value = file.CreationDate, DbType = DbTypes.db_datetime },
                    new DbParam() { Name = "@inv_id", Value = file.ObjectId, DbType = DbTypes.db_integer },
                    new DbParam() { Name = "@filename", Value = file.FileName, DbType = DbTypes.db_string },
                    new DbParam() { Name = "@fileExtension", Value = file.FileExtension, DbType = DbTypes.db_string },
                    new DbParam() { Name = "@filedata", Value = file.FileData, DbType = DbTypes.db_varbinary },
                    new DbParam() { Name = "@description", Value = file.Description, DbType = DbTypes.db_string },
                    new DbParam() { Name = "@category", Value = file.Category.code, DbType = DbTypes.db_string }
                };

                DbCommand commFile = new DbCommand(DbCommandType.crud, sql, paramList.ToArray());
                
                try
                {
                    db.Excecute(commFile);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
        public override void DeleteBijlage(FileItem file)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                DbCommand commFile = new DbCommand(DbCommandType.crud)
                {
                    Sql = "delete from inventaris_files where id = @id"
                };
                commFile.Params.Add(new DbParam() { Name = "@id", Value = file.Id, DbType = DbTypes.db_integer });

                try
                {
                    db.Excecute(commFile);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
    }
    public class BijlagenDalProviderClientdossierOvereenkomsten : AbstractBijlagenDALProvider
    {
        /** FIELDS */
        private int _bewonerId;

        /** CONSTRUCTOR */
        public BijlagenDalProviderClientdossierOvereenkomsten(int bewonerId)
        {
            _bewonerId = bewonerId;
        }

        /** METHODS */
        public override List<file_cats> SelectAllFilecategories()
        {
            using (Entities entity = new Entities(GlobalData.Instance.Entity))
            {
                return entity.file_cats.Where(x => x.module == "OVEREENKOMST").ToList();
            }
        }

        public override List<FileItem> SelectAllFiles(int overeenkomstId, file_cats category)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                List<DbParam> paramList = new List<DbParam>()
                {
                    new DbParam() { Name = "@overeenkomstId", Value = overeenkomstId , DbType = DbTypes.db_integer }
                };
                String sql = @"SELECT f.id, f.creator, f.creationdate, f.bewonerid, f.fName as filename, f.fExtension, f.category, f.overeenkomst_id, 
g.voornaam AS creator_voornaam, g.achternaam AS creator_achternaam, l.id as LabelId, l.name as LabelNaam, file_cats.code AS categoryCode, file_cats.name AS categoryName, file_cats.module AS categoryModule,
                                CASE
                                    WHEN f.overeenkomst_id is null 
		                            THEN f.description
		                            ELSE f.description +  ' ( Overeenkomst loopt van ' + LEFT(CONVERT(VARCHAR,  o.vanDatum, 120), 10) + ' tot ' + 
			                            CASE 
				                            WHEN o.totDatum is not null 
				                            THEN LEFT(CONVERT(VARCHAR, o.totDatum, 120), 10) 
				                            ELSE '...'
			                            END
			                            + ' )' 
	                            END description
                               FROM cd_files AS f 
                               LEFT JOIN file_cats on file_cats.code = f.category
                               LEFT JOIN cd_files_labels on cd_files_labels.fileid = f.id 
                               LEFT JOIN Cd_Overeenkomst AS o ON o.id = f.overeenkomst_id 
                               LEFT JOIN Overeenkomst_status AS s ON o.status_id = s.id, gebruiker AS g, labels AS l 
                               WHERE g.id = f.creator AND overeenkomst_id = @overeenkomstId AND l.id = cd_files_labels.labelid";
                if (category != null)
                {
                    sql = sql + " AND category = @code";
                    paramList.Add(new DbParam() { Name = "@code", Value = category.code, DbType = DbTypes.db_string });
                }

                try
                {
                    DataTable dt = db.GetDataTable(sql, paramList.ToArray());

                    List<FileItem> filesList = ConvertDatatableToListFileItem(dt, overeenkomstId, true);
                    List<FileItem> finalList = new List<FileItem>();

                    foreach (labels userLabel in GlobalData.Instance.LoggedOnUser.LabelManager.GetAllActiveLabels())
                    {
                        foreach (FileItem gevondenItem in filesList.FindAll(x => x.VisibleLabels.Any(y => y.id == userLabel.id)))
                        {
                            if (gevondenItem != null && !finalList.Contains(gevondenItem))
                            {
                                gevondenItem.LabelsKommaSeperated = gevondenItem.LabelsKommaSeperated.Remove(gevondenItem.LabelsKommaSeperated.Count() - 2);
                                finalList.Add(gevondenItem);
                            }
                        }
                    }
                    return finalList.OrderByDescending(x => x.CreationDate).ToList();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
        public override byte[] SelectFileDataFromFileItem(FileItem file)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                String sql = @"Select * From cd_files Where id = @id";
                DataTable dt = db.GetDataTable(sql, new DbParam() { Name = "@id", Value = file.Id });

                return (Byte[])dt.Rows[0]["filedata"];
            }
        }
        public override void DeleteBijlage(FileItem file)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                List<DbCommand> commandList = new List<DbCommand>();

                foreach (labels label in file.VisibleLabels)
                {
                    DbCommand commLabels = new DbCommand(DbCommandType.crud)
                    {
                        Sql = "delete from cd_files_labels where fileid = @id"
                    };
                    commLabels.Params.Add(new DbParam() { Name = "@id", Value = file.Id, DbType = DbTypes.db_integer });

                    commandList.Add(commLabels);
                }

                if (file.VisibleLabels.Count == file.Labels.Count)
                {
                    DbCommand commFile = new DbCommand(DbCommandType.crud)
                    {
                        Sql = "delete from cd_files where id = @id"
                    };
                    commFile.Params.Add(new DbParam() { Name = "@id", Value = file.Id, DbType = DbTypes.db_integer });

                    commandList.Add(commFile);
                }

                try
                {
                    db.Excecute(commandList);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
        public override void InsertFile(FileItem file)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                try
                {
                    db.BeginTransaction();
                    
                    DbCommand commFile = new DbCommand(DbCommandType.crud)
                    {
                        Sql = @"INSERT INTO cd_files(creator, creationdate, bewonerid, fName, fExtension, filedata, description, category, overeenkomst_id)
                                VALUES (@creator, @creationdate, @bewonerid, @filename, @fileExtension, @filedata, @description, @category, @overeenkomstId)",
                        Params = new List<DbParam>()
                        {
                        new DbParam() { Name = "@creator", Value = file.Creator, DbType = DbTypes.db_integer },
                        new DbParam() { Name = "@creationdate", Value = file.CreationDate, DbType = DbTypes.db_datetime },
                        new DbParam() { Name = "@bewonerid", Value = _bewonerId, DbType = DbTypes.db_integer },
                        new DbParam() { Name = "@filename", Value = file.FileName, DbType = DbTypes.db_string },
                        new DbParam() { Name = "@fileExtension", Value = file.FileExtension, DbType = DbTypes.db_string },
                        new DbParam() { Name = "@filedata", Value = file.FileData, DbType = DbTypes.db_varbinary },
                        new DbParam() { Name = "@description", Value = file.Description, DbType = DbTypes.db_string },
                        new DbParam() { Name = "@category", Value = file.Category.code, DbType = DbTypes.db_string },
                        new DbParam() { Name = "@overeenkomstId", Value = file.ObjectId, DbType = DbTypes.db_integer }
                        }
                    };
                    file.Id = Convert.ToInt32(db.ExcecuteScalar(commFile));

                    List<DbCommand> commList = new List<DbCommand>();
                    if (file.Labels != null || file.Labels.Count != 0)
                    {
                        foreach (labels label in file.Labels)
                        {
                            DbCommand commLabel = new DbCommand(DbCommandType.crud)
                            {
                                Sql = "insert into cd_files_labels(fileid, labelid, modifier, modificationdate) values (@fileid, @labelid, @modifier, @modificationdate)",
                                Params = new List<DbParam>()
                                {
                                    new DbParam() { Name = "@fileid", Value = file.Id, DbType = DbTypes.db_integer },
                                    new DbParam() { Name = "@labelid", Value = label.id, DbType = DbTypes.db_integer },
                                    new DbParam() { Name = "@modifier", Value = file.Creator, DbType = DbTypes.db_integer },
                                    new DbParam() { Name = "@modificationdate", Value = DateTime.Today, DbType = DbTypes.db_date }
                                }
                            };

                            commList.Add(commLabel);
                        }
                    }
                    else
                        throw new Exception("Er zijn geen labels ingegeven voor dit bestand!");

                    db.Excecute(commList);

                    db.CommitTransaction();
                }
                catch (Exception ex)
                {
                    db.RollbackTransaction();
                    throw ex;
                }
            }
        }
        public override void UpdateFileData(FileItem file)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                try
                {
                    DbCommand comm = new DbCommand(DbCommandType.crud)
                    {
                        Sql = "update cd_files set filedata = @filedata where id=@id",
                        Params = new List<DbParam>()
                        {
                            new DbParam() { Name = "@filedata", Value = file.FileData, DbType = DbTypes.db_varbinary },
                            new DbParam() { Name = "@id", Value = file.Id, DbType = DbTypes.db_integer }
                        }
                    };

                    db.Excecute(comm);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
        public override void UpdateFileDescription(FileItem file)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                try
                {
                    DbCommand comm = new DbCommand(DbCommandType.crud)
                    {
                        Sql = @"update cd_files set description=@description where id=@id",
                        Params = new List<DbParam>()
                    {
                        new DbParam() { Name = "@description", Value = file.Description, DbType = DbTypes.db_string },
                        new DbParam() { Name = "@id", Value = file.Id, DbType = DbTypes.db_integer }
                    }
                    };

                    db.Excecute(comm);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
    }
}
