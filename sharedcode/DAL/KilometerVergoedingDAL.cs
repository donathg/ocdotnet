﻿
using sharedcode.BLL;
using sharedcode.common;
using sharedcode.WeergaveClasses;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sharedcode.DAL
{
    public class KilometerVergoedingDAL
    {
        public static void CloseKilometers(KilometerBLLSettings settings)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                try
                {
                    string sql = @" UPDATE kilometers";

                    if (settings.Mode == KilometerModeType.bewoners)
                        sql += @"   SET km_closed_bewoners_date = GetDate(), km_closed_bewoners_user = @gebIdSluiter
                                    WHERE km_closed_bewoners_date is null";
                    else if (settings.Mode == KilometerModeType.personeelsdienst)
                        sql += @"   SET km_closed_persdienst_date = GetDate(), km_closed_persdienst_user = @gebIdSluiter
                                    WHERE km_closed_persdienst_date is null";
                    else if (settings.Mode == KilometerModeType.boekhouding)
                        sql += @"   SET km_closed_boekh_date = GetDate(), km_closed_boekh_user = @gebIdSluiter
                                    WHERE km_closed_boekh_date is null";
                    else
                        throw new Exception("wrong mode");

                    List<DbParam> paramList = GetQuery("km_id", settings, out string sqlKilometers, out string sqlLeefgroepen, out string sqlBewoners, out string sqlKampen);

                    sql += $@"      AND km_bedrag is not null 
                                    AND km_kilometers is not null 
                                    AND km_id in ({sqlKilometers})";

                    paramList.Insert(0, new DbParam() { Name = "@gebIdSluiter", Value = GlobalData.Instance.LoggedOnUser.Id.GetValueOrDefault() });
                    DbCommand comm = new DbCommand(DbCommandType.crud, sql, paramList.ToArray());
                    db.Excecute(comm);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
           
        }
        public static void DeleteKilometers(int id)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                List<DbCommand> commList = new List<DbCommand>();

                DbCommand comm = new DbCommand(DbCommandType.crud, "delete from kilometer_leefgroepen where kmlfg_km_id = @id", new DbParam() { Name = "@id", Value = id });
                commList.Add(comm);

                DbCommand comm2 = new DbCommand(DbCommandType.crud, "delete from kilometer_bewoners where kmbew_km_id = @id", new DbParam() { Name = "@id", Value = id });
                commList.Add(comm2);

                DbCommand comm3 = new DbCommand(DbCommandType.crud, "delete from kilometer_kampen where kmkamp_km_id = @id", new DbParam() { Name = "@id", Value = id });
                commList.Add(comm3);

                string sql4 = @"delete from kilometers
                                where km_id = @id
                                and km_closed_boekh_date is null
                                and km_closed_bewoners_date is null
                                and km_closed_persdienst_date is null";
                DbCommand comm4 = new DbCommand(DbCommandType.crud, sql4, new DbParam() { Name = "@id", Value = id });
                commList.Add(comm4);

                try
                {
                    db.Excecute(commList);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
        public static void GetKilometers(KilometerBLLSettings settings, out DataTable dtKilometers, out DataTable dtLeefgroepen, out DataTable dtBewoners, out DataTable dtKampen)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                List<DbParam> paramList = GetQuery("*", settings, out string sqlKilometers, out string sqlLeefgroepen, out string sqlBewoners, out string sqlKampen);

                dtKilometers = db.GetDataTable(sqlKilometers, paramList.ToArray());
                dtLeefgroepen = db.GetDataTable(sqlLeefgroepen, paramList.ToArray());
                dtBewoners = db.GetDataTable(sqlBewoners, paramList.ToArray());
                dtKampen = db.GetDataTable(sqlKampen, paramList.ToArray());
            }
        }
        public static List<DbParam> GetQuery(string select, KilometerBLLSettings settings, out string sqlKilometers, out string sqlLeefgroepen, out string sqlBewoners, out string sqlKampen)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                string filterSql = " and km_datum between @datefrom and @dateto ";

                List<DbParam> paramList = new List<DbParam>
                {
                    new DbParam() { Name = "@datefrom", Value = settings.DateFrom.Date },
                    new DbParam() { Name = "@dateto", Value = settings.DateTo.Date }
                };

                if (settings.Mode == KilometerModeType.ingave)
                {
                    filterSql += " and kilometers.km_bestuurder = @gebId and km_kmw_id in (1,5) ";
                    paramList.Add(new DbParam() { Name = "@gebId", Value = settings.FilterGebruikerId });
                }
                else if (settings.Mode == KilometerModeType.bewoners)
                {
                    filterSql += " and km_kmt_code in ('KAMP','CLIENT') ";
                    if (settings.Closed.HasValue)
                    {
                        if (settings.Closed.Value)
                            filterSql += " and km_closed_bewoners_date is not null";
                        else
                            filterSql += " and km_closed_bewoners_date is null";
                    }
                }
                else if (settings.Mode == KilometerModeType.personeelsdienst)
                {
                    filterSql += " and km_kmw_id in (1,5) "; //eigen wagen of eigen fiets
                    if (settings.Closed.HasValue)
                    {
                        if (settings.Closed.Value)
                            filterSql += " and km_closed_persdienst_date is not null";
                        else
                            filterSql += " and km_closed_persdienst_date is null";
                    }
                }
                else if (settings.Mode == KilometerModeType.boekhouding)
                {
                    filterSql += " and km_kmt_code = 'LFGR' ";
                    if (settings.Closed.HasValue)
                    {
                        if (settings.Closed.Value)
                            filterSql += " and km_closed_boekh_date is not null";
                        else
                            filterSql += " and km_closed_boekh_date is null";
                    }
                }
                if (settings.BewonerId > 0)
                {
                    filterSql += " and @bewId in (select kmbew_bewonerid from kilometer_bewoners where kmbew_km_id = kilometers.km_id)";
                    paramList.Add(new DbParam() { Name = "@bewId", Value = settings.BewonerId.GetValueOrDefault() });
                }
                if (settings.LeefgroepId> 0)
                {
                    filterSql += " and @lfgId in (select kmlfg_leefgroepid from kilometer_leefgroepen where kmlfg_km_id = kilometers.km_id)";
                    paramList.Add(new DbParam() { Name = "@lfgId", Value = settings.LeefgroepId });
                }
                if (!string.IsNullOrWhiteSpace(settings.Type))
                {
                    filterSql += " and km_kmt_code =  @type";
                    paramList.Add(new DbParam() { Name = "@type", Value = settings.Type });
                }

                sqlKilometers = $@"SELECT {select}
                                   FROM kilometers, kilometer_type, kilometer_wagen, gebruiker 
                                   WHERE kilometers.km_kmt_code = kmt_code 
                                   AND kilometers.km_kmw_id = kmw_id 
                                   AND kilometers.km_bestuurder = gebruiker.id
                                   {filterSql}";

                sqlLeefgroepen = $@"SELECT {select}
                                    FROM kilometers, kilometer_leefgroepen, leefgroep 
                                    WHERE kmlfg_leefgroepid = leefgroep.id
                                    AND km_id = kmlfg_km_id
                                    {filterSql}";

                sqlBewoners = $@"SELECT {select}
                                 FROM kilometers, kilometer_bewoners, bewoner
                                 WHERE kmbew_bewonerid=bewoner.id
                                 AND km_id =kmbew_km_id
                                 {filterSql}";

                sqlKampen = $@"SELECT {select}
                               FROM kilometers, kilometer_kampen, kampen
                               WHERE kmkamp_kampid=kampen.id
                               AND km_id =kmkamp_km_id
                               {filterSql}";

                return paramList;
            }
        }
        public static List<string> GetVerplaatsingSuggestions(int userId, int wagenId, String typeCode)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                string sql = @"SELECT distinct LTRIM(RTRIM(km_bestemming)) bestemming 
                               FROM kilometers 
                               WHERE km_bestuurder = @userId
                               and km_kmw_id = @wagenId
                               and km_kmt_code = @type
                               ORDER BY 
                               LTRIM(RTRIM(km_bestemming))";
                DataTable dt = db.GetDataTable(sql, new DbParam() { Name = "@userId", Value = userId }, new DbParam() { Name = "@wagenId", Value = wagenId }, new DbParam() { Name = "@type", Value = typeCode });

                List<string> suggesties = new List<string>();
                foreach (DataRow dr in dt.Rows)
                {
                    if (!dr.IsNull("BESTEMMING"))
                        suggesties.Add(dr["BESTEMMING"].ToString());
                }

                return suggesties;
            }
        }
        public static List<string> GetRedenSuggestions(int userId, int wagenId, String typeCode)
        {

            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                DataTable dt = db.GetDataTable(@"select distinct LTRIM(RTRIM(km_reden)) reden from kilometers 
where 

km_bestuurder = @userId
and km_kmw_id = @wagenId
and km_kmt_code = @type
order by  LTRIM(RTRIM(km_reden))", new DbParam() { Name = "@userId", Value = userId }, new DbParam() { Name = "@wagenId", Value = wagenId }, new DbParam() { Name = "@type", Value = typeCode });
                List<string> suggesties = new List<string>();
                foreach (DataRow dr in dt.Rows)
                {
                    if (!dr.IsNull("REDEN"))
                        suggesties.Add(dr["REDEN"].ToString());
                }

                return suggesties;
            }
        }
        public static DataTable LoadTypes()
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                return db.GetDataTable(@"select * from kilometer_type order by kmt_code");
            }
        }
        public static DataTable LoadWagens()
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                return db.GetDataTable(@"select kilometer_wagen.*,locatie.naam LocatieNaam from kilometer_wagen left outer join locatie 
on kilometer_wagen.locatieId = locatie.id where actief=1
order by (case when  kmw_naam  = 'eigen wagen' then -1
 when  kmw_naam  = 'eigen fiets' then -0
 else kmw_id end)");
            }
        }
        public static void SaveKilometers(KilometerItem km)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                try
                {

                    //1 Procedure aanroepen
                    DataTable dtLeefgroepen = new DataTable();
                    dtLeefgroepen.Columns.Add("id");
                    foreach (leefgroep lfg in km.Leefgroepen)
                        dtLeefgroepen.Rows.Add(lfg.id);
                    
                    //2 Procedure aanroepen
                    DataTable dtBewoners = new DataTable();

                    dtBewoners.Columns.Add("id");
                    foreach (bewoner bew in km.Bewoners)
                        dtBewoners.Rows.Add(bew.id);

                    //2 Procedure aanroepen
                    DataTable dtKampen = new DataTable();

                    dtKampen.Columns.Add("id");
                    foreach (kampen kamp in km.Kampen)
                        dtKampen.Rows.Add(kamp.id);
                    List<DbParam> paramList = new List<DbParam>()
                    {
                        new DbParam() { Name = "@retval", Direction = DbParamDirection.return_value },
                        new DbParam() { Name = "@id", Value = km.Id },
                        new DbParam() { Name = "@type", Value = km.Type.Code },
                        new DbParam() { Name = "@wagenId", Value = km.Wagen.Id },
                        new DbParam() { Name = "@modifier", Value = GlobalData.Instance.LoggedOnUser.Id },
                        new DbParam() { Name = "@bestuurder", Value = km.Bestuurder.Id },
                        new DbParam() { Name = "@datum", Value = km.Datum.Date },
                        new DbParam() { Name = "@bestemming", Value = km.Bestemming },
                        new DbParam() { Name = "@reden", Value = km.Reden },
                        new DbParam() { Name = "@beginstand", Value = km.BeginstandKilometer },
                        new DbParam() { Name = "@eindstand", Value = km.EindstandKilometer },
                        new DbParam() { Name = "@kilometers", Value = km.Kilometers },
                        new DbParam() { Name = "@leefgroepenList", Value = dtLeefgroepen, DbType = DbTypes.db_table },
                        new DbParam() { Name = "@bewonersList", Value = dtBewoners, DbType = DbTypes.db_table },
                        new DbParam() { Name = "@kampenList", Value = dtKampen, DbType = DbTypes.db_table },
                        new DbParam() { Name = "@o_bedrag", Direction = DbParamDirection.output, DbType = DbTypes.db_decimal, Precision = 10, Scale = 4 },
                        new DbParam() { Name = "@o_bedrag_omnium", Direction = DbParamDirection.output, DbType = DbTypes.db_decimal, Precision = 10, Scale = 4 },
                        new DbParam() { Name = "@o_omnium", Direction = DbParamDirection.output, DbType = DbTypes.db_string, Size = 1 }
                    };

                    DbCommand command = new DbCommand(DbCommandType.procedure, "[saveKilometer2]", paramList.ToArray());
                    db.Excecute(command);
                 
                    km.Id = (int)command.Results["@retval"];
                    km.Omnium = command.Results["@o_omnium"].ToString() == "Y";

                    if (command.Results["@o_bedrag"] != null && command.Results["@o_bedrag"] != DBNull.Value)
                        km.Price = (decimal)command.Results["@o_bedrag"];
                    else
                    {
                        km.Price = null;
                        if (km.Kilometers.GetValueOrDefault()!=0)
                            throw new Exception("Geen prijzen gevonden voor deze periode. Gelieve de boekhouding te contacteren.");
                    }
                    if (command.Results["@o_bedrag_omnium"] != null && command.Results["@o_bedrag_omnium"] != System.DBNull.Value)
                        km.PriceOmnium = (decimal)command.Results["@o_bedrag_omnium"];
                    else
                        km.PriceOmnium = null;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

        public static void MarkAsNagekeken(int id)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                try
                {
                    String sql = @" UPDATE kilometers SET  km_checkedbybewoners_date = GetDate() WHERE km_id = @id";
                    List<DbParam> paramList = new List<DbParam>()
                        {
                            new DbParam() { Name = "@id", Value = id, DbType = DbTypes.db_integer },
                           
                        };
                    DbCommand comm = new DbCommand(DbCommandType.crud, sql, paramList.ToArray());
                    db.Excecute(comm);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
        public static void MarkAsNietNagekeken(int id)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                try
                {
                    String sql = @" UPDATE kilometers SET  km_checkedbybewoners_date =null WHERE km_id = @id";
                    List<DbParam> paramList = new List<DbParam>()
                        {
                            new DbParam() { Name = "@id", Value = id, DbType = DbTypes.db_integer },

                        };
                    DbCommand comm = new DbCommand(DbCommandType.crud, sql, paramList.ToArray());
                    db.Excecute(comm);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
    }
}
