﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sharedcode.DAL
{
    public enum DbCommandType
    {
        crud,//insert, update, delete,
        procedure,
        function
    }
    public class DbCommand
    {
        public Dictionary<string, object> Results { get; private set; }
        public void ResetResults ()
        {
            Results = new Dictionary<string, object>();

        }
        public DbCommand(DbCommandType type)
        {
            Params = new List<DbParam>();
            Type = type;
            ResetResults();
        }
        public DbCommand(DbCommandType type, string sql, params DbParam[] dbParams)
        {
            Params = new List<DbParam>();
            Type = type;
            Sql = sql;
            Params = dbParams.ToList();
        }
        public List<DbParam> Params { get; set; }
        public string Sql { get; set; }
        public DbCommandType Type { get; set; }
    }

    public enum DbParamDirection
    {
        input,
        output,
        input_output,
        return_value
    }
    public enum DbTypes
    {
        db_auto,
        db_integer,
        db_string, 
        db_double,
        db_decimal,
        db_date,
        db_datetime,
        db_varbinary,
        db_table,
        db_variant,
    }
    public class DbParam
    {
        public DbParam()
        {
            Direction = DbParamDirection.input;
            DbType = DbTypes.db_auto;
        }
        public string Name { get; set; }
        public object Value { get; set; }
        public DbParamDirection Direction { get; set; }
        public DbTypes DbType { get; set; }
        public int? Size { get; set; }
        public int? Precision { get; set; }
        public int? Scale { get; set; }
    }
    public class DatabaseCentral : IDisposable
    {
        /** FIELDS */
        private SqlConnection _sqlConnection;
        private SqlTransaction _sqlTransaction;

        /** CONSTRUCTOR */
        public DatabaseCentral (String connectString)
        {
            Connect(connectString);
        }

        /** METHODS */
        private void Connect(String connectionString)
        {
            _sqlConnection = new SqlConnection(connectionString);
            _sqlConnection.Open();
        }
        public void Disconnect()
        {
            _sqlConnection.Close();
        }
        public void Dispose()
        {
            Disconnect();
            _sqlConnection = null;
        }

        private SqlParameter ConverParameter(DbParam param)
        {
            SqlParameter sqlParam = new SqlParameter(param.Name, param.Value ?? DBNull.Value);
            if (param.Direction == DbParamDirection.input)
                sqlParam.Direction = ParameterDirection.Input;
            if (param.Direction == DbParamDirection.input_output)
                sqlParam.Direction = ParameterDirection.InputOutput;
            if (param.Direction == DbParamDirection.output)
                sqlParam.Direction = ParameterDirection.Output;
            if (param.Direction == DbParamDirection.return_value)
                sqlParam.Direction = ParameterDirection.ReturnValue;
            if (param.Size.HasValue)
                sqlParam.Size = param.Size.GetValueOrDefault();
            if (param.Precision.HasValue)
                sqlParam.Precision = (byte)param.Precision.GetValueOrDefault();
            if (param.Scale.HasValue)
                sqlParam.Scale = (byte)param.Scale.GetValueOrDefault();
            if (param.DbType == DbTypes.db_integer)
                sqlParam.DbType = DbType.Int32;
            if (param.DbType == DbTypes.db_string)
                sqlParam.DbType = DbType.String;
            if (param.DbType == DbTypes.db_double)
                sqlParam.DbType = DbType.Double;
            if (param.DbType == DbTypes.db_date)
                sqlParam.DbType = DbType.Date;
            if (param.DbType == DbTypes.db_datetime)
                sqlParam.DbType = DbType.DateTime;
            if (param.DbType == DbTypes.db_varbinary)
                sqlParam.DbType = DbType.Binary;
            if (param.DbType == DbTypes.db_decimal)
                sqlParam.DbType = DbType.Decimal;

            if (param.DbType == DbTypes.db_table)//for tables
               sqlParam.SqlDbType = SqlDbType.Structured;

            if (param.DbType == DbTypes.db_variant)
                sqlParam.SqlDbType = SqlDbType.Structured;
            return sqlParam;
        }

        public DataTable GetDataTable(string sql, params DbParam [] dbParams)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand(sql, this._sqlConnection);
            if (dbParams != null)
            {
                foreach (DbParam param in dbParams)
                    cmd.Parameters.Add(ConverParameter(param));
            }
            SqlDataAdapter da = new SqlDataAdapter(cmd);
             da.Fill(dt);
            da.Dispose();
            return dt;   
        }
        public DataTable GetDataTable(string sql)
        {
            return GetDataTable(sql, null);
        }
        public void Excecute(DbCommand comm)
        {
            Excecute(new List<DbCommand>() { comm });
        }
        public void Excecute(List<DbCommand> dbCommands)
        {
            bool hasLocallyCreatedTransaction = false;
            if (_sqlTransaction == null)
            {
                BeginTransaction();
                hasLocallyCreatedTransaction = true;
            }

            try
            {
                foreach (DbCommand com in dbCommands)
                {
                    //de collection met mogelijke resultaten(return -en out parameters) van de databank legen
                    com.ResetResults();
                    SqlCommand cmd = new SqlCommand(com.Sql, this._sqlConnection, _sqlTransaction);

                    if (com.Type == DbCommandType.procedure)
                        cmd.CommandType = CommandType.StoredProcedure;

                    foreach (DbParam param in com.Params)
                        cmd.Parameters.Add(ConverParameter(param));

                    cmd.ExecuteNonQuery();

                    //return parameters terug ophalen en in Result stoppen
                    foreach (DbParam param in com.Params)
                    {
                        if (param.Direction == DbParamDirection.return_value || param.Direction == DbParamDirection.output)
                        {
                            Object obj = cmd.Parameters[param.Name].Value;
                            com.Results.Add(param.Name, obj);
                        }
                    }
                }
                if (hasLocallyCreatedTransaction)
                    _sqlTransaction.Commit();
            }
            catch (Exception ex)
            {
                if (hasLocallyCreatedTransaction)
                    _sqlTransaction.Rollback();
                throw ex;
            }
        }
        public object ExcecuteScalar(DbCommand dbCommand)
        {
            bool autoCommit = false;
            if (_sqlTransaction == null)
            {
                BeginTransaction();
                autoCommit = true;
            }

            try
            {
                SqlCommand cmd = new SqlCommand(dbCommand.Sql, this._sqlConnection, _sqlTransaction);
                cmd.CommandText += "; SELECT SCOPE_IDENTITY()";
                foreach (DbParam param in dbCommand.Params)
                    cmd.Parameters.Add(ConverParameter(param));

                if (autoCommit)
                    _sqlTransaction.Commit();

                return cmd.ExecuteScalar();
            }
            catch (Exception ex)
            {
                if (autoCommit)
                    _sqlTransaction.Rollback();
                throw ex;
            }
        }

        public void BeginTransaction()
        {
            _sqlTransaction = _sqlConnection.BeginTransaction();
        }
        public void CommitTransaction()
        {
            _sqlTransaction.Commit();
        }
        public void RollbackTransaction()
        {
            _sqlTransaction.Rollback();
        }
    }
}
