﻿using sharedcode.BLL;
using sharedcode.common;
using sharedcode.WeergaveClasses;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sharedcode.DAL
{
    public abstract class AdresDALProvider
    {
        #region adrestype methods
        protected static List<adrestype> ConvertDatatableToAdrestypeList(DataTable dt)
        {
            List<adrestype> tempList = new List<adrestype>();
            foreach (DataRow row in dt.Rows)
            {
                adrestype type = new adrestype()
                {
                    id = (int)row["id"],
                    naam = row["naam"].ToString(),
                    isBedrijf = (bool)row["isBedrijf"],
                    ishoofdkeuze = (bool)row["ishoofdkeuze"],
                    actief = (bool)row["actief"],
                    module_code = row["module_code"].ToString()
                };
                if (!String.IsNullOrWhiteSpace(row["opmerking"].ToString()))
                    type.opmerking = row["opmerking"].ToString();

                tempList.Add(type);
            }

            return tempList;
        }

        public abstract List<adrestype> SelectAllAdrestypes(bool? isHoofdKeuze, bool? isBedrijf);
        public static adrestype SelectAdrestypeFromId(int id)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                List<DbParam> paramList = new List<DbParam>()
                {
                    new DbParam() { Name = "@id", Value = id, DbType = DbTypes.db_auto }
                };
                String sql = @"SELECT * FROM adrestype WHERE id = @id";
                DataTable dt = db.GetDataTable(sql, paramList.ToArray());

                return ConvertDatatableToAdrestypeList(dt).SingleOrDefault();
            }
        }
        #endregion
        #region Contactpersonen methods
        protected static List<ContactpersoonWeergave> ConvertDatatableToContactpersoonweergaveList(DataTable dt)
        {
            List<ContactpersoonWeergave> tempList = new List<ContactpersoonWeergave>();
            foreach (DataRow row in dt.Rows)
            {
                ContactpersoonWeergave contactpersoon = new ContactpersoonWeergave()
                {
                    id = (int)row["id"],
                    adrestype_id = (int)row["adrestype_id"],
                    adres_id = (int)row["adres_id"],
                };
                if (!String.IsNullOrWhiteSpace(row["voornaam"].ToString()))
                    contactpersoon.voornaam = row["voornaam"].ToString();
                if (!String.IsNullOrWhiteSpace(row["naam"].ToString()))
                    contactpersoon.naam = row["naam"].ToString();
                if (!String.IsNullOrWhiteSpace(row["functie"].ToString()))
                    contactpersoon.functie = row["functie"].ToString();
                if (!String.IsNullOrWhiteSpace(row["telefoon"].ToString()))
                    contactpersoon.telefoon = row["telefoon"].ToString();
                if (!String.IsNullOrWhiteSpace(row["gsm"].ToString()))
                    contactpersoon.gsm = row["gsm"].ToString();
                if (!String.IsNullOrWhiteSpace(row["email"].ToString()))
                    contactpersoon.email = row["email"].ToString();
                if (!String.IsNullOrWhiteSpace(row["opmerking"].ToString()))
                    contactpersoon.opmerking = row["opmerking"].ToString();

                adrestype type = new adrestype()
                {
                    id = (int)row["type_id"],
                    naam = row["type_naam"].ToString(),
                    isBedrijf = (bool)row["type_isbedrijf"],
                    ishoofdkeuze = (bool)row["type_ishoofdkeuze"],
                    actief = (bool)row["type_actief"]
                };
                if (!String.IsNullOrWhiteSpace(row["type_opmerking"].ToString()))
                    type.opmerking = row["type_opmerking"].ToString();

                contactpersoon.Type = type;

                tempList.Add(contactpersoon);
            }
            return tempList;
        }
        protected DbCommand CreateCommandForAddOrEditContactpersoon(ContactpersoonWeergave contactpersoon)
        {
            List<DbParam> paramList = new List<DbParam>()
            {
                new DbParam() { Name = "@id", Value = contactpersoon.id, DbType = DbTypes.db_integer },
                new DbParam() { Name = "@adrestypeId", Value = contactpersoon.adrestype_id, DbType = DbTypes.db_integer },
                new DbParam() { Name = "@adresId", Value = contactpersoon.adres_id, DbType = DbTypes.db_integer },
                new DbParam() { Name = "@voornaam", Value = contactpersoon.voornaam, DbType = DbTypes.db_string },
                new DbParam() { Name = "@naam", Value = contactpersoon.naam, DbType = DbTypes.db_string },
                new DbParam() { Name = "@functie", Value = contactpersoon.functie, DbType = DbTypes.db_string },
                new DbParam() { Name = "@telefoon", Value = contactpersoon.telefoon, DbType = DbTypes.db_string },
                new DbParam() { Name = "@gsm", Value = contactpersoon.gsm, DbType = DbTypes.db_string },
                new DbParam() { Name = "@email", Value = contactpersoon.email, DbType = DbTypes.db_string },
                new DbParam() { Name = "@opmerking", Value = contactpersoon.opmerking, DbType = DbTypes.db_string }
            };
            String sql;
            if (contactpersoon.id == 0)
                sql = @"INSERT INTO Adres_contactpersonen (adrestype_id, adres_id, voornaam, naam, functie, telefoon, gsm, email, opmerking)
                        VALUES (@adrestypeId, @adresId, @voornaam, @naam, @functie, @telefoon, @gsm, @email, @opmerking)";
            else
                sql = @"UPDATE Adres_contactpersonen
                        SET voornaam = @voornaam, naam = @naam, functie = @functie, telefoon = @telefoon, gsm = @gsm, email = @email, opmerking = @opmerking
                        WHERE id = @id";
            return new DbCommand(DbCommandType.crud, sql, paramList.ToArray());
        }

        public static List<ContactpersoonWeergave> SelectAllContactpersonenFromAdresId(int adresId)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                List<DbParam> paramList = new List<DbParam>()
                {
                    new DbParam() { Name = "@adresId", Value = adresId, DbType = DbTypes.db_integer }
                };
                String sql = @"SELECT ac.*, t.id As type_id, t.naam AS type_naam, t.isBedrijf AS type_isbedrijf, t.ishoofdkeuze AS type_ishoofdkeuze, 
t.opmerking AS type_opmerking, t.actief as type_actief
                               FROM (
                                    SELECT *
                                    FROM Adres_contactpersonen
                                    WHERE adres_id = @adresId
                               ) AS ac, adrestype As t
                               WHERE ac.adrestype_id = t.id";

                DataTable dt = db.GetDataTable(sql, paramList.ToArray());

                return ConvertDatatableToContactpersoonweergaveList(dt);
            }
        }
        public static ContactpersoonWeergave SelectContactpersoonFromId(int id)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                List<DbParam> paramList = new List<DbParam>()
                {
                    new DbParam() { Name = "@id", Value = id, DbType = DbTypes.db_integer }
                };
                String sql = @"SELECT * FROM Adres_contactpersonen where id = @id";
                DataTable dt = db.GetDataTable(sql, paramList.ToArray());

                return ConvertDatatableToContactpersoonweergaveList(dt).SingleOrDefault();
            }
        }
        public abstract void DeleteContactpersoon(ContactpersoonWeergave selectedContactpersoon);
        #endregion
        #region Adressen methods
        protected static List<AdresWeergave> ConvertDatatableToAdresWeergaveList(DataTable dt)
        {
            List<AdresWeergave> tempList = new List<AdresWeergave>();
            foreach (DataRow row in dt.Rows)
            {
                AdresWeergave adres = new AdresWeergave()
                {
                    id = (int)row["id"],
                    straat = row["straat"].ToString(),
                    huisnr = row["huisnr"].ToString(),
                    postcode = row["postcode"].ToString(),
                    gemeente = row["gemeente"].ToString(),
                    landcode = row["landcode"].ToString(),
                    active = (bool)row["active"],
                    naam = row["naam"].ToString(),
                    LandNaam = row["country_naam"].ToString(),
                };
                if (!String.IsNullOrWhiteSpace(row["busnr"].ToString()))
                    adres.busnr = row["busnr"].ToString();
                if (!String.IsNullOrWhiteSpace(row["telefoon"].ToString()))
                    adres.telefoon = row["telefoon"].ToString();
                if (!String.IsNullOrWhiteSpace(row["gsm"].ToString()))
                    adres.gsm = row["gsm"].ToString();
                if (!String.IsNullOrWhiteSpace(row["email"].ToString()))
                    adres.email = row["email"].ToString();
                if (!String.IsNullOrWhiteSpace(row["voornaam"].ToString()))
                    adres.voornaam = row["voornaam"].ToString();
                if (!String.IsNullOrWhiteSpace(row["geboortedatum"].ToString()))
                    adres.geboortedatum = (DateTime)row["geboortedatum"];
                if (!String.IsNullOrWhiteSpace(row["rijksregisternummer"].ToString()))
                    adres.rijksregisternummer = row["rijksregisternummer"].ToString();
                if (!String.IsNullOrWhiteSpace(row["nationaliteit"].ToString()))
                    adres.nationaliteit = row["nationaliteit"].ToString();
                if (!String.IsNullOrWhiteSpace(row["geslacht"].ToString()))
                    adres.geslacht = row["geslacht"].ToString();
                if (!String.IsNullOrWhiteSpace(row["btwnummer"].ToString()))
                    adres.btwnummer = row["btwnummer"].ToString();
                if (!String.IsNullOrWhiteSpace(row["adrestype_id"].ToString()))
                    adres.adrestype_id = (int)row["adrestype_id"];

                if (row.Table.Columns.Contains("MemberOfClaraFey") && !String.IsNullOrWhiteSpace(row["MemberOfClaraFey"].ToString()))
                    adres.MemberOfClaraFey = true;

                if (row.Table.Columns.Contains("bewoneradresId"))
                {
                    if (!String.IsNullOrWhiteSpace(row["bewoneradresId"].ToString()))
                        adres.BewoneradresId = (int)row["bewoneradresId"];
                    if (!String.IsNullOrWhiteSpace(row["bewoner_id"].ToString()))
                        adres.OwnerId = (int)row["bewoner_id"];
                    else if (!String.IsNullOrWhiteSpace(row["locatie_id"].ToString()))
                        adres.OwnerId = (int)row["locatie_id"];
                    if (!String.IsNullOrWhiteSpace(row["aansluitingsnummer"].ToString()))
                        adres.Aansluitingsnummer = row["aansluitingsnummer"].ToString();
                    if (!String.IsNullOrWhiteSpace(row["opmerking"].ToString()))
                        adres.Opmerking = row["opmerking"].ToString();
                }

                if (row.Table.Columns.Contains("type_id"))
                {
                    adrestype type = new adrestype()
                    {
                        id = (int)row["type_id"],
                        naam = row["type_naam"].ToString(),
                        isBedrijf = (bool)row["type_isbedrijf"],
                        ishoofdkeuze = (bool)row["type_ishoofdkeuze"],
                        actief = (bool)row["type_actief"],
                        module_code = row["module_code"].ToString()
                    };
                    if (!String.IsNullOrWhiteSpace(row["type_opmerking"].ToString()))
                        type.opmerking = row["type_opmerking"].ToString();

                    adres.Type = type;
                }

                adres.Contactpersonen = new ObservableCollection<ContactpersoonWeergave>(SelectAllContactpersonenFromAdresId(adres.id));

                tempList.Add(adres);
            }
            return tempList;
        }
        protected DbCommand CreateCommandForAddOrEditAdres(AdresWeergave adres)
        {
            int userId = GlobalData.Instance.LoggedOnUser.Id.Value;

            List<DbParam> paramList = new List<DbParam>()
            {
                new DbParam() { Name = "@id", Value = adres.id, DbType = DbTypes.db_integer },
                new DbParam() { Name = "@straat", Value = adres.straat, DbType = DbTypes.db_string },
                new DbParam() { Name = "@huisnr", Value = adres.huisnr, DbType = DbTypes.db_string },
                new DbParam() { Name = "@busnr", Value = adres.busnr, DbType = DbTypes.db_string },
                new DbParam() { Name = "@postcode", Value = adres.postcode, DbType = DbTypes.db_string },
                new DbParam() { Name = "@gemeente", Value = adres.gemeente, DbType = DbTypes.db_string },
                new DbParam() { Name = "@landcode", Value = adres.landcode, DbType = DbTypes.db_string },
                new DbParam() { Name = "@telefoon", Value = adres.telefoon, DbType = DbTypes.db_string },
                new DbParam() { Name = "@gsm", Value = adres.gsm, DbType = DbTypes.db_string },
                new DbParam() { Name = "@email", Value = adres.email, DbType = DbTypes.db_string },
                new DbParam() { Name = "@active", Value = GlobalSharedMethods.BooleantoBitConverter(adres.active), DbType = DbTypes.db_auto },
                new DbParam() { Name = "@naam", Value = adres.naam, DbType = DbTypes.db_string },
                new DbParam() { Name = "@voornaam", Value = adres.voornaam, DbType = DbTypes.db_string },
                new DbParam() { Name = "@geboortedatum", Value = adres.geboortedatum, DbType = DbTypes.db_date },
                new DbParam() { Name = "@rijksregisternr", Value = adres.rijksregisternummer, DbType = DbTypes.db_string },
                new DbParam() { Name = "@nationaliteit", Value = adres.nationaliteit, DbType = DbTypes.db_string },
                new DbParam() { Name = "@geslacht", Value = adres.geslacht, DbType = DbTypes.db_string },
                new DbParam() { Name = "@btwnr", Value = adres.btwnummer, DbType = DbTypes.db_string },
                new DbParam() { Name = "@adrestypeId", Value = adres.adrestype_id, DbType = DbTypes.db_auto },
                new DbParam() { Name = "@creator", Value = userId, DbType = DbTypes.db_integer },
                new DbParam() { Name = "@creationDate", Value = adres.modification_date, DbType = DbTypes.db_datetime },
                new DbParam() { Name = "@modifier", Value = userId, DbType = DbTypes.db_integer },
                new DbParam() { Name = "@modificationDate", Value = adres.modification_date, DbType = DbTypes.db_datetime },
            };
            String sql;
            if (adres.id != 0)
                sql = @"UPDATE Adressen 
                        SET straat = @straat, huisnr = @huisnr, busnr = @busnr, postcode = @postcode, gemeente = @gemeente, landcode = @landcode, 
telefoon = @telefoon, gsm = @gsm, email = @email, naam = @naam, voornaam = @voornaam, geboortedatum = @geboortedatum, rijksregisternummer = @rijksregisternr, 
nationaliteit = @nationaliteit, geslacht = @geslacht, btwnummer = @btwnr, modifier = @modifier, modification_date = @modificationDate
                        WHERE id = @id";
            else
                sql = @"INSERT INTO Adressen (straat, huisnr, busnr, postcode, gemeente, landcode, telefoon, gsm, email, active, naam, 
voornaam, geboortedatum, rijksregisternummer, nationaliteit, geslacht, btwnummer, adrestype_id, creator, creation_date, modifier, modification_date)
                                VALUES (@straat, @huisnr, @busnr, @postcode, @gemeente, @landcode, @telefoon, @gsm, @email, @active, @naam, @voornaam,
@geboortedatum, @rijksregisternr, @nationaliteit, @geslacht, @btwnr, @adrestypeId, @creator, @creationDate, @modifier, @modificationDate)";
            return new DbCommand(DbCommandType.crud, sql, paramList.ToArray());
        }

        public static AdresWeergave SelectAdressenFromId(int id)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                List<DbParam> paramList = new List<DbParam>()
                {
                    new DbParam() { Name = "@id", Value = id, DbType = DbTypes.db_integer }
                };
                String sql = @"SELECT * FROM Adressen WHERE id = @id";

                DataTable dt = db.GetDataTable(sql, paramList.ToArray());

                return ConvertDatatableToAdresWeergaveList(dt).SingleOrDefault();
            }
        }
        public abstract List<AdresWeergave> SelectAdresweergavesFromOwnerId(int ownerId);
        public static List<AdresWeergave> SelectAllAdresweergaveFromType(adrestype type)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                List<DbParam> paramList = new List<DbParam>()
                {
                    new DbParam() { Name = "@typeId", Value = type.id, DbType = DbTypes.db_integer }
                };
                String sql = @"SELECT ab.*, c.name AS land, t.id As type_id, t.naam AS type_naam, t.isBedrijf AS type_isbedrijf, 
t.ishoofdkeuze AS type_ishoofdkeuze, t.opmerking AS type_opmerking, t.actief AS type_actief, t.module_code, c.name AS country_naam
                               FROM (";

                if (type.isBedrijf)
                    sql += @"       SELECT * FROM Adressen WHERE adrestype_id = @typeId
                               ) AS ab, country AS c, adrestype As t
                               WHERE ab.adrestype_id = t.id";
                else
                    sql += @"       SELECT a.*, ba.adrestype_id AS typeid
                                    FROM Adressen AS a, Bewoner_adressen AS ba
                                    WHERE a.id = ba.adres_id AND a.adrestype_id IS NULL
                               ) AS ab, country AS c, adrestype As t
                               WHERE ab.typeid = t.id";

                sql += @"      AND ab.landcode = c.code";
                DataTable dt = db.GetDataTable(sql, paramList.ToArray());

                return ConvertDatatableToAdresWeergaveList(dt);
            }
        }
        #endregion
    }
    public class AdresBewonerDALProvider : AdresDALProvider
    {
        private DbCommand CreateCommandForAddOrEditBewoneradres(AdresWeergave adres)
        {
            List<DbParam> paramList = new List<DbParam>()
            {
                new DbParam() { Name = "@bewoneradresId", Value = adres.BewoneradresId, DbType = DbTypes.db_integer },
                new DbParam() { Name = "@bewonerId", Value = adres.OwnerId, DbType = DbTypes.db_integer },
                new DbParam() { Name = "@adresId", Value = adres.id, DbType = DbTypes.db_integer },
                new DbParam() { Name = "@adrestypeId", Value = adres.Type.id, DbType = DbTypes.db_auto },
                new DbParam() { Name = "@aansluitingsnummer", Value = adres.Aansluitingsnummer, DbType = DbTypes.db_string },
                new DbParam() { Name = "@opmerking", Value = adres.Opmerking, DbType = DbTypes.db_string }
            };
            String sql;
            if (adres.BewoneradresId == 0)
                sql = @"INSERT INTO Bewoner_adressen (bewoner_id, adres_id, adrestype_id, aansluitingsnummer, opmerking)
                        VALUES (@bewonerId, @adresId, @adrestypeId, @aansluitingsnummer, @opmerking)";
            else
                sql = @"UPDATE Bewoner_adressen
                        SET aansluitingsnummer = @aansluitingsnummer, opmerking = @opmerking
                        WHERE id = @bewoneradresId";
           return new DbCommand(DbCommandType.crud, sql, paramList.ToArray());
        }
        private DbCommand CreateCommandToAddBewonercontactpersoon(int bewoneradresId, int contactpersoonId)
        {
            List<DbParam> paramListBewonerContactpersoon = new List<DbParam>()
            {
            new DbParam() { Name = "@bewoneradresId", Value = bewoneradresId, DbType = DbTypes.db_integer },
            new DbParam() { Name = "@adrescontactpersoonId", Value = contactpersoonId, DbType = DbTypes.db_integer }
            };
            String sqlBewoneradresContactpersonen = @"INSERT INTO Bewoneradres_contactpersonen (bewoneradres_id, adrescontactpersoon_id)
                                                    VALUES (@bewoneradresId, @adrescontactpersoonId)";
            return new DbCommand(DbCommandType.crud, sqlBewoneradresContactpersonen, paramListBewonerContactpersoon.ToArray());
        }

        public override List<adrestype> SelectAllAdrestypes(bool? isHoofdKeuze, bool? isBedrijf)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                List<DbParam> paramList = new List<DbParam>();
                String sql = @"SELECT *
                               FROM adrestype
                               WHERE actief = 1 AND module_code='CD'";
                if (isHoofdKeuze.HasValue)
                {
                    sql += "AND ishoofdkeuze = @isHoofdkeuze ";
                    paramList.Add(new DbParam() { Name = "@isHoofdkeuze", Value = GlobalSharedMethods.BooleantoBitConverter(isHoofdKeuze.Value), DbType = DbTypes.db_auto });
                }
                if (isBedrijf.HasValue)
                {
                    sql += "AND isBedrijf = @isBedrijf ";
                    paramList.Add(new DbParam() { Name = "@isBedrijf", Value = GlobalSharedMethods.BooleantoBitConverter(isBedrijf.Value), DbType = DbTypes.db_auto });
                }
                sql += "ORDER BY isBedrijf desc, naam";
                DataTable dt = db.GetDataTable(sql, paramList.ToArray());

                return ConvertDatatableToAdrestypeList(dt);
            }
        }
        #region Adressen methods
        public List<AdresWeergave> SelectAllAdresweergavesFromSettings(AdressenListSettings settings)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                List<DbParam> paramList = new List<DbParam>();
                String sql = @"SELECT ab.*, c.name AS country_naam, t.id As type_id, t.naam AS type_naam, t.isBedrijf AS type_isbedrijf, 
t.ishoofdkeuze AS type_ishoofdkeuze, t.opmerking AS type_opmerking, t.actief as type_actief, t.module_code,
case when bewonerrijksregisternr  is not null then 'X' else '' end as MemberOfClaraFey
                               FROM (
                                    SELECT a.*, ba.id AS bewoneradresId, ba.bewoner_id, ba.aansluitingsnummer, ba.opmerking, ba.adrestype_id AS typeid,bewoner.rijksregisternr bewonerrijksregisternr
                                    FROM Bewoner_adressen AS ba, Adressen AS a
                                    left outer join bewoner on bewoner.rijksregisternr = a.rijksregisternummer
                                    WHERE a.id = ba.adres_id";

                if (settings.Bewoner is bewoner bew)
                {
                    paramList.Add(new DbParam() { Name = "@bewonerId", Value = settings.Bewoner.id, DbType = DbTypes.db_integer });
                    sql += @"       AND ba.bewoner_id = @bewonerId";
                }
                sql += @"      ) AS ab, country AS c, adrestype As t
                               WHERE (ab.adrestype_id = t.id OR ab.typeid = t.id)
                               AND ab.landcode = c.code";
                if (settings.AdresTypes != null && settings.AdresTypes.Count != 0)
                {
                    sql += @"  AND ab.typeid in (" + settings.GetAllAdrestypeIds() + ")";
                }
                DataTable dt = db.GetDataTable(sql, paramList.ToArray());

                return ConvertDatatableToAdresWeergaveList(dt);
            }
        }
        public override List<AdresWeergave> SelectAdresweergavesFromOwnerId(int ownerId)
        {
            return SelectAllAdresweergavesFromSettings(new AdressenListSettings() { Bewoner = BewonersDAL.SelectBewonerFromId(ownerId) });
        }
        /*public override List<AdresWeergave> SelectAdresweergavesFromOwnerId(int ownerId)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                List<DbParam> paramList = new List<DbParam>()
                {
                    new DbParam() { Name = "@bewonerId", Value = ownerId, DbType = DbTypes.db_integer }
                };
                String sql = @"SELECT ab.*, c.name AS country_naam, t.id As type_id, t.naam AS type_naam, t.isBedrijf AS type_isbedrijf, 
t.ishoofdkeuze AS type_ishoofdkeuze, t.opmerking AS type_opmerking, t.actief as type_actief, t.module_code
                               FROM (
                                    SELECT a.*, ba.id AS bewoneradresId, ba.bewoner_id, ba.aansluitingsnummer, ba.adrestype_id AS typeid
                                    FROM Adressen AS a, Bewoner_adressen AS ba
                                    WHERE a.id = ba.adres_id
                                    AND ba.bewoner_id = @bewonerId
                               ) AS ab, country AS c, adrestype As t
                               WHERE (ab.adrestype_id = t.id OR ab.typeid = t.id)
                               AND ab.landcode = c.code";
                DataTable dt = db.GetDataTable(sql, paramList.ToArray());

                return ConvertDatatableToAdresWeergaveList(dt);
            }
        }*/
        public List<AdresWeergave> SelectAllAdresweergaveFromCategory(bool isBedrijf, bewoner bewonerObj)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                String sql = "";
                List<DbParam> paramList = new List<DbParam>();

                if (isBedrijf)
                    sql += @"SELECT ab.*, c.name AS country_naam, t.id As type_id, t.naam AS type_naam, t.isBedrijf AS type_isbedrijf, t.ishoofdkeuze AS type_ishoofdkeuze, 
t.opmerking AS type_opmerking, t.actief AS type_actief, t.module_code, c.name AS country_naam
                             FROM (
                                SELECT *
                                FROM Adressen AS a
                                WHERE adrestype_id is NOT NULL
                             ) AS ab, country AS c, adrestype As t
                             WHERE ab.landcode = c.code AND ab.adrestype_id = t.id ";
                else
                {

                    sql += @"SELECT Distinct a.*, c.name AS country_naam
                             FROM Adressen AS a, country AS c, Bewoner_adressen AS b_a
                             WHERE a.adrestype_id IS NULL
                             AND b_a.adres_id = a.id
                             AND a.landcode = c.code";
                             
                    if (bewonerObj != null)
                    {
                        paramList.Add(new DbParam() { Name = "@bewonerId", Value = bewonerObj.id, DbType = DbTypes.db_integer });
                        sql += @" AND b_a.bewoner_id = @bewonerId";
                    }
                }

                DataTable dt = db.GetDataTable(sql, paramList.ToArray());

                return ConvertDatatableToAdresWeergaveList(dt);
            }
        }
        public void DeleteBewoneradresAndBewonercontactpersonen(AdresWeergave selectedAdres)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                List<DbParam> paramList = new List<DbParam>()
                {
                    new DbParam() { Name = "@id", Value = selectedAdres.BewoneradresId, DbType = DbTypes.db_integer }
                };
                String sql = @"DELETE FROM Bewoneradres_contactpersonen WHERE bewoneradres_id = @id;
                               DELETE FROM Bewoner_adressen WHERE id = @id;";

                DbCommand comm = new DbCommand(DbCommandType.crud, sql, paramList.ToArray());

                try
                {
                    db.Excecute(comm);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
        public void InsertAdres(AdresWeergave adres)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                adres.modification_date = DateTime.Now;
                
                try
                {
                    db.BeginTransaction();

                    adres.id = Convert.ToInt32(db.ExcecuteScalar(CreateCommandForAddOrEditAdres(adres)));
                    adres.BewoneradresId = Convert.ToInt32(db.ExcecuteScalar(CreateCommandForAddOrEditBewoneradres(adres)));

                    List<DbCommand> commList = new List<DbCommand>();

                    foreach (ContactpersoonWeergave contactpersoon in adres.Contactpersonen)
                    {
                        contactpersoon.id = Convert.ToInt32(db.ExcecuteScalar(CreateCommandForAddOrEditContactpersoon(contactpersoon)));

                        if (contactpersoon.HasPresentBewoner)
                            db.Excecute(CreateCommandToAddBewonercontactpersoon(adres.BewoneradresId, contactpersoon.id));
                    }
                    db.CommitTransaction();
                }
                catch (Exception ex)
                {
                    db.RollbackTransaction();
                    throw ex;
                }
            }
        }
        public void UpdateAdres(AdresWeergave adres)
        {
            adres.modification_date = DateTime.Now;

            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {

                try
                {
                    db.BeginTransaction();

                    db.Excecute(CreateCommandForAddOrEditAdres(adres));
                    db.Excecute(CreateCommandForAddOrEditBewoneradres(adres));

                    foreach (ContactpersoonWeergave contactpersoon in adres.Contactpersonen)
                    {
                        contactpersoon.adres_id = adres.id;
                        if (contactpersoon.id == 0)
                            contactpersoon.id = Convert.ToInt32(db.ExcecuteScalar(CreateCommandForAddOrEditContactpersoon(contactpersoon)));
                        else
                            db.Excecute(CreateCommandForAddOrEditContactpersoon(contactpersoon));

                        if (contactpersoon.HasPresentBewoner)
                        {
                            try
                            {
                                db.Excecute(CreateCommandToAddBewonercontactpersoon(adres.BewoneradresId, contactpersoon.id));
                            }
                            catch (Exception ex)
                            {
                                if (ex.Message.Contains("UNIQUE"))
                                    continue;
                                else
                                    throw ex;
                            }
                        }
                    }

                    db.CommitTransaction();
                }
                catch (Exception ex)
                {
                    db.RollbackTransaction();
                    throw ex;
                }
            }
        }
        #endregion
        #region Bewoneradres_contactpersonen methods
        public List<int> SelectAllContactpersonenFromBewonerid(int bewonerId)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                List<DbParam> paramList = new List<DbParam>()
                {
                    new DbParam() { Name = "@bewonerId", Value = bewonerId, DbType = DbTypes.db_integer }
                };
                String sql = @"SELECT bc.adrescontactpersoon_id
                               FROM Bewoneradres_contactpersonen AS bc, Bewoner_adressen as ba
                               WHERE bc.bewoneradres_id = ba.id
                               AND ba.bewoner_id = @bewonerId";
                DataTable dt = db.GetDataTable(sql, paramList.ToArray());

                List<int> tempList = new List<int>();
                foreach (DataRow row in dt.Rows)
                    tempList.Add((int)row["adrescontactpersoon_id"]);
                return tempList;
            }
        }
        public override void DeleteContactpersoon(ContactpersoonWeergave selectedContactpersoon)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                List<DbParam> paramList = new List<DbParam>()
                {
                    new DbParam() { Name = "@id", Value = selectedContactpersoon.id, DbType = DbTypes.db_integer }
                };
                String sqlBewonerContactpersonen = @"DELETE FROM Bewoneradres_contactpersonen WHERE adrescontactpersoon_id = @id";
                String sqlMain = @"DELETE FROM Adres_contactpersonen WHERE id = @id";
                List<DbCommand> commList = new List<DbCommand>()
                {
                    new DbCommand(DbCommandType.crud, sqlBewonerContactpersonen, paramList.ToArray()),
                    new DbCommand(DbCommandType.crud, sqlMain, paramList.ToArray())
                };
                try
                {
                    db.Excecute(commList);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
        public void DeleteBewonerContactpersoon(ContactpersoonWeergave contactpersoon)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                List<DbParam> paramList = new List<DbParam>()
                {
                    new DbParam() { Name = "@contactpersoonId", Value = contactpersoon.id, DbType = DbTypes.db_integer },
                    new DbParam() { Name = "@bewoneradresId", Value = contactpersoon.BewoneradresId, DbType = DbTypes.db_integer }
                };
                String sql = @"DELETE FROM Bewoneradres_contactpersonen 
                               WHERE bewoneradres_id = @bewoneradresId AND adrescontactpersoon_id = @contactpersoonId";
                DbCommand comm = new DbCommand(DbCommandType.crud, sql, paramList.ToArray());

                try
                {
                    db.Excecute(comm);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
        #endregion
    }
    public class AdresCampusDALProvider : AdresDALProvider
    {
        public override List<adrestype> SelectAllAdrestypes(bool? isHoofdKeuze, bool? isBedrijf)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                List<DbParam> paramList = new List<DbParam>();
                String sql = @"SELECT *
                               FROM adrestype
                               WHERE actief = 1 AND module_code='LOCATIE'";
                if (isHoofdKeuze.HasValue)
                {
                    sql += "AND ishoofdkeuze = @isHoofdkeuze ";
                    paramList.Add(new DbParam() { Name = "@isHoofdkeuze", Value = GlobalSharedMethods.BooleantoBitConverter(isHoofdKeuze.Value), DbType = DbTypes.db_auto });
                }
                if (isBedrijf.HasValue)
                {
                    sql += "AND isBedrijf = @isBedrijf ";
                    paramList.Add(new DbParam() { Name = "@isBedrijf", Value = GlobalSharedMethods.BooleantoBitConverter(isBedrijf.Value), DbType = DbTypes.db_auto });
                }
                sql += "ORDER BY isBedrijf desc, naam";
                DataTable dt = db.GetDataTable(sql, paramList.ToArray());

                return ConvertDatatableToAdrestypeList(dt);
            }
        }

        public override List<AdresWeergave> SelectAdresweergavesFromOwnerId(int ownerId)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                List<DbParam> paramList = new List<DbParam>()
                {
                    new DbParam() { Name = "@locatieId", Value = ownerId, DbType = DbTypes.db_integer }
                };
                String sql = @"SELECT ab.*, c.name AS land, t.id As type_id, t.naam AS type_naam, t.isBedrijf AS type_isbedrijf, 
t.ishoofdkeuze AS type_ishoofdkeuze, t.opmerking AS type_opmerking, t.actief as type_actief, t.module_code
                               FROM (
                                    SELECT a.*, campus.locatie_id, campus.adrestype_id AS typeid
                                    FROM Adressen AS a, Campussen AS campus
                                    WHERE a.id = campus.adres_id
                                    AND campus.locatie_id = @locatieId
                               ) AS ab, country AS c, adrestype As t
                               WHERE (ab.adrestype_id = t.id OR ab.typeid = t.id)
                               AND ab.landcode = c.code";
                DataTable dt = db.GetDataTable(sql, paramList.ToArray());

                return ConvertDatatableToAdresWeergaveList(dt);
            }
        }

        public void InsertOrUpdateAdres(AdresWeergave adres)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                try
                {
                    db.BeginTransaction();

                    adres.id = Convert.ToInt32(db.ExcecuteScalar(CreateCommandForAddOrEditAdres(adres)));

                    List<DbCommand> commList = new List<DbCommand>();
                    foreach (ContactpersoonWeergave contactpersoon in adres.Contactpersonen)
                    {
                        commList.Add(CreateCommandForAddOrEditContactpersoon(contactpersoon));
                    }
                    db.Excecute(commList);

                    db.CommitTransaction();
                }
                catch (Exception ex)
                {
                    db.RollbackTransaction();
                    throw ex;
                }
            }
        }
        
        public override void DeleteContactpersoon(ContactpersoonWeergave selectedContactpersoon)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                List<DbParam> paramList = new List<DbParam>()
                {
                    new DbParam() { Name = "@id", Value = selectedContactpersoon.id, DbType = DbTypes.db_integer }
                };
                String sqlMain = @"DELETE FROM Adres_contactpersonen WHERE id = @id";
                DbCommand comm = new DbCommand(DbCommandType.crud, sqlMain, paramList.ToArray());
                try
                {
                    db.Excecute(comm);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

        public locatie SelectLocatieFromId(int id)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                List<DbParam> paramList = new List<DbParam>()
                {
                    new DbParam() { Name = "@id", Value = id, DbType = DbTypes.db_integer }
                };
                String sql = @"SELECT * FROM locatie WHERE id = @id";
                DataTable dt = db.GetDataTable(sql, paramList.ToArray());

                DataRow row = dt.Rows[0];
                return new locatie()
                {
                    id = (int)row["loc_id"],
                    naam = row["loc_naam"].ToString(),
                    parent_id = (int)row["loc_parent_id"],
                    beschrijving = row["loc_beschrijving"].ToString(),
                    opmerking = row["loc_opmerking"].ToString()
                };
            }
        }
    }
    // STILL NEEDS TO BE DONE: a.t.m. just gives errors
    public class AdresLeverancierDALProvider : AdresDALProvider
    {
        public override void DeleteContactpersoon(ContactpersoonWeergave selectedContactpersoon)
        {
            throw new NotImplementedException();
        }
        public override List<AdresWeergave> SelectAdresweergavesFromOwnerId(int ownerId)
        {
            throw new NotImplementedException();
        }
        public override List<adrestype> SelectAllAdrestypes(bool? isHoofdKeuze, bool? isBedrijf)
        {
            throw new NotImplementedException();
        }
    }
}
