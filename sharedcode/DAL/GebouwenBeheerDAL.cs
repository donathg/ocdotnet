﻿
using sharedcode.BLL;
using sharedcode.common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sharedcode.DAL
{
   public class GebouwenBeheerDAL
    {
       public static DataTable loadLocatieTree()
       {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                return db.GetDataTable("select locatie.*, (select count(*) from locatie_files where locatie_files.loc_id = locatie.id) attachements from locatie order by parent_id, naam");
            }

       }
       public static bool DeleteLocation(Location locatie)
       {
           using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
           {
               DbCommand com = new DbCommand(DbCommandType.crud,"delete from locatie where loc_id=@id",new DbParam() {Name="@id",Value=locatie.Id} );
               db.Excecute(com);
               return true;
           }
       }
       public static int? SaveLocation(int? id, String name,String description, String remark, int? parentId)
       {
           using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
           {
               DbCommand com = new DbCommand(DbCommandType.procedure, "saveLocatie"
                   , new DbParam() { Name = "@retval", Direction= DbParamDirection.return_value }
                   , new DbParam() { Name = "@id", Value = id }
                   , new DbParam() { Name = "@naam", Value = name }
                   , new DbParam() { Name = "@beschrijving", Value = description }
                   , new DbParam() { Name = "@opmerking", Value = remark }
                   , new DbParam() { Name = "@parent_id", Value = parentId }

                   );
               db.Excecute(com);
               //de nieuwe ID gegenereert van de databank ophalen en in het locatie-object steken.
               return (int)com.Results["@retval"];
           }
       }
      /* public static void SaveLocatieTree(List<Location> locaties)
       {
           List<DbCommand> dbCommands  = new  List<DbCommand>();
           using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectStringClaraFey))
           {
               foreach (Location loc in locaties)
               {
                   DbCommand com = new DbCommand(DbCommandType.procedure);
                   
                   switch (loc.ChangedMode)
                   {
                       case itemChangedMode.added :
                       case itemChangedMode.modified:
                           com.Sql = "iuLocatie";
                           break;

                   }
                   com.Params.Add (new DbParam() { Name = "@id", Value = loc.Id });
                   com.Params.Add(new DbParam() { Name = "@naam", Value = loc.Name });
                   com.Params.Add(new DbParam() { Name = "@beschrijving", Value = loc.Description });
                   com.Params.Add(new DbParam() { Name = "@opmerking", Value = loc.Remark });
                   com.Params.Add(new DbParam() { Name = "@parent_id", Value = loc.ParentId });
                   dbCommands.Add(com);

               }
               db.ExcecuteInTransaction(dbCommands);
           }*/

       

    }
}
