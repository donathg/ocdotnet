﻿using sharedcode.common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using sharedcode.BLL;

namespace sharedcode.DAL
{
    public class BewonersDAL
    {
        private static List<bewoner> ConvertDatatableToListBewoners(DataTable dt)
        {
            List<bewoner> bewList = new List<bewoner>();
            foreach (DataRow row in dt.Rows)
            {
                bewoner bew = bewList.Where(x => x.id == (int)row["id"]).FirstOrDefault();
                if (bew == null)
                {
                    bew = new bewoner()
                    {
                        id = (int)row["id"],
                        code = row["code"].ToString(),
                        voornaam = row["voornaam"].ToString(),
                        achternaam = row["achternaam"].ToString(),
                        sex = row["sex"].ToString(),
                        rijksregisternr = row["rijksregisternr"].ToString()
                    };
                    if (row["geboortedatum"] is DateTime geboorteDatum)
                        bew.geboortedatum = geboorteDatum;
                    if (row["opnameeinde"] is DateTime opnameEinde)
                        bew.opnameeinde = opnameEinde;
                    if (row["nationaliteit"].ToString() is String nation)
                        bew.nationaliteit = nation;

                    if (row["foto_id"] is int fotoId)
                    {
                        bew.bewoner_foto.Add(new bewoner_foto()
                        {
                            id = fotoId,
                            foto = (byte[])row["foto_foto"],
                            bewonerId = (int)row["foto_bewonerId"]
                        });
                    }
                    bewList.Add(bew);
                }

                bew.leefgroep.Add(new leefgroep()
                {
                    id = (int)row["leefgroep_id"],
                    code = row["leefgroep_code"].ToString(),
                    naam = row["leefgroep_naam"].ToString(),
                    fromorbis = row["leefgroep_fromOrbis"].ToString(),
                    forboekh = row["leefgroep_forBoekh"].ToString(),
                    actief = row["leefgroep_actief"].ToString()
                });
            }
            return bewList;
        }

        public static List<bewoner> SelectAllBewoners()
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                String sql = @"SELECT bew.*, foto.id AS foto_id, foto.foto As foto_foto, foto.bewonerId As foto_bewonerId, l.id As leefgroep_id, l.code AS leefgroep_code, l.naam AS leefgroep_naam, 
l.fromorbis AS leefgroep_fromOrbis, l.forboekh AS leefgroep_forBoekh, l.actief As leefgroep_actief
                               FROM bewoner AS bew left outer join bewoner_foto AS foto ON bew.id = foto.bewonerId, bewonerleefgroep AS bl, leefgroep AS l
                               WHERE bl.bewonerid = bew.id
                               AND l.id = bl.leefgroepid
                               AND (bew.opnameeinde is null OR bew.opnameeinde >= GETDATE()-7)
                               AND l.actief = 'Y'";
                DataTable dt = db.GetDataTable(sql);

                return ConvertDatatableToListBewoners(dt);
            }
        }
        public static bewoner SelectBewonerFromId(int id)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                List<DbParam> paramList = new List<DbParam>()
                {
                    new DbParam() { Name = "@id", Value = id, DbType = DbTypes.db_integer}
                };
                String sql = @" SELECT * FROM bewoner WHERE id = @id";

                DataTable dt = db.GetDataTable(sql, paramList.ToArray());

                return ConvertDatatableToListBewoners(dt).SingleOrDefault();
            }
        }
        public static List<int> SelectMyBewonerIds(int usrId)
        {
            List<int> result = new List<int>();
            String sql = @"select   bewonerid from gepland_begeleidingsteam  where gebruikerid=@gebruikerId
union  
select bewonerid   from gepland_begeleidingsteam ,rechten_groep, rechten_groepengebruikers
where gepland_begeleidingsteam.groepcode   = rechten_groep.groep_code and 
rechten_groep.groep_code = rechten_groepengebruikers.groepgeb_groep_code and groepgeb_geb_id=@gebruikerId and rechten_groep.actif=1";
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                DbParam p = new DbParam() { Name = "@gebruikerId", Value = GlobalData.Instance.LoggedOnUser.Id.GetValueOrDefault() };
                List<DbParam> list = new List<DbParam>
                {
                    p
                };
                DataTable dt = db.GetDataTable(sql, list.ToArray());
                foreach (DataRow dr in dt.Rows)
                {
                    result.Add((int)dr[0]);
                }
            }
            return result;

        }
        #region foto Methods
        public static void InsertPicture(bewoner_foto pic)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                // delete all previous foto's of this user
                string sqlDelete = "DELETE FROM bewoner_foto WHERE bewonerId = @bewonerId";

                string sql = @"INSERT INTO bewoner_foto (bewonerId, foto)
                               VALUES (@bewonerId, @foto)";
                List<DbParam> paramList = new List<DbParam>()
                {
                    new DbParam() { Name = "@bewonerId", Value = pic.bewonerId, DbType = DbTypes.db_integer},
                    new DbParam() { Name = "@foto", Value = pic.foto, DbType = DbTypes.db_varbinary}
                };
                List<DbCommand> commList = new List<DbCommand>()
                {
                    new DbCommand(DbCommandType.crud, sqlDelete, paramList.ToArray()),
                    new DbCommand(DbCommandType.crud, sql, paramList.ToArray())
                };

                try
                {
                    db.Excecute(commList);
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
        }
        public static void DeletePicture(bewoner_foto pic)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                String sql = "DELETE FROM bewoner_foto WHERE id = @id";
                List<DbParam> paramList = new List<DbParam>()
                {
                    new DbParam() { Name = "@id", Value = pic.id, DbType = DbTypes.db_integer }
                };
                DbCommand comm = new DbCommand(DbCommandType.crud, sql, paramList.ToArray());

                try
                {
                    db.Excecute(comm);
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
        }
        #endregion
    }
}
