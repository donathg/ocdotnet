﻿using sharedcode.BLL;
using sharedcode.common;
using sharedcode.WeergaveClasses;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sharedcode.DAL
{
    public class KasbeheerExportDAL
    {
        private static List<KasBeheerExportWeergave> ConvertDatatableToListExportWeergaves(DataTable dt)
        {
            List<KasBeheerExportWeergave> tempList = new List<KasBeheerExportWeergave>();
            foreach (DataRow row in dt.Rows)
            {
                KasBeheerExportWeergave weergave = new KasBeheerExportWeergave()
                {
                    Id = (int)row["id"],
                    ParentId = (int)row["parentId"],
                    LeefgroepNaam = row["leefgroep_naam"].ToString(),
                    Bewoner = new bewoner()
                    {
                        voornaam = row["bewoner_voornaam"].ToString(),
                        achternaam = row["bewoner_achternaam"].ToString(),
                        rijksregisternr = row["bewoner_rijksregisternummer"].ToString()
                    },
                    Bedrag = (decimal)row["bedrag"],
                    Omschrijving = row["transactieOmschrijving"].ToString(),
                    AangemaaktDoor = row["modifier"].ToString(),
                    AangemaaktOp = (DateTime)row["modifier_date"],
                    IsAfgesloten = (bool)row["IsClosed"],
                    TransactieCode = row["transactieCode"].ToString(),
                    TransactieDatum = (DateTime)row["transactieDatum"]
                };
                if (!row.IsNull("closer_date"))
                    weergave.AfgeslotenOp = (DateTime?)row["closer_date"];

                tempList.Add(weergave);
            }

            return tempList;
        }

        public static List<KasBeheerExportWeergave> SelectAllKasbeheerItemsFromFilter(KasbeheerExportFilter filter)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                string sql = @" SELECT * FROM (
	                                SELECT tc.id, t.id AS parentId, l.id AS leefgroep_id, l.naam as leefgroep_naam, b.voornaam as bewoner_voornaam, b.achternaam as bewoner_achternaam, b.rijksregisternr as bewoner_rijksregisternummer,
	                                    (case 
										when t.transactieBedrag > 0 then -tc.bedrag
										else tc.bedrag end) AS bedrag
										, t.transactieOmschrijving, g.voornaam + ' ' + g.achternaam as modifier, t.modificationDate as modifier_date, tc.IsClosed, t.transactieKode AS transactieCode,
                                        g2.voornaam + ' ' + g2.achternaam as closer, tc.DateClosed as closer_date, t.transactieDatum
                                    FROM kasbeheer.transactieClient AS tc LEFT OUTER JOIN dbo.gebruiker AS g2 ON tc.ClosedBy = g2.id, kasbeheer.transacties AS t, dbo.leefgroep AS l, 
	                                    dbo.bewoner AS b, dbo.gebruiker AS g 
                                    WHERE tc.transactieId = t.id
                                    AND t.leefgroepId = l.id
                                    AND tc.bewonerId = b.id
                                    AND t.modifier = g.id
                                    AND tc.bedrag != 0
                                UNION
	                                SELECT 0, t.id AS parentId, l.id AS leefgroep_id, l.naam as leefgroep_naam, '' as bewoner_voornaam, '' as bewoner_achternaam, '' as bewoner_rijksregisternummer, t.transactieBedrag, 
		                                t.transactieOmschrijving, g.voornaam + ' ' + g.achternaam as modifier, t.modificationDate as modifier_date, t.afgerekend, t.transactieKode AS transactieCode,
                                        g2.voornaam + ' ' + g2.achternaam as closer, t.afgerekendDatum as closer_date, t.transactieDatum
	                                FROM kasbeheer.transacties AS t LEFT OUTER JOIN dbo.gebruiker AS g2 ON t.afgerekendDoor = g2.id, dbo.leefgroep AS l, dbo.gebruiker AS g
	                                WHERE t.leefgroepId = l.id
	                                AND t.modifier = g.id
	                                AND t.transactieTypeId = 1
                                ) AS abc";
                List<DbParam> paramList = new List<DbParam>();

                if (filter.DateFrom > DateTime.MinValue && filter.DateTo >= filter.DateFrom)
                {
                    sql += @"  WHERE abc.transactieDatum >= @DatumVan
                               AND abc.transactieDatum <= @Datumtot";
                    paramList.Add(new DbParam() { Name = "@DatumVan", Value = filter.DateFrom.Date, DbType = DbTypes.db_date });
                    paramList.Add(new DbParam() { Name = "@Datumtot", Value = filter.DateTo.Date, DbType = DbTypes.db_date });
                }
                if (filter.IsClosed.HasValue)
                {
                    if (paramList != null && paramList.Count != 0)
                        sql += " AND";
                    else
                        sql += " WHERE";

                    if (filter.IsClosed.Value)
                        sql += " abc.IsClosed = 1";
                    else
                        sql += " abc.IsClosed = 0";
                }
                if (filter.LeefgroepId != 0)
                {
                    if (paramList != null && paramList.Count != 0)
                        sql += " AND";
                    else
                        sql += " WHERE";

                    sql += "     leefgroep_id = @leefgroepId";
                    paramList.Add(new DbParam() { Name = "@leefgroepId", Value = filter.LeefgroepId, DbType = DbTypes.db_integer });
                }

                DataTable dt = db.GetDataTable(sql, paramList.ToArray());

                return ConvertDatatableToListExportWeergaves(dt);
            }
        }
        public static List<KasBeheerExportWeergave> SelectAllLastClosedClientKasbeheerItemsFromLeefgroep(int leefgroepId)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                string sql = @" SELECT tc.id, t.id AS parentId, l.naam as leefgroep_naam, b.voornaam as bewoner_voornaam, b.achternaam as bewoner_achternaam, b.rijksregisternr as bewoner_rijksregisternummer,
	                                (case 
										when t.transactieBedrag > 0 then -tc.bedrag
										else tc.bedrag end) AS bedrag
                                    , t.transactieOmschrijving, g.voornaam + ' ' + g.achternaam as modifier, t.modificationDate as modifier_date, tc.IsClosed, t.transactieKode AS transactieCode,
                                    g2.voornaam + ' ' + g2.achternaam as closer, tc.DateClosed as closer_date, t.transactieDatum
                                FROM kasbeheer.transactieClient AS tc LEFT OUTER JOIN dbo.gebruiker AS g2 ON tc.ClosedBy = g2.id, kasbeheer.transacties AS t, dbo.leefgroep AS l, 
	                                dbo.bewoner AS b, dbo.gebruiker AS g 
                                WHERE tc.transactieId = t.id
                                AND t.leefgroepId = l.id
                                AND tc.bewonerId = b.id
                                AND t.modifier = g.id
                                AND tc.bedrag != 0
								AND tc.DateClosed = (SELECT MAX(DateClosed) FROM kasbeheer.transactieClient LEFT OUTER JOIN kasbeheer.transacties AS t ON transactieClient.transactieId = t.id";
                List<DbParam> paramList = new List<DbParam>();

                if (leefgroepId != 0)
                {
                    sql += @"       WHERE t.leefgroepId = @leefgroepId)
                                AND t.leefgroepId = @leefgroepId";
                    paramList.Add(new DbParam() { Name = "@leefgroepId", Value = leefgroepId, DbType = DbTypes.db_integer });
                }
                else
                    sql += @"   )";

                DataTable dt = db.GetDataTable(sql, paramList.ToArray());

                return ConvertDatatableToListExportWeergaves(dt);
            }
        }
        public static void UpdateKasbeheerStateToClosed(List<KasBeheerExportWeergave> kasbeheerList)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                string childIds = "";
                foreach (int kasbeheerId in kasbeheerList.Select(x => x.Id))
                {
                    if (!string.IsNullOrWhiteSpace(childIds))
                        childIds += ", ";
                    childIds += kasbeheerId;
                }
                string parentIds = "";
                foreach (int kasbeheerParentId in kasbeheerList.Select(x => x.ParentId).Distinct())
                {
                    if (!string.IsNullOrWhiteSpace(parentIds))
                        parentIds += ", ";
                    parentIds += kasbeheerParentId;
                }

                string sql = $@"UPDATE kasbeheer.transactieClient
                                SET IsClosed = 1, ClosedBy = @ClosedBy, DateClosed = @dateClosed
                                WHERE id in ({childIds})";
                string sqlParent = $@"  UPDATE kasbeheer.transacties
                                        SET afgerekend = 1, afgerekendDoor = @ClosedBy, afgerekendDatum = @dateClosed
                                        WHERE id in ({parentIds})";


                List<DbParam> paramList = new List<DbParam>()
                {
                    new DbParam { Name = "@ClosedBy", Value = GlobalData.Instance.LoggedOnUser.Id.Value, DbType = DbTypes.db_integer},
                    new DbParam { Name = "@dateClosed", Value = DateTime.Now, DbType = DbTypes.db_datetime }
                };

                List<DbCommand> commList = new List<DbCommand> {
                    new DbCommand(DbCommandType.crud, sql, paramList.ToArray()),
                    new DbCommand(DbCommandType.crud, sqlParent, paramList.ToArray()),
                };

                try
                {
                    db.Excecute(commList);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
    }
}
