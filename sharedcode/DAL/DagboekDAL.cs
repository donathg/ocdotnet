﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using sharedcode.BLL;
using sharedcode.common;
using System.Data;
using System.Data.SqlClient;

namespace sharedcode.DAL
{
    public class DagboekDALManager
    {
        private AbstractDagboekDALProvider _provider;

        public DagboekDALManager(AbstractDagboekDALProvider provider)
        {
            _provider = provider;
            _provider.CachingAllFavorieten();
        }
        
        public List<DagboekWeergave> GetAllDagboekWeergaves(DagBoekFilter filter)
        {
            return _provider.SelectAllDagboekWeergaves(filter);
        }
        public void InsertDagboek(DagboekWeergave dagBoek)
        {
            _provider.InsertDagboek(dagBoek);
        }
        public void InsertDagboekFavoriet(DagboekWeergave dagBoek)
        {
            _provider.InsertDagboekFavoriet(dagBoek);
        }
        public void DeleteDagboekFavoriet(DagboekWeergave dagBoek)
        {
            _provider.DeleteDagboekFavoriet(dagBoek);
        }
        public void DeleteDagboek(DagboekWeergave dagBoek)
        {
            _provider.DeleteDagboek(dagBoek);
        }
        public void UpdateDagboek(DagboekWeergave dagBoek)
        {
            _provider.UpdateDagboek(dagBoek);
        }
    }

    public abstract class AbstractDagboekDALProvider
    {
        private List<int> _favorietenIds;

        protected DagboekWeergave CreateNewDagboekWeergaveFromDataRow(DataRow row)
        {
            dagboek dagBoek = new dagboek()
            {
                id = (int)row["id"],
                creatorId = (int)row["creatorId"],
                creationdate = (DateTime)row["creationdate"],
                descr = row["descr"].ToString(),
            };
            if (row["datum"] is DateTime datum)
                dagBoek.datum = datum;
            if (row["leefgroep_id"] is int leefgroepId)
                dagBoek.leefgroep_id = leefgroepId;
            if (row["bewonerId"] is int bewonerId)
                dagBoek.bewonerId = bewonerId;
            DagboekWeergave dagBoekWeergave = new DagboekWeergave(dagBoek)
            {
                FotoDescr = row["fotodescr"].ToString(),
                CreationdateIntelligentText = row["username"].ToString() + " toegevoegd op " + GlobalSharedMethods.DateTimeToIntelligentText(dagBoek.creationdate),
                IsFavoriet = _favorietenIds.Contains(dagBoek.id)
            };
            if (row["foto"] is byte[] fotoStream)
            {
                dagBoekWeergave.ImageBewoner = GlobalSharedMethods.DecodePhoto(fotoStream);
                dagBoekWeergave.ImageInBytes = fotoStream;
            }

            dagBoekWeergave.DagboekLabels.Add(LabelsDAL.CreateNewLabelFromDataRow(row));

            return dagBoekWeergave;
        }
        protected List<DagboekWeergave> ConvertDataTableToDagboekWeergaves(DataTable dt)
        {
            List<DagboekWeergave> dagboekenList = new List<DagboekWeergave>();
            foreach (DataRow row in dt.Rows)
            {
                // Checks if dagboekenList contains this dagboekweergave
                if (dagboekenList.Find(x => x.DagboekId == (int)row["id"]) is DagboekWeergave weergave)
                    weergave.DagboekLabels.Add(LabelsDAL.CreateNewLabelFromDataRow(row));
                else
                    dagboekenList.Add(CreateNewDagboekWeergaveFromDataRow(row));
            }

            dagboekenList.ForEach(x => x.InitLabelsKommaSeperated());

            return dagboekenList;
        }
        protected int GetDagBoekIdFromDateAndCreator(int creator, DateTime creationdate)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                String sql = @"SELECT id FROM dagboek WHERE creationdate = @creationdate AND creatorId = @creatorId";
                List<DbParam> paramList = new List<DbParam>()
                {
                    new DbParam() { Name = "@creationdate", Value = creationdate, DbType =DbTypes.db_datetime },
                    new DbParam() { Name = "@creatorId", Value = creator, DbType = DbTypes.db_integer }
                };
                DataTable dt = db.GetDataTable(sql, paramList.ToArray());

                return dt.AsEnumerable().Select(x => (int)x[0]).SingleOrDefault();
            }
        }

        public abstract void InsertDagboek(DagboekWeergave dagBoek);
        public abstract List<DagboekWeergave> SelectAllDagboekWeergaves(DagBoekFilter filter);
        public void InsertDagboekFavoriet(DagboekWeergave dagBoek)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                String sql = @"INSERT INTO dagboek_favorieten(gebruikerId, dagboekId, creationDate) 
                               VALUES (@gebruikerId, @dagboekId, @creationdate)";
                List<DbParam> paramList = new List<DbParam>()
                {
                    new DbParam() { Name="@gebruikerId", Value = GlobalData.Instance.LoggedOnUser.Id.Value },
                    new DbParam() { Name="@dagboekid", Value = dagBoek.dagboekDB.id },
                    new DbParam() { Name="@creationdate", Value = DateTime.Today }
                };
                DbCommand comm = new DbCommand(DbCommandType.crud, sql, paramList.ToArray());

                try
                {
                    db.Excecute(comm);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

            CachingAllFavorieten();
        }
        public void DeleteDagboekFavoriet(DagboekWeergave dagBoek)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                String sql = @"DELETE FROM dagboek_favorieten WHERE dagboekId = @dagboekid";
                List<DbParam> paramList = new List<DbParam>()
                {
                    new DbParam() { Name="@dagboekid", Value = dagBoek.dagboekDB.id }
                };

                DbCommand comm = new DbCommand(DbCommandType.crud, sql, paramList.ToArray());

                try
                {
                    db.Excecute(comm);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

            CachingAllFavorieten();
        }
        public void CachingAllFavorieten()
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                List<DbParam> paramList = new List<DbParam>()
                {
                    new DbParam(){ Name = "@userId", Value = GlobalData.Instance.LoggedOnUser.Id.Value, DbType = DbTypes.db_integer }
                };
                String sql = @"SELECT fav.dagboekId FROM dagboek_favorieten as fav WHERE fav.gebruikerId = @userId";
                DataTable dt = db.GetDataTable(sql, paramList.ToArray());

                _favorietenIds = dt.AsEnumerable().Select(x => (int)x[0]).ToList();
            }
        }
        public void DeleteDagboek(DagboekWeergave dagBoek)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                List<DbParam> paramList = new List<DbParam>()
                {
                    new DbParam() { Name="@dagboekId", Value=dagBoek.DagboekId, DbType=DbTypes.db_integer }
                };
                String sqlFavorieten = @"DELETE FROM dagboek_favorieten WHERE dagboekId=@dagboekId";
                String sqlLabels = @"DELETE FROM dagboek_labels WHERE dagboekid=@dagboekId";
                String sqlDagboek = @"DELETE FROM dagboek WHERE id=@dagboekId";

                List<DbCommand> commandList = new List<DbCommand>()
                {
                    new DbCommand(DbCommandType.crud, sqlFavorieten, paramList.ToArray()),
                    new DbCommand(DbCommandType.crud, sqlLabels, paramList.ToArray()),
                    new DbCommand(DbCommandType.crud, sqlDagboek, paramList.ToArray())
                };

                try
                {
                    db.Excecute(commandList);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
        public void UpdateDagboek(DagboekWeergave dagBoek)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                List<DbParam> paramList = new List<DbParam>()
                {
                    new DbParam() { Name="@id", Value=dagBoek.DagboekId, DbType=DbTypes.db_integer },
                    new DbParam() { Name="@message", Value=dagBoek.Message, DbType=DbTypes.db_string },
                    new DbParam() { Name="@modifier", Value=GlobalData.Instance.LoggedOnUser.Id, DbType=DbTypes.db_integer },
                    new DbParam() { Name="@modificationDate", Value=DateTime.Today, DbType=DbTypes.db_date },
                    new DbParam() { Name = "@bewonerId", Value = dagBoek.dagboekDB.bewonerId, DbType = DbTypes.db_integer },
                    new DbParam() { Name = "@leefgroepId", Value = dagBoek.dagboekDB.leefgroep_id, DbType = DbTypes.db_integer }
                };
                String sql = @"UPDATE dagboek SET descr=@message, modifier=@modifier, modification_date=@modificationDate, bewonerId=@bewonerId, leefgroep_id=@leefgroepId 
                               WHERE id=@id";

                DbCommand comm = new DbCommand(DbCommandType.crud, sql, paramList.ToArray());

                try
                {
                    db.Excecute(comm);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
    }
    public class DagboekDALProviderAll : AbstractDagboekDALProvider
    {
        public override void InsertDagboek(DagboekWeergave dagBoek)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                int creatorId = GlobalData.Instance.LoggedOnUser.Id.Value;
                DateTime creationDate = DateTime.Now;
                
                try
                {
                    db.BeginTransaction();

                    String sqlDagboek = @"INSERT INTO dagboek (bewonerId, creatorId, creationdate, descr, leefgroep_id, datum)
                                      VALUES (@bewonerId, @creatorId, @creationdate, @descr, @leefgroepId, @datum)";
                    List<DbParam> paramListDagboek = new List<DbParam>()
                    {
                        new DbParam() { Name="@bewonerId", Value = dagBoek.dagboekDB.bewonerId },
                        new DbParam() { Name="@creatorId", Value = creatorId },
                        new DbParam() { Name="@creationdate", Value = creationDate },
                        new DbParam() { Name="@descr", Value = dagBoek.Message },
                        new DbParam() { Name="@leefgroepId", Value = dagBoek.dagboekDB.leefgroep_id },
                        new DbParam() { Name="@datum", Value = dagBoek.dagboekDB.datum, DbType = DbTypes.db_date }
                    };
                    DbCommand commDagboek = new DbCommand(DbCommandType.crud, sqlDagboek, paramListDagboek.ToArray());
                    int dagboekId = Convert.ToInt32(db.ExcecuteScalar(commDagboek));

                    List<DbCommand> commList = new List<DbCommand>();

                    foreach (labels label in dagBoek.DagboekLabels)
                    {
                        String sqlLabel = @"INSERT INTO dagboek_labels (dagboekid, labelid, modificationdate, modifier) 
                                        VALUES (@dagboekId, @labelId, @modificationdate, @modifier)";
                        List<DbParam> paramListLabel = new List<DbParam>()
                        {
                            new DbParam() { Name="@dagboekId", Value = dagboekId, DbType = DbTypes.db_integer },
                            new DbParam() { Name="@labelId", Value = label.id },
                            new DbParam() { Name="@modificationdate", Value = DateTime.Today },
                            new DbParam() { Name="@modifier", Value = creatorId }
                        };

                        commList.Add(new DbCommand(DbCommandType.crud, sqlLabel, paramListLabel.ToArray()));
                    }
                    db.Excecute(commList);

                    db.CommitTransaction();
                }
                catch (Exception ex)
                {
                    db.RollbackTransaction();
                    throw ex;
                }
            }
        }
        public override List<DagboekWeergave> SelectAllDagboekWeergaves(DagBoekFilter filter)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                #region make query
                List<DbParam> paramList = new List<DbParam>()
                {
                    new DbParam(){ Name = "@presentUserId", Value = GlobalData.Instance.LoggedOnUser.Id.Value, DbType = DbTypes.db_integer },
                    new DbParam(){ Name = "@vandatum", Value = filter.VanDatum, DbType = DbTypes.db_datetime },
                    new DbParam(){ Name = "@totdatum", Value = filter.TotDatum, DbType = DbTypes.db_datetime }
                };
                String sql = @"SELECT DISTINCT dbg.*, l.id as label_id, l.parentid as label_parentid, l.name as label_name, l.descr as label_descr,
l.module as label_module, l.active as label_active, l.dagboek as label_dagboek, l.bijlagen as label_bijlagen, l.deadlinedate as label_deadlinedate
                               FROM(
                                    SELECT db.*, (g.voornaam + ' ' + g.achternaam) as username
                                    FROM(
                                        SELECT d.*, bl.leefgroepid AS leefgroepVanBew, null AS bewVanLeefgroep, (b.voornaam + ' ' + b.achternaam) AS fotodescr, b_f.foto
		                                FROM dagboek AS d LEFT OUTER JOIN bewoner_foto AS b_f ON b_f.bewonerId = d.bewonerId, bewonerleefgroep AS bl , bewoner AS b 
		                                WHERE d.bewonerId = bl.bewonerid
		                                AND b.id = d.bewonerId
	                                UNION
		                                SELECT d.*, null, bl.bewonerid, l.naam, null
		                                FROM dagboek AS d, bewonerleefgroep AS bl, leefgroep AS l
		                                WHERE bl.leefgroepid = d.leefgroep_id
		                                AND l.id = d.leefgroep_id
                                    ) AS db, gebruiker AS g";

                if (GlobalData.Instance.LoggedOnUser.HasAccess("1001")) // wanneer je superuser bent, krijg je alles te zien
                {
                    sql += @"       WHERE db.creatorId = g.id";
                }
                else // wanneer je geen superuser bent, mag je alleen de dagboek van jouw clienten zien
                {
                    sql += @", vw_myBewoners AS myBew
	                                WHERE db.creatorId = g.id
	                                AND (db.bewonerId = myBew.bewonerid OR db.bewVanLeefgroep = myBew.bewonerid)
	                                AND myBew.gebruikerid = @presentUserId";
                }
                if (filter.GekozenGebruiker != null && filter.GekozenGebruiker.id != 0)
                {
                    sql += @"       AND db.creatorId = @creatorId";
                    paramList.Add(new DbParam() { Name = "@creatorId", Value = filter.GekozenGebruiker.id, DbType = DbTypes.db_integer });
                }
                if (!String.IsNullOrEmpty(filter.Bericht))
                {
                    sql += @"       AND db.descr like @message";
                    paramList.Add(new DbParam() { Name = "@message", Value = "%" + filter.Bericht +"%" , DbType = DbTypes.db_string });
                }
                if (filter.Bewoner != null && filter.Bewoner.id != 0)
                {
                    sql += @"       AND (db.bewonerId = @bewonerId OR db.bewVanLeefgroep = @bewonerId)";
                    paramList.Add(new DbParam() { Name = "@bewonerId", Value = filter.Bewoner.id, DbType = DbTypes.db_integer });
                }
                else if (filter.GekozenLeefgroep != null && filter.GekozenLeefgroep.id != 0)
                {
                    sql += @"       AND (db.leefgroep_id = @leefgroepId OR db.leefgroepVanBew = @leefgroepId)";
                    paramList.Add(new DbParam() { Name = "@leefgroepId", Value = filter.GekozenLeefgroep.id, DbType = DbTypes.db_integer });
                }
                sql += @"           AND db.creationdate BETWEEN @vandatum AND @totdatum
                                ) AS dbg, dagboek_labels AS dl, labels AS l
                                WHERE dl.dagboekid = dbg.id
                                AND dl.labelid = l.id";
                if (!string.IsNullOrWhiteSpace(filter.GetLabelIds()))
                    sql += @"   AND l.id IN (" + filter.GetLabelIds() + @")";
                else
                    return new List<DagboekWeergave>();
                sql += @"       ORDER BY dbg.creationdate DESC";
                #endregion
                DataTable dt = db.GetDataTable(sql, paramList.ToArray());

                return base.ConvertDataTableToDagboekWeergaves(dt);
            }
        }
    }
    public class DagboekDALProviderFavorieten : AbstractDagboekDALProvider
    {
        public override void InsertDagboek(DagboekWeergave dagBoek)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                List<DbCommand> commList = new List<DbCommand>();

                #region Add Dagboek To DB
                String sqlDagboek = @"INSERT INTO dagboek (bewonerId, creatorId, creationdate, descr, leefgroep_id)
                                      VALUES (@bewonerId, @creatorId, @creationdate, @descr, @leefgroepId)";
                List<DbParam> paramListDagboek = new List<DbParam>()
                {
                    new DbParam() { Name="@bewonerId", Value = dagBoek.dagboekDB.bewonerId },
                    new DbParam() { Name="@creatorId", Value = GlobalData.Instance.LoggedOnUser.Id.Value },
                    new DbParam() { Name="@creationdate", Value = DateTime.Today },
                    new DbParam() { Name="@descr", Value = dagBoek.Message },
                    new DbParam() { Name="@leefgroepId", Value = dagBoek.dagboekDB.leefgroep_id },
                };
                commList.Add(new DbCommand(DbCommandType.crud, sqlDagboek, paramListDagboek.ToArray()));
                #endregion
                #region Add Labels To DB
                foreach (labels label in dagBoek.DagboekLabels)
                {
                    String sqlLabel = @"INSERT INTO dagboek_labels (dagboekid, labelid, modificationdate, modifier) 
                                        VALUES (@dagboekId, @labelId, @modificationdate, @modifier)";
                    List<DbParam> paramListLabel = new List<DbParam>()
                    {
                        new DbParam() { Name="@dagboekId", Value = dagBoek.dagboekDB.id },
                        new DbParam() { Name="@labelId", Value = label.id },
                        new DbParam() { Name="@modificationdate", Value = DateTime.Today },
                        new DbParam() { Name="@modifier", Value = GlobalData.Instance.LoggedOnUser.Id.Value }
                    };

                    commList.Add(new DbCommand(DbCommandType.crud, sqlLabel, paramListLabel.ToArray()));
                }
                #endregion

                try
                {
                    db.Excecute(commList);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
        public override List<DagboekWeergave> SelectAllDagboekWeergaves(DagBoekFilter filter)
        {
            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                #region make query
                List<DbParam> paramList = new List<DbParam>()
                {
                    new DbParam(){ Name = "@presentUserId", Value = GlobalData.Instance.LoggedOnUser.Id.Value, DbType = DbTypes.db_integer },
                    new DbParam(){ Name = "@vandatum", Value = filter.VanDatum, DbType = DbTypes.db_datetime },
                    new DbParam(){ Name = "@totdatum", Value = filter.TotDatum, DbType = DbTypes.db_datetime }
                };
                String sql = @"SELECT DISTINCT dbg.*, l.id as label_id, l.parentid as label_parentid, l.name as label_name, l.descr as label_descr,
l.module as label_module, l.active as label_active, l.dagboek as label_dagboek, l.bijlagen as label_bijlagen, l.deadlinedate as label_deadlinedate
                               FROM(
                                    SELECT db.*, (g.voornaam + ' ' + g.achternaam) as username
                                    FROM(
	                                    SELECT DISTINCT d.*, (b.voornaam + ' ' + b.achternaam) as fotodescr, bewoner_foto.foto
	                                    FROM dagboek as d, bewoner as b left outer join bewoner_foto on bewoner_foto.bewonerId = b.id
	                                    WHERE d.bewonerId = b.id
                                    UNION
	                                    SELECT DISTINCT d.*, l.naam as fotodescr, null
	                                    FROM dagboek as d, leefgroep as l, bewonerleefgroep as bl
	                                    WHERE d.leefgroep_id = l.id
                                    ) AS db, gebruiker as g, dagboek_favorieten AS fav
                                    WHERE db.creatorId = g.id
                                    AND fav.dagboekId = db.id
                                    AND fav.gebruikerId = @presentUserId";
                
                if (filter.GekozenGebruiker != null && filter.GekozenGebruiker.id != 0)
                {
                    sql += @"       AND db.creatorId = @creatorId";
                    paramList.Add(new DbParam() { Name = "@creatorId", Value = filter.GekozenGebruiker.id, DbType = DbTypes.db_integer });
                }
                if (!String.IsNullOrWhiteSpace(filter.Bericht))
                {
                    sql += @"       AND db.descr = @message";
                    paramList.Add(new DbParam() { Name = "@message", Value = filter.Bericht, DbType = DbTypes.db_string });
                }
                if (filter.Bewoner != null && filter.Bewoner.id != 0)
                {
                    sql += @"       AND (db.bewonerId = @bewonerId";
                    paramList.Add(new DbParam() { Name = "@bewonerId", Value = filter.Bewoner.id, DbType = DbTypes.db_integer });
                    if (filter.GekozenLeefgroep != null && filter.GekozenLeefgroep.id != 0)
                    {
                        sql += @"   OR db.leefgroep_id = @leefgroepId)";
                        paramList.Add(new DbParam() { Name = "@leefgroepId", Value = filter.GekozenLeefgroep.id, DbType = DbTypes.db_integer });
                    }
                    else
                        sql += ")";
                }
                else if (filter.GekozenLeefgroep != null && filter.GekozenLeefgroep.id != 0)
                {
                    sql += @"       AND db.leefgroep_id = @leefgroepId";
                    paramList.Add(new DbParam() { Name = "@leefgroepId", Value = filter.GekozenLeefgroep.id, DbType = DbTypes.db_integer });
                }
                sql += @"           AND db.creationdate BETWEEN @vandatum AND @totdatum
                                ) as dbg, dagboek_labels as dl, labels as l
                                WHERE dl.dagboekid = dbg.id
                                AND dl.labelid = l.id
                                AND l.id IN (" + filter.GetLabelIds() + @")
                                ORDER BY dbg.creationdate DESC";
                #endregion
                DataTable dt = db.GetDataTable(sql, paramList.ToArray());

                return base.ConvertDataTableToDagboekWeergaves(dt);
            }
        }
    }
}
