﻿
using sharedcode.common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sharedcode.DAL
{
    public class LeefGroepDAL
    {
        public static DataTable LoadLeefGroepen()
        {
            String sql = @"select * from leefgroep where actief='Y' order by code";

            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
            {
                return db.GetDataTable(sql);

            }


        }

    }
}
