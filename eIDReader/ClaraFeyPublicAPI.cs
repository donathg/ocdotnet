﻿using EidSamples.tests;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EidSamples
{

    public class EidInfo
    {

        public String Voornaam { get; set; }
        public String Achternaam { get; set; }
        public String Naam
        {
            get
            {

                return Voornaam + " " + Achternaam;
            }


        }
        public String Sex { get; set; }
        public String Land { get; set; }
        public String Adres { get; set; }
        public String Postcode { get; set; }
        public String GeboortePlaats { get; set; }
        public String GeboorteDatum { get; set; }
        public byte[] Foto { get; set; }
    }

    public class ClaraFeyPublicAPI
    {
        DataTests dt;
        SignTests st;
        IntegrityTests it;

        public ClaraFeyPublicAPI()
        {
            dt = new DataTests();
            st = new SignTests();
            it = new IntegrityTests();

        }
        public EidInfo GetCardInfo()
        {
            EidInfo info = new EidInfo();
            ClaraFeyAPI dataTest = new ClaraFeyAPI("beidpkcs11.dll");
            info.Achternaam = dataTest.GetSurname();
            info.Voornaam = dataTest.GetFirstnames();
            info.GeboorteDatum = dataTest.GetDateOfBirth();
            info.GeboortePlaats = dataTest.GetLocationOfBirth();
            byte[] adress  = dataTest.GetAddressFile();
            info.Adres = System.Text.Encoding.Default.GetString(adress);
            info.Foto = dataTest.GetPhotoFile();
            info.Sex = dataTest.GetGender();
          
            return info;
        }
        public String GetSurname()
        {
            ClaraFeyAPI dataTest = new ClaraFeyAPI("beidpkcs11.dll");
            return dataTest.GetSurname();

        }
    }
}
