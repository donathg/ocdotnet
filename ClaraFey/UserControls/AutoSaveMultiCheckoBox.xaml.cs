﻿using ClaraFey.ViewControllers.UserControls;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DevExpress.CodeParser;
using DevExpress.Mvvm.POCO;
using DevExpress.Xpf.Grid;
using DevExpress.XtraEditors.DXErrorProvider;

namespace ClaraFey.UserControls
{
    /// <summary>
    /// Interaction logic for AutoSaveMultiCheckoBox.xaml
    /// </summary>
    public partial class AutoSaveMultiCheckoBox : UserControl
    {

        AutoSaveMultiCheckboxClientDossier vc = new AutoSaveMultiCheckboxClientDossier();
        public AutoSaveMultiCheckoBox()
        {
            InitializeComponent();
            this.DataContext = Vc;
            this.Loaded += AutoSaveMultiCheckoBox_Loaded;
            DevExpress.Xpf.Grid.GridControl.AllowInfiniteGridSize = true;
        }

        private void AutoSaveMultiCheckoBox_Loaded(object sender, RoutedEventArgs e)
        {
            this.gridView.ValidateEditor();
        }



        public static readonly DependencyProperty HeaderIdProperty = DependencyProperty.Register("HeaderId", typeof(int), typeof(AutoSaveMultiCheckoBox), new FrameworkPropertyMetadata(new PropertyChangedCallback(AdjustControl)));
        public int HeaderId
        {
            get
            {
                return (int)GetValue(HeaderIdProperty);
            }
            set
            {
                SetValue(HeaderIdProperty, value);
            }
        }
        public AutoSaveMultiCheckboxClientDossier Vc
        {
            get
            {
                return vc;
            }

            set
            {
                vc = value;
            }
        }

        private static void AdjustControl(DependencyObject source, DependencyPropertyChangedEventArgs e)
        {
            if (source is AutoSaveMultiCheckoBox control)
                control.UpdateControls((int)e.NewValue);
        }
        private void UpdateControls(int headerId)
        {
            this.Vc.LoadList(headerId);
        }
        private void tableView1_CellValueChanged(object sender, DevExpress.Xpf.Grid.CellValueChangedEventArgs e)
        {
            grid.UpdateLayout();
        }

        private void GridColumn_Validate(object sender, DevExpress.Xpf.Grid.GridCellValidationEventArgs e)
        {
            if (e.Row is AutoSaveMultiCheckboxItemClientDossier obj)
            {
                if (e.Value == null)
                {
                    e.ErrorType = ErrorType.Critical;
                    e.ErrorContent = "Verplicht";
                    e.IsValid = false;


                }
            }
        }

        private void GridColumn_OnValidate(object sender, GridCellValidationEventArgs e)
        {
            if (e.Row is AutoSaveMultiCheckboxItemClientDossier obj)
            {
                if (obj.canadddate && obj.IsChecked && gridView.VisibleColumns.Count > 2)
                {
                    gridView.FocusedColumn = gridView.VisibleColumns[2];
                    gridView.ShowEditor();
                }
            }
        }
    }
    public class TextReadOnlyConvertor : MarkupExtension, IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {


            if (value != null)
            {
                if (value.ToString() == "green")
                    return Brushes.DarkSeaGreen;
                else if (value.ToString() == "red")
                    return Brushes.IndianRed;
                else if (value.ToString() == "green")
                {
                    return Brushes.DarkSeaGreen;
                }
            }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            // TODO Not used yet
            return null;
        }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }
    }
    public class DateReadOnlyConvertor : MarkupExtension, IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {


            if (value != null)
            {
                if (value.ToString() == "green")
                    return Brushes.DarkSeaGreen;
                else if (value.ToString() == "red")
                    return Brushes.IndianRed;
                else if (value.ToString() == "green")
                {
                    return Brushes.DarkSeaGreen;
                }
            }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            // TODO Not used yet
            return null;
        }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }
    }
    public class DecimalReadOnlyConvertor : MarkupExtension, IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {



            if (value != null)
            {
                if (value.ToString() == "green")
                    return Brushes.DarkSeaGreen;
                else if (value.ToString() == "red")
                    return Brushes.IndianRed;
                else if (value.ToString() == "green")
                {
                    return Brushes.DarkSeaGreen;
                }
            }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            // TODO Not used yet
            return null;
        }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }
    }
}
