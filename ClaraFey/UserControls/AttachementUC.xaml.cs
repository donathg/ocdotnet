﻿using ClaraFey.ViewControllers.ClientDossier;
using ClaraFey.ViewControllers.Common;
using ClaraFey.ViewControllers.UserControls;
using ClaraFey.Windows.ClientDossier;
using ClaraFey.Windows.Common;
using sharedcode.BLL;
using sharedcode.common;
using sharedcode.CommonObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ClaraFey.UserControls
{
    /// <summary>
    /// Interaction logic for AttachementUC.xaml
    /// </summary>
    public partial class AttachementUC : UserControl
    {
        /** FIELDS */
        public static readonly DependencyProperty LabelsProperty = DependencyProperty.Register("Labels", typeof(string), typeof(AttachementUC), new FrameworkPropertyMetadata(new PropertyChangedCallback(AdjustControlCategory)));
        public static readonly DependencyProperty titleProperty = DependencyProperty.Register("Title", typeof(string), typeof(AttachementUC), new FrameworkPropertyMetadata(new PropertyChangedCallback(AdjustControlTitle)));

        public string Labels
        {
            get
            {
                return (string)GetValue(LabelsProperty);
            }
            set
            {
                SetValue(LabelsProperty, value);
            }
        }
        public string Title
        {
            get
            {
                return (String)GetValue(titleProperty);
            }
            set
            {
                SetValue(titleProperty, value);
            }
        }
        public AttachementVC Vc { get; set; }

        /** CONSTRUCTOR */
        public AttachementUC()
        {
            Vc = new AttachementVC();
            this.DataContext = Vc;
            InitializeComponent();
            DevExpress.Xpf.Grid.DataControlBase.AllowInfiniteGridSize = true;
        }

        /** EVENTS */
        private static void AdjustControlCategory(DependencyObject source, DependencyPropertyChangedEventArgs e)
        {
            if (source is AttachementUC control)
                control.Vc.Category = e.NewValue.ToString();
        }
        private static void AdjustControlTitle(DependencyObject source, DependencyPropertyChangedEventArgs e)
        {
            if (source is AttachementUC control)
            {
                control.Vc.Title = e.NewValue.ToString();
                control.Vc.NotifiyPropertyChanged(nameof(AttachementVC.Title));
            }
        }
        private void btnOpenAttachementWindow_Click(object sender, RoutedEventArgs e)
        {
            List<labels> labelList = new List<labels>();
            foreach (string labelId in Labels.Split(null))
            {
                if (int.TryParse(labelId, out int id))
                {
                    labelList.Add(LabelManager.GetLabelById(id));
                }
            }
            UploadFileWindowSettings settings = new UploadFileWindowSettings("Bijlagen voor item " + this.Vc.Title, "clientdossier")
            {
                AllowAddFile = true,
                AllowEditing = GlobalData.Instance.LoggedOnUser.GetAccesType("1001") == AccessType.access,
                AllowDeleteFile = GlobalData.Instance.LoggedOnUser.GetAccesType("1001") == AccessType.access,
                PresetLabelList = labelList
            };

            UploadFileVC bijlagenVc = new UploadFileVC(
                new BijlagenBLLProviderClientdossier(),
                Vc.BewonerID,
                settings,
                Vc.Category
            );
            bijlagenVc.Load();

            UploadFileWindow fw = new UploadFileWindow(bijlagenVc, CommonObjects.GlobalMethods.Themes.clientdossier);

            fw.ShowDialog();

            this.Vc.NumberOfAttachments = fw.NumberOfItems.GetValueOrDefault();
        }
    }
}
