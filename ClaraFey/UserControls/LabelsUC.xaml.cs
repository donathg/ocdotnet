﻿using ClaraFey.ViewControllers.ClientDossier;
using ClaraFey.Windows.ClientDossier;
using sharedcode.BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ClaraFey.UserControls
{
    /// <summary>
    /// Interaction logic for LabelsUC.xaml
    /// </summary>
    public partial class LabelsUC : UserControl
    {
        
        ViewControllers.ClientDossier.LabelsVC vc = new ViewControllers.ClientDossier.LabelsVC();
        public LabelsUC()
        {
            InitializeComponent();
            this.DataContext = vc;
            DevExpress.Xpf.Grid.GridControl.AllowInfiniteGridSize = true;
        }
        
        public static readonly DependencyProperty AllExistingLabelsProperty = DependencyProperty.Register("AllExistingLabels", typeof(List<labels>), typeof(LabelsUC), new FrameworkPropertyMetadata(new PropertyChangedCallback(AdjustControlAllExistingLabels)));
        public List<labels> AllExistingLabels
        {
            get
            {
                return (List<labels>) GetValue(AllExistingLabelsProperty);
            }
            set
            {
                SetValue(AllExistingLabelsProperty, value);
            }
        }
        public static readonly DependencyProperty AlUserLabelsProperty = DependencyProperty.Register("AlUserLabels", typeof(List<labels>), typeof(LabelsUC), new FrameworkPropertyMetadata(new PropertyChangedCallback(AdjustControlAllUSerLabels)));
        public List<labels> AllUserLabels
        {
            get
            {
                return (List<labels>)GetValue(AlUserLabelsProperty);
            }
            set
            {
                SetValue(AlUserLabelsProperty, value);
            }
        }

        public static readonly DependencyProperty SelectedLabelsProperty = DependencyProperty.Register("SelectedLabels", typeof(List<labels>), typeof(LabelsUC), new FrameworkPropertyMetadata(new PropertyChangedCallback(AdjustControlSelectedLabels)));
        public List<labels> SelectedLabels
        {
            get
            {
                return vc.SelectedLabels;
            }
            set
            {
                SetValue(SelectedLabelsProperty, value);
            }
        }



        private static void AdjustControlAllExistingLabels(DependencyObject source, DependencyPropertyChangedEventArgs e)
        {
 
            var control = source as LabelsUC;
            if (control != null)
            {
               control.vc.SetAllExistingLabels((List<labels>)e.NewValue);
               

            }
        }

        private static void AdjustControlAllUSerLabels(DependencyObject source, DependencyPropertyChangedEventArgs e)
        {
 
            var control = source as LabelsUC;
            if (control != null)
            {
               control.vc.SetAllUSerLabels((List<labels>)e.NewValue);
               

            }
        }



        private static void AdjustControlSelectedLabels(DependencyObject source, DependencyPropertyChangedEventArgs e)
        {

            var control = source as LabelsUC;
            if (control != null)
            {
                control.vc.SetSelectedLabels((List<labels>)e.NewValue);
                 

            }
        }




        private void UpdateControlsLabel(String labelTitle)
        {
            //this.Vc.UpdateLabelTitle(labelTitle);
        }
        private void UpdateControls(String category)
        {
            //this.Vc.LoadList(category);
        }

 
    }
}
