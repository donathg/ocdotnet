﻿using ClaraFey.CommonObjects;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ClaraFey.UserControls
{
    /// <summary>
    /// Interaction logic for MyMultiSelectDropdown.xaml
    /// </summary>
    public partial class MyMultiSelectDropdown : UserControl
    {
        public MyMultiSelectDropdown()
        {
            InitializeComponent();
        }
        private void MyLookUpEdit_KeyDown(object sender, KeyEventArgs e)
        {

        }
        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            Add();
        }
        private void Add()
        {
            Object o = MyLookUpEdit.SelectedItem as Object;
            if (o != null)
            {
                List<Object> list = ((IEnumerable<Object>)this.listBoxSelected.ItemsSource).ToList();
                list.Add(o);
                this.listBoxSelected.ItemsSource = null;
                this.listBoxSelected.ItemsSource = list;
                MyLookUpEdit.SelectedItem = null;
            }
        }
        private void miDelete_Click(object sender, RoutedEventArgs e)
        {
            DeleteSelectedItem();
        }
        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            DeleteSelectedItem();
        }
        private void DeleteSelectedItem()
        {
            Object o = listBoxSelected.SelectedItem as Object;
            if (o != null)
            {
                List<Object> list = ((IEnumerable<Object>)this.listBoxSelected.ItemsSource).ToList();
                list.Remove(o);
                this.listBoxSelected.ItemsSource = null;
                this.listBoxSelected.ItemsSource = list;
                MyLookUpEdit.SelectedItem = null;
            }

        }
        private void listBoxSelected_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Delete)
            {
                DeleteSelectedItem();
            }
        }
        private void  MylookUpEdit_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter || e.Key == Key.Tab)
            {
                Add();

            }
        }

        public IEnumerable ItemsSource
        {
            get { return (IEnumerable)GetValue(ItemsSourceProperty); }
            set { SetValue(ItemsSourceProperty, value); }
        }
        public static readonly DependencyProperty ItemsSourceProperty =
            DependencyProperty.Register("ItemsSource", typeof(IEnumerable), typeof(MyMultiSelectDropdown), new PropertyMetadata(new PropertyChangedCallback(OnItemsSourcePropertyChanged)));

        private static void OnItemsSourcePropertyChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            var control = sender as MyMultiSelectDropdown;
            if (control != null)
                control.OnItemsSourceChanged((IEnumerable)e.OldValue, (IEnumerable)e.NewValue);
        }
        private void OnItemsSourceChanged(IEnumerable oldValue, IEnumerable newValue)
        {
            // Remove handler for oldValue.CollectionChanged
            var oldValueINotifyCollectionChanged = oldValue as INotifyCollectionChanged;

            if (null != oldValueINotifyCollectionChanged)
            {
                oldValueINotifyCollectionChanged.CollectionChanged -= new NotifyCollectionChangedEventHandler(newValueINotifyCollectionChanged_CollectionChanged);
            }
            // Add handler for newValue.CollectionChanged (if possible)
            var newValueINotifyCollectionChanged = newValue as INotifyCollectionChanged;
            if (null != newValueINotifyCollectionChanged)
            {
                newValueINotifyCollectionChanged.CollectionChanged += new NotifyCollectionChangedEventHandler(newValueINotifyCollectionChanged_CollectionChanged);
            }
        }

        void newValueINotifyCollectionChanged_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            //Do your stuff here.
        }


        /**/


        public IEnumerable ItemsSource2
        {
            get { return (IEnumerable)GetValue(ItemsSourceProperty2); }
            set { SetValue(ItemsSourceProperty2, value); }
        }
        public static readonly DependencyProperty ItemsSourceProperty2 =
            DependencyProperty.Register("ItemsSource2", typeof(IEnumerable), typeof(MyMultiSelectDropdown), new PropertyMetadata(new PropertyChangedCallback(OnItemsSourcePropertyChanged2)));

        private static void OnItemsSourcePropertyChanged2(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            var control = sender as MyMultiSelectDropdown;
            if (control != null)
                control.OnItemsSourceChanged2((IEnumerable)e.OldValue, (IEnumerable)e.NewValue);
        }



        private void OnItemsSourceChanged2(IEnumerable oldValue, IEnumerable newValue)
        {
            // Remove handler for oldValue.CollectionChanged
            var oldValueINotifyCollectionChanged = oldValue as INotifyCollectionChanged;

            if (null != oldValueINotifyCollectionChanged)
            {
                oldValueINotifyCollectionChanged.CollectionChanged -= new NotifyCollectionChangedEventHandler(newValueINotifyCollectionChanged_CollectionChanged2);
            }
            // Add handler for newValue.CollectionChanged (if possible)
            var newValueINotifyCollectionChanged = newValue as INotifyCollectionChanged;
            if (null != newValueINotifyCollectionChanged)
            {
                newValueINotifyCollectionChanged.CollectionChanged += new NotifyCollectionChangedEventHandler(newValueINotifyCollectionChanged_CollectionChanged2);
            }
        }

        void newValueINotifyCollectionChanged_CollectionChanged2(object sender, NotifyCollectionChangedEventArgs e)
        {
            //Do your stuff here.
        }





    }
}
