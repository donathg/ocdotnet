﻿using ClaraFey.CommonObjects;
using ClaraFey.Windows.Discussions;
using sharedcode.common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ClaraFey.UserControls
{
    /// <summary>
    /// Interaction logic for statusBarUserControl.xaml
    /// </summary>
    public partial class statusBarUserControl : UserControl
    {


        public String ShutDownInfoText {get;set;}

        public statusBarUserControl()
        {
            DataContext = this;
            InitializeComponent();
            Refresh();
        }
        public void Refresh()
        {

            BiVersionInfo.Content = "Versie : " + GlobalData.Instance.ApplicationVersion;
            ClaraFeyDatabase db = sharedcode.common.GlobalData.Instance.GetConnectedDatabaseType();
            if (db == ClaraFeyDatabase.produktie)
                biDatabase.Content = "Databank : Productie";
            else
                biDatabase.Content = "Databank : Test";
            biUserInfo.Content = "Ingelogd als : " + sharedcode.common.GlobalData.Instance.LoggedOnUser.Name;
            biShutDownInfo.IsVisible = false;
            if (GlobalData.Instance.GlobalShutDownTimer.IsUpdateAvailble)
            {
                biShutDownInfo.IsVisible = true;
                ShutDownInfoText = "Update om : " + GlobalData.Instance.GlobalShutDownTimer.MaintenanceMessage.timeShutDown.ToString("HH:mm");
            }
        }

         
        private void biNewMail_ItemClick(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            DiscussionWindow w = new DiscussionWindow();
            w.Show();
        }
    }
}
