﻿using ClaraFey.ViewControllers.UserControls;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ClaraFey.UserControls
{
    /// <summary>
    /// Interaction logic for AutoSaveMultiCheckoBox.xaml
    /// </summary>
    public partial class AutoSaveSingleCheckoBox : UserControl
    {

        AutoSaveSingleCheckboxClientDossier vc = new AutoSaveSingleCheckboxClientDossier();
        public AutoSaveSingleCheckoBox()
        {
            this.DataContext = Vc;
            InitializeComponent();
            DevExpress.Xpf.Grid.GridControl.AllowInfiniteGridSize = true;
        }
        public static readonly DependencyProperty HeaderIdProperty = DependencyProperty.Register("HeaderId", typeof(int), typeof(AutoSaveSingleCheckoBox), new FrameworkPropertyMetadata(new PropertyChangedCallback(AdjustControl)));
        public int HeaderId
        {
            get
            {
                return (int)GetValue(HeaderIdProperty);
            }
            set
            {
                SetValue(HeaderIdProperty, value);
            }
        }
        public AutoSaveSingleCheckboxClientDossier Vc
        {
            get
            {
                return vc;
            }

            set
            {
                vc = value;
            }
        }

        private static void AdjustControl(DependencyObject source, DependencyPropertyChangedEventArgs e)
        {
            var control = source as AutoSaveSingleCheckoBox;
            if (control != null)
                control.UpdateControls((int)e.NewValue);
        }
        private void UpdateControls(int headerId)
        {
           // this.Vc.LoadList(headerId);
        }
        private void tableView1_CellValueChanged(object sender, DevExpress.Xpf.Grid.CellValueChangedEventArgs e)
        {
            //grid.UpdateLayout();
        }
    }
    

}
