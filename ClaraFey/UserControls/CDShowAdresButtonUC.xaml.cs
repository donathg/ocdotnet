﻿using ClaraFey.ViewControllers.UserControls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using sharedcode.BLL;
using sharedcode.WeergaveClasses;
using ClaraFey.Windows.Common;
using System.ComponentModel;
using sharedcode.Caches;

namespace ClaraFey.UserControls
{
    /// <summary>
    /// Interaction logic for AttachementUC.xaml
    /// </summary>
    public partial class CDShowAdresButtonUC : UserControl, INotifyPropertyChanged
    {
        /** FIELDS */
        public static readonly DependencyProperty BewonerAdresTypeIdsProperty = DependencyProperty.Register(
            "BewonerAdresTypeIds", 
            typeof(String), 
            typeof(CDShowAdresButtonUC), 
            new FrameworkPropertyMetadata(new PropertyChangedCallback(AdjustControlBewonerAdresTypeIds))
        );
        public static readonly DependencyProperty WidgetNameProperty = DependencyProperty.Register(
            "WidgetName",
            typeof(String), 
            typeof(CDShowAdresButtonUC),
            new FrameworkPropertyMetadata(new PropertyChangedCallback(AdjustControlWidgetName))
        );

        public event PropertyChangedEventHandler PropertyChanged;

        public String BewonerAdresTypeIds
        {
            get
            {
                return GetValue(BewonerAdresTypeIdsProperty).ToString();
            }
            set
            {
                SetValue(BewonerAdresTypeIdsProperty, value);
            }
        }
        public String WidgetName
        {
            get
            {
                return GetValue(WidgetNameProperty).ToString();
            }
            set
            {
                SetValue(WidgetNameProperty, value);
            }
        }
        public CDShowAdressButtonVC Vc { get; set; }

        /** CONSTRUCTORS */
        public CDShowAdresButtonUC()
        {
            Vc = new CDShowAdressButtonVC();
            InitializeComponent();
            DevExpress.Xpf.Grid.DataControlBase.AllowInfiniteGridSize = true;
        }

        /** EVENTS*/
        private static void AdjustControlBewonerAdresTypeIds(DependencyObject source, DependencyPropertyChangedEventArgs e)
        {
            if (source is CDShowAdresButtonUC control)
            {
                control.Vc.InitList(e.NewValue.ToString());
            }
        }
        private static void AdjustControlWidgetName(DependencyObject source, DependencyPropertyChangedEventArgs e)
        {
            if (source is CDShowAdresButtonUC control)
            {
                control.titleLabel.Content = e.NewValue.ToString();
            }
        }
        private void BtnShowAdresses_Click(object sender, RoutedEventArgs e)
        {
            AdressenWindow w = new AdressenWindow()
            {
                Settings = new AdressenListSettings()
                {
                    Bewoner = BewonersListCache.Instance.GetBewoner(Vc.BewonerID),
                    AdresTypes = Vc.AdresTypeList
                }
            };
            w.ShowDialog();
            Vc.Title = (String)GetValue(WidgetNameProperty);
        }
    }
}
