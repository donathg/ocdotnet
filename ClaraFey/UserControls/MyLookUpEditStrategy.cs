﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Xpf.Grid.LookUp;
using DevExpress.Data.Filtering;
using DevExpress.Xpf.Grid;

namespace ClaraFey.UserControls
{







    /// <summary>
    /// LookUpedit die in alle colommen zoekt
    /// 
    /// 
    /// </summary>
    /// 
    /*


          <userControls:MyLookUpEdit
            x:Name="NamelookUpEdit"
            ItemsSource="{Binding bewonersList}"
            DisplayMember="Naam"
            ValueMember="Id"
            AutoPopulateColumns="False"
            AutoComplete="True"
            IncrementalFiltering="True"
            ImmediatePopup="True"
            Width="290"
            DataContext="{Binding}"
            DisplayFormatString="{Binding}">
                            <userControls:MyLookUpEdit.ColumnsToFilter>
                                <userControls:ColumnItem FieldName="Code"/>
                                <userControls:ColumnItem FieldName="Voornaam"/>
                                <userControls:ColumnItem FieldName="Achternaam"/>
                            </userControls:MyLookUpEdit.ColumnsToFilter>
                            <dxg:LookUpEdit.PopupContentTemplate>
                                <ControlTemplate>
                                    <dxg:GridControl Name="PART_GridControl">
                                        <dxg:GridControl.Columns>
                                            <dxg:GridColumn FieldName="Code" Header="Code" />
                                            <dxg:GridColumn FieldName="Voornaam" Header="Voornaam"></dxg:GridColumn>
                                            <dxg:GridColumn FieldName="Achternaam" Header="Achternaam"></dxg:GridColumn>
                                        </dxg:GridControl.Columns>
                                        <dxg:GridControl.View>
                                            <dxg:TableView AutoWidth="True" ShowSearchPanelFindButton="True"/>
                                        </dxg:GridControl.View>
                                    </dxg:GridControl>
                                </ControlTemplate>
                            </dxg:LookUpEdit.PopupContentTemplate>
                        </userControls:MyLookUpEdit>
    */

    public class MyLookUpEditStrategy : LookUpEditStrategy
    {
        public MyLookUpEditStrategy(LookUpEdit editor)
            : base(editor)
        {
        }

        public override void UpdateDisplayFilter()
        {
           Editor.Dispatcher.BeginInvoke(new Action(() => ItemsProvider.SetDisplayFilterCriteria(CreateDisplayFilterCriteria(AutoSearchText), this.CurrentDataViewHandle)));
           
        }
        protected CriteriaOperator CreateDisplayFilterCriteria(string text)
        {
            MyLookUpEdit lookUp = (MyLookUpEdit)Editor;
            GroupOperator groupOperator = new GroupOperator() { OperatorType = GroupOperatorType.Or };
            foreach (ColumnItem col in lookUp.ColumnsToFilter)
            {
                List<CriteriaOperator> list = new List<CriteriaOperator>();
                list.Add(new OperandProperty() { PropertyName = col.FieldName });
                list.Add(new OperandValue() { Value = text });
                groupOperator.Operands.Add(new FunctionOperator(FunctionOperatorType.Contains, list));
            }
            return groupOperator;
        }
    }
}
