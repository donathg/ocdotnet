﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DevExpress.Mvvm.DataAnnotations;

namespace DXSampleNotificationSevice.ViewModel
{
    [POCOViewModel]
    public class CustomNotificationViewModel
    {
        public virtual string Caption { get; set; }
        public virtual string Content { get; set; }
        public virtual byte[] Image { get; set; }
       
    }
}