﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;
using ClaraFey.CommonObjects;
using sharedcode.BLL;

namespace ClaraFey.Notifications
{
    public class NotificationChecker : IDisposable
    {
        DispatcherTimer dt = new DispatcherTimer();
        DateTime _lastDate = DateTime.Now.AddDays(-2);
        private void timer_Tick(object sender, EventArgs e)
        {
          List<dashboard> dashboardItems = NotificationDLL.GetNotificationsClientDossier(_lastDate);
            foreach (dashboard d in dashboardItems)
            {
                SentNotificationClientDossier(d);
            }
             _lastDate = DateTime.Now;
        }
        private void SentNotificationClientDossier(dashboard d)
        {
            try
            {
                NotificationSubModuleType type = ConvertSubCategoryFromDB(d.subcategory);
                GlobalMethods.ShowCustomNotificationClientDossier(type, UserBLL.GetUser(d.creatorId), d.title, d.descr, d.bewoner,
                    NotificationDurationType.untilClick, true);
            }
            catch { }

        }

        private NotificationSubModuleType ConvertSubCategoryFromDB(String subcategory)
        {
            switch (subcategory)
            {
                case "beeldvorming":
                    return NotificationSubModuleType.Beeldvorming;
                    break;

            }
            return NotificationSubModuleType.NotDefined;

        }

        public void Dispose()
        {
            dt.Stop();
            dt = null; 
        }

        public NotificationChecker()
        {
            dt.Tick += new EventHandler(timer_Tick);
            dt.Interval = new TimeSpan(0, 0, 15); // execute every minute
            dt.Start();


        }


    }
}
