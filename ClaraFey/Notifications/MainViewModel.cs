﻿using System;
using System.IO;
using System.Windows;
using System.Windows.Media.Imaging;
using ClaraFey.CommonObjects;
using DevExpress.CodeParser;
using DevExpress.Mvvm;
using DevExpress.Mvvm.DataAnnotations;
using DevExpress.Mvvm.POCO;
using DXSampleNotificationSevice.ViewModel;
using sharedcode.BLL;
using sharedcode.common;
using sharedcode.CommonObjects;

namespace ClaraFey.Notifications
{

    public enum NotificationModuleType
    {
        General,
        Clientdossier

    }
    public enum NotificationSubModuleType
    {
        Beeldvorming,
        NotDefined


    }
    public enum NotificationDurationType
    {
        veryShort,
        normal,
        untilClick

    }



    [POCOViewModel]
    public class MainViewModel
    {

        [ServiceProperty(Key = "ServiceWithCustomNotifications")]
        protected virtual INotificationService CustomNotificationService { get { return null; } }

        public virtual TimeSpan Duration { get; set; }
       
        public void ShowCustomNotificationBewoner(NotificationSubModuleType subModule, User user , String caption, String text, bewoner bewoner , NotificationDurationType duration, bool canGotoModule)
        {
            CustomNotificationViewModel vm = ViewModelSource.Create(() => new CustomNotificationViewModel());
            vm.Caption = caption;
            vm.Content = text;//  String.Format("Time: {0}", DateTime.Now);
            if (duration == NotificationDurationType.veryShort)
                this.Duration = TimeSpan.FromSeconds(1);
            else if (duration == NotificationDurationType.normal)
                this.Duration = TimeSpan.FromSeconds(10);
            else
                this.Duration = TimeSpan.FromDays(1);
           /* if (user != null)
            {
                vm.Image = UserBLL.GetPicture(UserBLL.GetPicturePath(user.PersNr));
            }*/
            if (bewoner != null)
            {
                vm.Image = bewoner.GetFoto();
            }

            INotification notification = CustomNotificationService.CreateCustomNotification(vm);
            notification.ShowAsync();
        }

        public void ShowCustomNotification(String caption, String text, byte[] foto)
        {
            CustomNotificationViewModel vm = ViewModelSource.Create(() => new CustomNotificationViewModel());
            vm.Caption = caption;
            vm.Content = text;//  String.Format("Time: {0}", DateTime.Now);
            this.Duration = TimeSpan.FromSeconds(1);
            if (foto !=null)  
                    vm.Image = foto;
            INotification notification = CustomNotificationService.CreateCustomNotification(vm);
            notification.ShowAsync();
        }
        public void ShowCustomNotification(String caption, String text, User user)
        {
          
            CustomNotificationViewModel vm = ViewModelSource.Create(() => new CustomNotificationViewModel());
            vm.Caption = caption;
            vm.Content = text;//  String.Format("Time: {0}", DateTime.Now);
            this.Duration = TimeSpan.FromSeconds(1);
           /* if (user != null)
            {
                vm.Image = UserBLL.GetPicture(UserBLL.GetPicturePath(user.PersNr));
            }*/
            INotification notification = CustomNotificationService.CreateCustomNotification(vm);
            notification.ShowAsync();
        }


    }
}