﻿using ClaraFey.Windows.Werkbonnenbeheer;
using DevExpress.Xpf.Core;
using DevExpress.Xpf.Grid;
using sharedcode.BLL;
using sharedcode.common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media.Imaging;
using ClaraFey.Notifications;
using sharedcode.CommonObjects;
using ClaraFey.Windows.Common;
using System.ComponentModel;

namespace ClaraFey.CommonObjects
{
    public class GlobalMethods
    {
        public enum Themes
        {
            normal,
            clientdossier
        }

        public static String GetImageString(String imageName)
        {
            WriteToLog("GlobalMethods.GetImageString");
            return $"/ClaraFey;component/images/{imageName}";
        }
        public static void WriteToLog (String info)
        {
            //var time = DateTime.Now;
            //File.AppendAllText(@"C:\temp\clarafey_wpf_log.csv", string.Format("{0},{1}", time, info));
        }
        public static void DeleteLog()
        {
           // File.Delete(@"C:\temp\clarafey_wpf_log.csv");
        }
        public static void ShowCustomNotification(String caption, String text, User user)
        {
            MainWindow w = GetMainWindow();
            w.ShowCustomNotification(caption, text,  user);
        }
        public static void ShowCustomNotificationClientDossier(NotificationSubModuleType subModule, User user, String caption, String text, 
            bewoner bewoner, NotificationDurationType duration, bool canGotoModule)
        {
            MainWindow w = GetMainWindow();
            w.ShowCustomNotificationBewoner(subModule, user, caption, text, bewoner, duration, canGotoModule);
        }
        public static void ShowCustomNotification(String text1, String text2, byte[] image)
        {
            MainWindow w = GetMainWindow();
            w.ShowCustomNotification(text1, text2, image);
        }
        public static void StartDevExpressMap(String adres, int zoomLevel)
        {
            adres = adres.Replace("BE", "+BE");
            /// Van kristus koning naar opgegeven adres. Current location werkt niet: pakt de locatie van de server en die staat niet op kristus koning
            String url = String.Format("https://www.google.com/maps/dir/BuSO+Kristus+Koning,+Sint-Job-in-t-Goor,+BE/{0}", adres);
            /// Toont het adres met een tag aan op google maps
            // String url = String.Format("http://maps.google.com/?q={0}", adres);
            StartBrowser(url);
            //      mapWindow mapWindow = new mapWindow(new ViewControllers.Common.mapSettings() { SearchString = adres });
            //      mapWindow.Show();
        }
        public static void StartBrowserGoogleMaps (String adres, int zoomLevel)
        {
            adres = adres.Replace("BE", "+BE");
            /// Van kristus koning naar opgegeven adres. Current location werkt niet: pakt de locatie van de server en die staat niet op kristus koning
            String url = String.Format("https://www.google.com/maps/dir/BuSO+Kristus+Koning,+Sint-Job-in-t-Goor,+BE/{0}", adres); 
            /// Toont het adres met een tag aan op google maps
            // String url = String.Format("http://maps.google.com/?q={0}", adres);
            StartBrowser(url);
      //      mapWindow mapWindow = new mapWindow(new ViewControllers.Common.mapSettings() { SearchString = adres });
      //      mapWindow.Show();
        }
        public static void StartBrowserWithBlazorApp(String module)
        {
            String externalLogonInfoDecoded;
            if (sharedcode.common.GlobalData.Instance.LoggedOnUser.IsLoggedOn)
            {
                String externalLogonInfo = sharedcode.common.GlobalData.Instance.LoggedOnUser.PersNr + "@" + DateTime.Now.ToLongTimeString();
                externalLogonInfoDecoded = GlobalSharedMethods.EncodeStringToBase64(externalLogonInfo);
                
            }
            else
                return;
        
            String url = String.Format("{0}/{1}/{2}", sharedcode.common.GlobalData.Instance.BlazorAppUrl, externalLogonInfoDecoded, module);
            StartBrowser(url); 
        }
        public static void StartBrowser(String url)
        {
            try
            {
                if (String.IsNullOrWhiteSpace(GlobalData.Instance.LoggedOnUser.Settings.browserpath))
                    System.Diagnostics.Process.Start(url);
                else
                {
                    if (GlobalData.Instance.LoggedOnUser.Settings.browserpath == "microsoft-edge")
                        System.Diagnostics.Process.Start($"microsoft-edge:{url}");
                    else 
                        System.Diagnostics.Process.Start(GlobalData.Instance.LoggedOnUser.Settings.browserpath, url);

                }
            }
            catch (Win32Exception)
            {
                MessageBox.Show("De gekozen browser is niet geïnstalleerd. \n\rGelieve in de persoonlijke instellingen de browser te veranderen of de browser te installeren", "Probleem met de browser", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public static void SetTheme(object o, Themes theme)
        {
            /*    if (theme == Themes.normal)
                    ThemeManager.SetThemeName((DependencyObject)o, "Office2010Black");
                else
                    ThemeManager.SetThemeName((DependencyObject)o, "DeepBlue"); //"Office2010Blue");*/



            ThemeManager.SetThemeName((DependencyObject)o, "DeepBlue"); //"Office2010Blue");*
          
        }
        /// <summary>
        /// Deze methode kijkt of een Window is geopend
        /// if (Helpers.IsWindowOpen<Window>("MyWindowName"))
        /// if (Helpers.IsWindowOpen<MyCustomWindowType>("CustomWindowName"))
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="name"></param>
        /// <returns></returns>
        public static bool IsWindowOpen<T>(string name = "") where T : Window
        {
            return string.IsNullOrEmpty(name)
               ? Application.Current.Windows.OfType<T>().Any()
               : Application.Current.Windows.OfType<T>().Any(w => w.Name.Equals(name));
        }
        public static MainWindow GetMainWindow()
        {
            return (MainWindow)GetOpenWindow("logonForm");
        }
        public static Window GetOpenWindow(string name = "")
        {
            foreach (Window w in Application.Current.Windows)
            {
                if (w.Name == name)
                    return w;
            }
            return null;
        }
        public static int NumWindowOpen<T>(string name = "") where T : Window
        {
            return string.IsNullOrEmpty(name)
               ? Application.Current.Windows.OfType<T>().Count()
               : Application.Current.Windows.OfType<T>().Count(w => w.Name.Equals(name));
        }
        public static void ActivateWindown<T>(string name = "") where T : Window
        {

          var window =  Application.Current.Windows.OfType<T>().FirstOrDefault(w => w.Name.Equals(name));
            if (window != null)
            {

                if (window.IsVisible == false)
                    window.Show();
                else if(window.WindowState == WindowState.Minimized)
                    window.WindowState = WindowState.Normal;

                else
                {
                    window.Activate();

                    window.Topmost = true;  // important
                    window.Topmost = false; // important
                    window.Focus();
                }
            }
        }       
        public static void OpenWerkaanvraagWindow(werkopdracht_diensten dienst, WerkopdrachtUserType userType, String windowTitle)
        {
            bool isOpen = GlobalMethods.IsWindowOpen<Window>("WindowWerkaanvraag");
            if (isOpen)
                GlobalMethods.ActivateWindown<Window>("WindowWerkaanvraag");
            else
            {
                WerkbonnenbeheerWindow win = new WerkbonnenbeheerWindow(sharedcode.BLL.WerkopdrachtType.Werkaanvraag, dienst, userType)
                {
                    Title = windowTitle
                };
                win.Show();
            }
        }
        public static void OpenWerkbonWindow(werkopdracht_diensten dienst, WerkopdrachtUserType userType,String windowTitle)
        {
            bool isOpen = GlobalMethods.IsWindowOpen<Window>("WindowWerkbon");
            if (isOpen)
                GlobalMethods.ActivateWindown<Window>("WindowWerkbon");
            else
            {
                WerkbonnenbeheerWindow win = new WerkbonnenbeheerWindow(sharedcode.BLL.WerkopdrachtType.Werkbon, dienst, userType)
                {
                    Title = windowTitle
                };
                win.Show();
            }
        }
        /// <param name="window">The window whose size you want to change</param>
        /// <param name="width">The new width of the window</param>
        /// <param name="height">The new height of the window</param>
        public static void SetSizeAndCenterWindow(Window window, int width, int height)
        {
            double ScreenWidth = SystemParameters.PrimaryScreenWidth;
            double ScreenHeight = SystemParameters.PrimaryScreenHeight;

            window.Width = width;
            window.Height = height;

            window.Left = (ScreenWidth - window.Width) / 2;
            window.Top = (ScreenHeight - window.Height) / 2;
        }
        /// <param name="window">The window whose size you want to change</param>
        /// <param name="percentage">Has to be a value between 1 & 100</param>
        public static void SetSizeAndCenterWindow(Window window, double percentage)
        {
            double ScreenWidth = SystemParameters.PrimaryScreenWidth;
            double ScreenHeight = SystemParameters.PrimaryScreenHeight;

            if (percentage <= 100 && percentage > 0)
            {
                window.Width = ScreenWidth * percentage / 100;
                window.Height = ScreenHeight * percentage / 100;
            }
            else
                return;

            window.Left = (ScreenWidth - window.Width) / 2;
            window.Top = (ScreenHeight - window.Height) / 2;
        }
        public static void ExportGridToExcel (GridControl gc, String filename)
        {
            System.Windows.Forms.SaveFileDialog d = new System.Windows.Forms.SaveFileDialog
            {
                FileName = filename,
                Filter = "Excel File|*.xlsx",
                Title = "Exporteren naar Excel",
                RestoreDirectory = true,
                CheckFileExists = false,
                CheckPathExists = false,
                SupportMultiDottedExtensions = true,
                AutoUpgradeEnabled = true,
                DefaultExt = Path.GetExtension("xlsx"),
                AddExtension = true
            };

            string dates = DateTime.Now.ToString("dd-MM-yyyy");

            if (d.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                try
                {
                    ((TableView)gc.View).ExportToXlsx(d.FileName);

                    String question = String.Format("Wilt u het bestand {0} openen ?", d.FileName);
                    if (MessageBox.Show(question, "Openen", MessageBoxButton.YesNo, MessageBoxImage.Question) != MessageBoxResult.Yes)
                        return;

                    System.Diagnostics.Process.Start(d.FileName);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Fout", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }
        /*
        #region EXIT APPLICATION

        private static bool shutdownInProgress = false;
        private CommonUI.frmMessageBox frmMaintananceMessage;

        private void ExitApplicationHardWay()
        {
            if (GlobalData.Instance.ExitApplicationHardWay == false)
            {
                GlobalData.Instance.ExitApplicationHardWay = true;
            }
            if (shutdownInProgress == false)
            {
                try
                {
                    shutdownInProgress = true;

                    // This is added to prevent locked records
                    PrologUser.Logoff();

                    globalLogoffTimer.Dispose();
                    globalLogoffTimer = null;

                    List<Form> flist = new List<Form>();
                    foreach (Form f in Application.OpenForms)
                    {
                        if (f is MainForm || f.Name == "frmMaintananceMessage")
                        {
                            frmMaintananceMessage = (CommonUI.frmMessageBox)f;
                        }
                        else
                            flist.Add(f);
                    }

                    foreach (Form f in flist)
                    {
                        //closeFormFromAnotherThread(f);
                        disposeFormFromAnotherThread(f);
                    }
                    flist.Clear();
                    if (frmMaintananceMessage != null)
                        frmMaintananceMessage.Close();
                    frmMaintananceMessage.Dispose();

                    this.Close();
                    Application.Exit();
                    // The next call requires that you have SecurityPermissionFlag.UnmanagedCode permissions
                    Environment.Exit(0); //Closes (kills) process
                }
                catch (Exception)
                {
                    Application.Exit();
                    // The next call requires that you have SecurityPermissionFlag.UnmanagedCode permissions
                    Environment.Exit(1); //Closes (kills) process
                }
            }
        }

        private delegate void closeFormFromAnotherThreadDelegate(Form f);

        public void closeFormFromAnotherThread(Form f)
        {
            if (f.InvokeRequired)
            {
                f.Invoke(new closeFormFromAnotherThreadDelegate(closeFormFromAnotherThread), f);
            }
            else
            {
                if (f is ModuleForm)
                    ((ModuleForm)f).OnMaintainanceShutdown();
                if (f is ProjectForm)
                    ((ProjectForm)f).OnMaintainanceShutdown();
                f.Close();
            }
        }

        private delegate void disposeFormFromAnotherThreadDelegate(Form f);

        public void disposeFormFromAnotherThread(Form f)
        {
            if (f.InvokeRequired)
            {
                f.Invoke(new disposeFormFromAnotherThreadDelegate(disposeFormFromAnotherThread), f);
            }
            else
            {
                if (f is ModuleForm)
                    ((ModuleForm)f).OnMaintainanceShutdown();
                if (f is ProjectForm)
                    ((ProjectForm)f).OnMaintainanceShutdown();
                f.Close();
                f.Dispose();
                GC.Collect();
            }
        }

        #endregion EXIT APPLICATION*/
    }
}
