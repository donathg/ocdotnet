﻿using DevExpress.Data;
using DevExpress.Mvvm;
using DevExpress.Xpf.Data;
using DevExpress.Xpf.Editors.Settings;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClaraFey.CommonObjects
{


    public class SMMWindow2ContextMenuItem
    {
        public String Title { get; set; }
        public int UniqueId { get; set; }
        public int Level { get; set; }

        public delegate void ContextMenuItemClicked(Object row, Object vc, SMMWindow2ContextMenuItem item);
        public ContextMenuItemClicked ContextMenuItemClickedMethod { get; set; }

    }

    public class SMMWindow2LevelTree : SMMWindow2Level
    {
        public SMMWindow2LevelTree(int level, string title) : base(level, title)
        {
        }
    }

    public class GroupSummary
    {

        public String FieldName { get; set; }
        public String DisplayFormat { get; set; }
        public SummaryItemType SummaryType { get; set; }
    }

    public class SMMWindow2Level : ViewModelBase, INotifyPropertyChanged
    {
        public delegate void MouseDoubleClick(Object value);
        public MouseDoubleClick MouseDoubleClickMethod { get; set; }
        public delegate void BeforeUpDateDelegate(object row);
        public BeforeUpDateDelegate BeforeUpdateMethod { get; set; }
        public delegate void AfterUpDateDelegate(object row);
        public AfterUpDateDelegate AfterUpdateMethod { get; set; }
        public delegate void AfterDeleteDelegate(object row);
        public AfterUpDateDelegate AfterDeleteMethod { get; set; }
        public new event PropertyChangedEventHandler PropertyChanged;

        public List<SMMWindow2ContextMenuItem> ContextMenuItems = new List<SMMWindow2ContextMenuItem>();
        public List<SMMWindow2Column> Colums { get; set; }
        public bool CanExportToExcel { get; set; }       
        public int Level { get; set; }
        public String JoinField { get; set; }
        public String Title { get; set; }
        private object _dataSource;
        public object DataSource
        {
            get
            {
                return _dataSource;
            }
            set
            {
                _dataSource = value;
                NotifiyPropertyChanged(nameof(DataSource));
            }
        }
        public bool AllowDelete { get; set; }
        public bool AllowAdd1RowWhenNoRows { get; set; }
        public bool AllowAdd { get; set; }
        public bool AllowModify { get; set; }
        public bool AllowSearch { get; set; }
        public bool AllowFilter { get; set; }
        public bool AllowGroup { get; set; }


        public GroupSummary GroupSummary { get; set; } = null;

        public SMMWindow2Level(int level, String title)
        {
            Level = level;
            Colums = new List<SMMWindow2Column>();
            this.Title = title;
        }
        
        public void AddColumns(params SMMWindow2Column[] cols)
        {
            foreach (SMMWindow2Column col in cols)
            {
                col.Level = Level;
                this.Colums.Add(col);
            }
        }
        public SMMWindow2Column GetColomnByFieldName(String fieldName)
        {
            foreach (SMMWindow2Column col in this.Colums)
            {
                if (col.FieldName == fieldName)
                    return col;
            }
            return null;
        }
        public void NotifiyPropertyChanged(string property)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(property));
        }

        internal void AddGroupSummary(String fieldName, SummaryItemType itemType, string displayFormat)
        {
            GroupSummary = new GroupSummary();
            GroupSummary.FieldName = fieldName;
            GroupSummary.SummaryType = itemType;
            GroupSummary.DisplayFormat = displayFormat;
        }
    }
    public class SMMWindow2Settings
    {
        public bool ShowHelpButton { get; set; }

        public bool AddNewButton { get; set; }
        public string HelpText { get; set; }
        /// <summary>
        /// OK en Canle-Buttons tonen indien het scherm als keuzescherm met dienen
        /// </summary>
        public bool IsChoiceWindow { get; set; }

        public List<SMMWindow2Level> Levels;
        public GlobalMethods.Themes theme;
        public int LevelCount
        {
            get
            { return Levels.Count; }
        }

        public String WindowTitle { get; set; }

        public SMMWindow2Settings(String windowTitle, GlobalMethods.Themes theme)
        {
            this.theme = theme;
            this.WindowTitle = windowTitle;
            Levels = new List<SMMWindow2Level>();
            WindowSizePercentage = 100;
        }
        public void AddLevel(SMMWindow2Level level)
        {
            Levels.Add(level);
        }
        public int WindowSizePercentage
        {
            set
            {
                WindowSizeWidth = (int)(double)System.Windows.SystemParameters.PrimaryScreenWidth / 100 * value;
                WindowSizeHeight = (int)(double)System.Windows.SystemParameters.PrimaryScreenHeight / 100 * value;
            }
        }


        public int WindowSizeHeight { get; set; }
        public int WindowSizeWidth { get; set; }
    }
    public enum SMMWindow2ColumnUnboundType
    {
        Bound,
        String,
        Integer,
        Decimal,
        Image,
        DateTime,
        Boolean

    }

    public class SMMWindow2Column
    {

        public bool IsSummary { get; set; }
        public String FieldName { get; set; }
        public String ColumnHeader { get; set; }
        public bool IsEditable { get; set; }
        /// <summary>
        /// if null , the value from IsEditbable is taken
        /// </summary>
        public bool? IsEditableInNewRow { get; set; }
        public bool Visible { get; set; }

        public bool IsNullable { get; set; }

        public delegate String ValidateDelegate(SMMWindow2Column col, Object value, Object vc);

        public ValidateDelegate ValidateMethod { get; set; }
        public bool IsComboBox { get { return _comboboxItems != null && _comboboxItems.Count > 0; } }
        public object DefaultValue { get; set; }
        public SMMWindow2ColumnUnboundType UnboundType { get; set; }
        public delegate Object UnboundGetValueDelegate(Object row);
        public UnboundGetValueDelegate UnboundGetValue { get; set; }

        /// <summary>
        /// Unbound Type == Picture
        /// </summary>
        public double? PictureWidth { get; set; }
        public double? PictureHeight { get; set; }


        private int _level;

        public List<SMMWindow2ComboItem> ComboboxItems
        {
            get
            {
                return _comboboxItems;
            }

            set
            {
                _comboboxItems = value;
            }
        }

        public int Level
        {
            get
            {
                return _level;
            }

            set
            {
                _level = value;
            }
        }
        public bool IsCheckBox
        {
            get; set;

        }
        public bool IsPicture
        {
            get; set;

        }
        
        public BaseEditSettings EditSettings { get; set; }

        public Dictionary<String, String> customColumnDisplayText { get; set; }

        private List<SMMWindow2ComboItem> _comboboxItems;
        public SMMWindow2Column()
        {

            Visible = true;
            IsEditable = true;
            IsNullable = false;
            UnboundType = SMMWindow2ColumnUnboundType.Bound;

        }
    }
    public class SMMWindow2ComboItem
    {
        public object Id { get; set; }
        public String Name { get; set; }
    }

}
