﻿using DevExpress.Mvvm;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClaraFey.CommonObjects
{
    public class NavBarListItem : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        /** PROPERTIES */
        public int Index { get; set; }
        public string GroupName { get; set; }
        public bool BewonerIndependent { get; set; }
        public string ItemName { get; set; }
        private ViewModelBase _viewModelBase;
        public ViewModelBase ViewModelBase
        {
            get
            {
                return _viewModelBase;
            }
            set
            {
                _viewModelBase = value;
                NotifiyPropertyChanged("ViewModelBase");
            }
        }
        public Object Tag { get; set; }
        private bool _isEnabled;
        public bool IsEnabled
        {
            get
            {
                return _isEnabled;
            }

            set
            {
                _isEnabled = value;
                NotifiyPropertyChanged("IsEnabled");
            }
        }
        private bool _isVisible;

        public bool IsVisible
        {
            get
            {
                return _isVisible;
            }

            set
            {
                _isVisible = value;
                NotifiyPropertyChanged("IsVisible");
            }
        }

        /** METHODS */
        public override string ToString()
        {
            return GroupName + " -> " + ItemName;
        }
        private void NotifiyPropertyChanged(string property)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(property));
        }
    }
}
