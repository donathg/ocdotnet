﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevExpress.XtraRichEdit.API.Native;
using sharedcode.DAL;
using sharedcode.WeergaveClasses;

namespace ClaraFey.CommonObjects.WordParsers
{

    public static class WordParserInsertMethods
    {
        public static Document InsertDomicilieAdres(InsertionSettings args)
        {
            List<DomicilieadresWeergave> domicilieList = IdoDAL.SelectDomicilieAdresFromBewonerRijksregisternummer(args.Rijksregisnr);
            Document tempDoc = args.WordDocument;
            DocumentPosition pos = tempDoc.CreatePosition(args.Position);

            foreach (DomicilieadresWeergave weergave in domicilieList)
            {
                tempDoc.InsertText(pos, weergave.Voornaam + " " + weergave.Naam + Environment.NewLine + 
                    "Rijksregisternummer: " + weergave.Rijksregisternummer + Environment.NewLine +
                    "Woonplaats: \t" + Environment.NewLine + 
                    weergave.Straat + " " + weergave.Huisnummer + " bus " + weergave.Busnummer + Environment.NewLine + 
                    weergave.Postcode + " " + weergave.Woonplaats + " " + weergave.Land);
            }

            return tempDoc;
        }
        public static Document InsertOuderAdres(InsertionSettings args)
        {
            List<OuderadresWeergave> domicilieList = IdoDAL.SelectOuderAdresFromBewonerRijksregisternummer(args.Rijksregisnr);
            Document tempDoc = args.WordDocument;
            DocumentPosition pos = tempDoc.CreatePosition(args.Position);

            foreach (OuderadresWeergave weergave in domicilieList)
            {
                tempDoc.InsertText(pos, weergave.Naam + Environment.NewLine + 
                    weergave.Straat + " " + weergave.Huisnummer + " bus " + weergave.Busnummer + Environment.NewLine +
                    weergave.Postcode + " " + weergave.Woonplaats + " " + weergave.Land);
            }

            return tempDoc;
        }
        public static Document InsertKinderbijslagfondsNaam(InsertionSettings args)
        {
            List<KinderbijslagadresWeergave> domicilieList = IdoDAL.SelectKinderbijslagAdresFromBewonerRijksregisternummer(args.Rijksregisnr);
            Document tempDoc = args.WordDocument;
            DocumentPosition pos = tempDoc.CreatePosition(args.Position);

            foreach (KinderbijslagadresWeergave weergave in domicilieList)
            {
                tempDoc.InsertText(pos, weergave.Naam);
            }

            return tempDoc;
        }
        public static Document InsertBewindvoerderAdres(InsertionSettings args)
        {
            List<BewindvoerderadresWeergave> domicilieList = IdoDAL.SelectBewindvoerderAdresFromBewonerRijksregisternummer(args.Rijksregisnr);
            Document tempDoc = args.WordDocument;
            DocumentPosition pos = tempDoc.CreatePosition(args.Position);

            if (domicilieList != null && domicilieList.Count != 0)
            {
                foreach (BewindvoerderadresWeergave weergave in domicilieList)
                {
                    tempDoc.InsertText(pos, Environment.NewLine + "Aangesteld als " + weergave.Type.ToLower() + Environment.NewLine + 
                        weergave.Naam + Environment.NewLine +
                        weergave.Straat + " " + weergave.Huisnummer + " bus " + weergave.Busnummer + Environment.NewLine +
                        weergave.Postcode + " " + weergave.Woonplaats + " " + weergave.Land + Environment.NewLine);
                }
            }
            else
            {
                tempDoc.InsertText(pos, Environment.NewLine + "Geen bewindvoerder" + Environment.NewLine);
            }

            return tempDoc;
        }
        public static Document InsertHoofdfactuurAdresWithEmail(InsertionSettings args)
        {
            List<FacturatieadresWeergave> domicilieList = IdoDAL.SelectFacturatieAdresAndEmailFromBewonerRijksregisternummer(args.Rijksregisnr);
            Document tempDoc = args.WordDocument;

            foreach (FacturatieadresWeergave weergave in domicilieList)
            {
                DocumentPosition pos = tempDoc.CreatePosition(args.Position);
                tempDoc.InsertText(pos, weergave.Naam + Environment.NewLine +
                    "Woonplaats: \t" + Environment.NewLine +
                    weergave.Straat + " " + weergave.Huisnummer + " bus " + weergave.Busnummer + Environment.NewLine +
                    weergave.Postcode + " " + weergave.Woonplaats + " " + weergave.Land + Environment.NewLine +
                    weergave.Email);
            }

            return tempDoc;
        }
        public static Document InsertIBAN(InsertionSettings args)
        {
            List<IbanFacturatieAdresWeergave> domicilieList = IdoDAL.SelectIbanFacturatieadresFromBewonerRijksregisternummer(args.Rijksregisnr);
            Document tempDoc = args.WordDocument;

            foreach (IbanFacturatieAdresWeergave weergave in domicilieList)
            {
                DocumentPosition pos = tempDoc.CreatePosition(args.Position);
                tempDoc.InsertText(pos, weergave.Ibannummer);
            }

            return tempDoc;
        }
        public static Document InsertBWaarde(InsertionSettings args)
        {
            List<BWaardeWeergave> domicilieList = IdoDAL.SelectBWaardeFromBewonerRijksregisternummer(args.Rijksregisnr);
            Document tempDoc = args.WordDocument;

            foreach (BWaardeWeergave weergave in domicilieList)
            {
                DocumentPosition pos = tempDoc.CreatePosition(args.Position);
                tempDoc.InsertText(pos, weergave.BWaarde);
            }

            return tempDoc;
        }
        public static Document InsertPWaarde(InsertionSettings args)
        {
            List<PWaardeWeergave> domicilieList = IdoDAL.SelectPWaardeFromBewonerRijksregisternummer(args.Rijksregisnr);
            Document tempDoc = args.WordDocument;

            foreach (PWaardeWeergave weergave in domicilieList)
            {
                DocumentPosition pos = tempDoc.CreatePosition(args.Position);
                tempDoc.InsertText(pos, weergave.PWaarde);
            }

            return tempDoc;
        }
        public static Document InsertPunterPerJaar(InsertionSettings args)
        {
            List<PunterPerJaarWeergave> domicilieList = IdoDAL.SelectPuntenPerJaarFromBewonerRijksregisternummer(args.Rijksregisnr);
            Document tempDoc = args.WordDocument;

            foreach (PunterPerJaarWeergave weergave in domicilieList)
            {
                DocumentPosition pos = tempDoc.CreatePosition(args.Position);
                tempDoc.InsertText(pos, weergave.LijstPuntenEnPeriode);
            }

            return tempDoc;
        }
        public static Document InsertToestemmingen(InsertionSettings args) // NOG DOEN
        {
            throw new Exception("Nog niet geimplementeerd");
            List<KinderbijslagadresWeergave> domicilieList = IdoDAL.SelectKinderbijslagAdresFromBewonerRijksregisternummer(args.Rijksregisnr);
            Document tempDoc = args.WordDocument;

            foreach (KinderbijslagadresWeergave weergave in domicilieList)
            {
                DocumentPosition pos = tempDoc.CreatePosition(args.Position);
                tempDoc.InsertText(pos, weergave.Naam);
            }

            return tempDoc;
        }
    }
}
