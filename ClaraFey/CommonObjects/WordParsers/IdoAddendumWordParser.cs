﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevExpress.XtraRichEdit.API.Native;

namespace ClaraFey.CommonObjects.WordParsers
{
    public class IdoAddendumWordParser : IWordParser
    {
        // FIELDS
        public Dictionary<int, Func<InsertionSettings, Document>> Functions { get; set; }
        public string WordDocumentLocation { get; set; }
        public Document WordDocument { get; set; }
        public string WindowTitle
        {
            get
            {
                return "Wordparser voor IDO-addendums";
            }
        }

        // CONSTRUCTORS
        public IdoAddendumWordParser()
        {
            Functions = new Dictionary<int, Func<InsertionSettings, Document>>()
            {
                { 5632, WordParserInsertMethods.InsertPunterPerJaar },
                { 3124, WordParserInsertMethods.InsertPWaarde },
                { 3112, WordParserInsertMethods.InsertBWaarde },
                { 1337, WordParserInsertMethods.InsertBewindvoerderAdres },
                { 805, WordParserInsertMethods.InsertDomicilieAdres }
            };
        }

        // METHODS
        public void DoAllInserts(string rijksregisternr)
        {
            foreach (KeyValuePair<int, Func<InsertionSettings, Document>> pair in Functions)
            {
                InsertionSettings args = new InsertionSettings
                {
                    Position = pair.Key,
                    WordDocument = WordDocument,
                    Rijksregisnr = rijksregisternr
                };

                WordDocument = pair.Value(args);
            }
        }
    }
}
