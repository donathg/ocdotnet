﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevExpress.XtraRichEdit.API.Native;

namespace ClaraFey.CommonObjects.WordParsers
{
    public interface IWordParser
    {
        Dictionary<int, Func<InsertionSettings, Document>> Functions { get; set; }
        string WordDocumentLocation { get; set; }
        Document WordDocument { get; set; }
        string WindowTitle { get; }

        void DoAllInserts(string rijksregisternr);
    }
    public class InsertionSettings
    {
        public int Position { get; set; }
        public string Rijksregisnr { get; set; }
        public Document WordDocument { get; set; }
    }
}
