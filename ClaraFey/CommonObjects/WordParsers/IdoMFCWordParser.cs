﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevExpress.XtraRichEdit.API.Native;

namespace ClaraFey.CommonObjects.WordParsers
{
    public class IdoMFCWordParser : IWordParser
    {
        // FIELDS
        public Dictionary<int, Func<InsertionSettings, Document>> Functions { get; set; }
        public string WordDocumentLocation { get; set; }
        public Document WordDocument { get; set; }
        public string WindowTitle
        {
            get
            {
                return "Wordparser voor IDO's van MFC";
            }
        }

        // CONSTRUCTORS
        public IdoMFCWordParser()
        {
            Functions = new Dictionary<int, Func<InsertionSettings, Document>>()
            {
                { 13607, WordParserInsertMethods.InsertIBAN },
                { 13488, WordParserInsertMethods.InsertHoofdfactuurAdresWithEmail },
                { 10480, WordParserInsertMethods.InsertKinderbijslagfondsNaam },
                { 879, WordParserInsertMethods.InsertOuderAdres },
                { 619, WordParserInsertMethods.InsertDomicilieAdres }
                /*,
                { 21124, WordParserInsertMethods.InsertToestemmingen }*/
            };
        }

        // METHODS
        public void DoAllInserts(string rijksregisternr)
        {
            foreach (KeyValuePair<int, Func<InsertionSettings, Document>> pair in Functions)
            {
                InsertionSettings args = new InsertionSettings
                {
                    Position = pair.Key,
                    WordDocument = WordDocument,
                    Rijksregisnr = rijksregisternr
                };

                WordDocument = pair.Value(args);
            }
        }
    }
}
