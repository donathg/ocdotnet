﻿using DevExpress.XtraReports.UI;
using sharedcode.BLL;
using sharedcode.common;
using sharedcode.DAL;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClaraFey.Reports.PrintModels;
namespace ClaraFey.ViewControllers.Aankoop.IM
{
    public class AfsluitDatumInfo 
    {

        
        public DateTime? Datum { get; set; }
        public String DisplayText { get; set; }
    }


    public class AankoopIMCSVItem
    {

       
            public string Naam { get; set; }
            public string Rijksregisternummer { get; set; }

            public string Ondersteuningsvorms { get; set; }

            public DateTime Datum { get; set; }
            public string DatumString
            {
                get
                {

                    return Datum.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                }
            }
            public string FinancielbewegingsCode
            {
                get
                {
                     

                    return Ondersteuningsvorms == "MFC" ? "PAM" : "PAP";


                }
            }
            public String Omschrijving { get; set; }
            public int Eenheden { get; set; } = 1;
            public double Eenheidsprijs { get; set; }
            public double Totaal { get; set; }


        


    }

    public class AankoopIMAfsluitVM : MyNotifyPropertyChanged
    {

        public bool CanExportToExcel { get { return SelectedAfsluitDatum.HasValue; } }
        public bool CanClose { get { return !SelectedAfsluitDatum.HasValue; } }
        public  List<AankoopIM> AankoopItems { get; set; } = new List<AankoopIM>();
       public List<AfsluitDatumInfo> AfsluitDatums { get; set; } = new List<AfsluitDatumInfo>();


        private DateTime? _selectedAfsluitDatum { get; set; }
        public DateTime? SelectedAfsluitDatum 
        { 
            get { 
                return _selectedAfsluitDatum; 
            } 
            set { 
                _selectedAfsluitDatum = value; 
                LoadItems(value); 
                NotifiyPropertyChanged(nameof(CanExportToExcel));
                NotifiyPropertyChanged(nameof(CanClose));
                NotifiyPropertyChanged(nameof(SelectedAfsluitDatum));
            } 
        }
        public AankoopIMAfsluitVM()
        {
            FillAfsluitDatums();
        }

        public List<AankoopIMCSVItem> GetCSV()
        {
            List<AankoopIMCSVItem> list = new List<AankoopIMCSVItem>();
            DateTime dtPrevMonth = DateTime.Now.AddMonths(-1);
            foreach (AankoopIM a in AankoopItems)
            {
                AankoopIMCSVItem csv = new AankoopIMCSVItem();
                list.Add(csv);
               
                csv.Rijksregisternummer = a.ClientRijksregisternummer;
                csv.Ondersteuningsvorms = a.ClientOndersteuningsvorm;
                csv.Datum = new DateTime(dtPrevMonth.Year, dtPrevMonth.Month, DateTime.DaysInMonth(dtPrevMonth.Year, dtPrevMonth.Month));
                csv.Omschrijving = a.ArtikelNaam;  
                csv.Eenheden = a.Aantal;
                csv.Eenheidsprijs = Math.Round(a.ArtikelPrijs, 2, MidpointRounding.AwayFromZero);
                csv.Totaal = Math.Round(a.Aankoopbedrag, 2, MidpointRounding.AwayFromZero);
            }
            return list;


        }

        public void LoadItems (DateTime? afsluitDatum)
        {
            AankoopItems = AankoopIM_DAL.GetAankoopIM(afsluitDatum);
            NotifiyPropertyChanged(nameof(AankoopItems));
        }
        public void FillAfsluitDatums()
        {
            List<DateTime> afsluitDatums =  AankoopIM_DAL.GetAfsluitDatums();
            AfsluitDatums.Clear();
            AfsluitDatums.Add(new AfsluitDatumInfo() {  Datum = null, DisplayText = "open bestellingen" }) ;

            foreach (DateTime dt in afsluitDatums)
                AfsluitDatums.Add(new AfsluitDatumInfo() { Datum = dt, DisplayText = dt.ToString() });
            
            SelectedAfsluitDatum = null;
            NotifiyPropertyChanged(nameof(AfsluitDatums));

        }

        internal void Aflsuiten()
        {
            

            AankoopIM_DAL.Afsluiten(GlobalData.Instance.LoggedOnUser.Id.Value);
            FillAfsluitDatums();
        }

        internal void PrintBestelBon()
        {
         
            Reports.AankoopIMBestelbon report = new Reports.AankoopIMBestelbon(AankoopItems);

           

           using (ReportPrintTool printTool = new ReportPrintTool(report))
             {
                printTool.ShowRibbonPreviewDialog();
             }
        } 
        internal void PrintAfrekening()
        {
         
            Reports.AankoopIMAfrekeningClient report = new Reports.AankoopIMAfrekeningClient(AankoopItems);

           

           using (ReportPrintTool printTool = new ReportPrintTool(report))
             {
                printTool.ShowRibbonPreviewDialog();
             }
        }

        internal void PrintRapportLeefgroep()
        {
            Reports.AankoopIMAfrekeningEntiteit report = new Reports.AankoopIMAfrekeningEntiteit(AankoopItems);



            using (ReportPrintTool printTool = new ReportPrintTool(report))
            {
                printTool.ShowRibbonPreviewDialog();
            }
        }
    }
}
