﻿using ClaraFey.ViewControllers.ClientDossier;
using ClaraFey.ViewControllers.Common;
using sharedcode.BLL;
using sharedcode.Caches;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClaraFey.ViewControllers.UserControls
{
    public class AttachementVC : INotifyPropertyChanged, IMyUserControl
    {
        /** FIELDS */
        public event PropertyChangedEventHandler PropertyChanged;

        private int _NumberOfAttachments;
        public int NumberOfAttachments
        {
            get
            {
                return _NumberOfAttachments;
            }
            set
            {
                _NumberOfAttachments = value;

                NotifiyPropertyChanged(nameof(NumberOfAttachments));
                NotifiyPropertyChanged(nameof(ButtonContent));
            }
        }
        public int BewonerID { get; set; }
        public string Category { get; set; }
        public bool NeedSave { get; set; }
        public string ButtonContent
        {
            get
            {
                return NumberOfAttachments.ToString();
            }
        }
        public string Title { get; set; }
        public List<IMyUserControl> ChildrenUC { get; set; }

        /** METHODS */
        public void NotifiyPropertyChanged(string property)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(property));
        }
        public void RegisterChildUCs(IMyUserControl uc)
        {

        }
        public void Save()
        {

        }
        public void Load(int? id)
        {
            BewonerID = id.GetValueOrDefault();
            int.TryParse(this.Category, out int labelId);
            NumberOfAttachments = BewonersListCache.Instance.GetNumberOfAttachements(id.GetValueOrDefault(), labelId);
        }
    }
}

