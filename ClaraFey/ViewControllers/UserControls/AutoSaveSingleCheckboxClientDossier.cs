﻿using ClaraFey.ViewControllers.ClientDossier;
using ClaraFey.ViewControllers.Common;
using sharedcode.BLL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClaraFey.ViewControllers.UserControls
{

    
    public class AutoSaveSingleCheckboxClientDossier : INotifyPropertyChanged,IMyUserControl
    {

        private bool _isChecked=false;
    

        public int BewonerID { get; set; }
        public bool NeedSave { get; set; }
        public AutoSaveSingleCheckboxClientDossier()
        {

        }
        public string Title { get; set; }

        public string SubTitle { get; set; }

        public List<IMyUserControl> ChildrenUC
        {
            get;set;
        }

        public bool IsChecked
        {
            get { return _isChecked; }
            set
            {
                _isChecked = value;
                if (_isChecked  != value)
                    NeedSave = true;
                NotifiyPropertyChanged("IsChecked");
            }
        }

        void NotifiyPropertyChanged(string property)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(property));
            }
        }

        public void Load(int? id)
        {
            BewonerID = id.GetValueOrDefault();

           
        }

        public void Save()
        {
            if (NeedSave)
            {
                NeedSave = false;

            }

        }

        public void RegisterChildUCs(IMyUserControl uc)
        {
             
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
