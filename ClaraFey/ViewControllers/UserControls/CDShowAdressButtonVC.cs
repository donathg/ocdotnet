﻿using ClaraFey.ViewControllers.Common;
using sharedcode.BLL;
using sharedcode.Caches;
using sharedcode.common;
using sharedcode.WeergaveClasses;
using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace ClaraFey.ViewControllers.UserControls
{
    public class CDShowAdressButtonVC : MyNotifyPropertyChanged, IMyUserControl
    {
        private AdresBLLProvider _manager;

        public int BewonerID { get; set; }
        public List<adrestype> AdresTypeList { get; set; }
        public List<AdresWeergave> AdresList { get; set; }
        public bool NeedSave { get; set; }
        public List<IMyUserControl> ChildrenUC { get; set; }
        public string Title { get; set; }

        public CDShowAdressButtonVC()
        {
            _manager = new BewonerAdresBLLProvider(BewonersListCache.Instance.GetBewoner(BewonerID));

            AdresList = new List<AdresWeergave>();
            AdresTypeList = new List<adrestype>();
        }
        
        public void InitList(String adresTypeIds)
        {
            foreach (String item in adresTypeIds.Split(' '))
            {
                AdresTypeList.Add(new adrestype() { id = Int32.Parse(item) });
            }
        }
        public void Load(int? id)
        {
            this.BewonerID = id.GetValueOrDefault();
            
          /*  foreach (adrestype item in AdresTypeList)
            {
                if (item != null)
                {
                    foreach (AdresWeergave data in ((BewonerAdresBLLProvider)_manager).GetAllAdressenFromType(item))
                    {
                        AdresList.Add(data);
                    }
                }
            }*/
            NotifiyPropertyChanged(nameof(AdresList));
        }
        public void Save()
        {
           
        }
        public void RegisterChildUCs(IMyUserControl uc)
        {

        }
    }
}
