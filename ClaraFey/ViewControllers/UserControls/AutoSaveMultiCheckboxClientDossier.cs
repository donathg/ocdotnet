﻿using ClaraFey.ViewControllers.ClientDossier;
using sharedcode.BLL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevExpress.Data;
using ClaraFey.ViewControllers.Common;

namespace ClaraFey.ViewControllers.UserControls
{

    public class AutoSaveMultiCheckboxItemClientDossier : cd_lists_detail , INotifyPropertyChanged
    {       
        private bool _isChecked;
        public bool TextColumnReadOnly
        {
            get
            {
                if (!CheckBoxMode)
                    return false;
                if (!this.canaddtext)
                    return true;
                if (!IsChecked)
                    return true;
                return false;
            }
        }

        public bool CheckBoxMode { get; set; }

        public  bool IsStringColumnMandatory { get; set; }
        public bool IsDecimalColumnMandatory { get; set; }
        public bool IsDateColumnMandatory { get; set; }

 
       
        public bool DateColumnReadOnly
        {
            get
            {
                if (!CheckBoxMode)
                    return false;

                if (!this.canadddate)
                    return true;
                if (!IsChecked)
                    return true;
                return false;
            }
        }

        public String StringColumnColor
        {
            get
            {
                if (!CheckBoxMode)
                    return "white";
                if (!this.canaddtext)
                    return "white";
                if (!IsChecked)
                    return "white";

                if (String.IsNullOrWhiteSpace(ColumnString) && IsStringColumnMandatory)
                    return "red";
                else return "green";
            }
        }


        public String DateColumnColor
        {
            get
            {
                if (!CheckBoxMode)
                    return "white";
                if (!this.canadddate)
                    return "white";
                if (!IsChecked)
                    return "white";

                if (ColumnDate.HasValue == false && IsDateColumnMandatory)
                    return "red";
                else return "green";
            }
        }
        public String DecimalColumnColor
        {
            get
            {
                if (!CheckBoxMode)
                    return "white";
                if (!this.canadddecimal)
                    return "white";
                if (!IsChecked)
                    return "white";

                if (ColumnDecimal.HasValue == false && IsDecimalColumnMandatory)
                    return "red";
                else return "green";
            }
        }

        public bool DecimalColumnReadOnly
        {
            get
            {
                if (!CheckBoxMode)
                    return false;
                if (!this.canadddecimal)
                    return true;
                if (!IsChecked)
                    return true;
                return false;
            }

        }

        public string _columnString;

        private DateTime? _columnDate;
        public DateTime? ColumnDate
        {
            get { return _columnDate; }
            set
            {
                _columnDate = value;
                NotifiyPropertyChanged("DateColumnColor");

            }
        }


        private Decimal? _columnDecimal;
 

        public bool IsChecked
        {
            get
            {
                
                return _isChecked;
            }
            set
            {
                _isChecked = value;
                if (value == false)
                {
                    ColumnString = null;
                    ColumnDate = null;
                    ColumnDecimal = null;
                }

                
                NotifiyPropertyChanged("TextColumnReadOnly");
                NotifiyPropertyChanged("DateColumnReadOnly");
                NotifiyPropertyChanged("DecimalColumnReadOnly");
                NotifiyPropertyChanged("ColumnString");
                NotifiyPropertyChanged("ColumnDate");
                NotifiyPropertyChanged("ColumnDecimal");
                NotifiyPropertyChanged("DateColumnColor");
                NotifiyPropertyChanged("DecimalColumnColor");
                NotifiyPropertyChanged("StringColumnColor");
            }
        }

        public decimal? ColumnDecimal
        {
            get { return _columnDecimal; }
            set
            {
                _columnDecimal = value;


                NotifiyPropertyChanged("DecimalColumnColor");

            }
        }

        public string ColumnString
        {
            get { return _columnString; }
            set
            {
                _columnString = value;

                NotifiyPropertyChanged("StringColumnColor");

            }
        }


        void NotifiyPropertyChanged(string property)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(property));
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;
    }



    public class AutoSaveMultiCheckboxClientDossierGroup : INotifyPropertyChanged, IMyUserControl
    {
        public List<IMyUserControl> ChildrenUC
        {
            get; set;

        } = new List<IMyUserControl>();

               
        public event PropertyChangedEventHandler PropertyChanged;
        void NotifiyPropertyChanged(string property)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(property));
        }

        public string Title { get; set; }
       
        public bool NeedSave { get; set; }

        public void Load(int? id)
        {

            List<int> ids = new List<int>();

            foreach (IMyUserControl u in ChildrenUC)
            {
                ids.Add(((AutoSaveMultiCheckboxClientDossier)u).ListHeaderID);
            }

                List<cd_lists_detail> list = cd_lists.GetListItems(ids);
                List<cd_lists_results> listResults = cd_lists.GetResultItems(ids, id.GetValueOrDefault());
                List<cd_lists_header> headers = cd_lists.GetHeaders(ids);

            foreach (IMyUserControl u in ChildrenUC)
            {
                AutoSaveMultiCheckboxClientDossier a = (AutoSaveMultiCheckboxClientDossier)u;
                a.LoadBulk(id, headers.Where(x=>x.id == a.ListHeaderID).FirstOrDefault() , list.Where(x=>x.headerid == a.ListHeaderID ).OrderBy(x=>x.orderNum).ToList(), listResults.Where(x => x.headerid == a.ListHeaderID).ToList());
            }
        }

        public void RegisterChildUCs(IMyUserControl uc)
        {
            ChildrenUC.Add(uc);
        }

        public void Save()
        {
            List<CdListSaveResults> listResults = new List<CdListSaveResults>();
            foreach (IMyUserControl u in ChildrenUC)
            {
                AutoSaveMultiCheckboxClientDossier a = (AutoSaveMultiCheckboxClientDossier)u;
                listResults.Add(a.GetSaveResult());
            
            }
            cd_lists.SaveResults(listResults);
        }
   
         
    }

    public class AutoSaveMultiCheckboxClientDossier : INotifyPropertyChanged,IMyUserControl
    {
        public int ListHeaderID { get; set; }
        public int BewonerID { get; set; }
        public bool NeedSave { get; set; }
        public bool IsColumnDateVisible { get; set; }
        public bool IsColumnStringVisible { get; set; }
        public bool IsColumnDecimalVisible { get; set; }
        public bool CheckBoxMode { get; set; }
        public bool IsColumnCheckboxVisible { get; set; }

        public AutoSaveMultiCheckboxClientDossier()
        {
        }
        public void LoadList(int listHeaderId)
        {
            this.ListHeaderID = listHeaderId;
        }

        public List<AutoSaveMultiCheckboxItemClientDossier> ItemList
        {
            get;
            set;
        }

        public string Title { get; set; }

        public string SubTitle { get; set; }

        public List<IMyUserControl> ChildrenUC
        {
            get;set;
        }

        void NotifiyPropertyChanged(string property)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(property));
        }


        private void Init(int? id, cd_lists_header header, List<cd_lists_detail> list, List<cd_lists_results> listResults)
        {
            BewonerID = id.GetValueOrDefault();
            CheckBoxMode = header.showcheckbox;
            this.Title = header.label;
            this.SubTitle = header.sublabel;



            ItemList = new List<AutoSaveMultiCheckboxItemClientDossier>();
            IsColumnDateVisible = false;
            IsColumnStringVisible = false;
            IsColumnDecimalVisible = false;
            IsColumnCheckboxVisible = header.showcheckbox;

            foreach (cd_lists_detail i in list)
            {
                AutoSaveMultiCheckboxItemClientDossier newitem = new AutoSaveMultiCheckboxItemClientDossier();

                newitem.id = i.id;
                newitem.canaddtext = i.canaddtext;
                newitem.canadddate = i.canadddate;
                newitem.canadddecimal = i.canadddecimal;
                newitem.headerid = i.headerid;
                newitem.value = i.value;
                newitem.name = i.name;
                newitem.ColumnString = String.Empty;
                newitem.ColumnDate = null;
                newitem.ColumnDecimal = null;
                newitem.IsChecked = !header.showcheckbox;
                newitem.CheckBoxMode = CheckBoxMode;
                newitem.decimalColumnMask = i.decimalColumnMask;
                if (CheckBoxMode)
                {
                    newitem.IsDateColumnMandatory = i.isdatemandatory;
                    newitem.IsDecimalColumnMandatory = i.isdecimalmandatory;
                    newitem.IsStringColumnMandatory = i.istextmandatory;
                }
                else
                {
                    newitem.IsDateColumnMandatory = false;
                    newitem.IsDecimalColumnMandatory = false;
                    newitem.IsStringColumnMandatory = false;

                }

                if (i.canadddate)
                    IsColumnDateVisible = true;

                if (i.canaddtext)
                    IsColumnStringVisible = true;

                if (i.canadddecimal)
                    IsColumnDecimalVisible = true;

                foreach (cd_lists_results result in listResults)
                {
                    if (i.id == result.detailid)
                    {
                        newitem.IsChecked = true;
                        newitem.ColumnString = result.columnstring;
                        newitem.ColumnDecimal = result.columndecimal;
                        newitem.ColumnDate = result.columndate;
                        continue;
                    }
                }
                ItemList.Add(newitem);
            }
            NotifiyPropertyChanged("ItemList");
            NotifiyPropertyChanged("Title");
            NotifiyPropertyChanged("SubTitle");
            NotifiyPropertyChanged("IsColumnDateVisible");
            NotifiyPropertyChanged("IsColumnStringVisible");
            NotifiyPropertyChanged("IsColumnDecimalVisible");
            NotifiyPropertyChanged("IsColumnCheckboxVisible");

        }

        public void LoadBulk(int? id, cd_lists_header header,  List<cd_lists_detail> list, List<cd_lists_results> listResults)
        {
            Init(id, header, list, listResults);
        }

        public void Load(int? id)
        {
            cd_lists_header header = cd_lists.GetHeader(this.ListHeaderID);
            List<cd_lists_detail> list = cd_lists.GetListItems(this.ListHeaderID);
            List<cd_lists_results> listResults = cd_lists.GetResultItems(this.ListHeaderID, this.BewonerID);
            Init(id, header, list, listResults);
            /*
            BewonerID = id.GetValueOrDefault();
        
            CheckBoxMode = header.showcheckbox;

            this.Title = header.label;
            this.SubTitle = header.sublabel;

          

            ItemList = new List<AutoSaveMultiCheckboxItemClientDossier>();
            IsColumnDateVisible = false;
            IsColumnStringVisible = false;
            IsColumnDecimalVisible = false;
            IsColumnCheckboxVisible = header.showcheckbox;

            foreach (cd_lists_detail i in list)
            {
                AutoSaveMultiCheckboxItemClientDossier newitem = new AutoSaveMultiCheckboxItemClientDossier();

                newitem.id = i.id;
                newitem.canaddtext = i.canaddtext;
                newitem.canadddate = i.canadddate;
                newitem.canadddecimal = i.canadddecimal;
                newitem.headerid = i.headerid;
                newitem.value = i.value;
                newitem.name = i.name;
                newitem.ColumnString = String.Empty;
                newitem.ColumnDate = null;
                newitem.ColumnDecimal = null;
                newitem.IsChecked = !header.showcheckbox;
                newitem.CheckBoxMode = CheckBoxMode;
                newitem.decimalColumnType = i.decimalColumnType;
                if (CheckBoxMode)
                {
                    newitem.IsDateColumnMandatory = i.isdatemandatory;
                    newitem.IsDecimalColumnMandatory = i.isdecimalmandatory;
                    newitem.IsStringColumnMandatory = i.istextmandatory;
                }
                else
                {
                    newitem.IsDateColumnMandatory = false;
                    newitem.IsDecimalColumnMandatory = false;
                    newitem.IsStringColumnMandatory = false;

                }

                if (i.canadddate )
                    IsColumnDateVisible = true;

                if (i.canaddtext)
                    IsColumnStringVisible = true;

                if (i.canadddecimal)
                    IsColumnDecimalVisible = true;

                foreach (cd_lists_results result in listResults)
                {
                    if (i.id == result.detailid)
                    {
                        newitem.IsChecked = true;
                        newitem.ColumnString = result.columnstring;
                        newitem.ColumnDecimal = result.columndecimal;
                        newitem.ColumnDate = result.columndate;
                        continue;
                    }
                }
                ItemList.Add(newitem);
            }
            NotifiyPropertyChanged("ItemList");
            NotifiyPropertyChanged("Title");
            NotifiyPropertyChanged("SubTitle");
            NotifiyPropertyChanged("IsColumnDateVisible");
            NotifiyPropertyChanged("IsColumnStringVisible");
            NotifiyPropertyChanged("IsColumnDecimalVisible");
            NotifiyPropertyChanged("IsColumnCheckboxVisible");*/
        }

        public CdListSaveResults GetSaveResult()
        {
            List<int> detailIds = new List<int>();
            List<String> textColumn = new List<string>();
            List<DateTime?> dateColumn = new List<DateTime?>();
            List<Decimal?> decimalColumn = new List<Decimal?>();
            foreach (AutoSaveMultiCheckboxItemClientDossier item in this.ItemList)
            {
                if (item.IsChecked)
                {
                    if (item.canadddate && item.ColumnDate.HasValue == false && item.IsDateColumnMandatory)
                        throw new Exception(this.Title + ". Gelieve een datum in te vullen.");
                    if (item.canadddecimal && item.ColumnDecimal.HasValue == false && item.IsDecimalColumnMandatory)
                        throw new Exception(this.Title + ". Gelieve een waarde/bedrag in te vullen.");
                    if (item.canaddtext && String.IsNullOrWhiteSpace(item.ColumnString) && item.IsStringColumnMandatory)
                        throw new Exception(this.Title + ". Gelieve bijhorende informatie in te vullen.");
                    if (!item.canadddate)
                        item.ColumnDate = null;
                    detailIds.Add(item.id);
                    textColumn.Add(item.ColumnString);
                    dateColumn.Add(item.ColumnDate);
                    decimalColumn.Add(item.ColumnDecimal);
                }
            }


            CdListSaveResults sr = new CdListSaveResults()
            {
                bewonerid = this.BewonerID,
                dateColumnVals = dateColumn,
                decimalColumnVals = decimalColumn,
                detailIds = detailIds,
                listHeaderId = this.ListHeaderID,
                textColumnVals = textColumn
            };

            return sr;

        }
     
          
        public void Save()
        {
            CdListSaveResults sr = GetSaveResult();
            List<CdListSaveResults> list = new List<CdListSaveResults>();
            list.Add(sr);
            cd_lists.SaveResults(list);
        }
        public void RegisterChildUCs(IMyUserControl uc)
        {
        }
        public event PropertyChangedEventHandler PropertyChanged;
    }
}
