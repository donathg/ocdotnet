﻿using ClaraFey.ViewControllers.ClientDossier;
using ClaraFey.ViewControllers.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClaraFey.ViewControllers.UserControls
{
    public class BeeldVormingTextVC  : INotifyPropertyChanged, IMyUserControl
    {

        public int BewonerID { get; set; }
    public String Category { get; set; }

    private int _NumberOfAttachments;

    public BeeldVormingTextVC()
    {

    }
        public bool NeedSave { get; set; }
        public String ButtonContent
    {

        get
        {
            return NumberOfAttachments + "";

        }
    }

    public void LoadList(String Category)
    {
        this.Category = Category;
    }

    public string Title { get; set; }
    public List<IMyUserControl> ChildrenUC
    {
        get; set;
    }

    public int NumberOfAttachments
    {
        get
        {
            return _NumberOfAttachments;
        }

        set
        {
            _NumberOfAttachments = value;
            NotifiyPropertyChanged("NumberOfAttachments");

            NotifiyPropertyChanged("ButtonContent");
        }
    }

    void NotifiyPropertyChanged(string property)
    {
        if (PropertyChanged != null)
        {
            PropertyChanged(this, new PropertyChangedEventArgs(property));
        }
    }

    public void Load(int? id)
    {
        this.BewonerID = id.GetValueOrDefault();
        
    }
    public void UpdateLabelTitle(String label)
    {
        this.Title = label;
        NotifiyPropertyChanged("Title");
    }


    public void Save()
    {


    }

    public void RegisterChildUCs(IMyUserControl uc)
    {

    }

    public event PropertyChangedEventHandler PropertyChanged;
}
}



