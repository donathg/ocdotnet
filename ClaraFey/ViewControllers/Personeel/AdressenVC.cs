﻿using System;
using DevExpress.Mvvm;
using System.ComponentModel;
using System.Collections.Generic;
using sharedcode.BLL;
using ClaraFey.CommonObjects;
using ClaraFey.Windows.Common;
using System.Windows;
using System.Linq;
using System.Collections.ObjectModel;
using sharedcode.WeergaveClasses;
using ClaraFey.ViewControllers.Common;

namespace ClaraFey.ViewControllers.Personeel
{
    public class AdressenVC : ViewModelBase, IDisposable, INotifyPropertyChanged, IMyUserControl
    {
        /** PROPERTIES */
        public new event PropertyChangedEventHandler PropertyChanged;
        
        private BewonerAdresBLLProvider _bll;
        
        public gebruiker Gebruiker;
        public ObservableCollection<AdresWeergave> AdresList { get; set; }
        private AdresWeergave _SelectedAdres;
        public AdresWeergave SelectedAdres
        {
            get { return _SelectedAdres; }
            set
            {
                _SelectedAdres = value;
                NotifiyPropertyChanged(nameof(SelectedAdres));
                NotifiyPropertyChanged(nameof(CanDelete));
            }
        }
        public bool CanDelete
        {
            get { return SelectedAdres != null; }
        }
        
        /** CONSTRUCTORS */
        public AdressenVC(gebruiker selectedGebruiker)
        {
        /*    _bll = new BewonerAdresBLLProvider(selectedBewoner);
            Bewoner = selectedBewoner;
            AdresList = new ObservableCollection<AdresWeergave>();

            if (Bewoner != null)
                FillContactpersonenadresList();*/
        }
        
        /** METHODS */
        private void NotifiyPropertyChanged(string property)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(property));
        }
        public void FillContactpersonenadresList()
        {
            AdresList = new ObservableCollection<AdresWeergave>(_bll.GetAdressenFromSetBewoner());
            NotifiyPropertyChanged(nameof(AdresList));
            NotifiyPropertyChanged(nameof(CanDelete));
        }
        public void DeleteSelectedAdres()
        {
            _bll.DeleteAdresAndContactpersonen(SelectedAdres);
            FillContactpersonenadresList();
        }
        /// <summary>show selection popup for adressubtype</summary>
        /// <returns>selected adressubtype or an empty object if nothing is selected</returns>
        public adrestype SelectAdrestypePopup(AdresWeergave adres)
        {
            SMMWindow2Settings settings = new SMMWindow2Settings("Adrescontext?", GlobalMethods.Themes.clientdossier) { IsChoiceWindow = true };

            SMMWindow2Level level0 = new SMMWindow2Level(0, "Adrescontext?")
            {
                AllowDelete = false,
                AllowModify = false,
                AllowAdd = false,
                AllowSearch = true,
                AllowFilter = true
            };
            level0.AddColumns(new SMMWindow2Column() { FieldName = "naam", ColumnHeader = "context", IsEditable = false },
                              new SMMWindow2Column() { FieldName = "isBedrijf", ColumnHeader = "Is Bedrijf?", IsEditable = false }
            );
            if (adres == null)
                level0.DataSource = _bll.GetAllHoofdAdrestypes();
            else
                level0.DataSource = _bll.GetAllSubAdrestypes();
            settings.AddLevel(level0);

            smmWindowVC vc = new smmWindowVC();
            SMMWindow2 w = new SMMWindow2(vc, settings) {
                WindowStartupLocation = WindowStartupLocation.CenterScreen
            };

            if (w.ShowDialog().GetValueOrDefault())
                return (adrestype)vc.SelectedItem0;

            return null;
        }
        /// <summary>show selection popup for existing contactpersonen of a certain adressubtype</summary>
        /// <returns>selected contactpersoon or an empty object if nothing is selected</returns>
        public AdresWeergave SelectExistingAdresFromAdrestypePopup(adrestype adrestype)
        {
            List<AdresWeergave> adressen;
            if (adrestype.isBedrijf)
                adressen = _bll.GetAllAdressenFromType(adrestype);
            else
                adressen = _bll.GetAllPersoonAdressen();

            if (adressen.Count != 0)
            {
                SMMWindow2Settings settings = new SMMWindow2Settings("bestaande lijst contactpersonen", GlobalMethods.Themes.clientdossier)
                {
                    IsChoiceWindow = true,
                    AddNewButton = true
                };

                SMMWindow2Level level0 = new SMMWindow2Level(0, "adressen")
                {
                    AllowDelete = false,
                    AllowModify = false,
                    AllowAdd = false,
                    AllowSearch = true,
                    AllowFilter = true
                };
                level0.AddColumns(new SMMWindow2Column() { FieldName = "Fullname", ColumnHeader = "naam", IsEditable = false });
                level0.DataSource = adressen;
                settings.AddLevel(level0);

                smmWindowVC vc = new smmWindowVC();
                SMMWindow2 w = new SMMWindow2(vc, settings)
                {
                    WindowStartupLocation = WindowStartupLocation.CenterScreen
                };
                if (w.ShowDialog().GetValueOrDefault())
                {
                    if (vc.SelectedItem0 is AdresWeergave selectedWeergave)
                    {
                        if (selectedWeergave.Type == null)
                            selectedWeergave.Type = adrestype;
                        return selectedWeergave;
                    }
                    else
                    {
                        AdresWeergave adres = new AdresWeergave()
                        {
                            Type = adrestype,
                            OwnerId = Gebruiker.id,
                            Contactpersonen = new ObservableCollection<ContactpersoonWeergave>(new List<ContactpersoonWeergave>()),
                            active = true,
                            landcode = "BE"
                        };
                        if (adrestype.isBedrijf)
                            adres.adrestype_id = adrestype.id;

                        return adres;
                    }
                }
            }
            return null;
        }

        /** INHERITED PARTS */
        public string Title { get; set; }
        public List<IMyUserControl> ChildrenUC { get; set; }
        public bool NeedSave { get; set; }
        public void Dispose()
        {
        }
        public void RegisterChildUCs(IMyUserControl uc)
        {
        }
        public void Save()
        {
        }
        public void Load(int? id)
        {
        }
    }
}