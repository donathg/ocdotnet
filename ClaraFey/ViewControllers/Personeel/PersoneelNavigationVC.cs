﻿using ClaraFey.CommonObjects;
using DevExpress.Mvvm;
using DevExpress.Xpf.NavBar;
using sharedcode.BLL;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using sharedcode.common;
using sharedcode.CommonObjects;
using ClaraFey.Windows.Common;
using ClaraFey.ViewControllers.Common;
using ClaraFey.ViewControllers.Personeel.UurroosterValidatie;
using ClaraFey.ViewControllers.Personeel.ContactGegevens;

namespace ClaraFey.ViewControllers.Personeel
{
   

    public class PersoneelNavigationVC : ViewModelBase, INotifyPropertyChanged
    {
        /** FIELDS */
        public delegate void SaveErrorHandler(object sender, string errorMessage);
        public event SaveErrorHandler OnSaveError;
        public new event PropertyChangedEventHandler PropertyChanged;

        public ICommand SaveCommand { get; set; }
        public List<gebruiker> personeelList { get; set; }
        public List<NavBarListItem> NavBarList { get; set; }
        public List<NavBarListItem> NavBarListGeneral { get; set; }
        
        public bool NavbarIsLocked { get; set; }
        private gebruiker _selectedPersoneel;
        public gebruiker SelectedPersoneel
        {
            get
            {
                return _selectedPersoneel;
            }

            set
            {
                if (_selectedPersoneel != value)
                {
                    if (!this.Save())
                    {

                        NotifiyPropertyChanged(nameof(SelectedPersoneel));
                        NotifiyPropertyChanged(nameof(HasPersoneel));
                        NotifiyPropertyChanged(_selectedPersoneel.VoornaamAchternaam); 
                        return;
                    }
                    _selectedPersoneel = value;
                    if (_selectedPersoneel != null)
                        SelectNavBarItem = NavBarList[0];//dashboard
                    CheckMenuItems();

                    NotifiyPropertyChanged(nameof(SelectedPersoneel));
                    NotifiyPropertyChanged(nameof(HasPersoneel));
                }
            }
        }
        public bool HasPersoneel
        {
            get
            {
                return SelectedPersoneel == null;
            }
        }
        public void SelectPersoneel (int gebruikerId)
        {
            SelectedPersoneel = personeelList.Where(x => x.id == gebruikerId).FirstOrDefault();
            NotifiyPropertyChanged(nameof(SelectedPersoneel)); 
        }

        private NavBarListItem _selectNavBarItem;
        public NavBarListItem SelectNavBarItem
        {
            get
            {
                return _selectNavBarItem;
            }
            set
            {
                _selectNavBarItem = value;
                if (_selectNavBarItem != null)
                {
                    if (_selectNavBarItem.GroupName == "Algemeen")
                    {
                        SelectedPersoneel = null;
                        NotifiyPropertyChanged(nameof(SelectedPersoneel));
                        CheckMenuItems();
                    }
 
                }
                GlobalMethods.WriteToLog("ClientDossierNavigationVC SelectNavBarItem Load B : "  + _selectNavBarItem.ItemName);
                Load();
                GlobalMethods.WriteToLog("ClientDossierNavigationVC SelectNavBarItem Load E : "  + _selectNavBarItem.ItemName);

                NotifiyPropertyChanged(nameof(NavbarIsLocked));
                NotifiyPropertyChanged(nameof(SelectNavBarItem));
            }
        }
        public bool IsSystemAdmin
        {
            get
            {
                if (GlobalData.Instance.LoggedOnUser.IsLoggedOn == false)
                    return false;
                return false;
            }
        }

        /** CONSTRUCTOR */
        public PersoneelNavigationVC()
        {
            GlobalMethods.WriteToLog("PersoneelNavigationVC");     
        
            UploadFileWindowSettings settings = new UploadFileWindowSettings("Bijlages", "personeelsdossier")
            {
                AllowAddFile = true,
                AllowDeleteFile = true,
                AllowEditing = true
            };
            UploadFileVC bijlagenVC = new UploadFileVC(
                new BijlagenBLLProviderPersoneel(),
                settings
            );

            int i = 1;
            NavBarList = new List<NavBarListItem>()
            {
                new NavBarListItem() {Index = i++, Tag="Contactgegevens", GroupName="Personeel", ItemName="Contactgegevens", ViewModelBase = new ContactGegevensVC() , IsVisible = true, IsEnabled=false},
                new NavBarListItem() {Index = i++, Tag="Bijlagemanager", GroupName="Personeel", ItemName="Bijlagemanager", ViewModelBase = bijlagenVC , IsVisible = true, IsEnabled=false},
                //new NavBarListItem() {Index = i++, Tag="Adresmanager", GroupName="Personeel", ItemName="Adresmanager", ViewModelBase = null, IsVisible = true, IsEnabled=false},
                new NavBarListItem() {Index = i++, Tag="Uurrooster", GroupName="Personeel", ItemName="Uurrooster", ViewModelBase = null, IsVisible = false, IsEnabled=false},
                new NavBarListItem() {Index = i++, Tag="Uurroostervalidatie", GroupName="Tools", ItemName="Uurroostervalidatie", ViewModelBase = new UurroosterValidatie_OverzichtVC(), IsVisible = false, IsEnabled=false},
            };

            personeelList = GebruikerBLL.GetAllGebruikersModulePersoneel();
            SaveCommand = new DelegateCommand<NavBarItemSelectingEventArgs>(SaveC);
        }

        /** EVENTS */
        void SaveC(NavBarItemSelectingEventArgs args)
        {
            args.Cancel = !this.Save();
        }

        /** METHODS */
        private void CheckMenuItems()
        {
            foreach (NavBarListItem i in this.NavBarList)
            {
                if (i.BewonerIndependent)
                    i.IsEnabled = true;
                else
                    i.IsEnabled = SelectedPersoneel != null;
            }
            NotifiyPropertyChanged(nameof(NavBarList));
        }
        private void NotifiyPropertyChanged(string property)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(property));
        }
        private void ThrowError(string errorMessage)
        {
            if (OnSaveError == null)
                return;
            OnSaveError(this, errorMessage);
        }
        public bool Save()
        {
            try
            {
                GlobalMethods.WriteToLog("ClientDossierNavigationVC BEGIN");
                if (_selectNavBarItem != null && _selectNavBarItem.ViewModelBase is IMyUserControl/* && this.SelectedBewoner != null*/)
                    ((IMyUserControl)_selectNavBarItem.ViewModelBase).Save();

                GlobalMethods.WriteToLog("ClientDossierNavigationVC TRUE");
                return true;
            }
            catch (Exception ex)
            {
                ThrowError(ex.Message);
                return false;
            }
        }
        public void Load()
        {
            GlobalMethods.WriteToLog("ClientDossierNavigationVC Load Start");
            if (_selectNavBarItem != null && _selectNavBarItem.ViewModelBase is IMyUserControl)
            {
                InitSMM();

                int? bewonerId = null;
                if (this.SelectedPersoneel != null)
                    bewonerId = this.SelectedPersoneel.id;
                ((IMyUserControl)_selectNavBarItem.ViewModelBase).Load(bewonerId);
            }
            GlobalMethods.WriteToLog("ClientDossierNavigationVC Load End");
        }
        private void InitSMM()
        {
           
        }
        public void RenewBContext()
        {
        }
    }
}
