﻿using ClaraFey.ViewControllers.Common;
using DevExpress.Mvvm;
using sharedcode.BLL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClaraFey.ViewControllers.Personeel.ContactGegevens
{
    public class ContactGegevensVC : ViewModelBase, INotifyPropertyChanged, IMyUserControl
    {
        public string Title { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public List<IMyUserControl> ChildrenUC { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public bool NeedSave { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        public gebruiker Gebruiker { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        /** METHODS */
        void NotifiyPropertyChanged(string property)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(property));
        }
        internal void ResetPassword()
        {
            UserBLL.ResetPassword(Gebruiker.id);
        }
        public void Save()
        {
            GebruikerBLL.SaveGebruiker(Gebruiker);
        }
        public void Load(int? id)
        {
            this.Gebruiker = GebruikerBLL.GetGebruiker(id.GetValueOrDefault());

            NotifiyPropertyChanged(nameof(Gebruiker));
            NotifiyPropertyChanged(nameof(Gebruiker.voornaam));
            NotifiyPropertyChanged(nameof(Gebruiker.achternaam));
            NotifiyPropertyChanged(nameof(Gebruiker.gsm));
            NotifiyPropertyChanged(nameof(Gebruiker.telefoon));
            NotifiyPropertyChanged(nameof(Gebruiker.functiebeschrijving));
            NotifiyPropertyChanged(nameof(Gebruiker.werkplaats));
            NotifiyPropertyChanged(nameof(Gebruiker.telefoon_intern));
            NotifiyPropertyChanged(nameof(Gebruiker.telefoon_intern_groep));
            NotifiyPropertyChanged(nameof(Gebruiker.email));
        }
        public void RegisterChildUCs(IMyUserControl uc)
        {

        }
    }
}
