﻿using ClaraFey.CommonObjects;
using ClaraFey.Windows.Common;
using DevExpress.XtraReports.UI;
using sharedcode.BLL;
using sharedcode.Caches;
using sharedcode.common;
using sharedcode.CommonObjects;
using sharedcode.WeergaveClasses;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace ClaraFey.ViewControllers.Kilometervergoeding
{
    public class KilometerVergoedingVC : MyNotifyPropertyChanged
    {
        /** FIELDS */
        private KilometerVergoedingBLL _kilometersBLL;
        private KilometerItem _selectedKMItem = null;
        public KilometerBLLSettings Settings { get; set; }
        public bool CanDelete
        {
            get
            {
                if (SelectedKMItem == null)
                    return false;
                if (SelectedKMItem.GeslotenBewonersDatum.HasValue)
                    return false;
                if (SelectedKMItem.GeslotenBoekhoudingDatum.HasValue)
                    return false;
                if (SelectedKMItem.GeslotenPersoneelsdientDatum.HasValue)
                    return false;
                if (GlobalData.Instance.LoggedOnUser.GetAccesType("520") != AccessType.none/*admin*/)
                    return true;

                if (GlobalData.Instance.LoggedOnUser.GetAccesType("530") != AccessType.none && SelectedKMItem.Wagen.IsWagenOfFiets == false)
                    return true;

                if (GlobalData.Instance.LoggedOnUser.Id.GetValueOrDefault() == SelectedKMItem.Bestuurder.Id.GetValueOrDefault())
                    return true;

                return false;
            }
        }
        public bool filterBewonersVisible
        {
            get
            {
                return (Settings.Mode == KilometerModeType.bewoners && FilterType == "CLIENT") || Settings.Mode == KilometerModeType.overzicht;
            }
        }
        public bool filterLeefgroepVisible
        {
            get
            {
                return Settings.Mode == KilometerModeType.boekhouding || Settings.Mode == KilometerModeType.overzicht;
            }
        }
        private int _selectedBewoner;
        private int _selectedLeefgroep;
        public List<bewoner> bewonersList
        {
            get
            {
                List<bewoner> list = BewonersListCache.Instance.AllBewonersList;

                if (!list.Exists(x => x.id == 0))
                {
                    list.Insert(0, new bewoner
                    {
                        voornaam = "- Alle cliënten -",
                        code = null,
                        id = 0
                    });
                }
                return list;
            }
        }
        public List<leefgroep> leefgroepenList
        {
            get
            {
                List<leefgroep> list = LeefgroepBLL.GetLeefgroepenKM();
                if (list != null && list.Count > 0 && list[0].code != null)
                {
                    list.Insert(0, new leefgroep
                    {
                        naam = "- Alle leefgroepen -",
                        code = null,
                        id = 0
                    });
                }

                return list;
            }
        }
        public bool CanCloseSelection
        {
            get
            {
                return Settings.Mode == KilometerModeType.personeelsdienst || Settings.Mode == KilometerModeType.bewoners || Settings.Mode == KilometerModeType.boekhouding;
            }
        }
        public DateTime FilterDateFrom { get; set; }
        public DateTime FilterDateTo { get; set; }
        public string FilterClosedText
        {
            get
            {
                if (Settings.Mode == KilometerModeType.personeelsdienst || Settings.Mode == KilometerModeType.boekhouding || Settings.Mode == KilometerModeType.bewoners)
                {
                    if (!FilterClosed.HasValue)
                    {
                        return "afgesloten en niet afgesloten";
                    }
                    else if (!FilterClosed.Value)
                        return "niet afgesloten";
                    else
                        return "afgesloten";
                }

                return "?";
            }
        }
        private string _filterType;
        public string FilterType
        {
            get
            {
                return _filterType;
            }
            set
            {
                if (_filterType != value)
                {
                    SelectedLeefgroep = 0;
                    SelectedBewoner = 0;
                    _filterType = value;
                    NotifiyPropertyChanged(nameof(filterBewonersVisible));
                    NotifiyPropertyChanged(nameof(filterLeefgroepVisible));
                    NotifiyPropertyChanged(nameof(SelectedLeefgroep));
                    NotifiyPropertyChanged(nameof(SelectedBewoner));
                }
            }
        }
        private bool? _filterClosed;
        public bool? FilterClosed
        {
            get
            {
                return _filterClosed;
            }
            set
            {
                _filterClosed = value;
                NotifiyPropertyChanged(nameof(FilterClosedText));
                NotifiyPropertyChanged(nameof(CanCloseSelection));
            }
        }
        public ObservableCollection<KilometerItem> KMItems
        {
            get
            {
                if (_kilometersBLL.KilometerList != null)
                    return new ObservableCollection<KilometerItem>(_kilometersBLL.KilometerList);
                else
                    return new ObservableCollection<KilometerItem>();
            }
        }
        public KilometerItem SelectedKMItem
        {
            get
            {
                return _selectedKMItem;
            }
            set
            {
                _selectedKMItem = value;
                NotifiyPropertyChanged(nameof(CanDelete));
            }
        }
        public List<KilometersWagen> Wagens
        {
            get
            {
                return _kilometersBLL.KilometersWagens;
            }
        }
        public List<KilometersType> Types
        {
            get
            {
                return _kilometersBLL.KilometersTypes;
            }
        }
        public bool CanModifyPrices
        {
            get
            {
                return GlobalData.Instance.LoggedOnUser.GetAccesType("520") != AccessType.none/*ADMIN*/;
            }
        }
        public bool CanAddKilometers
        {
            get
            {
                return Settings.Mode == KilometerModeType.ingave;
            }
        }
        public bool CanExportToExcel
        {
            get
            {
                return Settings.Mode != KilometerModeType.ingave;
            }
        }
        public bool CanModifyKilometers
        {
            get
            {
                if (SelectedKMItem == null)
                    return false;


                if (SelectedKMItem.GeslotenPersoneelsdientDatum.HasValue
                      ||
                      SelectedKMItem.GeslotenBoekhoudingDatum.HasValue
                       ||
                       SelectedKMItem.GeslotenBewonersDatum.HasValue)
                {
                    MessageBox.Show("Deze lijn is reeds afgesloten en verwerkt");
            
                    return false;

                }

              



                if (GlobalData.Instance.LoggedOnUser.GetAccesType("520") == AccessType.none )
                {
                    if (SelectedKMItem.Wagen.IsWagenOfFiets == false)
                    {
                        if (GlobalData.Instance.LoggedOnUser.GetAccesType("530") == AccessType.none)
                        {
                            MessageBox.Show("geen rechten");
                            return false;
                        }
                        else if (SelectedKMItem.Bestuurder.Id != GlobalData.Instance.LoggedOnUser.Id.GetValueOrDefault())
                        {
                            MessageBox.Show("geen rechten");
                            return false;

                        }
                    }

                }
                if (Settings.Mode != KilometerModeType.overzicht)  
                {
                    if (SelectedKMItem.GeslotenPersoneelsdientDatum.HasValue
                        ||
                        SelectedKMItem.GeslotenBoekhoudingDatum.HasValue
                        ||
                        SelectedKMItem.GeslotenBewonersDatum.HasValue
                        )
                    {
                        MessageBox.Show("Deze lijn is reeds afgesloten en verwerkt");
                        return false;
                    }
                }
                if (Settings.Mode == KilometerModeType.overzicht) //alles afgesloten, niet meer wijzigbaar
                {
                    if (SelectedKMItem.GeslotenPersoneelsdientDatum.HasValue
                        &&
                        SelectedKMItem.GeslotenBoekhoudingDatum.HasValue
                        &&
                        SelectedKMItem.GeslotenBewonersDatum.HasValue
                        )
                    {
                        MessageBox.Show("Deze lijn is reeds afgesloten en verwerkt");
                        return false;
                    }
                }

           

                return true;
            }
        }

        

        public bool IsCheckedByBewonersAdministratieVisible
        {
            get
            {
                return  Settings.Mode == KilometerModeType.overzicht;
            }
        }

        public bool IsColumnGeslotenPersoneelsdienstVisible
        {
            get
            {
                return Settings.Mode == KilometerModeType.personeelsdienst || Settings.Mode == KilometerModeType.ingave || Settings.Mode == KilometerModeType.overzicht || Settings.Mode == KilometerModeType.boekhouding;
            }
        }
        public bool IsColumnGeslotenBoehoudingVisible
        {
            get
            {
                return Settings.Mode == KilometerModeType.boekhouding || Settings.Mode == KilometerModeType.overzicht;
            }
        }
        public bool IsColumnGeslotenBewonersVisible
        {
            get
            {
                return Settings.Mode == KilometerModeType.bewoners || Settings.Mode == KilometerModeType.overzicht || Settings.Mode == KilometerModeType.boekhouding;
            }
        }
        public bool IsFilterGeslotenVisible
        {
            get
            {
                return Settings.Mode == KilometerModeType.bewoners || Settings.Mode == KilometerModeType.boekhouding || Settings.Mode == KilometerModeType.personeelsdienst;
            }
        }
        public bool IsFilterTypeBewonersVisible
        {
            get
            {
                return Settings.Mode == KilometerModeType.bewoners;
            }
        }
        public List<KilometersType> listBewonersKampOrClient
        {
            get
            {
                List<KilometersType> list = KilometerVergoedingBLL.GetKilometersTypes().Where(x => x.Code == "KAMP" || x.Code == "CLIENT").ToList();
                list.Insert(0, new KilometersType
                {
                    Code = "",
                    Naam = "Kamp & Cliënt "
                });

                return list;
            }
        }
        public int SelectedLeefgroep
        {
            get
            {
                return _selectedLeefgroep;
            }
            set
            {
                if (value != _selectedLeefgroep)
                {
                    _selectedLeefgroep = value;
                    _selectedBewoner = 0;
                    NotifiyPropertyChanged(nameof(SelectedBewoner));
                    NotifiyPropertyChanged(nameof(SelectedLeefgroep));
                }
            }
        }
        public int SelectedBewoner
        {
            get
            {
                return _selectedBewoner;
            }
            set
            {
                if (value != _selectedBewoner)
                {
                    _selectedBewoner = value;
                    _selectedLeefgroep = 0;
                    NotifiyPropertyChanged(nameof(SelectedLeefgroep));
                    NotifiyPropertyChanged(nameof(SelectedBewoner));
                }
            }
        }

        /** CONVERTORS */
        public KilometerVergoedingVC(KilometerModeType mode)
        {
            Settings = new KilometerBLLSettings(mode);

            if (mode == KilometerModeType.ingave)
            {
                DateTime start = DateTime.Now.AddMonths(-2);
                FilterDateFrom = new DateTime(start.Year, start.Month, 1);
                FilterDateTo = FilterDateFrom.AddMonths(3).AddDays(-1);
            }
            else
            {
                FilterDateFrom = new DateTime(DateTime.Now.Year, 1, 1);
                FilterDateTo = FilterDateFrom.AddYears(1).AddDays(-1);
            }

            FilterClosed = null;
            FilterType = string.Empty;
            Settings.FilterGebruikerId = GlobalData.Instance.LoggedOnUser.Id.Value;
            _kilometersBLL = new KilometerVergoedingBLL(Settings);
        }

        /** METHODS */
        public void PrintReport(DateTime dateFrom, DateTime dateTo, bool afsluiten)
        {
            FilterDateFrom = dateFrom;
            FilterDateTo = dateTo;
            FilterClosed = false;
            SelectedBewoner = 0;
            SelectedLeefgroep = 0;
            LoadKilometers();
            NotifiyPropertyChanged(nameof(FilterDateFrom));
            NotifiyPropertyChanged(nameof(FilterDateTo));
            NotifiyPropertyChanged(nameof(FilterClosed));
            NotifiyPropertyChanged(nameof(SelectedBewoner));
            NotifiyPropertyChanged(nameof(SelectedLeefgroep));

            if (Settings.Mode == KilometerModeType.personeelsdienst)
            {
                if (afsluiten)
                {
                    _kilometersBLL.CloseKilometers();
                }
                KilometerPersoneelsdienstReport reportBO = new KilometerPersoneelsdienstReport(dateFrom, dateTo);
                reportBO.GenerateReport(KMItems.ToList(), afsluiten);
                Reports.KilometerPersoneelsdienstReport report = new Reports.KilometerPersoneelsdienstReport(reportBO);

                if (!afsluiten)
                {
                    report.Watermark.Text = "Test";
                    report.Watermark.TextDirection = DevExpress.XtraPrinting.Drawing.DirectionMode.BackwardDiagonal;
                }
                using (ReportPrintTool printTool = new ReportPrintTool(report))
                {
                    printTool.ShowRibbonPreviewDialog();
                }
            }
            else if (Settings.Mode == KilometerModeType.bewoners || Settings.Mode == KilometerModeType.boekhouding)
            {
                if (afsluiten)
                {
                    _kilometersBLL.CloseKilometers();
                }
                KilometerLeefgroepBewonerReport reportBO = new KilometerLeefgroepBewonerReport(dateFrom, dateTo);
                reportBO.GenerateReport(KMItems.ToList(), afsluiten, Settings.Mode);
                Reports.KilometerBoekhBewonersReport report = new Reports.KilometerBoekhBewonersReport(reportBO, Settings.Mode);

                if (!afsluiten)
                {
                    report.Watermark.Text = "Test";
                    report.Watermark.TextDirection = DevExpress.XtraPrinting.Drawing.DirectionMode.BackwardDiagonal;
                }

                using (ReportPrintTool printTool = new ReportPrintTool(report))
                {
                    printTool.ShowRibbonPreviewDialog();
                }
                ShowCvsExportableGrid(reportBO);
            }

            if (afsluiten)
            {
                FilterClosed = true;
                NotifiyPropertyChanged(nameof(FilterClosed));
            }
            LoadKilometers();
        }
        public void ShowCvsExportableGrid(KilometerLeefgroepBewonerReport bo)
        {
            string title = "Kilometervergoeding - aflsuiten " + DateTime.Now.ToString("dd-MM-yyyy");
            SMMWindow2Level level0 = new SMMWindow2Level(0, title)
            {
                AllowDelete = false,
                AllowModify = false,
                AllowAdd = false,
                AllowSearch = false,
                AllowFilter = false,
                CanExportToExcel = true
            };
            level0.AddColumns(
                new SMMWindow2Column() { FieldName = "Rijksregisternummer", ColumnHeader = "Rijksregisternummer", IsEditable = false },
                new SMMWindow2Column() { FieldName = "DatumString", ColumnHeader = "Datum", IsEditable = false },
                new SMMWindow2Column() { FieldName = "FinancielbewegingsCode", ColumnHeader = "FinancielbewegingsCode", IsEditable = false },
                new SMMWindow2Column() { FieldName = "Omschrijving", ColumnHeader = "Omschrijving", IsEditable = false },
                new SMMWindow2Column() { FieldName = "Eenheden", ColumnHeader = "Eenheden", IsEditable = false },
                new SMMWindow2Column() { FieldName = "Eenheidsprijs", ColumnHeader = "Eenheidsprijs", IsEditable = false },
                new SMMWindow2Column() { FieldName = "Totaal", ColumnHeader = "Totaal", IsEditable = false }
            );
            level0.DataSource = new ObservableCollection<KilometerLeefgroepBewonerShortCsvReportItem>(bo.reportItemsCSVShort).ToList();
            SMMWindow2Settings settings = new SMMWindow2Settings(title, GlobalMethods.Themes.normal)
            {
                WindowSizePercentage = 75
            };
            settings.AddLevel(level0);
            settings.theme = GlobalMethods.Themes.clientdossier;
            smmWindowVC vc = new smmWindowVC();
            SMMWindow2 w = new SMMWindow2(vc, settings);
            w.ShowDialog();
        }
        public void DeleteSelectedItem()
        {
            if (!CanDelete)
                throw new Exception("Geen rechten");
            _kilometersBLL.DeleteKilometers(SelectedKMItem.Id.GetValueOrDefault());

            SelectedKMItem = null;
            NotifiyPropertyChanged("KMItems");
        }

        public void MarkAsNagekeken(int id)
        {
            _kilometersBLL.MarkAsNagekeken(id);
        }
        public void MarkAsNietNagekeken(int id)
        {
            _kilometersBLL.MarkAsNietNagekeken(id);
        }

        private void UpdateSettings()
        {
            Settings.DateFrom = FilterDateFrom;
            Settings.DateTo = FilterDateTo;
            Settings.Type = FilterType;
            Settings.Closed = FilterClosed;
            Settings.BewonerId = SelectedBewoner;
            Settings.LeefgroepId = SelectedLeefgroep;
        }
        public void LoadKilometers()
        {
            UpdateSettings();
            _kilometersBLL.LoadKilometers();
            NotifiyPropertyChanged(nameof(KMItems));
        }
        public void AddNewKilometer(KilometerItem item)
        {
            _kilometersBLL.KilometerList.Add(item);
            NotifiyPropertyChanged(nameof(KMItems));
        }
        public void NotifyModifiedKmItem()
        {
            NotifiyPropertyChanged(nameof(SelectedKMItem));
            NotifiyPropertyChanged(nameof(KMItems));
        }
        public void CleanUp()
        {
            _kilometersBLL.CleanUp();
            _kilometersBLL = null;
        }
    }
}
