﻿using ClaraFey.BLL;
using ClaraFey.CommonObjects;
using ClaraFey.Windows.Kilometervergoeding;
using sharedcode.BLL;
using sharedcode.Caches;
using sharedcode.common;
using sharedcode.CommonObjects;
using sharedcode.WeergaveClasses;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace ClaraFey.ViewControllers.Kilometervergoeding
{
    public class KilometerSaveVC : MyNotifyPropertyChanged
    {
        /** FIELDS */
        public KilometerItem KMItem { get; set; } = null;
        public bool CanChangeBestuurder
        {
            get
            {
                if (ChangeMode == SaveKilometerWindowChangeModes.ChangeType)
                    return false;
                if (GlobalData.Instance.LoggedOnUser.GetAccesType("530") != AccessType.none)
                    return true;
                if (GlobalData.Instance.LoggedOnUser.GetAccesType("520") != AccessType.none)
                    return true;
                return false;
            }
        }
        public string Bestuurder
        {
            get
            {
                if (KMItem != null && KMItem.Bestuurder != null)
                    return KMItem.Bestuurder.ToString();
                else
                    return string.Empty;
            }

        }
        public ObservableCollection<bewoner> selectedBewonersList
        {
            get
            {
                return new ObservableCollection<bewoner>(KMItem.Bewoners);
            }
        }
        public ObservableCollection<leefgroep> selectedLeefgroepenList
        {
            get
            {
                return new ObservableCollection<leefgroep>(KMItem.Leefgroepen);
            }
        }
        public ObservableCollection<kampen> selectedKampenList
        {
            get
            {
                return new ObservableCollection<kampen>(KMItem.Kampen);
            }
        }
        public List<KilometersType> kmTypesList
        {
            get
            {
                return KilometerVergoedingBLL.GetKilometersTypes();
            }
        }
        public List<KilometersWagen> kmWagensList
        {
            get
            {
                try
                {
                    List<KilometersWagen> allWagens = KilometerVergoedingBLL.GetKilometersWagens();

                    //indien rechen voor busjes, alle wagen types teruggeven
                    if (GlobalData.Instance.LoggedOnUser.GetAccesType("530") == AccessType.access)
                        return allWagens;
                    else
                    {
                        //indien de ingegevn wagen bij een update en busjes is, ook alles terug geven
                        if (KMItem.Wagen != null && KMItem.Wagen.Id.HasValue && KMItem.Wagen.Id != 1 && KMItem.Wagen.Id != 5)
                            return allWagens;
                    }

                    //anders enkel eigen wagen en fiets
                    List<KilometersWagen> wa = new List<KilometersWagen>();
                    foreach (KilometersWagen w in allWagens)
                    {
                        if (w.IsWagenOfFiets)
                            wa.Add(w);
                    }  
           
                    return wa;
                }
                catch (Exception ex)
                {


                }
                return new List<KilometersWagen>();
            }
        }
        private List<string> _bestemmingSuggesties = new List<string>();
        public List<string> BestemmingSuggesties
        {
            get
            {
                _bestemmingSuggesties = new List<string>();
                if (_bestemmingSuggesties == null || _bestemmingSuggesties.Count == 0)
                {
                    int userId = GlobalData.Instance.LoggedOnUser.Id.GetValueOrDefault();
              
                    
                 

                    if (KMItem.Bestuurder.Id.HasValue)
                        userId = KMItem.Bestuurder.Id.GetValueOrDefault();

                    if (KMItem.Wagen != null && !String.IsNullOrWhiteSpace(KMItem.Type.Code))
                        _bestemmingSuggesties = KilometerVergoedingBLL.GetVerplaatsingSuggestions(userId, KMItem.Wagen.Id.GetValueOrDefault(), KMItem.Type.Code);

                }
                return _bestemmingSuggesties;
            }
        }
        private List<string> _redenSuggesties = new List<string>();
        public List<string> RedenSuggesties
        {
            get
            {
                _redenSuggesties = new List<string>();
                if (_redenSuggesties == null || _redenSuggesties.Count == 0)
                {
                    int userId = GlobalData.Instance.LoggedOnUser.Id.GetValueOrDefault();
                    if (KMItem.Bestuurder.Id.HasValue)
                        userId = KMItem.Bestuurder.Id.GetValueOrDefault();


                    if (KMItem.Wagen != null && !String.IsNullOrWhiteSpace(KMItem.Type.Code) )
                        _redenSuggesties = KilometerVergoedingBLL.GetRedenSuggestions(userId, KMItem.Wagen.Id.GetValueOrDefault(), KMItem.Type.Code);

                }
                return _redenSuggesties;
            }
        }
        public List<bewoner> bewonersList
        {
            get
            {
                return BewonersListCache.Instance.AllBewonersList;
            }
        }
        public List<leefgroep> leefgroepenList
        {
            get
            {
                return LeefgroepBLL.GetLeefgroepenKM();
            }
        }
        public List<kampen> kampenList
        {
            get
            {
                return kampen.GetAll(true);
            }
        }
        public bool CanSave
        {
            get
            {
                return true;
            }
        }
        public bool CanModifyBewoners
        {
            get
            {
              //  if (ChangeMode == SaveKilometerWindowChangeModes.ChangeType)
               //     return false;
                return KMItem.Type.Code == "CLIENT" || KMItem.Type.Code == "IRO";
            }
        }
        public bool CanModifyKampen
        {
            get
            {
               // if (ChangeMode == SaveKilometerWindowChangeModes.ChangeType)
               //     return false;
                return KMItem.Type.Code == "KAMP";
            }
        }
        public bool CanModifyLeefgroepen
        {
            get
            {
                //if (ChangeMode == SaveKilometerWindowChangeModes.ChangeType)
                //    return false;
                return KMItem.Type.Code == "LFGR";
            }
        }
        public bool CanModifyKilometerstandWagen
        {
            get
            {
                if (ChangeMode == SaveKilometerWindowChangeModes.ChangeType)
                    return false;

                if (KMItem.Wagen == null)
                    return false;
                return KMItem.Wagen.Id != null && KMItem.Wagen.Id != 1 && KMItem.Wagen.Id != 5;//indien  eigen wagen of eigen fiets
            }
        }
        public bool CanModifyKilometers
        {
            get
            {
                if (KMItem.Wagen == null)
                    return false;
                if (ChangeMode == SaveKilometerWindowChangeModes.ChangeType)
                    return false;
                return KMItem.Wagen.Id != null && (KMItem.Wagen.Id == 1 || KMItem.Wagen.Id == 5); //indien eigen wagen moeten km direct worden ingegeven (niet via stand)
            }
        }
        public bool CanEditKilometers
        {
            get
             {

                if (KMItem.Wagen == null)
                    return false;
                if (ChangeMode == SaveKilometerWindowChangeModes.ChangeType)
                    return false;
                return KMItem.Wagen.Id != null && KMItem.Type != null; //indien eigen wagen moeten km direct worden ingegeven (niet via stand)
            }
        }
        public bool CanModifyOmnium
        {
            get
            {

                if (KMItem.Wagen == null)
                    return false;
                if (ChangeMode == SaveKilometerWindowChangeModes.ChangeType)
                    return false;
                return KMItem.Wagen.Id == 1;
            }
        }
        public bool CanModifyRedenBestemming
        {
            get
            {
                if (ChangeMode == SaveKilometerWindowChangeModes.ChangeType)
                    return false;
                return true;
            }
        }

        public SaveKilometerWindowChangeModes ChangeMode { get; set; }

        /** CONSTRUCTORS */
        public KilometerSaveVC(KilometerItem item, SaveKilometerWindowChangeModes changeMode)
        {

            ChangeMode = changeMode;

            if (item == null)
                KMItem = new KilometerItem();
            else
            {

                KMItem = GlobalSharedMethods.DeepClone<KilometerItem>(item);
                KMItem.Wagen = kmWagensList.Where(x => x.Id == item.Wagen.Id).FirstOrDefault();

            }
 
        }
        
        /** METHODS */
        public void CleanUp()
        {
            //KMItem = null;
        }
        public void ChangeBestuurder(int id, String voornaam, string achternaam)
        {
            if (KMItem != null)
            {
                KMItem.Bestuurder.Id = id;
                KMItem.Bestuurder.FirstName = voornaam;
                KMItem.Bestuurder.LastName = achternaam;

                NotifiyPropertyChanged(nameof(Bestuurder));
            }
        }
        private void AddBestemmingSuggestionToCache(String newSuggestion)
        {
            if (_bestemmingSuggesties == null || _bestemmingSuggesties.Count == 0)
            {
                _bestemmingSuggesties = new List<string>();
            }
            if (!_bestemmingSuggesties.Contains(newSuggestion.Trim()))
                _bestemmingSuggesties.Add(newSuggestion.Trim());
        }
        public void NotifyChangeType()
        {
            NotifiyPropertyChanged(nameof(CanModifyKampen));
            NotifiyPropertyChanged(nameof(CanModifyBewoners));
            NotifiyPropertyChanged(nameof(CanModifyLeefgroepen));
            NotifiyPropertyChanged(nameof(CanModifyRedenBestemming));
            NotifiyPropertyChanged(nameof(CanModifyKilometers));
            NotifiyPropertyChanged(nameof(CanEditKilometers));
            NotifiyPropertyChanged("CanModifyKilometerstandWagen");
            NotifiyPropertyChanged("BestemmingSuggesties");
            NotifiyPropertyChanged("RedenSuggesties");
        }
        public void NotifyChangeWagen()
        {
            KMItem.BeginstandKilometer = null;
            KMItem.EindstandKilometer = null;
            KMItem.Kilometers = null;
            NotifiyPropertyChanged(nameof(KMItem));
            NotifiyPropertyChanged(nameof(CanEditKilometers));
            NotifiyPropertyChanged(nameof(CanModifyKilometers));
            NotifiyPropertyChanged(nameof(CanModifyKilometerstandWagen));
            NotifiyPropertyChanged(nameof(CanModifyOmnium));
            NotifiyPropertyChanged(nameof(CanModifyRedenBestemming));
            NotifiyPropertyChanged("CanModifyKilometerstandWagen");
            NotifiyPropertyChanged("BestemmingSuggesties");
            NotifiyPropertyChanged("RedenSuggesties");
        }
        public void AddBewoner(bewoner b)
        {
            KMItem.AddBewoner(b);
            NotifiyPropertyChanged(nameof(selectedBewonersList));
        }
        public void AddLeefgroep(leefgroep lfg)
        {
            KMItem.AddLeefgroep(lfg);
            NotifiyPropertyChanged(nameof(selectedLeefgroepenList));
        }
        public void AddKampen(kampen lfg)
        {
            KMItem.AddKamp(lfg);
            NotifiyPropertyChanged(nameof(selectedKampenList));
        }
        public void DeleteBewoner(int bewonerId)
        {
            KMItem.DeleteBewoner(bewonerId);
            NotifiyPropertyChanged(nameof(selectedBewonersList));
        }
        public void DeleteLeefgroep(int leefgroepId)
        {
            KMItem.DeleteLeefgroep(leefgroepId);
            NotifiyPropertyChanged(nameof(selectedLeefgroepenList));
        }
        public void DeleteKamp(int kampId)
        {
            KMItem.DeleteKamp(kampId);
            NotifiyPropertyChanged(nameof(selectedKampenList));
        }
        public string Save()
        {
            string errorMessage = Validate();
            if (!string.IsNullOrWhiteSpace(errorMessage))
                return errorMessage;
            else
            {
                try
                {
                    KMItem.Save();
                    AddBestemmingSuggestionToCache(KMItem.Bestemming);
                    return null;
                }
                catch (Exception e)
                {
                    return e.Message;
                }
            }
        }
        public string Validate()
        {
            //prepare data for save
            if (!CanModifyBewoners)
                KMItem.Bewoners.Clear();
            if (!CanModifyLeefgroepen)
                KMItem.Leefgroepen.Clear();
            if (!CanModifyKilometerstandWagen)
            {
                KMItem.EindstandKilometer = null;
                KMItem.BeginstandKilometer = null;
            }
            if (!CanModifyOmnium)
                KMItem.Omnium = false;
            if (KMItem.Bestuurder.Id.HasValue == false)
                return "Gelieve een bestuurder uit te kiezen.";
            //check for errors
            if (string.IsNullOrWhiteSpace(KMItem.Bestemming))
                return "Gelieve een reisroute in te geven.";
            if (string.IsNullOrWhiteSpace(KMItem.Reden))
                return "Gelieve een reden in te geven.";
            if (KMItem.Datum == null)
                return "Gelieve een datum in te geven.";
            if (KMItem.Type == null || string.IsNullOrWhiteSpace(KMItem.Type.Code))
                return "Gelieve type in te geven.";
            if (KMItem.Wagen == null || KMItem.Wagen.Id == null)
                return "Gelieve wagen in te geven.";
            if (CanModifyLeefgroepen && KMItem.Leefgroepen.Count == 0)
                return "Gelieve min. 1 Leefgroep  uit te kiezen";
            if (CanModifyBewoners && KMItem.Bewoners.Count == 0)
                return "Gelieve min. 1 cliënt  uit te kiezen";
            if (CanModifyKampen && KMItem.Kampen.Count == 0)
                return "Gelieve min. 1 Kamp  uit te kiezen";
            if (KMItem.Datum.Date > DateTime.Now.Date && KMItem.Kilometers.HasValue)
                return "De gereden kilometers kunnen niet worden ingegeven voor een datum in de toekomst.";

            return null;
        }
    }
}
