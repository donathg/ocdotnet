﻿
using ClaraFey.CommonObjects;
using sharedcode.BLL;
using sharedcode.common;
using sharedcode.CommonObjects;
using sharedcode.WeergaveClasses;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClaraFey.ViewControllers.Inventaris
{
    public class InventarisViewController : INotifyPropertyChanged/*,IClaraFeyCleanup*/
    {
        private InventarisWeergave _selectedInventarisItem;
        public InventarisWeergave SelectedInventarisItem
        {
            get { return _selectedInventarisItem; }
            set {

                if (_selectedInventarisItem != value)
                {
                    _selectedInventarisItem = value;
                    NotifiyPropertyChanged(nameof(SelectedInventarisItem));
                    NotifiyPropertyChanged(nameof(CanSeeInformaticaTab));
                }
            }
        }
        private Location _selectedLocatieItem;
        public Location SelectedLocatieItem
        {
            get { return _selectedLocatieItem; }
            set
            {
                if (_selectedLocatieItem != value)
                {
                    _selectedLocatieItem = value;
                    SetInventarisLocation(value);
                    NotifiyPropertyChanged(nameof(SelectedLocatieItem));
                }
            }
        }
        public GebouwenBeheerBLL BllGebouwenBeheer { get; set; } = new GebouwenBeheerBLL(new GebouwenBeheerSettings());
        public InventarisBLL BllInventaris { get; set; } = new InventarisBLL(new InventarisSettings() { Geinventariseerd = null, LoadUitdienst = null, LocatieId = null });
        public void Save()
        {
            if (CanModify)
                  BllInventaris.Save();
        }
        private HistoryType _historyType = HistoryType.active;
        public void ChangeHistoryType(HistoryType type)
        {
            this._historyType = type;
            SelectedInventarisItem = null;
            NotifiyPropertyChanged("SelectedInventarisItem");
            NotifiyPropertyChanged("InventarisItems");
        }
        /// <summary>
        /// Deze methode selecteerd een item(treenode) van de locatieboom
        /// </summary>
        /// <param name="id"></param>
        public void SelectLocation(int? id)
       {
           
           this.BllGebouwenBeheer.SelectLocation(id);
       }
        /// <summary>
        /// deze methode zet de locatie voor een inventarisitem
        /// </summary>
        /// <param name="id"></param>
        public void SetInventarisLocation(Location loc)
        {
            this.BllInventaris.SetLocation(SelectedInventarisItem, loc);
        }
        public bool CanModify
        {
            get
            {
                return sharedcode.common.GlobalData.Instance.LoggedOnUser.GetAccesType("100") == AccessType.access;
            }

        }
        public bool CannotModify
       {
           get
           {
               return !CanModify;
           }

       }
        public List<InventarisWeergave> InventarisItems
        {
            get
            {
                if (_historyType == HistoryType.active)
                    return new List<InventarisWeergave>(BllInventaris.GetAllInventarisItems().Where(inv => (inv.StatusType == InventarisStatusType.Actief || inv.StatusType == InventarisStatusType.Afgeschreven)));
                else
                    return new List<InventarisWeergave>(BllInventaris.GetAllInventarisItems().Where(inv => (inv.StatusType != InventarisStatusType.Actief && inv.StatusType != InventarisStatusType.Afgeschreven)));
            }
        }
        public List<LeverancierItem> LeveranciersItems
        {
            get
            {
                return BllInventaris.GetAllLeveranciers();
            }
        }
        public List<toestel> ToestelItems
        {
            get
            {
                return BllInventaris.GetAllToestellen();
            }
        }
        public List<merk> MerkItems
        {

            get
            {
                return BllInventaris.GetAllMerken();
            }
        }
        private ObservableCollection<LocatieobjectWeergave> _datarackWeergaves;
        public ObservableCollection<LocatieobjectWeergave> DatarackWeergaveList
        {
            get
            {
                if (_datarackWeergaves == null || _datarackWeergaves.Count == 0)
                    _datarackWeergaves = new ObservableCollection<LocatieobjectWeergave>(InventarisBLL.GetAllObjectenMetLocatiesFromObjecttypeId(4));

                return _datarackWeergaves;
            }
        }
        public bool CanSeeInformaticaTab
        {
            get
            {
                return (GlobalData.Instance.LoggedOnUser.HasAccess("201") || GlobalData.Instance.LoggedOnUser.HasAccess("202")) && SelectedInventarisItem != null && SelectedInventarisItem.ToestelId.HasValue && 
                    BllInventaris.GetToestelById(SelectedInventarisItem.ToestelId.Value) is toestel tst && tst.category == "IT";
            }
        }

        public InventarisViewController()
        {
            BllInventaris.GetAllInventarisItems();
        }

        public void NotifiyPropertyChanged(string property)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(property));
        }
        public event PropertyChangedEventHandler PropertyChanged;

        public void CleanUp()
        {
            this.BllInventaris.CleanUp();
            this.BllGebouwenBeheer = null;
            this.BllInventaris = null;
            this._selectedInventarisItem = null;
            this._selectedLocatieItem = null;
        }
        public void ReloadMerken()
        {
            this.BllInventaris.ReloadMerkenFromDB();
            NotifiyPropertyChanged("MerkItems");
        }
        public void ReloadLeveranciers()
        {
            this.BllInventaris.ReloadLeveranciersFromDB();
            NotifiyPropertyChanged("LeveranciersItems");
        }
        public void ReloadToestellen()
        {
            this.BllInventaris.ReloadToestellenFromDB();
            NotifiyPropertyChanged("ToestelItems");
        }
    }
}
