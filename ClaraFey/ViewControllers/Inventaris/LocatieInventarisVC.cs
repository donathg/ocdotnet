﻿using ClaraFey.CommonObjects;
using ClaraFey.ViewControllers.Common;
using ClaraFey.Windows.Common;
using sharedcode.BLL;
using sharedcode.common;
using sharedcode.CommonObjects;
using sharedcode.WeergaveClasses;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClaraFey.ViewControllers.Inventaris
{
    public class LocatieInventarisVC : MyNotifyPropertyChanged
    {
        /** VARIABLES */
        private LocatieManager _manager;
        
        public ObservableCollection<locatie_objecten> ObjectenList { get; set; }
        public ObservableCollection<locatie> LocatieList { get; set; }
        private locatie _selectedTreeItem;
        public locatie SelectedTreeItem
        {
            get
            {
                return _selectedTreeItem;
            }
            set
            {
                _selectedTreeItem = value;
                ChangeContentFromSelectedLocatie();
                NotifiyPropertyChanged(nameof(SelectedTreeItem));
            }
        }
        public List<InventarisWeergave> InventarisList { get; set; }
        public InventarisWeergave SelectedInventaris { get; set; }
        public bool IsTabsVisible { get; set; }
        public smmWindowVC SmmObjectenVC { get; set; }
        public UploadFileVC BijlagenVC { get; set; }

        /** CONSTRUCTOR */
        public LocatieInventarisVC()
        {
            _manager = new LocatieManager(new LocatieProvider());
            LocatieList = new ObservableCollection<locatie>(_manager.GetAllLocaties());
            IsTabsVisible = false;
            ObjectenList = new ObservableCollection<locatie_objecten>();

            NotifyStartPositions(); // nodig???
        }

        /** METHODS */
        private void NotifyStartPositions()
        {
            NotifiyPropertyChanged(nameof(LocatieList));
            NotifiyPropertyChanged(nameof(IsTabsVisible));
        }
        private void ChangeContentFromSelectedLocatie()
        {
            if (SelectedTreeItem != null)
            {
                IsTabsVisible = true;
                NotifiyPropertyChanged("IsTabsVisible");
                
                InventarisList = _manager.GetAllInventarisFromLocatie(SelectedTreeItem);
                NotifiyPropertyChanged("InventarisList");
                SetObjectenUsercontrol();
                SetBijlagenVC();
            }
            else
            {
                IsTabsVisible = false;
                NotifiyPropertyChanged("IsTabsVisible");
            }
        }
        private void SetBijlagenVC()
        {
            UploadFileWindowSettings settings = new UploadFileWindowSettings("Bijlagen", "Locaties")
            {
                AllowAddFile = true,
                AllowEditing = true,
                AllowDeleteFile = true
            };

            BijlagenVC = new UploadFileVC(
                new BijlagenBLLProviderLocaties(),
                SelectedTreeItem.id,
                settings
            );
            BijlagenVC.BijlagenChanged += new UploadFileVC.BijlagenChangedHandler(UpdateBijlagenAantal);

            NotifiyPropertyChanged(nameof(BijlagenVC));
        }
        private void UpdateBijlagenAantal(int aantalBijlagen)
        {
            SelectedTreeItem.AantalBijlagen = aantalBijlagen;
        }
        private void SetObjectenUsercontrol()
        {
            ObjectenList.Clear();
            foreach (locatie_objecten item in SmmObjectenVC.Entity.locatie_objecten.Where(x => x.loc_id == SelectedTreeItem.id).ToList())
            {
                ObjectenList.Add(item);
            }
        }
        private void BeforeUpdateObjects(object row)
        {
            locatie_objecten obj = (locatie_objecten)row;
            obj.loc_id = SelectedTreeItem.id;
        }
        public void InitObjectenUsercontrol()
        {
            SmmObjectenVC = new smmWindowVC();

            List<SMMWindow2ComboItem> cmbObjecten = new List<SMMWindow2ComboItem>();
            foreach (objecttype o in _manager.GetallObjecttypes())
            {
                cmbObjecten.Add(new SMMWindow2ComboItem() { Id = o.id, Name = o.naam });
            }

            SMMWindow2Settings settings = new SMMWindow2Settings("Objecten", GlobalMethods.Themes.normal);

            SMMWindow2Level level0 = new SMMWindow2Level(0, "")
            {
                CanExportToExcel = true,
                BeforeUpdateMethod = new SMMWindow2Level.BeforeUpDateDelegate(BeforeUpdateObjects),
                AfterUpdateMethod = new SMMWindow2Level.AfterUpDateDelegate(AfterUpdateObjects),
                AfterDeleteMethod = new SMMWindow2Level.AfterUpDateDelegate(AfterDeleteObjects),
                AllowGroup = true,
                AllowDelete = true,
                AllowModify = true,
                AllowAdd = true,
                AllowSearch = true,
                AllowFilter = true
            };
            level0.AddColumns(
                new SMMWindow2Column() { FieldName = "objecttype_id", ColumnHeader = "Object", ComboboxItems = cmbObjecten, IsEditable = true },
                new SMMWindow2Column() { FieldName = "serienummer", ColumnHeader = "Serienummer", IsEditable = true, IsNullable = true },
                new SMMWindow2Column() { FieldName = "opmerking", ColumnHeader = "Opmerking", IsEditable = true, IsNullable = true },
                new SMMWindow2Column() { FieldName = "hoeveelheid", ColumnHeader = "Hoeveelheid", IsEditable = true, IsNullable = true },
                new SMMWindow2Column() { FieldName = "eigenaar", ColumnHeader = "Eigenaar", IsEditable = true, IsNullable = true }
            );
            level0.DataSource = ObjectenList;
            settings.AddLevel(level0);

            SmmObjectenVC.Settings = settings;

            SmmObjectenVC.ChildrenUC = new List<IMyUserControl>();
        }
        private void AfterDeleteObjects(object row)
        {
            locatie_objecten item = (locatie_objecten)row;
            if (item.objecttype_id == 1 /*asbest*/)
            {
                SelectedTreeItem.AantalAsbest--;
            }
            else if (item.objecttype_id == 4 /*Datarack*/)
            {
                SelectedTreeItem.AantalRacks--;
            }
        }
        private void AfterUpdateObjects(object row)
        {
            locatie_objecten item = (locatie_objecten)row;
            if (item.objecttype_id == 1 /*asbest*/)
            {
                SelectedTreeItem.AantalAsbest++;
            }
            else if (item.objecttype_id == 4 /*Datarack*/)
            {
                SelectedTreeItem.AantalRacks++;
            }
        }
    }
}
