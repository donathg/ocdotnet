﻿using sharedcode.BLL;
using sharedcode.common;
using sharedcode.CommonObjects;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace ClaraFey.ViewControllers
{
    public class GebouwenBeheerVC: MyNotifyPropertyChanged
    {
        /** PROPERTIES */
        private LocatieBLL _bll;
        public ObservableCollection<locatie> LocatieList { get; set; }
        public locatie SelectedLocatie { get; set; }
        public bool CanModify
        {
            get
            {
                return GlobalData.Instance.LoggedOnUser.GetAccesType("100") == AccessType.access;
            }
        }

        public GebouwenBeheerVC()
        {
            _bll = new LocatieBLL();
            RefreshLocaties();
        }

        private void RefreshLocaties()
        {
            LocatieList = new ObservableCollection<locatie>(_bll.GetAllLocaties());
            NotifiyPropertyChanged(nameof(LocatieList));
        }
        public void ChangeLocatie(locatie loc)
        {
            _bll.ModifyLocatie(loc);
            RefreshLocaties();
        }
        public void RemoveLocation(locatie loc)
        {
            try
            {
                _bll.RemoveLocatie(loc);
                RefreshLocaties();
            }
            catch (SqlException ex)
            {
                if (ex.Message.Contains("REFERENCE"))
                {
                    if(MessageBox.Show(@"Er hangen nog andere objecten aan deze locatie.
Wil dit adres dan inactief maken?
(alle objecten hangen dan nog altijd aan deze locatie vast, maar deze locatie wordt dan niet meer weergegeven in werbonnenbeheer of inventarisbeheer)", "Verkeerde handeling geselecteerd", MessageBoxButton.YesNo, MessageBoxImage.Information) == MessageBoxResult.Yes)
                    {
                        _bll.SetStatusInactive(loc);
                        RefreshLocaties();
                    }
                }
            }
        }
        public void AddLocatie(locatie location)
        {
            _bll.AddLocatie(location);

            RefreshLocaties();
        }
    }
}
