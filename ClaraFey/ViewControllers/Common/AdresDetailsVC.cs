﻿using sharedcode.BLL;
using sharedcode.common;
using sharedcode.WeergaveClasses;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace ClaraFey.ViewControllers.ClientDossier
{
    public class AdresDetailsVC : MyNotifyPropertyChanged
    {
        /** PROPERTIES */
        private AdresBLLProvider _bll;
        public bewoner Bewoner { get; set; }

        public String Title { get; set; }
        public String TitleSelectedChildWindow
        {
            get
            {
                if (SelectedContactpersoon != null)
                {
                    String tempString = "";
                    if (SelectedContactpersoon.Type == null || !String.IsNullOrWhiteSpace(SelectedContactpersoon.Type.naam))
                        tempString += SelectedContactpersoon.Type.naam + ":";
                    if (!String.IsNullOrWhiteSpace(SelectedContactpersoon.voornaam))
                        tempString += " " + SelectedContactpersoon.voornaam;
                    if (!String.IsNullOrWhiteSpace(SelectedContactpersoon.naam))
                        tempString += " " + SelectedContactpersoon.naam;

                    return tempString;
                }
                return "";
            }
        }
        public ObservableCollection<country> LandList { get; set; }
        public List<String> GeslachtList { get; set; }        
        public AdresWeergave Adres { get; set; }
        private ContactpersoonWeergave _selectedContactpersoon;
        public ContactpersoonWeergave SelectedContactpersoon
        {
            get
            {
                return _selectedContactpersoon;
            }
            set
            {
                _selectedContactpersoon = value;

                NotifiyPropertyChanged(nameof(SelectedContactpersoon));
                NotifiyPropertyChanged(nameof(IsContactpersoonInfoVisible));
                NotifiyPropertyChanged(nameof(IsSelectedchildcontactpersoonEenPersoon));
                NotifiyPropertyChanged(nameof(TitleSelectedChildWindow));
            }
        }

        private bool _canEditAdres;
        public bool CanEditAdres
        {
            get
            {
                return _canEditAdres;
            }
            set
            {
                _canEditAdres = value;
                NotifiyPropertyChanged(nameof(CanEditAdres));
                NotifiyPropertyChanged(nameof(ShowEditButton));
            }
        }
        public bool ShowEditButton
        {
            get
            {
                return !_canEditAdres;
            }
        }
        public bool ShowAansluitingsnummer
        {
            get
            {
                if (Adres != null && Adres.Type.module_code == "CD")
                    return IsBedrijfInfoVisible;

                return false;
            }
        }
        public bool IsBedrijfInfoVisible
        {
            get
            {
                if (Adres != null)
                    return Adres.adrestype_id.HasValue && Adres.adrestype_id != 0;

                return false;
            }
        }
        public bool IsPersoonsInfoVisible
        {
            get
            {
                return !IsBedrijfInfoVisible;
            }
        }
        public bool CountryHasPostalList
        {
            get
            {
                if (Adres == null)
                    return false;
                return Adres.landcode == "BE";
            }
        }
        public bool IsContactpersoonInfoVisible { get; set; }
        public bool IsSelectedchildcontactpersoonEenPersoon
        {
            get
            {
                return SelectedContactpersoon != null && !SelectedContactpersoon.Type.isBedrijf;
            }
        }

        /** CONSTRUCTORS */
        public AdresDetailsVC()
        {
            GeslachtList = new List<string> { "Man", "Vrouw"};
            LandList = new ObservableCollection<country>(LandenBLL.GetAllCountries());
        }
        public AdresDetailsVC(bewoner bewoner): this()
        {
            Bewoner = bewoner;
            _bll = new BewonerAdresBLLProvider(Bewoner);

            NotifyAllProperties();
        }
        public AdresDetailsVC(gebruiker personeel) : this()
        {
           /* _bewoner = bewoner;
            _bll = new BewonerAdresBLLProvider(_bewoner);*/

            NotifyAllProperties();
        }
        public AdresDetailsVC(locatie locatie): this()
        {
            _bll = new CampusAdresBLLProvider();
            
            AdresWeergave campusAdresData = ((CampusAdresBLLProvider)_bll).GetAdresWeergaveFromLocatieid(locatie.id);
            if (campusAdresData != null)
                InsertSelectionIntoForm(campusAdresData);
            else
            {
                Adres = new AdresWeergave {
                    id = 0,
                    OwnerId = locatie.id,
                    naam = locatie.naam,
                    adrestype_id = 87,
                    Type = _bll.GetAdrestypeFromId(87),
                    landcode = "BE",
                    LandNaam = "België"
                };
                Title = Adres.Type.naam;
            }
            CanEditAdres = Adres.id == 0;

            NotifyAllProperties();
        }

        /** METHODS */
        private void NotifyAllProperties()
        {
            NotifiyPropertyChanged(nameof(Title));
            NotifiyPropertyChanged(nameof(LandList));
            NotifiyPropertyChanged(nameof(GeslachtList));

            NotifiyPropertyChanged(nameof(Adres));
            NotifiyPropertyChanged(nameof(SelectedContactpersoon));

            NotifiyPropertyChanged(nameof(IsBedrijfInfoVisible));
            NotifiyPropertyChanged(nameof(IsPersoonsInfoVisible));
            NotifiyPropertyChanged(nameof(ShowAansluitingsnummer));
            NotifiyPropertyChanged(nameof(CountryHasPostalList));
            NotifiyPropertyChanged(nameof(CanEditAdres));
            NotifiyPropertyChanged(nameof(IsContactpersoonInfoVisible));
            NotifiyPropertyChanged(nameof(IsSelectedchildcontactpersoonEenPersoon));
        }
        private void ValidateControls()
        {
            if (Adres.Type.isBedrijf && String.IsNullOrWhiteSpace(Adres.naam))
                throw new Exception("Gelieve de naam van het bedrijf/instelling/zelfstandige in te geven.");
            else if (!Adres.Type.isBedrijf)
            {
                if (String.IsNullOrWhiteSpace(Adres.voornaam))
                    throw new Exception("Gelieve de voornaam in te geven.");
                if (String.IsNullOrWhiteSpace(Adres.naam))
                    throw new Exception("Gelieve de familienaam in te geven.");
            }
            if (String.IsNullOrWhiteSpace(Adres.landcode))
                throw new Exception("Gelieve het land in te vullen voor deze locatie.");
        }
        public void InsertSelectionIntoForm(AdresWeergave adres)
        {
            Title = adres.Type.naam;                
            Adres = adres;
            CanEditAdres = Adres.id == 0;
            NotifyAllProperties();

            if (String.IsNullOrWhiteSpace(adres.landcode))
            {
                Adres.landcode = "BE";
                NotifiyPropertyChanged(nameof(Adres));
                NotifiyPropertyChanged(nameof(CountryHasPostalList));
            }
            if (adres.OwnerId == 0)
                adres.OwnerId = Bewoner.id;
        }
        public void SetPostalData(postal p)
        {
            Adres.postcode = p.postal1;
            Adres.gemeente = p.place;

            NotifiyPropertyChanged(nameof(Adres.postcode));
            NotifiyPropertyChanged(nameof(Adres.gemeente));
            NotifiyPropertyChanged(nameof(Adres));
        }
        public void SetBewonerData(bewoner b)
        {
            Adres.voornaam = b.voornaam;
            Adres.naam = b.achternaam;
            Adres.rijksregisternummer = b.rijksregisternr;
          
            NotifiyPropertyChanged(nameof(Adres.voornaam));
            NotifiyPropertyChanged(nameof(Adres.naam));
            NotifiyPropertyChanged(nameof(Adres.rijksregisternummer));
            NotifiyPropertyChanged(nameof(Adres));
        }
        public void AddBewonerAdrestype(adrestype adressubtype)
        {
            Adres.Contactpersonen.Add(new ContactpersoonWeergave()
            {
                id = 0,
                adrestype_id = adressubtype.id,
                Type = adressubtype
            });
        }
        public void DeleteContactpersoon()
        {
            if (SelectedContactpersoon != null)
            {
                if (SelectedContactpersoon.id != 0 && MessageBox.Show(@"Deze contactpersoon wordt mogelijks door verschillende bewoners gebruikt.
Ben je zeker dat je hem wil verwijderen?", "Bevestiging", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                    _bll.RemoveContactpersoonFromDB(SelectedContactpersoon);

                Adres.Contactpersonen.Remove(SelectedContactpersoon);
                SelectedContactpersoon = null;
            }
            else
                MessageBox.Show("Gelieve een contactpersoon uit te kiezen", "Info", MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
        }
        public void DeleteMyContactpersoon()
        {
            if (_bll is BewonerAdresBLLProvider bll)
            {
                SelectedContactpersoon.BewoneradresId = Adres.BewoneradresId;
                bll.RemoveBewoneradresContactpersoon(SelectedContactpersoon);
            }
        }
        public void Save()
        {
            ValidateControls();

            if (Adres.id == 0)
            {
                if (!Adres.active)
                    Adres.active = true;
                _bll.AddNewAdres(Adres);
            }
            else
                _bll.EditAdres(Adres);
        }
        public void ToggleVisibilityContactpersoonInfo()
        {
            IsContactpersoonInfoVisible = !IsContactpersoonInfoVisible;

            NotifiyPropertyChanged(nameof(IsContactpersoonInfoVisible));
        }
    }
}