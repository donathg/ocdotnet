﻿using ClaraFey.CommonObjects;
using ClaraFey.Windows.ClientDossier;
using ClaraFey.Windows.Common;
using DevExpress.Mvvm;
using DevExpress.Xpf.Grid;
using sharedcode.BLL;
using sharedcode.common;
using sharedcode.CommonObjects;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Security.AccessControl;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;

namespace ClaraFey.ViewControllers.Common
{
    public class UploadFileVC: ViewModelBase, IDisposable, INotifyPropertyChanged, IMyUserControl
    {
        /********** NIEUWE VERSIE **********/
        /** FIELDS */
        public new event PropertyChangedEventHandler PropertyChanged;
        public event BijlagenChangedHandler BijlagenChanged;
        public delegate void BijlagenChangedHandler(int aantalBijlagen);

        private BijlagenBLLManager _manager;
        private List<labels> _presetListLabels;
        private UploadFileWindowSettings _settings;
        private file_cats _category;
        private int _objectId;
        public AbstractBijlagenBLLProvider Provider;
        public GlobalMethods.Themes Theme;
        public ObservableCollection<FileItem> Items { get; set; }
        public ObservableCollection<FileItem> SelectedItems { get; set; }

        public int NumberOfItems { get; set; }
        public bool HasLabels { get; set; }
        public bool ShowCategory { get; set; }
        public bool AllowDelete
        {
            get
            {
                if (_settings != null && SelectedItems != null && SelectedItems.Count != 0)
                    return _settings.AllowDeleteFile;

                return false;
            }
        }
        public bool AllowEditing
        {
            get
            {
                if (_settings != null)
                    return _settings.AllowEditing;

                return false;
            }
        }
        public bool AllowOpenSaveEmail
        {
            get
            {
                return SelectedItems != null && SelectedItems.Count != 0;
            }
        }
        public bool AllowAddFile
        {
            get
            {
                if (_settings != null)
                    return _settings.AllowAddFile;

                return false;
            } 
        }

        public string Title { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public List<IMyUserControl> ChildrenUC { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public bool NeedSave { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        /** CONSTRUCTOR */
        //Constructor voor ViewModelMVVC naviagtion (bijv personeelsdossier)
        public UploadFileVC(AbstractBijlagenBLLProvider provider, UploadFileWindowSettings settings)
        {
            _settings = settings;
            Provider = provider;
            _manager = new BijlagenBLLManager(provider);
            HasLabels = provider is BijlagenBLLProviderClientdossier || provider is BijlagenBLLProviderClientdossierOvereenkomst;
            ShowCategory = provider.ShowCategory;
            SelectedItems = new ObservableCollection<FileItem>();
            SelectedItems.CollectionChanged += (sender, e) =>
            {
                if (e.Action == NotifyCollectionChangedAction.Add || e.Action == NotifyCollectionChangedAction.Remove)
                {
                    NotifiyPropertyChanged(nameof(AllowDelete));
                    NotifiyPropertyChanged(nameof(AllowOpenSaveEmail));
                }
            };

            NotifiyPropertyChanged(nameof(AllowDelete));
            NotifiyPropertyChanged(nameof(AllowEditing));
            NotifiyPropertyChanged(nameof(AllowAddFile));
            NotifiyPropertyChanged(nameof(AllowOpenSaveEmail));
            NotifiyPropertyChanged(nameof(HasLabels));
            NotifiyPropertyChanged(nameof(ShowCategory));
        }
        public UploadFileVC(AbstractBijlagenBLLProvider provider, int objectId, UploadFileWindowSettings settings): this (provider, settings)
        {
            List<labels> allUserLabels;
            if (Provider is BijlagenBLLProviderClientdossierOvereenkomst providerOvereenkomst)
                allUserLabels = new LabelProviderDataBase().Load(providerOvereenkomst.Overeenkomst);
            if (settings.PresetLabelList != null && settings.PresetLabelList.Count != 0)
            {
                allUserLabels = new List<labels>();

                foreach (labels label in settings.PresetLabelList)
                {
                    allUserLabels.Add(label);
                    allUserLabels.AddRange(LabelManager.GetChildLables(label.id));
                }
            }
            else
                allUserLabels = GlobalData.Instance.LoggedOnUser.LabelManager.GetAllActiveLabels();
            _presetListLabels = allUserLabels.Where(x => x.active == true && x.bijlagen == true && GlobalData.Instance.LoggedOnUser.LabelManager.GetAllActiveLabels().Exists(y => y.id == x.id)).ToList();
            _objectId = objectId;
            ReloadGrid();
        }
        public UploadFileVC(AbstractBijlagenBLLProvider provider, int objectId, UploadFileWindowSettings settings, string category) : this(provider, objectId, settings)
        {
            _category = _manager.GetFileCategoryFromCode(category);
        }

        /** METHODS */
        public void NotifiyPropertyChanged(string property)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(property));
        }
        private void ReloadGrid()
        {
            if (Provider is BijlagenBLLProviderClientdossier bllProvider)
                Items = new ObservableCollection<FileItem>(bllProvider.GetAllFilesFilteredOnOwnerIdAndLabellist(_objectId, _presetListLabels));
            else
                Items = new ObservableCollection<FileItem>(_manager.GetAllFilesFilteredOnOwnerIdAndCategory(_objectId, _category));
            NumberOfItems = Items.Count;

            // nodig???
            NotifiyPropertyChanged(nameof(Items));
        }
        public void Load()
        {
            ReloadGrid();
        }

        private String AskUserForSaveLocation(FileItem f)
        {
            SaveFileDialog d = new SaveFileDialog()
            {
                Filter = "All files (*.*)|*.*",
                Title = "Download",
                RestoreDirectory = true,
                CheckFileExists = false,
                CheckPathExists = false,
                SupportMultiDottedExtensions = true,
                AutoUpgradeEnabled = true,
                DefaultExt = Path.GetExtension(f.FileName),
                InitialDirectory = GlobalSharedMethods.GetDownloadPath(_settings.DownloadSubFolder),
                FileName = f.FileName
            };

            if (d.DefaultExt.Length > 0)
            {
                d.AddExtension = true;
                d.Filter = d.DefaultExt + " files (*." + d.DefaultExt + ")|*." + d.DefaultExt + "|" + d.Filter;
                d.FilterIndex = 0;
            }
            if (d.ShowDialog() == DialogResult.OK)
            {
                return d.FileName;
            }
            else
                return null;
        }
        public file_cats AskUserForFileCategory()
        {
            // Select the right file_cats bij overeenkomstbijlagen
            if (Provider is BijlagenBLLProviderClientdossierOvereenkomst bll)
                _category = bll.GetAllFileCats().Where(x => x.name.ToLower().Contains(bll.Overeenkomst.TypeText.ToLower())).FirstOrDefault();
            else if (Provider is BijlagenBLLProviderInventaris)
                _category = _manager.GetAllFileCatsFromModule().Find(x => x.code == "INVENTARIS");
            else if (Provider is BijlagenBLLProviderLocaties)
                _category = _manager.GetAllFileCatsFromModule().Find(x => x.code == "LOCATIE_INVENTARIS");

            if (_category != null)
                return _category;

            List<file_cats> list = _manager.GetAllFileCatsFromModule();

            if (list.Count == 0)
                return new file_cats();
            else if (list.Count == 1)
                return list[0];


            SMMWindow2Settings settings = new SMMWindow2Settings("Type bijlage", GlobalMethods.Themes.clientdossier) { IsChoiceWindow = true };

            SMMWindow2Level level0 = new SMMWindow2Level(0, "Type bijlage")
            {
                AllowDelete = false,
                AllowModify = false,
                AllowAdd = false,
                AllowSearch = true,
                AllowFilter = true
            };
            level0.AddColumns(
                new SMMWindow2Column() { FieldName = "name", ColumnHeader = "name", IsEditable = false }
            );

            smmWindowVC vc = new smmWindowVC();

            level0.DataSource = list;
            settings.AddLevel(level0);

            SMMWindow2 w = new SMMWindow2(vc, settings)
            {
                WindowStartupLocation = WindowStartupLocation.CenterScreen
            };

            if (w.ShowDialog().GetValueOrDefault())
            {
                return (file_cats)vc.SelectedItem0;
            }
            else
                return null;
        }
        private List<labels> AskUserForLabels()
        {
            if (HasLabels)
            {
                LabelsPopup labels = new LabelsPopup(new LabelConnectionProviderBijlages(GlobalData.Instance.LoggedOnUser, new LabelProviderDataBase().Load(), _presetListLabels));
                if (labels.ShowDialog().GetValueOrDefault())
                {
                    return labels.ConnectionManager.SelectedLabels;
                }
                else
                    throw new Exception("Je moet minstens 1 labels selecteren voor het bestand kan toevoegen!");
            }
            else
                return null;
        }
        private String AskUserForOvereenkomstStatus()
        {
            if (Provider is BijlagenBLLProviderClientdossierOvereenkomst bll)
            {
                OvereenkomstenBLL overeenkomstenBLL = new OvereenkomstenBLL();

                SMMWindow2Settings settings = new SMMWindow2Settings("Status Overeenkomst", GlobalMethods.Themes.clientdossier) { IsChoiceWindow = true };

                SMMWindow2Level level0 = new SMMWindow2Level(0, "Status Overeenkomst")
                {
                    AllowDelete = false,
                    AllowModify = false,
                    AllowAdd = false,
                    AllowSearch = true,
                    AllowFilter = true
                };
                level0.AddColumns(
                    new SMMWindow2Column() { FieldName = "naam", ColumnHeader = "naam", IsEditable = false }
                );
                level0.DataSource = overeenkomstenBLL.GetAllOvereenkomstStatussen();
                settings.AddLevel(level0);

                smmWindowVC vc = new smmWindowVC();
                SMMWindow2 w = new SMMWindow2(vc, settings)
                {
                    WindowStartupLocation = WindowStartupLocation.CenterScreen
                };

                if (w.ShowDialog().GetValueOrDefault())
                    return ((Overeenkomst_status)vc.SelectedItem0).naam;
                else
                    throw new Exception("Je hebt geen status geselecteerd! Dit is vereist");
            }
            return null;
        }

        public void LabelsEdit(Image image)
        {
            FileItem fileItem = (image.DataContext as EditGridCellData).RowData.Row as FileItem;
            
            LabelsPopup labels = new LabelsPopup(
                fileItem.Id,
                new LabelConnectionProviderBijlages(GlobalData.Instance.LoggedOnUser, new LabelProviderDataBase().Load(),
                _presetListLabels)
            );
            if (labels.ShowDialog().GetValueOrDefault())
            {
                labels.ConnectionManager.SaveSelectedLabels(null);
                //SelectedItem.NotifiyPropertyChanged(nameof(SelectedItem.LabelsKommaSeperated));
            }
        }
        public void UploadFiles(List<String> fileNames, file_cats category)
        {
            try
            {
                List<labels> labelList;

                if (_presetListLabels != null && _presetListLabels.Count == 1)
                    labelList = _presetListLabels;
                else
                    labelList = AskUserForLabels();

                List<SaveBijlageData> bijlageDataList = new List<SaveBijlageData>();
                foreach (String filename in fileNames)
                {
                    SaveBijlageData bijlageData = new SaveBijlageData
                    {
                        Category = category,
                        LabelList = labelList,
                        ObjectId = _objectId,
                        FileName = filename,
                        Descr = AskUserForOvereenkomstStatus()
                    };

                    bijlageDataList.Add(bijlageData);
                }
                _manager.UploadFiles(bijlageDataList);

                ReloadGrid();
                BijlagenChanged?.Invoke(Items.Count);
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.Message, "Fout", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        /// <summary>
        /// UploadFile-method + selection of category & files
        /// </summary>
        public void UploadFilesWithSelection()
        {
            if (AskUserForFileCategory() is file_cats category)
            {
                OpenFileDialog d = new OpenFileDialog()
                {
                    Multiselect = true
                };

                if (d.ShowDialog() == DialogResult.OK)
                {
                    UploadFiles(d.FileNames.ToList(), category);
                }
            }
        }

        public void UploadExcel()
        {
            try
            {
                if (Provider is BijlagenBLLProviderClientdossier)
                    Theme = GlobalMethods.Themes.clientdossier;

                RichExcelWindow d = new RichExcelWindow(null, false, Theme);

                if (d.ShowDialog().GetValueOrDefault())
                {
                    SaveBijlageData bijlageData = new SaveBijlageData
                    {
                        FileData = d.Data,
                        Category = AskUserForFileCategory(),
                        LabelList = AskUserForLabels(),
                        ObjectId = _objectId,
                        BijlageType = BijlageType.internalExcel
                    };
                    _manager.UploadSaveBijlageData(bijlageData);

                    ReloadGrid();
                }
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.Message, "fout", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        public void UploadNote()
        {
            try
            {
                if (Provider is BijlagenBLLProviderClientdossier)
                    Theme = GlobalMethods.Themes.clientdossier;

                RichEditWindow d = new RichEditWindow(null, false, Theme);

                if (d.ShowDialog().GetValueOrDefault())
                {
                    SaveBijlageData bijlageData = new SaveBijlageData
                    {
                        FileData = d.Data,
                        Category = AskUserForFileCategory(),
                        LabelList = AskUserForLabels(),
                        ObjectId = _objectId,
                         BijlageType = BijlageType.internalWord
                    };
                    _manager.UploadSaveBijlageData(bijlageData);

                    ReloadGrid();
                }
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.Message, "fout", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        public void EditFileItemDescription(FileItem file)
        {
            _manager.SaveFileItemDescription(file);
        }
        public void SaveSelectedFileOnComputer()
        {
            try
            {
                if (this.SelectedItems == null || this.SelectedItems.Count==0)
                    throw new Exception("Je hebt nog geen bestand geselecteerd uit de lijst!\nSelecteer eerst een bestand en druk dan op download bestand");

                foreach (FileItem f in this.SelectedItems)
                {
                  

                    String filename = AskUserForSaveLocation(f);
                    if (!String.IsNullOrWhiteSpace(filename))
                    {
                        _manager.DownloadFile(f, filename);

                        OpenSelectedFile();
                    }
                }
            }
            catch (Exception e)
            {
                System.Windows.MessageBox.Show(e.Message, "Foutmelding", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
        }
        public void OpenSelectedFile()
        {
            try
            {
                if (this.SelectedItems == null || this.SelectedItems.Count == 0)
                    throw new Exception("Je hebt nog geen bestand geselecteerd uit de lijst!\nSelecteer eerst een bestand en druk dan op download bestand");
                foreach (FileItem file in this.SelectedItems)
                {
                    string temperaryFolder = @"C:/Users/" + Environment.UserName;
                    if (!Directory.Exists(temperaryFolder))
                        Directory.CreateDirectory(temperaryFolder);
                    temperaryFolder += "/ClaraFeyTool_temp";
                    if (!Directory.Exists(temperaryFolder))
                        Directory.CreateDirectory(temperaryFolder);

                    if (file == null)
                        throw new Exception("Je hebt nog geen bestand geselecteerd uit de lijst!\nSelecteer eerst een bestand en druk dan op open bestand");

                    if (System.Windows.MessageBox.Show("Wilt u het bestand " + file.FileName + " openen ?", "Openen", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                    {

                        if (Provider is BijlagenBLLProviderClientdossier)
                            Theme = GlobalMethods.Themes.clientdossier;

                

                        Byte[] fileData = this.Provider.GetFileDataFromFileItem(file);
                        file.FileData = fileData;

                        if (file.FileName.EndsWith(".rtf") || file.FileName.EndsWith(".dtrtf") )
                        {
                            RichEditWindow d = new RichEditWindow(file.FileData, file.Creator != GlobalData.Instance.LoggedOnUser.Id.GetValueOrDefault(), Theme);

                            if (d.ShowDialog().GetValueOrDefault())
                            {
                                file.FileData = d.Data;

                                String fileName = GlobalSharedMethods.GetDownloadPath("") + @"\" + file.FileName + "." + file.FileExtension;
                                FileStream objFileStream = new FileStream(fileName, FileMode.Create, FileAccess.Write);
                                objFileStream.Write(file.FileData, 0, file.FileData.Length);
                                objFileStream.Close();

                                _manager.UpdateFileData(file);
                            }
                        }
                        else  
                             if (  file.FileName.EndsWith(".dtxls"))
                            {
                                RichExcelWindow d = new RichExcelWindow(file.FileData, file.Creator != GlobalData.Instance.LoggedOnUser.Id.GetValueOrDefault(), Theme);

                                if (d.ShowDialog().GetValueOrDefault())
                                {
                                    file.FileData = d.Data;

                                    String fileName = GlobalSharedMethods.GetDownloadPath("") + @"\" + file.FileName + "." + file.FileExtension;
                                    FileStream objFileStream = new FileStream(fileName, FileMode.Create, FileAccess.Write);
                                    objFileStream.Write(file.FileData, 0, file.FileData.Length);
                                    objFileStream.Close();

                                    _manager.UpdateFileData(file);
                                }
                            }
                            else
                        {
                            try
                            {
                                _manager.DownloadFile(file, temperaryFolder + "/" + file.FileName + "." + file.FileExtension);

                                System.Diagnostics.Process.Start(temperaryFolder + "/" + file.FileName + "." + file.FileExtension);
                            }
                            catch (Exception)
                            {
                                throw new Exception("Het openen van dit type bestand is niet ondersteund!");
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.Message, "Foutmelding", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
        }
        
        public void EmailSelectedFile()
        {
            try
            { 
                String temperaryFolder = @"C:/Users/" + Environment.UserName;
                if (!Directory.Exists(temperaryFolder))
                    Directory.CreateDirectory(temperaryFolder);
                temperaryFolder += "/ClaraFeyTool_temp";
                if (!Directory.Exists(temperaryFolder))
                    Directory.CreateDirectory(temperaryFolder);

                if (this.SelectedItems == null || this.SelectedItems.Count==0)
                    throw new Exception("Je hebt nog geen bestand geselecteerd uit de lijst!\nSelecteer eerst een bestand en druk dan op open bestand");
                List<String> filenames = new List<string>();
                foreach (FileItem f in this.SelectedItems)
                {
                    Byte[] fileData = this.Provider.GetFileDataFromFileItem(f);
                    f.FileData = fileData;
                    _manager.DownloadFile(f, temperaryFolder + "/" + f.FileName);
                    String filename = temperaryFolder + "/" + f.FileName;
                    filenames.Add(filename);
                }
                EmailBLL.SendEmailViaOutlook(null, String.Empty, String.Empty, filenames);

            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.Message, "Foutmelding", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
        }
        public void DeleteSelectedFile()
        {

            if (this.SelectedItems == null || this.SelectedItems.Count == 0)
                throw new Exception("Je hebt nog geen bestand geselecteerd uit de lijst!");
            if (_settings.AllowDeleteFile == false)
                return;

            String question = String.Format("Zeker dat u {0} bestand(en) wenst te verwijderen ?", SelectedItems.Count);
            if (System.Windows.Forms.MessageBox.Show(question, "Verwijderen", MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes)
                return;

            foreach (FileItem f in this.SelectedItems)
            {
                    try
                    {
                        _manager.DeleteBijlage(f);
             
                    }
                    catch (Exception ex)
                    {
                        System.Windows.MessageBox.Show(ex.Message, "fout", MessageBoxButton.OK, MessageBoxImage.Error);
                    }

             
            }
           
            ReloadGrid();
            BijlagenChanged?.Invoke(Items.Count);
        }
        public void Load(int? id)
        {
            _objectId = id.GetValueOrDefault();
            if (_presetListLabels == null || _presetListLabels.Count == 0)
                _presetListLabels = GlobalData.Instance.LoggedOnUser.LabelManager.GetAllActiveLabels();
         
            ReloadGrid();

            NotifiyPropertyChanged(nameof(AllowDelete));
            NotifiyPropertyChanged(nameof(AllowEditing));
            NotifiyPropertyChanged(nameof(AllowAddFile));
            NotifiyPropertyChanged(nameof(AllowOpenSaveEmail));
            NotifiyPropertyChanged(nameof(HasLabels));
            NotifiyPropertyChanged(nameof(ShowCategory));
        }
        public void Save()
        {
            // Clear selected item. Otherwise when reopening the filemanager, it thinks there is still an item selected (fe. see trashcan)
            SelectedItems.Clear();
        }
        public void RegisterChildUCs(IMyUserControl uc)
        {
       
        }
        public void Dispose()
        {
          
        }
    }
}
