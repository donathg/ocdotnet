﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClaraFey.ViewControllers.Common
{
    public interface IMyUserControl
    {
        String Title { get; set; }
        void Load(int? id);
        void Save();
        List<IMyUserControl> ChildrenUC { get; set; }
        void RegisterChildUCs(IMyUserControl uc);
        bool NeedSave { get; set; }
    }
}
