﻿using ClaraFey.CommonObjects;
using ClaraFey.ViewControllers.ClientDossier;
using ClaraFey.ViewControllers.Common;
using DevExpress.Mvvm;
using sharedcode.BLL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClaraFey.ViewControllers
{
    public class smmWindowVC : ViewModelBase, INotifyPropertyChanged, IDisposable , IMyUserControl
    {
        public new event PropertyChangedEventHandler PropertyChanged;

        public List<object> SelectedItem { get; set; }
        public bool ButtonOKEnabled
        {
            get
            {
                if (Settings == null)
                    return false;
                if (Settings.IsChoiceWindow == false)
                    return false;
                if (Settings.LevelCount == 2)
                    return SelectedItem[1] != null;
                if (Settings.LevelCount == 1)
                    return SelectedItem[0] != null;
                return false;
            }
        }
        public object SelectedItem0
        {

            get
            {
                if (SelectedItem != null)
                    return SelectedItem[0];
                else return null;
            }
            set
            {
                if (value != SelectedItem[0])
                {
                    SelectedItem[0] = value;
                    NotifiyPropertyChanged("SelectedItem0");
                    NotifiyPropertyChanged("SelectedItem");
                    NotifiyPropertyChanged("ButtonOKEnabled");
                }
            }
        }
        public object SelectedItem1
        {

            get
            {
                return SelectedItem[1];
            }
            set
            {
                if (value != SelectedItem[1])
                {
                    SelectedItem[1] = value;
                    NotifiyPropertyChanged("SelectedItem1");
                    NotifiyPropertyChanged("SelectedItem");
                    NotifiyPropertyChanged("ButtonOKEnabled");
                }
            }
        }
        public bool NeedSave { get; set; }
        public SMMWindow2Settings Settings { get; set; }
        public Entities Entity { get; set; }
        public string Title
        {
            get
            {
                return "SMM";
            }
            set { }
        }
        public List<IMyUserControl> ChildrenUC { get; set; }
        
        public smmWindowVC()
        {
            Entity = new Entities(sharedcode.common.GlobalData.Instance.Entity);
            SelectedItem = new List<object>
            {
                //voor de  max 2 niveaus
                null,
                null
            };
            this.Settings = Settings;
        }

        void NotifiyPropertyChanged(string property)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(property));
        }
        public void Dispose()
        {
            if (Entity != null)
            {
                Entity.Dispose();
                Entity = null;
            }
        }
        public void Load(int? id)
        {
           
        }
        public void Save()
        {
            
        }
        public void RegisterChildUCs(IMyUserControl uc)
        {
            throw new NotImplementedException();
        }
    }
    public class SMMLabels : smmWindowVC { }
    public class smmGroepenLabels: smmWindowVC { }
    public class smmRechtenGroepenFucnties : smmWindowVC { }
    public class SmmContactverslagSoortBegeleiding : smmWindowVC { }
    public class SmmAdrestypesManager : smmWindowVC { }
    public class smmAanvinklijsten: smmWindowVC { }
    public class smmBeeldvormingSetting: smmWindowVC { }
    public class smmToelatingen: smmWindowVC { }
    public class smmWindowVCDashboard : smmWindowVC { }
    public class AIMutatues: smmWindowVC { }
}
