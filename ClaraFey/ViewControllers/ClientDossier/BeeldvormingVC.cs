﻿using DevExpress.Mvvm;
using sharedcode.BLL;
using sharedcode.common;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using DevExpress.CodeParser;
using DevExpress.Xpf.Grid.Printing;
using ClaraFey.ViewControllers.Common;
using sharedcode.Caches;
using System.Diagnostics;

namespace ClaraFey.ViewControllers.ClientDossier
{
    public class MyBeeldvormingtree : beeldvormingtree, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public bool CommentAvailable { get; set; }
        public bool IsClosed { get; set; }
        public bool IsChecked4Print { get; set; } = true;

        private void NotifiyPropertyChanged(string property)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(property));
        }
    }

    public class BeeldvormingVC : ViewModelBase, IDisposable, INotifyPropertyChanged, IMyUserControl
    {
        /** FIELDS */
        public new event PropertyChangedEventHandler PropertyChanged;

        private List<IMyUserControl> ChildrenUC { get; set; }
        List<IMyUserControl> IMyUserControl.ChildrenUC { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public bool NeedSave { get; set; }
        public int BewonerId { get; set; }
        public String Title
        {
            get { return "BeeldvormingList"; }
            set { }
        }
        public String OldVersionText
        {
            get
            {
                if (isOldVersionSelected)
                    return "oude versie " + this.SelectedBeeldvormingHeader.closingdate.GetValueOrDefault().ToString("dd'/'MM'/'yyyy");
                return String.Empty;
            }
        }
        public int LatestVersion
        {
            get
            {
                if (CurrentBeeldvormingHeader is beeldvormingheader)
                    return CurrentBeeldvormingHeader.version;

                return 1;
            }
        }
        public bool CanMakeNewVersion
        {
            get
            {
                if (GlobalData.Instance.LoggedOnUser.HasAccess("1503"))
                    return IsCurrentHeaderVersionClosed;

                return false;
            }
        }
        public bool CanViewVertrouwelijkeInfo
        {
            get
            {
                return GlobalData.Instance.LoggedOnUser.HasAccess("1504");
            }
        }
        public bool CanEditVertrouwelijkeInfo
        {
            get
            {
                return GlobalData.Instance.LoggedOnUser.HasAccess("1505");
            }
        }
        private bool _isSelectedItemClosed;
        public bool IsSelectedItemClosed
        {
            get { return _isSelectedItemClosed; }
            set
            {
                _isSelectedItemClosed = value;

                NotifiyPropertyChanged(nameof(IsSelectedItemReadOnly));
            }
        }
        /// <summary>
        /// Is het mogelijk de beeldvorming treeItem in de popup te opnenen of te sluiten
        /// </summary>
        public bool CanCloseOrOpenSelectedItem
        {
            get
            {
                bool hasAccess = false;

                if (SelectedTreeItem != null)
                    {
                if (SelectedTreeItem.BeeldvormingtreeMainType is BeeldvormingtreeMainType.anamnese)
                    hasAccess = GlobalData.Instance.LoggedOnUser.HasAccess("1512");
                else if (SelectedTreeItem.BeeldvormingtreeMainType is BeeldvormingtreeMainType.diagnostiek)
                    hasAccess = GlobalData.Instance.LoggedOnUser.HasAccess("1522");
                else if (SelectedTreeItem.BeeldvormingtreeMainType is BeeldvormingtreeMainType.sociaal_emotioneel_functioneren)
                    hasAccess = GlobalData.Instance.LoggedOnUser.HasAccess("1532");
                else if (SelectedTreeItem.BeeldvormingtreeMainType is BeeldvormingtreeMainType.functioneringsprofiel)
                    hasAccess = GlobalData.Instance.LoggedOnUser.HasAccess("1542");

                return hasAccess && !IsCurrentHeaderVersionClosed && isLatestVersionSelected;
                }
                return false;
            }
        }
        public bool CanPrintBeeldVorming
        {
            get
            {
                if (GlobalData.Instance.LoggedOnUser.HasAccess("1502") == false)
                    return false;
                return true;
            }
        }
        public bool isOldVersionSelected
        {
            get { return !isLatestVersionSelected; }
        }
        public bool isLatestVersionSelected
        {
            get
            {
                if (SelectedBeeldvormingHeader is beeldvormingheader)
                    return CurrentBeeldvormingHeader == SelectedBeeldvormingHeader;

                return false;
            }
        }
        public bool IsSelectedItemReadOnly
        {
            get
            {
                if (SelectedTreeItem is MyBeeldvormingtree)
                {
                    bool hasAccess = false;

                    if (SelectedTreeItem.BeeldvormingtreeMainType == BeeldvormingtreeMainType.anamnese)
                        hasAccess = !GlobalData.Instance.LoggedOnUser.HasAccess("1511");
                    else if (SelectedTreeItem.BeeldvormingtreeMainType == BeeldvormingtreeMainType.diagnostiek)
                        hasAccess = !GlobalData.Instance.LoggedOnUser.HasAccess("1521");
                    else if (SelectedTreeItem.BeeldvormingtreeMainType == BeeldvormingtreeMainType.sociaal_emotioneel_functioneren)
                        hasAccess = !GlobalData.Instance.LoggedOnUser.HasAccess("1531");
                    else if (SelectedTreeItem.BeeldvormingtreeMainType == BeeldvormingtreeMainType.functioneringsprofiel)
                        hasAccess = !GlobalData.Instance.LoggedOnUser.HasAccess("1541");

                    return hasAccess || !isLatestVersionSelected || IsSelectedItemClosed;
                }
                else
                    return true;
            }
        }
        public bool CanEditDatesAndAttendees
        {
            get
            {
                return isLatestVersionSelected && !IsCurrentHeaderVersionClosed;
            }
        }
        public bool IsCurrentHeaderVersionClosed
        {
            get
            {
                return CurrentBeeldvormingHeader.closingdate.HasValue;
            }
        }
        /// <summary>
        /// Heeft de ingelogde persoon rechten om de volledige beeldvorming te openen o te sluiten
        /// </summary>
        public bool IsAllowedToOpenOrCloseCurrentHeaderVersion
        {
            get
            {
                return GlobalData.Instance.LoggedOnUser.HasAccess("1501");
            }
        }
        /// <summary>
        /// Zijn alle beeldvormingen van de aktuele versie gesloten
        /// </summary>
        /// <returns></returns>
        public bool IsAllCurrentVersionClosed()
        {
            foreach (MyBeeldvormingtree t in BeeldVormingTree.Where(x => x.isEditable))
            {
                if (!(GetCurrentBeeldvormingTreeItem(t.id) is beeldvorming bv && bv.closed))
                    return false;
            }
            return true;
        }
        public bewoner Bewoner { get; set; }
        public List<beeldvorming> BeeldvormingList { get; set; }
        public List<beeldvormingtree> Tree { get; set; }
        public string TreePath
        {
            get
            {
                if (SelectedTreeItem is MyBeeldvormingtree)
                    return BeeldvormingBLL.GetTreePathString(SelectedTreeItem);

                return "?";
            }
        }
        public ObservableCollection<beeldvormingheader> BeeldvormingHeaders { get; set; }
        public beeldvormingheader CurrentBeeldvormingHeader
        {
            get
            {
                int maxVersion = -1;
                beeldvormingheader heighest = null;
                foreach (beeldvormingheader h in BeeldvormingHeaders)
                {
                    if (h.version > maxVersion)
                    {
                        maxVersion = h.version;
                        heighest = h;
                    }
                }
                return heighest;
            }
        }
        private beeldvormingheader _selectedBeeldvormingHeader;
        public beeldvormingheader SelectedBeeldvormingHeader
        {
            get
            {
                return _selectedBeeldvormingHeader;
            }
            set
            {
                _selectedBeeldvormingHeader = value;
                RefreshSelectedBeeldvormingInfo();

                NotifiyPropertyChanged(nameof(SelectedBeeldvormingHeader));
                NotifiyPropertyChanged(nameof(IsSelectedItemReadOnly));
            }
        }
        public ObservableCollection<MyBeeldvormingtree> BeeldVormingTree { get; set; }
        private MyBeeldvormingtree _selectedTreeItem;
        public MyBeeldvormingtree SelectedTreeItem
        {
            get
            {
                return _selectedTreeItem;
            }
            set
            {
                _selectedTreeItem = value;
                RefreshSelectedBeeldvormingInfo();

                NotifiyPropertyChanged(nameof(TreePath));
            }
        }
        public string A { get; set; }
        public string B { get; set; }
        public string V { get; set; }
        public string O { get; set; }

        /** CONSTRUCTORS */
        public BeeldvormingVC()
        {

        }

        /** METHODS */
        public void NotifiyPropertyChanged(string property)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(property));
        }
        public BeeldvormingReport CreateBeeldvormingReport(BeeldvormingReportPrintTypes printType, bool vertrouwelijkeInfo)
        {
            return BeeldvormingBLL.CreateBeeldvormingReport(this.Bewoner, this.SelectedBeeldvormingHeader, this.GetCheck4PrintBeeldVormingTree(), this.GetCheck4PrintBeeldVormingen(), printType, vertrouwelijkeInfo);
        }
        public void RegisterChildUCs(IMyUserControl uc)
        {
            if (ChildrenUC == null)
                ChildrenUC = new List<IMyUserControl>();
            uc.Load(this.BewonerId);
            ChildrenUC.Add(uc);
        }
        public void Save()
        {
            SaveDatesAndAttendees();
            if (ChildrenUC != null && ChildrenUC.Count > 0)
            {
                foreach (IMyUserControl c in ChildrenUC)
                    c.Save();
            }
        }
        public void SaveDatesAndAttendees()
        {
            BeeldvormingBLL.SaveDatesAndAttendees(this.BewonerId, SelectedBeeldvormingHeader);
        }
        public void CreateNewVersion(DateTime ExpirationDate)
        {
            if (!CanMakeNewVersion)
                throw new Exception("er is een fout opgetreden");
            try
            {
                BeeldvormingBLL.CreateNewVersion(this.BewonerId, ExpirationDate);

                Load(this.BewonerId);
                NotifiyPropertyChanged(nameof(CanMakeNewVersion));
                NotifiyPropertyChanged(nameof(CanEditDatesAndAttendees));
            }
            catch (Exception)
            {
                throw;
            }
        }
        public void CloseOpenCurrentVersion()
        {
            if (!IsAllowedToOpenOrCloseCurrentHeaderVersion)
                throw new Exception("U heeft geen rechten.");

            beeldvormingheader bvh;
            if (IsCurrentHeaderVersionClosed)
                bvh = ReopenCurrentVersion();
            else
                bvh = CloseCurrentVersion();

            GlobalSharedMethods.CopyPublicProperties(bvh, this.CurrentBeeldvormingHeader);
            NotifiyPropertyChanged("CanMakeNewVersion");
            NotifiyPropertyChanged("CanEditDatesAndAttendees");

        }
        private beeldvormingheader CloseCurrentVersion()
        {
            return BeeldvormingBLL.CloseCurrentVersion(this.BewonerId, this.LatestVersion, GlobalData.Instance.LoggedOnUser.Id.GetValueOrDefault());
        }
        private beeldvormingheader ReopenCurrentVersion()
        {
            return BeeldvormingBLL.ReopenCurrentVersion(this.BewonerId, this.LatestVersion, GlobalData.Instance.LoggedOnUser.Id.GetValueOrDefault());
        }
        public void SaveBeeldvorming()
        {
            if (!isLatestVersionSelected || IsCurrentHeaderVersionClosed)
                return;

            beeldvorming modItem = BeeldvormingBLL.SaveBeeldvorming(this.BewonerId, SelectedTreeItem.id, this.SelectedBeeldvormingHeader.version, this.A, this.B, this.V, this.O, this.IsSelectedItemClosed);

            if (GetBeeldvorming(BewonerId, SelectedTreeItem.id, SelectedBeeldvormingHeader.version) is beeldvorming existingItem)
                GlobalSharedMethods.CopyPublicProperties(modItem, existingItem);
            else
                BeeldvormingList.Add(modItem);

            UpdateExtraInfoToMyTreeItem(SelectedTreeItem);

            NotifiyPropertyChanged(nameof(BeeldvormingList));
            NotifiyPropertyChanged(nameof(CanEditDatesAndAttendees));
        }
        public List<beeldvorming> GetCheck4PrintBeeldVormingen()
        {
            return BeeldvormingList.Where(x => x.version == SelectedBeeldvormingHeader.version).ToList();
        }
        public List<beeldvormingtree> GetCheck4PrintBeeldVormingTree()
        {
            List<beeldvormingtree> list = new List<beeldvormingtree>();
            foreach (MyBeeldvormingtree t in BeeldVormingTree)
            {
                if (t.IsChecked4Print)
                    list.Add((beeldvormingtree)t);
            }
            return list;
        }
        public beeldvorming GetCurrentBeeldvormingTreeItem(int treeId)
        {
            return BeeldvormingList.Where(x => x.bewonerid == this.BewonerId && x.treeid == treeId && x.version == this.LatestVersion).FirstOrDefault();
        }
        public beeldvorming GetCurrentBeeldvorming()
        {
            if (SelectedTreeItem is MyBeeldvormingtree)
                return BeeldvormingList.Where(x => x.bewonerid == this.BewonerId && x.treeid == this.SelectedTreeItem.id && x.version == this.LatestVersion).FirstOrDefault();

            return new beeldvorming();
        }
        /// <summary>
        /// Geeft de volledig beeldvorming terug van 1 treeItem en versie
        /// </summary>
        /// <param name="bewonerId"></param>
        /// <param name="treeId"></param>
        /// <param name="version"></param>
        /// <returns></returns>
        public beeldvorming GetBeeldvorming(int bewonerId, int treeId, int version)
        {
            return BeeldvormingList.Where(x => x.bewonerid == bewonerId && x.treeid == treeId && x.version == version).FirstOrDefault();
        }
        public void Load(int? id)
        {
            BewonerId = id.GetValueOrDefault();
            Bewoner = BewonersListCache.Instance.GetBewoner(id.GetValueOrDefault());
            BeeldvormingHeaders = new ObservableCollection<beeldvormingheader>(BeeldvormingBLL.LoadBeeldVormingHeaders(BewonerId));

            SelectLatestHeader();
            BeeldvormingList = BeeldvormingBLL.LoadBeeldVorming(BewonerId);
            LoadTree();

            NotifiyPropertyChanged(nameof(BeeldvormingHeaders));
            NotifiyPropertyChanged(nameof(IsCurrentHeaderVersionClosed));
        }
        public void LoadTree()
        {
            BeeldVormingTree = new ObservableCollection<MyBeeldvormingtree>();

            foreach (beeldvormingtree t in BeeldvormingBLL.BeeldvormingTreeList)
            {
                if (
                    (t.BeeldvormingtreeMainType == BeeldvormingtreeMainType.anamnese && GlobalData.Instance.LoggedOnUser.HasAccess("1510")) || 
                    (t.BeeldvormingtreeMainType == BeeldvormingtreeMainType.diagnostiek && GlobalData.Instance.LoggedOnUser.HasAccess("1520")) ||
                    (t.BeeldvormingtreeMainType == BeeldvormingtreeMainType.sociaal_emotioneel_functioneren && GlobalData.Instance.LoggedOnUser.HasAccess("1530")) ||
                    (t.BeeldvormingtreeMainType == BeeldvormingtreeMainType.functioneringsprofiel && GlobalData.Instance.LoggedOnUser.HasAccess("1540"))
                   )
                {
                    MyBeeldvormingtree bvt = new MyBeeldvormingtree();
                    GlobalSharedMethods.CopyPublicProperties(t, bvt);
                    bvt.BeeldvormingtreeMainType = t.BeeldvormingtreeMainType;
                    bvt.Children = new List<object>();
                    foreach (Object c in t.Children)
                    {
                        MyBeeldvormingtree bvtChild = new MyBeeldvormingtree();
                        GlobalSharedMethods.CopyPublicProperties(c as beeldvormingtree, bvtChild);
                        bvt.Children.Add(bvtChild);
                    }

                    UpdateExtraInfoToMyTreeItem(bvt);
                    BeeldVormingTree.Add(bvt);
                }
            }
        }
        private void UpdateExtraInfoToMyTreeItem(MyBeeldvormingtree mb)
        {
            if (GetCurrentBeeldvormingTreeItem(mb.id) is beeldvorming bv)
            {
                mb.IsClosed = bv.closed;
                mb.CommentAvailable = !String.IsNullOrWhiteSpace(bv.content_o);
            }

            NotifiyPropertyChanged(nameof(MyBeeldvormingtree.IsClosed));
            NotifiyPropertyChanged(nameof(MyBeeldvormingtree.CommentAvailable));
        }
        public void SelectLatestHeader()
        {
            _selectedBeeldvormingHeader = CurrentBeeldvormingHeader;
            RefreshSelectedBeeldvormingInfo();

            NotifiyPropertyChanged(nameof(SelectedBeeldvormingHeader));
        }
        public void RefreshSelectedBeeldvormingInfo()
        {
            if (SelectedTreeItem is MyBeeldvormingtree && SelectedBeeldvormingHeader is beeldvormingheader)
            {
                if (GetBeeldvorming(this.BewonerId, this.SelectedTreeItem.id, this.SelectedBeeldvormingHeader.version) is beeldvorming v)
                {
                    A = v.content_a;
                    B = v.content_b;
                    O = v.content_o;
                    V = v.content_v;
                    _isSelectedItemClosed = v.closed;
                }
                else
                {
                    A = String.Empty;
                    B = String.Empty;
                    O = String.Empty;
                    V = String.Empty;
                    _isSelectedItemClosed = false;
                }
            }
            NotifiyPropertyChanged(nameof(A));
            NotifiyPropertyChanged(nameof(B));
            NotifiyPropertyChanged(nameof(V));
            NotifiyPropertyChanged(nameof(O));
            NotifiyPropertyChanged(nameof(MyBeeldvormingtree.IsClosed));
            NotifiyPropertyChanged(nameof(isLatestVersionSelected));
            NotifiyPropertyChanged(nameof(IsSelectedItemReadOnly));
            NotifiyPropertyChanged(nameof(CanCloseOrOpenSelectedItem));
            NotifiyPropertyChanged(nameof(IsSelectedItemClosed));
            NotifiyPropertyChanged(nameof(isOldVersionSelected));
            NotifiyPropertyChanged(nameof(OldVersionText));
            NotifiyPropertyChanged(nameof(BeeldVormingTree));
            NotifiyPropertyChanged(nameof(SelectedTreeItem)); 
            NotifiyPropertyChanged(nameof(CanEditDatesAndAttendees));
        } 
        public void Dispose()
        {
        }
    }
}
