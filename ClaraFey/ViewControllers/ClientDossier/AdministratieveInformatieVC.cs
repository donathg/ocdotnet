﻿using ClaraFey.ViewControllers.Common;
using DevExpress.Mvvm;
using sharedcode.BLL;
using sharedcode.Caches;
using sharedcode.WeergaveClasses;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ClaraFey.ViewControllers.ClientDossier
{
    public enum AdministratieveInformatieTab
    {
        gepland_begeleidingsteam_occf = 0,
        algemene_gegevens = 1,
        zorgvraag = 2,
        betrokken_context = 3,
        juridische_gegevens = 4,
        financiele_gegevens = 5,
        betrokken_netwerk = 6,
    }

    public class BegeleidingsteamChoiceItem
    {

        public int? GebruikerId { get; set; }
        public String GroepCode { get; set; }
        public String Description { get; set; }
        public String Voornaam { get; set; }
        public String Achternaam { get; set; }

        public String VoornaamAchternaam { get; set; }
        public String AchternaamVoornaam { get; set; }
    }


    public class AdministratieveInformatieVC : ViewModelBase, INotifyPropertyChanged, IMyUserControl
    {
        /** FIELDS */
        public new event PropertyChangedEventHandler PropertyChanged;

        public ObservableCollection<gepland_begeleidingsteam> OrthosSelectedList
        {
            get
            {
                return new ObservableCollection<gepland_begeleidingsteam>(AllGeplandeBegeleidingsteam.Where(x => x.begeleidingsteamtypeid == 1).ToList());
            }
        }
        public ObservableCollection<gepland_begeleidingsteam> IndividueleAandachtsopvoederSelectedList
        {
            get
            {
                return new ObservableCollection<gepland_begeleidingsteam>(AllGeplandeBegeleidingsteam.Where(x => x.begeleidingsteamtypeid == 2).ToList());
            }
        }
        public ObservableCollection<gepland_begeleidingsteam> MaatschappelijkwerkerSelectedList
        {
            get
            {
                return new ObservableCollection<gepland_begeleidingsteam>(AllGeplandeBegeleidingsteam.Where(x => x.begeleidingsteamtypeid == 3).ToList());
            }
        }
        public ObservableCollection<gepland_begeleidingsteam> MobielebegeleiderSelectedList
        {
            get
            {
                return new ObservableCollection<gepland_begeleidingsteam>(AllGeplandeBegeleidingsteam.Where(x => x.begeleidingsteamtypeid == 4).ToList());
            }
        }
        public ObservableCollection<gepland_begeleidingsteam> VerpleegkundigeSelectedList
        {
            get
            {
                return new ObservableCollection<gepland_begeleidingsteam>(AllGeplandeBegeleidingsteam.Where(x => x.begeleidingsteamtypeid == 5).ToList());
            }
        }
        public ObservableCollection<gepland_begeleidingsteam> KinesistSelectedList
        {
            get
            {
                return new ObservableCollection<gepland_begeleidingsteam>(AllGeplandeBegeleidingsteam.Where(x => x.begeleidingsteamtypeid == 6).ToList());
            }
        }
        public ObservableCollection<gepland_begeleidingsteam> PsychotherapeutSelectedList
        {
            get
            {
                return new ObservableCollection<gepland_begeleidingsteam>(AllGeplandeBegeleidingsteam.Where(x => x.begeleidingsteamtypeid == 7).ToList());
            }
        }
        public ObservableCollection<gepland_begeleidingsteam> KinderpsychiaterSelectedList
        {
            get
            {
                return new ObservableCollection<gepland_begeleidingsteam>(AllGeplandeBegeleidingsteam.Where(x => x.begeleidingsteamtypeid == 9).ToList());
            }
        }
        public ObservableCollection<gepland_begeleidingsteam> LogopedistSelectedList
        {
            get
            {
                return new ObservableCollection<gepland_begeleidingsteam>(AllGeplandeBegeleidingsteam.Where(x => x.begeleidingsteamtypeid == 10).ToList());
            }
        }
        public ObservableCollection<gepland_begeleidingsteam> DagbestedersSelectedList
        {
            get
            {
                return new ObservableCollection<gepland_begeleidingsteam>(AllGeplandeBegeleidingsteam.Where(x => x.begeleidingsteamtypeid == 8).ToList());
            }
        }
        public ObservableCollection<gepland_begeleidingsteam> GroepenSelectedList
        {
            get
            {
                return new ObservableCollection<gepland_begeleidingsteam>(AllGeplandeBegeleidingsteam.Where(x => x.begeleidingsteamtypeid == 11).ToList());
            }
        }

        public List<BegeleidingsteamChoiceItem> AllIndividueleAandachtsopvoeders
        {
            get
            {
                return ConvertToBegeleidingsteamChoiceItem(Gebruikers).ToList();
            }
        }
        public List<BegeleidingsteamChoiceItem> AllMaatschappelijkWerkers
        {
            get
            {
                return ConvertToBegeleidingsteamChoiceItem(Gebruikers).ToList();
            }
        }
        public List<BegeleidingsteamChoiceItem> AllOrthos
        {
            get
            {
                return ConvertToBegeleidingsteamChoiceItem(Gebruikers).ToList();
            }
        }
        public List<BegeleidingsteamChoiceItem> AllMobielebegeleiders
        {
            get
            {
                return ConvertToBegeleidingsteamChoiceItem(Gebruikers).ToList();
            }
        }
        public List<BegeleidingsteamChoiceItem> AllVerpleegkundige
        {
            get
            {
                return ConvertToBegeleidingsteamChoiceItem(Gebruikers).ToList();
            }
        }
        public List<BegeleidingsteamChoiceItem> AllKinesist
        {
            get
            {
                return ConvertToBegeleidingsteamChoiceItem(Gebruikers).ToList();
            }
        }
        public List<BegeleidingsteamChoiceItem> AllLogopedist
        {
            get
            {
                return ConvertToBegeleidingsteamChoiceItem(Gebruikers).ToList();
            }
        }
        public List<BegeleidingsteamChoiceItem> AllPsychotherapeut
        {
            get
            {
                return ConvertToBegeleidingsteamChoiceItem(Gebruikers).ToList();
            }
        }
        public List<BegeleidingsteamChoiceItem> AllKinderpsychiaters
        {
            get
            {
                return ConvertToBegeleidingsteamChoiceItem(Gebruikers).ToList();
            }
        }
        public List<BegeleidingsteamChoiceItem> AllDagbesteders
        {
            get
            {
                return ConvertToBegeleidingsteamChoiceItem(Gebruikers).ToList();
            }
        }
        public List<BegeleidingsteamChoiceItem> AllGroepen
        {
            get
            {
                return ConvertToBegeleidingsteamChoiceItem(Groepen.ToList());
            }
        }
        

        public String Title
        {
            get { return "Overeenkomsten"; }
        }
        public int BewonerId { get; set; }
        public bewoner Bewoner { get; set; }
        public bewoner selectedBewoner;
        public locatie SelectedLocatie { get; set; }
        public bool NeedSave { get; set; }
        private List<IMyUserControl> ChildrenUC { get; set; }
        public cd_administratieveinfo cd_administratieveinfo { get; set; }
        private List<vw_campussen> _campussen;
        public List<vw_campussen> Campussen
        {
            get
            {
                if (_campussen == null)
                    _campussen = vw_campussen.GetCampussen();
                return _campussen;
            }
            set
            {
                _campussen = value;
            }
        }
        private List<vw_gebruikergroep> _gebruikers;
        public List<vw_gebruikergroep> Gebruikers
        {
            get
            {
                if (_gebruikers == null)
                    _gebruikers = GebruikerBLL.GetGebruikers();
                return _gebruikers;
            }

            set
            {
                _gebruikers = value;
            }
        }
        private List<rechten_groep> _groepen;
        public List<rechten_groep> Groepen
        {
            get
            {
                if (_groepen == null)
                    _groepen = RechtenBLL.GetGroepen(false);
                return _groepen;
            }

            set
            {
                _groepen = value;
            }
        }
        public List<gepland_begeleidingsteam> AllGeplandeBegeleidingsteam { get; set; }
        List<IMyUserControl> IMyUserControl.ChildrenUC
        {
            get => throw new NotImplementedException();
            set => throw new NotImplementedException();
        }
        string IMyUserControl.Title
        {
            get => throw new NotImplementedException();
            set => throw new NotImplementedException();
        }

        /** CONSTRUCTOR FOR KEEPING SELECTEDBEWONER */
        public AdministratieveInformatieVC(bewoner livein)
        {
            this.selectedBewoner = livein;
        }

        /** METHODS */
        private void NotifiyPropertyChanged(string property)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(property));
        }
        private void BegeleidingsTeamReload()
        {
            AllGeplandeBegeleidingsteam = AdministratieveInformatieBLL.LoadGeplandeBegeleidingsTeams(Bewoner.id);

            NotifiyPropertyChanged(nameof(OrthosSelectedList));
            NotifiyPropertyChanged(nameof(IndividueleAandachtsopvoederSelectedList));
            NotifiyPropertyChanged(nameof(MaatschappelijkwerkerSelectedList));
            NotifiyPropertyChanged(nameof(MobielebegeleiderSelectedList));
            NotifiyPropertyChanged(nameof(VerpleegkundigeSelectedList));
            NotifiyPropertyChanged(nameof(KinesistSelectedList));
            NotifiyPropertyChanged(nameof(PsychotherapeutSelectedList));
            NotifiyPropertyChanged(nameof(KinderpsychiaterSelectedList));
            NotifiyPropertyChanged(nameof(LogopedistSelectedList));
            NotifiyPropertyChanged(nameof(DagbestedersSelectedList));
            NotifiyPropertyChanged(nameof(GroepenSelectedList));
        }
        private List<BegeleidingsteamChoiceItem> ConvertToBegeleidingsteamChoiceItem(List<vw_gebruikergroep> gebList)
        {
            List<BegeleidingsteamChoiceItem> itemList = new List<BegeleidingsteamChoiceItem>();
            foreach (vw_gebruikergroep groep in gebList)
            {
                itemList.Add(new BegeleidingsteamChoiceItem()
                {
                    GebruikerId = groep.id,
                    Description = groep.VoornaamAchternaam,
                    Voornaam = groep.Voornaam,
                    Achternaam = groep.Achternaam,
                    AchternaamVoornaam = groep.AchternaamVoornaam,
                    VoornaamAchternaam = groep.VoornaamAchternaam
                });
            }
            return itemList;
        }
        private List<BegeleidingsteamChoiceItem> ConvertToBegeleidingsteamChoiceItem(List<rechten_groep> groepList)
        {
            List<BegeleidingsteamChoiceItem> itemList = new List<BegeleidingsteamChoiceItem>();
            foreach (rechten_groep groep in groepList)
            {
                itemList.Add(new BegeleidingsteamChoiceItem()
                {
                    GroepCode = groep.groep_code,
                    Description = groep.groep_naam
                });
            }
            return itemList;
        }
        public void AddBegeleidingsTeamEntry(BegeleidingsteamChoiceItem choice, int typeId)
        {
            if (choice.GebruikerId.HasValue)
            {
                if (AllGeplandeBegeleidingsteam.Where(x => x.begeleidingsteamtypeid == typeId && x.gebruikerid == choice.GebruikerId).Count() == 0)
                    AdministratieveInformatieBLL.AddBegeleidingsTeamEntry(Bewoner.id, choice.GebruikerId.GetValueOrDefault(), typeId);
                else
                    throw new Exception("Deze persoon is reeds toegevoegd.");
            }
            else
            {
                if (AllGeplandeBegeleidingsteam.Where(x => x.begeleidingsteamtypeid == typeId && x.groepcode == choice.GroepCode).Count() == 0)
                    AdministratieveInformatieBLL.AddBegeleidingsTeamEntry(Bewoner.id, choice.GroepCode, typeId);
                else
                    throw new Exception("Deze groep is reeds toegevoegd.");
            }
            BegeleidingsTeamReload();
        }
        public void RemoveBegeleidingsTeamEntry(gepland_begeleidingsteam item)
        {
            AdministratieveInformatieBLL.RemoveBegeleidingsTeamEntry(item.id);
            BegeleidingsTeamReload();
        }
        public void RegisterChildUCs(IMyUserControl uc)
        {
            if (ChildrenUC == null)
                ChildrenUC = new List<IMyUserControl>();
            uc.Load(this.BewonerId);
            ChildrenUC.Add(uc);
        }
        public void Save()
        {
            cd_administratieveinfo = AdministratieveInformatieBLL.Save(cd_administratieveinfo);
            if (ChildrenUC != null && ChildrenUC.Count > 0)
            {
                foreach (IMyUserControl c in ChildrenUC)
                {
                    c.Save();
                }
            }
        }
        public void Load(int? id)
        {
            BewonerId = id.GetValueOrDefault();
            cd_administratieveinfo = AdministratieveInformatieBLL.GetData(this.BewonerId);
            Bewoner = BewonersListCache.Instance.GetBewoner(id.GetValueOrDefault());

            if (cd_administratieveinfo == null)
            {
                cd_administratieveinfo = new cd_administratieveinfo()
                {
                    bewonerid = id.GetValueOrDefault()
                };
            }
            BegeleidingsTeamReload();
        }
    }
}
