﻿using ClaraFey.CommonObjects;
using sharedcode.BLL;
using sharedcode.common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClaraFey.ViewControllers.ClientDossier
{

    public class LabelsChecked : labels
    {
     
       public bool IsChecked { get; set; } 
    }
    public class LabelsVC
    {
       
        public List<labels> SelectedLabels { get
            {
                return this.AllUserLabels.Where(x => x.IsChecked == true).ToList<labels>();
            }
        }
        public List<LabelsChecked> AllExistingLabels { get; set; }
        public List<LabelsChecked> AllUserLabels { get; set; }

        public void SetAllExistingLabels(List<labels> allExistingLabels)
        {
            this.AllExistingLabels = new List<LabelsChecked>();
            foreach (labels l in allExistingLabels)
            {
                LabelsChecked lc = new LabelsChecked();
                GlobalSharedMethods.CopyPublicProperties(l, lc);
                lc.IsChecked = false;
                this.AllExistingLabels.Add(lc);
            }
        }
        public void SetAllUSerLabels(List<labels> allUserLabels)
        {
            this.AllUserLabels = new List<LabelsChecked>();
            foreach (labels l in allUserLabels)
            {
                LabelsChecked lc = new LabelsChecked();
                GlobalSharedMethods.CopyPublicProperties(l, lc);
                lc.IsChecked = false;
                this.AllUserLabels.Add(lc);
            }
        }
        public void SetSelectedLabels(List<labels> labelsToSelect)
        {
            foreach (labels l in labelsToSelect)
            {
                LabelsChecked al = AllUserLabels.Where(x => x.id == l.id).FirstOrDefault();
                if (al != null)
                 al.IsChecked = true;

         

            }

        }

    }
}
