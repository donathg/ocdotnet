﻿
using ClaraFey.CommonObjects;
using DevExpress.Mvvm;
using DevExpress.Xpf.NavBar;
using sharedcode.BLL;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using sharedcode.common;
using sharedcode.CommonObjects;
using ClaraFey.ViewControllers.Common;
using ClaraFey.Windows.Common;
using sharedcode.Caches;

namespace ClaraFey.ViewControllers.ClientDossier
{
    
    public class ClientDossierNavigationVC : ViewModelBase, INotifyPropertyChanged
    {

        public String WindowTitle { get; set; }

        /** FIELDS */
        public delegate void SaveErrorHandler(object sender, string errorMessage);
        public event SaveErrorHandler OnSaveError;
        public new event PropertyChangedEventHandler PropertyChanged;

        public ICommand SaveCommand { get; set; }
        public List<bewoner> BewonersList { get; set; }
        public List<NavBarListItem> NavBarList { get; set; }
        public List<NavBarListItem> NavBarListGeneral { get; set; }
        
        public bool NavbarIsLocked { get; set; }
        private bewoner _selectedBewoner;
        public bewoner SelectedBewoner
        {
            get
            {
                return _selectedBewoner;
            }
            set
            {
                if (_selectedBewoner != value)
                {
                    if (!this.Save())
                        return;
                    _selectedBewoner = value;
                    if (_selectedBewoner != null)
                        SelectNavBarItem = NavBarList[0];//InfoFiche
                    CheckMenuItems();
                 //   BewonerAdresBLLProvider.FillCache(_selectedBewoner);
                    NotifiyPropertyChanged(nameof(SelectedBewoner));
                    NotifiyPropertyChanged(nameof(HasBewoner));
             
                }
              
            }
        }
        public bool HasBewoner
        {
            get
            {
                return SelectedBewoner == null;
            }
        }
        public void SelectBewoner (int bewonderId)
        {
            SelectedBewoner = BewonersList.Where(x => x.id == bewonderId).FirstOrDefault();
            NotifiyPropertyChanged(nameof(SelectedBewoner)); 
        }

        private NavBarListItem _selectNavBarItem;
        public NavBarListItem SelectNavBarItem
        {
            get
            {
                return _selectNavBarItem;
            }
            set
            {
                _selectNavBarItem = value;
                if (_selectNavBarItem != null)
                {
                    if (_selectNavBarItem.GroupName == "Algemeen")
                    {
                        SelectedBewoner = null;
                        NotifiyPropertyChanged(nameof(SelectedBewoner));
                        CheckMenuItems();
                    }
                    if (_selectNavBarItem.ViewModelBase is GebruikerLabelsDashboardVC)
                    {
                        LabelProviderEmpty provider = new LabelProviderEmpty();
                        if (_selectNavBarItem.Tag is List<labels> list)
                        {
                            provider.InjectLabels(list);

                            LabelManager manager = new LabelManager(provider);
                            (_selectNavBarItem.ViewModelBase as GebruikerLabelsDashboardVC).Load(SelectedBewoner.id, manager);
                        }
                    }
                }
                GlobalMethods.WriteToLog("ClientDossierNavigationVC SelectNavBarItem Load B : "  + _selectNavBarItem.ItemName);
                Load();
                GlobalMethods.WriteToLog("ClientDossierNavigationVC SelectNavBarItem Load E : "  + _selectNavBarItem.ItemName);

                NotifiyPropertyChanged(nameof(NavbarIsLocked));
                NotifiyPropertyChanged(nameof(SelectNavBarItem));
            }
        }
        public bool IsSystemAdmin
        {
            get
            {
                if (GlobalData.Instance.LoggedOnUser.IsLoggedOn == false)
                    return false;
                return false;
            }
        }

        /** CONSTRUCTOR */
        public ClientDossierNavigationVC()
        {
            GlobalMethods.WriteToLog("ClientDossierNavigationVC");
            OvereenkomstenBLL overeenkomstenBLL = new OvereenkomstenBLL();
            OvereenkomstenSettings nietGetekendeOvereenkomstenSettings = new OvereenkomstenSettings()
            {
                Statussen = overeenkomstenBLL.GetAllOvereenkomstStatussen().Where(x => x.naam.ToLower().Contains("niet getekend")).ToList()
            };

            NavBarListGeneral = new List<NavBarListItem>()
            {
                new NavBarListItem() {Index = 0, Tag="Dagboek", GroupName="Algemeen", ItemName="Dagboek", ViewModelBase = new DagboekVC(GlobalData.Instance.LoggedOnUser.LabelManager,false), IsVisible = true, IsEnabled = GlobalData.Instance.LoggedOnUser.HasAccess("1000"), BewonerIndependent=true}
            };

            UploadFileWindowSettings settings = new UploadFileWindowSettings("Bijlages", "clientdossier")
            {
                AllowAddFile = true,
                AllowDeleteFile = true,
                AllowEditing = GlobalData.Instance.LoggedOnUser.GetAccesType("1001") == AccessType.access
            };
            UploadFileVC bijlagenVC = new UploadFileVC(
                new BijlagenBLLProviderClientdossier(),
                settings
            );

            int i = 1;
            NavBarList = new List<NavBarListItem>()
            {

                new NavBarListItem() {Index = i++, Tag="Infofiche", GroupName="Cliënt", ItemName="Infofiche", ViewModelBase = new InfoFicheVC(), IsVisible = GlobalData.Instance.LoggedOnUser.HasAccess("1000"), IsEnabled=false},
                new NavBarListItem() {Index = i++, Tag="DagboekClient", GroupName="Cliënt", ItemName="Dagboek cliënt", ViewModelBase = new DagboekVC(GlobalData.Instance.LoggedOnUser.LabelManager,true), IsVisible = GlobalData.Instance.LoggedOnUser.HasAccess("1000"), IsEnabled=false},
                new NavBarListItem() {Index = i++, Tag="BewonerOvereenkomsten", GroupName="Cliënt", ItemName="Overeenkomsten", ViewModelBase = new OvereenkomstenVC(), IsVisible = GlobalData.Instance.LoggedOnUser.HasAccess("1300"), IsEnabled=false},
                new NavBarListItem() {Index = i++, Tag="BewonerAdministratieveInformatie", GroupName="Cliënt", ItemName="Administratieve informatie", ViewModelBase = new AdministratieveInformatieVC(SelectedBewoner), IsVisible=GlobalData.Instance.LoggedOnUser.HasAccess("1000"), IsEnabled=false},
                new NavBarListItem() {Index = i++, Tag="BewonerBeeldvorming", GroupName="Cliënt", ItemName="Beeldvorming", ViewModelBase = new BeeldvormingVC(),IsVisible = GlobalData.Instance.LoggedOnUser.HasAccess("1500"), IsEnabled=false},
                new NavBarListItem() {Index = i++, Tag="Actieplan", GroupName="Cliënt", ItemName="Actieplan" , ViewModelBase = new ActiePlanVC(), IsVisible = GlobalData.Instance.LoggedOnUser.HasAccess("1800"), IsEnabled=false},
               // new NavBarListItem() {Index = i++, Tag="Bijlagemanager", GroupName="Cliënt", ItemName="Bijlagemanager", ViewModelBase = bijlagenVC, IsVisible=GlobalData.Instance.LoggedOnUser.HasAccess("1000"), IsEnabled=false},
                new NavBarListItem() {Index = i++, Tag="OverzichtenNietGettekendeOvereenkomsten", GroupName="Overzichten & Rapportages", ItemName = "Niet getekende overeenkomsten" , ViewModelBase = new OvereenkomstenPartVC(nietGetekendeOvereenkomstenSettings), IsVisible=GlobalData.Instance.LoggedOnUser.HasAccess("1001"), IsEnabled=true, BewonerIndependent = true },
                new NavBarListItem() {Index = i++, Tag="Aanvinklijsten", GroupName="SMM", ItemName="Aanvinklijsten", ViewModelBase = new smmAanvinklijsten(), IsVisible = GlobalData.Instance.LoggedOnUser.HasAccess("1001"), IsEnabled=true, BewonerIndependent=true},
                new NavBarListItem() {Index = i++, Tag="Toelatingen", GroupName="SMM", ItemName = "Toelatingen", ViewModelBase = new smmToelatingen(), IsVisible = GlobalData.Instance.LoggedOnUser.HasAccess("1001"), IsEnabled = true, BewonerIndependent=true},
                new NavBarListItem() {Index = i++, Tag="Beeldvorming - Settings", GroupName="SMM", ItemName = "Beeldvorming - Settings", ViewModelBase = new smmBeeldvormingSetting(), IsVisible =GlobalData.Instance.LoggedOnUser.HasAccess("1001"), IsEnabled = true, BewonerIndependent=true},
                new NavBarListItem() {Index = i++, Tag="Labels", GroupName="SMM", ItemName="Labels" , ViewModelBase = new SMMLabels(), IsVisible = GlobalData.Instance.LoggedOnUser.HasAccess("1001"), IsEnabled=true, BewonerIndependent=true},
                new NavBarListItem() {Index = i++, Tag="GroepenLabels", GroupName="SMM", ItemName="GroepenLabels" , ViewModelBase = new smmGroepenLabels(), IsVisible =GlobalData.Instance.LoggedOnUser.HasAccess("1001"), IsEnabled=true, BewonerIndependent=true},
                new NavBarListItem() {Index = i++, Tag="Rechten - GroepenFuncties", GroupName="SMM", ItemName="Rechten - GroepenFuncties" , ViewModelBase = new smmRechtenGroepenFucnties(), IsVisible = GlobalData.Instance.LoggedOnUser.HasAccess("1001"), IsEnabled=true, BewonerIndependent=true},
                new NavBarListItem() {Index = i++, Tag="Contactverslag - Soorten Begeleiding", GroupName="SMM", ItemName="Contactverslag - Soorten Begeleiding" , ViewModelBase = new SmmContactverslagSoortBegeleiding(), IsVisible = GlobalData.Instance.LoggedOnUser.HasAccess("1001"), IsEnabled=true, BewonerIndependent=true},
                new NavBarListItem() {Index = i++, Tag="Adrestypes manager", GroupName="SMM", ItemName="Adrestypes manager" , ViewModelBase = new SmmAdrestypesManager(), IsVisible = GlobalData.Instance.LoggedOnUser.HasAccess("1001"), IsEnabled=true, BewonerIndependent=true}
            };

            AddLabelNavbarItem("Context-Sociaal", i++, new List<int>() { 3, 25, 64, 69, 70, 57 });
            AddLabelNavbarItem("Dagbesteding", i++, new List<int>() { 9, 30 });
            AddLabelNavbarItem("Medisch", i++, new List<int>() { 4, 43 });
            AddLabelNavbarItem("Psycho Pedagogisch", i++, new List<int>() { 43, 53, 85 });
            AddLabelNavbarItem("Verblijf-Dagopvang", i++, new List<int>() { 30, 53, 85, 64, 69, 57, 70, 71 });
            AddLabelNavbarItem("Externe verslagen", i++, new List<int>() { 81, 57, 70 });

            NavBarList.Add(new NavBarListItem() { Index = i++, Tag = "Bijlagemanager", GroupName = "Cliënt", ItemName = "Bijlagemanager", ViewModelBase = bijlagenVC, IsVisible = GlobalData.Instance.LoggedOnUser.HasAccess("1000"), IsEnabled = false }         );

            LoadBewonerList();
            SaveCommand = new DelegateCommand<NavBarItemSelectingEventArgs>(SaveC);
            this.SelectNavBarItem = NavBarListGeneral[0];
        }

        /** EVENTS */
        void SaveC(NavBarItemSelectingEventArgs args)
        {
            GlobalMethods.WriteToLog("ClientDossierNavigationVC SaveC");
            args.Cancel = !this.Save();
        }
        /** METHODS */
        private void AddLabelNavbarItem(string NavbarItemText, int index, List<int> parentLabels)
        {
            List<labels> listLabels = new List<labels>();
            listLabels.AddRange(GlobalData.Instance.LoggedOnUser.LabelManager.GetAllActiveLabels().Where(x => parentLabels.Contains(x.parentid.GetValueOrDefault()) || parentLabels.Contains(x.id)));
            if (listLabels.Count > 0)
            {
                NavBarListItem item = new NavBarListItem()
                {
                    Index = index,
                    GroupName = "Cliënt",
                    ItemName = NavbarItemText,
                    Tag = listLabels.Where(x => x.parentid == null).ToList(),
                    IsVisible = GlobalData.Instance.LoggedOnUser.HasAccess("1000"),
                    IsEnabled = false,
                    ViewModelBase = new GebruikerLabelsDashboardVC(true)
                };
                NavBarList.Add(item);
            }
        }
        public void LoadBewonerList()
        {
            BewonersList = BewonersListCache.Instance.GetMyBewoners(GlobalData.Instance.LoggedOnUser.Id.GetValueOrDefault());
            NotifiyPropertyChanged(nameof(BewonersList));
        }
        private void CheckMenuItems()
        {
            foreach (NavBarListItem i in this.NavBarList)
            {
                if (i.BewonerIndependent)
                    i.IsEnabled = true;
                else
                    i.IsEnabled = SelectedBewoner != null;

                if (i.Tag.ToString() == "BewonerBegeleidingen")
                {
                    if (SelectedBewoner != null)
                        i.ItemName = "Begeleidingen van " + SelectedBewoner.VoornaamAchternaam;
                    else
                        i.ItemName = "Begeleidingen";
                }
            }
            NotifiyPropertyChanged(nameof(NavBarList));
        }
        private void NotifiyPropertyChanged(string property)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(property));
        }
        private void ThrowError(string errorMessage)
        {
            if (OnSaveError == null)
                return;
            OnSaveError(this, errorMessage);
        }
        public bool Save()
        {
            try
            {
                GlobalMethods.WriteToLog("ClientDossierNavigationVC BEGIN");
                if (_selectNavBarItem != null && _selectNavBarItem.ViewModelBase is IMyUserControl/* && this.SelectedBewoner != null*/)
                    ((IMyUserControl)_selectNavBarItem.ViewModelBase).Save();

                GlobalMethods.WriteToLog("ClientDossierNavigationVC TRUE");
                return true;
            }
            catch (Exception ex)
            {
                ThrowError(ex.Message);
                return false;
            }
        }
        public void Load()
        {
            GlobalMethods.WriteToLog("ClientDossierNavigationVC Load Start");
            if (_selectNavBarItem != null && _selectNavBarItem.ViewModelBase is IMyUserControl)
            {
                InitSMM();

                int? bewonerId = null;
                if (this.SelectedBewoner != null)
                    bewonerId = this.SelectedBewoner.id;
                ((IMyUserControl)_selectNavBarItem.ViewModelBase).Load(bewonerId);
            }
            GlobalMethods.WriteToLog("ClientDossierNavigationVC Load End");
        }
        private void InitSMM()
        {
            if (_selectNavBarItem.ViewModelBase is smmWindowVCDashboard && this.SelectedBewoner != null)
            {
                //SMMWindow2Settings settings = new SMMWindow2Settings("Dashboard", GlobalMethods.Themes.clientdossier);
                //SMMWindow2Level level0 = new SMMWindow2Level(0, "Dashboard") { CanExportToExcel = false, AllowDelete = false, AllowModify = false, AllowAdd = false, AllowSearch = true, AllowFilter = true };
                //level0.AddColumns(new SMMWindow2Column() { FieldName = "id", ColumnHeader = "Id", IsEditable = false, Visible = false }, new SMMWindow2Column() { FieldName = "category", ColumnHeader = "category", IsEditable = false }, new SMMWindow2Column() { FieldName = "titel", ColumnHeader = "titel", IsEditable = false }
                //        , new SMMWindow2Column() { FieldName = "bericht", ColumnHeader = "bericht", IsEditable = false }
                //        , new SMMWindow2Column() { FieldName = "creationdate", ColumnHeader = "datum", IsEditable = false });
                //smmWindowVC vc = ((smmWindowVC)_selectNavBarItem.ViewModelBase);
                //var someDbSet = vc.Entity.Set<vw_cd_dashboard>().SqlQuery("select * from vw_cd_dashboard where bewonerid = " + this.SelectedBewoner.id + " order by creationdate desc").ToList();
                //level0.DataSource = someDbSet;
                //settings.AddLevel(level0);
                //vc.Settings = settings;
            }
            else if (_selectNavBarItem.ViewModelBase is SMMLabels)
            {
                SMMWindow2Settings settings = new SMMWindow2Settings("Labels", GlobalMethods.Themes.clientdossier);


                SMMWindow2Level level0 = new SMMWindow2Level(0, "Labels") { CanExportToExcel = true, AllowDelete = false, AllowModify = true, AllowAdd = true, AllowSearch = true, AllowFilter = true };
                level0.AddColumns(
                    new SMMWindow2Column() { FieldName = "id", ColumnHeader = "Id", IsEditable = false, Visible = false, IsNullable = false },
                    new SMMWindow2Column() { FieldName = "parentid", ColumnHeader = "parentId", Visible = false, DefaultValue = null, IsEditable = false, IsNullable = true },
                    new SMMWindow2Column() { FieldName = "name", ColumnHeader = "Labelnaam", IsEditable = true, IsNullable = false },
                    new SMMWindow2Column() { FieldName = "descr", ColumnHeader = "Beschrijving", IsEditable = true, IsNullable = true },
                    new SMMWindow2Column() { FieldName = "module", ColumnHeader = "Module", IsEditable = false, IsNullable = false, Visible = false, DefaultValue = "CD" },
                    new SMMWindow2Column() { FieldName = "active", ColumnHeader = "Actief?", IsEditable = true, IsNullable = false },
                    new SMMWindow2Column() { FieldName = "dagboek", ColumnHeader = "dagboek", IsEditable = true, IsNullable = false },
                    new SMMWindow2Column() { FieldName = "bijlagen", ColumnHeader = "bijlagen", IsEditable = true, IsNullable = false },
                    new SMMWindow2Column() { FieldName = "contactverslagen", ColumnHeader = "contactverslagen", IsEditable = true, IsNullable = false },
                    new SMMWindow2Column() { FieldName = "deadlinedate", ColumnHeader = "deadlinedate", IsEditable = true, IsNullable = false }
                );
                level0.JoinField = "id";

                SMMWindow2Level level1 = new SMMWindow2Level(0, "SubLabels") { CanExportToExcel = true, AllowDelete = false, AllowModify = true, AllowAdd = true, AllowSearch = true, AllowFilter = true };
                level1.AddColumns(
                    new SMMWindow2Column() { FieldName = "id", ColumnHeader = "Id", IsEditable = false, Visible = false, IsNullable = false },
                    new SMMWindow2Column() { FieldName = "parentid", ColumnHeader = "parentId", Visible = false, DefaultValue = null, IsEditable = false, IsNullable = true },
                    new SMMWindow2Column() { FieldName = "name", ColumnHeader = "Labelnaam", IsEditable = true, IsNullable = false },
                    new SMMWindow2Column() { FieldName = "descr", ColumnHeader = "Beschrijving", IsEditable = true, IsNullable = true },
                    new SMMWindow2Column() { FieldName = "module", ColumnHeader = "Module", IsEditable = false, IsNullable = false, Visible = false, DefaultValue = "CD" },
                    new SMMWindow2Column() { FieldName = "active", ColumnHeader = "Aktief ?", IsEditable = true, IsNullable = false },
                    new SMMWindow2Column() { FieldName = "dagboek", ColumnHeader = "dagboek", IsEditable = true, IsNullable = false },
                    new SMMWindow2Column() { FieldName = "bijlagen", ColumnHeader = "bijlagen", IsEditable = true, IsNullable = false },
                    new SMMWindow2Column() { FieldName = "contactverslagen", ColumnHeader = "contactverslagen", IsEditable = true, IsNullable = false },
                    new SMMWindow2Column() { FieldName = "deadlinedate", ColumnHeader = "deadlinedate", IsEditable = true, IsNullable = false }
                );
                level1.JoinField = "parentid";

                smmWindowVC vc = ((smmWindowVC)_selectNavBarItem.ViewModelBase);
                vc.Entity.Configuration.LazyLoadingEnabled = false;

                var someDbSet = new ObservableCollection<labels>(vc.Entity.labels.Where(x => x.module == "CD" && x.parentid.HasValue == false).OrderBy(x => x.name).ToList());
                level0.DataSource = someDbSet;
                settings.AddLevel(level0);

                var someDbSetChild = new ObservableCollection<labels>(vc.Entity.labels.Where(x => x.module == "CD" && x.parentid.HasValue).OrderBy(x => x.name).ToList());
                level1.DataSource = someDbSetChild;
                settings.AddLevel(level1);

                vc.Settings = settings;
            }
            else if (_selectNavBarItem.ViewModelBase is smmGroepenLabels)
            {
                SMMWindow2Settings settings = new SMMWindow2Settings("Groepen Labels", GlobalMethods.Themes.clientdossier);

                SMMWindow2Level level0 = new SMMWindow2Level(0, "Groepen") { CanExportToExcel = true, AllowDelete = false, AllowModify = false, AllowAdd = false, AllowSearch = true, AllowFilter = true };
                level0.AddColumns(
                    new SMMWindow2Column() { FieldName = "groep_code", ColumnHeader = "groep_code", IsEditable = false },
                    new SMMWindow2Column() { FieldName = "groep_naam", ColumnHeader = "groep_naam", IsEditable = false },
                    new SMMWindow2Column() { FieldName = "groep_orbis", ColumnHeader = "groep_orbis", IsEditable = false, Visible = false },
                    new SMMWindow2Column() { FieldName = "groep_email_werkopdrachten", ColumnHeader = "groep_email_werkopdrachten", IsEditable = false, Visible = false }
                    );
                level0.JoinField = "groep_code";

                SMMWindow2Level level1 = new SMMWindow2Level(1, "GroepenLabels") { CanExportToExcel = true, AllowDelete = true, AllowModify = true, AllowAdd = true, AllowSearch = true, AllowFilter = true };

                List<SMMWindow2ComboItem> cmbLabels = new List<SMMWindow2ComboItem>();
                using (Entities entity = new Entities(sharedcode.common.GlobalData.Instance.Entity))
                {
                    var labels = entity.labels.ToList();
                    foreach (labels l in labels)
                        cmbLabels.Add(new SMMWindow2ComboItem() { Id = l.id, Name = l.name });
                }
                cmbLabels = cmbLabels.OrderBy(x => x.Name).ToList();

                level1.AddColumns(
                    new SMMWindow2Column() { FieldName = "id", ColumnHeader = "Id", IsEditable = false, Visible = false },
                    new SMMWindow2Column() { FieldName = "groepcode", ColumnHeader = "Groepcode", IsEditable = false, Visible = false },
                    new SMMWindow2Column() { FieldName = "labelid", ColumnHeader = "Label", ComboboxItems = cmbLabels, IsEditable = true }
                );
                level1.JoinField = "groepcode";


                smmWindowVC vc = ((smmWindowVC)_selectNavBarItem.ViewModelBase);

                var someDbSet = new ObservableCollection<rechten_groep>(vc.Entity.Set<rechten_groep>().Where(x => x.actif).OrderBy(x => x.groep_code).ToList());
                level0.DataSource = someDbSet;
                settings.AddLevel(level0);

                var someDbSetChild = new ObservableCollection<rechten_groepenlabels>(vc.Entity.Set<rechten_groepenlabels>().OrderBy(x => x.labels.name).ToList());
                level1.DataSource = someDbSetChild;
                settings.AddLevel(level1);

                vc.Settings = settings;
            }
            else if (_selectNavBarItem.ViewModelBase is smmAanvinklijsten)
            {
                smmWindowVC vc = ((smmWindowVC)_selectNavBarItem.ViewModelBase);
                List<SMMWindow2ComboItem> cmbItemLocaties = new List<SMMWindow2ComboItem>();
                SMMWindow2Settings settings = new SMMWindow2Settings("Clientdossier: lijsten", GlobalMethods.Themes.normal);

                List<SMMWindow2ComboItem> cmbItemsDecimalTypes = new List<SMMWindow2ComboItem>
                {
                    new SMMWindow2ComboItem() { Id = "C", Name = "Euro" },
                    new SMMWindow2ComboItem() { Id = "N", Name = "Decimaal getal" },
                    new SMMWindow2ComboItem() { Id = "d", Name = "Geheel getal" }
                };

                SMMWindow2Level level0 = new SMMWindow2Level(0, "Cliëntdossier : (meer)keuzelijsten") { AllowDelete = false, AllowModify = true, AllowAdd = true, AllowSearch = true, AllowFilter = true };
                level0.AddColumns(
                    new SMMWindow2Column() { FieldName = "id", ColumnHeader = "id", IsEditable = false, Visible = true },
                    new SMMWindow2Column() { FieldName = "showcheckbox", ColumnHeader = "Aanvinklijst", IsEditable = true },
                    new SMMWindow2Column() { FieldName = "code", ColumnHeader = "Code", IsEditable = true },
                    new SMMWindow2Column() { FieldName = "descr", ColumnHeader = "Naam", IsEditable = true },
                    new SMMWindow2Column() { FieldName = "label", ColumnHeader = "Label (boven grid)", IsEditable = true },
                    new SMMWindow2Column() { FieldName = "sublabel", ColumnHeader = "sublabel (boven grid, italic)", IsEditable = true }
                );
                level0.JoinField = "id";
                level0.DataSource = new ObservableCollection<cd_lists_header>(vc.Entity.Set<cd_lists_header>().ToList());
                settings.AddLevel(level0);

                SMMWindow2Level level1 = new SMMWindow2Level(1, "Rechten") { AllowDelete = true, AllowModify = true, AllowAdd = true, AllowSearch = false, AllowFilter = false };
                level1.AddColumns(
                    new SMMWindow2Column() { FieldName = "headerid", ColumnHeader = "headerid", IsEditable = false, Visible = false },
                    new SMMWindow2Column() { FieldName = "name", ColumnHeader = "naam", IsEditable = true },
                    new SMMWindow2Column() { FieldName = "actif", ColumnHeader = "actief", DefaultValue = true },
                    new SMMWindow2Column() { FieldName = "orderNum", ColumnHeader = "Volgorde", DefaultValue = true },
                    new SMMWindow2Column() { FieldName = "value", ColumnHeader = "waarde", IsEditable = true, IsNullable = true },
                    new SMMWindow2Column() { FieldName = "canaddtext", ColumnHeader = "kan tekst toevoegen?", IsEditable = true, IsNullable = true },
                    new SMMWindow2Column() { FieldName = "textcolumnheader", ColumnHeader = "tekst", IsEditable = true, IsNullable = true },
                    new SMMWindow2Column() { FieldName = "istextmandatory", ColumnHeader = "is tekst verplicht?", IsEditable = true, IsNullable = true },
                    new SMMWindow2Column() { FieldName = "canadddate", ColumnHeader = "kan datum toevoegen?", IsEditable = true, IsNullable = true },
                    new SMMWindow2Column() { FieldName = "datecolumnheader", ColumnHeader = "datum", IsEditable = true, IsNullable = true },
                    new SMMWindow2Column() { FieldName = "isdatemandatory", ColumnHeader = "is datum verplicht?", IsEditable = true, IsNullable = true },
                    new SMMWindow2Column() { FieldName = "canadddecimal", ColumnHeader = "kan decimaal toevoegen?", IsEditable = true, IsNullable = true },
                    new SMMWindow2Column() { FieldName = "decimalcolumnheader", ColumnHeader = "decimaal", IsEditable = true, IsNullable = true },
                    new SMMWindow2Column() { FieldName = "isdecimalmandatory", ColumnHeader = "is decimaal verplicht?", IsEditable = true, IsNullable = true },
                    new SMMWindow2Column() { FieldName = "decimalColumnMask", ColumnHeader = "decimal type", IsEditable = true, IsNullable = false , ComboboxItems = cmbItemsDecimalTypes }
                );
                level1.JoinField = "headerid";
                level1.DataSource = new ObservableCollection<cd_lists_detail>(vc.Entity.Set<cd_lists_detail>().ToList());
                settings.AddLevel(level1);
                
                vc.Settings = settings;
            }
            else if (_selectNavBarItem.ViewModelBase is smmBeeldvormingSetting)
            {
                SMMWindow2Settings settings = new SMMWindow2Settings("Tree: Test", GlobalMethods.Themes.normal);

                SMMWindow2LevelTree level0 = new SMMWindow2LevelTree(0, "Cliënt dossier : toelatingen") { AllowDelete = false, AllowModify = true, AllowAdd = true, AllowSearch = true, AllowFilter = true, CanExportToExcel = true };
                level0.AddColumns(new SMMWindow2Column() { FieldName = "id", ColumnHeader = "id", Visible = false, IsEditable=false },
                                    new SMMWindow2Column() { FieldName = "parentid", ColumnHeader = "parentid", Visible = false },
                                    new SMMWindow2Column() { FieldName = "name", ColumnHeader = "Naam",  IsEditable = true },
                                        new SMMWindow2Column() { FieldName = "numbering", ColumnHeader = "Nummer", IsEditable=true }
                );
                level0.JoinField = "id";

                SMMWindow2Level level1 = new SMMWindow2Level(0, "Cliënt dossier : toelatingen") { AllowDelete = false, AllowModify = true, AllowAdd = false, AllowSearch = true, AllowFilter = true, CanExportToExcel = true };
                level1.AddColumns(new SMMWindow2Column() { FieldName = "id", ColumnHeader = "id", Visible = false },
                                    new SMMWindow2Column() { FieldName = "parentid", ColumnHeader = "parentid", IsEditable = false, Visible = false, IsNullable = true },
                                    new SMMWindow2Column() { FieldName = "aktiv", ColumnHeader = "aktiv", IsEditable = false, Visible = false },
                                    new SMMWindow2Column() { FieldName = "isEditable", ColumnHeader = "isEditable", IsEditable = false, Visible = false },
                                    new SMMWindow2Column() { FieldName = "level", ColumnHeader = "level", IsEditable = false, Visible = false },
                                    new SMMWindow2Column() { FieldName = "descr", ColumnHeader = "Beschrijving", IsEditable = true, IsNullable = true },
                                    new SMMWindow2Column() { FieldName = "infohelp", ColumnHeader = "Help", IsEditable = true, IsNullable = true }
                );
                level1.JoinField = "id";

                smmWindowVC vc = ((smmWindowVC)_selectNavBarItem.ViewModelBase);

                var ds = new ObservableCollection<beeldvormingtree>(vc.Entity.Set<beeldvormingtree>().OrderBy(x=>x.numbering).ToList());
                level0.DataSource = ds;
                settings.AddLevel(level0);

                var ds2 = new ObservableCollection<beeldvormingtree>(vc.Entity.Set<beeldvormingtree>().ToList());
                level1.DataSource = ds2;
                settings.AddLevel(level1);

                vc.Settings = settings;
            }
            else if (_selectNavBarItem.ViewModelBase is smmToelatingen)
            {
                SMMWindow2Settings settings = new SMMWindow2Settings("Clientdossier: toelatingen", GlobalMethods.Themes.normal);

                SMMWindow2Level level0 = new SMMWindow2Level(0, "Cliënt dossier : toelatingen") { AllowDelete = false, AllowModify = true, AllowAdd = true, AllowSearch = true, AllowFilter = true, CanExportToExcel = true };
                level0.AddColumns(new SMMWindow2Column() { FieldName = "title", ColumnHeader = "titel" },
                                    new SMMWindow2Column() { FieldName = "category", ColumnHeader = "categorie" },
                                    new SMMWindow2Column() { FieldName = "actif", ColumnHeader = "actief", DefaultValue = true }
                );

                smmWindowVC vc = ((smmWindowVC)_selectNavBarItem.ViewModelBase);
                var ds = new ObservableCollection<toelatingen_types>(vc.Entity.Set<toelatingen_types>().OrderBy(x => x.category).ToList());
                level0.DataSource = ds;
                settings.AddLevel(level0);

                vc.Settings = settings;
            }
            else if (_selectNavBarItem.ViewModelBase is smmRechtenGroepenFucnties)
            {
                smmWindowVC vc = ((smmWindowVC)_selectNavBarItem.ViewModelBase);

                List<SMMWindow2ComboItem> cmbItemsRechten = new List<SMMWindow2ComboItem>(new SMMWindow2ComboItem[] { new SMMWindow2ComboItem() { Id = "schrijven", Name = "schrijven" } });
                List<SMMWindow2ComboItem> cmbItemsFuncCodes = new List<SMMWindow2ComboItem>();
                foreach (rechten_functie f in vc.Entity.rechten_functie.Where(x => x.module_code == "CD"))
                {
                    cmbItemsFuncCodes.Add(new SMMWindow2ComboItem() { Id = f.func_code, Name = f.func_code + " " + f.func_naam });
                }

                SMMWindow2Settings settings = new SMMWindow2Settings("Groepen-rechten", GlobalMethods.Themes.normal);

                SMMWindow2Level level0 = new SMMWindow2Level(0, "Groepen") { AllowDelete = false, AllowModify = false, AllowAdd = false, AllowSearch = true, AllowFilter = true };
                level0.AddColumns(
                    new SMMWindow2Column() { FieldName = "groep_code", ColumnHeader = "Code", IsEditable = true },
                    new SMMWindow2Column() { FieldName = "groep_naam", ColumnHeader = "Naam", IsEditable = true },
                    new SMMWindow2Column() { FieldName = "groep_orbis", ColumnHeader = "Groep uit Orbis", DefaultValue = "N", IsEditable = false }
                );
                level0.JoinField = "groep_code";
                level0.DataSource = new ObservableCollection<rechten_groep>(vc.Entity.rechten_groep.Where(x => x.actif).ToList());
                settings.AddLevel(level0);

                SMMWindow2Level level1 = new SMMWindow2Level(1, "Rechten") { AllowDelete = true, AllowModify = true, AllowAdd = true, AllowSearch = false, AllowFilter = false };
                level1.AddColumns(
                    new SMMWindow2Column() { FieldName = "groepfunc_groep_code", ColumnHeader = "Groepcode", IsEditable = false },
                    new SMMWindow2Column() { FieldName = "groepfunc_func_code", ColumnHeader = "Functiecode", ComboboxItems = cmbItemsFuncCodes, IsEditable = true },
                    new SMMWindow2Column() { FieldName = "groepfunc_recht", ColumnHeader = "recht", DefaultValue = "schrijven", ComboboxItems = cmbItemsRechten, IsEditable = true, Visible = false }
                );
                level1.JoinField = "groepfunc_groep_code";
                var someDbSet1 = vc.Entity.rechten_groepenfuncties.Include("rechten_functie").Where(x => x.rechten_functie.module_code == "CD").OrderBy(x => x.groepfunc_func_code);
                level1.DataSource = new ObservableCollection<rechten_groepenfuncties>(someDbSet1.ToList());
                settings.AddLevel(level1);

                vc.Settings = settings;
            }
            else if (_selectNavBarItem.ViewModelBase is SmmContactverslagSoortBegeleiding)
            {
                SMMWindow2Settings settings = new SMMWindow2Settings("Clientdossier: contactverslag - soort begeleiding", GlobalMethods.Themes.normal);

                SMMWindow2Level level0 = new SMMWindow2Level(0, "Cliënt dossier : soorten begeleiding")
                {
                    AllowDelete = false,
                    AllowModify = true,
                    AllowAdd = true,
                    AllowSearch = true,
                    AllowFilter = true,
                    CanExportToExcel = true
                };
                level0.AddColumns(
                    new SMMWindow2Column() { FieldName = "naam", ColumnHeader = "Naam" },
                    new SMMWindow2Column() { FieldName = "descr", ColumnHeader = "Beschrijving", IsNullable = true },
                    new SMMWindow2Column() { FieldName = "active", ColumnHeader = "Actief", DefaultValue = true }
                );

                smmWindowVC vc = ((smmWindowVC)_selectNavBarItem.ViewModelBase);

                level0.DataSource = new ObservableCollection<Contactverslag_SoortBegeleiding>(vc.Entity.Set<Contactverslag_SoortBegeleiding>().OrderBy(x => x.naam).ToList());
                settings.AddLevel(level0);

                vc.Settings = settings;
            }
            else if (_selectNavBarItem.ViewModelBase is SmmAdrestypesManager)
            {
                SMMWindow2Settings settings = new SMMWindow2Settings("Clientdossier: adrestype manager", GlobalMethods.Themes.clientdossier);

                SMMWindow2Level level0 = new SMMWindow2Level(0, "Cliënt dossier : adrestype manager")
                {
                    AllowDelete = false,
                    AllowModify = true,
                    AllowAdd = true,
                    AllowSearch = true,
                    AllowFilter = true,
                    CanExportToExcel = true
                };
                level0.AddColumns(
                    new SMMWindow2Column() { FieldName = "naam", ColumnHeader = "Naam" },
                    new SMMWindow2Column() { FieldName = "opmerking", ColumnHeader = "Beschrijving", IsNullable = true },
                    new SMMWindow2Column() { FieldName = "actief", ColumnHeader = "Actief", DefaultValue = true },
                    new SMMWindow2Column() { FieldName = "ishoofdkeuze", ColumnHeader = "Is Adres?", DefaultValue = true },
                    new SMMWindow2Column() { FieldName = "isBedrijf", ColumnHeader = "Is Bedrijf?", DefaultValue = true },
                    new SMMWindow2Column() { FieldName = "module_code", ColumnHeader = "Module", Visible = false, DefaultValue = "CD" }
                );

                smmWindowVC vc = ((smmWindowVC)_selectNavBarItem.ViewModelBase);

                level0.DataSource = new ObservableCollection<adrestype>(vc.Entity.Set<adrestype>().Where(x => x.module_code == "CD").OrderBy(x => x.naam).ToList());
                settings.AddLevel(level0);

                vc.Settings = settings;
            }
        }
    }
}
