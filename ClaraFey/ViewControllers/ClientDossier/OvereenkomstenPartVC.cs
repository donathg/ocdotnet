﻿using ClaraFey.CommonObjects;
using ClaraFey.ViewControllers.Common;
using ClaraFey.Windows.Common;
using DevExpress.Mvvm;
using sharedcode.BLL;
using sharedcode.common;
using sharedcode.CommonObjects;
using sharedcode.WeergaveClasses;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace ClaraFey.ViewControllers.ClientDossier
{
    public class OvereenkomstenPartVC : ViewModelBase, INotifyPropertyChanged, IMyUserControl
    {
        /***** NEW VERSION *****/
        /** FIELDS */
        public new event PropertyChangedEventHandler PropertyChanged;

        private OvereenkomstenBLL _bll;
        private OvereenkomstenVC _parentVC;
        private OvereenkomstenSettings _settings;
        public ObservableCollection<OvereenkomstWeergave> OvereenkomstenList { get; set; }
        private OvereenkomstWeergave _selectedOvereenkomst;
        public OvereenkomstWeergave SelectedOvereenkomst
        {
            get
            {
                return _selectedOvereenkomst;
            }
            set
            {
                _selectedOvereenkomst = value;

                if (_parentVC != null)
                {
                    _parentVC.CanRemoveOvereenkomst = GlobalData.Instance.LoggedOnUser.HasAccess("1302");
                    _parentVC.NotifiyPropertyChanged(nameof(_parentVC.CanRemoveOvereenkomst));
                }
            }
        }
        // auto-implemented fields (IMyUsercontrol)
        public string Title { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public List<IMyUserControl> ChildrenUC { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public bool NeedSave { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public bool HasStatusFiltering { get; set; } = false;

        /** CONSTRUCTORS */
        public OvereenkomstenPartVC(OvereenkomstenSettings settings, OvereenkomstenVC vc = null)
        {
            _bll = new OvereenkomstenBLL();
            _parentVC = vc;
            _settings = settings;
            if (settings.Statussen != null && settings.Statussen.Count != 0)
                HasStatusFiltering = true;

            RefreshList();
        }

        /** METHODS */
        private void NotifiyPropertyChanged(string property)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(property));
        }
        public void RefreshList()
        {
            OvereenkomstenList = new ObservableCollection<OvereenkomstWeergave>(_bll.GetAllOvereenkomsten(_settings));
            NotifiyPropertyChanged(nameof(OvereenkomstenList));
        }
        public void OpenBijlagenScherm()
        {
            try
            {
                if (SelectedOvereenkomst == null)
                    throw new Exception("Je moet eerst een overeenkomst selecteren");
                else if (SelectedOvereenkomst.id == 0)
                    throw new Exception("Dit item is nog aan de DB toegevoegd");

                UploadFileWindowSettings settings = new UploadFileWindowSettings("Bijlagen voor " + SelectedOvereenkomst.TypeText + " gestart op: " + SelectedOvereenkomst.vanDatum, "inventaris")
                {
                    AllowDeleteFile = GlobalData.Instance.LoggedOnUser.GetAccesType("1001") == AccessType.access || GlobalData.Instance.LoggedOnUser.GetAccesType("1302") == AccessType.access,
                    PresetLabelList = new List<labels>() { LabelManager.GetLabelFromOvereenkomst(_selectedOvereenkomst) },
                    AllowEditing = GlobalData.Instance.LoggedOnUser.GetAccesType("1001") == AccessType.access,
                    AllowAddFile = GlobalData.Instance.LoggedOnUser.GetAccesType("1001") == AccessType.access || GlobalData.Instance.LoggedOnUser.GetAccesType("1301") == AccessType.access
                };

                UploadFileVC bijlagenVc = new UploadFileVC(
                    new BijlagenBLLProviderClientdossierOvereenkomst(SelectedOvereenkomst),
                    SelectedOvereenkomst.id,
                    settings
                );

                UploadFileWindow fw = new UploadFileWindow(bijlagenVc, GlobalMethods.Themes.clientdossier);
                fw.ShowDialog();

                OnBijlageManagerClose(fw);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Foutmelding", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void OnBijlageManagerClose(UploadFileWindow fw)
        {
            SelectedOvereenkomst.AantalBijlagen = fw.NumberOfItems.Value;

            List<FileItem> fileList = fw.BijlagenVC.Items.ToList();
            List<Overeenkomst_status> statusList = _bll.GetAllOvereenkomstStatussen().OrderByDescending(x => x.prioriteit).ToList();
            
            if (fileList.Count() == 0)
            {
                Overeenkomst_status laagstePrioriteitStatus = statusList.Last();

                SelectedOvereenkomst.Status = laagstePrioriteitStatus;
                SelectedOvereenkomst.status_id = laagstePrioriteitStatus.id;
            }
            else
            {
                foreach (Overeenkomst_status status in statusList)
                {
                    if (fileList.Where(x => x.Description.Remove(x.Description.IndexOf('(')-1).ToLower() == status.naam.ToLower()).Count() != 0)
                    {
                        SelectedOvereenkomst.Status = status;
                        SelectedOvereenkomst.status_id = status.id;
                        break;
                    }
                }
            }

            _bll.ChangeStatus(SelectedOvereenkomst);

            RefreshList();
        }
        // auto-implemented methods (IMyUsercontrol)
        public void Load(int? id)
        {
            RefreshList();
        }
        public void Save()
        {
        }
        public void RegisterChildUCs(IMyUserControl uc)
        {
        }
    }
}
