﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevExpress.Mvvm;
using sharedcode.BLL;
using sharedcode.common;
using ClaraFey.CommonObjects;
using ClaraFey.ViewControllers.Common;
using sharedcode.Caches;

namespace ClaraFey.ViewControllers.ClientDossier
{


    public class DashboardVC : ViewModelBase, IDisposable, INotifyPropertyChanged, IMyUserControl
    {
        public bool ClientMode { get; set; }
        public bool NeedSave { get; set; }
        public String Title
        {

            get { return "Dashboard"; }
            set { }
        }
        private List<IMyUserControl> ChildrenUC { get; set; }
        public ObservableCollection<dashboard> DashboardList { get; set; }
        private int _loggedOnUserId { get; set; }
        private bool _isVerwijderdView { get; set; }


        // constructor
        public DashboardVC(bool ClientMode)
        {
            this.ClientMode = ClientMode;
        }


        public void NotifyAdresProperties()
        {

        }

        public void Save()
        {

            GlobalMethods.WriteToLog("Dashboard.Save");
            if (ChildrenUC != null && ChildrenUC.Count > 0)
            {
                foreach (IMyUserControl c in ChildrenUC)
                {
                    c.Save();
                }
            }
            GlobalMethods.WriteToLog("Dashboard.End");
        }

        public void Load(int? id)
        {
            GlobalMethods.WriteToLog("Dashboard.Load Start");
            _loggedOnUserId = GlobalData.Instance.LoggedOnUser.Id.GetValueOrDefault();
            _bewoner = BewonersListCache.Instance.GetBewoner(id.GetValueOrDefault());
            SetDefaultFilter();

            if (this.ClientMode && id.HasValue)
            {
                this.Title += _bewoner.AchternaamVoornaam;
                NotifiyPropertyChanged("Title");
            }
            LoadDashboardItems();
            GlobalMethods.WriteToLog("Dashboard.Load End");
        }

        private void LoadDashboardItems()
        {
            _verwijderdList = DashbboardBLL.GetAllDashboardVerwijderd();
            DashboardList = new ObservableCollection<dashboard>(DashbboardBLL.GetDashboardItems(null, Filter));

            QuickRefreshAfterChanges();
        }

        private void QuickRefreshAfterChanges()
        {
            SetDefaultVerwijderdStatus();
            foreach (dashboard d in DashboardList)
            {
                d.InitPictures();

            }
            NotifiyPropertyChanged("DashboardList");
        }

        public void RegisterChildUCs(IMyUserControl uc)
        {
            if (ChildrenUC == null)
                ChildrenUC = new List<IMyUserControl>();
            uc.Load(_bewoner.id);
            ChildrenUC.Add(uc);
        }


        /***** NODIG VOOR ZOEKFUNCTIE *****/
        public ObservableCollection<bewoner> BewonersList
        {
            get
            {
                return new ObservableCollection<bewoner>(BewonersListCache.Instance.AllBewonersList);
            }
        }
        public ObservableCollection<gebruiker> GebruikersList
        {
            get
            {
                return new ObservableCollection<gebruiker>(GebruikerBLL.GetAllGebruikers());
            }
        }
        public ObservableCollection<leefgroep> LeefgroepenList
        {
            get
            {
                return new ObservableCollection<leefgroep>(LeefgroepBLL.GetLeefgroepenBewoners());
            }
        }
        public DashboardFilter Filter { get; set; }
        private bewoner _bewoner { get; set; }
        public bool IsAddBewonerBarVisible
        {
            get
            {
                if (ClientMode)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }


        private void SetDefaultFilter()
        {
            Filter = new DashboardFilter();

            Filter.bewoner = _bewoner;
            if (this.ClientMode)
            {
                Filter.gekozenLeefgroep = Filter.bewoner.leefgroep.FirstOrDefault();
            }
            else
            {
                Filter.gekozenLeefgroep = null;
            }
            Filter.vanDatum = DateTime.Today.AddMonths(-3);
            Filter.totDatum = DateTime.Now.AddHours(5);
            Filter.bericht = String.Empty;
        }

        internal void FilterZoekResults()
        {
            if (ClientMode)
            {
                Filter.bewoner = _bewoner;
            }
            if (_isVerwijderdView)
            {
                DashboardList = new ObservableCollection<dashboard>(DashbboardBLL.GetDashboardItems(_loggedOnUserId, Filter));
            }
            else
            {
                DashboardList = new ObservableCollection<dashboard>(DashbboardBLL.GetDashboardItems(null, Filter));
            }

            QuickRefreshAfterChanges();
        }


        /***** NODIG VOOR FAVORIETEN TE SELECTEREN EN DESELECTEREN *****/
        private List<dashboard_verwijderd> _verwijderdList;


        public void ChangeFavorietStatus(dashboard dashboard)
        {
            dashboard_verwijderd verwijderd = _verwijderdList.Find(x => x.dashboardId == dashboard.id && x.gebruikerId == _loggedOnUserId);
            if (verwijderd == null)
            {
                dashboard.IsVerwijderd = true;
                DashbboardBLL.InsertDashboardVerwijderdItem(_loggedOnUserId, dashboard.id);
                _verwijderdList = DashbboardBLL.GetAllDashboardVerwijderd();
            }
            else
            {
                dashboard.IsVerwijderd = false;
                _verwijderdList.Remove(verwijderd);
                DashbboardBLL.RemoveDashboardVerwijderdItem(verwijderd.id);
            }
            ChangeVerwijderdImageDagboekItem(dashboard);
        }

        private void ChangeVerwijderdImageDagboekItem(dashboard item)
        {
            DashboardList.Remove(item);

            NotifiyPropertyChanged("DashboardList");
        }

        private void SetDefaultVerwijderdStatus()
        {
            foreach (var dashboard in DashboardList)
            {
                if (_verwijderdList.Any(x => x.dashboardId == dashboard.id && x.gebruikerId == _loggedOnUserId))
                {
                    dashboard.IsVerwijderd = true;
                }
                else
                {
                    dashboard.IsVerwijderd = false;
                }
            }
        }


        /***** NODIG VOOR VIEW TE SWITCHEN TUSSEN FAVORIETEN EN NORMAAL *****/
        internal void VeranderViewType(bool isChecked)
        {
            DashboardList = null;
            if (isChecked)
            {
                DashboardList = new ObservableCollection<dashboard>(DashbboardBLL.GetDashboardItems(_loggedOnUserId, Filter));
                QuickRefreshAfterChanges();
                _isVerwijderdView = true;
            }
            else
            {
                LoadDashboardItems();
                _isVerwijderdView = false;
            }
        }


        /***** METHODS VAN GERT *****/
        public new  event PropertyChangedEventHandler PropertyChanged;


        private void NotifiyPropertyChanged(string property)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(property));
            }
        }

        public void Dispose()
        {

        }

        List<IMyUserControl> IMyUserControl.ChildrenUC
        {
            get
            {
                throw new NotImplementedException();
            }

            set
            {
                throw new NotImplementedException();
            }
        }
    }
}
