﻿using DevExpress.Mvvm;
using sharedcode.BLL;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevExpress.Mvvm.POCO;
using sharedcode.common;
using ClaraFey.Windows.ClientDossier;
using sharedcode.DAL;
using ClaraFey.ViewControllers.Common;
using sharedcode.WeergaveClasses;
using sharedcode.Caches;

namespace ClaraFey.ViewControllers.ClientDossier
{
    public class ActiePlanVC : ViewModelBase, IDisposable, INotifyPropertyChanged, IMyUserControl
    {
        /** FIELDS */
        public new event PropertyChangedEventHandler PropertyChanged;

        public string MainCategory { get; private set; }
        public bewoner Bewoner { get; set; }
        public ObservableCollection<ActieplanOpvolgingWeergave> OpvolgingList { get; set; }
        public List<actieplantypes> ActiePlanTypesList { get; set; }
        public ObservableCollection<ActieplanWeergave> ActiePlanList { get; set; }
        private ActieplanWeergave _selectedActieplan;
        public ActieplanWeergave SelectedActieplan
        {
            get
            {
                return _selectedActieplan;
            }
            set
            {
                _selectedActieplan = value;

                if (_selectedActieplan != null)
                    LoadOpvolgingenFromSelectedActieplan();
                else
                    ClearOpvolgingList();

                NotifiyPropertyChanged(nameof(OpvolgingList));
                NotifiyPropertyChanged(nameof(CanAddEditDelete));
                NotifiyPropertyChanged(nameof(CanCloseActiePlan));
                NotifiyPropertyChanged(nameof(CanPrintActieplan));
                NotifiyPropertyChanged(nameof(IsOpvolgingVisible));
            }
        }
        public string Title
        {
            get
            {
                return "Overeenkomsten";
            }
            set => throw new NotImplementedException();
        }
        public bool NeedSave { get; set; }
        public bool CanAddEditDelete
        {
            get
            {
                if (SelectedActieplan is ActieplanWeergave)
                {
                    if (
                        (MainCategory == "CLIENT" && GlobalData.Instance.LoggedOnUser.HasAccess("1811")) ||
                        (MainCategory == "OUDERS" && GlobalData.Instance.LoggedOnUser.HasAccess("1821")) ||
                        (MainCategory == "BTEAM" && GlobalData.Instance.LoggedOnUser.HasAccess("1831"))
                       )
                        return true;
                }

                return false;
            }
        }
        public bool CanCloseActiePlan
        {
            get
            {
                return GlobalData.Instance.LoggedOnUser.HasAccess("1801") && SelectedActieplan != null && !SelectedActieplan.IsAfgewerkt;
            }
        }
        public bool CanPrintActieplan
        {
            get
            {
                return GlobalData.Instance.LoggedOnUser.HasAccess("1802") && SelectedActieplan != null && !SelectedActieplan.IsAfgewerkt;
            }
        }
        public bool IsOpvolgingVisible
        {
            get
            {
                return SelectedActieplan != null;
            }
        }
        public bool AreAfgewerktColumsVisible { get; set; }

        /** METHODS */
        private void NotifiyPropertyChanged(string property)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(property));
        }
        private void ClearOpvolgingList()
        {
            OpvolgingList = new ObservableCollection<ActieplanOpvolgingWeergave>();
            NotifiyPropertyChanged(nameof(OpvolgingList));
        }
        private void LoadOpvolgingenFromSelectedActieplan()
        {
            OpvolgingList = new ObservableCollection<ActieplanOpvolgingWeergave>(ActiePlanBLL.GetAllActieplanOpvolgingenFromActieplan(SelectedActieplan));
            NotifiyPropertyChanged(nameof(OpvolgingList));
        }
        public void CloseSelectedActiePlan()
        {
            if (SelectedActieplan == null)
                throw new Exception("Gelieve een actieplan uit te kiezen");

            ActieplanEvaluatieAddShow popup = new ActieplanEvaluatieAddShow(new ActieplanEvaluatiesAddModifyWindowSettings()
            {
                Versie = ActieplanEvaluatieVersies.EINDEVALUATIE,
                SelectedEvaluatie = new ActieplanEvaluatiesWeergave(),
                BewonerId = Bewoner.id,
                ActieplanId = SelectedActieplan.id,
                ActieplantypesList = SelectedActieplan.Actieplantypes
            });
            if (popup.ShowDialog().GetValueOrDefault())
            {
                ActiePlanBLL.CloseActieplan(this.SelectedActieplan);
                LoadActiePlannen();

                // this.SelectedActieplan = this.ActiePlanList.Where(x => x.id == a.id).FirstOrDefault();
                // NotifiyPropertyChanged(nameof(SelectedActieplan));
            }
        }
        public void SetMainCategory(String category)
        {
            MainCategory = category;
            LoadActiePlannen();

            AreAfgewerktColumsVisible = category == "HIST";
            NotifiyPropertyChanged(nameof(AreAfgewerktColumsVisible));
        }
        public void Load(int? id)
        {
            Bewoner = BewonersListCache.Instance.GetBewoner(id.GetValueOrDefault());

            ActiePlanTypesList = ActiePlanBLL.GetActiveAllActieplantypes();
            LoadActiePlannen();

            NotifiyPropertyChanged(nameof(Bewoner));
        }
        public void LoadActiePlannen()
        {
            if (!String.IsNullOrWhiteSpace(MainCategory))
            {
                if (MainCategory == "HIST")
                    ActiePlanList = new ObservableCollection<ActieplanWeergave>(ActiePlanBLL.GetAllAfgewerkteActieplanWeergavesFromBewoner(Bewoner));
                else
                    ActiePlanList = new ObservableCollection<ActieplanWeergave>(ActiePlanBLL.GetAllOpenActieplanWeergavesFromBewonerAndCategory(Bewoner, MainCategory));
                NotifiyPropertyChanged(nameof(ActiePlanList));
                ClearOpvolgingList();
            }
        }
        public void AddActiePlan(ActieplanWeergave ap)
        {
            ActiePlanBLL.AddActieplan(ap);

            LoadActiePlannen();
        }
        public void ModifyActiePlan(ActieplanWeergave ap)
        {
            if (ap != null)
            {
                ActiePlanBLL.ChangeActieplan(ap);
                LoadActiePlannen();

                SelectedActieplan = this.ActiePlanList.Where(x => x.id == ap.id).FirstOrDefault();
                NotifiyPropertyChanged(nameof(SelectedActieplan));
            }
        }
        public void AddOpvolging(ActieplanOpvolgingWeergave opvolging)
        {            
            ActiePlanBLL.AddActieplanOpvolging(opvolging);
            LoadOpvolgingenFromSelectedActieplan();
        }
        public void EditOpvolging(ActieplanOpvolgingWeergave opvolging)
        {
            ActiePlanBLL.ChangeActieplanOpvolging(opvolging);
            LoadOpvolgingenFromSelectedActieplan();
        }
        public void Dispose()
        {
        }
        public void Save()
        {
             
        }
        public void RegisterChildUCs(IMyUserControl uc)
        {
            throw new NotImplementedException();
        }
        public List<IMyUserControl> ChildrenUC
        {
            get => throw new NotImplementedException();
            set => throw new NotImplementedException();
        }
    }
}
