﻿using ClaraFey.ViewControllers.Common;
using DevExpress.Mvvm;
using sharedcode.BLL;
using sharedcode.Caches;
using sharedcode.common;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ClaraFey.ViewControllers.ClientDossier
{
/*
    public enum BegeleidingWindowTypes
    {
            mijnBewoner, //de geselecteerde bewoner van de ingeloggde gebruiker
            allMijnBewoners, // alle bewoners van de ingeloggde gebruiker
            alleBegeleidersBewoners, // alle bewoners van alle begeleiders gebruiker
    }


    public class BegeleidingenAlleBewonersVC : BegeleidingenVC
    {
        public BegeleidingenAlleBewonersVC()
        {
            WindowType = BegeleidingWindowTypes.allMijnBewoners;
            FilterDateFrom = new DateTime(DateTime.Now.Year, 1, 1);
            FilterDateTo = new DateTime(DateTime.Now.Year, 12, 31);
            CanPrintReportVAP = false;
        }

    }

    /// <summary>
    /// Vieuw
    /// </summary>
    public class BegeleidingenAlleBegeleidersBewonersVC : BegeleidingenVC
    {
        public BegeleidingenAlleBegeleidersBewonersVC()
        {
            WindowType = BegeleidingWindowTypes.alleBegeleidersBewoners;
            FilterDateFrom = new DateTime(DateTime.Now.Year, 1, 1);
            FilterDateTo = new DateTime(DateTime.Now.Year, 12, 31);
            CanPrintReportVAP = true;
        }

    }

    public class BegeleidingenVC : ViewModelBase, IDisposable, INotifyPropertyChanged, IMyUserControl
    {
        public DateTime FilterDateFrom { get; set; }
        public DateTime FilterDateTo { get; set; }
        public BegeleidingWindowTypes WindowType { get; set; }
        public bool NeedSave { get; set; }
        public String Title
        {
            get { return "Begeleidingen"; }
            set { }
        }
        public bool CanChangeBewoner
        {
            get
            {
                return this.WindowType == BegeleidingWindowTypes.allMijnBewoners;
            }
        }
        public bool CanPrintReportVAP { get; set; }
        public bool CanAddBegeleiding
        {
            get
            {
                return this.WindowType != BegeleidingWindowTypes.alleBegeleidersBewoners;
            }
        }

        private List<IMyUserControl> ChildrenUC { get; set; }


        private bool _alleBewonersTonen;
        public bool AlleBewonersTonen
        {
            get
            {
                return _alleBewonersTonen;
            }

            set
            {
                _alleBewonersTonen = value;
                NotifiyPropertyChanged("CanModifyBewoner");
                NotifiyPropertyChanged("BegeleidingsItems");
            }
        }
        public BegeleidingenVC()
        {
            FilterDateFrom = new DateTime(DateTime.Now.Year, 1, 1);
            FilterDateTo = new DateTime(DateTime.Now.Year, 12, 31);
            WindowType = BegeleidingWindowTypes.mijnBewoner;
            CanPrintReportVAP = false;
            ListBewoners = new ObservableCollection<bewoner>(BewonersListCache.Instance.AllBewonersList);
            ListGebruikers = new ObservableCollection<vw_gebruikergroep>(GebruikerBLL.GetGebruikers());
        }

        public void RegisterChildUCs(IMyUserControl uc)
        {

            if (ChildrenUC == null)
                ChildrenUC = new List<IMyUserControl>();
            uc.Load(this.BewonerId);
            ChildrenUC.Add(uc);
        }

        public void Save()
        {

        }

        public ObservableCollection<begeleiding> BegeleidingsItems { get; set; }
        public bool CanModifyBewoner
        {
            get { return AlleBewonersTonen; }
        }


        private int? BewonerId { get; set; }

        public begeleiding SelectedItem { get; set; }

        public void Load(int? id)
        {
            BewonerId = id;
            LoadItems();
            NotifiyPropertyChanged("BegeleidingsItems");
        }

        public void Refresh()
        {
            LoadItems();
        }

        private void LoadItems()
        {
            if (this.WindowType == BegeleidingWindowTypes.mijnBewoner)
                BegeleidingsItems = new ObservableCollection<begeleiding>(BegeleidingBLL.GetBegeleidingen(FilterDateFrom,FilterDateTo, GlobalData.Instance.LoggedOnUser.Id.GetValueOrDefault(), this.BewonerId));
            if (this.WindowType == BegeleidingWindowTypes.allMijnBewoners)
                BegeleidingsItems = new ObservableCollection<begeleiding>(BegeleidingBLL.GetBegeleidingen(FilterDateFrom, FilterDateTo, GlobalData.Instance.LoggedOnUser.Id.GetValueOrDefault(), null));
            if (this.WindowType == BegeleidingWindowTypes.alleBegeleidersBewoners)
                BegeleidingsItems = new ObservableCollection<begeleiding>(BegeleidingBLL.GetBegeleidingen(FilterDateFrom, FilterDateTo, null, null));
            NotifiyPropertyChanged("BegeleidingsItems");
        }

        public cd_administratieveinfo cd_administratieveinfo
        {
            get; set;
        }



        public void Dispose()
        {
        }
        private void NotifiyPropertyChanged(string property)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(property));
        }

        public List<begeleiding_begeleidingen> ListBegeleidingen
        {
            get
            {
                return BegeleidingBLL.GetBegeleidingTypes();

            }
        }

        public List<begeleiding_groepen> ListBegeleidingenGroepen
        {
            get
            {
                return BegeleidingBLL.GetBegeleidingGroepen();

            }
        }

        public List<begeleiding_types> ListBegeleidingenTypes
        {
            get
            {
                return BegeleidingBLL.GetTypes();

            }
        }

        public List<begeleiding_locaties> ListBegeleidingenLocaties
        {
            get
            {
                return BegeleidingBLL.GetLocaties();

            }
        }


        List<IMyUserControl> IMyUserControl.ChildrenUC
        {
            get
            {
                throw new NotImplementedException();
            }

            set
            {
                throw new NotImplementedException();
            }
        }


        public ObservableCollection<bewoner> ListBewoners
        {

            get; set;

        }

        public ObservableCollection<vw_gebruikergroep> ListGebruikers
        {

            get; set;

        }


        public new event PropertyChangedEventHandler PropertyChanged;


        #region edititem for popup
        public begeleiding EditItem { get; set; }
        public bewoner EditSelectedBewoner { get; set; }
        public begeleiding InitEditItem(bool isNew)
        {

            EditSelectedBewoner = ListBewoners[3];

            if (isNew)
            {
                EditItem = new begeleiding()
                {
                    gebruikerid = GlobalData.Instance.LoggedOnUser.Id.GetValueOrDefault()
                };
                if (this.BewonerId.HasValue)
                {
                    EditItem.bewonerid = this.BewonerId.Value;
                    foreach (bewoner b in ListBewoners)
                    {
                        if (b.id == EditItem.bewonerid)
                            EditSelectedBewoner = b;//this.ListBewoners.Where(x => x.id == this.BewonerId.Value).FirstOrDefault();
                    }

                }



            }
            else
                EditItem = SelectedItem;
            return EditItem;
        }
        public bool CanSave
        {
            get
            {
                return true;
            }
        }
        public String SaveEditItem()
        {


            String errorMessage = Validate();
            if (!String.IsNullOrWhiteSpace(errorMessage))
                return errorMessage;
            else
            {
                try
                {
                    this.EditItem.bewoner = null;
                    this.EditItem.bewonerid = EditSelectedBewoner.id;
                    this.EditItem.bewonerleefgroepid = null;
                    if (EditSelectedBewoner.leefgroep != null && EditSelectedBewoner.leefgroep.Count > 0)
                        this.EditItem.bewonerleefgroepid = EditSelectedBewoner.leefgroep.ToArray()[0].id;
                    BegeleidingBLL.Save(this.EditItem);
                    LoadItems();
                    NotifiyPropertyChanged(nameof(BegeleidingsItems));
                    return null;

                }
                catch (Exception e)
                {
                    return e.Message;
                }
            }
        }
        public String Validate()
        {

            //check for errors

            if (EditSelectedBewoner == null)
                return "Gelieve een bewoner uit te kiezen in te geven.";
            if (this.EditItem.datum_van >= this.EditItem.datum_tot)
                return "van-tijd moet grotere zijn dan tot-tijd";
            if (this.EditItem.begeleidingsgroepid == 0)
                return "Gelieve een groep uit te kiezen";
            if (this.EditItem.begeleidingsbegeleidingid == 0)
                return "Gelieve een begeleiding uit te kiezen";
            if (this.EditItem.begeleidingstypeid == 0)
                return "Gelieve een type uit te kiezen";
            if (this.EditItem.begeleidingslocatieid == 0)
                return "Gelieve een locatie uit te kiezen";

            return null;

        }


        #endregion

        public bool IsColumnGebruikerVisible
        {

            get
            {

                return this.WindowType == BegeleidingWindowTypes.alleBegeleidersBewoners;
            }
        }
    }*/
}
