﻿using ClaraFey.CommonObjects;
using ClaraFey.ViewControllers.Common;
using DevExpress.Mvvm;
using sharedcode.BLL;
using sharedcode.Caches;
using sharedcode.common;
using sharedcode.WeergaveClasses;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClaraFey.ViewControllers.ClientDossier
{
    public class ContactverslagVC: ViewModelBase, INotifyPropertyChanged, IMyUserControl
    {
        /** FIELDS */
        public new event PropertyChangedEventHandler PropertyChanged;

        private ContactverslagBLL _bll;
        public bewoner Bewoner;
        public LabelManager LabelManager { get; set; }
        public ContactverslagFilter Filter { get; set; }

        public ObservableCollection<ContactverslagWeergave> ContactverslagList { get; set; }
        public ObservableCollection<Contactverslag_SoortBegeleiding> SoortBegeleidingList { get; set; }
        public ObservableCollection<gebruiker> GebruikersList { get; set; }

        /** CONSTRUCTOR */
        public ContactverslagVC()
        {
            _bll = new ContactverslagBLL();
            ContactverslagList = new ObservableCollection<ContactverslagWeergave>();
            SoortBegeleidingList = new ObservableCollection<Contactverslag_SoortBegeleiding>(_bll.GetAllActiveSoortBegeleidingen());
            GebruikersList = new ObservableCollection<gebruiker>(GebruikerBLL.GetAllGebruikers());
            LabelManager = GlobalData.Instance.LoggedOnUser.LabelManager;
        }
        public ContactverslagVC(LabelManager manager): this()
        {
            LabelManager = manager;
        }

        /** METHODS */
        private void SetContactverslagItems()
        {
            ContactverslagList.Clear();
            foreach (ContactverslagWeergave item in _bll.GetAllContactverslagenFromFilter(Filter))
                ContactverslagList.Add(item);

            NotifiyPropertyChanged(nameof(ContactverslagList));
        }
        private void SetDefaultFilter()
        {
            List<labels> labelList = new List<labels>();
            foreach (labels label in LabelManager.GetAllActiveLabels())
            {
                labelList.Add(label);
                labelList.AddRange(LabelManager.GetChildLables(label.id));
            }
            Filter = new ContactverslagFilter()
            {
                GekozenBewoner = Bewoner,
                VanDatum = DateTime.Today.AddMonths(-3),
                TotDatum = DateTime.Now,
                GekozenAfspraak = String.Empty,
                Labels = labelList.Where(x => x.contactverslagen == true && GlobalData.Instance.LoggedOnUser.LabelManager.GetAllActiveLabels().Exists(y => y.id == x.id)).ToList()
            };
        }
        public void NotifiyPropertyChanged(string property)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(property));
        }
        public void RefreshList()
        {
            SetContactverslagItems();
        }
        public void ClearFilter()
        {
            Filter.Clear();
            NotifiyPropertyChanged(nameof(Filter));
        }
        public void DeleteContactverslag(ContactverslagWeergave item)
        {
            _bll.RemoveContactverslag(item);
            SetContactverslagItems();
        }
        #region Inherited from IMyUserControl
        public string Title
        {
            get
            {
                if (Bewoner is bewoner)
                    return "Contactverslagen van " + Bewoner.VoornaamAchternaam;
                else
                    return "Contactverslagen";
            }
            set
            {
            }
        }
        public List<IMyUserControl> ChildrenUC { get; set; }
        public bool NeedSave { get; set; }
        public void Load(int? id)
        {
            Bewoner = BewonersListCache.Instance.GetBewoner(id.GetValueOrDefault());
            NotifiyPropertyChanged(nameof(Title));

            SetDefaultFilter();
            SetContactverslagItems();
        }
        public void Save()
        {
            if (ChildrenUC != null && ChildrenUC.Count > 0)
            {
                foreach (IMyUserControl c in ChildrenUC)
                {
                    c.Save();
                }
            }
        }
        public void RegisterChildUCs(IMyUserControl uc)
        {

        }
        #endregion
    }
}
