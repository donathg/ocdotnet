﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevExpress.Mvvm;
using sharedcode.BLL;
using System.Drawing;
using System.IO;
using sharedcode.common;
using System.Reflection;
using ClaraFey.Windows.ClientDossier;
using ClaraFey.CommonObjects;
using ClaraFey.ViewControllers.Common;
using sharedcode.Caches;

namespace ClaraFey.ViewControllers.ClientDossier
{
    public class DagboekVC : ViewModelBase, IDisposable, INotifyPropertyChanged, IMyUserControl
    {
        /** FIELDS */
        public new event PropertyChangedEventHandler PropertyChanged;

        private DagboekBLL _manager;
        public LabelManager LabelManager { get; set; }

        private List<IMyUserControl> ChildrenUC { get; set; }
        List<IMyUserControl> IMyUserControl.ChildrenUC
        {
            get
            {
                throw new NotImplementedException();
            }

            set
            {
                throw new NotImplementedException();
            }
        }
        private int _loggedOnUserId { get; set; }
        private bool _isFavorietenView;
        public bool IsFavorietenView
        {
            get
            {
                return _isFavorietenView;
            }
            set
            {
                _isFavorietenView = value;
                NotifiyPropertyChanged(nameof(IsFavorietenView));
            }
        }
        public String Title
        {
            get
            {
                if (ClientMode)
                    return "Dagboek van " + _bewoner.VoornaamAchternaam;
                else
                    return "Algemeen dagboek";
            }
            set { }
        }
        public bool ClientMode { get; set; }
        public string IngavePositie
        {
            get
            {
                return GlobalData.Instance.LoggedOnUser.Settings.dagboek_invoerpositie;
            }
            set
            {
                gebruiker_settingsBLL.UpdateDagboekInvoerPositie(_loggedOnUserId, value);
                NotifiyPropertyChanged(nameof(IngavePositie));
            }
        }
        public bool NeedSave { get; set; }
        public ObservableCollection<DagboekWeergave> DagboekList { get; set; }

        /** CONSTRUCTOR */
        public DagboekVC(LabelManager labels, bool isClientMode)
        {
            _manager = new DagboekBLL();
            LabelManager = labels;
            ClientMode = isClientMode;
            DatumGebeurtenis = DateTime.Now;
        }

        /** METHODS */
        public void NotifiyPropertyChanged(string property)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(property));
        }
        public void RegisterChildUCs(IMyUserControl uc)
        {
            if (ChildrenUC == null)
                ChildrenUC = new List<IMyUserControl>();
            uc.Load(_bewoner.id);
            ChildrenUC.Add(uc);
        }
        public void Dispose()
        {

        }
        public void Save()
        {
            if (ChildrenUC != null && ChildrenUC.Count > 0)
            {
                foreach (IMyUserControl c in ChildrenUC)
                {
                    c.Save();
                }
            }
        }
        public void Load(int? id)
        {
            _loggedOnUserId = GlobalData.Instance.LoggedOnUser.Id.GetValueOrDefault();
            _bewoner = BewonersListCache.Instance.GetBewoner(id.GetValueOrDefault());
            SetDefaultFilter();
            DagboekItemText = null;

            if (ClientMode && id.HasValue)
            {
                Title += _bewoner.AchternaamVoornaam;

                NotifiyPropertyChanged(nameof(Title));
            }
            SetDagboekItems();
        }
        private void SetDagboekItems()
        {
            DagboekList = new ObservableCollection<DagboekWeergave>(_manager.GetAllDagboekWeergaves(Filter));

            NotifiyPropertyChanged(nameof(DagboekList));
        }

        #region Export Functions
        public String PreviewText { get; set; }

        public void RefreshPreview()
        {
            SetDagboekItems();

            String tempString = "";
            foreach (DagboekWeergave dagboekItem in DagboekList)
            {
                tempString += @"";
            }
            PreviewText = tempString;
            NotifiyPropertyChanged(nameof(PreviewText));
        }
        #endregion

        #region TOEVOEGEN DAGBOEKITEM
        // properties voor ClientSelectBar
        public ObservableCollection<bewoner> BewonersList
        {
            get
            {
                List<bewoner> bewoners = BewonersListCache.Instance.AllBewonersList;
                bewoners.Insert(0, null);

                return new ObservableCollection<bewoner>(bewoners);
            }
        }
        private object _selectedItem;
        public object SelectedItem
        {
            get
            {
                return _selectedItem;
            }
            set
            {
                _selectedItem = value;
                NotifiyPropertyChanged(nameof(IsAddButtonEnabled));
            }
        }
        // properties voor BerichtToevoegenBar
        private String _dagboekItemText;
        public String DagboekItemText
        {
            get
            {
                return _dagboekItemText;
            }
            set
            {
                _dagboekItemText = value;
                NotifiyPropertyChanged(nameof(IsAddButtonEnabled));
            }
        }
        public DateTime DatumGebeurtenis { get; set; }
        public bool IsAddButtonEnabled
        {
            get
            {
                if ((ClientMode || _selectedItem != null) && !String.IsNullOrWhiteSpace(DagboekItemText))
                    return true;
                else
                    return false;
            }
        }

        public void AddDagboekItem(List<labels> labelManager)
        {
            dagboek dagboekItem = new dagboek()
            {
                descr = DagboekItemText
            };
            if (DatumGebeurtenis.Date >= new DateTime(2010, 1, 2))
                dagboekItem.datum = DatumGebeurtenis;
            if (SelectedItem is bewoner bew)
                dagboekItem.bewonerId = bew.id;
            else if (SelectedItem is leefgroep groep)
                dagboekItem.leefgroep_id = groep.id;
            else
                dagboekItem.bewonerId = _bewoner.id;

            DagboekWeergave newDagboek = new DagboekWeergave(dagboekItem)
            {
                DagboekLabels = new ObservableCollection<labels>(labelManager)
            };

            // voeg item toe aan databank
            _manager.InsertDagboek(newDagboek);

            // reload items
            Filter.TotDatum = DateTime.Now;
            DatumGebeurtenis = DateTime.Now;
            NotifiyPropertyChanged(nameof(DatumGebeurtenis));
            SetDagboekItems();

            // clears text from inputfield
            DagboekItemText = String.Empty;
            NotifiyPropertyChanged(nameof(DagboekItemText));
        }
        #endregion
        #region ZOEKFUNCTIE
        public DagBoekFilter Filter { get; set; }
        public List<gebruiker> GebruikersList
        {
            get
            {
                return GebruikerBLL.GetAllGebruikers();
            }
        }
        private bewoner _bewoner { get; set; }
        public bool IsAddBewonerBarVisible
        {
            get
            {
                return !ClientMode;
            }
        }

        public void SetDefaultFilter()
        {
            Filter = new DagBoekFilter()
            {
                Bewoner = _bewoner,
                VanDatum = DateTime.Today.AddMonths(-3),
                TotDatum = DateTime.Now,
                Bericht = String.Empty,
                Labels = LabelManager.GetAllActiveLabels().Where(x => x.dagboek == true && GlobalData.Instance.LoggedOnUser.LabelManager.GetAllActiveLabels().Contains(x)).ToList()
            };

            if (ClientMode)
                Filter.GekozenLeefgroep = Filter.Bewoner.leefgroep.FirstOrDefault();
            else
                Filter.GekozenLeefgroep = null;
        }
        public void FilterZoekResults()
        {
            if (ClientMode)
                Filter.Bewoner = _bewoner;

            SetDagboekItems();
        }
        #endregion
        #region FAVORIETENBEHEER
        public void ChangeFavorietStatus(DagboekWeergave dagBoek)
        {
            if (dagBoek.IsFavoriet)
            {
                dagBoek.IsFavoriet = false;
                _manager.RemoveDagboekFromFavorieten(dagBoek);
                if (IsFavorietenView)
                    SetDagboekItems();
            }
            else
            {
                dagBoek.IsFavoriet = true;
                _manager.InsertDagboekFavoriet(dagBoek);
            }
            dagBoek.NotifiyPropertyChanged(nameof(dagBoek.ToolTipFavorietenSter));
        }
        #endregion
        #region VIEW SWITCHEN TUSSEN FAVORIETEN EN NORMAAL
        public void VeranderViewType(bool isChecked)
        {
            if (isChecked != IsFavorietenView)
            {
                IsFavorietenView = isChecked;
                _manager.SwitchViews();

                DagboekList = null;
                SetDagboekItems();
            }
        }
        #endregion
        #region VERWIJDEREN ITEM
        public void DeleteItem(DagboekWeergave dagBoek)
        {
            _manager.RemoveDagboek(dagBoek);
            SetDagboekItems();
        }
        #endregion
        #region EDITEREN ITEM
        public void EditDagboek(DagboekWeergave dagBoek)
        {
            DagboekEditPopup popup = new DagboekEditPopup(dagBoek, GlobalMethods.Themes.clientdossier);
            popup.ShowDialog();
            SetDagboekItems();
        }
        #endregion
    }
}
