﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using DevExpress.Mvvm;
using ClaraFey.ViewControllers.Common;
using sharedcode.BLL;
using sharedcode.Caches;
using sharedcode.WeergaveClasses;

namespace ClaraFey.ViewControllers.ClientDossier
{
    public class InfoFicheVC : ViewModelBase, IDisposable, INotifyPropertyChanged, IMyUserControl
    {


        public Action BewonerChanged = delegate () { };
        /** FIELDS */
        public new event PropertyChangedEventHandler PropertyChanged;
        public cd_administratieveinfo cd_administratieveinfo { get; set; }
        public  bewoner Bewoner { get; set; }
        private List<IMyUserControl> ChildrenUC { get; set; }
        List<IMyUserControl> IMyUserControl.ChildrenUC
        {
            get
            {
                throw new NotImplementedException();
            }

            set
            {
                throw new NotImplementedException();
            }
        }
       
        public String Title
        {
            get
            {
                return "Infofiche";
            }
            set { }
        }
        public bool ClientMode { get; set; }
        
        public bool NeedSave { get; set; }
 

        /** CONSTRUCTOR */
        public InfoFicheVC()
        {
            
        }

        /** METHODS */
        public void NotifiyPropertyChanged(string property)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(property));
        }
        public void RegisterChildUCs(IMyUserControl uc)
        {
            if (ChildrenUC == null)
                ChildrenUC = new List<IMyUserControl>();
            uc.Load(Bewoner.id);
            ChildrenUC.Add(uc);
        }
        public void Dispose()
        {

        }
        public void Save()
        {
            if (ChildrenUC != null && ChildrenUC.Count > 0)
            {
                foreach (IMyUserControl c in ChildrenUC)
                {
                    c.Save();
                }
            }
        }

//        public List<AdresWeergave> GezinsledenOpClaraFay { get; set; } = new List<AdresWeergave>();

       // AdresWeergave DomicilieAdres { get; set; }

        public void Load(int? id)
        {
            Bewoner = BewonersListCache.Instance.GetBewoner(id.GetValueOrDefault());
            cd_administratieveinfo = AdministratieveInformatieBLL.GetData(id.GetValueOrDefault());
            Bewoner = BewonersListCache.Instance.GetBewoner(id.GetValueOrDefault());

            if (cd_administratieveinfo == null)
            {
                cd_administratieveinfo = new cd_administratieveinfo()
                {
                    bewonerid = id.GetValueOrDefault()
                };
            }

          //  GezinsledenOpClaraFay =  BewonerAdresBLLProvider.GetAdresFamilieOpOC(Bewoner, BewonersListCache.Instance.AllBewonersList);
          //  DomicilieAdres =  BewonerAdresBLLProvider.GetAdresFromCache(Bewoner, 2);
            
            NotifiyPropertyChanged("GezinsledenOpClaraFay");
            NotifiyPropertyChanged("Bewoner");
            if (BewonerChanged != null)
                BewonerChanged();

            if (ChildrenUC != null && ChildrenUC.Count > 0)
            {
                foreach (IMyUserControl c in ChildrenUC)
                {
                    c.Load(Bewoner.id);
                }
            }
        }
    }
}
