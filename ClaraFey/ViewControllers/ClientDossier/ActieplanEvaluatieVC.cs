﻿using sharedcode.BLL;
using sharedcode.common;
using sharedcode.DAL;
using sharedcode.WeergaveClasses;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClaraFey.ViewControllers.ClientDossier
{
    public class ActieplanEvaluatiesAddModifyWindowSettings
    {
        public ActieplanEvaluatieVersies Versie { get; set; }
        public ActieplanEvaluatiesWeergave SelectedEvaluatie { get; set; }
        public int ActieplanId { get; set; }
        public int BewonerId { get; set; }
        public List<actieplantypes> ActieplantypesList { get; set; }
        public Boolean CanSeePeriode
        {
            get
            {
                return Versie == ActieplanEvaluatieVersies.PERIODIEKE_EVALUATIE;
            }
        }
        public Boolean CanChangeData
        {
            get
            {
                if (SelectedEvaluatie.id == 0)
                    return true;
                else if (Versie == ActieplanEvaluatieVersies.PERIODIEKE_EVALUATIE)
                    return GlobalData.Instance.LoggedOnUser.IsUserInFunctieGroep("ORTH") || GlobalData.Instance.LoggedOnUser.HasAccess("1001");
                else
                    return false;
            }
        }
    }
    public class ActieplanEvaluatiesListData
    {
        public ActieplanEvaluatieVersies Versie { get; set; }
        public Boolean CanDelete { get; set; }
    }

    public class ActieplanEvaluatieVC: MyNotifyPropertyChanged
    {
        /** FIELDS */
        public int BewonerId;
        public ActieplanEvaluatieVersies Versie;
        public ObservableCollection<ActieplanEvaluatiesWeergave> EvaluatiesList { get; set; }
        public ActieplanEvaluatiesWeergave SelectedEvaluatie { get; set; }
        public List<actieplantypes> ActiePlanTypesList { get; set; }
        public List<Actieplan_Resultaat> ActieplanResultatenList { get; set; }
        public Boolean CanModifyDelete
        {
            get
            {
                return (Versie == ActieplanEvaluatieVersies.PERIODIEKE_EVALUATIE &&
                       GlobalData.Instance.LoggedOnUser.HasAccess("1001"));
            }
        }
        public Boolean IsPeriodiekeEvaluatie
        {
            get
            {
                return Versie == ActieplanEvaluatieVersies.PERIODIEKE_EVALUATIE;
            }
        }

        /** CONSTRUCTORS */
        public ActieplanEvaluatieVC(ActieplanEvaluatieVersies versie, int bewonerId)
        {
            BewonerId = bewonerId;
            Versie = versie;
            ActiePlanTypesList = ActiePlanBLL.GetAllActieplantypes();
            ActieplanResultatenList = ActieplanEvaluatiesBLL.GetAllActieplanResultaten();

            FillEvaluatiesList();
        }

        /** METHODS */
        public void FillEvaluatiesList()
        {
            if (Versie == ActieplanEvaluatieVersies.EINDEVALUATIE)
                EvaluatiesList = new ObservableCollection<ActieplanEvaluatiesWeergave>(ActieplanEvaluatiesBLL.GetAllEindevaluaties(BewonerId));
            else if (Versie == ActieplanEvaluatieVersies.PERIODIEKE_EVALUATIE)
                EvaluatiesList = new ObservableCollection<ActieplanEvaluatiesWeergave>(ActieplanEvaluatiesBLL.GetAllPeriodiekeEvaluaties(BewonerId));

            NotifiyPropertyChanged(nameof(EvaluatiesList));
        }
        public void RemoveActieplanEvaluatie()
        {
            ActieplanEvaluatiesBLL.RemoveActieplanEvaluatie(SelectedEvaluatie);
            FillEvaluatiesList();
        }
    }
}
