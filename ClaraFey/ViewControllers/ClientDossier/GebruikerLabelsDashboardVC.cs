﻿using ClaraFey.CommonObjects;
using ClaraFey.ViewControllers.Common;
using ClaraFey.Windows.ClientDossier;
using ClaraFey.Windows.Common;
using DevExpress.Mvvm;
using sharedcode.BLL;
using sharedcode.common;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace ClaraFey.ViewControllers.ClientDossier
{
    class GebruikerLabelsDashboardVC : ViewModelBase, INotifyPropertyChanged
    {
        /** FIELDS */
        public new event PropertyChangedEventHandler PropertyChanged;
        
        public UploadFileVC BijlagenVC { get; set; }
        public DagboekVC DagboekVC { get; set; }
        public ContactverslagVC ContactverslagenVC { get; set; }
        public string Title
        {
            get { return "Dashboard Label"; }
        }
        public bool ClientMode { get; set; }

        private int _bewonerId;
        private LabelManager _labelManager;

        /** CONSTRUCTOR */
        public GebruikerLabelsDashboardVC(bool ClientMode)
        {
            this.ClientMode = ClientMode;
        }

        /** METHODS */
        public void NotifiyPropertyChanged(string property)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(property));
        }
        public void Load(int? id, LabelManager manager)
        {
            // reset all VC
            BijlagenVC = null;
            DagboekVC = null;
            ContactverslagenVC = null;

            if (id.HasValue)
                _bewonerId = id.Value;
            if (manager != null)
                _labelManager = manager;
        }
        public List<labels> GetAllBijlagenLabels()
        {
            return _labelManager.GetAllActiveLabels().Where(x => x.bijlagen).ToList();
        }
        public List<labels> GetAllDagboekLabels()
        {
            return _labelManager.GetAllActiveLabels().Where(x => x.dagboek).ToList();
        }
        public List<labels> GetAllContactverslagenLabels()
        {
            return _labelManager.GetAllActiveLabels().Where(x => x.contactverslagen).ToList();
        }
        public void SetDagboektabContent()
        {
            DagboekVC = new DagboekVC(_labelManager, ClientMode);
            DagboekVC.Load(_bewonerId);
        }
        public void SetBijlagentabContent()
        {
            UploadFileWindowSettings settings = new UploadFileWindowSettings("Bijlagen", "clientdossier")
            {
                AllowAddFile = true,
                AllowEditing = GlobalData.Instance.LoggedOnUser.GetAccesType("1001") == sharedcode.CommonObjects.AccessType.access,
                AllowDeleteFile = GlobalData.Instance.LoggedOnUser.GetAccesType("1001") == sharedcode.CommonObjects.AccessType.access,
                PresetLabelList = _labelManager.GetAllActiveLabels()
            };
            BijlagenVC = new UploadFileVC(
                new BijlagenBLLProviderClientdossier(),
                _bewonerId,
                settings
            );
            BijlagenVC.Load(_bewonerId);
        }
        public void SetContactverslagentabContent()
        {
            ContactverslagenVC = new ContactverslagVC(_labelManager);
            ContactverslagenVC.Load(_bewonerId);
        }
    }
}
