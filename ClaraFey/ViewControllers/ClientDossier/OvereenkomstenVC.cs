﻿using ClaraFey.ViewControllers.Common;
using ClaraFey.Windows.ClientDossier;
using DevExpress.Mvvm;
using DevExpress.Xpf.Core;
using sharedcode.BLL;
using sharedcode.Caches;
using sharedcode.common;
using sharedcode.WeergaveClasses;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClaraFey.ViewControllers.ClientDossier
{
    public class OvereenkomstenVC : ViewModelBase, INotifyPropertyChanged, IMyUserControl
    {
        /***** NEW VERSION *****/
        /** FIELDS */
        public new event PropertyChangedEventHandler PropertyChanged;

        public bewoner Bewoner { get; set; }
        public String Title
        {
            get { return "Overeenkomsten"; }
            set => throw new NotImplementedException();
        }
        private OvereenkomstenBLL _bll;
        public List<Overeenkomst_type> TypeList { get; set; }
        public bool CanRemoveOvereenkomst { get; set; }
        public bool CanAddOvereenkomst { get; set; }
        private OvereenkomstenPartVC _partVC;
        public OvereenkomstenPartVC PartVC
        {
            get
            {
                return _partVC;
            }
            set
            {
                _partVC = value;

                if (PartVC != null)
                {
                    CanAddOvereenkomst = GlobalData.Instance.LoggedOnUser.HasAccess("1301");

                    if (PartVC.SelectedOvereenkomst != null)
                        CanRemoveOvereenkomst = GlobalData.Instance.LoggedOnUser.HasAccess("1302");
                    else
                        CanRemoveOvereenkomst = false;
                }
                else
                {
                    CanAddOvereenkomst = false;
                    CanRemoveOvereenkomst = false;
                }
            }
        }

        // Automatic generated IMyUserControl. Why?
        public List<IMyUserControl> ChildrenUC { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public bool NeedSave { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public ObservableCollection<myToelating> toelatingeList { get; set; }
        private ToelatingenBLL toelatingenBLL;

        /** CONSTRUCTORS */
        public OvereenkomstenVC()
        {
            _bll = new OvereenkomstenBLL();
            TypeList = _bll.GetAllOvereenkomstTypes();
        }

        /** METHODS */
        public void NotifiyPropertyChanged(string property)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(property));
        }
        public void RemoveOvereenkomst(OvereenkomstWeergave overeenkomst)
        {
            if (overeenkomst.AantalBijlagen == 0)
                _bll.RemoveOvereenkomst(overeenkomst);
            else
                throw new Exception("Als je deze overeenkomst wil verwijderen, moet je eerst alle bijlagen verwijderen.");
        }
        // neccessary for bewoner id 
        public void Load(int? id)
        {
            Bewoner = BewonersListCache.Instance.GetBewoner(id.GetValueOrDefault());
            ResetValues();
            toelatingenBLL = new ToelatingenBLL(this.Bewoner.id);
            List<myToelating> t = toelatingenBLL.GetToelatingen(this.Bewoner.id);
            this.toelatingeList = new ObservableCollection<myToelating>(t);
            String prevcat = "";
            foreach (myToelating myT in t)
            {
                if (prevcat != myT.Category)
                {
                    myT.CategoryToDisplay = myT.Category;

                }
                else
                {
                    myT.CategoryToDisplay = "";

                }
                prevcat = myT.Category;
            }
        }
        public void Save()
        {
        }
        public void RegisterChildUCs(IMyUserControl uc)
        {
        }
        public void AddCustomToelating(String text)
        {
            myToelating t = toelatingenBLL.AddCustomToelating(text);
            this.toelatingeList.Add(t);
            NotifiyPropertyChanged(nameof(toelatingeList));
        }
        public void ModifyCustomToelating(myToelating toelating)
        {
            toelatingenBLL.SaveCustomToelating(toelating);
            NotifiyPropertyChanged(nameof(toelatingeList));
        }
        public void DeleteCustomToelating(myToelating toelating)
        {
            toelatingenBLL.DeleteToelating(toelating);
            toelatingeList.Remove(toelating);
            NotifiyPropertyChanged(nameof(toelatingeList));
        }
        private void ResetValues()
        {
            if (toelatingeList != null)
            {
                toelatingeList.Clear();
                toelatingeList = null;
            }
        }
        public void SetTabContent(DXTabItem item)
        {
            OvereenkomstenSettings settings = new OvereenkomstenSettings()
            {
                BewonerId = Bewoner.id,
                Type = (Overeenkomst_type)item.Tag
            };
            PartVC = new OvereenkomstenPartVC(settings, this);

            item.Content = new OvereenkomstPartUC(PartVC);

        }
    }
}
