﻿using ClaraFey.CommonObjects;
using ClaraFey.Windows.Common;
using sharedcode.BLL;
using sharedcode.common;
using sharedcode.WeergaveClasses;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClaraFey.ViewControllers.Rapportage
{
    public class KasBeheerExportVC : MyNotifyPropertyChanged
    {
        /** FIELDS */
        private readonly KasBeheerExportBLL _bll;
        public ObservableCollection<KasBeheerExportWeergave> KasbeheerItems { get; set; }
        public KasbeheerExportFilter Filter { get; set; }
        public ObservableCollection<leefgroep> LeefgroepenList { get; set; }

        /** CONSTRUCTOR */
        public KasBeheerExportVC()
        {
            _bll = new KasBeheerExportBLL();

            int thisYear = DateTime.Today.Year;
            Filter = new KasbeheerExportFilter
            {
                DateFrom = new DateTime(thisYear, 1, 1),
                DateTo = new DateTime(thisYear, 12, 31),
                IsClosed = null
            };
            LeefgroepenList = new ObservableCollection<leefgroep>(LeefgroepBLL.GetLeefgroepen().OrderBy(x => x.naam));
            Filter.LeefgroepId = LeefgroepenList.FirstOrDefault().id;

            LoadKasbeheerItems();

            NotifiyPropertyChanged(nameof(Filter.DateFrom));
            NotifiyPropertyChanged(nameof(Filter.DateTo));
            NotifiyPropertyChanged(nameof(Filter.IsClosed));
            NotifiyPropertyChanged(nameof(KasbeheerItems));
            NotifiyPropertyChanged(nameof(LeefgroepenList));
        }

        /** METHODS */
        public void LoadKasbeheerItems()
        {
            KasbeheerItems = new ObservableCollection<KasBeheerExportWeergave>(_bll.GetAllKasbeheerItemsFromFilter(Filter));

            NotifiyPropertyChanged(nameof(KasbeheerItems));
        }
        public void PrintReport(DateTime dateTo, bool isClosed)
        {
            List<KasBeheerExportWeergave> filteredItems = KasbeheerItems.Where(x => x.TransactieDatum <= dateTo && !x.IsAfgesloten).ToList();
            ShowCvsExportableGrid(filteredItems.Where(x => x.IsClientTransactie).ToList());

            if (isClosed) /* Set state as closed when not "testdruk"-option */
                _bll.CloseKasbeheerItems(filteredItems);

            LoadKasbeheerItems();
        }
        public void PrintLastClosedReport()
        {
            ShowCvsExportableGrid(_bll.GetAllClientKasbeheerItemsFromLeefgroepid(Filter.LeefgroepId));
        }
        private void ShowCvsExportableGrid(List<KasBeheerExportWeergave> kasbeheerItems)
        {
            string lastClosedDate = "";
            string leefgroepNaam = "";

            if (kasbeheerItems.Count != 0 && KasbeheerItems.FirstOrDefault() is KasBeheerExportWeergave weergave)
            {
                lastClosedDate = weergave.TransactieDatum.ToString("dd-MM-yyyy");
                leefgroepNaam = weergave.LeefgroepNaam;
            }

            string title = $"Kasboek ' {leefgroepNaam} ' - afsluiten " + lastClosedDate;
            List<KasbeheerExportReportWeergave> reportItems = new List<KasbeheerExportReportWeergave>();

            foreach (KasBeheerExportWeergave weergaveItem in kasbeheerItems)
            {
                reportItems.Add(new KasbeheerExportReportWeergave(weergaveItem));
            }

            SMMWindow2Level level0 = new SMMWindow2Level(0, title)
            {
                AllowDelete = false,
                AllowModify = false,
                AllowAdd = false,
                AllowSearch = false,
                AllowFilter = false,
                CanExportToExcel = true
            };
            level0.AddColumns(
                new SMMWindow2Column() { FieldName = "Rijksregisternummer", ColumnHeader = "Rijksregisternummer", IsEditable = false },
                new SMMWindow2Column() { FieldName = "DatumString", ColumnHeader = "Datum", IsEditable = false },
                new SMMWindow2Column() { FieldName = "FinancieleBewegingscode", ColumnHeader = "FinancielbewegingsCode", IsEditable = false },
                new SMMWindow2Column() { FieldName = "Omschrijving", ColumnHeader = "Omschrijving", IsEditable = false },
                new SMMWindow2Column() { FieldName = "Aantal", ColumnHeader = "Eenheden", IsEditable = false },
                new SMMWindow2Column() { FieldName = "Bedrag", ColumnHeader = "Eenheidsprijs", IsEditable = false },
                new SMMWindow2Column() { FieldName = "Totaal", ColumnHeader = "Totaal", IsEditable = false }
            );
            level0.DataSource = new ObservableCollection<KasbeheerExportReportWeergave>(reportItems).ToList();

            SMMWindow2Settings settings = new SMMWindow2Settings(title, GlobalMethods.Themes.normal)
            {
                WindowSizePercentage = 75
            };
            settings.AddLevel(level0);
            settings.theme = GlobalMethods.Themes.clientdossier;

            smmWindowVC vc = new smmWindowVC();
            SMMWindow2 w = new SMMWindow2(vc, settings);
            w.ShowDialog();
        }
    }
}
