﻿using ClaraFey.BLL;
using ClaraFey.CommonObjects;
using sharedcode.BLL;
using sharedcode.CommonObjects;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClaraFey.ViewControllers.Discussion
{
   public class SaveDiscussionViewController : INotifyPropertyChanged/*, IClaraFeyCleanup*/
    {

        public List<User> usersList { get; set; }
        public List<User> selectedUsersList { get; set; }
        public SaveDiscussionViewController()
        {
            selectedUsersList = new List<User>();
            usersList = UserBLL.GetAllActiveUsers();

        }

        void NotifiyPropertyChanged(string property)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(property));
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;

        public void AddUser(User u)
        {
            this.selectedUsersList.Add(u);
            NotifiyPropertyChanged("selectedUsersList");
        }

        public void CleanUp()
        {
            throw new NotImplementedException();
        }
    }
}
