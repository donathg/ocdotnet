﻿using ClaraFey.BLL;
using sharedcode.BLL;
using sharedcode.WeergaveClasses;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClaraFey.ViewControllers.Werkbonnenbeheer
{
    public class InventarisPopupVC : INotifyPropertyChanged/*,IClaraFeyCleanup*/
    {
        public int LocationId { get; set; }

        private InventarisBLL _bllInventaris;
        public InventarisPopupVC(int locationId, int? inventarisItemId, List<ToestelCategorieType> toestelCategorieList)
        {
            LocationId = locationId;           

            _bllInventaris = new InventarisBLL(new InventarisSettings() { Geinventariseerd = true, LoadUitdienst = false, LocatieId = LocationId, AddEmptyRecord = true, ToestelCategorien = toestelCategorieList, StatusTypes = new List<InventarisStatusType> { InventarisStatusType.Actief, InventarisStatusType.Afgeschreven } });
            InventarisItems = _bllInventaris.GetAllInventarisItems();
            if (inventarisItemId.HasValue)
            {
                try
                {
                    SelectedInventarisItem = this.InventarisItems.Single(x => x.id == inventarisItemId);
                }
                catch { SelectedInventarisItem = null; }
            }
        }


        public List<LeverancierItem> LeveranciersItems
        {
            get
            {
                return _bllInventaris.GetAllLeveranciers();
            }
        }
        public List<toestel> ToestelItems
        {

            get
            {
                return _bllInventaris.GetAllToestellen();
            }
        }
        public List<merk> MerkItems
        {

            get
            {
                return _bllInventaris.GetAllMerken();
            }
        }

        private InventarisWeergave _selectedInventarisItem;
        public InventarisWeergave SelectedInventarisItem
        {
            get { return _selectedInventarisItem; }
            set
            {

                if (_selectedInventarisItem != value)
                {
                    _selectedInventarisItem = value;
                    NotifiyPropertyChanged("SelectedInventarisItem");
                }
            }
        }
        public List<InventarisWeergave> InventarisItems
        {
            get;
            set;

        }
        void NotifiyPropertyChanged(string property)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(property));
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;

        public void CleanUp()
        {
            this._bllInventaris.CleanUp();
            this._bllInventaris = null;

        
        }
    }
}
