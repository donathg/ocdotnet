﻿
using System;
using System.Collections.Generic;
using System.Linq;

using System.ComponentModel;

using System.Collections.ObjectModel;

using sharedcode.BLL;
using sharedcode.CommonObjects;
using sharedcode.common;
using System.Windows;

namespace ClaraFey.ViewControllers.Werkbonnenbeheer
{
    public class WerkbonnenbeheerViewController : INotifyPropertyChanged/*, IClaraFeyCleanup*/
    {
        /** FIELDS */
        public event PropertyChangedEventHandler PropertyChanged;

        private string _activatedWoType;
        private List<WerkopdrachtItem> _WAList = new List<WerkopdrachtItem>();
        private List<WerkopdrachtItem> _WBList = new List<WerkopdrachtItem>();
        private HistoryType _historyType;
        public String TooltipHistoryButton
        {
            get
            {
                if (ViewMode == WerkopdrachtType.Werkaanvraag)
                {
                    return "gesloten werkaanvragen tonen";

                }
                if (ViewMode == WerkopdrachtType.Werkbon)
                {
                    return "gesloten werkbonnen tonen";

                }
                return "";

            }

        }
        //De kolom met het oog (gelezen) enkel voor Supervisors and TD
        public bool ReadColumnVisible
        {

            get
            {
                if (Settings.UserType == WerkopdrachtUserType.supervisor)
                    return true;
                if (Settings.UserType == WerkopdrachtUserType.excecuter)
                    return true;
                return false;


            }
        }
        //bijlagen avtiveren voor Werkbonnen
        public bool CanShowWBBijlagen
        {
            get
            {


                if (Settings.UserType == WerkopdrachtUserType.supervisor)
                    return true;
                if (Settings.UserType == WerkopdrachtUserType.excecuter)
                    return true;
                return false;
            }

        }
        public ObservableCollection<WerkopdrachtItem> WBItems
        {
            get
            {
                if (this.BllWerkbonnenBeheer == null)
                    return new ObservableCollection<WerkopdrachtItem>();
                if (this.ViewMode == WerkopdrachtType.Werkaanvraag)
                {
                    if (SelectedWAItem != null)
                        return new ObservableCollection<WerkopdrachtItem>(this.BllWerkbonnenBeheer.Werkbonnen.Where(wb => wb.Parent_id == this.SelectedWAItem.Id));
                    else
                        return new ObservableCollection<WerkopdrachtItem>();
                }
                else
                {

                    if (this._historyType == HistoryType.active)
                        return new ObservableCollection<WerkopdrachtItem>(this.BllWerkbonnenBeheer.Werkbonnen.Where(wb => wb.StatusId != (int)WerkopdrachtStatusType.wb_10_gesloten));
                    else
                        return new ObservableCollection<WerkopdrachtItem>(this.BllWerkbonnenBeheer.Werkbonnen.Where(wb => wb.StatusId == (int)WerkopdrachtStatusType.wb_10_gesloten));
                }
            }
        }
        public ObservableCollection<WerkopdrachtItem> WAItems
        {
            get
            {
                if (this.BllWerkbonnenBeheer == null)
                    return new ObservableCollection<WerkopdrachtItem>();
                if (this.ViewMode == WerkopdrachtType.Werkbon)
                {

                    if (SelectedWBItem != null)
                        return new ObservableCollection<WerkopdrachtItem>(this.BllWerkbonnenBeheer.Werkaanvragen.Where(wb => wb.Id == this.SelectedWBItem.Parent_id));
                    else
                    {
                        return new ObservableCollection<WerkopdrachtItem>();
                    }
                }
                else
                {

                    if (this._historyType == HistoryType.active)
                        return new ObservableCollection<WerkopdrachtItem>(this.BllWerkbonnenBeheer.Werkaanvragen.Where(wb => (wb.StatusId != (int)WerkopdrachtStatusType.wa_9_gesloten &&
                        wb.StatusId != (int)WerkopdrachtStatusType.wa_8_afgewezen
                        )));
                    else
                        return new ObservableCollection<WerkopdrachtItem>(this.BllWerkbonnenBeheer.Werkaanvragen.Where(wb => (wb.StatusId == (int)WerkopdrachtStatusType.wa_9_gesloten)
                        || wb.StatusId == (int)WerkopdrachtStatusType.wa_8_afgewezen
                        ));

                }
            }
        }
        private WerkopdrachtItem _selectedWaItem;
        public WerkopdrachtItem SelectedWAItem
        {
            get { return _selectedWaItem; }
            set
            {
                if (_selectedWaItem != value)
                {
                    _selectedWaItem = value;
                    if (_activatedWoType == "WA")
                        SelectedWOItem = value;
                    NotifiyPropertyChanged("SelectedWAItem");
                    if (ViewMode == WerkopdrachtType.Werkaanvraag)
                        NotifiyPropertyChanged("WBItems");
                }

            }
        }
        private WerkopdrachtItem _selectedWbItem;
        public WerkopdrachtItem SelectedWBItem
        {
            get { return _selectedWbItem; }
            set
            {
                if (_selectedWbItem != value)
                {
                    _selectedWbItem = value;
                    if (_activatedWoType == "WB")
                        SelectedWOItem = value;

                    NotifiyPropertyChanged("SelectedWBItem");


                    if (this.WAItems != null && this.WAItems.Count > 0 && this.ViewMode == WerkopdrachtType.Werkbon && value != null)
                    {
                        NotifiyPropertyChanged("WAItems");
                        _selectedWaItem = this.WAItems[0];
                        NotifiyPropertyChanged("SelectedWAItem");
                    }
                }
            }
        }
        public bool isUitgebreideBeschrijving
        {
            get
            {
                if (SelectedWOItem == null)
                    return false;

                return !String.IsNullOrWhiteSpace(SelectedWOItem.Beschrijving);
            }
        }
        private WerkopdrachtItem _selectedWOItem;
        /// <summary>
        /// de laatst geslecteerde werkopdracht(kan WA of WB zijn)
        /// </summary>
        public WerkopdrachtItem SelectedWOItem
        {
            get
            {
                return _selectedWOItem;
            }
            set
            {
                if (_selectedWOItem != value)
                {
                    _selectedWOItem = value;

                    NotifyCanProperties();
                }
            }
        }
        public bool CanEmailWA
        {
            get
            {
                return true;
            }
        }
        public bool CanEmailWB
        {
            get
            {
                return Settings.UserType != WerkopdrachtUserType.normal;
            }
        }
        private Location _selectedLocatieItem;
        public Location SelectedLocatieItem
        {
            get { return _selectedLocatieItem; }
            set
            {
                if (_selectedLocatieItem != value)
                {
                    _selectedLocatieItem = value;
                    SetWerkopdrachtItemLocation(value);
                    NotifiyPropertyChanged("SelectedLocatieItem");
                }
            }
        }
        public GebouwenBeheerBLL BllGebouwenBeheer { get; set; }
        public WerkbonnenBeheerBLL BllWerkbonnenBeheer { get; set; }
        public List<PrioriteitItem> PrioriteitItems
        {
            get { return this.BllWerkbonnenBeheer.PrioriteitItems; }
        }
        public List<TypeItem> TypeItems
        {
            get { return this.BllWerkbonnenBeheer.TypeItems; }
        }
        public List<StatusItem> StatusItems
        {
            get
            {
                if (this._activatedWoType == "WA")
                    return StatusItemsWA;
                else
                    return StatusItemsWB;
            }
        }
        public List<StatusItem> StatusItemsWA
        {
            get
            {
                return this.BllWerkbonnenBeheer.StatusItems.Where(s => s.Categorie == "WA").ToList();
            }
        }
        public List<StatusItem> StatusItemsWB
        {
            get
            {
                return this.BllWerkbonnenBeheer.StatusItems.Where(s => s.Categorie == "WB").ToList();
            }
        }
        #region Can-Properties
        public bool CanAddWaWizard
        {
            get
            {
                if (this._historyType == HistoryType.history)
                    return false;
                if (this.Settings.UserType == WerkopdrachtUserType.excecuter)
                    return false;
                return this.ViewMode == WerkopdrachtType.Werkaanvraag;
            }
        }
        public bool CanOpenWB
        {
            get
            {
                if (this._historyType == HistoryType.history)
                    return false;
                bool can = false;
                if (this.SelectedWBItem != null)
                {
                    //enkel personen met supervisor rechten 
                    /* if (this.SelectedWBItem.Dienst == WerkopdrachtDienstType.TECHD && GlobalData.Instance.LoggedOnUser.GetAccesType("325") == AccessType.write)
                         can = true; 

                     else if (this.SelectedWBItem.Dienst == WerkopdrachtDienstType.IT && GlobalData.Instance.LoggedOnUser.GetAccesType("330") == AccessType.write)
                         can = true;
                     //else if (GlobalData.Instance.LoggedOnUser.GetAccesType("320") == AccessType.write)
                     //    can = true; //Techniekers*/
                    if (Settings.UserType == WerkopdrachtUserType.supervisor /* || Settings.UserType == WerkopdrachtUserType.technician*/)
                        can = true;


                }
                return can;
            }

        }
        public bool CanOpenWA
        {

            get
            {

                if (this._historyType == HistoryType.history)
                    return false;

                if (this.SelectedWAItem != null)
                {

                    return true;

                }

                return false;

            }


        }
        public bool CanAddWB
        {
            get
            {
                if (this._historyType == HistoryType.history)
                    return false;
                bool can = false;
                if (this.SelectedWAItem != null && (this.SelectedWAItem.StatusId == (int)WerkopdrachtStatusType.wa_5_nieuwe_aanvraag
                   || this.SelectedWAItem.StatusId == (int)WerkopdrachtStatusType.wa_6_gelezen || this.SelectedWAItem.StatusId == (int)WerkopdrachtStatusType.wa_7_goedgekeurd))
                {
                    /* //enkel personen met supervisor rechten 
                     if (this.SelectedWAItem.Dienst == WerkopdrachtDienstType.TECHD && GlobalData.Instance.LoggedOnUser.GetAccesType("325") == AccessType.write)
                         can = true;
                     else if (this.SelectedWAItem.Dienst == WerkopdrachtDienstType.IT && GlobalData.Instance.LoggedOnUser.GetAccesType("330") == AccessType.write)
                         can = true;*/

                    if (Settings.UserType == WerkopdrachtUserType.supervisor)
                        can = true;

                }

                return can;
            }

        }
        public bool CanChangeStatusWB
        {
            get
            {
                if (this._historyType == HistoryType.history)
                    return false;

                bool can = false;
                if (this.SelectedWBItem != null)
                {
                    /*  //enkel personen met supervisor rechten 
                      if (this.SelectedWBItem.Dienst == WerkopdrachtDienstType.TECHD && GlobalData.Instance.LoggedOnUser.GetAccesType("325") == AccessType.write)
                          can = true;
                      else if (this.SelectedWBItem.Dienst == WerkopdrachtDienstType.IT && GlobalData.Instance.LoggedOnUser.GetAccesType("330") == AccessType.write)
                          can = true;*/

                    if (Settings.UserType == WerkopdrachtUserType.supervisor)
                        can = true;

                    //en enkel personen met recht 320 mijn werkbonnen als de status niet gesloten is door Supervisor
                    else if (this.SelectedWBItem.StatusId != (int)WerkopdrachtStatusType.wb_10_gesloten)
                    {
                        if (Settings.UserType == WerkopdrachtUserType.excecuter)
                            can = true;

                    }

                }
                return can;


            }
        }
        /// <summary>
        /// Heeft de ingeloggde gebruiker een Supervisor recht ? Techd of IT
        /// </summary>
        public bool HasSupererVisorRights
        {
            get
            {
                return Settings.UserType == WerkopdrachtUserType.supervisor;
            }

        }
        /// <summary>
        /// Vanuit Werkaanvragen naar Werkbonnen springen en vice versa
        /// </summary>
        public bool CanOpenOtherWindow
        {
            get
            {
                return HasSupererVisorRights && !(this._historyType == HistoryType.history);
            }
        }
        public bool CanRefresh
        {
            get
            {
                return !(this._historyType == HistoryType.history);
            }
        }
        /// <summary>
        /// Kan de ingeloggde gebruiker de status van een Werkaanvraag wijzigen ?
        /// </summary>
        public bool CanChangeStatusWA
        {
            get
            {
                if (this._historyType == HistoryType.history)
                    return false;

                bool can = false;
                if (this.SelectedWAItem != null)
                {
                    if (SelectedWAItem.StatusId == 9)
                        return false;
                    /* //enkel personen met supervisor rechten 
                     if (this.SelectedWAItem.Dienst == WerkopdrachtDienstType.TECHD && GlobalData.Instance.LoggedOnUser.GetAccesType("325") == AccessType.write)
                         can = true;
                     else if (this.SelectedWAItem.Dienst == WerkopdrachtDienstType.IT && GlobalData.Instance.LoggedOnUser.GetAccesType("330") == AccessType.write)
                         can = true;
                         */
                    if (Settings.UserType == WerkopdrachtUserType.supervisor)
                        return true;
                }
                return can;
            }
        }
        /// <summary>
        /// Kan de ingeloggde gebruiker een Werkaanvraag toevoegen
        /// </summary>
        public bool CanAddWA
        {
            get
            {
                if (this.ViewMode == WerkopdrachtType.Werkbon)
                    return false;
                if (this._historyType == HistoryType.history)
                    return false;
                bool can = false;

                //enkel personen met supervisor IT rechten 
                if (Settings.UserType == WerkopdrachtUserType.supervisor)
                    can = true;



                return can;
            }

        }
        /// <summary>
        /// Kan de ingeloggde gebruiker een Werkbon kopieren
        /// </summary>
        public bool CanCopyWB
        {
            get
            {
                if (this._historyType == HistoryType.history)
                    return false;
                bool can = false;
                if (this.SelectedWBItem != null)
                {

                    //enkel personen met supervisor IT rechten 
                    if (Settings.UserType == WerkopdrachtUserType.supervisor)
                        can = true;

                }
                return can;
            }
        }
        /// <summary>
        /// Kan de ingeloggde gebruiker een Werkaanvraag kopieren
        /// </summary>
        public bool CanCopyWA
        {
            get
            {
                if (this.ViewMode == WerkopdrachtType.Werkbon)
                    return false;
                if (this._historyType == HistoryType.history)
                    return false;
                bool can = false;
                if (this.SelectedWAItem != null)
                {
                    //enkel personen met supervisor IT rechten 
                    if (Settings.UserType == WerkopdrachtUserType.supervisor)
                        can = true;

                }
                return can;
            }

        }
        public bool CanShowWAStatusHistory
        {
            get
            {

                bool can = false;
                if (this.SelectedWAItem != null)
                    can = true;
                return can;
            }

        }
        public bool CanShowWBStatusHistory
        {
            get
            {

                bool can = false;
                if (this.SelectedWBItem != null)
                    can = true;
                return can;
            }

        }
        public bool CanPrintWA
        {
            get
            {

                bool can = false;
                if (this.SelectedWAItem != null)
                    can = true;
                return can;
            }

        }
        public bool CanPrintGantt
        {
            get
            {

                if (this._historyType == HistoryType.history)
                    return false;
                //enkel personen met supervisor IT rechten 
                if (Settings.UserType == WerkopdrachtUserType.supervisor)
                    return true;

                return false;

            }

        }
        public bool CanPrintWB
        {
            get
            {

                bool can = false;
                if (this.SelectedWBItem != null)
                    can = true;
                return can;
            }

        }
        public WerkopdrachtType ViewMode { get; set; }
        public WerkbonnenBeheerSettings2 Settings { get; set; }

        public List<WerkopdrachtEmailAddress> GetEmailAdressWA()
        {
            List<WerkopdrachtEmailAddress> list = new List<WerkopdrachtEmailAddress>();
            if (this.Settings.UserType == WerkopdrachtUserType.supervisor || this.Settings.UserType == WerkopdrachtUserType.excecuter)
                list = WerkbonnenBeheerBLL.GetEmailList(this.SelectedWAItem.AanvragerId.GetValueOrDefault());
            else if (this.Settings.UserType == WerkopdrachtUserType.normal)
            {
                WerkopdrachtEmailAddress em = new WerkopdrachtEmailAddress()
                {
                    isChecked = true,
                    isGroupEmailAddress = true,
                    name = "Clara Fey Werkaanvragen"
                };
                if (this.Settings.Dienst == "TECHD")
                    em.eMailadress = "clarafey.werkaanvragen@fracarita.org";
                if (this.Settings.Dienst == "IT")
                    em.eMailadress = "clarafey.it@fracarita.org";
                list.Add(em);

            }

            return list;

        }
        public List<WerkopdrachtEmailAddress> GetEmailAdressWB()
        {
            List<WerkopdrachtEmailAddress> list = new List<WerkopdrachtEmailAddress>();
            if (this.Settings.UserType == WerkopdrachtUserType.normal)
                return list;

            if (this.Settings.UserType == WerkopdrachtUserType.supervisor && this.Settings.Dienst == "TECHD")
            {
                List<String> addedGroepen = new List<string>();
                List<User> usersTD = UserBLL.GetUsersForDienst(this.Settings.Dienst);
                foreach (User u in usersTD)
                {
                    if (this.SelectedWBItem.MedewerkersString.Contains(u.Name))
                    {
                        addedGroepen.Add(u.SubGroep);
                        if (!addedGroepen.Contains("bouw"))
                        {
                            String email = UserBLL.GetTechnischeDienstEmailAdress(u.SubGroep);
                            WerkopdrachtEmailAddress em = new WerkopdrachtEmailAddress()
                            {
                                isChecked = true,
                                isGroupEmailAddress = true,
                                name = "Technische Dienst " + u.SubGroep,
                                eMailadress = email
                            };
                            list.Add(em);
                        }
                    }
                }

                if (!addedGroepen.Contains("bouw"))
                {
                    WerkopdrachtEmailAddress em = new WerkopdrachtEmailAddress()
                    {
                        isChecked = this.SelectedWBItem.Groepen.Contains("bouw"),
                        isGroupEmailAddress = true,
                        name = "Technische Dienst Bouw",
                        eMailadress = UserBLL.GetTechnischeDienstEmailAdress("bouw")
                    };
                    list.Add(em);
                }
                if (!addedGroepen.Contains("elektro"))
                {
                    WerkopdrachtEmailAddress em = new WerkopdrachtEmailAddress()
                    {
                        isChecked = this.SelectedWBItem.Groepen.Contains("elektro"),
                        isGroupEmailAddress = true,
                        name = "Technische Dienst Elektro",
                        eMailadress = UserBLL.GetTechnischeDienstEmailAdress("elektro")
                    };
                    list.Add(em);
                }

                if (!addedGroepen.Contains("hout"))
                {
                    WerkopdrachtEmailAddress em = new WerkopdrachtEmailAddress()
                    {
                        isChecked = this.SelectedWBItem.Groepen.Contains("hout"),
                        isGroupEmailAddress = true,
                        name = "Technische Dienst Hout",
                        eMailadress = UserBLL.GetTechnischeDienstEmailAdress("hout")
                    };
                    list.Add(em);
                }


                if (!addedGroepen.Contains("metaal"))
                {
                    WerkopdrachtEmailAddress em = new WerkopdrachtEmailAddress()
                    {
                        isChecked = this.SelectedWBItem.Groepen.Contains("metaal"),
                        isGroupEmailAddress = true,
                        name = "Technische Dienst Metaal",
                        eMailadress = UserBLL.GetTechnischeDienstEmailAdress("metaal")
                    };
                    list.Add(em);
                }
                if (!addedGroepen.Contains("loodgieter"))
                {
                    WerkopdrachtEmailAddress em = new WerkopdrachtEmailAddress()
                    {
                        isChecked = this.SelectedWBItem.Groepen.Contains("loodgieter"),
                        isGroupEmailAddress = true,
                        name = "Technische Dienst Sanitair",
                        eMailadress = UserBLL.GetTechnischeDienstEmailAdress("loodgieter")
                    };
                    list.Add(em);
                }

                if (!addedGroepen.Contains("schilder"))
                {
                    WerkopdrachtEmailAddress em = new WerkopdrachtEmailAddress()
                    {
                        isChecked = this.SelectedWBItem.Groepen.Contains("schilder"),
                        isGroupEmailAddress = true,
                        name = "Technische Dienst schilder",
                        eMailadress = UserBLL.GetTechnischeDienstEmailAdress("schilder")
                    };
                    list.Add(em);
                }

                if (!addedGroepen.Contains("tuin"))
                {
                    WerkopdrachtEmailAddress em = new WerkopdrachtEmailAddress()
                    {
                        isChecked = this.SelectedWBItem.Groepen.Contains("tuin"),
                        isGroupEmailAddress = true,
                        name = "Technische Dienst tuin",
                        eMailadress = UserBLL.GetTechnischeDienstEmailAdress("tuin")
                    };
                    list.Add(em);
                }

 

            }

            if (this.Settings.UserType == WerkopdrachtUserType.supervisor && this.Settings.Dienst == "IT")
            {
                WerkopdrachtEmailAddress em = new WerkopdrachtEmailAddress()
                {
                    isChecked = true,
                    isGroupEmailAddress = true,
                    name = "Technische Dienst tuin",
                    eMailadress = "clarafey.it@fracarita.org"
                };
                list.Add(em);

            }

            if (this.Settings.UserType == WerkopdrachtUserType.excecuter && (this.Settings.Dienst == "IT" || this.SelectedWBItem.Dienst == "IT"))
            {
                WerkopdrachtEmailAddress em = new WerkopdrachtEmailAddress()
                {
                    isChecked = true,
                    isGroupEmailAddress = true,
                    name = "Technische Dienst tuin",
                    eMailadress = "clarafey.it@fracarita.org"
                };
                list.Add(em);

            }

            if (this.Settings.UserType == WerkopdrachtUserType.excecuter && (this.Settings.Dienst == "TECHD" || this.SelectedWBItem.Dienst == "TECHD"))
            {
                WerkopdrachtEmailAddress em = new WerkopdrachtEmailAddress()
                {
                    isChecked = true,
                    isGroupEmailAddress = true,
                    name = "Technische Dienst tuin",
                    eMailadress = "clarafey.werkaanvragen@fracarita.org"
                };
                list.Add(em);

            }
            return list;

        }
        public void SendEmailOutllook(List<String> emailAdresses, WerkopdrachtItem wo)
        {
            try
            {
                String subject = "Vraag/opmerking : werkaanvraag WA" + wo.Id;
                String body = "Betreft : " + wo.Titel + "%0D%0A";
                body += "Locatie : " + wo.LocatiePath + "%0D%0A%0D%0A";
                body += @"

mvg,%0D%0A" + GlobalData.Instance.LoggedOnUser.Name + "%0D%0A";
                String emails = String.Empty;


                foreach (String e in emailAdresses)
                {
                    if (emails != String.Empty)
                        emails = emails + ";";
                    emails = emails + e;

                }

                var url = String.Format("mailto:{0}?subject={1}&body={2}", emails, subject, body);
                System.Diagnostics.Process.Start(url);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Foutmelding", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        #endregion

        /** CONSTRUCTOR */
        public WerkbonnenbeheerViewController(WerkopdrachtType viewMode, werkopdracht_diensten dienst, WerkopdrachtUserType userType)
        {
            this.ViewMode = viewMode;
            Settings = new WerkbonnenBeheerSettings2(ViewMode)
            {
                Dienst = dienst.wodienst_code,
                UserType = userType
            };
            if (userType == WerkopdrachtUserType.excecuter)
            {
                if (!String.IsNullOrWhiteSpace(GlobalData.Instance.LoggedOnUser.SubGroep))
                    Settings.AddSubGroupToMyWBFilter(GlobalData.Instance.LoggedOnUser.SubGroep);
                //zijn userId toevoegen aanFilter
                Settings.AddUserToMyWBFilter(GlobalData.Instance.LoggedOnUser.Id.GetValueOrDefault());


            }



            //enkel die van uw eigen groep of die van locatie(s) waaraan je bent toegewezen
            Settings.SetGroepFilter(GlobalData.Instance.LoggedOnUser.GetUserFunctieGroepen(true));
            if (Settings.UserType == WerkopdrachtUserType.normal)
            {
                List<int> locList = GlobalData.Instance.LoggedOnUser.GetLocationsForDienst(Settings.Dienst);
                if (locList == null || locList.Count == 0)
                    Settings.AddLocationToFilter(-1);
                foreach (KeyValuePair<string, List<int>> loc in GlobalData.Instance.LoggedOnUser.Locations)
                    Settings.AddLocationToFilter(GlobalData.Instance.LoggedOnUser.GetLocationsForDienst(Settings.Dienst));
            }
            this.BllWerkbonnenBeheer = new WerkbonnenBeheerBLL(Settings);
            this.BllGebouwenBeheer = new GebouwenBeheerBLL(new GebouwenBeheerSettings());
            this.BllWerkbonnenBeheer.LoadWerkopdrachten(HistoryType.active);
        }

        /** METHODS */
        /// <summary>
        /// De active grid(focused) bepaalt welke rij in de detail vensters wordt getoond
        /// </summary>
        /// <param name="WoType"></param>
        public void ActivateGrid(string WoType)
        {
            _activatedWoType = WoType;

            if (WoType == "WA")
            {
                if (SelectedWOItem != SelectedWAItem)
                {
                    SelectedWOItem = SelectedWAItem;
                    NotifiyPropertyChanged("StatusItems");
                    NotifiyPropertyChanged("SelectedWOItem");
                }
            }
            else
            {
                if (SelectedWOItem != SelectedWBItem)
                {
                    SelectedWOItem = SelectedWBItem;
                    NotifiyPropertyChanged("StatusItems");
                    NotifiyPropertyChanged("SelectedWOItem");
                }
            }

        }
        public void Refresh()
        {
            this.BllWerkbonnenBeheer.LoadWerkopdrachtenNew();
            NotifiyPropertyChanged("WBItems");
            NotifiyPropertyChanged("WAItems");
        }
        public void ChangeHistoryType(HistoryType type)
        {
            this._historyType = type;
            SelectedWAItem = null;
            SelectedWBItem = null;
            SelectedWOItem = null;
            NotifiyPropertyChanged("SelectedWAItem");
            NotifiyPropertyChanged("SelectedWBItem");
            NotifiyPropertyChanged("SelectedWOItem");
            if (this.ViewMode == WerkopdrachtType.Werkaanvraag)
                ActivateGrid("WA");
            else
                ActivateGrid("WB");

            this.BllWerkbonnenBeheer.LoadWerkopdrachten(type);

            NotifiyPropertyChanged("WBItems");
            NotifiyPropertyChanged("WAItems");
            NotifyCanProperties();
        }
        public void CleanUp()
        {
            this.BllWerkbonnenBeheer.Dispose();
            this.BllGebouwenBeheer = null;
            this.BllWerkbonnenBeheer = null;
            this._selectedWaItem = null;
            this._selectedWbItem = null;
            this._selectedWOItem = null;
        }
        public void NotifyCanProperties()
        {
            NotifiyPropertyChanged(nameof(SelectedWOItem));
            NotifiyPropertyChanged(nameof(CanAddWB));
            NotifiyPropertyChanged(nameof(CanAddWaWizard));
            NotifiyPropertyChanged(nameof(CanAddWA));
            NotifiyPropertyChanged(nameof(CanCopyWB));
            NotifiyPropertyChanged(nameof(CanCopyWA));
            NotifiyPropertyChanged(nameof(CanShowWAStatusHistory));
            NotifiyPropertyChanged(nameof(CanShowWBStatusHistory));
            NotifiyPropertyChanged(nameof(CanPrintWA));
            NotifiyPropertyChanged(nameof(CanPrintWB));
            NotifiyPropertyChanged(nameof(CanChangeStatusWB));
            NotifiyPropertyChanged(nameof(CanChangeStatusWA));
            NotifiyPropertyChanged(nameof(CanPrintGantt));
            NotifiyPropertyChanged(nameof(isUitgebreideBeschrijving));
            NotifiyPropertyChanged(nameof(CanOpenOtherWindow));
            NotifiyPropertyChanged(nameof(CanRefresh));
        }
        private void SetWerkopdrachtItemLocation(Location loc)
        {
            if (loc != null)
            {
                this._selectedWOItem.LocatieId = loc.Id;
                this._selectedWOItem.LocatiePath = loc.GetFullPath();
            }
        }
        /// <summary>
        /// Deze methode selecteerd een item(treenode) van de locatieboom
        /// </summary>
        /// <param name="id"></param>
        public void SelectLocation(int? id)
        {
            this.BllGebouwenBeheer.SelectLocation(id);
        }
        public void AddWerkaanvraag(WerkopdrachtItem wo)
        {
            wo.Id = null;
            this.BllWerkbonnenBeheer.Werkaanvragen.Add(wo);
            NotifiyPropertyChanged(nameof(WAItems));
            this.SelectedWAItem = wo;

        }
        public void AddWerkbon(WerkopdrachtItem newWa, WerkopdrachtItem newWb)
        {
            newWb.Id = null;
            this.BllWerkbonnenBeheer.Werkbonnen.Add(newWb);
            this.Save();
            newWa.StatusId = 7;// werkaanvraag automatisch op goedgekeurd zetten (de save procedure op de databank doet dit automatisch)
            NotifiyPropertyChanged("WBItems");
            this.SelectedWBItem = newWb;
        }
        public void Save()
        {
            this.BllWerkbonnenBeheer.Save();
        }
        void NotifiyPropertyChanged(string property)
        {

            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(property));
        }
    }
}
