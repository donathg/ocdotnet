﻿using ClaraFey.BLL;

using sharedcode.BLL;
using sharedcode.common;
using sharedcode.CommonObjects;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClaraFey.ViewControllers.Werkbonnenbeheer
{
    public class SMMWerkbonnenBeheerLocatieVC
    {
        private GebouwenBeheerBLL _bll = new GebouwenBeheerBLL(new GebouwenBeheerSettings());

        public GebouwenBeheerBLL Bll
        {
            get { return _bll; }
            set { _bll = value; }
        }

        void NotifiyPropertyChanged(string property)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(property));
        }
        public bool CanModify
        {
            get
            {
                return GlobalData.Instance.LoggedOnUser.GetAccesType("100") == AccessType.access;
            }
        }
        public ObservableCollection<Location> Locations
        {
            get
            {
                return _bll.Locations;
            }

        }
        public event PropertyChangedEventHandler PropertyChanged;
        public void CleanUp()
        {
            _bll = null;
        }
    }
}
