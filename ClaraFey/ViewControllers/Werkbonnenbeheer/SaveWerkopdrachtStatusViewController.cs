﻿

using sharedcode.BLL;
using sharedcode.common;
using sharedcode.CommonObjects;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClaraFey.ViewControllers.Werkbonnenbeheer
{
    public class SaveWerkopdrachtStatusViewController : INotifyPropertyChanged/*, IClaraFeyCleanup*/
    {
        WerkopdrachtItem _werkOpdrachtItem = null;
        public SaveWerkopdrachtStatusViewController(WerkopdrachtItem werkOpdrachtItem)
        {
            this._werkOpdrachtItem = werkOpdrachtItem;



        }
        public WerkopdrachtItem WerkOpdrachtItem
        {
            get { return _werkOpdrachtItem; }
            set { _werkOpdrachtItem = value; }
        }



        public String Opmerking
        {
            get { return this.WerkOpdrachtItem.StatusOpmerking; }
            set
            {
                if (this.WerkOpdrachtItem.StatusOpmerking != value)
                {
                    this.WerkOpdrachtItem.StatusOpmerking = value;
                    NotifiyPropertyChanged("Opmerking");
                }
            }
        }
        public int? StatusId
        {
            get
            {
                return this.WerkOpdrachtItem.StatusId;
            }
            set
            {
                if (this.WerkOpdrachtItem.StatusId != value)
                {
                    this.WerkOpdrachtItem.StatusId = value;
                    this.Opmerking = "";
                    NotifiyPropertyChanged("StatusId");
                }
            }
        }
        void NotifiyPropertyChanged(string property)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(property));
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;

        public List<StatusItem> StatusItems
        {
            get
            {
                if (this.WerkOpdrachtItem.WerkopdrachtType == WerkopdrachtType.Werkaanvraag)
                    return StatusItemsWA;
                else
                    return StatusItemsWB;
            }
        }

        /// <summary>
        /// Geeft een lijst terug met alle mogelijk statussen voor Werkaanvraag(alles hoger dan gelezen)
        /// </summary>
        public List<StatusItem> StatusItemsWA
        {
            get
            {

                if (_werkOpdrachtItem.StatusId == (int)WerkopdrachtStatusType.wa_5_nieuwe_aanvraag)
                {
                    return WerkbonnenBeheerBLL.GetStatusItems().Where(s => s.Categorie == "WA" &&
                        (s.Id == (int)WerkopdrachtStatusType.wa_5_nieuwe_aanvraag || s.Id == (int)WerkopdrachtStatusType.wa_8_afgewezen)).ToList();

                }
                else if (_werkOpdrachtItem.StatusId == (int)WerkopdrachtStatusType.wa_6_gelezen)
                {
                    return WerkbonnenBeheerBLL.GetStatusItems().Where(s => s.Categorie == "WA" &&
                        (s.Id == (int)WerkopdrachtStatusType.wa_6_gelezen || s.Id == (int)WerkopdrachtStatusType.wa_8_afgewezen)).ToList(); ;

                }
                else if (_werkOpdrachtItem.StatusId == (int)WerkopdrachtStatusType.wa_7_goedgekeurd)
                {
                    return WerkbonnenBeheerBLL.GetStatusItems().Where(s => s.Categorie == "WA" &&
                        (s.Id == (int)WerkopdrachtStatusType.wa_7_goedgekeurd)).ToList();

                }
                else if (_werkOpdrachtItem.StatusId == (int)WerkopdrachtStatusType.wa_8_afgewezen)
                {
                    return WerkbonnenBeheerBLL.GetStatusItems().Where(s => s.Categorie == "WA" &&
                       (s.Id == (int)WerkopdrachtStatusType.wa_8_afgewezen)).ToList();

                }

                return new List<StatusItem>();



            }
        }
        public List<StatusItem> StatusItemsWB
        {
            get
            {
                //enkel de supervisor en enkel wanneer de status op afgewerkt staat kan hem sluiten
                if (GlobalData.Instance.LoggedOnUser.GetAccesType("325") == AccessType.access
                  || GlobalData.Instance.LoggedOnUser.GetAccesType("330") == AccessType.access)
                {

                    if (this.WerkOpdrachtItem.StatusId == (int)WerkopdrachtStatusType.wb_10_gesloten)
                        return WerkbonnenBeheerBLL.GetStatusItems().Where(s => s.Categorie == "WB" && s.Id == (int)WerkopdrachtStatusType.wb_10_gesloten).ToList();
                    else 
                    return WerkbonnenBeheerBLL.GetStatusItems().Where(s => s.Categorie == "WB").ToList();
                }
                else
                {

                    //geen supervisor
                    return WerkbonnenBeheerBLL.GetStatusItems().Where(s => s.Categorie == "WB" && s.Id != (int)WerkopdrachtStatusType.wb_10_gesloten).ToList();
                }
            }
        }

        public void CleanUp()
        {
            this._werkOpdrachtItem = null;

        }
    }

}
