﻿

using sharedcode.BLL;
using sharedcode.common;
using sharedcode.CommonObjects;
using sharedcode.WeergaveClasses;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
 

namespace ClaraFey.ViewControllers.Werkbonnenbeheer
{
    public class SaveWerkOpdrachtViewController : INotifyPropertyChanged /*, IClaraFeyCleanup*/
    {
        /** INNER CLASS UserWOView */
        public class UserWOView : INotifyPropertyChanged
        {
            public callBackChecked myCallBack;
            public int Id { get; set; }
            public String FirstName { get; set; }
            public String LastName { get; set; }
            public String SubGroep { get; set; }
            public String Name
            {
                get { return FirstName + " " + LastName; }
            }
            public String NameSubGroep
            {
                get { return Name + " (" + SubGroep + ")"; }
            }
            private bool _userIsInSelectedGroup;
            public bool UserIsInSelectedGroup
            {
                get { return _userIsInSelectedGroup; }
                set
                {
                    _userIsInSelectedGroup = value;
                    NotifiyPropertyChanged("UserIsInSelectedGroup");
                }
            }
            private bool _isChecked;
            public bool IsChecked
            {
                get { return _isChecked; }
                set
                {
                    _isChecked = value;
                    myCallBack?.Invoke(this);
                }
            }
            public event PropertyChangedEventHandler PropertyChanged;
            void NotifiyPropertyChanged(string property)
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(property));
            }
        }
 


        /** properties, delegates & events*/
        public delegate void callBackChecked(UserWOView v);

        public event PropertyChangedEventHandler PropertyChanged;

        private WerkbonnenBeheerSettings2 _settings;
        public List<UserWOView> Groepen { get; set; }
        public List<UserWOView> Medewerkers { get; set; }
        public List<ToestelCategorieType> InventarisToestelCategorieList { get; set; }
        public bool IsEnabledTabPrioriteit
        {
            get {


                if (this.IsLocationisVisible)
                    return this.WerkOpdrachtItem.LocatieId != null;
                else return true;
            }
        }
        public bool IsEnabledTabInventaris
        {
            get {


                if (this.IsInventarisVisible == false)
                    return false;
                return this.WerkOpdrachtItem.LocatieId != null; }
        }
        public bool IsEnabledTabLocation
        {
            get {


                if (this.IsLocationisVisible == false)
                    return false;
                return (String.IsNullOrWhiteSpace(this.WerkOpdrachtItem.Dienst) == false); }
        }
        public bool IsEnabledTabSamenvatting
        {
            get
            {
                bool checkOk = IsEnabledTabPrioriteit && IsEnabledTabAlgemeneGegevens && !String.IsNullOrWhiteSpace(this.WerkOpdrachtItem.Titel);
                return checkOk;
            }
        }
        public bool IsEnabledTabAlgemeneGegevens
        {
            get
            {
                bool checkOk = IsEnabledTabPrioriteit && this.WerkOpdrachtItem.PrioriteitId.HasValue;
                return checkOk;
            }
        }
        public bool IsButtonNextEnabled
        {
            get
            {
                return NextIsEnabled(SelectedTabIndex);
            }
        }
        private bool NextIsEnabled (int currentIndex)
        {
            switch (SelectedTabIndex)
            {
                case 0: /*Dienst*/

                    if (IsLocationisVisible)
                        return IsEnabledTabLocation;
                    else if (IsInventarisVisible)
                        return IsInventarisVisible;
                    else return IsEnabledTabPrioriteit;

                case 1: /*Location*/
                    return IsEnabledTabInventaris;
                case 2: /*Inventaris*/
                    return IsEnabledTabPrioriteit;
                case 3: /*Prioriteit*/
                    return IsEnabledTabAlgemeneGegevens;
                case 4: /*alg gegevens*/
                    return IsEnabledTabSamenvatting;
                case 5: /*Samenvatting : eenmaal hier, knop wordt opslaan en is altijd toevoegbaar*/
                    return true;
            }
            return false;

        }
        private int _selectedTabIndex;
        public int SelectedTabIndex
        {
            get { return _selectedTabIndex; }
            set
            {
                _selectedTabIndex = value;
                NotifiyPropertyChanged("IsButtonNextEnabled");
                NotifiyPropertyChanged("IsEnabledTabSamenvatting");
                NotifiyPropertyChanged("IsEnabledTabPrioriteit");
                NotifiyPropertyChanged("IsEnabledTabAlgemeneGegevens");
                NotifiyPropertyChanged("IsEnabledTabInventaris");
            }
        }
        public int? PriortiteitId
        {
            get { return this.WerkOpdrachtItem.PrioriteitId; }
            set
            {
                if (this.WerkOpdrachtItem.PrioriteitId != value)
                {
                    this.WerkOpdrachtItem.PrioriteitId = value;
                    NotifiyPropertyChanged("PriortiteitId");
                    NotifiyPropertyChanged("Prioriteit");
                    NotifiyPropertyChanged("IsButtonNextEnabled");
                    NotifiyPropertyChanged("IsEnabledTabAlgemeneGegevens");
                }
            }
        }
        public bool CanSave
        {
            get
            {
                if (this.WerkOpdrachtItem.WerkopdrachtType == WerkopdrachtType.Werkaanvraag)
                {
                    if (this.WerkOpdrachtItem.StatusId != (int)WerkopdrachtStatusType.wa_5_nieuwe_aanvraag)
                        return false;

                    if (_settings.UserType == WerkopdrachtUserType.supervisor)
                        return true;

                    if (this.WerkOpdrachtItem.AanvragerId != GlobalData.Instance.LoggedOnUser.Id)
                        return false;
                    return true;
                }
                else
                {
                    if (_settings.UserType == WerkopdrachtUserType.supervisor)
                        return true;
                }
                return false;
            }
        }
        public bool IsReadOnly
        {
            get { return !CanSave; }
        }
        public bool CanModifyInventaris
        {
            get
            {
                if (!CanSave)
                {
                    return false;
                }
                return true;
            }
        }
        public bool CanModifyMedewerkers
        {
            get
            {
                if (!CanSave)
                {
                    return false;
                }
                return this.WerkOpdrachtItem.WerkopdrachtType == WerkopdrachtType.Werkbon;
            }
        }
        public bool CanModifyGroepen
        {
            get
            {
                if (!CanSave)
                {
                    return false;
                }
                //  return this.Groepen != null && this.Groepen.Count > 0;
                return this.WerkOpdrachtItem.WerkopdrachtType == WerkopdrachtType.Werkbon;// && this.WerkOpdrachtItem.Dienst == "TECHD";
            }
        }
        public bool CanModifyDatums
        {
            get
            {
                if (!CanSave)
                {
                    return false;
                }
                return this.WerkOpdrachtItem.WerkopdrachtType == WerkopdrachtType.Werkbon;
            }
        }
        public String WindowTitle
        {
            get
            {
                String text = String.Empty;
                if (this.WerkOpdrachtItem.WerkopdrachtType == WerkopdrachtType.Werkaanvraag)
                {
                    if (this.WerkOpdrachtItem.Id.HasValue)
                        text = String.Format("Wijzigen werkaanvraag {0} {1}", this.WerkOpdrachtItem.Id, this.WerkOpdrachtItem.Titel);
                    else
                        text = String.Format("Nieuwe werkaanvraag");
                }
                if (this.WerkOpdrachtItem.WerkopdrachtType == WerkopdrachtType.Werkbon)
                {
                    if (this.WerkOpdrachtItem.Id.HasValue)
                        text = String.Format("Wijzigen werkbon {0} {1}", this.WerkOpdrachtItem.Id, this.WerkOpdrachtItem.Titel);
                    else
                        text = String.Format("Nieuwe werkbon");
                }
                return text;
            }
        }
        public DateTime? BeginDatum
        {
            get { return this.WerkOpdrachtItem.Begindatum; }
            set
            {
                if (this.WerkOpdrachtItem.Begindatum != value)
                {
                    this.WerkOpdrachtItem.Begindatum = value;
                    NotifiyPropertyChanged(nameof(BeginDatum));
                }
            }
        }
        public DateTime? EindDatum
        {
            get { return this.WerkOpdrachtItem.Einddatum; }
            set
            {
                if (this.WerkOpdrachtItem.Einddatum != value)
                {
                    this.WerkOpdrachtItem.Einddatum = value;
                    NotifiyPropertyChanged("EindDatum");
                }
            }
        }
        public int? StatusId
        {
            get { return this.WerkOpdrachtItem.StatusId; }
            set
            {

                if (this.WerkOpdrachtItem.StatusId != value)
                {
                    this.WerkOpdrachtItem.StatusId = value;

                    NotifiyPropertyChanged("StatusId");

                }
            }
        }
        public int? TypeId
        {
            get { return this.WerkOpdrachtItem.TypeId; }
            set
            {

                if (this.WerkOpdrachtItem.TypeId != value)
                {
                    this.WerkOpdrachtItem.TypeId = value;

                    NotifiyPropertyChanged("TypeId");

                }
            }
        }
        public PrioriteitItem Prioriteit
        {
            get
            {
                if (PriortiteitId.HasValue)
                {
                    return WerkbonnenBeheerBLL.GetPrioriteitItemById(PriortiteitId.Value);
                }
                else return null;
            }
        }
        public String WerkOpdrachtKorteBeschrijving
        {
            get
            {
                return this.WerkOpdrachtItem.Titel;
            }
            set
            {
                if (this.WerkOpdrachtItem.Titel != value)
                {
                    this.WerkOpdrachtItem.Titel = value;
                    NotifiyPropertyChanged("WerkOpdrachtKorteBeschrijving");
                    NotifiyPropertyChanged("IsButtonNextEnabled");
                    NotifiyPropertyChanged("IsEnabledTabSamenvatting");
                }
            }
        }
        public String WerkOpdrachtLangeBeschrijving
        {
            get { return this.WerkOpdrachtItem.Beschrijving; }
            set
            {
                if (this.WerkOpdrachtItem.Beschrijving != value)
                {
                    this.WerkOpdrachtItem.Beschrijving = value;
                    NotifiyPropertyChanged("WerkOpdrachtLangeBeschrijving");
                    NotifiyPropertyChanged("IsButtonNextEnabled");
                    NotifiyPropertyChanged("IsEnabledTabSamenvatting");
                }
            }
        }
        public WerkopdrachtItem WerkOpdrachtItem { get; set; }
        private Location _selectedLocatieItem;
        public Location SelectedLocatieItem
        {
            get { return _selectedLocatieItem; }
            set
            {
                _selectedLocatieItem = value;

                if (value == null)
                {
                    BllGebouwenBeheer.SelectLocation(null);
                    WerkOpdrachtItem.LocatieId = null;
                    WerkOpdrachtItem.LocatiePath = String.Empty;
                }
                else
                {
                    WerkOpdrachtItem.LocatieId = value.Id;
                    WerkOpdrachtItem.LocatiePath = value.GetFullPath();
                }
                NotifiyPropertyChanged("SelectedLocatieItem");
                NotifiyPropertyChanged("GetSelectedLoactieFullPath");
                NotifiyPropertyChanged("IsButtonNextEnabled");
                NotifiyPropertyChanged("IsEnabledTabPrioriteit");
                NotifiyPropertyChanged("InventarisBeschrijving");
                NotifiyPropertyChanged("IsEnabledTabInventaris");
                NotifiyPropertyChanged("InventarisItems");
            }
        }
        public String GetSelectedLoactieFullPath
        {
            get
            {
                if (this._selectedLocatieItem != null)
                    return this._selectedLocatieItem.GetFullPath();
                else return String.Empty;
            }
        }
        public WerkbonnenBeheerBLL BllWerkbonnenBeheer { get; set; }
        public GebouwenBeheerBLL BllGebouwenBeheer { get; set; }
        public List<PrioriteitItem> PrioriteitItems
        {
            get { return this.BllWerkbonnenBeheer.PrioriteitItems; }
        }
        public List<StatusItem> StatusItems
        {
            get
            {
                if (this.WerkOpdrachtItem.WerkopdrachtType == WerkopdrachtType.Werkaanvraag)
                    return StatusItemsWA;
                else
                    return StatusItemsWB;
            }
        }
        public List<StatusItem> StatusItemsWA
        {
            get
            {
                return this.BllWerkbonnenBeheer.StatusItems.Where(s => s.Categorie == "WA").ToList();
            }
        }
        public List<StatusItem> StatusItemsWB
        {
            get
            {
                return this.BllWerkbonnenBeheer.StatusItems.Where(s => s.Categorie == "WB").ToList();
            }
        }
        public String InventarisBeschrijving
        {
            get
            {
                if (this.WerkOpdrachtItem.InventarisId.HasValue)
                    return this.WerkOpdrachtItem.InventarisText;
                else return null;
            }
        }
        public List<InventarisWeergave> InventarisItems
        {
            get
            {
                if (this._selectedLocatieItem == null || this._selectedLocatieItem.Id.HasValue == false)
                    return null;
                BllInventaris.ChangeLocation(this._selectedLocatieItem.Id.GetValueOrDefault());
                List<InventarisWeergave> items = BllInventaris.GetAllInventarisItems();
                return items;
            }
        }
        public InventarisBLL BllInventaris { get; set; }
        private InventarisWeergave _selectedInventarisItem;
        public InventarisWeergave SelectedInventarisItem
        {
            get { return _selectedInventarisItem; }
            set
            {
                if (_selectedInventarisItem != value)
                {
                    _selectedInventarisItem = value;
                    SetInventarisItem(_selectedInventarisItem);
                    NotifiyPropertyChanged("SelectedInventarisItem");
                }
            }
        }
        public List<LeverancierItem> LeveranciersItems
        {
            get
            {
                return BllInventaris.GetAllLeveranciers();
            }
        }
        public List<toestel> ToestelItems
        {
            get
            {
                return BllInventaris.GetAllToestellen();
            }
        }
        public List<merk> MerkItems
        {
            get
            {
                return BllInventaris.GetAllMerken();
            }
        }
        public List<TypeItem> TypeItems
        {
            get
            {
             


                List<TypeItem> allItems =  BllWerkbonnenBeheer.TypeItems.Where(
                    s => s.DienstCode == DienstDescr && s.IsActif==true/*&& 
                    (s.OnlyForSupervisor == false || s.OnlyForSupervisor== (this._settings.UserType == WerkopdrachtUserType.supervisor)
                    )*/
                ).ToList();


                if (DienstDescr != "TECHD")
                    return allItems;

                List<TypeItem> TechdItems = new List<TypeItem>();


                if (GlobalData.Instance.LoggedOnUser.GetAccesType("300") != AccessType.none/*herstelling*/)
                {
                    TypeItem herstelling = allItems.Where(x => x.Id == 1).FirstOrDefault();
                    if (herstelling != null)
                        TechdItems.Add(herstelling);
                }
                if (GlobalData.Instance.LoggedOnUser.GetAccesType("315") != AccessType.none/*periodiek onderhoud*/)
                {

                    TypeItem periodiekonderhoud = allItems.Where(x => x.Id == 4).FirstOrDefault();
                    if (periodiekonderhoud != null)
                        TechdItems.Add(periodiekonderhoud);
                }

                if (GlobalData.Instance.LoggedOnUser.GetAccesType("305") != AccessType.none/*opdracht*/)
                {

                    TypeItem opdracht = allItems.Where(x => x.Id == 6).FirstOrDefault();
                    if (opdracht != null)
                        TechdItems.Add(opdracht);
                }
                if (GlobalData.Instance.LoggedOnUser.GetAccesType("310") != AccessType.none/*project*/)
                {

                    TypeItem project = allItems.Where(x => x.Id == 14).FirstOrDefault();
                    if (project != null)
                        TechdItems.Add(project);
                }
                return TechdItems;

            }
        }
        public TypeItem SelectedType
        {
            get
            {
                return TypeItems.FirstOrDefault(x => x.Id == WerkOpdrachtItem.TypeId);
            }
            set
            {
                if (value != null)
                {
                    WerkOpdrachtItem.TypeId = value.Id;
                }
                NotifiyPropertyChanged("SelectedType");
            }
        }
        public String DienstDescr
        {
            get
            {
                return _settings.Dienst;

                /** VORIGE CODE:
                
                foreach (DienstItem d in this.DienstItems)
                {
                    if (d.Code == this.DienstCode)
                        return d.Naam;
                }
                return String.Empty;*/
            }
        }

        /** NOG NODIG? */
        public String DienstCode
        {
            get { return this.WerkOpdrachtItem.Dienst; }
            set
            {
                if (this.WerkOpdrachtItem.Dienst != value)
                {
                    this.WerkOpdrachtItem.Dienst = value;
                    this.WerkOpdrachtItem.SetDefaultType(); //herstelling voor TECHD en Computerprobleem voor IT
                    ChangeInventarisCategorienSettings();
                    NotifiyPropertyChanged("DienstCode");
                    NotifiyPropertyChanged("DienstDescr");
                    NotifiyPropertyChanged("PriortiteitId");
                    NotifiyPropertyChanged("Prioriteit");
                    NotifiyPropertyChanged("IsButtonNextEnabled");
                    NotifiyPropertyChanged("IsEnabledTabLocation");
                    NotifiyPropertyChanged("IsEnabledTabAlgemeneGegevens");
                    NotifiyPropertyChanged("IsEnabledTabInventaris");
                    NotifiyPropertyChanged("IsEnabledTabPrioriteit");
                    NotifiyPropertyChanged("IsEnabledTabAlgemeneGegevens");
                }
            }
        }
        /** NOG NODIG? */
        public List<DienstItem> DienstItems
        {
            get
            {
                return this.BllWerkbonnenBeheer.DienstItems.Where(c => c.Code == this._settings.Dienst).ToList<DienstItem>();
            }
        }

       public bool IsInventarisVisible {

            get
            {

                return _werkopdracht_diensten.inventaris;
            }


        }

        public bool IsLocationisVisible
        {

            get
            {

                return _werkopdracht_diensten.location;
            }


        }
        private werkopdracht_diensten _werkopdracht_diensten;

        /** constructor */
        public SaveWerkOpdrachtViewController(WerkopdrachtItem werkOpdrachtItem,WerkopdrachtUserType userType)
        {
            //werkbonnebeheer
            _settings = new WerkbonnenBeheerSettings2(WerkopdrachtType.Werkaanvraag)
            {
                UserType = userType,
                Dienst = werkOpdrachtItem.Dienst
            };

          
            _werkopdracht_diensten = GlobalData.Instance.LoggedOnUser.GetDienstByCode(werkOpdrachtItem.Dienst);
            if (_werkopdracht_diensten.location == false)
                werkOpdrachtItem.LocatieId = 97;//root


            this.BllWerkbonnenBeheer = new WerkbonnenBeheerBLL(_settings);
            this.Groepen = GetSubGroepen(werkOpdrachtItem.Dienst);
            this.Medewerkers = GetUsersDienst(werkOpdrachtItem.Dienst);

            this.WerkOpdrachtItem = werkOpdrachtItem;
            

            //Medewerker chekboxen aanvinken
            foreach (User u in werkOpdrachtItem.Medewerkers)
            {
                foreach (UserWOView uwo in this.Medewerkers)
                {
                    if (uwo.Id == u.Id)
                        uwo.IsChecked = true;

                }
            }
            //Groepen chekboxen aanvinken
            foreach (String g in werkOpdrachtItem.Groepen)
            {
                foreach (UserWOView uwo in this.Groepen)
                {
                    if (uwo.SubGroep == g)
                        uwo.IsChecked = true;

                }

            }
            //locatieboom: in de wizard gaan we filteren op de lokaties. dus die van Pav1 kunnen enkel WA indienen van PAV 1
            //Supervisors gaan via de extended popup
            GebouwenBeheerSettings gbSettings = new GebouwenBeheerSettings();
            if (userType == WerkopdrachtUserType.normal)
            {
                gbSettings.SelectionMode = GebouwenBeheerSelectionModeType.selectlowestonly;
            }
            if (IsReadOnly)
                gbSettings.SelectionMode = GebouwenBeheerSelectionModeType.selectnone;


            if (werkOpdrachtItem.Id == null)
            {
                if (userType == WerkopdrachtUserType.normal)
                  gbSettings.AddLocationsToFilter(GlobalData.Instance.LoggedOnUser.GetLocationsForDienst(werkOpdrachtItem.Dienst));
            }

            BllGebouwenBeheer = new GebouwenBeheerBLL(gbSettings);

            if (werkOpdrachtItem.LocatieId.HasValue)
            {
                SelectLocation(werkOpdrachtItem.LocatieId);
                NotifiyPropertyChanged("IsEnabledTabInventaris");
            }
            InventarisToestelCategorieList = new List<ToestelCategorieType>();
           
            BllInventaris = new InventarisBLL(new InventarisSettings() { Geinventariseerd = true, LoadUitdienst = false, LocatieId = null, AddEmptyRecord = true, ToestelCategorien = InventarisToestelCategorieList,StatusTypes = new List<InventarisStatusType> { InventarisStatusType.Actief, InventarisStatusType.Afgeschreven }  });
            ChangeInventarisCategorienSettings();


            //het eerste type al voorselecteren indien het nog geen heeft.
            if (WerkOpdrachtItem.TypeId.HasValue==false && TypeItems != null && TypeItems.Count> 0)
                WerkOpdrachtItem.TypeId = TypeItems[0].Id;



        }


        /** methods */
        public void ChangeInventarisCategorienSettings()
        {
           InventarisToestelCategorieList.Clear();
            /* if (this.WerkOpdrachtItem.Dienst == "IT")
                InventarisToestelCategorieList.Add(ToestelCategorieType.IT);
            else
            {
                InventarisToestelCategorieList.Add(ToestelCategorieType.GK);
                InventarisToestelCategorieList.Add(ToestelCategorieType.PAV);
                InventarisToestelCategorieList.Add(ToestelCategorieType.TD);
            }*/
            NotifiyPropertyChanged("InventarisItems"); 
        }
        public void CheckBoxCallBack(UserWOView v)
        {
            foreach (UserWOView u in Medewerkers.Where(X => X.SubGroep == v.SubGroep))
            {
                u.UserIsInSelectedGroup = (v.IsChecked);
            }
        }
        private List<UserWOView> GetUsersDienst(String dienst)
        {
            List<User> usersTD;
           
                usersTD = UserBLL.GetUsersForDienst(dienst).OrderBy(u => u.FirstName).ToList<User>();

            List<UserWOView> userWo = new List<UserWOView>();

            foreach (User u in usersTD)
            {
                UserWOView v = new UserWOView
                {
                    Id = u.Id.GetValueOrDefault(),
                    SubGroep = u.SubGroep,
                    LastName = u.LastName,
                    FirstName = u.FirstName
                };
                userWo.Add(v);
            }
            return userWo;

        }
        private List<UserWOView> GetSubGroepen(String dienst)
        {
            List<String> subgroepen = UserBLL.GetAllTechnicalSubgroups(dienst);
            List<UserWOView> userWo = new List<UserWOView>();
            foreach (String s in subgroepen)
            {
                UserWOView v = new UserWOView
                {
                    myCallBack = CheckBoxCallBack,
                    Id = -1,
                    SubGroep = s
                };
                userWo.Add(v);
            }
            return userWo;
        }
        public bool Validate(out String errorMessage)
        {
            errorMessage = String.Empty;
            if ((this.WerkOpdrachtItem.LocatieId == null))
            {
                errorMessage = "Gelieve een locatie uit te kiezen !";
                return false;
            }
            if (String.IsNullOrWhiteSpace(this.WerkOpdrachtItem.Titel))
            {
                errorMessage = "Gelieve een beknopte beschrijving in te geven !";
                return false;
            }

            if (this.WerkOpdrachtItem.WerkopdrachtType == WerkopdrachtType.Werkbon)
            {

                /* if (this.WerkOpdrachtItem.Begindatum == null)
                 {
                     errorMessage = "Gelieve een begindatum in te geven !";
                     return false;
                 }
                 if (this.WerkOpdrachtItem.Einddatum == null)
                 {
                     errorMessage = "Gelieve een einddatum in te geven !";
                     return false;
                 }*/
                if (this.WerkOpdrachtItem.Begindatum.HasValue && this.WerkOpdrachtItem.Einddatum.HasValue)
                {
                    if (this.WerkOpdrachtItem.Begindatum.Value.Date > this.WerkOpdrachtItem.Einddatum.Value)
                    {
                        errorMessage = "Einddatum ligt voor begindatum !";
                        return false;
                    }
                }


                var anyGroep = this.Groepen.Any(x => x.IsChecked);
                var anyMedewerker = this.Medewerkers.Any(x => x.IsChecked);

              

                    if (!anyGroep && !anyMedewerker)
                    {
                        errorMessage = "Gelieve een minimaal 1 groep of medewerker uit te kiezen.";
                        return false;
                    }
                
                 
            }
            return true;
        }
        public void SetInventarisItem(InventarisWeergave inventarisItem)
        {

            if (inventarisItem == null)
                UnselectInventarisItem();
            else
            {
                this.WerkOpdrachtItem.InventarisId = inventarisItem.id;
                this.WerkOpdrachtItem.InventarisText = String.Format("{0} {1}", inventarisItem.nummer_263, inventarisItem.productOmschrijving);
            }
            NotifiyPropertyChanged("InventarisBeschrijving");
        }
        public void UnselectInventarisItem()
        {
            this.WerkOpdrachtItem.InventarisId = null;
            this.WerkOpdrachtItem.InventarisText = null;
            NotifiyPropertyChanged("InventarisBeschrijving");
            NotifiyPropertyChanged("SelectedInventarisItem");

        }
        public void SelectLocation(int? id)
        {
            this.BllGebouwenBeheer.SelectLocation(id);
        }
        /// <summary>
        /// Deze functie finaliseert het nieuwe of te wijigen Item.
        /// a.o. geselecteerde gebruikers en groepen worden getransfeerd
        /// </summary>
        public void TransferMedewerkersGroepen()
        {
            this.WerkOpdrachtItem.Groepen.Clear();
            this.WerkOpdrachtItem.Medewerkers.Clear();
            foreach (UserWOView uwo in this.Medewerkers)
            {
                if (uwo.IsChecked)
                {
                    User u = new User
                    {
                        Id = uwo.Id,
                        FirstName = uwo.FirstName.Trim(),
                        LastName = uwo.LastName.Trim()
                    };
                    if (uwo.SubGroep !=null)
                        u.SubGroep = uwo.SubGroep.Trim();
                    this.WerkOpdrachtItem.Medewerkers.Add(u);
                }
            }
            foreach (UserWOView uwo in this.Groepen)
            {
                if (uwo.IsChecked)
                {
                    this.WerkOpdrachtItem.Groepen.Add(uwo.SubGroep);
                }
            }
        }
        void NotifiyPropertyChanged(string property)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(property));
        }
        public void CleanUp()
        {
            this.BllGebouwenBeheer = null;
            this.BllInventaris.CleanUp();
            this.BllInventaris = null;
            this.BllWerkbonnenBeheer.CleanUp();
            this.BllWerkbonnenBeheer = null;
            this.WerkOpdrachtItem = null;
            this._selectedLocatieItem = null;
            this._selectedInventarisItem = null;
     
        }
    }
}
