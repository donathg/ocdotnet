﻿ 
using sharedcode.common;
using sharedcode.CommonObjects;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevExpress.Mvvm;
using DevExpress.Mvvm.DataAnnotations;
using DevExpress.Mvvm.POCO;
using ClaraFey.ViewControllers;
using System.Windows.Threading;
using System.Windows;
using sharedcode.BLL;
using System.Globalization;

namespace ClaraFey.Views
{
    [POCOViewModel]
    public class LogonView : INotifyPropertyChanged
    {
        /** PROPERTIES */
        public event PropertyChangedEventHandler PropertyChanged;
        
        
        public bool IsSubMenuKalenderVoertuigenTechdVisible
        {

            get
            {

                return GlobalData.Instance.LoggedOnUser.HasAccess("418");
            }
        }

        public bool IsSubMenuSMMVoertuigenVisible
        {

            get
            {

                return GlobalData.Instance.LoggedOnUser.HasAccess("520");
            }

        }


        public bool IsMainMenuKasbeheerVisible
        {
            get
            {
                return GlobalData.Instance.LoggedOnUser.HasAccessToGroup(600, 699);
            }
        }
        public bool IsSubMenuKasbeheerVisible
        {
            get
            {
                return GlobalData.Instance.LoggedOnUser.HasAccess("600");
            }
        }
        public bool IsMainRapportageVisible
        {
            get
            {
                return GlobalData.Instance.LoggedOnUser.HasAccessToGroup(700,799);
            }
        }
        public bool IsSubRapportageCliëntenOverzicht
        {
            get
            {
                return GlobalData.Instance.LoggedOnUser.GetAccesType("712") != AccessType.none;
            }
        }
        public bool IsSubRapportageCliëntIDOOverzicht
        {
            get
            {
                return GlobalData.Instance.LoggedOnUser.GetAccesType("730") != AccessType.none;
            }
        }
        public bool IsSubRapportageCliëntIDOAddendumOverzicht
        {
            get
            {
                return GlobalData.Instance.LoggedOnUser.GetAccesType("720") != AccessType.none;
            }
        }
        public bool IsSubRapportageTijdregistratieVisible
        {
            get
            {
                return GlobalData.Instance.LoggedOnUser.GetAccesType("710") != AccessType.none;
            }
        }
        public bool IsSubRapportageApotheekImport
        {
            get
            {
                return GlobalData.Instance.LoggedOnUser.GetAccesType("750") != AccessType.none;
            }
        }
        public bool IsSubRapportageKasbeheerExport
        {
            get
            {
                return GlobalData.Instance.LoggedOnUser.HasAccess("610");
            }
        }

        public bool IsMainMenuAdministratieVisible
        {
            get
            {
                return GlobalData.Instance.LoggedOnUser.HasAccessToGroup(800, 899);
            }
        }

        public bool IsMainMenuClientDossierVisible
        {
            get
            {
                return GlobalData.Instance.LoggedOnUser.GetAccesType("1000") != AccessType.none
                    || GlobalData.Instance.LoggedOnUser.GetAccesType("1001") != AccessType.none;
            }
        }
        public bool IsMainMenuPersoneelVisible
        {
            get
            {
                return GlobalData.Instance.LoggedOnUser.GetAccesType("2000") != AccessType.none;
            }
        }
        public bool IsSubMenumnuWerkaanvragenSupervisorITVisible
        {
            get
            {
                return GlobalData.Instance.LoggedOnUser.GetAccesType("330") != AccessType.none;
            }
        }
        public bool IsSubMenumnuWerkaanvragenSupervisorTechdVisible
        {
            get
            {
                return GlobalData.Instance.LoggedOnUser.GetAccesType("325") != AccessType.none;
            }
        }
        public bool IsSubMenuSMMApplicationSettings
        {
            get
            {
                return GlobalData.Instance.LoggedOnUser.GetAccesType("115") != AccessType.none;
            }
        }
        public bool IsMainMenuSMMVisible
        {
            get
            {
                return GlobalData.Instance.LoggedOnUser.HasAccessToGroup(100, 199);
            }
        }
        public bool IsSubMenuSMMRechtenGroepenVisible
        {
            get
            {
                return GlobalData.Instance.LoggedOnUser.GetAccesType("105") != AccessType.none;
            }
        }
        public bool IsSubMenuSMMRechtenGroepenGebruikersVisible
        {
            get
            {
                return GlobalData.Instance.LoggedOnUser.GetAccesType("105") != AccessType.none;
            }
        }
        public bool IsSubMenuSMMRechtenGroepenFunctiesVisible
        {
            get
            {
                return GlobalData.Instance.LoggedOnUser.GetAccesType("105") != AccessType.none;
            }
        }
        public bool IsSubMenuSMMRechtenMedewerkersVisible
        {
            get
            {
                return GlobalData.Instance.LoggedOnUser.GetAccesType("105") != AccessType.none;
            }
        }
        public bool IsSubMenuSMMLeefgroepenVisible
        {
            get
            {
                return GlobalData.Instance.LoggedOnUser.GetAccesType("110") != AccessType.none;
            }
        }
        public bool IsSubMenuSMMKampenVisible
        {
            get
            {
                return GlobalData.Instance.LoggedOnUser.GetAccesType("510") != AccessType.none; //Afsluiten bewoners 510
            }
        }
        public bool IsSubMenuKalenderTroonVoertuigenVisible
        {
            get
            {
                return GlobalData.Instance.LoggedOnUser.GetAccesType("412") != AccessType.none;
            }
        }
        public bool IsSubMenuKalenderKauriVoertuigenVisible
        {
            get
            {
                return GlobalData.Instance.LoggedOnUser.GetAccesType("413") != AccessType.none;
            }
        }
        public bool IsSubMenuKalenderEyndovensteenwegVoertuigenVisible
        {
            get
            {
                return GlobalData.Instance.LoggedOnUser.GetAccesType("411") != AccessType.none;
            }
        }
        public bool IsSubMenuKalenderVoertuigenIndividueleBegeleidingVisible
        {
            get
            {
                return GlobalData.Instance.LoggedOnUser.GetAccesType("410") != AccessType.none;
            }
        }
        public bool IsSubMenuSMMGebouwenbeheerVisible
        {
            get
            {
                return GlobalData.Instance.LoggedOnUser.GetAccesType("100") != AccessType.none;
            }
        }
        public bool IsMainMenuInventarisVisible
        {
            get
            {
                return GlobalData.Instance.LoggedOnUser.HasAccessToGroup(200, 299);
            }
        }
        public bool IsMainMenuWerkbonnenbeheerVisible
        {
            get
            {
                return GlobalData.Instance.LoggedOnUser.HasAccessToGroup(300, 399);
            }
        }
        public bool IsSubMenuWerkaanvragenVisible
        {
            get
            {
                return IsSubMenuWerkaanvragenTECHDVisible || IsSubMenuWerkaanvragenITVisible;
            }
        }
        public bool IsSubMenuWerkaanvragenTECHDVisible
        {
            get
            {
                if (GlobalData.Instance.LoggedOnUser.GetAccesType("325") != AccessType.none)
                {
                    return false;
                }
                return GlobalData.Instance.LoggedOnUser.GetAccesType("300") != AccessType.none;
            }
        }
        public bool IsSubMenuWerkaanvragenITVisible
        {
            get
            {
                if (GlobalData.Instance.LoggedOnUser.GetAccesType("330") != AccessType.none)
                    return false;
                return GlobalData.Instance.LoggedOnUser.GetAccesType("300") != AccessType.none;
            }
        }
        public bool IsSubMenuWerkbonnenVisible
        {
            get
            {
                return GlobalData.Instance.LoggedOnUser.GetAccesType("320") != AccessType.none; //werkbonnen
            }
        }
        public bool IsMainMenuKalenderVisible
        {
            get
            {
                if (GlobalData.Instance.GetConnectedDatabaseType() == ClaraFeyDatabase.test)
                    return false; //er is geen testkalender
                return GlobalData.Instance.LoggedOnUser.HasAccessToGroup(400, 499);
            }
        }
        public bool IsSubMenuKalenderVoertuigenVisible
        {
            get
            {
                return GlobalData.Instance.LoggedOnUser.GetAccesType("410") != AccessType.none;
            }
        }
        public bool IsSubMenuKalenderZalenVisible
        {
            get
            {
                return GlobalData.Instance.LoggedOnUser.GetAccesType("415") != AccessType.none;
            }
        }
        public bool IsSubMenuKalenderLaptopsVisible
        {
            get
            {
                return GlobalData.Instance.LoggedOnUser.GetAccesType("400") != AccessType.none;
            }
        }
        public bool IsSubMenuKalenderCKKZaalXLVisible
        {
            get
            {
                return GlobalData.Instance.LoggedOnUser.GetAccesType("416") != AccessType.none;
            }
        }
        
        public bool IsSubMenuKalenderCKKZalenAdministratieVisible
        {
            get
            {
                return true;
            }
        }
        public bool IsSubMenuKalenderActiviteitenVisible
        {
            get
            {
                return GlobalData.Instance.LoggedOnUser.GetAccesType("405") != AccessType.none;
            }
        } 
        public bool IsSubMenuKalenderCSRZalenVisible
        {
            get
            {
                return GlobalData.Instance.LoggedOnUser.GetAccesType("415") != AccessType.none;
            }
        }
        public bool IsSubMenuKalenderDagcentrumVoertuigenVisible
        {
            get
            {
                return true;// GlobalData.Instance.LoggedOnUser.GetAccesType("415") != AccessType.none;
            }
        }
        public bool CanModifyTelefoonboek
        {
            get
            {
                return GlobalData.Instance.LoggedOnUser.GetAccesType("105") != AccessType.none;
            }
        }
        public bool IsMainMenuKilometerVisible
        {
            get
            {
                if (GlobalData.Instance.LoggedOnUser.GetAccesType("520") != AccessType.none || GlobalData.Instance.LoggedOnUser.GetAccesType("500") != AccessType.none)
                    return true;
                return false;
            }
        }
        public bool IsSubMenuMijnKilometerVisible
        {
            get
            {
                if (GlobalData.Instance.LoggedOnUser.GetAccesType("520") != AccessType.none || GlobalData.Instance.LoggedOnUser.GetAccesType("500") != AccessType.none)
                    return true;
                return false;
            }
        }
        public bool IsSubMenuKilometerBoekhoudingVisible
        {
            get
            {
                return GlobalData.Instance.LoggedOnUser.GetAccesType("515") != AccessType.none;
            }
        }
        public bool IsSubMenuKilometerPersoneelVisible
        {
            get
            {
                return GlobalData.Instance.LoggedOnUser.GetAccesType("505") != AccessType.none;
            }
        }
        public bool IsSubMenuKilometerBewonersVisible
        {
            get
            {
                return GlobalData.Instance.LoggedOnUser.GetAccesType("510") != AccessType.none;
            }
        }
        public bool IsSubMenuKilometerOverzichtVisible
        {
            get
            {
                return GlobalData.Instance.LoggedOnUser.GetAccesType("520") != AccessType.none || GlobalData.Instance.LoggedOnUser.GetAccesType("530") != AccessType.none;
            }
        }
        public bool IsSubMenuSMMWerkbonnenbeheerLocatiesVisible
        {
            get
            {
                return GlobalData.Instance.LoggedOnUser.GetAccesType("100") != AccessType.none;
            }
        }
        public bool IsLoggedOn
        {
            get
            {
                return GlobalData.Instance.LoggedOnUser.IsLoggedOn;
            }
        }
        public bool IsNotLoggedOn
        {
            get
            {
                return !GlobalData.Instance.LoggedOnUser.IsLoggedOn;
            }
        }

        /** CONSTRUCTOR */
        public LogonView()
        {
            GlobalData.Instance.LoggedOnUser.PropertyChanged += LoggedOnUser_PropertyChanged;
        }

        /** EVENTS */
        void LoggedOnUser_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            /* GLOBAAL */
            NotifiyPropertyChanged(nameof(IsLoggedOn));
            NotifiyPropertyChanged(nameof(IsNotLoggedOn));
            /* CLIENTDOSSIER */
            NotifiyPropertyChanged(nameof(IsMainMenuClientDossierVisible));
            /* PERSONEEL */
            NotifiyPropertyChanged(nameof(IsMainMenuPersoneelVisible));
            /* INVENTARIS */
            NotifiyPropertyChanged(nameof(IsMainMenuInventarisVisible));
            /* KASBEHEER */
            NotifiyPropertyChanged(nameof(IsMainMenuKasbeheerVisible));
            NotifiyPropertyChanged(nameof(IsSubMenuKasbeheerVisible));
            /* WERKBONNENBEHEER */
            NotifiyPropertyChanged(nameof(IsMainMenuWerkbonnenbeheerVisible));
            NotifiyPropertyChanged(nameof(IsSubMenumnuWerkaanvragenSupervisorTechdVisible));
            NotifiyPropertyChanged(nameof(IsSubMenumnuWerkaanvragenSupervisorITVisible));
            NotifiyPropertyChanged(nameof(IsSubMenuWerkaanvragenVisible));
            NotifiyPropertyChanged(nameof(IsSubMenuWerkaanvragenTECHDVisible));
            NotifiyPropertyChanged(nameof(IsSubMenuWerkaanvragenITVisible));
            NotifiyPropertyChanged(nameof(IsSubMenuWerkbonnenVisible));
            /* KALENDER */
            NotifiyPropertyChanged(nameof(IsMainMenuKalenderVisible));
            NotifiyPropertyChanged(nameof(IsSubMenuKalenderVoertuigenVisible));
            NotifiyPropertyChanged(nameof(IsSubMenuKalenderVoertuigenIndividueleBegeleidingVisible));
            NotifiyPropertyChanged(nameof(IsSubMenuKalenderZalenVisible));
            NotifiyPropertyChanged(nameof(IsSubMenuKalenderCKKZaalXLVisible));
            NotifiyPropertyChanged(nameof(IsSubMenuKalenderCKKZalenAdministratieVisible));
            NotifiyPropertyChanged(nameof(IsSubMenuKalenderLaptopsVisible));
            NotifiyPropertyChanged(nameof(IsSubMenuKalenderActiviteitenVisible));
            NotifiyPropertyChanged(nameof(IsSubMenuKalenderCSRZalenVisible));
            NotifiyPropertyChanged(nameof(IsSubMenuKalenderDagcentrumVoertuigenVisible));
            NotifiyPropertyChanged(nameof(IsSubMenuKalenderVoertuigenTechdVisible));
            NotifiyPropertyChanged(nameof(IsSubMenuKalenderTroonVoertuigenVisible));
            NotifiyPropertyChanged(nameof(IsSubMenuKalenderKauriVoertuigenVisible));
            NotifiyPropertyChanged(nameof(IsSubMenuKalenderEyndovensteenwegVoertuigenVisible));
            /* KILOMETERVERGOEDING */
            NotifiyPropertyChanged(nameof(IsMainMenuKilometerVisible));
            NotifiyPropertyChanged(nameof(IsSubMenuMijnKilometerVisible));
            NotifiyPropertyChanged(nameof(IsSubMenuKilometerOverzichtVisible));
            NotifiyPropertyChanged(nameof(IsSubMenuKilometerBoekhoudingVisible));
            NotifiyPropertyChanged(nameof(IsSubMenuKilometerPersoneelVisible));
            NotifiyPropertyChanged(nameof(IsSubMenuKilometerBewonersVisible));
            /* RAPPORTAGE */
            NotifiyPropertyChanged(nameof(IsMainRapportageVisible));
            NotifiyPropertyChanged(nameof(IsSubRapportageTijdregistratieVisible));
            NotifiyPropertyChanged(nameof(IsSubRapportageCliëntenOverzicht));
            NotifiyPropertyChanged(nameof(IsSubRapportageKasbeheerExport));
            NotifiyPropertyChanged(nameof(IsSubRapportageApotheekImport));
            NotifiyPropertyChanged(nameof(IsSubRapportageCliëntIDOOverzicht));
            NotifiyPropertyChanged(nameof(IsSubRapportageCliëntIDOAddendumOverzicht));
            /* SMM */
            NotifiyPropertyChanged(nameof(IsMainMenuSMMVisible));
            NotifiyPropertyChanged(nameof(IsSubMenuSMMGebouwenbeheerVisible));
            NotifiyPropertyChanged(nameof(IsSubMenuSMMRechtenGroepenVisible));
            NotifiyPropertyChanged(nameof(IsSubMenuSMMRechtenGroepenGebruikersVisible));
            NotifiyPropertyChanged(nameof(IsSubMenuSMMRechtenGroepenFunctiesVisible));
            NotifiyPropertyChanged(nameof(IsSubMenuSMMRechtenMedewerkersVisible));
            NotifiyPropertyChanged(nameof(IsSubMenuSMMLeefgroepenVisible));
            NotifiyPropertyChanged(nameof(IsSubMenuSMMKampenVisible));
            NotifiyPropertyChanged(nameof(IsSubMenuSMMApplicationSettings));
            NotifiyPropertyChanged(nameof(IsSubMenuSMMWerkbonnenbeheerLocatiesVisible));
            NotifiyPropertyChanged(nameof(IsSubMenuSMMVoertuigenVisible));


            NotifiyPropertyChanged(nameof(IsMainMenuAdministratieVisible));

        }

        /** METHODS */
        void NotifiyPropertyChanged(string property)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(property));
        }
    }
}
