﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AutoFileCopier
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            Start();
        }

        private void Start()
        {
            btnStart.Enabled = false;
            Timer myTimer = new Timer();
            myTimer.Interval = 600000;
            myTimer.Tick += MyTimer_Tick; ;
            myTimer.Start();
        }

        private void MyTimer_Tick(object sender, EventArgs e)
        {
            try
            {
                Copy();
                btnStart.Enabled = true;
                lblResult.Text = "Copieren gelukt ! " + DateTime.Now.ToString("HH:mm");
                lblResult.Visible = true;
                txtResult.Text += DateTime.Now.ToString("HH:mm") + " - !!!!!!!!!!!!!!!!!!!!!!! YES !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!";

            }
            catch (Exception ex)
            {
                txtResult.Text += DateTime.Now.ToString("HH:mm") + " - " + ex.Message + "\n\r";
            }

        }

        private void Copy()
        {
            foreach (var file in Directory.GetFiles(txtSourceDir.Text))
                File.Copy(file, Path.Combine(txtDestDir.Text, Path.GetFileName(file)), true);


        }
    }
}
