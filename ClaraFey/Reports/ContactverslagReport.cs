﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using ClaraFey.Reports.PrintModels;
using System.Collections.Generic;

namespace ClaraFey.Reports
{
    public partial class ContactverslagReport : DevExpress.XtraReports.UI.XtraReport
    {
        public ContactverslagReport(ContactverslagReportModel model)
        {
            InitializeComponent();

            this.DataSource = new List<ContactverslagReportModel>(){ model };
        }
    }
}
