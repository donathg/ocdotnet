﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using sharedcode.BLL;
using System.Collections.Generic;

namespace ClaraFey.Reports
{
    public partial class BeeldvormingReport : DevExpress.XtraReports.UI.XtraReport
    {
        private sharedcode.BLL.BeeldvormingReport _report;




        BeeldvormingReportItem _prevRow=null;
        BeeldvormingReportItem _currentRow = null;
        List<sharedcode.BLL.BeeldvormingReport> reportBv;
        public BeeldvormingReport(sharedcode.BLL.BeeldvormingReport report)
        {
            _report = report;
            InitializeComponent();
            reportBv = new List<sharedcode.BLL.BeeldvormingReport>
            {
                report
            };
            this.DataSource = reportBv;
            if (!report.AfgeslotenDatum.HasValue)
                lblAfsluitdatum.Visible = false;

            this.DataSourceRowChanged += BeeldvormingReport_DataSourceRowChanged;

            SetVersionLabels();
        }

        private void BeeldvormingReport_DataSourceRowChanged(object sender, DataSourceRowEventArgs e)
        {



          

        }

        private void SetVersionLabels()
        {
            lblVersie1.Visible = false;
            lblVersie2.Visible = false;
            lblVersie3.Visible = false;
            lblContentVersie1.Visible = false;
            lblContentVersie2.Visible = false;
            lblContentVersie3.Visible = false;


            if (this._report.Type == BeeldvormingReportPrintTypes.A)
            {
                lblVersie1.Visible = true;
                lblContentVersie1.Visible = true;
                lblVersie1.Text = "Versie A";
                if (_report.VertrouwelijkeInfo)
                {
                    lblVersie2.Visible = true;
                    lblContentVersie2.Visible = true;
                    lblVersie2.Text = "Vertrouwelijke info";
                }
            }
            if (this._report.Type == BeeldvormingReportPrintTypes.B)
            {
                lblVersie1.Visible = true;
                lblContentVersie1.Visible = true;
                lblVersie1.Text = "Versie B";
                if (_report.VertrouwelijkeInfo)
                {
                    lblVersie2.Visible = true;
                    lblContentVersie2.Visible = true;
                    lblVersie2.Text = "Vertrouwelijke info";
                }
            }
            if (this._report.Type == BeeldvormingReportPrintTypes.AB)
            {
                lblVersie1.Visible = true;
                lblContentVersie1.Visible = true;
                lblVersie1.Text = "Versie A";

                lblVersie2.Visible = true;
                lblContentVersie2.Visible = true;
                lblVersie2.Text = "Versie B";

                if (_report.VertrouwelijkeInfo)
                {
                    lblVersie3.Visible = true;
                    lblContentVersie3.Visible = true;
                    lblVersie3.Text = "Vertrouwelijke info";
                }

            }
        }

        private void lblContentVersie1_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            String A =  DetailReport.GetCurrentColumnValue("A").ToString();
            String B = DetailReport.GetCurrentColumnValue("B").ToString();
            String V = DetailReport.GetCurrentColumnValue("V").ToString();
            XRLabel lbl = ((XRLabel) sender);
            if (this._report.Type == BeeldvormingReportPrintTypes.A || this._report.Type == BeeldvormingReportPrintTypes.AB)
                lbl.Text = A;
            if (this._report.Type == BeeldvormingReportPrintTypes.B)
            {
                lbl.Text = B;
            }
             

        }

        private void lblContentVersie2_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            String A = DetailReport.GetCurrentColumnValue("A").ToString();
            String B = DetailReport.GetCurrentColumnValue("B").ToString();
            String V = DetailReport.GetCurrentColumnValue("V").ToString();
            XRLabel lbl = ((XRLabel)sender);
            if (this._report.Type == BeeldvormingReportPrintTypes.A ||
                this._report.Type == BeeldvormingReportPrintTypes.B)
            {
                if (this._report.VertrouwelijkeInfo)
                {
                    lbl.Text = V;
                }
            }
            if (this._report.Type == BeeldvormingReportPrintTypes.AB)
            {
                lbl.Text = B;
            }


        }

        private void lblContentVersie3_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            String A = DetailReport.GetCurrentColumnValue("A").ToString();
            String B = DetailReport.GetCurrentColumnValue("B").ToString();
            String V = DetailReport.GetCurrentColumnValue("V").ToString();
            XRLabel lbl = ((XRLabel)sender);
            if (this._report.Type == BeeldvormingReportPrintTypes.AB && this._report.VertrouwelijkeInfo)
            {
                lbl.Text = V;
            }

        }

        private void LblCat1_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (_prevRow != null)
            {

                if (_currentRow.BeeldvormingLevel0Naam.ToString() == _prevRow.BeeldvormingLevel0Naam.ToString())
                {
                    ((XRLabel)sender).Visible = false;
                    ((XRLabel)sender).SizeF = new SizeF(new PointF(630, 0));
                }
                else
                {
                    ((XRLabel)sender).Visible = true;
                    ((XRLabel)sender).SizeF = new SizeF(new PointF(630, 35));
                }
            }
        }

        private void DetailReport_DataSourceRowChanged(object sender, DataSourceRowEventArgs e)
        {

       
                _prevRow = _currentRow;
                _currentRow = reportBv[0].ReportItems[e.CurrentRow];
                
             

        }

        private void LblCat2_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (_prevRow != null)
            {

                if (_currentRow.BeeldvormingLevel1Naam.ToString() == _prevRow.BeeldvormingLevel1Naam.ToString())
                {
                    ((XRLabel)sender).SizeF = new SizeF(new PointF(630, 0));
                    ((XRLabel)sender).Visible = false;
                }
                else
                {
                    ((XRLabel)sender).SizeF = new SizeF(new PointF(630, 31));
                    ((XRLabel)sender).Visible = true;

                }
            }
           
        }
    }
}
