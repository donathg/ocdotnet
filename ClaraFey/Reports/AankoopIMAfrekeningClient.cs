﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using ClaraFey.Reports.PrintModels;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using sharedcode.BLL;
using System.Linq;
using System.Globalization;

namespace ClaraFey.Reports
{
    public partial class AankoopIMAfrekeningClient : DevExpress.XtraReports.UI.XtraReport
    {
        public AankoopIMAfrekeningClient(List<AankoopIM> artikelen)
        {

            CultureInfo us = CultureInfo.GetCultureInfo("be-BE");
            InitializeComponent();
            this.DataSource = Get(artikelen);
        }
        private ObservableCollection<BestelbonAankoopIM> Get(List<AankoopIM> artikelen)
        {
            List<String> afleverplaats = new List<string>();
            foreach (AankoopIM a in artikelen)
            {
                if (!afleverplaats.Contains(a.Afleverplaats))
                    afleverplaats.Add(a.Afleverplaats);
              //  break;
            }
            ObservableCollection<BestelbonAankoopIM> coll = new ObservableCollection<BestelbonAankoopIM>();
            foreach (String af in afleverplaats)
            {
                List<AankoopIM> distinctArtikelenPerAfleverplaats = new List<AankoopIM>();
              
                foreach (AankoopIM artikel in artikelen.Where(x => x.Afleverplaats == af).ToList()) 
                {
                    AankoopIM a = distinctArtikelenPerAfleverplaats.Where(x => x.ArtikelId == artikel.ArtikelId).FirstOrDefault();
                    if (a==null)
                    {
                        distinctArtikelenPerAfleverplaats.Add(artikel);

                    }
                    else
                    {
                        a.Aantal = artikel.Aantal;
                    }
 
                }
                BestelbonAankoopIM b = new BestelbonAankoopIM(distinctArtikelenPerAfleverplaats);
                b.LeverAdresCampus = GetCampus(af);
                b.LeverAdresStraat = GetStraat(af);
                b.LeverAdresPostCodeGemeente = GetGemeente(af);
                b.AfleverInfo = GetAfleverInfo(af);
                coll.Add(b);

            }
            return coll;


        }
        private String GetCampus(string afleverPlaats)
        {
            switch (afleverPlaats.ToLower())
            {
                case "campus malle":
                    return "campus Kauri";
 

                case "ckk":
                    return "campus Kristus Koning";
                case "de troon":
                    return "De Troon";
                case "csr":
                    return "campus Sint-Rafaël";
                default: return "";
            }
        }
        private String GetStraat(string afleverPlaats)
        {
            switch (afleverPlaats.ToLower())
            {
                case "campus malle":
                    return "St. Pauluslaan 6a";


                case "ckk":
                    return "Bethaniëlei 5";
                case "de troon":
                    return "Sassenhout 43";
                case "csr":
                    return "Kerklei 44";
                default: return "";
            }
        }
        private String GetGemeente(string afleverPlaats)
        {
            switch (afleverPlaats.ToLower())
            {
                case "campus malle":
                    return "2390 Malle";


                case "ckk":
                    return "2960 Sint-Job-in-'t-Goor";
                case "de troon":
                    return "2290 Vorselaar";
                case "csr":
                    return "2960 Brecht (St. Job)";
                default: return "";
            }
        }
        private String GetAfleverInfo(string afleverPlaats)
        {
            switch (afleverPlaats.ToLower())
            {
                case "campus malle":
                    return "Deze campus ligt op terreinen van 'De Wijngaard'. De gebouwen van Kauri liggen aan de rechterkant als U de parking oprijdt.";


                case "ckk":
                    return "";
                case "de troon":
                    return "";
                case "csr":
                    return "AANMELDEN VOORAAN BIJ DE RECEPTIE. Indien de receptie gesloten is gelieve de bel van 'De Regenboog' te gebruiken. Er komt dan iemand de voordeur opendoen. Indien dit niet lukt graag bellen naar 03/217 03 06 en wij helpen U verder.";
                default: return "";
            }
        }
    }
}
