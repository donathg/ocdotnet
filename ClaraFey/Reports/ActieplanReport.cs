﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using DevExpress.XtraReports.UI;
using sharedcode.BLL;
using sharedcode.WeergaveClasses;

namespace ClaraFey.Reports
{
    public partial class ActieplanReport : XtraReport
    {
        public ActieplanReport(ActieplanReportModel reportObj)
        {
            List<ActieplanReportModel> listReport = new List<ActieplanReportModel>
            {
                reportObj
            };

            InitializeComponent();

            this.DataSource = listReport;
            //afgewerkt infovelden
            if (!reportObj.Actieplan.IsAfgewerkt)
            {
                //Actieplan.afgwerktdate
                lblAfsluitdatum.Text = "Deadline";
                txtAfgwerktDate.Text = reportObj.Actieplan.deadline.GetValueOrDefault().ToString("D");
            }
            if (reportObj.Opvolgingen == null || reportObj.Opvolgingen.Count == 0)
            {
                Detail1.Visible = false;
                lblOpvolging.Visible = false;
            }
        }
    }
}
