﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using ClaraFey.BLL;
using System.Collections.Generic;

namespace ClaraFey.Reports
{
    public partial class KilometerPersoneelsdienstReport : DevExpress.XtraReports.UI.XtraReport
    {
        public KilometerPersoneelsdienstReport(sharedcode.BLL.KilometerPersoneelsdienstReport report)
        {
            InitializeComponent();
            List<sharedcode.BLL.KilometerPersoneelsdienstReport> listPI = new List<sharedcode.BLL.KilometerPersoneelsdienstReport>();
            listPI.Add(report);
            this.DataSource = listPI;
            this.Name = "kilometer_rapport_personeel_" + DateTime.Now.ToLongDateString();
            if (report.AfsluitDatum.HasValue==false)
            {
                lblAfsluitdatum.Visible = false;
                //tbAfsluitdatum.Visible = false;

            }
        }

    }
}
