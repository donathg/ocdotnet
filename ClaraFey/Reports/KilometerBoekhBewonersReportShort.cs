﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Collections.Generic;
using ClaraFey.BLL;
using sharedcode.BLL;
using sharedcode.WeergaveClasses;

namespace ClaraFey.Reports
{
    public partial class KilometerBoekhBewonersReportShort : XtraReport
    {
        public KilometerBoekhBewonersReportShort(KilometerLeefgroepBewonerReport report, KilometerModeType mode)
        {
            InitializeComponent();

            this.DataSource = new List<KilometerLeefgroepBewonerReport>
            {
                report
            };

            if (!report.AfsluitDatum.HasValue)
            {
                lblAfsluitdatum.Visible = false;
            }
            if (mode== KilometerModeType.bewoners)
            {
                lblBoekhBewoners.Text = "Cliënten";
                Name = "Kilometers_rapport_bewoners_" + DateTime.Now.ToLongDateString();
            }
            else if (mode == KilometerModeType.boekhouding)
            {
                lblBoekhBewoners.Text = "Leefgroepen";
                Name = "kilometers_rapport_leefgroepen_" + DateTime.Now.ToLongDateString();
            }
        }
    }
}
 
