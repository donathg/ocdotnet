﻿using sharedcode.BLL;
using sharedcode.WeergaveClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClaraFey.Reports.PrintModels
{
    public class ContactverslagReportModel
    {
        public List<ContactverslagWeergave> Contactverslagen { get; set; }
        public String FilterText { get; private set; }

        public void CreateFilterText(ContactverslagFilter filter)
        {
            FilterText = @"De contactverslag(en) zijn gefilterd op:

- bewoner: " + filter.GekozenBewoner.VoornaamAchternaam;

            if (filter.GekozenGebruiker != null)
                FilterText += @"
- uploader: " + filter.GekozenGebruiker.VoornaamAchternaam;
            
            if (filter.GekozenSoortBegeleiding != null)
                FilterText += @"
- soort begeleiding: " + filter.GekozenSoortBegeleiding.naam;

            if (filter.VanDatum != null && filter.TotDatum != null)
                FilterText += @"
- datum: " + filter.VanDatum.ToShortDateString() + " - " + filter.TotDatum.ToShortDateString();

            if (!String.IsNullOrWhiteSpace(filter.GekozenAfspraak))
                FilterText += @"
- afspraak bevat: " + filter.GekozenAfspraak;
        }
    }
}
