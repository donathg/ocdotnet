﻿using sharedcode.BLL;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClaraFey.Reports.PrintModels
{

    public class BestelbonAankoopIM
    {

        public BestelbonAankoopIM(List<AankoopIM> artikelen)
        {
            Artikelen = artikelen;

        }
        public DateTime BestelbonDatum { get { return Artikelen[0].ClosedDate.Value; } }
        public DateTime AfsluitMaand { get { return BestelbonDatum.AddMonths(-1); } }
        public List<AankoopIM> Artikelen { get; set; } = new List<AankoopIM>();
        public String LeverAdresCampus { get; set; } = "Thuis";
        public String LeverAdresStraat { get; set; }
        public String LeverAdresPostCodeGemeente { get; set; }
        public String AfleverInfo { get; set; }
    }
   
}
