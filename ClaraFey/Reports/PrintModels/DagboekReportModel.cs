﻿using ClaraFey.ViewControllers.ClientDossier;
using sharedcode.BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClaraFey.Reports.PrintModels
{
    public class DagboekReportModel
    {
        public List<DagboekWeergave> DagboekList { get; set; }
        public DagBoekFilter Filter { get; set; }
        public String Title { get; set; }
        public String FilterText { get; set; }

        public DagboekReportModel(DagboekVC vc)
        {
            DagboekList = vc.DagboekList.ToList();
            Filter = vc.Filter;
            Title = "Dagboek van " + Filter.VanDatum.ToShortDateString() + " tot " + Filter.TotDatum.ToShortDateString();
            CreateAndSetFilterBericht();
        }
        private void CreateAndSetFilterBericht()
        {
            String tempString = "";
            if (!String.IsNullOrWhiteSpace(Filter.NaamBewonerOfLeefgroep))
                tempString += "- Gericht aan: " + Filter.NaamBewonerOfLeefgroep + "\r\n";
            if (Filter.GekozenGebruiker != null && !String.IsNullOrWhiteSpace(Filter.GekozenGebruiker.VoornaamAchternaam))
                tempString += "- Aangemaakt door: " + Filter.GekozenGebruiker.VoornaamAchternaam + "\r\n";
            if (!String.IsNullOrWhiteSpace(Filter.Bericht))
                tempString += "- Bericht bevat de tekst: \"" + Filter.Bericht + "\"\n";
            if (Filter.Labels != null && Filter.Labels.Count != 0)
                tempString += "- " + Filter.Labels.Count + " labels\r\n";
            if (String.IsNullOrWhiteSpace(tempString))
                tempString = "niets";

            FilterText = tempString;
        }
    }
}
