﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using ClaraFey.BLL;
using System.Collections.Generic;
using sharedcode.BLL;

namespace ClaraFey.Reports
{
    public partial class WerkOpdrachtReport : XtraReport
    {
        public WerkOpdrachtReport(WerkOpdrachtPrintItem printItem)
        {
            InitializeComponent();
            xlLabelWerkopdracht.Text = "Werkbon " + printItem.NrText;
            lblDienst.Text = printItem.DienstText;
            this.DataSource = new List<WerkOpdrachtPrintItem> { printItem };
        }
    }
}
