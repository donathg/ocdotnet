﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using ClaraFey.BLL;
using System.Collections.Generic;
using sharedcode.BLL;
using sharedcode.WeergaveClasses;

namespace ClaraFey.Reports
{
    public partial class InventarisReport : DevExpress.XtraReports.UI.XtraReport
    {
        public InventarisReport(List<InventarisPrintItem> printItems)
        {
            InitializeComponent();
            DataSource = printItems;
        }

    }
}
