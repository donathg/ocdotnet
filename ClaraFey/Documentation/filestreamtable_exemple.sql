﻿CREATE TABLE inventaris_files
 (
	id int IDENTITY(1,1) NOT NULL,
	creator int NOT NULL,
	creationdate datetime NOT NULL,
	inv_id int NOT NULL,
	rowguid uniqueidentifier NOT NULL Unique ROWGUIDCOL Default newid(),
	filename nvarchar(128) NOT NULL,
	filedata varbinary(max) FILESTREAM  NULL,
	description nchar(128) NULL,
	primary key (id)
	
	)

	ALTER TABLE inventaris_files
ADD CONSTRAINT fk_invf_creator
FOREIGN KEY (creator)
REFERENCES gebruiker(geb_id)

	ALTER TABLE inventaris_files
ADD CONSTRAINT fk_invf_inv_id
FOREIGN KEY (inv_id)
REFERENCES inventaris(inv_id)








ALTER DATABASE ClaraFey
SET FILESTREAM( NON_TRANSACTED_ACCESS = FULL, DIRECTORY_NAME = N'ClaraFeyDocumenten' ) WITH NO_WAIT 
GO


CREATE TABLE WerkopdrachtFileTable AS FileTable 
WITH ( 
FileTable_Directory = 'Werkopdrachten', 
FileTable_Collate_Filename = database_default 
); 
GO


CREATE TABLE werkopdracht_files
 (
	id int IDENTITY(1,1) NOT NULL,
	creator int NOT NULL,
	creationdate datetime NOT NULL,
	wo_id int NOT NULL,
	stream_id uniqueidentifier NOT NULL,
	filename nvarchar(128) NOT NULL,
	description nchar(128) NULL,
	primary key (id)
	
)	ALTER TABLE werkopdracht_files
ADD CONSTRAINT fk_wof_creator
FOREIGN KEY (creator)
REFERENCES gebruiker(geb_id)

	ALTER TABLE werkopdracht_files
ADD CONSTRAINT fk_wof_wo_id
FOREIGN KEY (wo_id)
REFERENCES werkopdracht(wo_id)





CREATE TABLE werkopdracht_files
 (
	id int IDENTITY(1,1) NOT NULL,
	creator int NOT NULL,
	creationdate datetime NOT NULL,
	wo_id int NOT NULL,
	filename nvarchar(255) NOT NULL,
	filedata varbinary(max)  NULL,
	description nchar(255) NULL,
	primary key (id)
	
	)

	ALTER TABLE werkopdracht_files
ADD CONSTRAINT fk_wof_creator
FOREIGN KEY (creator)
REFERENCES gebruiker(geb_id)

	ALTER TABLE werkopdracht_files
ADD CONSTRAINT fk_wof_wo_id
FOREIGN KEY (wo_id)
REFERENCES werkopdracht(wo_id)


drop table werkopdracht_files
 