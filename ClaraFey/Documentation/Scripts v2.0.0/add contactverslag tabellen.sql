﻿/* CREATE Contactverslag_Soortbegeleiding table */
create table Contactverslag_SoortBegeleiding
(
	id int not null identity(1,1),

	naam varchar(100) not null,
	descr varchar(max),
	active bit not null default(1),

	primary key(id)
);
create index idx_active on Contactverslag_SoortBegeleiding(active);

/* CREATE Contactverslag table */
create table Contactverslag
(
	id int not null identity(1,1),

	bewoner_id int not null,
	soortBegeleiding_id int not null,

	afspraken varchar(max) not null,
	aanwezigen varchar(max) not null,
	datum date not null,

	creator int not null,
	creation_date datetime not null,

	modifier int not null,
	modification_date datetime not null,

	primary key (id),
	constraint fk_Contactverslag_bewonerid foreign key (bewoner_id) references bewoner(id),
	constraint fk_Contactverslag_soortbegeleidingid foreign key (soortBegeleiding_id) references Contactverslag_SoortBegeleiding(id),
	constraint fk_Contactverslag_creator foreign key (creator) references gebruiker(geb_id),
	constraint fk_Contactverslag_modifier foreign key (modifier) references gebruiker(geb_id)
);
create index idx_bewonerid on Contactverslag(bewoner_id);
create index idx_creator on Contactverslag(creator);
create index idx_modifier on Contactverslag(modifier);
create index idx_soortbegeleidingid on Contactverslag(soortBegeleiding_id);

/* CREATE Contactverslag_Labels table */
create table Contactverslag_Labels
(
	id int identity(1,1) not null,

	contactverslag_id int not null,
	label_id int not null,

	modifier int not null,
	modification_date datetime,

	primary key(id),
	constraint fk_ContactverslagType_contactverslagid foreign key (contactverslag_id) references Contactverslag(id),
	constraint fk_ContactverslagType_labelid foreign key (label_id) references labels(id),
	constraint fk_ContactverslagType_modifier foreign key (modifier) references gebruiker(geb_id)
);
create index idx_contactverslagid on Contactverslag_Labels(contactverslag_id);
create index idx_labelid on Contactverslag_Labels(label_id);
create index idx_modifier on Contactverslag_Labels(modifier);