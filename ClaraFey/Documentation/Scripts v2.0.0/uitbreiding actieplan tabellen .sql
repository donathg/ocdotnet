﻿/* CREATE Actieplan_Resultaat TABLE */
create table Actieplan_Resultaat
(
	id int not null identity(1,1),
	naam varchar(100) not null,
	beschrijving varchar(max),

	primary key (id)
);

/* INSERT VALUES INTO Actieplan_Resultaat TABLE */
insert into Actieplan_Resultaat values ('Gefaald','Alles is misgelopen. Leg uit waarom in het "Hoe Verliep Het?"-vak.');
insert into Actieplan_Resultaat values ('Doelstelling veranderd','Creëer nieuwe doelstelling met de aangepaste vereisten. Leg uit waarom de overschakeling in het "Hoe Verliep Het?"-vak.');
insert into Actieplan_Resultaat values ('Half geslaagd','Sommige dingen zijn gelukt, andere dan weer niet. Leg uit wat wel en wat niet in het "Hoe Verliep Het?"-vak.');
insert into Actieplan_Resultaat values ('Geslaagd','Alles is gelukt. Leg uit wat er werkt en waarom in het "Hoe Verliep Het?"-vak.');
insert into Actieplan_Resultaat values ('Afgewezen','Door omstandigheden is deze doelstelling niet meer haalbaar. Leg uit waarom in het "Hoe Verliep Het?"-vak.');

/* CREATE Actieplan_Evaluaties TABLE */
create table Actieplan_Evaluaties
(
	id int not null identity(1,1),
	actieplan_id int,
	bewoner_id int not null,
	type_id int not null,
	resultaat_id int not null,
	titel varchar(200) not null,
	hoeVerliepHet varchar(max) not null,
	datum_evaluatie date not null,
	start_datum date,
	eind_datum date,
	creator int not null,
	creation_date datetime not null,
	modifier int not null,
	modification_date datetime not null,

	primary key(id),
	constraint fk_ActieplanEvaluaties_actieplanid foreign key (actieplan_id) references actieplan(id),
	constraint fk_ActieplanEvaluaties_bewonerid foreign key (bewoner_id) references bewoner(id),
	constraint fk_ActieplanEvaluaties_typeid foreign key (type_id) references actieplantypes(id),
	constraint fk_ActieplanEvaluaties_resultaatid foreign key (resultaat_id) references Actieplan_Resultaat(id),
	constraint fk_ActieplanEvaluaties_creator foreign key (creator) references gebruiker(geb_id),
	constraint fk_ActieplanEvaluaties_modifier foreign key (modifier) references gebruiker(geb_id)
);
create index idx_actieplanid on Actieplan_Evaluaties(actieplan_id);
create index idx_bewonerid on Actieplan_Evaluaties(bewoner_id);
create index idx_typeid on Actieplan_Evaluaties(type_id);
create index idx_statusid on Actieplan_Evaluaties(resultaat_id);
create index idx_creator on Actieplan_Evaluaties(creator);
create index idx_modifier on Actieplan_Evaluaties(modifier);