/* ALTER adressen TABLE */
alter table adressen
alter column geslacht varchar(32)

/* SET INDEXES ON cd_files TABLE */
alter table cd_files
alter column description varchar(max);

create index index_creator on cd_files(creator);
create index index_bewonerid on cd_files(bewonerid);
create index index_category on cd_files(category);

/* SET INDEXES ON cd_files_labels TABLE */
create index index_fileid on cd_files_labels(fileid);
create index index_labelid on cd_files_labels(labelid);
create index index_modifier on cd_files_labels(modifier);

/* ADD id TO country TABLE */
alter table country 
add id int not null identity(1,1);

alter table country
drop constraint PK__country__3213E83F6C44B9C4;

alter table country
add constraint pk_country primary key (code);

/* ALTER dagboek TABLE */
alter table dagboek
alter column bewonerId int

alter table dagboek
add leefgroep_id int;

alter table dagboek
add constraint fk_dagboek_leefgroepid foreign key (leefgroep_id) references leefgroep(id);

create index index_creatorid on dagboek(creatorId);
create index index_leefgroepid on dagboek(leefgroep_id);
create index index_creationdate on dagboek(creationdate);

/* SET INDEXES ON dagboek_labels TABLE */
create index index_dagboekid on dagboek_labels(dagboekid);
create index index_labelid on dagboek_labels(labelid);
create index index_modifier on dagboek_labels(modifier);

USE [ClaraFey]
GO
ALTER TABLE [dbo].[dagboek_labels] ADD DEFAULT (getdate()) FOR [modificationdate]
GO

/* SET INDEXES ON dashboard TABLE */
create index index_creator on dashboard(creatorId);
create index index_bewonerid on dashboard(bewonerid);
create index index_creationdate on dashboard(creationdate);

/* SET INDEXES ON gebruiker TABLE */
create index index_subgroep on gebruiker(geb_subgroep);
create index index_personeelscode on gebruiker(geb_personeelscode);
create index index_voornaam on gebruiker(geb_voornaam);
create index index_achternaam on gebruiker(geb_achternaam);
create index index_functiebeschrijving on gebruiker(geb_functiebeschrijving);
create index index_werkplaats on gebruiker(geb_werkplaats);

/* ALTER inventaris_files TABLE */
alter table inventaris_files
alter column category varchar(128);

alter table inventaris_files
alter column description varchar(max);

alter table inventaris_files
add constraint fk_invf_category foreign key (category) references file_cats(code);

create index index_creator on inventaris_files(creator);
create index index_invid on inventaris_files(inv_id);
create index index_category on inventaris_files(category);

/* ALTER locatie_files TABLE */
alter table locatie_files
alter column category varchar(128);

alter table locatie_files
alter column description varchar(max);

alter table locatie_files
add constraint fk_locf_category foreign key (category) references file_cats(code);

create index index_creator on locatie_files(creator);
create index index_locid on locatie_files(loc_id);
create index index_category on locatie_files(category);

/* ALTER werkopdracht_files TABLE */
alter table werkopdracht_files
alter column category varchar(128);

alter table werkopdracht_files
alter column description varchar(max);

alter table werkopdracht_files
add constraint fk_wof_category foreign key (category) references file_cats(code);

create index index_creator on werkopdracht_files(creator);
create index index_woid on werkopdracht_files(wo_id);
create index index_category on werkopdracht_files(category);

/* ADD DATA TO file_cats TABLE */
insert into file_cats (code, name, descr, module) values ('AI-JURIDISCH_VREDERECHTER', 'Vrederechter', null, 'CD')
insert into file_cats (code, name, descr, module) values ('AI-JURIDISCH_RECHTBANK', 'Rechtbank', null, 'CD')
insert into file_cats (code, name, descr, module) values ('AI-JURIDISCH_ADVOCAAT', 'Advocaat', null, 'CD')

/* ADD UNIQUE CONSTRAINT TO dagboek_labels */
alter table dagboek_labels
add constraint UC_dagboeklabels unique (labelid, dagboekid)

/* ADD UNIQUE CONSTRAINT TO cd_file_labels */
alter table cd_files_labels
add constraint UC_fileslabels unique (labelid, fileid)