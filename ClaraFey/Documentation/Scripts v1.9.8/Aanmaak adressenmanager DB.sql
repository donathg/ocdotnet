create table adressen /* vervanger adres tabel */
(
	id int not null identity(1, 1),

	voornaam varchar(128),
	naam varchar(128) not null,
	btwnummer varchar(128),
	geboortedatum datetime,
	rijkregisternummer varchar(128),
	nationaliteit varchar(128),
	geslacht bit,
	telefoon varchar(128),
	gsm varchar (128),
	email varchar(128),
	isBedrijf bit not null,
	straat varchar(128) not null,
	huisnr varchar(16) not null,
	busnr varchar(16),
	postcode varchar(16) not null,
	gemeente varchar(128) not null,
	landcode varchar(3) not null,
	active bit,

	PRIMARY KEY (id),
	CONSTRAINT fk_adressen_land FOREIGN KEY (landcode) REFERENCES country(code)
)
CREATE INDEX index_land ON adressen(landcode); 
CREATE INDEX index_isbedrijf ON adressen(isBedrijf);
CREATE INDEX index_active ON adressen(active);

create table adrestype /* vervanger bewoner_adrestype tabel */
(
	id int not null identity(1, 1),

	naam varchar(128) not null,
	ishoofdkeuze bit not null,
	isBedrijf bit not null,
	opmerking varchar(max),
	active bit not null,

	PRIMARY KEY (id)
)
CREATE INDEX index_ishoofdkeuze ON adrestype(isHoofdkeuze);
CREATE INDEX index_active ON adrestype(active);

create table bewoner_adressen /* tussentabel tussen bewoner en adressen tabellen */
(
	id int not null identity(1, 1),

	bewoner_id int not null,
	adres_id int not null,
	adrestype_id int not null,

	PRIMARY KEY (id),
    CONSTRAINT FK_bewoneradressen_bewonerid FOREIGN KEY (bewoner_id) REFERENCES bewoner(id),
    CONSTRAINT FK_bewoneradressen_adresid FOREIGN KEY (adres_id) REFERENCES adressen(id),
    CONSTRAINT FK_bewoneradressen_adrestypeid FOREIGN KEY (adrestype_id) REFERENCES adrestype(id)
)
CREATE INDEX index_bewonerid ON bewoner_adressen (bewoner_id);
CREATE INDEX index_adresid ON bewoner_adressen (bewoner_id);
CREATE INDEX index_adrestypeid ON bewoner_adressen (adrestype_id);

create table bewoner_contactpersonen /* vervanger bewoner_adressen tabel */
(
	id int not null identity(1,1),

	adrestype_id int not null,
	bewoneradres_id int not null,

	voornaam varchar(128),
	naam varchar(128),
	functie varchar(128),
	telefoon varchar(128),
	gsm varchar(128),
	email varchar(128),
	opmerking varchar(128),

	PRIMARY KEY (id),
    CONSTRAINT FK_bewonercontactpersonen_adrestypeid FOREIGN KEY (adrestype_id) REFERENCES adrestype(id),
    CONSTRAINT FK_bewonercontactpersonen_bewoneradresid FOREIGN KEY (bewoneradres_id) REFERENCES bewoner_adressen(id)
)
CREATE INDEX index_adrestypeid ON bewoner_contactpersonen (adrestype_id);
CREATE INDEX index_bewoneradresid ON bewoner_contactpersonen (bewoneradres_id);



create table campussen /* tussentabel tussen locatie en adressen tabellen */
(
	id int not null identity(1, 1),

	adres_id int not null,
	adrestype_id int not null,
	locatie_id int not null,

	PRIMARY KEY (id),
	CONSTRAINT FK_campussen_adresid FOREIGN KEY (adres_id) REFERENCES adressen(id),
    CONSTRAINT FK_campussen_adrestypeid FOREIGN KEY (adrestype_id) REFERENCES adrestype(id),
	CONSTRAINT FK_campussen_locatieid FOREIGN KEY (locatie_id) REFERENCES locatie(loc_id)
)
CREATE INDEX index_adresid ON campussen (adres_id);
CREATE INDEX index_adrestypeid ON campussen (adrestype_id);
CREATE INDEX index_locatieid ON campussen (locatie_id);

create table campussen_contactpersonen /* nieuwe tabel voor contactpersonen per campus bij te houden */
(
	id int not null identity(1,1),

	adrestype_id int not null,
	campus_id int not null,

	voornaam varchar(128),
	naam varchar(128),
	functie varchar(128),
	telefoon varchar(128),
	gsm varchar(128),
	email varchar(128),
	opmerking varchar(128),

	PRIMARY KEY (id),
    CONSTRAINT FK_campuscontactpersonen_adrestypeid FOREIGN KEY (adrestype_id) REFERENCES adrestype(id),
    CONSTRAINT FK_campuscontactpersonen_campusid FOREIGN KEY (campus_id) REFERENCES campussen(id)
)
CREATE INDEX index_adrestypeid ON campussen_contactpersonen (adrestype_id);
CREATE INDEX index_campusid ON campussen_contactpersonen (campus_id);