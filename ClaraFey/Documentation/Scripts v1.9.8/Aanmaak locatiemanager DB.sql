create table objecttype
(
	id int not null identity(1,1),

	naam varchar(128) not null,
	beschrijving varchar(max),

	PRIMARY KEY (id)
)

create table locatie_objecten
(
	id int not null identity(1,1),

	loc_id int not null,
	objecttype_id int not null,

	serienummer varchar(128),
	opmerking varchar(max),
	hoeveelheid int,
	eigenaar varchar(128),

	PRIMARY KEY (id),
	CONSTRAINT FK_locatieobjecten_locid FOREIGN KEY (loc_id) REFERENCES locatie(loc_id),
	CONSTRAINT FK_locatieobjecten_objecttypeid FOREIGN KEY (objecttype_id) REFERENCES objecttype(id)
)
CREATE INDEX index_locid ON locatie_objecten(loc_id);
CREATE INDEX index_objecttypeid ON locatie_objecten(objecttype_id);