﻿



alter table bewoner drop column foto


create table bewoner_foto 
(
	id integer identity (1,1) primary key not null,
	bewonerId integer not null,
	foto varbinary(max)

)


drop table gebruiker_foto
create table gebruiker_foto 
(
	id integer identity (1,1)  primary key not null,
	gebruikerId integer not null,
	foto varbinary(max)

)


 ALTER TABLE bewoner_foto
ADD CONSTRAINT fk_bewoner_foto
FOREIGN KEY (bewonerId)
REFERENCES bewoner(id)


 ALTER TABLE gebruiker_foto
ADD CONSTRAINT fk_gebruiker_foto
FOREIGN KEY (gebruikerId)
REFERENCES gebruiker(id)



























create table dagboek_labels
(
	id integer identity(1,1) not null primary key,
	dagboekid integer not null,
	labelid integer not null,
	modifier integer not null,
	modificationdate date not null

)


ALTER TABLE dagboek_labels
ADD CONSTRAINT fk_dagboek_labels_dagboekid
FOREIGN KEY (dagboekid)
REFERENCES dagboek(id)

 ALTER TABLE dagboek_labels
ADD CONSTRAINT fk_dagboek_labels_labelid
FOREIGN KEY (labelid)
REFERENCES labels(id)

ALTER TABLE dagboek_labels
ADD CONSTRAINT fk_dagboek_labels_modifier
FOREIGN KEY (modifier)
REFERENCES gebruiker(geb_id)



















create table module
(

	code varchar(12) primary key,
	descr varchar(max)

)

insert into module values ('CD', 'Cliënt Dossier')

 
create table labels
(

	id integer identity(1,1) primary key not null,
	parentid integer,
	name varchar(50),
	descr varchar(max),
	module varchar(12),
	
)


ALTER TABLE labels
ADD CONSTRAINT fk_label_parentid
FOREIGN KEY (parentid)
REFERENCES labels(id)


CREATE INDEX ind_label_parent_id
ON labels (parentid)




ALTER TABLE labels
ADD CONSTRAINT fk_label_module
FOREIGN KEY (module)
REFERENCES module(code)


CREATE INDEX ind_label_module
ON labels (module)



alter table file_cats alter column module varchar(12) not null



ALTER TABLE file_cats
ADD CONSTRAINT fk_file_cats_module
FOREIGN KEY (module)
REFERENCES module(code)


CREATE INDEX ind_file_cats_module
ON file_cats (module)



create table cd_ai_gepland_begeleidingsteam_occf
(

	id int identity(1,1) not null primary key,
	bewonerid int not null,
	modifier int not null,
	modificationdate datetime not null,
	startdatum_begeiding datetime,
	leefgroepid int,
	locatieid int,
	orthopedagoog int,
	individuele_aandachtsopvoeder int,
	maatschappelijk_werker int,
	mobiele_begeleider int,
	medische_opvolging int,
	contactpersoon_dagbesteding int


)
ALTER TABLE cd_ai_gepland_begeleidingsteam_occf
ADD CONSTRAINT fk_cd_ai_gepland_begeleidingsteam_occf_bewonerid
FOREIGN KEY (bewonerid)
REFERENCES bewoner(id)


CREATE INDEX ind_cd_ai_gepland_begeleidingsteam_occf_bewonerid
ON cd_ai_gepland_begeleidingsteam_occf (bewonerid)








CREATE TABLE kilometer_type
(
	kmt_code varchar(6) not null,
	kmt_naam varchar(128)
)
CREATE TABLE kilometer_wagen
(
	kmw_id integer not null identity (1,1),
	kmw_naam varchar(128) not null
)
 
CREATE TABLE kilometers
(
km_id int not null identity(1,1),
km_kmt_code varchar(6) not null,
km_kmw_id integer not null,
km_datum datetime not null,
km_bestemming varchar(128) not null,
km_reden varchar(256),
km_omnium varchar(1) default 'N' not null,
km_beginstand integer,
km_eindstand integer,
km_kilometers decimal(6,2) not null,
km_pricekm decimal(6,2) not null,
km_price decimal(10,2) not null,
km_creator integer not null,
km_creationdate datetime not null,
km_boekhoudingdate datetime default null,
km_personeelsdienstdate datetime default null,
km_bewonersadmindate datetime default null,
km_closed_boekh_date datetime default null,
km_closed_persdienst_date datetime default null,
km_closed_bewoners_date datetime default null,
km_closed_boekh_user integer default null,
km_closed_persdienst_user integer default null,
km_closed_bewoners_user integer default null
); 
 ALTER TABLE kilometer_type
ADD CONSTRAINT pk_kmt_code PRIMARY KEY (kmt_code)

ALTER TABLE kilometers
ADD CONSTRAINT pk_km_id PRIMARY KEY (km_id)


ALTER TABLE kilometer_wagen
ADD CONSTRAINT pk_kmw_id PRIMARY KEY (kmw_id)


ALTER TABLE kilometers
ADD CONSTRAINT fk_closed_boekh_user
FOREIGN KEY (closed_boekh_user)
REFERENCES gebruikers(geb_id)

ALTER TABLE kilometers
ADD CONSTRAINT fk_closed_persdienst_user
FOREIGN KEY (closed_persdienst_user)
REFERENCES gebruikers(geb_id)

ALTER TABLE kilometers
ADD CONSTRAINT fk_closed_bewoners_user
FOREIGN KEY (closed_bewoners_user)
REFERENCES gebruikers(geb_id)




ALTER TABLE kilometers
ADD CONSTRAINT fk_km_creator
FOREIGN KEY (km_creator)
REFERENCES gebruikers(geb_id)

ALTER TABLE kilometers
ADD CONSTRAINT fk_km_kmt_code
FOREIGN KEY (km_kmt_code)
REFERENCES kilometer_type(kmt_code)

ALTER TABLE kilometers
ADD CONSTRAINT fk_km_kmw_id
FOREIGN KEY (km_kmw_id)
REFERENCES kilometer_wagen(kmw_id)



CREATE INDEX ind_km_kmw_id
ON kilometers (km_kmw_id)

CREATE INDEX ind_km_creator
ON kilometers (km_creator)

CREATE INDEX ind_km_kmt_code
ON kilometers (km_kmt_code)


CREATE INDEX ind_km_datum
ON kilometers (km_datum)

CREATE INDEX ind_km_kmt_code
ON kilometers (km_kmt_code)

 