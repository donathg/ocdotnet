/***** RECREATE Adressen TABEL *****/
drop table adressen

create table Adressen
(
	id int not null identity(1,1),
	/* BOTH */
	straat varchar(128),
	huisnr varchar(16),
	busnr varchar(16),
	postcode varchar(16),
	gemeente varchar(128),
	landcode varchar(3),
	telefoon varchar(128),
	gsm varchar(128),
	email varchar(128),
	active bit not null,
	naam varchar(128) not null,
	/* PERSON SPECIFIC */
	voornaam varchar(128),
	geboortedatum date,
	rijksregisternummer varchar(128),
	nationaliteit varchar(128),
	geslacht varchar(32),
	/* COMPANY SPECIFIC */
	btwnummer varchar(128),
	adrestype_id int,
	/* HISTORY DATA */
	creator int not null,
	creation_date datetime not null,
	modifier int not null,
	modification_date datetime not null,

	primary key (id),
	constraint fk_Adressen_adrestypeid foreign key (adrestype_id) references adrestype(id),
	constraint fk_Adressen_landcode foreign key (landcode) references country(code),
	constraint fk_Adressen_creator foreign key (creator) references gebruiker(geb_id),
	constraint fk_Adressen_modifier foreign key (modifier) references gebruiker(geb_id)
)
create index idx_adrestypeid on Adressen(adrestype_id);
create index idx_landcode on Adressen(landcode);
create index idx_active on Adressen(active);

/***** CREATE Adres_contactpersonen TABLE TO REPLACE bewoner_contactpersonen TABLE *****/
create table Adres_contactpersonen
(
	id int not null identity(1,1),
	adrestype_id int not null,
	adres_id int not null,
	voornaam varchar(128),
	naam varchar(128),
	functie varchar(128),
	telefoon varchar(128),
	gsm varchar(128),
	email varchar(128),
	opmerking varchar(max),

	primary key(id),
	constraint fk_AdresContactpersonen_adrestypeid foreign key (adrestype_id) references adrestype(id),
	constraint fk_AdresContactpersonen_adresid foreign key (adres_id) references Adressen(id),
	constraint unique_contactpersoon unique (adrestype_id, adres_id, naam)
)
create index idx_adrestypeid on Adres_contactpersonen(adrestype_id);
create index idx_adresid on Adres_contactpersonen(adres_id);

/***** RECREATE Bewoner_adressen TABLE *****/
drop table bewoner_adressen

create table Bewoner_adressen
(
	id int not null identity(1,1),
	bewoner_id int not null,
	adres_id int not null,
	adrestype_id int not null,
	aansluitingsnummer varchar(128),
	opmerking varchar(max),

	primary key(id),
	constraint fk_BewonerAdressen_bewonerid foreign key (bewoner_id) references bewoner(id),
	constraint fk_BewonerAdressen_adresid foreign key (adres_id) references Adressen(id),
	constraint fk_BewonerAdressen_adrestypeid foreign key (adrestype_id) references adrestype(id),
	constraint unique_bewoneradres unique(bewoner_id, adres_id, adrestype_id)
)
create index idx_bewonerid on Bewoner_adressen(bewoner_id);
create index idx_adresid on Bewoner_adressen(adres_id);
create index idx_adrestypeid on Bewoner_adressen(adrestype_id);

/***** CREATE Bewoneradres_contactpersonen TABLE *****/
create table Bewoneradres_contactpersonen
(
	id int not null identity(1,1),
	bewoneradres_id int not null,
	adrescontactpersoon_id int not null,

	primary key(id),
	constraint fk_BewoneradresContactpersonen_bewoneradresid foreign key (bewoneradres_id) references Bewoner_adressen(id),
	constraint fk_BewoneradresContactpersonen_adrescontactpersoonid foreign key (adrescontactpersoon_id) references Adres_contactpersonen(id),
	constraint unique_bewoneradrescontactpersonen unique (bewoneradres_id, adrescontactpersoon_id)
)
create index idx_bewoneradresid on Bewoneradres_contactpersonen (bewoneradres_id);
create index idx_adrescontactpersoonid on Bewoneradres_contactpersonen (adrescontactpersoon_id);

/***** REMOVE campussen_contactpersonen TABLE *****/
drop table campussen_contactpersonen

/***** RECREATE Campussen TABLE *****/
drop table campussen

create table Campussen 
(
	id int not null identity(1, 1),

	adres_id int not null,
	adrestype_id int not null,
	locatie_id int not null,

	PRIMARY KEY (id),
	CONSTRAINT FK_campussen_adresid FOREIGN KEY (adres_id) REFERENCES Adressen(id),
    CONSTRAINT FK_campussen_adrestypeid FOREIGN KEY (adrestype_id) REFERENCES adrestype(id),
	CONSTRAINT FK_campussen_locatieid FOREIGN KEY (locatie_id) REFERENCES locatie(loc_id)
)
CREATE INDEX index_adresid ON Campussen (adres_id);
CREATE INDEX index_adrestypeid ON Campussen (adrestype_id);
CREATE INDEX index_locatieid ON Campussen (locatie_id);

/***** ADD COLUMN TO adrestype *****/
alter table adrestype
add module_code varchar(12) not null default 'CD';