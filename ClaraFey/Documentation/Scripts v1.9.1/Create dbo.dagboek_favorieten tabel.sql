create table dbo.dagboek_favorieten
(
	id int IDENTITY(1,1) PRIMARY KEY,
	gebruikerId int NOT NULL,
	dagboekId int NOT NULL,
	creationDate date NOT NULL,
	CONSTRAINT FK_gebruikerId FOREIGN KEY (gebruikerId) REFERENCES gebruiker(geb_id),
	CONSTRAINT FK_dagboekId FOREIGN KEY (dagboekId) REFERENCES dagboek(id)
)
CREATE INDEX idx_dagboek_favorieten_gebruikerid
ON dagboek_favorieten (gebruikerId);

CREATE INDEX idx_dagboek_favorieten_dagboekid
ON dagboek_favorieten (dagboekId);
