﻿create table dbo.dashboard_verwijderd
(
	id int IDENTITY(1,1) PRIMARY KEY,
	gebruikerId int NOT NULL,
	dashboardId int NOT NULL,
	creationDate date NOT NULL,
	CONSTRAINT FK_dashboard_verwijderd_gebruikerId FOREIGN KEY (gebruikerId) REFERENCES gebruiker(geb_id),
	CONSTRAINT FK_dashboard_verwijderd_dashboardId FOREIGN KEY (dashboardId) REFERENCES dashboard(id)
)
CREATE INDEX idx_dashboard_verwijderd_gebruikerId
ON dashboard_verwijderd (gebruikerId);

CREATE INDEX idx_dashboard_verwijderd_dashboardId
ON dashboard_verwijderd (dashboardId);
