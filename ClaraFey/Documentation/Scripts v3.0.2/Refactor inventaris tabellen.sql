﻿/** REFACTORING (eerst inventaris SHEMA aanmaken) */
-- REFACTOR dbo.toestel TO inventaris.toestel
CREATE TABLE inventaris.toestel
(
	id int IDENTITY(1,1) NOT NULL,
	naam varchar(50) not null,
	category varchar(10) not null,
	actief bit not null default 1,

	primary key(id)
)
create index ind_actief on inventaris.toestel(actief);

-- REFACTOR dbo.merk TO inventaris.merk 
CREATE TABLE inventaris.merk
(
	id int IDENTITY(1,1) NOT NULL,
	naam varchar(50) not null,
	actief bit not null default 1,

	primary key(id)
)
create index ind_actief on inventaris.merk(actief);

-- SPLIT OFF IT PART FROM dbo.inventaris TO inventaris.infoIT
CREATE TABLE inventaris.infoIT
(
	id int IDENTITY(1,1) NOT NULL,

	datarack_id int,

	ipAdres varchar(15),
	ipHasDHCP bit,
	macAdres varchar(17),
	patchPaneel varchar(10),
	deviceLogin varchar(20),
	deviceWachtwoord varchar(20),
	deviceURL varchar(100),
	switchPoort varchar(2),
	switchIpadres varchar(15),
	wifiMacadres varchar(17),
	vlan int,
	inventaris_id_TEMP int not null,

	primary key(id),
	constraint fk_infoIt_datarackid foreign key (datarack_id) references dbo.locatie_objecten(id)
)
create index ind_datarackid on inventaris.infoIT(datarack_id);

-- REFACTOR dbo.inventaris TO inventaris.inventaris 
CREATE TABLE inventaris.inventaris
(
	id int IDENTITY(1,1) NOT NULL,
		
	toestel_id int,
	merk_id int,
	locatie_id int,
	leverancier_id int,
	infoIT_id int,

	nummer_263 varchar(30),
	label varchar(30),
	productCode varchar(50),
	productOmschrijving varchar(255) not null,
	serieNummer varchar(50),
	bestelbon varchar(10),
	uitDienst date,
	aankoopDatum date,
	opmerking varchar(max),
	investering decimal,
	groep varchar(8),
	creator int not null,
	creationDate datetime not null,
	modifier int not null,
	modificationDate datetime not null,
	status varchar(1),
	actief bit not null default 1,

	primary key(id),
	constraint UC_nummer263 unique(nummer_263),
	constraint fk_inventaris_toestelid foreign key (toestel_id) references inventaris.toestel(id),
	constraint fk_inventaris_merkid foreign key (merk_id) references inventaris.merk(id),
	constraint fk_inventaris_locatieid foreign key (locatie_id) references dbo.locatie(id),
	constraint fk_inventaris_leverancierid foreign key (leverancier_id) references dbo.leverancier(lev_id),
	constraint fk_inventaris_infoITid foreign key (infoIT_id) references inventaris.infoIT(id),
	constraint fk_inventaris_creator foreign key (creator) references dbo.gebruiker(id),
	constraint fk_inventaris_modifier foreign key (modifier) references dbo.gebruiker(id)
)
create index ind_actief on inventaris.inventaris(actief);
create index ind_toestelid on inventaris.inventaris(toestel_id);
create index ind_merkid on inventaris.inventaris(merk_id);
create index ind_locatieid on inventaris.inventaris(locatie_id);
create index ind_leverancier_id on inventaris.inventaris(leverancier_id);
create index ind_infoITid on inventaris.inventaris(infoIT_id);
create index ind_creator on inventaris.inventaris(creator);
create index ind_modifier on inventaris.inventaris(modifier);
create index ind_nummer263 on inventaris.inventaris(nummer_263);


/** OVERZETTEN INFO VAN OUDE TABELLEN NAAR NIEUWE TABELLEN */
-- dbo.toestel OVERZETTEN NAAR inventaris.toestel
SET IDENTITY_INSERT inventaris.toestel ON;
insert into inventaris.toestel (id, naam, category, actief)
select tst_id, tst_naam, tst_cat, 1  from dbo.toestel;
SET IDENTITY_INSERT inventaris.toestel OFF;

-- dbo.merk OVERZTEEN NAAR inventaris.merk
SET IDENTITY_INSERT inventaris.merk ON;
insert into inventaris.merk(id, naam, actief)
select mer_id, mer_naam, 1  from dbo.merk;
SET IDENTITY_INSERT inventaris.merk OFF;

-- dbo.inventaris OVERZETTEN NAAR inventaris.inventaris
SET IDENTITY_INSERT inventaris.inventaris ON;
INSERT INTO inventaris.inventaris (id, toestel_id, merk_id, locatie_id, leverancier_id, nummer_263, label, productCode, productOmschrijving, serieNummer,
	bestelbon, uitDienst, aankoopDatum, opmerking, investering, groep, creator, creationDate, modifier, modificationDate, status, actief)
select inv_id, inv_tst_id, inv_mer_id, inv_loc_id, inv_lev_id, inv_263_nummer, inv_label, inv_productcode, inv_productomschrijving, inv_serienummer,
	inv_bestelbon, inv_uitdienst, inv_aankoopdatum, inv_opmerking, inv_investering, inv_groep, inv_creatie_geb_id, inv_creatiedatum, inv_wijziging_geb_id,
	inv_wijziginggsdatum, inv_status, 1 from dbo.inventaris;
SET IDENTITY_INSERT inventaris.inventaris OFF;

/** OPSPITSEN dbo.inventaris NAAR inventaris.infoIT */
-- dbo.inventaris OVERZETTEN NAAR inventaris.infoIT
insert into inventaris.infoIT (ipAdres, macAdres, deviceLogin, deviceWachtwoord, inventaris_id_TEMP)
select inv_ipadres, inv_macadres, inv_devicelogin, inv_devicewachtwoord, inv_id from dbo.inventaris
where ((inv_ipadres is not null and datalength(LTRIM(RTRIM(inv_ipadres))) != 0) or 
	   (inv_macadres is not null and datalength(LTRIM(RTRIM(inv_macadres))) != 0) or 
	   (inv_devicelogin is not null and datalength(LTRIM(RTRIM(inv_devicelogin))) != 0) or 
	   (inv_devicewachtwoord is not null and datalength(LTRIM(RTRIM(inv_devicewachtwoord))) != 0));

-- inventaris.inventaris LINKEN AAN inventaris.infoIT
update inventaris.inventaris
set infoIT_id = (select id from inventaris.infoIT WHERE infoIT.inventaris_id_TEMP = inventaris.id)

-- REMOVE TEMPORARY ID FROM inventaris.infoIt
alter table inventaris.infoIt
drop column inventaris_id_TEMP;


/** TOEVOEGEN RECHT AAN rechten_functie */
insert into rechten_functie(func_code, func_naam, func_cat, func_opm, module_code)
values ('202', 'Toegang tot IT-tab', 'inventaris', null, 'INVENTARIS');