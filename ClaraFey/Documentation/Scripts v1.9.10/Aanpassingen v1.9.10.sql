/* ALTER dagboek: ADD 2 COLUMNS, FOREIGN KEY AND INDEX (TO KEEP MODIFIER + DATE) */
alter table dagboek
add modifier int;

alter table dagboek
add modification_date date;

alter table dagboek
add constraint FK_dagboek_modifier foreign key (modifier) references gebruiker(geb_id);

create index idx_modifier on dagboek(modifier);