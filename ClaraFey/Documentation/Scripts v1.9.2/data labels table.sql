﻿GO
SET IDENTITY_INSERT [dbo].[labels] ON 

GO
INSERT [dbo].[labels] ([id], [parentid], [name], [descr], [module], [active], [dagboek], [bijlagen], [deadlinedate]) VALUES (1, NULL, N'pedagogisch', NULL, N'CD', 1, 1, 1, 1)
GO
INSERT [dbo].[labels] ([id], [parentid], [name], [descr], [module], [active], [dagboek], [bijlagen], [deadlinedate]) VALUES (2, NULL, N'psychotherapie', NULL, N'CD', 1, 1, 1, 0)
GO
INSERT [dbo].[labels] ([id], [parentid], [name], [descr], [module], [active], [dagboek], [bijlagen], [deadlinedate]) VALUES (3, NULL, N'context - sociaal', NULL, N'CD', 1, 1, 1, 0)
GO
INSERT [dbo].[labels] ([id], [parentid], [name], [descr], [module], [active], [dagboek], [bijlagen], [deadlinedate]) VALUES (4, NULL, N'medische begeleidingsinfo', NULL, N'CD', 1, 0, 1, 0)
GO
INSERT [dbo].[labels] ([id], [parentid], [name], [descr], [module], [active], [dagboek], [bijlagen], [deadlinedate]) VALUES (5, 4, N'medicatiefiche', NULL, N'CD', 1, 0, 1, 0)
GO
INSERT [dbo].[labels] ([id], [parentid], [name], [descr], [module], [active], [dagboek], [bijlagen], [deadlinedate]) VALUES (6, 4, N'ziekenhuisfiche', NULL, N'CD', 1, 0, 1, 0)
GO
INSERT [dbo].[labels] ([id], [parentid], [name], [descr], [module], [active], [dagboek], [bijlagen], [deadlinedate]) VALUES (7, NULL, N'administratieve info', NULL, N'CD', 1, 1, 1, 0)
GO
INSERT [dbo].[labels] ([id], [parentid], [name], [descr], [module], [active], [dagboek], [bijlagen], [deadlinedate]) VALUES (8, NULL, N'CLB', NULL, N'CD', 1, 1, 1, 0)
GO
INSERT [dbo].[labels] ([id], [parentid], [name], [descr], [module], [active], [dagboek], [bijlagen], [deadlinedate]) VALUES (9, NULL, N'dagbesteding', NULL, N'CD', 1, 0, 1, 1)
GO
INSERT [dbo].[labels] ([id], [parentid], [name], [descr], [module], [active], [dagboek], [bijlagen], [deadlinedate]) VALUES (10, 9, N'werkshema', NULL, N'CD', 1, 0, 1, 1)
GO
INSERT [dbo].[labels] ([id], [parentid], [name], [descr], [module], [active], [dagboek], [bijlagen], [deadlinedate]) VALUES (11, 9, N'behoeftenonderzoek', NULL, N'CD', 1, 0, 1, 1)
GO
INSERT [dbo].[labels] ([id], [parentid], [name], [descr], [module], [active], [dagboek], [bijlagen], [deadlinedate]) VALUES (12, 9, N'verslagen', NULL, N'CD', 1, 0, 1, 0)
GO
INSERT [dbo].[labels] ([id], [parentid], [name], [descr], [module], [active], [dagboek], [bijlagen], [deadlinedate]) VALUES (13, NULL, N'erkenningen', NULL, N'CD', 1, 0, 1, 1)
GO
INSERT [dbo].[labels] ([id], [parentid], [name], [descr], [module], [active], [dagboek], [bijlagen], [deadlinedate]) VALUES (14, 13, N'ITP', NULL, N'CD', 1, 0, 1, 1)
GO
INSERT [dbo].[labels] ([id], [parentid], [name], [descr], [module], [active], [dagboek], [bijlagen], [deadlinedate]) VALUES (15, 13, N'VAPH', NULL, N'CD', 1, 0, 1, 1)
GO
INSERT [dbo].[labels] ([id], [parentid], [name], [descr], [module], [active], [dagboek], [bijlagen], [deadlinedate]) VALUES (16, 13, N'geweigerde erkennigen', NULL, N'CD', 1, 0, 1, 1)
GO
INSERT [dbo].[labels] ([id], [parentid], [name], [descr], [module], [active], [dagboek], [bijlagen], [deadlinedate]) VALUES (17, NULL, N'externe hulpverlening', NULL, N'CD', 1, 1, 1, 0)
GO
INSERT [dbo].[labels] ([id], [parentid], [name], [descr], [module], [active], [dagboek], [bijlagen], [deadlinedate]) VALUES (18, NULL, N'financieel', NULL, N'CD', 1, 1, 1, 1)
GO
INSERT [dbo].[labels] ([id], [parentid], [name], [descr], [module], [active], [dagboek], [bijlagen], [deadlinedate]) VALUES (19, 18, N'FOD - kinderbijslag', NULL, N'CD', 1, 0, 1, 1)
GO
INSERT [dbo].[labels] ([id], [parentid], [name], [descr], [module], [active], [dagboek], [bijlagen], [deadlinedate]) VALUES (20, 18, N'IPH', NULL, N'CD', 1, 0, 1, 1)
GO
INSERT [dbo].[labels] ([id], [parentid], [name], [descr], [module], [active], [dagboek], [bijlagen], [deadlinedate]) VALUES (21, 18, N'PVB toewijzing', NULL, N'CD', 1, 0, 1, 1)
GO
INSERT [dbo].[labels] ([id], [parentid], [name], [descr], [module], [active], [dagboek], [bijlagen], [deadlinedate]) VALUES (22, 18, N'PVB terbeschikkingstelling', NULL, N'CD', 1, 0, 1, 1)
GO
INSERT [dbo].[labels] ([id], [parentid], [name], [descr], [module], [active], [dagboek], [bijlagen], [deadlinedate]) VALUES (23, 18, N'zakgeld - afspraken', NULL, N'CD', 1, 1, 1, 1)
GO
INSERT [dbo].[labels] ([id], [parentid], [name], [descr], [module], [active], [dagboek], [bijlagen], [deadlinedate]) VALUES (24, NULL, N'hulpmiddelen - visualisaties', NULL, N'CD', 1, 1, 1, 0)
GO
INSERT [dbo].[labels] ([id], [parentid], [name], [descr], [module], [active], [dagboek], [bijlagen], [deadlinedate]) VALUES (25, NULL, N'juridisch - JRB', NULL, N'CD', 1, 1, 1, 1)
GO
INSERT [dbo].[labels] ([id], [parentid], [name], [descr], [module], [active], [dagboek], [bijlagen], [deadlinedate]) VALUES (26, 25, N'vonissen', NULL, N'CD', 1, 1, 1, 1)
GO
INSERT [dbo].[labels] ([id], [parentid], [name], [descr], [module], [active], [dagboek], [bijlagen], [deadlinedate]) VALUES (27, 25, N'correspondentie - afspraken', NULL, N'CD', 1, 1, 1, 0)
GO
INSERT [dbo].[labels] ([id], [parentid], [name], [descr], [module], [active], [dagboek], [bijlagen], [deadlinedate]) VALUES (28, 25, N'verslagen', NULL, N'CD', 1, 0, 1, 0)
GO
INSERT [dbo].[labels] ([id], [parentid], [name], [descr], [module], [active], [dagboek], [bijlagen], [deadlinedate]) VALUES (29, 4, N'verslagen', NULL, N'CD', 1, 0, 1, 0)
GO
INSERT [dbo].[labels] ([id], [parentid], [name], [descr], [module], [active], [dagboek], [bijlagen], [deadlinedate]) VALUES (30, NULL, N'ontvluchting', NULL, N'CD', 1, 1, 1, 0)
GO
INSERT [dbo].[labels] ([id], [parentid], [name], [descr], [module], [active], [dagboek], [bijlagen], [deadlinedate]) VALUES (31, 30, N'seinen', NULL, N'CD', 1, 1, 1, 0)
GO
INSERT [dbo].[labels] ([id], [parentid], [name], [descr], [module], [active], [dagboek], [bijlagen], [deadlinedate]) VALUES (32, 30, N'ontseinen', NULL, N'CD', 1, 1, 1, 0)
GO
INSERT [dbo].[labels] ([id], [parentid], [name], [descr], [module], [active], [dagboek], [bijlagen], [deadlinedate]) VALUES (33, 30, N'weglooptraject - afspraken', NULL, N'CD', 1, 1, 1, 0)
GO
INSERT [dbo].[labels] ([id], [parentid], [name], [descr], [module], [active], [dagboek], [bijlagen], [deadlinedate]) VALUES (34, NULL, N'overeenkomsten', NULL, N'CD', 1, 1, 1, 1)
GO
INSERT [dbo].[labels] ([id], [parentid], [name], [descr], [module], [active], [dagboek], [bijlagen], [deadlinedate]) VALUES (35, 34, N'IDO', NULL, N'CD', 1, 1, 1, 1)
GO
INSERT [dbo].[labels] ([id], [parentid], [name], [descr], [module], [active], [dagboek], [bijlagen], [deadlinedate]) VALUES (36, 34, N'IDO - handelingsplan', NULL, N'CD', 1, 1, 1, 1)
GO
INSERT [dbo].[labels] ([id], [parentid], [name], [descr], [module], [active], [dagboek], [bijlagen], [deadlinedate]) VALUES (37, 34, N'offerte', NULL, N'CD', 1, 0, 1, 1)
GO
INSERT [dbo].[labels] ([id], [parentid], [name], [descr], [module], [active], [dagboek], [bijlagen], [deadlinedate]) VALUES (38, 34, N'zorgboerderij - overeenkomst', NULL, N'CD', 1, 0, 1, 1)
GO
INSERT [dbo].[labels] ([id], [parentid], [name], [descr], [module], [active], [dagboek], [bijlagen], [deadlinedate]) VALUES (39, 34, N'begeleid werken - overeenkomst', NULL, N'CD', 1, 0, 1, 1)
GO
INSERT [dbo].[labels] ([id], [parentid], [name], [descr], [module], [active], [dagboek], [bijlagen], [deadlinedate]) VALUES (40, 34, N'stage - overeenkomst', NULL, N'CD', 1, 0, 1, 1)
GO
INSERT [dbo].[labels] ([id], [parentid], [name], [descr], [module], [active], [dagboek], [bijlagen], [deadlinedate]) VALUES (41, 34, N'arbeidsovereenkomst', NULL, N'CD', 1, 0, 1, 1)
GO
INSERT [dbo].[labels] ([id], [parentid], [name], [descr], [module], [active], [dagboek], [bijlagen], [deadlinedate]) VALUES (42, 34, N'overeenkomst met externe hulpverlener', NULL, N'CD', 1, 0, 1, 1)
GO
INSERT [dbo].[labels] ([id], [parentid], [name], [descr], [module], [active], [dagboek], [bijlagen], [deadlinedate]) VALUES (43, NULL, N'paramedisch', NULL, N'CD', 1, 1, 1, 0)
GO
INSERT [dbo].[labels] ([id], [parentid], [name], [descr], [module], [active], [dagboek], [bijlagen], [deadlinedate]) VALUES (44, 43, N'logopedie', NULL, N'CD', 1, 1, 1, 0)
GO
INSERT [dbo].[labels] ([id], [parentid], [name], [descr], [module], [active], [dagboek], [bijlagen], [deadlinedate]) VALUES (45, 43, N'ergotherapie', NULL, N'CD', 1, 1, 1, 0)
GO
INSERT [dbo].[labels] ([id], [parentid], [name], [descr], [module], [active], [dagboek], [bijlagen], [deadlinedate]) VALUES (46, 43, N'kinesitherapie', NULL, N'CD', 1, 1, 1, 0)
GO
INSERT [dbo].[labels] ([id], [parentid], [name], [descr], [module], [active], [dagboek], [bijlagen], [deadlinedate]) VALUES (47, 43, N'andere', NULL, N'CD', 1, 1, 1, 0)
GO
INSERT [dbo].[labels] ([id], [parentid], [name], [descr], [module], [active], [dagboek], [bijlagen], [deadlinedate]) VALUES (48, 1, N'testgegevens - diagnostiek', NULL, N'CD', 1, 1, 1, 1)
GO
INSERT [dbo].[labels] ([id], [parentid], [name], [descr], [module], [active], [dagboek], [bijlagen], [deadlinedate]) VALUES (49, 1, N'SEO', NULL, N'CD', 1, 1, 1, 1)
GO
INSERT [dbo].[labels] ([id], [parentid], [name], [descr], [module], [active], [dagboek], [bijlagen], [deadlinedate]) VALUES (50, 1, N'A-document', NULL, N'CD', 1, 0, 1, 1)
GO
INSERT [dbo].[labels] ([id], [parentid], [name], [descr], [module], [active], [dagboek], [bijlagen], [deadlinedate]) VALUES (51, 1, N'ondersteuningsplan', NULL, N'CD', 1, 1, 1, 1)
GO
INSERT [dbo].[labels] ([id], [parentid], [name], [descr], [module], [active], [dagboek], [bijlagen], [deadlinedate]) VALUES (52, 1, N'verslagen', NULL, N'CD', 1, 0, 1, 0)
GO
INSERT [dbo].[labels] ([id], [parentid], [name], [descr], [module], [active], [dagboek], [bijlagen], [deadlinedate]) VALUES (53, NULL, N'SOAS', NULL, N'CD', 1, 0, 1, 0)
GO
INSERT [dbo].[labels] ([id], [parentid], [name], [descr], [module], [active], [dagboek], [bijlagen], [deadlinedate]) VALUES (54, NULL, N'persoonlijke handleidingen', NULL, N'CD', 1, 1, 1, 1)
GO
INSERT [dbo].[labels] ([id], [parentid], [name], [descr], [module], [active], [dagboek], [bijlagen], [deadlinedate]) VALUES (55, 54, N'vrijheidsbeperking', NULL, N'CD', 1, 1, 1, 1)
GO
INSERT [dbo].[labels] ([id], [parentid], [name], [descr], [module], [active], [dagboek], [bijlagen], [deadlinedate]) VALUES (56, 54, N'(sonde)voeding', NULL, N'CD', 1, 1, 1, 1)
GO
INSERT [dbo].[labels] ([id], [parentid], [name], [descr], [module], [active], [dagboek], [bijlagen], [deadlinedate]) VALUES (57, NULL, N'school - stage', NULL, N'CD', 1, 1, 1, 0)
GO
INSERT [dbo].[labels] ([id], [parentid], [name], [descr], [module], [active], [dagboek], [bijlagen], [deadlinedate]) VALUES (58, 57, N'diploma', NULL, N'CD', 1, 0, 1, 0)
GO
INSERT [dbo].[labels] ([id], [parentid], [name], [descr], [module], [active], [dagboek], [bijlagen], [deadlinedate]) VALUES (59, 57, N'TOAH', NULL, N'CD', 1, 1, 1, 0)
GO
INSERT [dbo].[labels] ([id], [parentid], [name], [descr], [module], [active], [dagboek], [bijlagen], [deadlinedate]) VALUES (60, 57, N'verslagen', NULL, N'CD', 1, 0, 1, 0)
GO
INSERT [dbo].[labels] ([id], [parentid], [name], [descr], [module], [active], [dagboek], [bijlagen], [deadlinedate]) VALUES (61, 57, N'schorsing', NULL, N'CD', 1, 1, 1, 0)
GO
INSERT [dbo].[labels] ([id], [parentid], [name], [descr], [module], [active], [dagboek], [bijlagen], [deadlinedate]) VALUES (62, 57, N'schooltraject - afspraken', NULL, N'CD', 1, 1, 1, 0)
GO
INSERT [dbo].[labels] ([id], [parentid], [name], [descr], [module], [active], [dagboek], [bijlagen], [deadlinedate]) VALUES (64, NULL, N'vrije tijd - extra''s', NULL, N'CD', 1, 1, 1, 0)
GO
INSERT [dbo].[labels] ([id], [parentid], [name], [descr], [module], [active], [dagboek], [bijlagen], [deadlinedate]) VALUES (65, 64, N'kampen en vakantie''s', NULL, N'CD', 1, 1, 1, 0)
GO
INSERT [dbo].[labels] ([id], [parentid], [name], [descr], [module], [active], [dagboek], [bijlagen], [deadlinedate]) VALUES (66, 64, N'sport', NULL, N'CD', 1, 1, 1, 0)
GO
INSERT [dbo].[labels] ([id], [parentid], [name], [descr], [module], [active], [dagboek], [bijlagen], [deadlinedate]) VALUES (67, 64, N'hobbyclub', NULL, N'CD', 1, 1, 1, 0)
GO
INSERT [dbo].[labels] ([id], [parentid], [name], [descr], [module], [active], [dagboek], [bijlagen], [deadlinedate]) VALUES (68, 64, N'uitgaan', NULL, N'CD', 1, 1, 1, 0)
GO
INSERT [dbo].[labels] ([id], [parentid], [name], [descr], [module], [active], [dagboek], [bijlagen], [deadlinedate]) VALUES (69, NULL, N'weekend- en vakantieregeling', NULL, N'CD', 1, 1, 1, 0)
GO
INSERT [dbo].[labels] ([id], [parentid], [name], [descr], [module], [active], [dagboek], [bijlagen], [deadlinedate]) VALUES (70, NULL, N'werk', NULL, N'CD', 1, 1, 1, 1)
GO
INSERT [dbo].[labels] ([id], [parentid], [name], [descr], [module], [active], [dagboek], [bijlagen], [deadlinedate]) VALUES (71, NULL, N'wonen - dagondersteuning', NULL, N'CD', 1, 1, 1, 0)
GO
INSERT [dbo].[labels] ([id], [parentid], [name], [descr], [module], [active], [dagboek], [bijlagen], [deadlinedate]) VALUES (72, 70, N'sollicitatie - CV', NULL, N'CD', 1, 1, 1, 0)
GO
INSERT [dbo].[labels] ([id], [parentid], [name], [descr], [module], [active], [dagboek], [bijlagen], [deadlinedate]) VALUES (73, 70, N'werkboek - arbeidsinteressetest', NULL, N'CD', 1, 1, 1, 0)
GO
INSERT [dbo].[labels] ([id], [parentid], [name], [descr], [module], [active], [dagboek], [bijlagen], [deadlinedate]) VALUES (74, 70, N'werktraject - afspraken', NULL, N'CD', 1, 1, 1, 1)
GO
SET IDENTITY_INSERT [dbo].[labels] OFF
GO
