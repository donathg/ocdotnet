﻿Alter table rechten_groepenlabels
add constraint fk_rechten_groepenlabels_groepcode
foreign key (groepcode)
references rechten_groep(groep_code)

Alter table rechten_groepenlabels
add constraint fk_rechten_groepenlabels_labelid
foreign key (labelid)
references labels(id)

  alter table labels
  add unique (name, parentid);

  create table dagboek_labels (
	id int not null identity(1,1) primary key,
	labelid int not null,
	dagboekid int not null,
	modifier int not null,

	constraint fk_dagboek_labels_labelid foreign key (labelid) references labels(id),
	constraint fk_dagboek_labels_dagboekid foreign key (dagboekid) references dagboek(id),
	constraint fk_dagboek_labels_modifier foreign key (modifier) references gebruiker(geb_id)
  )

  create index ind_dagboek_labels on dagboek_labels(labelid, dagboekid, modifier)

  create table dashboard_labels (
	id int not null identity(1,1) primary key,
	labelid int not null,
	dashboardid int not null,
	modifier int not null,

	constraint fk_dashboard_labels_labelid foreign key (labelid) references labels(id),
	constraint fk_dashboard_labels_dashboardid foreign key (dashboardid) references dashboard(id),
	constraint fk_dashboard_labels_modifier foreign key (modifier) references gebruiker(geb_id)
  )

  create index ind_dashboard_labels on dashboard_labels(labelid, dashboardid, modifier)