/** RENAME gebruiker Table COLUMNS */
sp_rename 'dbo.gebruiker.geb_id', 'id', 'COLUMN';
sp_rename 'dbo.gebruiker.geb_personeelscode', 'personeelscode', 'COLUMN';
sp_rename 'dbo.gebruiker.geb_wachtwoord', 'wachtwoord', 'COLUMN';
sp_rename 'dbo.gebruiker.geb_lastlogindatum', 'lastlogindatum', 'COLUMN';
sp_rename 'dbo.gebruiker.geb_email', 'email', 'COLUMN';
sp_rename 'dbo.gebruiker.geb_voornaam', 'voornaam', 'COLUMN';
sp_rename 'dbo.gebruiker.geb_achternaam', 'achternaam', 'COLUMN';
sp_rename 'dbo.gebruiker.geb_subgroep', 'subgroep', 'COLUMN';
sp_rename 'dbo.gebruiker.geb_telefoon', 'telefoon', 'COLUMN';
sp_rename 'dbo.gebruiker.geb_gsm', 'gsm', 'COLUMN';
sp_rename 'dbo.gebruiker.geb_functiebeschrijving', 'functiebeschrijving', 'COLUMN';
sp_rename 'dbo.gebruiker.geb_werkplaats', 'werkplaats', 'COLUMN';
sp_rename 'dbo.gebruiker.geb_telefoon_intern', 'telefoon_intern', 'COLUMN';
sp_rename 'dbo.gebruiker.geb_telefoon_intern_groep', 'telefoon_intern_groep', 'COLUMN';
sp_rename 'dbo.gebruiker.geb_foto', 'foto', 'COLUMN';

/** RECREATE ALL CONSTRAINTS, KEYS & INDEXES */
ALTER TABLE gebruiker
DROP CONSTRAINT fk_subgroep;
ALTER TABLE gebruiker
ADD CONSTRAINT fk_gebruiker_subgroep FOREIGN KEY (subgroep) REFERENCES werkopdracht_subgroepdefinities(naam);

/** ALTER PROCEDURES, FUNCTIONS & VIEWS DEPENDANT ON locatie TABLE */
USE ClaraFey_DEV;
GO
IF OBJECT_ID ( 'dbo.logon', 'P' ) IS NOT NULL 
    DROP PROCEDURE dbo.logon;
GO
CREATE PROCEDURE dbo.logon	
@gebruikerid integer, @appinfo varchar(32), @pcinfo varchar(32)
WITH EXEC AS CALLER
AS
	SET NOCOUNT ON
	BEGIN
		delete from online_gebruikers where online_geb_id = @gebruikerid or online_pc = @pcinfo
		insert into online_gebruikers (online_geb_id, online_datum, online_pc, online_appinfo) values (@gebruikerid, GETDATE(), @pcinfo, @appinfo)
		update gebruiker set lastlogindatum = GETDATE() where id = @gebruikerid 
	END

GO
USE ClaraFey_DEV;
GO
IF OBJECT_ID ( 'dbo.paswoordWijzigen', 'P' ) IS NOT NULL 
    DROP PROCEDURE dbo.paswoordWijzigen;
GO
CREATE PROCEDURE dbo.paswoordWijzigen
@persNr varchar(32), @paswoord varchar(512)
WITH EXEC AS CALLER
AS
	SET NOCOUNT ON
	BEGIN
		update gebruiker set wachtwoord = @paswoord  where personeelscode = @persNr
	END

GO
USE ClaraFey_DEV
GO
    DROP VIEW dbo.vw_online_gebruikers;
GO  
CREATE VIEW dbo.vw_online_gebruikers
AS
	SELECT TOP (100) PERCENT g.personeelscode AS persneelsnr, g.voornaam AS voornaam, g.achternaam AS achternaam, g.lastlogindatum AS laatst_ingelogged,
		o_g.online_datum AS nu_online_sinds, o_g.online_pc AS pc, o_g.online_appinfo AS application
	FROM dbo.gebruiker AS g LEFT OUTER JOIN dbo.online_gebruikers AS o_g ON o_g.online_geb_id = g.id
	ORDER BY nu_online_sinds DESC

GO
USE ClaraFey_DEV
GO
    DROP VIEW dbo.vw_online_gebruikers2;
GO  
CREATE VIEW dbo.vw_online_gebruikers2
AS
	SELECT TOP (100) PERCENT g.personeelscode AS persneelsnr, g.voornaam AS voornaam, g.achternaam AS achternaam, g.lastlogindatum AS laatst_ingelogged, 
		o_g.online_datum AS nu_online_sinds, o_g.online_pc AS pc, o_g.online_appinfo AS application
	FROM dbo.gebruiker AS g INNER JOIN dbo.online_gebruikers AS o_g ON o_g.online_geb_id = g.id
	ORDER BY nu_online_sinds DESC

GO
USE ClaraFey_DEV
GO
    DROP VIEW dbo.vw_gebruikergroep;
GO  
CREATE VIEW dbo.vw_gebruikergroep
AS
	SELECT r_gg.groepgeb_groep_code, r_gg.groepgeb_geb_id, g.id, g.personeelscode, g.wachtwoord, g.lastlogindatum, g.email, g.telefoon, g.gsm, g.voornaam, g.achternaam, 
		g.subgroep, r_g.groep_code, r_g.groep_naam, g.functiebeschrijving, g.werkplaats
	FROM dbo.rechten_groepengebruikers AS r_gg
	INNER JOIN dbo.gebruiker AS g ON r_gg.groepgeb_geb_id = g.id
	INNER JOIN dbo.rechten_groep AS r_g ON r_gg.groepgeb_groep_code = r_g.groep_code AND r_g.groep_orbis = 'Y'