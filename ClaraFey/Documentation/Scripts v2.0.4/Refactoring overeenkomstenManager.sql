/** DROP cd_overeenkomst_files & vw_cd_nietgetekendeovereenkomsten TABLES */
DROP TABLE cd_overeenkomst_files;

DROP VIEW vw_cd_nietgetekendeovereenkomsten;

/** ADD TABLE Overeenkomst_status */
CREATE TABLE Overeenkomst_status
(
	id int not null identity(1,1),
	naam varchar(64) not null,
	descr varchar(max),
	active bit not null default(1),
	prioriteit int not null,

	PRIMARY KEY(id)
)

/** ADD VALUES TO Overeenkomst_status TABLE*/
INSERT INTO Overeenkomst_status (naam, descr, prioriteit) VALUES('Nieuw', null, 1);
INSERT INTO Overeenkomst_status (naam, descr, prioriteit) VALUES('Niet getekend', null, 2);
INSERT INTO Overeenkomst_status (naam, descr, prioriteit) VALUES('Getekend', null, 3);
INSERT INTO Overeenkomst_status (naam, descr, prioriteit) VALUES('Gewijzigd', null, 4);
INSERT INTO Overeenkomst_status (naam, descr, prioriteit) VALUES('Verlopen', null, 5);
INSERT INTO Overeenkomst_status (naam, descr, prioriteit) VALUES('Stopgezet', null, 6);

/** ADD VALUES TO module TABLE */
INSERT INTO module (code, descr) VALUES ('OVEREENKOMST', 'OvereenkomstenManager');

/** ADD VALUES TO labels TABLE */
INSERT INTO file_cats(code, name, descr, module) VALUES ('IDO', 'Ido', null, 'OVEREENKOMST');
INSERT INTO file_cats(code, name, descr, module) VALUES ('IDO-HANDELINGSPLAN', 'Ido-handelingsplan', null, 'OVEREENKOMST');
INSERT INTO file_cats(code, name, descr, module) VALUES ('PERSOONLIJKEHANDLEIDING', 'Persoonlijke Handleiding', null, 'OVEREENKOMST');
INSERT INTO file_cats(code, name, descr, module) VALUES ('PROTOCOL', 'Protocol', null, 'OVEREENKOMST');
INSERT INTO file_cats(code, name, descr, module) VALUES ('CHARTER', 'Charter', null, 'OVEREENKOMST');
INSERT INTO file_cats(code, name, descr, module) VALUES ('VARIA', 'Varia', null, 'OVEREENKOMST');

/** CREATE Overeenkomst_type TABLE*/
CREATE TABLE Overeenkomst_type
(
	id int not null identity(1,1),
	naam varchar(32) not null,
	descr varchar(max),
	active bit not null default(1),

	PRIMARY KEY(id),
)

/** ADD VALUES TO Overeenkomst_type TABLE */
INSERT INTO Overeenkomst_type (naam) VALUES ('IDO');
INSERT INTO Overeenkomst_type (naam) VALUES ('IDO_Handelingsplan');
INSERT INTO Overeenkomst_type (naam) VALUES ('Persoonlijke Handleiding');
INSERT INTO Overeenkomst_type (naam) VALUES ('Protocol');
INSERT INTO Overeenkomst_type (naam) VALUES ('Charter');
INSERT INTO Overeenkomst_type (naam) VALUES ('Varia');

/** RECREATE cd_overeenkomst TABLE */
DROP TABLE Cd_Overeenkomst
CREATE TABLE Cd_Overeenkomst
(
	id int identity(1,1) not null,
	bewoner_id int not null,
	type_id int not null,
	status_id int not null,
	vanDatum date not null,
	totDatum date,
	creator int not null,
	creation_date datetime not null,

	PRIMARY KEY (id),
	CONSTRAINT fk_CdOvereenkomst_bewonerid FOREIGN KEY (bewoner_id) REFERENCES bewoner(id),
	CONSTRAINT fk_CdOvereenkomst_statusid FOREIGN KEY (status_id) REFERENCES Overeenkomst_status(id),
	CONSTRAINT fk_CdOvereenkomst_typeid FOREIGN KEY (type_id) REFERENCES Overeenkomst_type(id),
	CONSTRAINT fk_CdOvereenkomst_creator FOREIGN KEY (creator) REFERENCES gebruiker(id),
	CONSTRAINT unique_CdOvereenkomst UNIQUE (bewoner_id, type_id, vanDatum)
)
CREATE INDEX idx_bewonerid ON Cd_Overeenkomst(bewoner_id);
CREATE INDEX idx_statusid ON Cd_Overeenkomst(status_id);
CREATE INDEX idx_typeid ON Cd_Overeenkomst(type_id);
CREATE INDEX idx_creator ON Cd_Overeenkomst(creator);

/** ADD overeenkomst_id COLUMN TO cd_files TABLE */
ALTER TABLE cd_files
ADD overeenkomst_id int;
ALTER TABLE cd_files
ADD CONSTRAINT fk_cdfiles_overeenkomstid FOREIGN KEY (overeenkomst_id) REFERENCES Cd_Overeenkomst(id);