﻿/** Adding of Foreign keys & indexes to speed up database queries.
 *	Dropping 3 tables: begeleiding, bestelbonartikel, bestelbonartikelcategorie
 */

--actieplanopvolging
create index idx_modifier on actieplanopvolging(modifier);
--adressen
create index idx_modifier on adressen(modifier);
create index idx_creator on adressen(creator);
--beeldvorming
create index idx_modifier on beeldvorming(modifier);
create index idx_headerid on beeldvorming(headerid);
create index idx_treeid on beeldvorming(treeid);
create index idx_bewonerid on beeldvorming(bewonerid);
--begeleiding
drop table begeleiding;
--bestelbonartikel
drop table bestelbonartikel;
--bestelbonartikelcategorie
drop table bestelbonartikelcategorie;
--bewoner_foto
create index idx_bewonerid on bewoner_foto(bewonerid);
--cd_administratieveinfo
alter table cd_administratieveinfo add constraint fk_cdAdministratieveinfo_modifier foreign key (modifier) references gebruiker(id);
create index idx_modifier on cd_administratieveinfo(modifier);
create index idx_bewonerid on cd_administratieveinfo(bewonerid);
--cd_files
create index idx_overeenkomstId on cd_files(overeenkomst_id);
--cd_lists_results
create index idx_bewonerid on cd_lists_results(bewonerid);
create index idx_headerid on cd_lists_results(headerid);
create index idx_detailid on cd_lists_results(detailid);
--gebruiker_foto
create index idx_gebruikerId on gebruiker_foto(gebruikerId);
--gebruiker_settings
alter table gebruiker_settings add constraint fk_gebruikerSettings_gebruikerId foreign key (gebruikerid) references gebruiker(id);
create index idx_gebruikerId on gebruiker_settings(gebruikerid);
--gepland_begeleidingsteam
create index idx_gebruikerId on gepland_begeleidingsteam(gebruikerid);
create index idx_bewonerid on gepland_begeleidingsteam(bewonerid);
create index idx_groepcode on gepland_begeleidingsteam(groepcode);
--help
alter table help add constraint fk_help_modifier foreign key (modifier) references gebruiker(id);
create index idx_modifier on help(modifier);
--inventaris
create index idx_merkId on inventaris(inv_mer_id);
create index idx_modifier on inventaris(inv_wijziging_geb_id);
create index idx_actief on inventaris(inv_actief);
alter table inventaris add constraint fk_inventaris_toestelId foreign key (inv_tst_id) references toestel(tst_id);
create index idx_toestelId on inventaris(inv_tst_id);
--inventariscvo
create index idx_actief on inventariscvo(Actief);
--kalender_reservatie
create index idx_modifier on kalender_reservatie(kalr_geb_id_modificator);
--kampen
create index idx_actief on kampen(aktief);
--kilometers
alter table kilometers add constraint fk_kilometers_modifier foreign key (km_modifier) references gebruiker(id);
create index idx_modifier on kilometers(km_modifier);
alter table kilometers add constraint fk_kilometers_bestuurder foreign key (km_bestuurder) references gebruiker(id);
create index idx_bestuurder on kilometers(km_bestuurder);
create index idx_closedBoekhoudId on kilometers(km_closed_boekh_user);
create index idx_closedPersdienstId on kilometers(km_closed_persdienst_user);
create index idx_closedBewonersId on kilometers(km_closed_bewoners_user);
--labels
create index idx_actief on labels(active);--leefgroep
create index idx_actief on leefgroep(actief);
--locatie
create index idx_actief on locatie(active);
--Overeenkomst_status
create index idx_actief on Overeenkomst_status(active);
--Overeenkomst_type
create index idx_actief on Overeenkomst_type(active);
--personeel_files
create index idx_creator on personeel_files(creator);
create index idx_gebruikerId on personeel_files(gebruikerid);
create index idx_category on personeel_files(category);
--rechten_groep
create index idx_actief on rechten_groep(actif);
--rechten_groepenfuncties
create index idx_groepCode on rechten_groepenfuncties(groepfunc_groep_code);
create index idx_functieCode on rechten_groepenfuncties(groepfunc_func_code);
--rechten_groepengebruikers
create index idx_groepCode on rechten_groepengebruikers(groepgeb_groep_code);
create index idx_gebruikerId on rechten_groepengebruikers(groepgeb_geb_id);
--rechten_groepenlabels
create index idx_groepCode on rechten_groepenlabels(groepcode);
create index idx_labelId on rechten_groepenlabels(labelid);
--rechten_groepenlocaties
create index idx_groepCode on rechten_groepenlocaties(groeploc_groep_code);
create index idx_dienst on rechten_groepenlocaties(groeploc_dienst);
--toelatingen
create index idx_typeId on toelatingen(typeid);
create index idx_modifier on toelatingen(modifier);
--toelatingen_types
create index idx_actief on toelatingen_types(actif);
--werkopdracht
alter table werkopdracht add constraint fk_werkopdracht_parentId foreign key (wo_parent_id) references werkopdracht(wo_id);
create index idx_parentId on werkopdracht(wo_parent_id);
alter table werkopdracht add constraint fk_werkopdracht_statusHistoriek foreign key (wo_wosh_id) references werkopdracht_status_historiek(wosh_id);
create index idx_statusHistoriek on werkopdracht(wo_wosh_id);
create index idx_prioriteit on werkopdracht(wo_wop_id);
create index idx_creator on werkopdracht(wo_geb_id_creator);
create index idx_modifier on werkopdracht(wo_geb_id_modificator);
--werkopdracht_status_historiek
create index idx_werkopdrachtId on werkopdracht_status_historiek(wosh_wo_id);
create index idx_werkopdrachtStatusId on werkopdracht_status_historiek(wosh_wos_id);
create index idx_gebruikerId on werkopdracht_status_historiek(wosh_geb_id);
--werkopdracht_type
create index idx_actief on werkopdracht_type(actif);
--kasbeheer.rekening
create index idx_actief on kasbeheer.rekening(actif);
--kasbeheer.transactieClient
create index idx_isClosed on kasbeheer.transactieClient(IsClosed);
--kasbeheer.transacties
alter table kasbeheer.transacties add constraint fk_kasbeheerTransacties_afgerekendDoor foreign key (afgerekendDoor) references gebruiker(id);
create index idx_afgerekendDoor on kasbeheer.transacties(afgerekendDoor);
create index idx_rekeningId on kasbeheer.transacties(rekeningId);
create index idx_leefgroepId on kasbeheer.transacties(leefgroepId);
create index idx_transactieTypeId on kasbeheer.transacties(transactieTypeId);
create index idx_modifier on kasbeheer.transacties(modifier);
create index idx_isAfgerekend on kasbeheer.transacties(afgerekend);
--kasbeheer.transactieType
create index idx_isClientBased on kasbeheer.transactieType(isClientBased);