﻿
delete from rechten_functie where func_code >=1000

insert into rechten_functie values ('1000','cliëntdossier',null,'cliëntdossier')
insert into rechten_functie values ('1001','cliënt dossier power user','alle rechten, alle cliënten','cliëntdossier')

insert into rechten_functie values ('1100','cd dashboard',null,'cliëntdossier')
insert into rechten_functie values ('1101','cd dashboard/wijzigingen in dagboek zien',null,'cliëntdossier')
insert into rechten_functie values ('1102','cd dashboard/wijzigingen in overeenkomsten zien',null,'cliëntdossier')
insert into rechten_functie values ('1103','cd dashboard/wijzigingen in administratieve info zien',null,'cliëntdossier')
insert into rechten_functie values ('1104','cd dashboard/wijzigingen in beeldvorming zien',null,'cliëntdossier')
insert into rechten_functie values ('1105','cd dashboard/verwijdering bijlagen zien ',null,'cliëntdossier')
insert into rechten_functie values ('1106','cd dashboard/verwijdering adressen zien',null,'cliëntdossier')
insert into rechten_functie values ('1107','cd dashboard/dashboardbericht lezen',null,'cliëntdossier')
insert into rechten_functie values ('1108','cd dashboard/dashboardbericht plaatsen',null,'cliëntdossier')
insert into rechten_functie values ('1109','cd dashboard/operations-mededeling plaatsen',null,'cliëntdossier')

insert into rechten_functie values ('1200','cd dagboek algemeen',null,'cliëntdossier')
insert into rechten_functie values ('1201','cd dagboek algemeen/lezen en schrijven','dagboek dienst / leefgroep lezen en schrijven','cliëntdossier')
insert into rechten_functie values ('1202','cd dagboek algemeen/archiveren',null,'cliëntdossier')
insert into rechten_functie values ('1220','cd dagboek cliënt',null,'cliëntdossier')
insert into rechten_functie values ('1221','cd dagboek cliënt/lezen en schrijven','dagboek cliënt lezen en schrijven','cliëntdossier')
insert into rechten_functie values ('1222','cd dagboek cliënt/archiveren',null,'cliëntdossier')


insert into rechten_functie values ('1300','cd overeenkomsten',null,'cliëntdossier')
insert into rechten_functie values ('1301','cd overeenkomsten IDO',null,'cliëntdossier')
insert into rechten_functie values ('1302','cd overeenkomsten IDO/openen',null,'cliëntdossier')
insert into rechten_functie values ('1303','cd overeenkomsten IDO/toevoegen',null,'cliëntdossier')
insert into rechten_functie values ('1304','cd overeenkomsten IDO/verwijderen',null,'cliëntdossier')

insert into rechten_functie values ('1330','cd overeenkomsten/collectieve rechten en plichten/Charter (CRP)',null,'cliëntdossier')
insert into rechten_functie values ('1331','cd overeenkomsten/CRP/openen',null,'cliëntdossier')
insert into rechten_functie values ('1332','cd overeenkomsten/CRP/toevoegen',null,'cliëntdossier')
insert into rechten_functie values ('1333','cd overeenkomsten/CRP/verwijderen',null,'cliëntdossier')

insert into rechten_functie values ('1350','cd overeenkomsten/wijziging of stopzetting IDO/protocol',null,'cliëntdossier')
insert into rechten_functie values ('1351','cd overeenkomsten/wijziging of stopzetting IDO/openen',null,'cliëntdossier')
insert into rechten_functie values ('1352','cd overeenkomsten/wijziging of stopzetting IDO/toevoegen',null,'cliëntdossier')
insert into rechten_functie values ('1353','cd overeenkomsten/wijziging of stopzetting IDO/verwijderen',null,'cliëntdossier')

insert into rechten_functie values ('1370','cd overeenkomsten/persoonlijke handleiding VBM',null,'cliëntdossier')
insert into rechten_functie values ('1371','cd overeenkomsten/persoonlijke handleiding VBM/openen',null,'cliëntdossier')
insert into rechten_functie values ('1372','cd overeenkomsten/persoonlijke handleiding VBM/toevoegen',null,'cliëntdossier')
insert into rechten_functie values ('1373','cd overeenkomsten/persoonlijke handleiding VBM/verwijderen',null,'cliëntdossier')

insert into rechten_functie values ('1390','cd protocol',null,'cliëntdossier')
insert into rechten_functie values ('1391','cd protocol/openen',null,'cliëntdossier')
insert into rechten_functie values ('1392','cd protocol/toevoegen',null,'cliëntdossier')
insert into rechten_functie values ('1393','cd protocol/verwijderen',null,'cliëntdossier')

 

insert into rechten_functie values ('1400','cd administratieve info',null,'cliëntdossier')

insert into rechten_functie values ('1410','cd adm.info/adressen',null,'cliëntdossier')
insert into rechten_functie values ('1411','cd adm.info/adres toevoegen',null,'cliëntdossier')
insert into rechten_functie values ('1412','cd adm.info/adres wijzigen',null,'cliëntdossier')
insert into rechten_functie values ('1413','cd adm.info/exporteren excell','adressenlijst cliënt exporteren naar excell (met filteropties?)','cliëntdossier')

insert into rechten_functie values ('1420','cd adm.info/bijlages',null,'cliëntdossier')
insert into rechten_functie values ('1421','cd adm.info/bijlagen openen',null,'cliëntdossier')
insert into rechten_functie values ('1422','cd adm.info/bijlagen toevoegen',null,'cliëntdossier')
insert into rechten_functie values ('1423','cd adm.info/bijlagen opslaan op harde schijf',null,'cliëntdossier')
insert into rechten_functie values ('1424','cd adm.info/bijlagen verwijderen',null,'cliëntdossier')

insert into rechten_functie values ('1425','cd adm.info/bijlagen type diagnose-verslaggeving openen',null,'cliëntdossier')
insert into rechten_functie values ('1426','cd adm.info/bijlagen type rechtbank openen',null,'cliëntdossier')
insert into rechten_functie values ('1427','cd adm.info/bijlagen type verslaggeving voorgaande opnames&trajecten openen',null,'cliëntdossier')
insert into rechten_functie values ('1428','cd adm.info/bijlagen type therapeutische verslaggeving openen',null,'cliëntdossier')

insert into rechten_functie values ('1430','cd adm.info/begeleidingsteam occf',null,'cliëntdossier')
insert into rechten_functie values ('1431','cd adm.info/begeleidingsteam occf/Begeleidingsteam aanpassen','begeleidingsteam toevoegen, bewerken of verwijderen','cliëntdossier')
insert into rechten_functie values ('1432','cd adm.info/begeleidingsteam occf/Startdatum begeleiding','Startdatum begeleiding toevoegen,  bewerken of verwijderen','cliëntdossier')


insert into rechten_functie values ('1440','cd adm.info/handicapspecifieke gegevens',null,'cliëntdossier')
insert into rechten_functie values ('1441','cd adm.info/handicapspecifieke gegevens aanpassen',null,'cliëntdossier')

insert into rechten_functie values ('1450','cd adm.info/juridische gegevens',null,'cliëntdossier')
insert into rechten_functie values ('1451','cd adm.info/juridische gegevens aanpassen',null,'cliëntdossier')

insert into rechten_functie values ('1460','cd adm.info/financiële gegevens',null,'cliëntdossier')
insert into rechten_functie values ('1461','cd adm.info/financiële gegevens aanpassen',null,'cliëntdossier')
 
insert into rechten_functie values ('1470','cd adm.info/toelatingen',null,'cliëntdossier')
insert into rechten_functie values ('1471','cd adm.info/toelatingen aanpassen',null,'cliëntdossier')

insert into rechten_functie values ('1500','cd beeldvorming',null,'cliëntdossier')
insert into rechten_functie values ('1501','cd beeldvorming/afsluiten',null,'cliëntdossier')
insert into rechten_functie values ('1502','cd beeldvorming/exporteren of printen',null,'cliëntdossier')
insert into rechten_functie values ('1503','cd beeldvorming/archiveren',null,'cliëntdossier')
insert into rechten_functie values ('1504','cd beeldvorming/opmerking toevoegen',null,'cliëntdossier')
insert into rechten_functie values ('1505','cd beeldvorming/vertrouwelijke info zien',null,'cliëntdossier')
insert into rechten_functie values ('1506','cd beeldvorming/vertrouwelijke info aanpassen','vertrouwelijke info toevoegen,  bewerken of verwijderen','cliëntdossier')

insert into rechten_functie values ('1520','cd beeldvorming/anamnese',null,'cliëntdossier')
insert into rechten_functie values ('1521','cd beeldvorming/anamnese aanpassen',null,'cliëntdossier')
insert into rechten_functie values ('1522','cd beeldvorming/anamnese afsluiten',null,'cliëntdossier')
insert into rechten_functie values ('1523','cd beeldvorming/anamnese printen',null,'cliëntdossier')

insert into rechten_functie values ('1530','cd beeldvorming/diagnostiek',null,'cliëntdossier')
insert into rechten_functie values ('1531','cd beeldvorming/diagnostiek aanpassen',null,'cliëntdossier')
insert into rechten_functie values ('1532','cd beeldvorming/diagnostiek afsluiten',null,'cliëntdossier')
insert into rechten_functie values ('1533','cd beeldvorming/diagnostiek printen',null,'cliëntdossier')

insert into rechten_functie values ('1540','cd beeldvorming/diagnostiek',null,'cliëntdossier')
insert into rechten_functie values ('1541','cd beeldvorming/diagnostiek aanpassen',null,'cliëntdossier')
insert into rechten_functie values ('1542','cd beeldvorming/diagnostiek afsluiten',null,'cliëntdossier')
insert into rechten_functie values ('1543','cd beeldvorming/diagnostiek printen',null,'cliëntdossier')

insert into rechten_functie values ('1550','cd beeldvorming/sociaal emotioneel functioneren',null,'cliëntdossier')
insert into rechten_functie values ('1551','cd beeldvorming/sociaal emotioneel functioneren aanpassen',null,'cliëntdossier')
insert into rechten_functie values ('1552','cd beeldvorming/sociaal emotioneel functioneren afsluiten',null,'cliëntdossier')
insert into rechten_functie values ('1553','cd beeldvorming/sociaal emotioneel functioneren printen',null,'cliëntdossier')

insert into rechten_functie values ('1560','cd beeldvorming/functioneringsprofiel',null,'cliëntdossier')
insert into rechten_functie values ('1561','cd beeldvorming/functioneringsprofiel aanpassen',null,'cliëntdossier')
insert into rechten_functie values ('1562','cd beeldvorming/functioneringsprofiel afsluiten',null,'cliëntdossier')
insert into rechten_functie values ('1563','cd beeldvorming/functioneringsprofiel printen',null,'cliëntdossier')

insert into rechten_functie values ('1700','cd actieplan',null,'cliëntdossier')
insert into rechten_functie values ('1701','cd actieplan afsluiten',null,'cliëntdossier')
insert into rechten_functie values ('1702','cd actieplan exporteren of printen',null,'cliëntdossier')
insert into rechten_functie values ('1703','cd actieplan archiveren','actieplan archiveren naar afgewerkte doelstellingen','cliëntdossier')

insert into rechten_functie values ('1720','cd actieplan/doelstellingen van de cliënt/openen',null,'cliëntdossier')
insert into rechten_functie values ('1721','cd actieplan/doelstellingen van de cliënt/aanpassen',null,'cliëntdossier')

insert into rechten_functie values ('1730','cd actieplan/doelstellingen van de ouders/openen',null,'cliëntdossier')
insert into rechten_functie values ('1731','cd actieplan/doelstellingen van de ouders/aanpassen',null,'cliëntdossier')


insert into rechten_functie values ('1740','cd actieplan/doelstellingen van het begeleidingsteam/openen',null,'cliëntdossier')
insert into rechten_functie values ('1741','cd actieplan/doelstellingen van het begeleidingsteam/aanpassen',null,'cliëntdossier')

insert into rechten_functie values ('1750','cd actieplan/afgewerkte doelstellingen/openen',null,'cliëntdossier')
 
insert into rechten_functie values ('1900','cd overzicht en rapportages',null,'cliëntdossier')
insert into rechten_functie values ('1901','cd overzicht en rapportages/niet getekende overeenkomsten - weigeringsregister',null,'cliëntdossier')

 


