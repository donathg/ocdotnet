/** Actieplan_actieplantypes TABLE CREATION */
create table Actieplan_actieplantypes
(
	id int not null identity(1,1),
	actieplan_id int not null,
	actieplantypes_id int not null,
	toegevoegdOp datetime not null,
	toegevoegdDoor int not null,

	primary key(id),
	constraint fk_ActieplanActieplantypes_actieplanid foreign key (actieplan_id) references actieplan(id),
	constraint fk_ActieplanActieplantypes_actieplantypesid foreign key (actieplantypes_id) references actieplantypes(id),
	constraint fk_ActieplanActieplantypes_toegevoegdDoor foreign key (toegevoegdDoor) references gebruiker(id)
)
create index idx_actieplanId on Actieplant_actieplantypes(actieplan_id);
create index idx_actieplantypesId on Actieplant_actieplantypes(actieplantypes_id);
create index idx_toegevoegdDoor on Actieplant_actieplantypes(toegevoegdDoor);

/** actieplan TABLE ADJUSTMENTS */
ALTER TABLE actieplan
DROP CONSTRAINT fk_actieplan_type;

ALTER TABLE actieplan
DROP COLUMN actieplantype;

ALTER TABLE actieplan
DROP COLUMN afgewerkt;

ALTER TABLE actieplan
ADD creator_id int not null;

ALTER TABLE actieplan
ADD CONSTRAINT fk_actieplan_creatorId FOREIGN KEY (creator_id) REFERENCES gebruiker(id);

CREATE INDEX idx_creatorId ON actieplan(creator_id);

ALTER TABLE actieplan
ADD creationDate datetime not null;

/** actiplanpvolging TABLE ADJUSTMENTS */
ALTER TABLE actieplanopvolging
ADD creator_id int not null;

ALTER TABLE actieplanopvolging
ADD creationDate datetime not null;

ALTER TABLE actieplanopvolging
ADD CONSTRAINT fk_actieplanopvolging_creatorId FOREIGN KEY (creator_id) REFERENCES gebruiker(id);

CREATE INDEX idx_creatorId ON actieplanopvolging(creator_id);

/** actieplantypes TABLE ADJUSTMENTS */
CREATE INDEX idx_actif ON actieplantypes(actif);

/** ActieplanEvaluaties_actieplantypes TABLE CREATION */
CREATE TABLE ActieplanEvaluaties_actieplantypes
(
	id int not null identity(1,1),
	actieplanEvaluatie_id int not null,
	actieplantype_id int not null,
	creator_id int not null,
	creationDate datetime not null,

	PRIMARY KEY(id),
	CONSTRAINT fk_ActieplanEvaluatiesActieplantypes_actieplanEvaluatieId FOREIGN KEY (actieplanEvaluatie_id) REFERENCES Actieplan_Evaluaties(id),
	CONSTRAINT fk_ActieplanEvaluatiesActieplantypes_actieplantypeId FOREIGN KEY (actieplantype_id) REFERENCES actieplantypes(id),
	CONSTRAINT fk_ActieplanEvaluatiesActieplantypes_creatorId FOREIGN KEY (creator_id) REFERENCES gebruiker(id)
)
CREATE INDEX idx_actieplanEvaluatieId ON ActieplanEvaluaties_actieplantypes(actieplanEvaluatie_id);
CREATE INDEX idx_actieplantypeId ON ActieplanEvaluaties_actieplantypes(actieplantype_id);
CREATE INDEX idx_creatorId ON ActieplanEvaluaties_actieplantypes(creator_id);

/** Actieplan_Evaluaties TABLE ADJUSTMENTS */
ALTER TABLE Actieplan_Evaluaties
DROP CONSTRAINT fk_ActieplanEvaluaties_typeid;

DROP INDEX idx_typeid ON Actieplan_Evaluaties;

ALTER TABLE Actieplan_Evaluaties
DROP COLUMN type_id;