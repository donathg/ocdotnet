﻿WB
alter table werkopdracht_diensten add  location bit not null default 1
alter table werkopdracht_diensten add  inventaris bit not null default 1

  alter table werkopdracht_subgroepdefinities add dienst varchar(6)
  update werkopdracht_subgroepdefinities set dienst  = 'TECHD';
 ALTER TABLE werkopdracht_subgroepdefinities
ADD FOREIGN KEY (dienst) REFERENCES werkopdracht_diensten(wodienst_code)  
CREATE INDEX inx_werkopdracht_subgroepdefinities_dienst  ON werkopdracht_subgroepdefinities (dienst);  


create table werkopdracht_uitvoerder
(
    dienst  varchar(6) not null,
	groep_code nvarchar(12) not null
)

ALTER TABLE werkopdracht_uitvoerder
ADD FOREIGN KEY (dienst) REFERENCES werkopdracht_diensten(wodienst_code)  
CREATE INDEX inx_werkopdracht_uitvoerder_dienst  ON werkopdracht_subgroepdefinities (dienst);  


ALTER TABLE werkopdracht_uitvoerder
ADD FOREIGN KEY (groep_code) REFERENCES rechten_groep(groep_code)  
CREATE INDEX inx_werkopdracht_uitvoerder_groep_code  ON werkopdracht_uitvoerder (groep_code);  



insert into  werkopdracht_uitvoerder values ('IT','KKINF')
insert into  werkopdracht_uitvoerder values ('TECHD','TECHD')
insert into  werkopdracht_uitvoerder values ('TECHD','WASSE')


create table werkopdracht_supervisor
(
    dienst  varchar(6) not null,
	gebruikerId integer not null
)


ALTER TABLE werkopdracht_supervisor
ADD FOREIGN KEY (dienst) REFERENCES werkopdracht_diensten(wodienst_code)  
CREATE INDEX inx_werkopdracht_supervisor_dienst  ON werkopdracht_supervisor (dienst);  

ALTER TABLE werkopdracht_supervisor
ADD FOREIGN KEY (gebruikerId) REFERENCES gebruiker(id)  
CREATE INDEX inx_werkopdracht_supervisor_gebruiker  ON werkopdracht_supervisor (gebruikerId);  


insert into werkopdracht_supervisor
SELECT 'IT',id FROM vw_gebruikergroep,werkopdracht_uitvoerder WHERE 
  groepgeb_groep_code = werkopdracht_uitvoerder.groep_code and werkopdracht_uitvoerder.dienst = 'IT'


  insert into werkopdracht_supervisor
SELECT 'TECHD',id FROM  gebruiker where gebruiker.personeelscode in ('20190','03043','20231','20038','19557','02767','01165','20378','03097')


ALTER PROCEDURE [dbo].[logon]	
@gebruikerid integer, @appinfo varchar(32), @pcinfo varchar(32) ,@o_supervisorWBDiensten VARCHAR(100) OUTPUT,
@o_excecuterWBDiensten VARCHAR(100) OUTPUT

WITH EXEC AS CALLER
AS
	SET NOCOUNT ON
	BEGIN
		delete from online_gebruikers where online_geb_id = @gebruikerid or online_pc = @pcinfo
		insert into online_gebruikers (online_geb_id, online_datum, online_pc, online_appinfo) values (@gebruikerid, GETDATE(), @pcinfo, @appinfo)
		update gebruiker set lastlogindatum = GETDATE() where id = @gebruikerid 


 
	   select @o_supervisorWBDiensten = COALESCE(@o_supervisorWBDiensten + ',', '') + dienst  from werkopdracht_supervisor where werkopdracht_supervisor.gebruikerid =  @gebruikerId
	--   select @o_WBDiensten = COALESCE(@o_WBDiensten + ',', '') + wodienst_code  from werkopdracht_diensten 
	   	    
 
  SELECT @o_excecuterWBDiensten = COALESCE(@o_excecuterWBDiensten + ',', '') + dienst  FROM vw_gebruikergroep,werkopdracht_uitvoerder WHERE 
  groepgeb_groep_code = werkopdracht_uitvoerder.groep_code and vw_gebruikergroep.groepgeb_geb_id = @gebruikerId
 

 
	END