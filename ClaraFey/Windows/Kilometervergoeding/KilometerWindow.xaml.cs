﻿using ClaraFey.BLL;
using ClaraFey.CommonObjects;
using ClaraFey.ViewControllers;
using ClaraFey.ViewControllers.Kilometervergoeding;
using ClaraFey.Windows.Common;
using DevExpress.Xpf.Bars;
using DevExpress.Xpf.Grid;
using DevExpress.XtraReports.UI;
using sharedcode.BLL;
using sharedcode.common;
using sharedcode.WeergaveClasses;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;

using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ClaraFey.Windows.Kilometervergoeding
{
    /// <summary>
    /// Interaction logic for KilometerWindow.xaml
    /// </summary>
    public partial class KilometerWindow : Window
    {
        /** FIELDS */
        private readonly KilometerVergoedingVC _vc;

        /** CONSTRUCTORS */
        public KilometerWindow(KilometerModeType mode)
        {
            _vc = new KilometerVergoedingVC(mode);
            InitializeComponent();
            GlobalMethods.SetTheme(this, GlobalMethods.Themes.clientdossier);
            this.Loaded += KilometerWindow_Loaded;
            this.Closed += KilometerWindow_Closed;
            this.DataContext = _vc;
        }

        /** EVENTS */
        private void KilometerWindow_Closed(object sender, EventArgs e)
        {
            switch (_vc.Settings.Mode)
            {
                case KilometerModeType.ingave:
                    WpfLayoutBLL.SaveLayoutGrid(gridKM, "KilometerModeType.ingave");
                    break;
                case KilometerModeType.boekhouding:
                    WpfLayoutBLL.SaveLayoutGrid(gridKM, "KilometerModeType.boekhouding");
                    break;
                case KilometerModeType.bewoners:
                    WpfLayoutBLL.SaveLayoutGrid(gridKM, "KilometerModeType.bewoners");
                    break;
                case KilometerModeType.personeelsdienst:
                    WpfLayoutBLL.SaveLayoutGrid(gridKM, "KilometerModeType.personeelsdienst");
                    break;
                case KilometerModeType.overzicht:
                    WpfLayoutBLL.SaveLayoutGrid(gridKM, "KilometerModeType.overzicht");
                    break;
                default:
                    break;
            }
        }
        private void KilometerWindow_Loaded(object sender, RoutedEventArgs e)
        {
            _vc.LoadKilometers();
            switch (_vc.Settings.Mode)
            {
                case KilometerModeType.ingave:
                    ((GridViewBase)gridKM.View).ShowGroupPanel = false;
                    ((GridViewBase)gridKM.View).AllowFilterEditor = DevExpress.Utils.DefaultBoolean.False;
                    ((GridViewBase)gridKM.View).HideSearchPanel();
                    ((GridViewBase)gridKM.View).AllowGrouping = false;
                    ((GridViewBase)gridKM.View).AllowColumnFiltering = false;
                    WpfLayoutBLL.LoadLayoutGrid(gridKM, "KilometerModeType.ingave");
                    break;
                case KilometerModeType.boekhouding:
                    ((GridViewBase)gridKM.View).ShowGroupPanel = false;
                    ((GridViewBase)gridKM.View).AllowFilterEditor = DevExpress.Utils.DefaultBoolean.False;
                    ((GridViewBase)gridKM.View).HideSearchPanel();
                    ((GridViewBase)gridKM.View).AllowGrouping = false;
                    ((GridViewBase)gridKM.View).AllowColumnFiltering = false;
                    WpfLayoutBLL.LoadLayoutGrid(gridKM, "KilometerModeType.boekhouding");
                    break;
                case KilometerModeType.bewoners:
                    ((GridViewBase)gridKM.View).ShowGroupPanel = false;
                    ((GridViewBase)gridKM.View).AllowFilterEditor = DevExpress.Utils.DefaultBoolean.False;
                    ((GridViewBase)gridKM.View).HideSearchPanel();
                    ((GridViewBase)gridKM.View).AllowGrouping = false;
                    ((GridViewBase)gridKM.View).AllowColumnFiltering = false;
                    WpfLayoutBLL.LoadLayoutGrid(gridKM, "KilometerModeType.bewoners");
                    break;
                case KilometerModeType.personeelsdienst:
                    ((GridViewBase)gridKM.View).ShowGroupPanel = false;
                    ((GridViewBase)gridKM.View).AllowFilterEditor = DevExpress.Utils.DefaultBoolean.False;
                    ((GridViewBase)gridKM.View).HideSearchPanel();
                    ((GridViewBase)gridKM.View).AllowGrouping = false;
                    ((GridViewBase)gridKM.View).AllowColumnFiltering = false;
                    WpfLayoutBLL.LoadLayoutGrid(gridKM, "KilometerModeType.personeelsdienst");
                    break;
                case KilometerModeType.overzicht:
                    ((GridViewBase)gridKM.View).AllowFilterEditor = DevExpress.Utils.DefaultBoolean.False;
                    ((GridViewBase)gridKM.View).ShowSearchPanel(true);
                    ((GridViewBase)gridKM.View).AllowColumnFiltering = true;
                    ((GridViewBase)gridKM.View).AllowGrouping = true;
                    ((GridViewBase)gridKM.View).ShowGroupPanel = true;
                    WpfLayoutBLL.LoadLayoutGrid(gridKM, "KilometerModeType.overzicht");
                    break;
                default:
                    break;
            }
        }
        private void gridKM_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            OpenSaveWindow();
        }
        private void barButtonNew_ItemClick(object sender, ItemClickEventArgs e)
        {
            AddKm();
        }
        private void barButtonTarieven_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (sender is BarButtonItem i)
                ShowTarieven(i.Tag.ToString());
        }
        private void btnFilterSearch2_ItemClick(object sender, ItemClickEventArgs e)
        {
            try
            {
                _vc.LoadKilometers();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Fout", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void barButtonClose_ItemClick(object sender, ItemClickEventArgs e)
        {
            this.Close();
        }
        private void barButtonCloseSelection_ItemClick(object sender, ItemClickEventArgs e)
        {
            try
            {
                AflsluitenReportWindow w = new AflsluitenReportWindow(_vc.FilterDateFrom, _vc.FilterDateTo)
                {
                    WindowStartupLocation = WindowStartupLocation.CenterScreen
                };
                w.ShowDialog();

                if (!string.IsNullOrWhiteSpace(w.Result))
                    PrintReport(w.DateFrom, w.DateTo, w.Result == "AFSLUITEN");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Fout", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void biShowHelp_ItemClick(object sender, ItemClickEventArgs e)
        {
            HelpWindow w = new HelpWindow("kilometervergoeding", "Help kilometervergoeding");
            w.Show();
        }
        private void barButtonExportToExel_ItemClick(object sender, ItemClickEventArgs e)
        {
            GlobalMethods.ExportGridToExcel(gridKM, "kilometervergoeding_" + _vc.FilterDateFrom.ToString("dd-MM-yyyy") + "_" + _vc.FilterDateTo.ToString("dd-MM-yyyy"));
        }
        private void miDelete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (_vc != null && _vc.SelectedKMItem != null)
                {
                    string messageboxText = $"Zeker dat u deze lijn wil verwijderen ? {_vc.SelectedKMItem.Bestuurder.Name} {_vc.SelectedKMItem.Kilometers.GetValueOrDefault().ToString("#.##")} km";
                    if (MessageBox.Show(messageboxText, "Verwijderen", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                    {
                        _vc.DeleteSelectedItem();
                        _vc.LoadKilometers();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Fout", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void miEdit_Click(object sender, RoutedEventArgs e)
        {
            OpenSaveWindow();
        }
        private void TableView_CellValueChanging(object sender, CellValueChangedEventArgs e)
        {
            if (e.Column.FieldName == "IsCheckedByBewonersAdministratie")
            {

                KilometerItem row = (KilometerItem)e.Row;
                try
                {
                    this.Cursor = Cursors.Wait;
                    var rowHandle = e.RowHandle;
                  
                    bool value = (bool)e.Value;
                    if (value)
                        _vc.MarkAsNagekeken(row.Id.GetValueOrDefault());
                    else
                        _vc.MarkAsNietNagekeken(row.Id.GetValueOrDefault());
                    row.IsCheckedByBewonersAdministratie = true;



                }
                catch (Exception ex)
                {
                    
                    row.IsCheckedByBewonersAdministratie = (bool) e.OldValue;
                  
                    e.Handled = true;
                    MessageBox.Show(ex.Message + ex.Message, "Fout", MessageBoxButton.OK, MessageBoxImage.Error);
                    
                }
                finally
                {
                    this.Cursor = Cursors.Arrow;
                }

            }
        }

        /** METHODS */
        private void AddKm()
        {
            KilometerItem item = new KilometerItem();
            SaveKilometerWindow w = new SaveKilometerWindow(item, SaveKilometerWindowChangeModes.ChangeAll)
            {
                WindowStartupLocation = WindowStartupLocation.CenterScreen
            };
            if (w.ShowDialog().GetValueOrDefault() == true)
            {

                _vc.AddNewKilometer(w.Result);
            }
        }
        private void OpenSaveWindow()
        {
            
            


            if (_vc.CanModifyKilometers == false)
                return;

            SaveKilometerWindowChangeModes changeMode = SaveKilometerWindowChangeModes.ChangeAll;
            if (_vc.Settings.Mode != KilometerModeType.ingave)
                changeMode = SaveKilometerWindowChangeModes.ChangeType;


            KilometerItem kmItem = _vc.SelectedKMItem;

            SaveKilometerWindow w = new SaveKilometerWindow(kmItem, changeMode);

            if (w.ShowDialog().GetValueOrDefault())
            {
                GlobalSharedMethods.CopyPublicProperties(w.Result, _vc.SelectedKMItem);
                _vc.SelectedKMItem.Bewoners = GlobalSharedMethods.DeepClone<List<bewoner>>(w.Result.Bewoners);
                _vc.SelectedKMItem.Leefgroepen = GlobalSharedMethods.DeepClone<List<leefgroep>>(w.Result.Leefgroepen);
                _vc.SelectedKMItem.Wagen = GlobalSharedMethods.DeepClone<KilometersWagen>(w.Result.Wagen);
                _vc.SelectedKMItem.Type = GlobalSharedMethods.DeepClone<KilometersType>(w.Result.Type);
                _vc.NotifyModifiedKmItem();
            }
        }
        private void ShowTarieven(string priceType)
        {
            try
            {
                string titel = "tarieven voor eigen wagen";
                if (priceType == "BIKE")
                    titel = "tarieven voor eigen fiets";
                if (priceType == "BUS")
                    titel = "tarieven voor bussen";

                recAdorner.Visibility = Visibility.Visible;
                List<SMMWindow2ComboItem> list = new List<SMMWindow2ComboItem>();
                SMMWindow2Settings settings = new SMMWindow2Settings(titel, GlobalMethods.Themes.normal);
                SMMWindow2Level level0 = new SMMWindow2Level(0, titel)
                {
                    AllowDelete = false,
                    AllowModify = _vc.CanModifyPrices,
                    AllowAdd = _vc.CanModifyPrices,
                    AllowSearch = _vc.CanModifyPrices,
                    AllowFilter = _vc.CanModifyPrices
                };
                level0.AddColumns(
                    new SMMWindow2Column() { FieldName = "kmp_id", Visible = false },
                    new SMMWindow2Column() { ValidateMethod = new SMMWindow2Column.ValidateDelegate(ValidateTariefBegindatum), DefaultValue = DateTime.Now.Date, FieldName = "kmp_datefrom",
                        ColumnHeader = "begindatum", IsEditable = true },
                    new SMMWindow2Column() { ValidateMethod = new SMMWindow2Column.ValidateDelegate(ValidateTariefEinddatum), FieldName = "kmp_dateto", DefaultValue = DateTime.Now.Date,
                        ColumnHeader = "einddatum", IsEditable = true },
                    new SMMWindow2Column() { FieldName = "kmp_pricekm", ColumnHeader = "bedrag/km", IsEditable = true },
                    new SMMWindow2Column() { FieldName = "kmp_priceomnium", ColumnHeader = "bedrag omnium", IsEditable = true },
                    new SMMWindow2Column() { FieldName = "kmp_type", Visible = false, IsEditable = false, DefaultValue = priceType/*eigen wagen of eigen fiets*/ }
                );

                smmWindowVC vc = new smmWindowVC();
                var someDbSet = vc.Entity.Set<kilometer_prices>().SqlQuery("select * from kilometer_prices where kmp_type='" + priceType + "'").ToList();

                level0.DataSource = new ObservableCollection<kilometer_prices>(someDbSet.ToList());
                settings.AddLevel(level0);

                SMMWindow2 w = new SMMWindow2(vc, settings)
                {
                    WindowStartupLocation = WindowStartupLocation.CenterScreen
                };
                w.ShowDialog();
            }
            finally
            {
                recAdorner.Visibility = Visibility.Hidden;
            }
        }
        public string ValidateTariefBegindatum(SMMWindow2Column col, object value, object vc)
        {
            if (value == null)
                return "Gelieve een datum in te geven";

            DateTime startdate = (DateTime)value;

            if (vc is smmWindowVC smmVc)
            {
                kilometer_prices p = (kilometer_prices)smmVc.SelectedItem[0];
                p.kmp_dateto = startdate;
            }

            return null;
        }
        public string ValidateTariefEinddatum(SMMWindow2Column col, object value, object vc)
        {
            if (value == null)
                return "Gelieve een datum in te geven";

            DateTime enddate = (DateTime)value;

            if (vc is smmWindowVC smmVc)
            {
                kilometer_prices p = (kilometer_prices)smmVc.SelectedItem[0];

                if (enddate < p.kmp_datefrom)
                    return "Einddatum moet groter zijn dan begindatum";
            }
            return null;
        }
        private void PrintReport(DateTime dateFrom, DateTime dateTo, bool afsluiten)
        {
            try
            {
                this.Cursor = Cursors.Wait;

                string messageboxText = $"Zeker dat u de periode {dateFrom.ToString("dd/MM/yyyy")} - {dateTo.ToString("dd/MM/yyyy")} wil aflsuiten ?";
                if (!afsluiten)
                    _vc.PrintReport(dateFrom, dateTo, false);
                else if (afsluiten && MessageBox.Show(messageboxText, "Afsluiten", MessageBoxButton.OKCancel, MessageBoxImage.Question) == MessageBoxResult.OK)
                {
                    _vc.PrintReport(dateFrom, dateTo, afsluiten);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + ex.Message, "Fout", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            finally
            {
                this.Cursor = Cursors.Arrow;
            }
        
        }
    }
    #region convertors
    public class NoKilometersColorConverter : MarkupExtension, IValueConverter
    {
        public static bool ignoreConvertor = false;
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
            {


                return Brushes.Red;

            }
            return Brushes.White;

        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            // TODO Not used yet
            return null;
        }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }
    }
    public class NoKilometersForeColorConverter : MarkupExtension, IValueConverter
    {       
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {


            if (value == null)
            {

 
                    return Brushes.White;

            }
            return Brushes.Black;

        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            // TODO Not used yet
            return null;
        }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }
    }

    public class myDetailVisibleConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            // Obtaining the value to be converted 
            string categoryValue = (string)value;

            // Specifying values for which to show expand buttons
            string[] categories = new string[] { "First", "Third" };
            if (categories.Contains(categoryValue))
                return true;

            // Disable expand button if the value isn't in the list
            return false;
        }
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
    #endregion
}
