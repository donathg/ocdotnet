﻿using ClaraFey.BLL;
using ClaraFey.CommonObjects;
using ClaraFey.ViewControllers;
using ClaraFey.ViewControllers.Kilometervergoeding;
using ClaraFey.Windows.Common;
using sharedcode.BLL;
using sharedcode.common;
using sharedcode.WeergaveClasses;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ClaraFey.Windows.Kilometervergoeding
{

    public enum SaveKilometerWindowChangeModes
    {
        ChangeAll,
        ChangeType
    }
    /// <summary>
    /// Interaction logic for SaveKilometerWindow.xaml
    /// </summary>
    public partial class SaveKilometerWindow : Window
    {
        SaveKilometerWindowChangeModes _changeMode;
        /** FIELDS */
        KilometerSaveVC _vc = null;
        public KilometerItem Result { get { return _vc.KMItem; } }
        /** CONSTRUCTORS */
        public SaveKilometerWindow(KilometerItem kmItem, SaveKilometerWindowChangeModes changeMode)
        {    
            _vc = new KilometerSaveVC(kmItem,  changeMode);
            this._changeMode = changeMode;
            InitializeComponent();
            GlobalMethods.SetTheme(this, GlobalMethods.Themes.clientdossier);
            CultureInfo culture = new CultureInfo("nl-NL");
            this.DataContext = _vc;
            this.Loaded += SaveKilometerWindow_Loaded;
            this.Closed += SaveKilometerWindow_Closed;
        }
        /** EVENTS */
        private void SaveKilometerWindow_Closed(object sender, EventArgs e)
        {
            listType.SelectionChanged -= listType_SelectionChanged;
        }
        private void SaveKilometerWindow_Loaded(object sender, RoutedEventArgs e)
        {
            listType.SelectionChanged += listType_SelectionChanged;
            if (_vc.KMItem.Id.GetValueOrDefault()==0)
            _vc.ChangeBestuurder(GlobalData.Instance.LoggedOnUser.Id.GetValueOrDefault(), GlobalData.Instance.LoggedOnUser.FirstName, GlobalData.Instance.LoggedOnUser.LastName);
        }
        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            this.Close();
        }
        private void btnOk_Click(object sender, RoutedEventArgs e)
        {
            string errorMessage = _vc.Save();
            if (string.IsNullOrWhiteSpace(errorMessage))
            {
                this.DialogResult = true;
                this.Close();
            }
            else
                MessageBox.Show(errorMessage, "Fout", MessageBoxButton.OK, MessageBoxImage.Error);
        }
        private void listType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            _vc.NotifyChangeType();
        }
        
        private void btnAddBewoner_Click(object sender, RoutedEventArgs e)
        {
            AddBewoner();
        }
        private void NamelookUpEdit_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter || e.Key== Key.Tab)
                AddBewoner();
        }
        private void listBoxSelectedBewoners_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Delete)
                DeleteBewoner();
        }
        private void btnDeleteBewoner_Click(object sender, RoutedEventArgs e)
        {
            DeleteBewoner();
        }
        private void miDeleteBewoner_Click(object sender, RoutedEventArgs e)
        {
            DeleteBewoner();
        }
        private void LeefgroepLookUpEdit_KeyDown(object sender, KeyEventArgs e)
        {
            AddLeefgroep();
        }
        private void btnAddLeefgroep_Click(object sender, RoutedEventArgs e)
        {
            AddLeefgroep();
        }
        private void listBoxSelectedLeefgroepen_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Delete)
            {
                DeleteLeefgroep();
            }
        }
        private void miDeleteLeefgroep_Click(object sender, RoutedEventArgs e)
        {
            DeleteLeefgroep();
        }
        private void btnDeleteLeefgroep_Click(object sender, RoutedEventArgs e)
        {
            DeleteLeefgroep();
        }
        private void btnChangeBestuurder_Click(object sender, RoutedEventArgs e)
        {
            SMMWindow2Settings settings = new SMMWindow2Settings("Kies bestuurder", GlobalMethods.Themes.normal) { IsChoiceWindow = true };
            SMMWindow2Level level0 = new SMMWindow2Level(0, "Overzicht medewerkers") { AllowDelete = false, AllowModify = false, AllowAdd = false, AllowSearch = true, AllowFilter = true };
            level0.AddColumns(
                new SMMWindow2Column() { FieldName = "personeelscode", ColumnHeader = "Personeelsnr.", IsEditable = false },
                new SMMWindow2Column() { FieldName = "voornaam", ColumnHeader = "voornaam", IsEditable = true },
                new SMMWindow2Column() { FieldName = "achternaam", ColumnHeader = "achternaam", IsEditable = true },
                new SMMWindow2Column() { FieldName = "werkplaats", ColumnHeader = "werkplaats", IsEditable = true, IsNullable = true }
            );
            smmWindowVC vc = new smmWindowVC();
            List<gebruiker> someDbSet = vc.Entity.Set<gebruiker>().SqlQuery(@"select * from gebruiker").ToList();
            level0.DataSource = new ObservableCollection<gebruiker>(someDbSet);
            settings.AddLevel(level0);
            SMMWindow2 w = new SMMWindow2(vc, settings);
            if (w.ShowDialog().GetValueOrDefault())
            {
                gebruiker g = (gebruiker)vc.SelectedItem0;
                _vc.ChangeBestuurder(g.id, g.voornaam, g.achternaam);
            }
        }
        private void listBoxSelectedKampen_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Delete)
            {
                DeleteKamp();
            }
        }
        private void miDeleteKamp_Click(object sender, RoutedEventArgs e)
        {
            DeleteKamp();
        }
        private void btnDeleteKampen_Click(object sender, RoutedEventArgs e)
        {
            DeleteKamp();
        }
        private void KampenLookUpEdit_KeyDown(object sender, KeyEventArgs e)
        {
            AddKampen();
        }
        private void btnAddKampen_Click(object sender, RoutedEventArgs e)
        {
            AddKampen();
        }
        private void miDeleteKampen_Click(object sender, RoutedEventArgs e)
        {
            DeleteKamp();
        }
        /** METHODS */
        private void AddBewoner()
        {
            if (NamelookUpEdit.SelectedItem is bewoner b)
            {
                _vc.AddBewoner(b);
                NamelookUpEdit.SelectedItem = null;
            }
        }
        private void AddLeefgroep()
        {
            if (LeefgroepLookUpEdit.SelectedItem is leefgroep lfg)
            {
                _vc.AddLeefgroep(lfg);
                LeefgroepLookUpEdit.SelectedItem = null;
            }
        }
        private void AddKampen()
        {
            if (KampenLookUpEdit.SelectedItem is kampen lfg)
            {
                _vc.AddKampen(lfg);
                KampenLookUpEdit.SelectedItem = null;
            }
        }
        private void DeleteBewoner()
        {
            if (listBoxSelectedBewoners.SelectedItem is bewoner b)
                _vc.DeleteBewoner(b.id);
        }
        private void DeleteLeefgroep()
        {
            if (listBoxSelectedLeefgroepen.SelectedItem is leefgroep b)
                _vc.DeleteLeefgroep(b.id);
        }
        private void DeleteKamp()
        {
            if (listBoxSelectedKampen.SelectedItem is kampen k)
                _vc.DeleteKamp(k.id);
        }

        private void WagenkUpEdit_PopupClosed(object sender, DevExpress.Xpf.Editors.ClosePopupEventArgs e)
        {
            _vc.NotifyChangeWagen();
        }

        private void WagenlookUpEdit_KeyDown(object sender, KeyEventArgs e)
        {
        }
    }
}
