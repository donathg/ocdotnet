﻿using ClaraFey.CommonObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ClaraFey.Windows.Kilometervergoeding
{
    /// <summary>
    /// Interaction logic for AflsluitenReportWindow.xaml
    /// </summary>
    public partial class AflsluitenReportWindow : Window
    {
        public string Result { get; set; }
        public DateTime DateFrom {get;set;}
        public DateTime DateTo { get; set; }
        public AflsluitenReportWindow(DateTime DateFrom, DateTime DateTo)
        {
            InitializeComponent();
            GlobalMethods.SetTheme(this, GlobalMethods.Themes.clientdossier);
            this.DateFrom = DateFrom;
            this.DateTo = DateTo;
            this.DataContext = this;
        }

        private void btnTest_Click(object sender, RoutedEventArgs e)
        {
            Result = "TEST";
            Close();
        }

        private void btnAfsluiten_Click(object sender, RoutedEventArgs e)
        {
            Result = "AFSLUITEN";
            this.Close();
        }
    }
}
