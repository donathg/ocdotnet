﻿using ClaraFey.BLL;
using ClaraFey.ViewControllers.Werkbonnenbeheer;
using sharedcode.BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ClaraFey.Windows.Werkbonnenbeheer
{
    /// <summary>
    /// Interaction logic for WerkopdrachtChangeStatusWindow.xaml
    /// </summary>
    public partial class WerkopdrachtChangeStatusWindow : Window
    {
        SaveWerkopdrachtStatusViewController _vc = null;
        public WerkopdrachtChangeStatusWindow(WerkopdrachtItem werkOpdrachtItem)
        {
            _vc = new SaveWerkopdrachtStatusViewController(werkOpdrachtItem);
           this.DataContext = _vc;
            InitializeComponent();
            if (_vc.WerkOpdrachtItem.WerkopdrachtType == WerkopdrachtType.Werkaanvraag)
                this.Title = String.Format ("Status wijzigen - Werkaanvraag {0}  : {1}", _vc.WerkOpdrachtItem.Id,_vc.WerkOpdrachtItem.Titel);
            else
                this.Title = String.Format("Status wijzigen - Werkbon {0}  : {1}", _vc.WerkOpdrachtItem.Id, _vc.WerkOpdrachtItem.Titel);

            this.Closed += WerkopdrachtChangeStatusWindow_Closed;
        }

        void WerkopdrachtChangeStatusWindow_Closed(object sender, EventArgs e)
        {
            _vc.CleanUp();
            _vc = null;
        }
        private void ButtonAnnuleren_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            this.Close();
        }

        private void ButtonOpslaan_Click(object sender, RoutedEventArgs e)
        {
            if (_vc.StatusId == 8)
            {
                if (String.IsNullOrWhiteSpace(_vc.Opmerking))
                {
                    MessageBox.Show("Gelieve een opmerking of reden in te geven.", "Fout", MessageBoxButton.OKCancel, MessageBoxImage.Error);
                    return;
                }
            }
            this.DialogResult = true;

            this.Close();
        }
         
    }
}
