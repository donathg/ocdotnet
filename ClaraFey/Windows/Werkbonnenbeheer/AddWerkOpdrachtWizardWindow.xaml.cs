﻿using ClaraFey.BLL;
using ClaraFey.CommonObjects;
using ClaraFey.ViewControllers;
using ClaraFey.ViewControllers.Werkbonnenbeheer;
using DevExpress.Xpf.Grid;
using sharedcode.BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ClaraFey.Windows.Werkbonnenbeheer
{
    /// <summary>
    /// Interaction logic for AddWerkaanvraag.xaml
    /// </summary>
    public partial class AddWerkOpdrachtWizardWindow : Window 
    {
        SaveWerkOpdrachtViewController _vc;

        public AddWerkOpdrachtWizardWindow(WerkopdrachtItem werkOpdrachItem, WerkopdrachtUserType userType)
        {
            //wizard kan alleen in normale gebruikers modus
            this._vc = new SaveWerkOpdrachtViewController(werkOpdrachItem, userType);
            this.DataContext = _vc;

            InitializeComponent();

            // binding van events aan methodes. Nodig: direct in xaml => geeft problemen
            tabWizard.SelectionChanged +=tabWizard_SelectionChanged;
            LocatieBoom.SelectedItemChanged +=LocatieBoom_SelectedItemChanged;

            //Vanwege het lege record bovenaan , sorteren en filteren uitschakelen
            gridInventaris.View.AllowColumnFiltering = false;
            gridInventaris.View.AllowFilterEditor = DevExpress.Utils.DefaultBoolean.False;
            gridInventaris.View.AllowSorting = false;
            gridInventaris.View.AllowEditing = false;
            
            this.Closed += AddWerkOpdrachtWizardWindow_Closed;
            
            ((GridViewBase)gridInventaris.View).ShowGroupPanel = false;
        }

        void AddWerkOpdrachtWizardWindow_Closed(object sender, EventArgs e)
        {
            LocatieBoom.SelectedItemChanged -= LocatieBoom_SelectedItemChanged;
            tabWizard.SelectionChanged -= tabWizard_SelectionChanged;
            _vc.CleanUp();
            _vc = null;
        }

        private void LocatieBoom_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            Location location = e.NewValue as Location;
            if (_vc != null)
            {
                if (location != null)
                {
                    _vc.SelectedLocatieItem = location;
                }
            }
        }
        private void tabWizard_SelectionChanged(object sender, DevExpress.Xpf.Core.TabControlSelectionChangedEventArgs e)
        {
            if (tabWizard.SelectedIndex == 5)
            {
                btnNext.Content = "Werkaanvraag indienen";
            }
            else
            {
                btnNext.Content = "volgende stap";
            }

            if (tabWizard.SelectedIndex == 0)
            {
                btnPrev.Content = "Annuleren";
            }
            else
            {
                btnPrev.Content = "vorige stap";
            }
        }

        private void btnNext_Click(object sender, RoutedEventArgs e)
        {
            if (tabWizard.SelectedIndex != 5)
                tabWizard.SelectNext();
            else
            {
                LocatieBoom.SelectedItemChanged -= LocatieBoom_SelectedItemChanged;
                this.DialogResult = true;
            }
        }

        private void btnPrev_Click(object sender, RoutedEventArgs e)
        {
            if (tabWizard.SelectedIndex != 0)
                tabWizard.SelectPrev();
            else this.DialogResult = false; //annuleren
        }
    }
}
