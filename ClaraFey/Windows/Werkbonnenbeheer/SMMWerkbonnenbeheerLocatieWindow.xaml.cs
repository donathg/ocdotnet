﻿using ClaraFey.ViewControllers;
using ClaraFey.ViewControllers.Werkbonnenbeheer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ClaraFey.Windows.Werkbonnenbeheer
{
    /// <summary>
    /// Interaction logic for SMMWerkbonnenbeheerLocatieWindow.xaml
    /// </summary>
    public partial class SMMWerkbonnenbeheerLocatieWindow : Window
    {
        private SMMWerkbonnenBeheerLocatieVC _vc = new SMMWerkbonnenBeheerLocatieVC();
        public SMMWerkbonnenbeheerLocatieWindow()
        {
            this.DataContext = _vc;
            InitializeComponent();
            this.Closed += SMMWerkbonnenbeheerLocatieWindow_Closed; ;
        }

        private void SMMWerkbonnenbeheerLocatieWindow_Closed(object sender, EventArgs e)
        {

            _vc.CleanUp();
            _vc = null;

        }
    }
}
