﻿using ClaraFey.BLL;
using sharedcode.BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ClaraFey.Windows.Werkbonnenbeheer
{
    /// <summary>
    /// Interaction logic for StatusGeschiedenisOverzicht.xaml
    /// </summary>
    public partial class StatusGeschiedenisOverzicht : Window
    {
        int _id;
        String _title;
        public StatusGeschiedenisOverzicht(int id, String titel)
        {
            this._id = id;
            this._title = titel;
            InitializeComponent();
            this.Title = "Status historiek";
            this.Loaded += StatusGeschiedenisOverzicht_Loaded;
        }

        private void StatusGeschiedenisOverzicht_Loaded(object sender, RoutedEventArgs e)
        {
          List<StatusHistoriekItem> list =  WerkbonnenBeheerBLL.GetStatusHistoriek(this._id);
          StringBuilder sb = new StringBuilder("<h2>" + _title + "</h2>");
          foreach (StatusHistoriekItem i  in list)
          {
                sb.Append("<table  style='font-family: Trebuchet MS, Helvetica, sans-serif'>");
                sb.Append("<tr><td>Status : </td>");
                sb.Append("<td>" + i.Status + "</td></tr>");
                sb.Append("<tr><td>Gewijzigd : </td>");
                sb.Append("<td>" + i.Modifier + ", " + i.Datum + "</td></tr>");
                sb.Append("<tr><td valign='top'>Opmerking : </td>");
                sb.Append("<td>" + (String.IsNullOrWhiteSpace (i.Opmerking) ? "-" : i.Opmerking) + "</td></tr>");
                sb.Append("</table>");
                sb.Append("<hr>");

            }
          this.webBrowser.NavigateToString(sb.ToString());
        }
    }
}
