﻿using ClaraFey.BLL;
using ClaraFey.CommonObjects;
using ClaraFey.Reports;
using ClaraFey.ViewControllers;
using ClaraFey.ViewControllers.Common;
using ClaraFey.ViewControllers.Werkbonnenbeheer;
using ClaraFey.Windows.Common;
using DevExpress.Data;
using DevExpress.Xpf.Grid;
using DevExpress.XtraReports.UI;
using sharedcode.BLL;
using sharedcode.common;
using sharedcode.CommonObjects;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Threading;

namespace ClaraFey.Windows.Werkbonnenbeheer
{
    /// <summary>
    /// Interaction logic for MijnWerkaanvragenWindow.xaml
    /// </summary>
    public partial class WerkbonnenbeheerWindow : ClaraFeyWindow
    {
        /** FIELDS */
        private WerkbonnenbeheerViewController _vc;
        DispatcherTimer dispatcherTimer = new DispatcherTimer();
        
        /** CONSTRUCTOR */
        public WerkbonnenbeheerWindow(WerkopdrachtType view, werkopdracht_diensten dienst, WerkopdrachtUserType userType)
        {
            this.Name = "Window" + view.ToString();
            _vc = new WerkbonnenbeheerViewController(view, dienst, userType);
            this.DataContext = _vc;

            InitializeComponent();

            if (view == WerkopdrachtType.Werkaanvraag)
            {
                gridWB.View.ShowTotalSummary = false;
                gridWA.View.ShowTotalSummary = true;
                ((GridViewBase)gridWB.View).ShowGroupPanel = false;
                gridWA.View.ShowSearchPanel(true);
                this.Title = "Werkaanvragen";
                panelWA.ItemHeight = new GridLength(1.8, GridUnitType.Star);
                biGoToOtherWindow.Content = "Werkbonnen";
            }
            if (view == WerkopdrachtType.Werkbon)
            {
                gridWA.View.ShowTotalSummary = false;
                gridWB.View.ShowTotalSummary = true;
                ((GridViewBase)gridWA.View).ShowGroupPanel = false;
                gridWB.View.ShowSearchPanel(true);
                this.Title = "Werkbonnen";
                panelWB.ItemHeight = new GridLength(9, GridUnitType.Star);
                biGoToOtherWindow.Content = "Werkaanvragen";
            }
            gridWA.SelectedItemChanged += GridWA_SelectedItemChanged;
            gridWB.SelectedItemChanged += GridWB_SelectedItemChanged;
            this.Closed += WerkbonnenbeheerWindow_Closed;
            this.Loaded += WerkbonnenbeheerWindow_Loaded;
        }

        /** EVENTS */
        private void DispatcherTimer_Tick(object sender, EventArgs e)
        {
            bool isOpenStatus = GlobalMethods.IsWindowOpen<Window>("ChangeStatusWindow");
            bool isOpenEdit = GlobalMethods.IsWindowOpen<Window>("SaveWerkOpdrachtWindow");
            bool isOpenWizard = GlobalMethods.IsWindowOpen<Window>("WerkopdrachtWizard");
            if (isOpenStatus || isOpenEdit || isOpenWizard)
                return;
            else
                _vc.Refresh();
        }
        private void BtnShowHistory_Click(object sender, RoutedEventArgs e)
        {
            if (sender is ToggleButton toggleButton)
            {
                if (toggleButton.IsChecked.HasValue && toggleButton.IsChecked.Value)
                {
                    DateEndToColorConverter.ignoreConvertor = true;
                    DateEndToForeColorConverter.ignoreConvertor = true;
                    barHistoryBtn.Hint = "actieve werkopdrachten tonen";
                    recHistory.Visibility = Visibility.Visible;
                    _vc.ChangeHistoryType(HistoryType.history);
                }
                else
                {
                    DateEndToColorConverter.ignoreConvertor = false;
                    DateEndToForeColorConverter.ignoreConvertor = false;
                    barHistoryBtn.Hint = "gesloten werkopdrachten tonen";
                    recHistory.Visibility = Visibility.Hidden;
                    _vc.ChangeHistoryType(HistoryType.active);
                }
            }
        }
        private void MiModify_Click(object sender, RoutedEventArgs e)
        {
            ModifyWO();
        }
        private void Grid_CustomUnboundColumnData(object sender, GridColumnDataEventArgs e)
        {
            if (e.IsGetData)
            {
                object row = ((IList)((GridControl)sender).ItemsSource)[e.ListSourceRowIndex];
                WerkopdrachtItem woItem = (WerkopdrachtItem)row;

                if (e.Column.FieldName == "IconUnboundAttachment")
                {
                    if (woItem.AantalBijlagen.GetValueOrDefault() > 0)
                        e.Value = new BitmapImage(new Uri("/images/attachment.png", UriKind.Relative));
                    else
                        e.Value = null;
                }
                else if (e.Column.FieldName == "IconUnboundRead")
                {
                    if (woItem.ReadDate.HasValue == false)
                        e.Value = new BitmapImage(new Uri("/images/eye-hidden-32.png", UriKind.Relative));
                    else
                        e.Value = null;
                }
            }
        }
        private void ContextWaOpening(object sender, ContextMenuEventArgs e)
        {
            if (_vc.SelectedWAItem is WerkopdrachtItem selectedItem)
            {
                if (selectedItem.AantalBijlagen.GetValueOrDefault() > 0)
                    miWaBestanden.Header = selectedItem.AantalBijlagen + " Bijlagen";
                else
                    miWaBestanden.Header = "Bijlage(n) toevoegen";
            }
        }
        #region MainWindow events
        private void WerkbonnenbeheerWindow_Loaded(object sender, RoutedEventArgs e)
        {
            if (_vc.ViewMode == WerkopdrachtType.Werkaanvraag)
            {
                WpfLayoutBLL.LoadLayoutDockManager(dockManager, "werkaanvraagDocking");
                WpfLayoutBLL.LoadLayoutGrid(gridWA, "werkaanvraagWaGrid");
                WpfLayoutBLL.LoadLayoutGrid(gridWB, "werkaanvraagWbGrid");
            }
            if (_vc.ViewMode == WerkopdrachtType.Werkbon)
            {
                WpfLayoutBLL.LoadLayoutDockManager(dockManager, "werkbonDocking");
                WpfLayoutBLL.LoadLayoutGrid(gridWA, "werkbonWaGrid");
                WpfLayoutBLL.LoadLayoutGrid(gridWB, "werkbonWbGrid");
            }
            dispatcherTimer.Tick += DispatcherTimer_Tick;
            dispatcherTimer.Interval = new TimeSpan(0, 5, 0);
            dispatcherTimer.Start();
        }
        private void WerkbonnenbeheerWindow_Closed(object sender, EventArgs e)
        {
            try
            {
                if (_vc.ViewMode == WerkopdrachtType.Werkaanvraag)
                {
                    WpfLayoutBLL.SaveLayoutDockManager(dockManager, "werkaanvraagDocking");
                    WpfLayoutBLL.SaveLayoutGrid(gridWA, "werkaanvraagWaGrid");
                    WpfLayoutBLL.SaveLayoutGrid(gridWB, "werkaanvraagWbGrid");
                }

                if (_vc.ViewMode == WerkopdrachtType.Werkbon)
                {
                    WpfLayoutBLL.SaveLayoutDockManager(dockManager, "werkbonDocking");
                    WpfLayoutBLL.SaveLayoutGrid(gridWA, "werkbonWaGrid");
                    WpfLayoutBLL.SaveLayoutGrid(gridWB, "werkbonWbGrid");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            dispatcherTimer.Stop();
            dispatcherTimer.Tick -= DispatcherTimer_Tick;
            dispatcherTimer = null;

            _vc.CleanUp();
            _vc = null;
        }
        #endregion
        #region Toolbar events
        private void BiNewWaWizard_ItemClick(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            try
            {
                recAdorner.Visibility = Visibility.Visible;

                WerkopdrachtItem woItem = new WerkopdrachtItem(null, _vc.Settings.Dienst);

                AddWerkOpdrachtWizardWindow win = new AddWerkOpdrachtWizardWindow(woItem, _vc.Settings.UserType)
                {
                    Owner = this
                };
                if (win.ShowDialog().Value == true)
                {
                    _vc.AddWerkaanvraag(woItem);
                    _vc.Save();
                    gridWA.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Fout", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            finally
            {
                recAdorner.Visibility = Visibility.Hidden;
            }
        }
        private void BiRefresh_ItemClick(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            Refresh();
        }
        private void BiGoToOtherWindow_ItemClick(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            this.Cursor = Cursors.Wait;


            String textSupervisor = "";
            if (_vc.Settings.UserType== WerkopdrachtUserType.supervisor)
                textSupervisor = "(Supervisor)";

            werkopdracht_diensten _werkopdracht_diensten = GlobalData.Instance.LoggedOnUser.GetDienstByCode(_vc.Settings.Dienst);


            if (_vc.ViewMode is WerkopdrachtType.Werkaanvraag)
            {
                GlobalMethods.OpenWerkbonWindow(_werkopdracht_diensten, _vc.Settings.UserType, "Werkbonnen voor dienst "  + _vc.Settings.Dienst + " " + textSupervisor);
            }
            else
            {
                GlobalMethods.OpenWerkaanvraagWindow(_werkopdracht_diensten, _vc.Settings.UserType, "Werkaanvragen voor dienst " + _vc.Settings.Dienst + " " + textSupervisor);
            }
            this.Cursor = Cursors.Arrow;
        }
        private void BiCloseWindow_ItemClick(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            this.Close();
        }
        private void BiShowHelp_ItemClick(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            HelpWindow w = new HelpWindow("werkbonnenbeheer", "Help werkbonnebeheer");
            w.Show();
        }
        #endregion
        #region WA grid events
        private void GridWA_SelectedItemChanged(object sender, SelectedItemChangedEventArgs e)
        {
            gridWB.SelectedItemChanged -= GridWB_SelectedItemChanged;
            RefreshControls();
            gridWB.SelectedItemChanged += GridWB_SelectedItemChanged;
        }
        private void GridWA_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            TableViewHitInfo hitInfo = ((TableView)gridWA.View).CalcHitInfo(e.OriginalSource as DependencyObject);
            if (hitInfo.InRow && hitInfo.Column != null)
            {
                if (hitInfo.Column.FieldName == "IconUnboundAttachment")
                    OpenBijlagenScherm(this._vc.SelectedWAItem,this._vc.Settings.UserType == WerkopdrachtUserType.supervisor);

                else
                    ModifyWO();
            }
        }
        private void GridWA_GotFocus(object sender, RoutedEventArgs e)
        {
            if (_vc != null)
                _vc.ActivateGrid("WA");
            RefreshControls();
        }
        private void MiWANew_Click(object sender, RoutedEventArgs e)
        {
            AddNewWA(_vc.Settings.Dienst);
        }
        private void MiWACopy_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                recAdorner.Visibility = Visibility.Visible;

                WerkopdrachtItem woItem = _vc.SelectedWAItem.Clone(true);

                SaveWerkopdrachtWindow win = new SaveWerkopdrachtWindow(woItem, _vc.Settings.UserType)
                {
                    Owner = this
                };
                if (win.ShowDialog().Value == true)
                {
                    _vc.AddWerkaanvraag(woItem);
                    _vc.Save();
                    gridWA.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Fout", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            finally
            {
                recAdorner.Visibility = Visibility.Hidden;
            }
        }
        private void MiWAChangeStatus_Click(object sender, RoutedEventArgs e)
        {
            UpdateStatus(_vc.SelectedWAItem);
        }
        private void MiWAShowHistory_Click(object sender, RoutedEventArgs e)
        {
            if (_vc.SelectedWAItem != null)
                ShowStatusHistoriek(_vc.SelectedWAItem.Id.GetValueOrDefault(), _vc.SelectedWAItem.Nr + " " + _vc.SelectedWAItem.Titel);
        }
        private void MiWABestanden_Click(object sender, RoutedEventArgs e)
        {
            if (_vc.SelectedWAItem is WerkopdrachtItem selected)
                OpenBijlagenScherm(selected,this._vc.Settings.UserType == WerkopdrachtUserType.supervisor);
        }
        private void MiWASendEmail_Click(object sender, RoutedEventArgs e)
        {
            SendMail(WerkopdrachtType.Werkaanvraag);
        }
        #endregion
        #region WB grid events
        private void GridWB_SelectedItemChanged(object sender, SelectedItemChangedEventArgs e)
        {
            gridWA.SelectedItemChanged -= GridWA_SelectedItemChanged;
            RefreshControls();
            gridWA.SelectedItemChanged += GridWA_SelectedItemChanged;
        }
        private void GridWB_GotFocus(object sender, RoutedEventArgs e)
        {
            if (_vc != null)
                _vc.ActivateGrid("WB");
            RefreshControls();
        }
        private void GridWB_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            TableViewHitInfo hitInfo = ((TableView)gridWB.View).CalcHitInfo(e.OriginalSource as DependencyObject);

            if (hitInfo.InRow && hitInfo.Column != null)
            {

                if (hitInfo.Column.FieldName == "IconUnboundAttachment")
                    OpenBijlagenScherm(this._vc.SelectedWBItem,this._vc.Settings.UserType == WerkopdrachtUserType.supervisor);
                else if (hitInfo.Column.FieldName == "IconUnboundRead")
                {
                    MarkReadByUser(this._vc.SelectedWBItem);

                }
                else
                    ModifyWO();
            }
        }
        private void MiWBNew_Click(object sender, RoutedEventArgs e)
        {
            NewWB(_vc.SelectedWAItem);
        }
        private void MiWBCopy_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                recAdorner.Visibility = Visibility.Visible;

                WerkopdrachtItem woItem = _vc.SelectedWBItem.Clone(true);

                SaveWerkopdrachtWindow win = new SaveWerkopdrachtWindow(woItem, _vc.Settings.UserType)
                {
                    Owner = this
                };
                if (win.ShowDialog().Value == true)
                {
                    _vc.AddWerkbon(_vc.SelectedWAItem, woItem);
                    gridWB.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Fout", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            finally
            {
                recAdorner.Visibility = Visibility.Hidden;
            }
        }
        private void MiWBNewFromWA_Click(object sender, RoutedEventArgs e)
        {
            NewWB(_vc.SelectedWAItem);
        }
        private void MiWBChangeStatus_Click(object sender, RoutedEventArgs e)
        {
            UpdateStatus(_vc.SelectedWBItem);
        }
        private void MiWBPrint_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.Cursor = Cursors.Wait;

                WerkOpdrachtPrintItem printItem = new WerkOpdrachtPrintItem(_vc.SelectedWAItem, _vc.SelectedWBItem);
                WerkOpdrachtReport report = new WerkOpdrachtReport(printItem);
                using (ReportPrintTool printTool = new ReportPrintTool(report))
                {
                    printTool.ShowRibbonPreviewDialog();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + ex.Message, "Fout", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            finally
            {
                this.Cursor = Cursors.Arrow;
            }
        }
        private void MiWBGantt_Click(object sender, RoutedEventArgs e)
        {
            this.Cursor = Cursors.Wait;

            List<GanttChartItem> listGanttItems = new List<GanttChartItem>();
            for (int i = 0; i < gridWB.VisibleRowCount; i++)
            {

                WerkopdrachtItem wo = (WerkopdrachtItem)gridWB.GetRow(gridWB.GetRowHandleByVisibleIndex(i));
                if (wo.Begindatum != null && wo.Einddatum != null)
                {
                    GanttChartItem ig = new GanttChartItem() { FromDate = wo.Begindatum.Value, ToDate = wo.Einddatum.Value, Name = wo.Nr + ":" + wo.Titel };
                    listGanttItems.Add(ig);
                }
            }
            
            GanttChart g = new GanttChart(listGanttItems, "Gantt werkbonnen");
            g.Show();

            this.Cursor = Cursors.Arrow;
        }
        private void MiWBShowHistory_Click(object sender, RoutedEventArgs e)
        {
            if (_vc.SelectedWBItem != null)
                ShowStatusHistoriek(_vc.SelectedWBItem.Id.GetValueOrDefault(), _vc.SelectedWBItem.Nr + " " + _vc.SelectedWBItem.Titel);
        }
        private void MiWBBestanden_Click(object sender, RoutedEventArgs e)
        {
            if (_vc.SelectedWBItem is WerkopdrachtItem selected)
                OpenBijlagenScherm(selected,this._vc.Settings.UserType == WerkopdrachtUserType.supervisor);
        }
        private void MiWBSendEmail_Click(object sender, RoutedEventArgs e)
        {
            SendMail(WerkopdrachtType.Werkbon);
        }
        #endregion

        /** METHODS */
        private void RefreshControls()
        {
            if (_vc == null)
                return;

            if (_vc.SelectedWOItem != null)
            {
                String typeLong = _vc.SelectedWOItem.WerkopdrachtType == WerkopdrachtType.Werkaanvraag ? "Werkaanvraag" : "Werkbon";
                //Werkbon heeft meer info, deze info niet tonen bij werkaanvragen
                if (_vc.SelectedWOItem.WerkopdrachtType is WerkopdrachtType.Werkbon)
                {
                    rowEinddatum.Height = new GridLength(25);
                    rowBegindatum.Height = new GridLength(25);
                    rowGroepen.Height = new GridLength(25);
                    rowMedewerkers.Height = new GridLength(25, GridUnitType.Star);
                }
                else
                {
                    rowEinddatum.Height = new GridLength(0);
                    rowBegindatum.Height = new GridLength(0);
                    rowGroepen.Height = new GridLength(0);
                    rowMedewerkers.Height = new GridLength(0);
                }
                _vc.SelectLocation(_vc.SelectedWOItem.LocatieId);
                panelInfo.Caption = String.Format("Info {0} {1}", typeLong, _vc.SelectedWOItem.Id);
                panelStatus.Caption = String.Format("Status {0} {1}", typeLong, _vc.SelectedWOItem.Id);
                panelLocatie.Caption = String.Format("Locatie {0} {1}", typeLong, _vc.SelectedWOItem.Id);
            }
        }
        private void AddNewWA(String dienst)
        {
            try
            {
                recAdorner.Visibility = Visibility.Visible;

                WerkopdrachtItem woItem = new WerkopdrachtItem(null, dienst);
                SaveWerkopdrachtWindow win = new SaveWerkopdrachtWindow(woItem, this._vc.Settings.UserType);
                if (win.ShowDialog().Value == true)
                {
                    _vc.AddWerkaanvraag(woItem);
                    _vc.Save();
                    gridWA.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Fout", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            finally
            {
                recAdorner.Visibility = Visibility.Hidden;
            }
        }
        private void NewWB(WerkopdrachtItem fromItem)
        {
            try
            {
                recAdorner.Visibility = Visibility.Visible;

                WerkopdrachtItem woItem = null;
                if (fromItem != null)
                    woItem = fromItem.Clone(true);
                else
                    throw new Exception("nieuwe WB moet WA hebben");

                woItem.Parent_id = _vc.SelectedWAItem.Id;
                woItem.SetDefaultStatus();

                SaveWerkopdrachtWindow win = new SaveWerkopdrachtWindow(woItem, _vc.Settings.UserType)
                {
                    Owner = this
                };
                if (win.ShowDialog().Value == true)
                {
                    _vc.AddWerkbon(_vc.SelectedWAItem, woItem);
                    gridWB.Focus();
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Fout", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            finally
            {
                recAdorner.Visibility = Visibility.Hidden;
            }
        }
        private void ModifyWO()
        {
            try
            {
                recAdorner.Visibility = Visibility.Visible;

                if ((_vc.SelectedWOItem.WerkopdrachtType == WerkopdrachtType.Werkaanvraag && _vc.CanOpenWA)
                    || (_vc.SelectedWOItem.WerkopdrachtType == WerkopdrachtType.Werkbon && _vc.CanOpenWB))
                {
                    WerkopdrachtItem woItem = _vc.SelectedWOItem.Clone(false);
                    _vc.BllWerkbonnenBeheer.MarkAsRead(_vc.SelectedWOItem,_vc.Settings.UserType == WerkopdrachtUserType.supervisor);

                    SaveWerkopdrachtWindow win = new SaveWerkopdrachtWindow(woItem, _vc.Settings.UserType);
                    MarkReadByUser(_vc.SelectedWOItem);
                    if (win.ShowDialog().Value == true && win.IsModified)
                    {
                        _vc.SelectedWOItem.Id = woItem.Id;
                        _vc.SelectedWOItem.InventarisId = woItem.InventarisId;
                        _vc.SelectedWOItem.InventarisText = woItem.InventarisText;
                        _vc.SelectedWOItem.Titel = woItem.Titel;
                        _vc.SelectedWOItem.Beschrijving = woItem.Beschrijving;
                        _vc.SelectedWOItem.Begindatum = woItem.Begindatum;
                        _vc.SelectedWOItem.Einddatum = woItem.Einddatum;
                        _vc.SelectedWOItem.Groepen = woItem.Groepen;
                        _vc.SelectedWOItem.Medewerkers = woItem.Medewerkers;
                        _vc.SelectedWOItem.LocatieId = woItem.LocatieId;
                        _vc.SelectLocation(_vc.SelectedWOItem.LocatieId);
                        _vc.SelectedWOItem.LocatiePath = woItem.LocatiePath;
                        _vc.SelectedWOItem.PrioriteitId = woItem.PrioriteitId;
                        _vc.SelectedWOItem.StatusId = woItem.StatusId;
                        _vc.SelectedWOItem.TypeId = woItem.TypeId;
                        _vc.SelectedWOItem.IsModified = true;

                        _vc.Save();

                        MarkReadByUser(_vc.SelectedWBItem);
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Fout", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            finally
            {
                recAdorner.Visibility = Visibility.Hidden;
            }
        }
        private void MarkReadByUser(WerkopdrachtItem i)
        {
            try
            {
                _vc.BllWerkbonnenBeheer.MarkReadByUser(_vc.SelectedWBItem);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Fout", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void UpdateStatus(WerkopdrachtItem werkOpdrachtItem)
        {
            try
            {
                recAdorner.Visibility = Visibility.Visible;

                WerkopdrachtItem woItem = werkOpdrachtItem.Clone(false);
                //de clone functie reset de status naar de beginwaarden. dus moeten manueel gekopieerd worden.
                woItem.StatusId = werkOpdrachtItem.StatusId;
                woItem.StatusOpmerking = werkOpdrachtItem.StatusOpmerking;

                WerkopdrachtChangeStatusWindow win = new WerkopdrachtChangeStatusWindow(woItem)
                {
                    Owner = this
                };
                if (win.ShowDialog().GetValueOrDefault() == true)
                {
                    werkOpdrachtItem.StatusId = woItem.StatusId;
                    werkOpdrachtItem.StatusOpmerking = woItem.StatusOpmerking;
                    _vc.BllWerkbonnenBeheer.SaveStatus(werkOpdrachtItem);
                    _vc.NotifyCanProperties();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Fout", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            finally
            {
                recAdorner.Visibility = Visibility.Hidden;
            }
        }
        private void ShowStatusHistoriek(int id, string title)
        {
            recAdorner.Visibility = Visibility.Visible;

            StatusGeschiedenisOverzicht win = new StatusGeschiedenisOverzicht(id, title);
            win.ShowDialog();

            recAdorner.Visibility = Visibility.Hidden;
        }
        private void Refresh()
        {
            _vc.Refresh();
        }
        private void OpenBijlagenScherm(WerkopdrachtItem woItem, bool isSuperVisor)
        {
            recAdorner.Visibility = Visibility.Visible;

            bool IsSuperVisorTD = GlobalData.Instance.LoggedOnUser.GetAccesType("325") != AccessType.none;
            bool IsSuperVisorIT = GlobalData.Instance.LoggedOnUser.GetAccesType("330") != AccessType.none;

            String winTitle;
            if (woItem.WerkopdrachtType == WerkopdrachtType.Werkaanvraag)
                winTitle = "Bijlagen voor werkaanvraag " + woItem.Id.GetValueOrDefault();
            else
                winTitle = "Bijlagen voor werkbon " + woItem.Id.GetValueOrDefault();

            UploadFileWindowSettings settings = new UploadFileWindowSettings(winTitle, "werkbonnenbeheer")
            {
                AllowAddFile = true,
                AllowEditing = true,
                AllowDeleteFile = isSuperVisor
            };
            UploadFileVC bijlagenVc = new UploadFileVC(
                new BijlagenBLLProviderWerkbonnen(),
                woItem.Id.GetValueOrDefault(),
                settings
            );
            UploadFileWindow fw = new UploadFileWindow(bijlagenVc, GlobalMethods.Themes.normal);
            fw.ShowDialog();
            woItem.AantalBijlagen = fw.NumberOfItems;

            recAdorner.Visibility = Visibility.Hidden;
        }
        private void SendMail(WerkopdrachtType type)
        {
            if (_vc == null || _vc.SelectedWAItem == null)
                return;

            List<WerkopdrachtEmailAddress> emailAdress;
            if (type== WerkopdrachtType.Werkaanvraag)
                emailAdress = this._vc.GetEmailAdressWA();
            else
                emailAdress = this._vc.GetEmailAdressWB();

            List<String> selectedEmails = new List<string>();
            if (emailAdress.Count > 1)
            {
                try
                {
                    SMMWindow2Settings settings = new SMMWindow2Settings("Telefoonboek", GlobalMethods.Themes.normal)
                    {
                        WindowSizePercentage = 50,
                        IsChoiceWindow = true
                    };

                    SMMWindow2Level level0 = new SMMWindow2Level(0, "Telefoonboek") { AllowDelete = false, AllowModify = true, AllowAdd = false, AllowSearch = true, AllowFilter = true };
                    level0.AddColumns(
                        new SMMWindow2Column() { FieldName = "isChecked", ColumnHeader = "Selecteren", IsEditable = true, IsCheckBox = true },
                        new SMMWindow2Column() { FieldName = "eMailadress", ColumnHeader = "Emailaddres", IsEditable = false },
                        new SMMWindow2Column() { FieldName = "name", ColumnHeader = "Naam", IsEditable = false }
                    );
                    level0.DataSource = emailAdress;
                    settings.AddLevel(level0);

                    smmWindowVC vc = new smmWindowVC();

                    SMMWindow2 w = new SMMWindow2(vc, settings);
                    if (w.ShowDialog().GetValueOrDefault())
                    {
                        foreach (WerkopdrachtEmailAddress a in emailAdress)
                        {
                            if (a.isChecked)
                            selectedEmails.Add(a.eMailadress);
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message + ex.InnerException, "Fout", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
            }
            else if (emailAdress.Count==1)
            {
                selectedEmails.Add(emailAdress[0].eMailadress);
            }

            _vc.SendEmailOutllook(selectedEmails, _vc.SelectedWAItem);
        }
        private void BarButtonExportToExel_ItemClick(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            try
            {
                if (_vc.ViewMode == WerkopdrachtType.Werkaanvraag)
                    GlobalMethods.ExportGridToExcel(this.gridWA, "overzicht_werkaanvragen_" + _vc.Settings.Dienst);
                else
                    GlobalMethods.ExportGridToExcel(this.gridWB, "overzicht_werkbonnen_" + _vc.Settings.Dienst);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Foutmelding", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }

    #region convertors
    public class PriorityToColorConverter : MarkupExtension, IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null)
            {
                int priority = (int)value;

                switch (priority)
                {
                    case 1:
                        return Brushes.YellowGreen;
                    case 2:
                        return Brushes.Gold;
                    case 3:
                        return Brushes.DarkOrange;
                    case 4:
                        return Brushes.Crimson;
                }
            }
            return Brushes.White;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            // TODO Not used yet
            return null;
        }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }
    }
    public class StatusToColorConverter : MarkupExtension, IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null)
            {
                int priority = (int)value;

                switch (priority)
                {
                    case 6:
                        return Brushes.LightBlue;
                   
                }
            }
            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            // TODO Not used yet
            return null;
        }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }
    }
    public class DateEndToColorConverter : MarkupExtension, IValueConverter
    {
        public static bool ignoreConvertor = false;
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!ignoreConvertor && value != null)
            {


                DateTime dt = (DateTime)value;

                if (dt.Date == DateTime.Now.Date)
                    return Brushes.DarkOrange;

                if (dt.Date < DateTime.Now.Date)
                    return Brushes.Crimson;

            }
            return Brushes.White;

        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            // TODO Not used yet
            return null;
        }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }
    }
    public class DateEndToForeColorConverter : MarkupExtension, IValueConverter
    {
        public static bool ignoreConvertor = false;
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {


            if (!ignoreConvertor && value != null)
            {


                DateTime dt = (DateTime)value;

                if (dt.Date == DateTime.Now.Date)
                    return Brushes.White;

                if (dt.Date < DateTime.Now.Date)
                    return Brushes.White;

            }
            return Brushes.Black;

        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            // TODO Not used yet
            return null;
        }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }
    }
    
    public class PriorityFGToColorConverter : MarkupExtension, IValueConverter
    {
        public static bool ignoreConvertor = false;
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {


            if (value != null)
            {
                int priority = (int)value;

                switch (priority)
                {
                    case 6:
                        return Brushes.White;

                }
            }
       
            return Brushes.Black;

        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            // TODO Not used yet
            return null;
        }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }
    }
    #endregion
}
