﻿using ClaraFey.BLL;
using ClaraFey.ViewControllers;
using ClaraFey.ViewControllers.Werkbonnenbeheer;
using sharedcode.BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ClaraFey.Windows.Werkbonnenbeheer
{
    /// <summary>
    /// Interaction logic for SaveWerkopdrachtWindow.xaml
    /// </summary>
  

    public partial class SaveWerkopdrachtWindow : Window
    {
        public bool IsModified { get; set; }
        SaveWerkOpdrachtViewController _vc;
        bool _loaded = false;


        private string oldGroepenString = String.Empty;
        private string oldMederwerkersString = String.Empty;
        public SaveWerkopdrachtWindow(WerkopdrachtItem werkOpdrachItem, WerkopdrachtUserType UserType)
        {
            if (werkOpdrachItem.Id.GetValueOrDefault() == 0)
            {
                werkOpdrachItem.Begindatum = DateTime.Today;
            }
            oldGroepenString = werkOpdrachItem.GroepenString.Trim();
            oldMederwerkersString = werkOpdrachItem.MedewerkersString.Trim();
            this._vc = new SaveWerkOpdrachtViewController(werkOpdrachItem, UserType);
            this.DataContext = _vc;
            InitializeComponent();
            this.Loaded += SaveWerkopdrachtWindow_Loaded;
            this.Closed += SaveWerkopdrachtWindow_Closed;
            LocatieBoom.SelectedItemChanged += LocatieBoom_SelectedItemChanged;
        }
        void SaveWerkopdrachtWindow_Closed(object sender, EventArgs e)
        {
            CleanUp();
        }
        void SaveWerkopdrachtWindow_Loaded(object sender, RoutedEventArgs e)
        {
            _loaded = true;
         
        }
        private void LocatieBoom_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            Location location = e.NewValue as Location;
            if (location != null && _vc != null) 
            {
                this._vc.SelectedLocatieItem = location;
                if (_loaded)
                   this._vc.UnselectInventarisItem();
            }
        }
        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            this.Close();
        }
        private void btnOk_Click(object sender, RoutedEventArgs e)
        {
            String errorMessage;
            if (_vc.Validate(out errorMessage))
            {
                _vc.TransferMedewerkersGroepen();

                LocatieBoom.SelectedItemChanged -= LocatieBoom_SelectedItemChanged;
                this.DialogResult = true;
                this.Close();
            }
            else
            {

                MessageBox.Show(errorMessage, "Fout", MessageBoxButton.OK, MessageBoxImage.Error);

            }

        }

        private void btnChooseInventaris_Click(object sender, RoutedEventArgs e)
        {

            
            if (_vc.SelectedLocatieItem == null || _vc.SelectedLocatieItem.Id.HasValue == false)
            {
                MessageBox.Show("Gelieve eerst een locatie uit te kiezen", "Locatie", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
        

            InventarisPopupWindow win = new InventarisPopupWindow(_vc.SelectedLocatieItem.Id.Value,this._vc.WerkOpdrachtItem.InventarisId,false, _vc.InventarisToestelCategorieList);
            win.Title = "Inventaris locatie " + _vc.SelectedLocatieItem.GetFullPath();
            if (win.ShowDialog().GetValueOrDefault() == true)
            {
                try
                {
                    this._vc.SetInventarisItem(win.SelectedItem);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Fout", MessageBoxButton.OK, MessageBoxImage.Error);
               }
            }
        }
        private void CleanUp()
        {
            this.IsModified = this._vc.WerkOpdrachtItem.IsModified;
            if (oldGroepenString != this._vc.WerkOpdrachtItem.GroepenString.Trim())
                this.IsModified = true;
            if (oldMederwerkersString != this._vc.WerkOpdrachtItem.MedewerkersString.Trim())
                this.IsModified = true;


            LocatieBoom.SelectedItemChanged -= LocatieBoom_SelectedItemChanged;
            this.Loaded -= SaveWerkopdrachtWindow_Loaded;
            this.Closed -= SaveWerkopdrachtWindow_Closed;
            this._vc.CleanUp();
            this._vc = null;
        }

        private void btnChangeStatus_Click(object sender, RoutedEventArgs e)
        {

        }
    }

}
