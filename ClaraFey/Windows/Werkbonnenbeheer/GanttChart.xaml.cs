﻿using ClaraFey.CommonObjects;
using DevExpress.Xpf.Charts;
using sharedcode.CommonObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ClaraFey.Windows.Werkbonnenbeheer
{
    /// <summary>
    /// Interaction logic for GanttChart.xaml
    /// </summary>
    public partial class GanttChart : Window
    {
        public GanttChart(List<GanttChartItem> listGanttItems,String formTitle)
        {
            InitializeComponent();
            this.Title = formTitle;
            AddPoints(listGanttItems);
        }
        private void AddPoints(List<GanttChartItem> listGanttItems)
        {

            ChartControl chart = this.chartControl1;

            // Create a diagram.
            XYDiagram2D diagram = (XYDiagram2D)chartControl1.Diagram;
  

            // Create a bar series.
            RangeBarSideBySideSeries2D series = new RangeBarSideBySideSeries2D();
            series.ArgumentScaleType = ScaleType.Qualitative;
            series.ValueScaleType = ScaleType.DateTime;
            diagram.Series.Clear();
            
            
            diagram.Series.Add(series);
            foreach (GanttChartItem i in listGanttItems)
            {
                SeriesPoint pi = new SeriesPoint();
                pi.Argument = i.Name;
                pi.DateTimeValue = i.FromDate;

                DevExpress.Xpf.Charts.RangeBarSeries2D.SetDateTimeValue2(pi, i.ToDate);
                series.Points.Add(pi);
            }   
          


           
        }

    }
}
