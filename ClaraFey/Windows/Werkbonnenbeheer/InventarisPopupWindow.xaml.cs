﻿using ClaraFey.BLL;
using ClaraFey.ViewControllers.Werkbonnenbeheer;
using sharedcode.BLL;
using sharedcode.WeergaveClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ClaraFey.Windows.Werkbonnenbeheer
{
    /// <summary>
    /// Interaction logic for InventarisPopupWindow.xaml
    /// </summary>
    public partial class InventarisPopupWindow : Window
    {
        InventarisPopupVC _vc;

        private bool _readOnly;
        public InventarisWeergave SelectedItem
        {

            get
            {

                if (_vc != null)
                    return _vc.SelectedInventarisItem;
                else return null;

            }

        }
        public InventarisPopupWindow(int locatieId, int? inventarisItemId, bool readOnly,List<ToestelCategorieType> categorien )
        {
            this._readOnly = readOnly;

           

            _vc = new InventarisPopupVC(locatieId, inventarisItemId,categorien);
            InitializeComponent();
            if (_readOnly)
                btnOk.Visibility = System.Windows.Visibility.Hidden;
            this.DataContext = _vc;
            this.Closed += InventarisPopupWindow_Closed;
        }

        void InventarisPopupWindow_Closed(object sender, EventArgs e)
        {
            _vc.CleanUp();
           
        }

        private void btnOk_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
            this.Close();
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            this.Close();

        }
    }
}
