﻿using ClaraFey.BLL;
using ClaraFey.CommonObjects;
using ClaraFey.ViewControllers;
using ClaraFey.ViewControllers.Aankoop.IM;
using ClaraFey.ViewControllers.Kilometervergoeding;
using ClaraFey.Windows.Common;
using DevExpress.Xpf.Bars;
using DevExpress.Xpf.Grid;
using DevExpress.XtraReports.UI;
using sharedcode.BLL;
using sharedcode.common;
using sharedcode.WeergaveClasses;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;

using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ClaraFey.Windows.Aankoop.IM
{
    /// <summary>
    /// Interaction logic for KilometerWindow.xaml
    /// </summary>
    public partial class AankoopIMAfsluitWindow : Window
    {
        /** FIELDS */
        private readonly AankoopIMAfsluitVM _vc;

        /** CONSTRUCTORS */
        public AankoopIMAfsluitWindow()
        {
             _vc = new AankoopIMAfsluitVM();
            InitializeComponent();
            GlobalMethods.SetTheme(this, GlobalMethods.Themes.clientdossier);
            this.Loaded += KilometerWindow_Loaded;
            this.Closed += KilometerWindow_Closed;
            this.DataContext = _vc;
        }

        /** EVENTS */
        private void KilometerWindow_Closed(object sender, EventArgs e)
        {
           
        }
        private void KilometerWindow_Loaded(object sender, RoutedEventArgs e)
        {
           
           
        }
       
       
        private void barButtonClose_ItemClick(object sender, ItemClickEventArgs e)
        {
            this.Close();
        }
        private void barButtonCloseSelection_ItemClick(object sender, ItemClickEventArgs e)
        {
            try
            {
                _vc.Aflsuiten();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Fout", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void biShowHelp_ItemClick(object sender, ItemClickEventArgs e)
        {
            HelpWindow w = new HelpWindow("AankoopIMAfsluit", "Help afsluiten aankoop incontinentiemateriaal");
            w.Show();
        }
        private void barButtonExportToExel_ItemClick(object sender, ItemClickEventArgs e)
        {
           
            string title = "Aankoop incontinentiemateriaal - " + _vc.SelectedAfsluitDatum.Value.ToString("dd-MM-yyyy");
            SMMWindow2Level level0 = new SMMWindow2Level(0, title)
            {
                AllowDelete = false,
                AllowModify = false,
                AllowAdd = false,
                AllowSearch = false,
                AllowFilter = false,
                CanExportToExcel = true
            };
            level0.AddColumns(
                  new SMMWindow2Column() { FieldName = "Rijksregisternummer", ColumnHeader = "Rijksregisternummer", IsEditable = false },
                new SMMWindow2Column() { FieldName = "DatumString", ColumnHeader = "Datum", IsEditable = false },
                new SMMWindow2Column() { FieldName = "FinancielbewegingsCode", ColumnHeader = "FinancielbewegingsCode", IsEditable = false },
                new SMMWindow2Column() { FieldName = "Omschrijving", ColumnHeader = "Omschrijving", IsEditable = false },
                new SMMWindow2Column() { FieldName = "Eenheden", ColumnHeader = "Eenheden", IsEditable = false },
                new SMMWindow2Column() { FieldName = "Eenheidsprijs", ColumnHeader = "Eenheidsprijs", IsEditable = false },
                new SMMWindow2Column() { FieldName = "Totaal", ColumnHeader = "Totaal", IsEditable = false }
            );
            level0.DataSource = _vc.GetCSV();
          








            SMMWindow2Settings settings = new SMMWindow2Settings(title, GlobalMethods.Themes.normal)
            {
                WindowSizePercentage = 75
            };
            settings.AddLevel(level0);
            settings.theme = GlobalMethods.Themes.clientdossier;
            smmWindowVC vc = new smmWindowVC();
            SMMWindow2 w = new SMMWindow2(vc, settings);
            w.ShowDialog();
            
        }

       

        private void TableView_CellValueChanging(object sender, CellValueChangedEventArgs e)
        {
             
        }
 
      
       
        private void PrintReport(DateTime dateFrom, DateTime dateTo, bool afsluiten)
        {
            try
            {
                this.Cursor = Cursors.Wait;

             
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + ex.Message, "Fout", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            finally
            {
                this.Cursor = Cursors.Arrow;
            }
        
        }

        private void barButtonBestelbon_ItemClick(object sender, ItemClickEventArgs e)
        {
            _vc.PrintBestelBon();
        }

        private void barButtonClientAfrekening_ItemClick(object sender, ItemClickEventArgs e)
        {
            _vc.PrintAfrekening();
        }

        private void barButtonRapportLeefgroep_ItemClick(object sender, ItemClickEventArgs e)
        {
            _vc.PrintRapportLeefgroep();
        }
    }
    #region convertors
    public class NoKilometersColorConverter : MarkupExtension, IValueConverter
    {
        public static bool ignoreConvertor = false;
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
            {


                return Brushes.Red;

            }
            return Brushes.White;

        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            // TODO Not used yet
            return null;
        }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }
    }
    public class NoKilometersForeColorConverter : MarkupExtension, IValueConverter
    {       
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {


            if (value == null)
            {

 
                    return Brushes.White;

            }
            return Brushes.Black;

        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            // TODO Not used yet
            return null;
        }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }
    }

    public class myDetailVisibleConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            // Obtaining the value to be converted 
            string categoryValue = (string)value;

            // Specifying values for which to show expand buttons
            string[] categories = new string[] { "First", "Third" };
            if (categories.Contains(categoryValue))
                return true;

            // Disable expand button if the value isn't in the list
            return false;
        }
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
    #endregion
}
