﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ClaraFey.Windows.Registratie
{
    /// <summary>
    /// Interaction logic for RegistratieWindow.xaml
    /// </summary>
    public partial class RegistratieWindow : Window
    {
        public String newPaswoord { get; set; }
        public RegistratieWindow(String userName)
        {
            InitializeComponent();
            lblTitle.Content = userName + ", gelieve een nieuw paswoord te kiezen.";
        }

        private void btnOK_Click(object sender, RoutedEventArgs e)
        {
            
            String  paswoord1 = tbPaswoord.Text.Trim();
            String paswoord2 = tbPaswoord2.Text.Trim();
            if (paswoord1.Length<5)
            {
                MessageBox.Show("Paswoord moet min. 5 karakters lang zijn.","fout",MessageBoxButton.OK,MessageBoxImage.Error);
                return;
            }
            if (paswoord1 != paswoord2)
            {
                MessageBox.Show("Paswoord en paswoord herhaling zijn niet gelijk.", "fout", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            newPaswoord = paswoord1;
            this.DialogResult = true;
            this.Close();
        }
    }
}
