﻿using ClaraFey.CommonObjects;
using ClaraFey.ViewControllers;
using ClaraFey.ViewControllers.ClientDossier;
using DevExpress.Xpf.Core;
using DevExpress.Xpf.Grid;
using DevExpress.Xpf.Grid.TreeList;
using sharedcode.BLL;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;
using ClaraFey.Notifications;
using sharedcode.common;
using ClaraFey.Windows.Common;
using ClaraFey.ViewControllers.Personeel;

namespace ClaraFey.Windows.Personeel
{
    /// <summary>
    /// Interaction logic for WindowMain.xaml
    /// </summary>
    public partial class WindowMain : Window
    {
        private static Action EmptyDelegate = delegate () { };

        PersoneelNavigationVC _vc = new PersoneelNavigationVC();

        /** CONSTRUCTOR */
        public WindowMain()
        {
            InitializeComponent();

            _vc.OnSaveError += _vc_OnSaveError;
            this.Title = "Personeelsdossier";
            GlobalMethods.SetTheme(this,GlobalMethods.Themes.clientdossier);
             
            this.DataContext = _vc;
            _vc.PropertyChanged += _vc_PropertyChanged;
        }

        /** EVENTS */
        private void view_ItemSelecting(object sender, DevExpress.Xpf.NavBar.NavBarItemSelectingEventArgs e)
        {
        }
        private void WindowMain_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            try
            {
                if (_vc.Save() == false)
                    e.Cancel = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message,"Fout", MessageBoxButton.OK,MessageBoxImage.Error);
                e.Cancel = true;
            }
        }
        private void _vc_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e!=null && e.PropertyName=="SelectNavBarItem")
            {
                if (_vc.SelectNavBarItem.Index == 0)
                {
                    navBarGeneral.Focus();
                }
                else
                {
                    navBar.Focus();
                }
            }
            if (e!=null && e.PropertyName=="SelectedBewoner")
            {
                ImageEdit.Source = null;
            }
        }
        private void NamelookUpEdit_KeyDown(object sender, KeyEventArgs e)
        {

        }
        private void ImageEdit_EditValueChanged(object sender, DevExpress.Xpf.Editors.EditValueChangedEventArgs e)
        {
            if (ImageEdit.Source != null && _vc.SelectedPersoneel != null)
            {
                try
                {
                    byte[] fotopBytes = GlobalSharedMethods.getJPGFromImageControl((BitmapImage)ImageEdit.Source);
                    GebruikerBLL.SavePicture(_vc.SelectedPersoneel.id, fotopBytes);
                    _vc.SelectedPersoneel.SetFoto(fotopBytes);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }

            }
        }
        private void NamelookUpEdit_PopupClosed(object sender, DevExpress.Xpf.Editors.ClosePopupEventArgs e)
        {
            if (e.CloseMode == DevExpress.Xpf.Editors.PopupCloseMode.Normal)
                    BewonerSetPicture();
        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            LeefgroepBewonersTree win = new LeefgroepBewonersTree(CommonObjects.GlobalMethods.Themes.clientdossier,false);
            if (win.ShowDialog().GetValueOrDefault())
            {
                if (win.SelectedItem.Object is gebruiker b)
                {
                    _vc.SelectPersoneel(b.id);
                    BewonerSetPicture();
              
                }
            }
        }
        private void navItemOvereenkomsten_Click(object sender, EventArgs e)
        {
            try
            {
                //   _vc.SelectPage(1);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Fout", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void contentControl1_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
        }
        private void Todo_Click(object sender, EventArgs e)
        {
        }
        private void biNieuw_ItemClick(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
        }
        private void navBarItemNietGetekendeOvereenkomsten_Click(object sender, EventArgs e)
        {
            /*     SMMWindow2Settings settings = new SMMWindow2Settings("Niet getekende overeenkomsten", GlobalMethods.themes.clientdossier);
                 SMMWindow2Level level0 = new SMMWindow2Level(0, "Niet getekende overeenkomsten") {   CanExportToExcel=false, AllowDelete = false, AllowModify = false, AllowAdd = false, AllowSearch = true, AllowFilter = true };
                 level0.AddColumns(new SMMWindow2Column() { FieldName = "id", ColumnHeader = "Id", IsEditable = false, Visible = false }, new SMMWindow2Column() { FieldName = "voornaam", ColumnHeader = "voornaam", IsEditable = false }, new SMMWindow2Column() { FieldName = "achternaam", ColumnHeader = "achternaam", IsEditable = false }
                 , new SMMWindow2Column() { FieldName = "CategoryText", ColumnHeader = "overeenkomst", IsEditable = false }
                 , new SMMWindow2Column() { FieldName = "creationdate", ColumnHeader = "toegevoegd op", IsEditable = false });

                 smmWindowVC vc = _vc.getSMMVC(ClientDossierNavigationVC.SMMPageType.nietgetekendeovereenkomsten);
                 var someDbSet = vc.Entity.Set<vw_cd_nietgetekendeovereenkomsten>().OrderByDescending(x=>x.creationdate).ToList();
                 level0.DataSource = someDbSet;
                 settings.AddLevel(level0);*/
            // _vc.SelectSMM(settings, ClientDossierNavigationVC.SMMPageType.nietgetekendeovereenkomsten);
        }
        private void navBarItemClientDashboard_Click(object sender, EventArgs e)
        {
            // _vc.SelectPage(0);
            /*  if (_vc.SelectedBewoner != null)
              {
                  SMMWindow2Settings settings = new SMMWindow2Settings("Dashboard", GlobalMethods.themes.clientdossier);
                  SMMWindow2Level level0 = new SMMWindow2Level(0, "Dashboard") { CanExportToExcel = false, AllowDelete = false, AllowModify = false, AllowAdd = false, AllowSearch = true, AllowFilter = true };
                  level0.AddColumns(new SMMWindow2Column() { FieldName = "id", ColumnHeader = "Id", IsEditable = false, Visible = false }, new SMMWindow2Column() { FieldName = "category", ColumnHeader = "category", IsEditable = false }, new SMMWindow2Column() { FieldName = "titel", ColumnHeader = "titel", IsEditable = false }
                  , new SMMWindow2Column() { FieldName = "bericht", ColumnHeader = "bericht", IsEditable = false }
                  , new SMMWindow2Column() { FieldName = "creationdate", ColumnHeader = "datum", IsEditable = false });
                  smmWindowVC vc = _vc.getSMMVC(ClientDossierNavigationVC.SMMPageType.dashboard);
                  var someDbSet = vc.Entity.Set<vw_cd_dashboard>().SqlQuery("select * from vw_cd_dashboard where bewonerid = " + _vc.SelectedBewoner.id + " order by creationdate desc").ToList();

                  level0.DataSource = someDbSet;
                  settings.AddLevel(level0);
                  //_vc.SelectSMM(settings, ClientDossierNavigationVC.SMMPageType.dashboard);
              } */
        }
        private void Test_Click(object sender, EventArgs e)
        {
            /* if (_vc.SelectedBewoner != null)
             {
                 SMMWindow2Settings settings = new SMMWindow2Settings("Dashboard");
                 SMMWindow2Level level0 = new SMMWindow2Level(0, "Dashboard") { CanExportToExcel = false, AllowDelete = false, AllowModify = false, AllowAdd = false, AllowSearch = true, AllowFilter = true };
                 level0.AddColumns(new SMMWindow2Column() { FieldName = "id", ColumnHeader = "Id", IsEditable = false, Visible = false }, new SMMWindow2Column() { FieldName = "category", ColumnHeader = "category", IsEditable = false }, new SMMWindow2Column() { FieldName = "titel", ColumnHeader = "titel", IsEditable = false }
                 , new SMMWindow2Column() { FieldName = "bericht", ColumnHeader = "bericht", IsEditable = false }
                 , new SMMWindow2Column() { FieldName = "creationdate", ColumnHeader = "datum", IsEditable = false });
                 smmWindowVC vc = _vc.getSMMVC(ClientDossierNavigationVC.SMMPageType.dashboard);
                 var someDbSet = vc.Entity.Set<vw_cd_dashboard>().SqlQuery("select * from vw_cd_dashboard where bewonerid = " + 2 + " order by creationdate desc").ToList();

                 level0.DataSource = null;
                 level0.DataSource = someDbSet;
                 settings.AddLevel(level0);
                 _vc.SelectSMM(settings, ClientDossierNavigationVC.SMMPageType.dashboard);
             }*/

            //   _vc.SelectPage(0);
        }
        private void AdministratieveInformatie_Click(object sender, EventArgs e)
        {
            //   _vc.SelectPage(2);
        }
        private void Begeleidingen_Click(object sender, EventArgs e)
        {
            //_vc.SelectPage(3);
        }
        private void NamelookUpEdit_SelectedIndexChanged(object sender, RoutedEventArgs e)
        {
            NamelookUpEdit.RefreshData();
        }

        /** METHODS */
        private void _vc_OnSaveError(object sender, string errorMessage)
        {
            MessageBox.Show(errorMessage, "Fout", MessageBoxButton.OK, MessageBoxImage.Error);
            navBar.UpdateLayout();
        }
        private void BewonerSetPicture()
        {
            this.ImageEdit.EditValueChanged -= new DevExpress.Xpf.Editors.EditValueChangedEventHandler(this.ImageEdit_EditValueChanged);
            ImageEdit.Source = null;
            if (_vc.SelectedPersoneel != null && _vc.SelectedPersoneel.GetFoto() != null)
            {
                try
                {
                    ImageEdit.Source = GlobalSharedMethods.DecodePhoto(_vc.SelectedPersoneel.GetFoto());
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Fout", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }

            GridControl g = NamelookUpEdit.GetGridControl();
            if (g != null)
                g.FilterString = String.Empty;
            this.ImageEdit.EditValueChanged += new DevExpress.Xpf.Editors.EditValueChangedEventHandler(this.ImageEdit_EditValueChanged);
        }
    }

    public class CustomChildrenSelector : IChildNodesSelector
    {
        public IEnumerable SelectChildren(object item)
        {
            if (item is bewoner)
                return null;
              if (item is leefgroep)
                return (item as leefgroep).bewoner;
          
            return null;
        }
    }
}
