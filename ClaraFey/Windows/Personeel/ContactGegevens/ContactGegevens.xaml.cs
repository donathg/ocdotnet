﻿using ClaraFey.ViewControllers.Personeel.ContactGegevens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ClaraFey.Windows.Personeel.ContactGegevens
{


   
    /// <summary>
    /// Interaction logic for ContactGegevens.xaml
    /// </summary>
    public partial class ContactGegevens : UserControl
    {
        public ContactGegevens()
        {
            InitializeComponent();
        }

        private void btnResetPassword_Click(object sender, RoutedEventArgs e)
        {
            ContactGegevensVC vc = GetVC();
            if (MessageBox.Show(String.Format("Wenst u het paswoord te resetten van {0} ?", vc.Gebruiker.VoornaamAchternaam), "paswoord", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
               
                    try
                    {
              
                        vc.ResetPassword();
                        MessageBox.Show(String.Format("Het wachtwoord van {0} is gereset.", vc.Gebruiker.VoornaamAchternaam), "paswoord", MessageBoxButton.YesNo, MessageBoxImage.Information);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "fout", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                 
            }
          
        }
        private ContactGegevensVC GetVC()
        {
            return this.DataContext as ContactGegevensVC;
        }
    }
}
