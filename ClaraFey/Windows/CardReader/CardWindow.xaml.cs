﻿using EidSamples.tests;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
 
 
using EidSamples;
using ClaraFey.CommonObjects;
using sharedcode.common;
using System.Threading;

namespace ClaraFey.Windows.CardReader
{
    /// <summary>
    /// Interaction logic for CardWindow.xaml
    /// </summary>
    public partial class CardWindow : Window
    {
        bool CardFound = false;
        System.Windows.Threading.DispatcherTimer dispatcherTimer = new System.Windows.Threading.DispatcherTimer();

        public CardWindow(GlobalMethods.Themes theme)
        {
            InitializeComponent();
            GlobalMethods.SetTheme(this, theme);
            infoLabel.Content = "Kaart zoeken....";
            this.Loaded += CardWindow_Loaded;
        }

        private void CardWindow_Loaded(object sender, RoutedEventArgs e)
        {
            dispatcherTimer.Tick += new EventHandler(dispatcherTimer_Tick);
            dispatcherTimer.Interval = new TimeSpan(0, 0, 2);
            dispatcherTimer.Start();
        }

        private void dispatcherTimer_Tick(object sender, EventArgs e)
        {
            try
            {
                infoLabel.Content = "Kaart zoeken...";
                ReadCard();
                dispatcherTimer.Stop();
                infoLabel.Content = "Kaartgegevens gevonden en uitgelezen";
                infoLabel.Foreground = Brushes.Green;
                CardFound = true;
            }
            catch (Exception)
            {
                infoLabel.Content = "Kaart niet gevonden.  Kaart zoeken...";
            }
       }

       
        private void ReadCard()
        {
           
            ClaraFeyPublicAPI api = new ClaraFeyPublicAPI();
            EidInfo info = api.GetCardInfo();
            tbFirstName.Text = info.Naam;
            tbGeboorteDatum.Text = info.GeboorteDatum + " " + info.GeboortePlaats;
            tbAdres.Text = info.Adres;
            tbGeslacht.Text = info.Sex;
            if (info.Foto != null)
                image.Source = GlobalSharedMethods.ByteToImage(info.Foto);
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
