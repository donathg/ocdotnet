﻿using ClaraFey.CommonObjects;
using ClaraFey.ViewControllers.Rapportage;
using ClaraFey.Windows.Common;
using ClaraFey.Windows.Kilometervergoeding;
using DevExpress.Data.Filtering;
using DevExpress.Data.Filtering.Helpers;
using DevExpress.Xpf.Editors.Filtering;
using DevExpress.Xpf.Grid;
using sharedcode.BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ClaraFey.Windows.Rapportage
{
    /// <summary>
    /// Interaction logic for KasbeheerExport.xaml
    /// </summary>
    public partial class KasbeheerExportWindow : Window
    {
        private readonly KasBeheerExportVC _vc;

        public KasbeheerExportWindow()
        {
            _vc = new KasBeheerExportVC();
            DataContext = _vc;

            InitializeComponent();
        }

        private void BtnFilterSearch2_ItemClick(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            try
            {
                _vc.LoadKasbeheerItems();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Fout", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void BarButtonCloseSelection_ItemClick(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            try
            {
                AfsluitenKasbeheerReportWindow w = new AfsluitenKasbeheerReportWindow(_vc.Filter.DateTo, _vc.Filter.LeefgroepId)
                {
                    WindowStartupLocation = WindowStartupLocation.CenterScreen
                };
                w.ShowDialog();

                if (!string.IsNullOrWhiteSpace(w.Result))
                {
                    try
                    {
                        this.Cursor = Cursors.Wait;

                        string messageboxText = $"Zeker dat u alle transacties tot {w.DateTo.ToString("dd/MM/yyyy")} wil aflsuiten ?";
                        if (!(w.Result == "AFSLUITEN"))
                            _vc.PrintReport(w.DateTo, false);
                        else if (w.Result == "AFSLUITEN" && MessageBox.Show(messageboxText, "Afsluiten", MessageBoxButton.OKCancel, MessageBoxImage.Question) == MessageBoxResult.OK)
                        {
                            _vc.PrintReport(w.DateTo, w.Result == "AFSLUITEN");
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message + ex.Message, "Fout", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                    finally
                    {
                        this.Cursor = Cursors.Arrow;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Fout", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void BarButtonLastClosed_ItemClick(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            try
            {
                _vc.PrintLastClosedReport();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Fout", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void BarButtonExportToExcel_ItemClick(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            GlobalMethods.ExportGridToExcel(gridKasbeheer, "kasbeheer_" + _vc.Filter.DateFrom.ToString("dd-MM-yyyy") + "_" + _vc.Filter.DateTo.ToString("dd-MM-yyyy"));
        }
        private void BiShowHelp_ItemClick(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            HelpWindow w = new HelpWindow("KasbeheerExport", "Help kasbeheer export");
            w.Show();
        }
    }
}
