﻿using ClaraFey.CommonObjects;
using sharedcode.BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ClaraFey.Windows.Rapportage
{
    /// <summary>
    /// Interaction logic for AfsluitenKasbeheerReportWindow.xaml
    /// </summary>
    public partial class AfsluitenKasbeheerReportWindow : Window
    {
        public string Text { get; set; }
        public string Result { get; set; }
        public DateTime DateTo { get; set; }
        public AfsluitenKasbeheerReportWindow(DateTime dateTo, int leefgroepId)
        {
            GlobalMethods.SetTheme(this, GlobalMethods.Themes.clientdossier);
            DateTo = dateTo;
            Text = $"Je staat op punt om alles af te sluiten van de leefgroep: {LeefgroepBLL.GetLeefgroepFromId(leefgroepId).naam} tot einddatum:";
            this.DataContext = this;

            InitializeComponent();
        }

        private void btnTest_Click(object sender, RoutedEventArgs e)
        {
            Result = "TEST";
            Close();
        }

        private void btnAfsluiten_Click(object sender, RoutedEventArgs e)
        {
            Result = "AFSLUITEN";
            this.Close();
        }
    }
}
