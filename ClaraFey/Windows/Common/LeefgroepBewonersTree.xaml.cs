﻿using ClaraFey.CommonObjects;
using DevExpress.Xpf.Grid;
using DevExpress.Xpf.Grid.TreeList;
using DevExpress.Xpf.Layout.Core;
using DevExpress.Xpf.Layout.Core.Base;
using sharedcode.BLL;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ClaraFey.Windows.Common
{
    /// <summary>
    /// Interaction logic for LeefgroepBewonersTree.xaml
    /// </summary>
    public partial class LeefgroepBewonersTree : Window, INotifyPropertyChanged
    {
        /** FIELDS */
        public event PropertyChangedEventHandler PropertyChanged;

        public List<LeefgroepBewonerTreeItem> ListLeefgroep { get; set; }
        private LeefgroepBewonerTreeItem _selectedItem;
        public LeefgroepBewonerTreeItem SelectedItem
        {
            get
            {
                return _selectedItem;
            }
            set
            {
                _selectedItem = value;
                NotifiyPropertyChanged("IsSelected");
            }
        }
        public bool IsSelected
        {
            get
            {
                if (SelectedItem == null)
                    return false;
                if (this.canSelectLeefgroep)
                    return true;
                if (SelectedItem.ParentId != null)
                    return true;

                return false;

            }
        }

        bool canSelectLeefgroep = false;

        /** CONSTRUCTOR */
        public LeefgroepBewonersTree(GlobalMethods.Themes theme, bool canSelectLeefgroep)
        {
            this.canSelectLeefgroep = canSelectLeefgroep;
            ListLeefgroep = LeefgroepBLL.GetLeefGroepenBewonersForTree();
            GridControl.AllowInfiniteGridSize = true;
            InitializeComponent();
            GlobalMethods.SetTheme(this, theme);
            DataContext = this;
        }
        public LeefgroepBewonersTree(GlobalMethods.Themes theme, int bewonerId )
        {
            this.canSelectLeefgroep = false;
            ListLeefgroep = LeefgroepBLL.GetLeefGroepenBewonersForTree(bewonerId);
            GridControl.AllowInfiniteGridSize = true;
            InitializeComponent();
            GlobalMethods.SetTheme(this, theme);
            DataContext = this;
            this.Loaded += LeefgroepBewonersTree_Loaded;


        }

        private void LeefgroepBewonersTree_Loaded(object sender, RoutedEventArgs e)
        {
            treeView.ShowSearchPanelMode = ShowSearchPanelMode.Never;
            treeView.ExpandAllNodes();
        }

        /** EVENTS */
        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            SelectedItem = null;
            this.Close();
        }
        private void AcceptSelection_Click(object sender, RoutedEventArgs e)
        {
            if (IsSelected)
            {
                this.DialogResult = true;
                this.Close();
            }
        }

        /** METHODS */
        public void NotifiyPropertyChanged(string property)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(property));
        }

         
    }
}
