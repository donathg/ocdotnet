﻿using ClaraFey.BLL;
using DevExpress.Xpf.Grid;
using sharedcode.BLL;
using sharedcode.common;
using sharedcode.CommonObjects;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Forms;
using ClaraFey.CommonObjects;
using ClaraFey.ViewControllers;
using ClaraFey.Windows.ClientDossier;
using ClaraFey.ViewControllers.ClientDossier;
using ClaraFey.ViewControllers.Common;
using System.ComponentModel;
using DevExpress.Mvvm.UI.Interactivity;

namespace ClaraFey.Windows.Common
{
    /// <summary>
    /// Interaction logic for UploadFileWindow.xaml
    /// </summary>
    public class UploadFileWindowSettings
    {
        public string WindowTitle { get; set; }
        public bool AllowAddFile { get; set; }
        public bool AllowEditing { get; set; }
        public bool AllowDeleteFile { get; set; }
        public String DownloadSubFolder { get; set; }
        public List<labels> PresetLabelList { get; set; }
        
        /// <param name="windowTitle">the title of the window</param>
        /// <param name="DownloadSubFolder">fe werkbonnenbeheer or inventaris</param>
        public UploadFileWindowSettings(String windowTitle, String downloadSubFolder)
        {
            WindowTitle = windowTitle;
            DownloadSubFolder = downloadSubFolder;
        }
    }
    /** NODIG VOOR DISABLEN VAN CHECKBOXCOLUMNHEADER */
    public class DisableSelectorColumnBehavior : Behavior<TableView>
    {
        protected override void OnAttached()
        {
            base.OnAttached();

            AssociatedObject.CheckBoxSelectorColumnHeaderTemplate = new DataTemplate();
            AssociatedObject.PreviewMouseDown += OnViewPreviewMouseDown;
        }
        protected virtual void OnViewPreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            TableViewHitInfo hi = AssociatedObject.CalcHitInfo((DependencyObject)e.OriginalSource);

            if (hi.InColumnHeader && hi.Column.FieldName == "DX$CheckboxSelectorColumn")
                e.Handled = true;
        }
        protected override void OnDetaching()
        {
            AssociatedObject.PreviewMouseDown -= OnViewPreviewMouseDown;
            base.OnDetaching();
        }
    }

    public partial class UploadFileControl : System.Windows.Controls.UserControl
    {
        /** VARIABLES */
        private UploadFileVC _vc;
        
        /** CONSTRUCTOR */
        public UploadFileControl()
        {
            InitializeComponent();
            ((GridViewBase)gridFiles.View).ShowGroupPanel = false;
            gridFiles.View.ShowSearchPanel(true);
        }
        public UploadFileControl(UploadFileVC vc) : this()
        {
            _vc = vc;
            this.DataContext = _vc;
        }
        public UploadFileControl(GlobalMethods.Themes theme, UploadFileVC vc) : this(vc)
        {
            _vc.Theme = theme;
            GlobalMethods.SetTheme(this, theme);
        }

        /** EVENTS */
        private void UserControl_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            _vc = this.DataContext as UploadFileVC;
        }
        private void SaveFile_Click(object sender, RoutedEventArgs e)
        {
            _vc.SaveSelectedFileOnComputer();
        }
        private void DeleteFile_Click(object sender, RoutedEventArgs e)
        {
            _vc.DeleteSelectedFile();
        }
        private void OpenFile_Click(object sender, RoutedEventArgs e)
        {
            _vc.OpenSelectedFile();
        }
        private void UploadFileWindow_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == Key.Delete && _vc.SelectedItems != null && _vc.SelectedItems.Count != 0)
                _vc.DeleteSelectedFile();
        }
        private void Grid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            TableViewHitInfo hitInfo = ((TableView)gridFiles.View).CalcHitInfo(e.OriginalSource as DependencyObject);

            if (hitInfo.InRow && hitInfo.Column.FieldName != "Description")
                _vc.OpenSelectedFile();
        }
        private void Grid_Drop(object sender, System.Windows.DragEventArgs e)
        {
            if (_vc.AskUserForFileCategory() is file_cats category)
            {
                String[] myFiles = (String[])e.Data.GetData(System.Windows.DataFormats.FileDrop);
                if (myFiles != null && myFiles.Length != 0)
                {
                    _vc.UploadFiles(myFiles.ToList(), category);
                }
            }
        }
        private void BiAddFile_ItemClick(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            _vc.UploadFilesWithSelection();
        }
        private void BiAddNote_ItemClick(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            _vc.UploadNote();
        }
        private void GridColumnDescription_Validate(object sender, GridCellValidationEventArgs e)
        {
            if (e.Row is FileItem file)
            {
                file.Description = e.Value as String;
                _vc.EditFileItemDescription(file);
            }
        }
        private void LabelIcon_MouseDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                _vc.LabelsEdit((Image)sender);
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.Message, "Fout", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void BiExport_ItemClick(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            this._vc.EmailSelectedFile();
        }
        private void EmailFile_Click(object sender, RoutedEventArgs e)
        {
            this._vc.EmailSelectedFile();
        }

        /** METHODS */
        public int? GetNumberOfItems()
        {
            return _vc.Items.Count;
        }

        private void BiAddExcel_ItemClick(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            _vc.UploadExcel();
        }
    }
}
