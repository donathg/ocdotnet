﻿using ClaraFey.CommonObjects;
using sharedcode.BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ClaraFey.Windows.Common
{
    /// <summary>
    /// Interaction logic for AdresWindow.xaml
    /// </summary>
    public partial class AdressenWindow : Window
    {
        public AdressenListSettings Settings { get; set; }

        public AdressenWindow()
        {
            InitializeComponent();
            
            GlobalMethods.SetTheme(this, GlobalMethods.Themes.clientdossier);
        }

        private void MyUC_Loaded(object sender, RoutedEventArgs e)
        {
            myUC.Init(Settings);
        }
    }
}
