﻿
using ClaraFey.CommonObjects;
using ClaraFey.ViewControllers;
using DevExpress.Xpf.Editors.Settings;
using DevExpress.Xpf.Grid;
using sharedcode.BLL;
using sharedcode.common;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ClaraFey.Windows.Common
{
    /// <summary>
    /// Interaction logic for SmmLeverancieWindow.xaml
    /// </summary>
    public partial class SMMWindow2 : Window
    {
        /** PROPERTIES */
        private smmWindowVC _vc;
        
        /** CONSTRUCTOR */
        public SMMWindow2(smmWindowVC vc, SMMWindow2Settings settings)
        {
            _vc = vc;
            DataContext = vc;
            _vc.Settings = settings;

            InitializeComponent();

            GlobalMethods.SetTheme(this, settings.theme);
            this.Title = settings.WindowTitle;

            myUC.DataContext = vc;
            myUC.parentWindow = this;

            /// adjust visibility buttonbar according to settings
            if (settings.IsChoiceWindow)
            {
                MainPanel.Margin = new Thickness(5);
                ButtonBar.Visibility = Visibility.Visible;
                btnCancel.Visibility = Visibility.Visible;
                btnOK.Visibility = Visibility.Visible;
            }
            if (settings.AddNewButton)
            {
                MainPanel.Margin = new Thickness(5);
                ButtonBar.Visibility = Visibility.Visible;
                btnNew.Visibility = Visibility.Visible;
            }
        }

        /** EVENTS */
        private void SMMWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            //Gevaar : wanneer een rij geediteerd wordt maar niet wordt gecommit door tab of enter
            //en dan het kruisje wordt gekllikt om het scherm te sluiten.
            //CommitEditing() oproepen waarna de validaterow event wordt aangeroepen.
            smmWindowVC vc  = myUC.DataContext as smmWindowVC;
            if (vc != null)
            {
                for (int level = 0; level < vc.Settings.LevelCount; level++)
                {
                    GridControl grid = myUC.GetGrid(level);
                    if (grid.View.IsEditing)
                    {
                        //Nodig om de wijzigingen te committen wannneer op het sluiten kruisje wordt gedrukt
                        //Zo wordt de validate row method aangeroepen die op zijn beurt saved
                        grid.View.CommitEditing();
                    }
                }
                vc.Dispose();
            }
        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            CloseWindow(true);
        }
        private void ButtonCancel_Click(object sender, RoutedEventArgs e)
        {
            CloseWindow(false);
        }
        private void ButtonNew_Click(object sender, RoutedEventArgs e)
        {
            _vc.SelectedItem = null;
            CloseWindow(true);
        }

        /** METHODS */
        private void CloseWindow(bool dialogResult)
        {
            this.DialogResult = dialogResult;
            this.Close();
        }
        public void SimulateOKButton()
        {
            CloseWindow(true);
        }
    }
}
