﻿using ClaraFey.CommonObjects;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ClaraFey.Windows.Common
{
    /// <summary>
    /// Interaction logic for RichEditWindow.xaml
    /// </summary>
    public partial class RichEditWindow : Window
    {

        public Byte[] Data { get;set; }
        public RichEditWindow(Byte[] data, bool readOnly,GlobalMethods.Themes theme)
        {
            InitializeComponent();
           
            GlobalMethods.SetTheme(this, theme);

            if (data != null)
            {
                System.IO.MemoryStream str = new System.IO.MemoryStream(data);
                this.richEditControl1.LoadDocument(str, DevExpress.XtraRichEdit.DocumentFormat.Rtf);
            }
            if (readOnly)
            {
                richEditControl1.ReadOnly = true;
                ribbonControl1.Visibility = Visibility.Collapsed;
            }
            this.Width = SystemParameters.PrimaryScreenWidth / 100 * 90;
            this.Height = SystemParameters.PrimaryScreenHeight / 100 * 90;
            this.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            this.Closing += RichEditWindow_Closing;
        }

        private void RichEditWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            this.DialogResult = true;
            System.IO.MemoryStream str = new System.IO.MemoryStream();
            this.richEditControl1.SaveDocument(str, DevExpress.XtraRichEdit.DocumentFormat.Rtf);
            str.Seek(0, System.IO.SeekOrigin.Begin);
            Data = str.ToArray();
        }

        private void Window_PreviewKeyDown(object sender, KeyEventArgs e)
        {

        }

        private void richEditControl1_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.S && Keyboard.Modifiers == ModifierKeys.Control)
            {
                e.Handled = true;
            }
        }
    }
}
