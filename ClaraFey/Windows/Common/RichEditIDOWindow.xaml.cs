﻿using ClaraFey.CommonObjects;
using DevExpress.Office.Utils;
using DevExpress.XtraRichEdit.API.Native;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using System.ComponentModel;
using sharedcode.BLL;
using sharedcode.DAL;
using sharedcode.WeergaveClasses;
using ClaraFey.CommonObjects.WordParsers;

namespace ClaraFey.Windows.Common
{
    /// <summary>
    /// Interaction logic for RichEditWindow.xaml
    /// </summary>
    public partial class RichEditIDOWindow : Window
    {

        IWordParser _wordParser;
        /** FIELDS */
        public Byte[] Data { get; set; }
        private bewoner _selectedBewoner;
        public bewoner SelectedBewoner
        {
            get
            {
                return _selectedBewoner;
            }
            set
            {
                _selectedBewoner = value;

                RefreshTemplate();
                InputBewonerInfoIntoDocfile();
            }
        }
        public List<bewoner> BewonerList { get; set; } = new List<bewoner>();

        BackgroundWorker backgroundWorker = new BackgroundWorker();

        /** CONSTRUCTORS */
        public RichEditIDOWindow(IWordParser wordParser, GlobalMethods.Themes theme)
        {
            _wordParser = wordParser;

            BewonerList.AddRange(BewonersDAL.SelectAllBewoners().OrderBy(x=> x.AchternaamVoornaam));

            this.DataContext = this;

            InitializeComponent();

       //     backgroundWorker.DoWork += BackgroundWorker_DoWork;
        //    backgroundWorker.RunWorkerCompleted += BackgroundWorker_RunWorkerCompleted;
            backgroundWorker.RunWorkerAsync();

            RefreshTemplate();
            this.richEditControl1.MouseDoubleClick += RichEditControl1_MouseDoubleClick;
            GlobalMethods.SetTheme(this, theme);
 
            this.Width = SystemParameters.PrimaryScreenWidth / 100 * 90;
            this.Height = SystemParameters.PrimaryScreenHeight / 100 * 90;
            this.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            this.Closing += RichEditWindow_Closing;
            GlobalMethods.SetTheme(this, GlobalMethods.Themes.clientdossier);
        }

        /** EVENTS */
     /*   private void BackgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
           if(e.Result is MemoryStream m)
           {

                this.richEditControl1.Document = m;
            }
        }

        private void BackgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
        MemoryStream m =     LoadDocument.LoadDocument(System.AppDomain.CurrentDomain.BaseDirectory + "\\Templates\\idotemplate.docx");
            e.Result = m;
        }
        */
        private void RichEditControl1_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            System.Windows.Point point = e.GetPosition(richEditControl1);
            System.Drawing.Point docPoint = new System.Drawing.Point((int)point.X, (int)point.Y);
            docPoint = Units.PixelsToDocuments(docPoint, richEditControl1.DpiX, richEditControl1.DpiY);
            DocumentPosition pos = richEditControl1.GetPositionFromPoint(docPoint);
            String text = "";
            if (pos != null)
                text = System.String.Format("Mouse is over position {0}", pos);
            MessageBox.Show($"Position {text}");
        }
        private void RichEditWindow_Closing(object sender, CancelEventArgs e)
        {
            /*this.DialogResult = true;
            System.IO.MemoryStream str = new System.IO.MemoryStream();
            this.richEditControl1.SaveDocument(str, DevExpress.XtraRichEdit.DocumentFormat.Rtf);
            str.Seek(0, System.IO.SeekOrigin.Begin);
            Data = str.ToArray();*/
        }
        private void richEditControl1_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.S && Keyboard.Modifiers == ModifierKeys.Control)
            {
                e.Handled = true;
            }
        }
        private void SaveAsButton_Click(object sender, RoutedEventArgs e)
        {
            this.richEditControl1.SaveDocumentAs();
        }

        /** METHODS */
        private void RefreshTemplate()
        {
            richEditControl1.LoadDocument(_wordParser.WordDocumentLocation);
            _wordParser.WordDocument = richEditControl1.Document;
        }
        private void InputBewonerInfoIntoDocfile()
        {
            try
            {
                _wordParser.DoAllInserts(SelectedBewoner.rijksregisternr);

                System.IO.MemoryStream stream = new System.IO.MemoryStream();

                if (stream != null)
                {
                    _wordParser.WordDocument.SaveDocument(stream, DevExpress.XtraRichEdit.DocumentFormat.Doc);
                    stream.Seek(0, System.IO.SeekOrigin.Begin);
                }
                richEditControl1.LoadDocument(stream);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
