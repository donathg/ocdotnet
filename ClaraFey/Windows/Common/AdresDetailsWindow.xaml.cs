﻿using ClaraFey.CommonObjects;
using ClaraFey.ViewControllers;
using ClaraFey.ViewControllers.ClientDossier;
using DevExpress.Xpf.Editors;
using DevExpress.Xpf.Grid;
using sharedcode.BLL;
using sharedcode.Caches;
using sharedcode.WeergaveClasses;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ClaraFey.Windows.Common
{
    /// <summary>
    /// Interaction logic for adresWindow.xaml
    /// </summary>
    public partial class AdresDetailsWindow : Window
    {
        /** PROPERTIES */
        public AdresDetailsVC Vc { get; set; }

        /** CONSTRUCTORS */
        public AdresDetailsWindow(bewoner livein)
        {
            GlobalMethods.SetTheme(this, GlobalMethods.Themes.clientdossier);
            GlobalMethods.SetSizeAndCenterWindow(this, 90);
            Title = "Adres aanpassen";

            Vc = new AdresDetailsVC(livein);
            this.DataContext = Vc;

            InitializeComponent();
        }
        public AdresDetailsWindow(gebruiker personeel)
        {
            GlobalMethods.SetTheme(this, GlobalMethods.Themes.clientdossier);
            GlobalMethods.SetSizeAndCenterWindow(this, 90);
            Title = "Adres aanpassen";

            Vc = new AdresDetailsVC(personeel);
            this.DataContext = Vc;

            InitializeComponent();
        }
        public AdresDetailsWindow(locatie campus)
        {
            GlobalMethods.SetSizeAndCenterWindow(this, 90);
            GlobalMethods.SetTheme(this, GlobalMethods.Themes.clientdossier);
            Title = "Campus aanpassen";

            Vc = new AdresDetailsVC(campus);
            this.DataContext = Vc;

            InitializeComponent();
        }

        /** EVENTS */
        private void BtnEditCompany_ItemClick(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            try
            {
                if (MessageBox.Show("Opgelet : dit adres wordt gedeeld onder de collega's ./n/rWil je toch wijzigingen aan het adres doorvoeren ?", "Adres wijzigen", MessageBoxButton.OKCancel, MessageBoxImage.Question) == MessageBoxResult.OK)
                    Vc.CanEditAdres = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Foutmelding", MessageBoxButton.OK, MessageBoxImage.Error);
            }
}
        private void BtnPostalCodeLookup_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                List<SMMWindow2ComboItem> listCountries = new List<SMMWindow2ComboItem>();
                foreach (country c in Vc.LandList)
                {
                    listCountries.Add(new SMMWindow2ComboItem() { Id = c.code, Name = c.name });
                }

                SMMWindow2Settings settings = new SMMWindow2Settings("Postcodelijst België en Nederland", GlobalMethods.Themes.clientdossier)
                {
                    IsChoiceWindow = true
                };
                SMMWindow2Level level0 = new SMMWindow2Level(0, "Postcodelijst België en Nederland ") { AllowDelete = false, AllowModify = false, AllowAdd = false, AllowSearch = true, AllowFilter = true };
                level0.AddColumns(
                    new SMMWindow2Column() { FieldName = "country", ColumnHeader = "Land", ComboboxItems = listCountries, IsEditable = false },
                    new SMMWindow2Column() { FieldName = "postal1", ColumnHeader = "Postcode", IsEditable = false },
                    new SMMWindow2Column() { FieldName = "place", ColumnHeader = "Plaats", IsEditable = false },
                    new SMMWindow2Column() { FieldName = "gewest", ColumnHeader = "Gewest", IsEditable = false },
                    new SMMWindow2Column() { FieldName = "provincie", ColumnHeader = "Provincie", IsEditable = false }
                );

                smmWindowVC vc = new smmWindowVC();
                level0.DataSource = new ObservableCollection<postal>(vc.Entity.Set<postal>().ToList());
                settings.AddLevel(level0);

                SMMWindow2 w = new SMMWindow2(vc, settings) {
                    WindowStartupLocation = WindowStartupLocation.CenterScreen
                };

                if (w.ShowDialog().GetValueOrDefault())
                {
                    Vc.SetPostalData((postal)vc.SelectedItem0);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Foutmelding", MessageBoxButton.OK, MessageBoxImage.Error);
            }
}
        private void BarButtonNew_ItemClick(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            try
            {
                AdressenVC aiAdresssenVC = new AdressenVC(null);
                adrestype gekozenAdressubtype = aiAdresssenVC.SelectAdrestypePopup(Vc.Adres);

                if (gekozenAdressubtype != null && gekozenAdressubtype.id != 0)
                    Vc.AddBewonerAdrestype(gekozenAdressubtype);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Foutmelding", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void BtnOk_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Vc.Save();
                DialogResult = true;
                Close();
            }
            catch (DbEntityValidationException)
            {
                MessageBox.Show("Niet alle verplichte velden zijn ingevuld. Gelieve deze in te vullen en opnieuw te proberen.", "Foutmelding", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("UNIQUE"))
                    MessageBox.Show("Dit adres met type: " + Vc.Adres.Type.naam + " bestaat al voor deze bewoner", "Fout", MessageBoxButton.OK, MessageBoxImage.Error);
                else
                    MessageBox.Show(ex.Message, "Fout", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {
            Vc.Adres = null;
            DialogResult = false;
            Close();
        }
        private void BtnDeleteSubClick(object sender, RoutedEventArgs e)
        {
            try
            {
                Vc.DeleteContactpersoon();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Foutmelding", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void Contactpersonen_CellValueChanged(object sender, CellValueChangedEventArgs e)
        {
            try
            {
                if (e.Cell.Property == "HasPresentBewoner" && e.Cell.Value is bool val && !val)
                    Vc.DeleteMyContactpersoon();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Foutmelding", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void VoornaamTextedit_EditValueChanged(object sender, EditValueChangedEventArgs e)
        {
            try
            {
                if (Vc.IsContactpersoonInfoVisible)
                {
                    ObservableCollection<ContactpersoonWeergave> tempList = this.gridControlBewonerAdresType.ItemsSource as ObservableCollection<ContactpersoonWeergave>;

                    if (sender is TextEdit editBox)
                        tempList.Where(x => x.id == Vc.SelectedContactpersoon.id).SingleOrDefault().voornaam = editBox.Text;

                    this.gridControlBewonerAdresType.ItemsSource = null;
                    this.gridControlBewonerAdresType.ItemsSource = tempList;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Foutmelding", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void NaamTextedit_EditValueChanged(object sender, EditValueChangedEventArgs e)
        {
            if (Vc.IsContactpersoonInfoVisible)
            {
                ObservableCollection<ContactpersoonWeergave> tempList = this.gridControlBewonerAdresType.ItemsSource as ObservableCollection<ContactpersoonWeergave>;

                if (sender is TextEdit editBox)
                {
                    tempList.Where(x => x.id == Vc.SelectedContactpersoon.id).SingleOrDefault().naam = editBox.Text;
                }

                this.gridControlBewonerAdresType.ItemsSource = null;
                this.gridControlBewonerAdresType.ItemsSource = tempList;
            }
        }
        private void GridControlBewonerAdresType_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                if (sender is TableView view)
                {
                    TableViewHitInfo hitInfo = view.CalcHitInfo(e.GetPosition(this.gridControlBewonerAdresType));
                    if (!hitInfo.InColumnHeader)
                        Vc.ToggleVisibilityContactpersoonInfo();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Foutmelding", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void BtnAskForBewoner_Click(object sender, RoutedEventArgs e)
        {
        

            try
            {
                List<SMMWindow2ComboItem> listCountries = new List<SMMWindow2ComboItem>();
                foreach (country c in Vc.LandList)
                {
                    listCountries.Add(new SMMWindow2ComboItem() { Id = c.code, Name = c.name });
                }

                SMMWindow2Settings settings = new SMMWindow2Settings("Cliënten van Clara Fey", GlobalMethods.Themes.clientdossier)
                {
                    IsChoiceWindow = true
                };
                SMMWindow2Level level0 = new SMMWindow2Level(0, "Cliënten van Clara Fey") { AllowDelete = false, AllowModify = false, AllowAdd = false, AllowSearch = true, AllowFilter = true };
                level0.AddColumns(

                    new SMMWindow2Column() { FieldName = "code", ColumnHeader = "Code", IsEditable = false },
                    new SMMWindow2Column() { FieldName = "voornaam", ColumnHeader = "Voornaam", IsEditable = false },
                    new SMMWindow2Column() { FieldName = "achternaam", ColumnHeader = "Achternaam", IsEditable = false },
                    new SMMWindow2Column() { FieldName = "rijksregisternr", ColumnHeader = "Rijksregisternr", IsEditable = false }
                    
                );
              
                 
                 
             
                smmWindowVC vc = new smmWindowVC();
                level0.DataSource = BewonersListCache.Instance.AllBewonersList;
                settings.AddLevel(level0);

                SMMWindow2 w = new SMMWindow2(vc, settings)
                {
                    WindowStartupLocation = WindowStartupLocation.CenterScreen
                };

                if (w.ShowDialog().GetValueOrDefault())
                {
                    Vc.SetBewonerData((bewoner)vc.SelectedItem0);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Foutmelding", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
