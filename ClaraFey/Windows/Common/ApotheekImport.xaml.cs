﻿using ClaraFey.CommonObjects;
using ClaraFey.ViewControllers;
using DevExpress.Spreadsheet;
using DevExpress.Xpf.Data;
using DevExpress.Xpf.Spreadsheet;
using sharedcode.BLL;
using sharedcode.Caches;
using sharedcode.common;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ClaraFey.Windows.Common
{
    /// <summary>
    /// Interaction logic for RichEditWindow.xaml
    /// </summary>
    public partial class ApotheekImport : Window
    {
        private List<ApotheekImportError> ApotheekImportErrors = new List<ApotheekImportError>();
        public ApotheekImport()
        {
            InitializeComponent();
            GlobalMethods.SetTheme(this, GlobalMethods.Themes.clientdossier);
            this.Width = SystemParameters.PrimaryScreenWidth / 100 * 90;
            this.Height = SystemParameters.PrimaryScreenHeight / 100 * 90;
            this.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            this.Closing += RichEditWindow_Closing;
        }

        private void RichEditWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {


        }


        private void Window_PreviewKeyDown(object sender, KeyEventArgs e)
        {

        }

        private void richEditControl1_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.S && Keyboard.Modifiers == ModifierKeys.Control)
            {
                e.Handled = true;
            }
        }

        private void About_ItemClick(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {

        }



        private int FindLastRow(Worksheet ws, int colToCheck = 0)
        {

            //5 legen rijen achter elkaar == einde
            for (int row = 5; row < ws.Cells.RowCount; row++)
            {
                if (String.IsNullOrWhiteSpace(ws[row, colToCheck].Value.ToString())
                    && String.IsNullOrWhiteSpace(ws[row - 1, colToCheck].Value.ToString())
                    && String.IsNullOrWhiteSpace(ws[row - 2, colToCheck].Value.ToString())
                     && String.IsNullOrWhiteSpace(ws[row - 3, colToCheck].Value.ToString())
                      && String.IsNullOrWhiteSpace(ws[row - 4, colToCheck].Value.ToString())

                    )

                {

                    return row;
                }

            }
            return ws.Cells.RowCount;
        }




        static List<bewoner> allBewoners = null;
        private bewoner GetBewoner(String naam, String sheetName)
        {

            /*if (naam.ToLower()== "immens, kian")
            {

            }*/

            if (String.IsNullOrWhiteSpace(naam))
                return null;

            if (allBewoners == null)
            {
                using (Entities entity = new Entities(GlobalData.Instance.Entity))
                {
                    allBewoners = entity.bewoner.ToList();
                }
            }
            Decimal rkr;
            if (Decimal.TryParse(naam, out rkr))
            {
                bewoner b = allBewoners.Where(x => x.rijksregisternr == rkr.ToString()).FirstOrDefault();
                return b;
            }
            bewoner bewoner = null;
            String[] nameParts = naam.Split(',');
            if (nameParts.Length != 2)
            {
                ApotheekImportErrors.Add(new ApotheekImportError() { Error = "naam bestaat niet uit 2 delen op D1", Sheet = sheetName });

                return null;
            }
            String LastName = nameParts[0].ToUpper().Trim();
            String[] firstNames = nameParts[1].Trim().Split(' ');
            String FirstName = firstNames[0].ToUpper().Trim();
            foreach (bewoner b in allBewoners)
            {
                if (b.voornaam.Trim().ToUpper() == FirstName && b.achternaam.Trim().ToUpper() == LastName)
                {
                    return b;
                }
                if (firstNames.Length==2 && b.achternaam.Trim().ToUpper() == LastName && firstNames[0].Trim().ToUpper() + " " + firstNames[1].Trim().ToUpper() == b.voornaam.Trim().ToUpper())
                {
                    return b;
                }
            }


          //  bewoner t = allBewoners.Where(x => x.rijksregisternr == "08061233727").FirstOrDefault();

            return null;
        }
        private void CsvExportClick(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            List<ApotheekImportLine> lines = new List<ApotheekImportLine>();

            foreach (Worksheet ws in this.spreadsheet.Document.Worksheets)
            {
                bool addedLineWS = false;
                for (int row = 0; row < 1000; row++)
                {
                    if (ws[row, 10].Value.IsEmpty == false && ws[row, 11].Value.IsEmpty==false && ws[row, 12].Value.IsEmpty==false && ws[row, 13].Value.IsEmpty == false)
                    {
                        ApotheekImportLine line = new ApotheekImportLine();
                        line.RijksregisterNummer = ws[row, 10].Value.TextValue;
                        line.Datum = ws[row, 11].Value.DateTimeValue;
                        line.Omschrijving = ws[row, 12].Value.TextValue;
                        line.Bedrag = ws[row, 13].Value.NumericValue;
                        line.Ondersteuningsvorm = ws[row, 14].Value.TextValue;
                        lines.Add(line);
                        addedLineWS = true;
                    }
                }
                //laatste totaal lijn verwijderen
           /*     if (addedLineWS)
                {
                    lines.RemoveAt(lines.Count - 1);
                }*/
            }
            ShowCvsExportableGrid(lines);

        }

        public void ShowCvsExportableGrid(List<ApotheekImportLine> lines)
        {
            string title = "Apotheek import - CSV " + DateTime.Now.ToString("dd-MM-yyyy");

            SMMWindow2Level level0 = new SMMWindow2Level(0, title)
            {
                AllowDelete = false,
                AllowModify = false,
                AllowAdd = false,
                AllowSearch = true,
                AllowFilter = false,
                CanExportToExcel = true,
                AllowGroup = true,
            };
            level0.AddColumns(
                new SMMWindow2Column() { FieldName = "RijksregisterNummer", ColumnHeader = "Rijksregisternummer", IsEditable = false },
                new SMMWindow2Column() { FieldName = "DatumString", ColumnHeader = "Datum", IsEditable = false },
                new SMMWindow2Column() { FieldName = "FinancieleBewegingsCode", ColumnHeader = "FinancielbewegingsCode", IsEditable = false },
                new SMMWindow2Column() { FieldName = "Omschrijving", ColumnHeader = "Omschrijving", IsEditable = false },
                new SMMWindow2Column() { FieldName = "Eenheden", ColumnHeader = "Eenheden", IsEditable = false },
                new SMMWindow2Column() { FieldName = "Bedrag", ColumnHeader = "Eenheidsprijs", IsEditable = false },
                new SMMWindow2Column() { FieldName = "Bedrag", ColumnHeader = "Totaal", IsEditable = false }
            );
            level0.AddGroupSummary("Bedrag", DevExpress.Data.SummaryItemType.Sum, "Totaal {0:c2}");


            level0.DataSource = new ObservableCollection<ApotheekImportLine>(lines).ToList();
            SMMWindow2Settings settings = new SMMWindow2Settings(title, GlobalMethods.Themes.normal)
            {
                WindowSizePercentage = 75,
                theme = GlobalMethods.Themes.clientdossier
            };
            settings.AddLevel(level0);
            SMMWindow2 w = new SMMWindow2(new smmWindowVC(), settings);
            w.ShowDialog();
        }
        private void ParseClick2(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            try
            {
                this.Cursor = Cursors.Wait;

                foreach (Worksheet ws in this.spreadsheet.Document.Worksheets)
                {

                    List<ApotheekImportLine> lines = new List<ApotheekImportLine>();
                    bewoner bewoner = FindSheetBewoner(ws);
                    int? teBetalenColIndex = null;
                    int? naamColIndex = null;
                    if (bewoner != null)
                    {
                        int lastRowIndex = 1000;// FindLastRow(ws, 5/*Numeric kolom check*/);

                        DateTime datum = DateTime.Now;
                        for (int row = 0; row < lastRowIndex; row++)
                        {
                            if (teBetalenColIndex == null)
                            {
                                for (int col = 0; col < 20; col++)
                                {
                                    String ColumnHeader = ws.Rows[row][col].Value.ToString().Trim().ToLower();
                                    if (ColumnHeader == "te betalen bedrag")
                                    {
                                        teBetalenColIndex = col;
                                       // break;
                                    }
                                    if (ColumnHeader == "naam")
                                    {
                                        naamColIndex = col;
                                       // break;
                                    }
                                }
                            }
                            if (teBetalenColIndex == null)
                                continue;

                            if (!ws[row, teBetalenColIndex.Value].Value.IsNumeric)
                                continue;


 



                            ApotheekImportLine line = new ApotheekImportLine();

                            line.Bedrag = ws[row, teBetalenColIndex.Value].Value.NumericValue;
                            if (ws[row, 0].Value.IsDateTime)
                                datum = ws[row, 0].Value.DateTimeValue;

                            line.Datum = datum;
                            line.RijksregisterNummer = bewoner.rijksregisternr;
                            line.Ondersteuningsvorm = bewoner.ondersteuningsvorm;

                            if (naamColIndex.HasValue && ws[row, naamColIndex.GetValueOrDefault()].Value.IsText)
                                line.Omschrijving = ws[row, naamColIndex.GetValueOrDefault()].Value.TextValue.Trim();
                            else
                                line.Omschrijving = "";

                            if (String.IsNullOrWhiteSpace(line.Omschrijving))
                                continue;

                            ws[row, 10].SetValue(line.RijksregisterNummer);
                            DateTime dtPrevMonth = DateTime.Now.AddMonths(-1);
                            ws[row, 11].SetValue(new DateTime(dtPrevMonth.Year, dtPrevMonth.Month, DateTime.DaysInMonth(dtPrevMonth.Year, dtPrevMonth.Month)));
                            ws[row, 12].SetValue(line.Omschrijving);
                            ws[row, 13].Value = 0;
                            ws[row, 13].NumberFormat = "0.00";
                            ws[row, 13].Value = line.Bedrag;
                            ws[row, 13].Alignment.Horizontal = SpreadsheetHorizontalAlignment.Right;
                            ws[row, 14].Value = line.Ondersteuningsvorm;
                            lines.Add(line);
                        }
                    }

                    if (lines.Count <= 0 /*inclusief totaal*/)
                        ws.ActiveView.TabColor = System.Drawing.Color.Red;
                    else
                        ws.ActiveView.TabColor = System.Drawing.Color.Green;
                }

            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Wait;
            }
        }

        private bewoner FindSheetBewoner(Worksheet ws)
        {
            try
            {
                String RK = "";
                String naam = "";
                if (ws[0, 0].Value.IsText)
                    naam = ws[0, 0].Value.TextValue;

                else if (ws[0, 0].Value.IsNumeric)
                    naam = ws[0, 0].Value.NumericValue.ToString();

                if (String.IsNullOrWhiteSpace(naam))
                {
                    ApotheekImportErrors.Add(new ApotheekImportError() { Error = "geen naam gevonden op D1", Sheet = ws.Name });
                    return null;
                }

                bewoner b = GetBewoner(naam, ws.Name);

                if (b == null)
                {
                    ApotheekImportErrors.Add(new ApotheekImportError() { Error = "rijkregisternr van  " + naam + " niet gevonden. (nog actief ?)", Sheet = ws.Name });
                    return null;
                }
                else
                    return b;
            }
            catch (Exception ex)
            {
                ApotheekImportErrors.Add(new ApotheekImportError() { Error = ex.Message, Sheet = ws.Name });
                return null;
            }
        }
    }

    public class ApotheekImportLine
    {
        public String RijksregisterNummer { get; set; }

        public String Ondersteuningsvorm { get; set; }

        public DateTime Datum { get; set; }
        public String DatumString { get {

                return Datum.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                    }
            }
        public String Omschrijving { get; set; }
        public double Bedrag { get; set; }

        public String FinancieleBewegingsCode
        {
            get
            {

                if (Ondersteuningsvorm == "MFC")
                    return "APO";
                else return "APP";

            }
        }
        public int Eenheden { get; set; } = 1;

    }

    public class ApotheekImportError
    {
        public String Sheet { get; set; }

        public String Error { get; set; }

    }
}
