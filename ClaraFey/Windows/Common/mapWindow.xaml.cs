﻿using ClaraFey.ViewControllers.Common;
using DevExpress.Xpf.Map;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ClaraFey.Windows.Common
{
    /// <summary>
    /// Interaction logic for mapWindow.xaml
    /// </summary>
    public partial class MapWindow : Window
    {
        mapVC _vc;

        public MapWindow(mapSettings settings)
        {
            _vc = new mapVC(settings);
            InitializeComponent();
            this.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            this.WindowState = WindowState.Maximized;
            BingSearchDataProvider searchProvider = new BingSearchDataProvider()
            {
                BingKey = "ArwRHtMq0CttoSx2D0OZIt8lZnFiO-MkLTyUxkLH_daQskzUrL9f-QEQZ0c6YR4j"
            };
            searchProvider.SearchCompleted += SearchProvider_SearchCompleted;
            if (String.IsNullOrWhiteSpace(settings.SearchString))
                 searchProvider.Search("Heimeulenweg 6 + 2980 Zoersel + België");
            else
                searchProvider.Search(settings.SearchString);
        }

        private void SearchProvider_SearchCompleted(object sender, BingSearchCompletedEventArgs e)
        {
            mapControl1.ZoomLevel = 16;
            if (e.RequestResult.SearchRegion != null)
                mapControl1.CenterPoint = e.RequestResult.SearchRegion.Location;
        }
    }
}
