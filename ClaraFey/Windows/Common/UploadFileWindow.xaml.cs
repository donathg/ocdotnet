﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ClaraFey.ViewControllers.Common;
using ClaraFey.CommonObjects;
using static ClaraFey.CommonObjects.GlobalMethods;

namespace ClaraFey.Windows.Common
{
    /// <summary>
    /// Interaction logic for UploadFileWindow.xaml
    /// </summary>
    public partial class UploadFileWindow : Window
    {
        /** FIELDS */
        public int? NumberOfItems
        {
            get
            {
                 return myUC.GetNumberOfItems();
            }
        }
        public UploadFileVC BijlagenVC { get; set; }

        /** CONSTRUCTOR */
        /// <param name="uploadFileVC">sets the right environment for this window</param>
        public UploadFileWindow(UploadFileVC uploadFileVC, Themes theme)
        {
            BijlagenVC = uploadFileVC;
            this.DataContext = BijlagenVC;
            InitializeComponent();
            GlobalMethods.SetTheme(this, theme);
            this.Width = (int)SystemParameters.PrimaryScreenWidth / 100 * 90;
            this.Height = (int)SystemParameters.PrimaryScreenHeight / 100 * 90;
        }
    }
}
