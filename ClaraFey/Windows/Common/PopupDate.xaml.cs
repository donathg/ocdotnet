﻿using ClaraFey.CommonObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ClaraFey.Windows.Common
{
    /// <summary>
    /// Interaction logic for PopupDate.xaml
    /// </summary>
    public partial class PopupDate : Window
    {
       public DateTime? Result { get; set; }

        public PopupDate(String title,DateTime defaultValue, bool enableTimeEdit,GlobalMethods.Themes theme)
        {
          
            InitializeComponent();
            this.Title = title;
            dateTimePicker.SelectedDate = defaultValue;
            GlobalMethods.SetTheme(this, theme);
        }

        private void btnOK_Click(object sender, RoutedEventArgs e)
        {
            this.Result = dateTimePicker.SelectedDate.GetValueOrDefault();
            DialogResult = true;
        }
    }
}
