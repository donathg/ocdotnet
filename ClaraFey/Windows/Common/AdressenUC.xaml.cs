﻿using ClaraFey.CommonObjects;
using ClaraFey.ViewControllers.ClientDossier;
using DevExpress.Xpf.Grid;
using sharedcode.BLL;
using sharedcode.WeergaveClasses;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using System.Windows.Threading;

namespace ClaraFey.Windows.Common
{
    /// <summary>
    /// Interaction logic for AIAdressen.xaml
    /// </summary>
    public partial class AdressenUC : UserControl
    {
        /** PROPERTIES */
        private AdressenVC _vc;

        /** CONSTRUCTORS */
        public AdressenUC()
        {
            InitializeComponent();
        }
        public AdressenUC(AdressenListSettings settings): this()
        {
            Init(settings);
        }

        /** EVENTS */
        public void Init(AdressenListSettings settings)
        {
            _vc = new AdressenVC(settings);
            DataContext = _vc;
        }
        private void AIAdressen_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key == Key.Delete)
                    DeleteAdres();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Foutmelding", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void BiRemoveAdres_ItemClick(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            try
            {
                DeleteAdres();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Foutmelding", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void NewBewonerAdres_ItemClick(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            try
            {
                if (_vc.SelectAdrestypePopup(null) is adrestype gekozenAdrestype)
                    ShowAddAdresWindow(_vc.SelectExistingAdresFromAdrestypePopup(gekozenAdrestype));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Foutmelding", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void BarButtonExportToExel_ItemClick(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            try
            {
                GlobalMethods.ExportGridToExcel(gridAdressen, "adressen_" + _vc.Bewoner.AchternaamVoornaam);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Foutmelding", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void Grid_CustomUnboundColumnData(object sender, GridColumnDataEventArgs e)
        {
            try
            {
                if (e.Column.FieldName == "IconUnboundAttachment" && e.IsGetData)
                    e.Value = new BitmapImage(new Uri("/images/Map_32x32.png", UriKind.Relative));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Foutmelding", MessageBoxButton.OK, MessageBoxImage.Error);
            }
}
        private void Grid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                AdresWeergave adres = _vc.SelectedAdres;
                if (adres != null)
                {
                    TableViewHitInfo hitInfo = ((TableView)gridAdressen.View).CalcHitInfo(e.OriginalSource as DependencyObject);

                    if (hitInfo.InRow)
                    {
                        if (hitInfo.Column.FieldName == "IconUnboundAttachment")
                        {
                            if (!String.IsNullOrWhiteSpace(adres.Fulladres))
                                GlobalMethods.StartBrowserGoogleMaps(adres.Fulladres, 5);
                        }
                        else
                            ShowAddAdresWindow(adres);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Foutmelding", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void BarButtonRefresh_ItemClick(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            try
            {
                _vc.FillContactpersonenAdresList();                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Foutmelding", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        /** METHODS */
        private void ShowAddAdresWindow(AdresWeergave adres)
        {
            AdresDetailsWindow w = new AdresDetailsWindow(_vc.Bewoner);
            if (adres != null)
                w.Vc.InsertSelectionIntoForm(adres);

            w.ShowDialog();

            _vc.FillContactpersonenAdresList();
        }
        private void DeleteAdres()
        {
            if (_vc.SelectedAdres != null)
            {
                if (MessageBox.Show(String.Format("Adres {0} verwijderen ?", _vc.SelectedAdres.naam), "Verwijderen", MessageBoxButton.YesNo, MessageBoxImage.Information) == MessageBoxResult.Yes)
                    _vc.DeleteSelectedAdres();
            }
        }

    }
}
