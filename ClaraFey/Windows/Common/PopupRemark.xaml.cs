﻿using ClaraFey.CommonObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ClaraFey.Windows.Common
{
    /// <summary>
    /// Interaction logic for PopupRemark.xaml
    /// </summary>
    public partial class PopupRemark : Window
    {
        public String Opmerking
        {
            get
            {
                return txtOpmerking.Text;
            }
               set
            {
                txtOpmerking.Text = value;

            }
        }
        public PopupRemark(String windowTitle, String acceptButtonText,String opmerkingDefaultValue, GlobalMethods.Themes theme)
        {
            InitializeComponent();
            GlobalMethods.SetTheme(this, theme);
            Opmerking = opmerkingDefaultValue;
            btnOK.Content = acceptButtonText;
            this.Title = windowTitle;
        }

        private void btnOK_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
            this.Close();

        }
    }
}
