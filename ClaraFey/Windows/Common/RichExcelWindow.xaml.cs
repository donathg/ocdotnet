﻿using ClaraFey.CommonObjects;
using DevExpress.Spreadsheet;
using DevExpress.Xpf.Spreadsheet;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ClaraFey.Windows.Common
{
    /// <summary>
    /// Interaction logic for RichEditWindow.xaml
    /// </summary>
    public partial class RichExcelWindow : Window
    {

        public Byte[] Data
        {
            get;set;
        }
        public RichExcelWindow(Byte[] data, bool readOnly,GlobalMethods.Themes theme)
        {
            InitializeComponent();
           
            GlobalMethods.SetTheme(this, theme);

            if (data != null)
            {
                System.IO.MemoryStream str = new System.IO.MemoryStream(data);
                this.richEditControl1.LoadDocument(str.ToArray());
            }
            if (readOnly)
            {
                richEditControl1.ReadOnly = true;
                //ribbonControl1.Visibility = Visibility.Collapsed;
            }
            this.Width = SystemParameters.PrimaryScreenWidth / 100 * 90;
            this.Height = SystemParameters.PrimaryScreenHeight / 100 * 90;
            this.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            this.Closing += RichEditWindow_Closing;
        }

        private void RichEditWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {

            using (MemoryStream ms = new MemoryStream())
            {
                richEditControl1.SaveDocument(ms, DocumentFormat.OpenXml);
                SpreadsheetDocumentSource ModelDocumentSourceStream = new SpreadsheetDocumentSource(ms, DocumentFormat.OpenXml);
                this.DialogResult = true;
                // your custom code here for saving the stream to the data base
                ModelDocumentSourceStream.Stream.Seek(0, System.IO.SeekOrigin.Begin);
                Data = ReadFully(ModelDocumentSourceStream.Stream);
            }
        }
        public static byte[] ReadFully(Stream input)
        {
            byte[] buffer = new byte[16 * 1024];
            using (MemoryStream ms = new MemoryStream())
            {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }
        private void Window_PreviewKeyDown(object sender, KeyEventArgs e)
        {

        }

        private void richEditControl1_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.S && Keyboard.Modifiers == ModifierKeys.Control)
            {
                e.Handled = true;
            }
        }
    }
}
