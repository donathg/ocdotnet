﻿using sharedcode.BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ClaraFey.Windows.Common
{
    /// <summary>
    /// Interaction logic for frmMaintenanceMessageWindow.xaml
    /// </summary>
    public partial class frmMaintenanceMessageWindow : Window
    {
        public maintenance_message Message { get; set; }
        public String ShutDownText { get; set; }
        public bool DoNotShowMesssage { get; set; }

        public frmMaintenanceMessageWindow(maintenance_message message, bool hideCheckBox)
        {
            this.Message = message;
            this.DataContext = this;
            ShutDownText = "Tijdstip : " + new DateTime(message.timeShutDown.Ticks).ToString("HH:mm") + " uur." ; 
            InitializeComponent();
            if (hideCheckBox)
                chkDoNotShow.Visibility = Visibility.Collapsed;


        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            DoNotShowMesssage = chkDoNotShow.IsChecked.GetValueOrDefault();
            this.Close();
        }
    }
}
