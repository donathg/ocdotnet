﻿using ClaraFey.CommonObjects;
using ClaraFey.ViewControllers;
using DevExpress.Data;
using DevExpress.Xpf.Bars;
using DevExpress.Xpf.Editors.Settings;
using DevExpress.Xpf.Grid;
using DevExpress.XtraPrinting;
using sharedcode.Extension_Methods;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ClaraFey.Windows.Common
{
    /// <summary>
    /// Interaction logic for SMMUserControl.xaml
    /// </summary>
    public partial class SMMWindowUserControl : UserControl
    {
        /** PROPERTIES */
        #pragma warning disable 414
        static bool _closeCancelled = false;
        static bool _cancelLevel0Selection = false;
        #pragma warning restore 414

        public SMMWindow2 parentWindow { get; set; }
        private Dictionary<string, object> customColumnDisplayText = new Dictionary<string, object>();

        /** CONSTRUCTORS */
        public SMMWindowUserControl()
        {
            InitializeComponent();

            Grid0.Tag = 0;
            Grid0.CustomColumnDisplayText += Grid0_CustomColumnDisplayText;
            Grid1.Tag = 1;
            this.Loaded -= SmmWindow_Loaded;
            this.Loaded += SmmWindow_Loaded;
        }

        /** EVENTS */
        private void SmmWindow_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= SmmWindow_Loaded;

            smmWindowVC vc = getVC();
            if (vc == null || vc.Settings == null)
                return;

            if (vc.Settings.LevelCount < 2)
                panelInfo1.Visibility = Visibility.Hidden;
          
            if (vc.Settings.Levels[0] is SMMWindow2LevelTree)
            {
                panelInfo0.Visibility = Visibility.Hidden;
                panelInfo0Tree.Visibility = Visibility.Visible;
                if (vc.Settings.LevelCount < 2)
                {
                    panelInfo0Tree.ItemWidth = new GridLength(100, GridUnitType.Star);
                }
                else
                {
                    panelInfo0Tree.ItemWidth = new GridLength(100, GridUnitType.Star);
                    panelInfo1.ItemWidth = new GridLength(100, GridUnitType.Star);
                }
            }
            else
            {
                panelInfo0.Visibility = Visibility.Visible;
                panelInfo0Tree.Visibility = Visibility.Hidden;
                if (vc.Settings.LevelCount < 2)
                {
                    panelInfo0.ItemWidth = new GridLength(100, GridUnitType.Star);
                }
                else
                {
                    panelInfo0.ItemWidth = new GridLength(100, GridUnitType.Star);
                    panelInfo1.ItemWidth = new GridLength(100, GridUnitType.Star);
                }
            }
            
            Menu.Visibility = Visibility.Collapsed;
            barButtonExportToExel1.IsVisible = barButtonExportToCsv.IsVisible= false;
            barButtonExportToExel2.IsVisible = false;
            bool showMenuBar = false;

            if (vc.Settings.LevelCount > 0)
            {
                InitGrid(0);
                if (vc.Settings.Levels[0].CanExportToExcel)
                {
                    barButtonExportToExel1.IsVisible = barButtonExportToCsv.IsVisible = true;
                    showMenuBar = true;
                }
            }
            if (vc.Settings.LevelCount > 1)
            {
                InitGrid(1);
                if (vc.Settings.Levels[1].CanExportToExcel)
                {
                    barButtonExportToExel2.IsVisible  = true;
                    showMenuBar = true;
                }
            }
            if (vc.Settings.ShowHelpButton)
            {
                biShowHelp.IsVisible = true;
                biShowHelp.Content = vc.Settings.HelpText;
                showMenuBar = true;
            }
            else
            {
                biShowHelp.IsVisible = false;
            }
            if (!showMenuBar)
                Menu.Visibility = Visibility.Collapsed;
            else
                Menu.Visibility = Visibility.Visible;
        }
        private void Grid0_CustomColumnDisplayText(object sender, CustomColumnDisplayTextEventArgs e)
        {
            if (customColumnDisplayText.ContainsKey(e.Column.FieldName))
            {
                Dictionary<String, String> translationList =( Dictionary < String, String>) customColumnDisplayText[e.Column.FieldName];

                e.DisplayText = translationList[e.Value.ToString()];
            }
        }
        private void grid0_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            smmWindowVC vc = getVC();

            vc.Settings.Levels[0].MouseDoubleClickMethod?.Invoke(vc.SelectedItem[0]);

            if (vc.Settings.IsChoiceWindow == false)
            {
                return;
            }
            TableViewHitInfo hitInfo = this.view0.CalcHitInfo(e.OriginalSource as DependencyObject);

            if (hitInfo.InRow)
            {
                if (vc.Settings.Levels[0].MouseDoubleClickMethod != null)
                    vc.Settings.Levels[0].MouseDoubleClickMethod(vc.SelectedItem[0]);
                else
                {
                    if (this.parentWindow != null && vc.SelectedItem != null)
                        this.parentWindow.SimulateOKButton();
                }
            }
        }
        private void Grid_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Delete && !Grid1.View.IsEditing)
            {
                GridControl g = sender as GridControl;
                int level = (int)g.Tag;
                if (g != null)
                {
                    if (MessageBox.Show("Zeker dat u deze rij wil verwijderen ?", "Verwijderen", MessageBoxButton.OKCancel, MessageBoxImage.Question) == MessageBoxResult.OK)
                    {
                        smmWindowVC vc = getVC();
                        Type t = vc.SelectedItem[level].GetType();
                        var dbSet = vc.Entity.Set(t);
                        dbSet.Remove(vc.SelectedItem[level]);
                        String errorMessage = Save();
                        if (String.IsNullOrWhiteSpace(errorMessage))
                        {
                            vc.Settings.Levels[level].AfterDeleteMethod?.Invoke(vc.SelectedItem[level]);
                            ((GridViewBase)g.View).DeleteRow(g.View.FocusedRowHandle);
                        }
                        else
                        {
                            MessageBox.Show("Er is een fout opgetreden bij het verwijderen. Waarschijnlijk wordt dit object ergens gebruikt", "Verwijderen", MessageBoxButton.OK, MessageBoxImage.Error);
                        }
                    }
                }
            }
            if (e.Key == Key.Escape)
            {
                GridControl g = sender as GridControl;
                int level = (int)g.Tag;
                if (level==1)
                {

                    GridControl parentGrid = GetGrid(0);
                    parentGrid.IsEnabled = true;
                }

            }
        }
        private void grid_CustomUnboundColumnData(object sender, GridColumnDataEventArgs e)
        {
            if (e.IsGetData)
            {
                smmWindowVC vc = getVC();
                GridControl g = sender as GridControl;
                if (g == null)
                    return;
                SMMWindow2Column col = vc.Settings.Levels[(int)g.Tag].GetColomnByFieldName(e.Column.FieldName);
                if (col != null)
                {
                    object row = ((IList)((GridControl)sender).ItemsSource)[e.ListSourceRowIndex];
                    if (row != null && col.UnboundGetValue != null)
                        e.Value = col.UnboundGetValue(row);
                }
            }
        }
        private void Grid_SelectedItemChanged(object sender, DevExpress.Xpf.Grid.SelectedItemChangedEventArgs e)
        {
            GridControl grid = sender as GridControl;
            FilterGrid((int)grid.Tag, GridType.Grid);
        }
        private void GridColumn_Validate(object sender, GridCellValidationEventArgs e)
        {
            smmWindowVC vc = getVC();
            GridColumn gc = sender as GridColumn;
            int level = (int)gc.Tag;
            List<SMMWindow2Column> cols = vc.Settings.Levels[level].Colums;
            
            foreach (SMMWindow2Column col in cols)
            {
                if (e.Column.FieldName == col.FieldName)
                {
                    if (col.ValidateMethod != null)
                    {
                        String errorMessage = col.ValidateMethod(col, e.Value, vc);
                        if (!String.IsNullOrWhiteSpace(errorMessage))
                        {
                            e.ErrorType = DevExpress.XtraEditors.DXErrorProvider.ErrorType.Critical;
                            e.ErrorContent = errorMessage;
                            e.IsValid = false;
                        }
                        else
                        {
                            if (e.Column.FieldName == col.FieldName && !col.IsNullable && (e.Value == null || String.IsNullOrWhiteSpace(e.Value.ToString())))
                            {
                                e.ErrorType = DevExpress.XtraEditors.DXErrorProvider.ErrorType.Critical;
                                e.ErrorContent = "Verplicht!";
                                e.IsValid = false;
                            }
                        }
                    }
                }
            }
        }
        private void TableView_ValidateRow(object sender, GridRowValidationEventArgs e)
        {
            if (sender is TableView g)
            {
                String errorCols = String.Empty;

                smmWindowVC vc = getVC();
                int level = (int)g.Grid.Tag;
                vc.Settings.Levels[level].BeforeUpdateMethod?.Invoke(e.Row);

                List<SMMWindow2Column> cols = vc.Settings.Levels[(int)g.Grid.Tag].Colums;
                foreach (SMMWindow2Column col in cols)
                {
                    if (col.UnboundType != SMMWindow2ColumnUnboundType.Bound)
                        continue;

                    Object o = GetPropValue(e.Row, col.FieldName);

                    if (!col.IsNullable && (o == null || String.IsNullOrWhiteSpace(o.ToString())))
                        errorCols += "\n\r" + col.ColumnHeader + " mag niet leeg zijn";
                }

                if (!string.IsNullOrWhiteSpace(errorCols))
                {
                    e.IsValid = false;
                    e.ErrorContent = "Volgende fouten zijn opgetreden :\n\r" + errorCols + "\n\r";
                    e.ErrorType = DevExpress.XtraEditors.DXErrorProvider.ErrorType.Critical;
                }
                if (e.IsValid)
                {
                    bool newEntry = false;
                    if (e.RowHandle == GridControl.NewItemRowHandle)
                        newEntry = true;
                    if (newEntry)
                    {
                        Type t = e.Row.GetType();
                        var dbSet = vc.Entity.Set(t);
                        dbSet.Add(e.Row);
                    }
                    String errorMessage = Save();
                    if (!String.IsNullOrWhiteSpace(errorMessage))
                    {
                        e.IsValid = false;
                        e.ErrorContent = "Volgende fout is opgetreden :\n\r" + errorMessage;
                        e.ErrorType = DevExpress.XtraEditors.DXErrorProvider.ErrorType.Critical;
                    }
                    else
                        vc.Settings.Levels[level].AfterUpdateMethod?.Invoke(e.Row);
                }
                else
                {
                    _closeCancelled = true;
                    if (level == 1)
                        _cancelLevel0Selection = true;
                }
            }
        }
        private void TableView_InvalidRowException(object sender, InvalidRowExceptionEventArgs e)
        {
            e.ExceptionMode = ExceptionMode.NoAction;
        }
        private void Tree0_SelectedItemChanged(object sender, SelectedItemChangedEventArgs e)
        {
            FilterGrid(0, GridType.Tree);
        }
        /// <summary>
        /// Wanneer een nieuwe rij wordt aangemaakt wordt deze event eerst aangeroepen
        /// Ideaal om default waarden in te vullen
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void view_InitNewRow(object sender, InitNewRowEventArgs e)
        {
            smmWindowVC vc = getVC();
            TableView g = sender as TableView;
            if (g == null)
                return;

            int level = (int)g.Grid.Tag;

            if (level >= vc.Settings.LevelCount)
            {
                return;
            }
            List<SMMWindow2Column> cols = vc.Settings.Levels[level].Colums;
            if (level > 0 && !String.IsNullOrWhiteSpace(vc.Settings.Levels[level].JoinField))
            {
                int parentLevel = level - 1;
                GridControl parentGrid = GetGrid(parentLevel);

                //de Foreign Key (Join) vullen met de Primary key uit level 0
                if (parentGrid.SelectedItem != null)
                {
                    object defaultValue = GetPropValue(parentGrid.SelectedItem, vc.Settings.Levels[parentLevel].JoinField);
                    if (defaultValue == null)
                    {
                        e.Handled = false;
                        return;
                    }
                    g.Grid.SetCellValue(e.RowHandle, vc.Settings.Levels[level].JoinField, defaultValue);
                }
                else
                {
                    e.Handled = false;
                    return;
                }
            }
            foreach (SMMWindow2Column col in cols)
            {
                if (col.DefaultValue != null)
                    g.Grid.SetCellValue(e.RowHandle, col.FieldName, col.DefaultValue);
            }
        }
        private void view_Loaded(object sender, RoutedEventArgs e)
        {
            smmWindowVC vc = getVC();
            TableView tableView1 = sender as TableView;
            int level = (int)tableView1.Grid.Tag;
            if (level >= vc.Settings.LevelCount)
            {
                return;
            }
            tableView1.BestFitMode = DevExpress.Xpf.Core.BestFitMode.VisibleRows;
            tableView1.BestFitColumns();

            if (vc.Settings.Levels[level].AllowAdd)
                tableView1.NewItemRowPosition = NewItemRowPosition.Bottom;
            else
                tableView1.NewItemRowPosition = NewItemRowPosition.None;

            if (vc.Settings.Levels[level].AllowSearch)
                tableView1.ShowSearchPanelMode = ShowSearchPanelMode.Always;
            else
                tableView1.ShowSearchPanelMode = ShowSearchPanelMode.Never;

            //     tableView1.AllowFilterEditor = vc.Settings.Levels[level].AllowFilter;
            if (vc.Settings.Levels[level].AllowFilter)
                tableView1.AllowFilterEditor = DevExpress.Utils.DefaultBoolean.True;
            else
                tableView1.AllowFilterEditor = DevExpress.Utils.DefaultBoolean.False;

            tableView1.ShowGroupPanel = vc.Settings.Levels[level].AllowGroup;

            tableView1.ShowFilterPanelMode = ShowFilterPanelMode.Never;

            FilterGrid(0, GridType.Grid);
        }
        private void view_ShowingEditor(object sender, ShowingEditorEventArgs e)
        {
            smmWindowVC vc = getVC();

            TableView tableView1 = sender as TableView;
            int level = (int)tableView1.Grid.Tag;
            if (level == 1)
            {
                GridControl g = GetGrid(0);
                g.IsEnabled = false;
            }
            
            if (level >= vc.Settings.LevelCount)
            {
                return;
            }
            foreach (SMMWindow2Column col in vc.Settings.Levels[level].Colums)
            {
                if (col.FieldName == e.Column.FieldName)
                {
                    if (e.RowHandle==DataControlBase.NewItemRowHandle && col.IsEditableInNewRow.HasValue)
                    {
                        if (col.IsEditableInNewRow.GetValueOrDefault() == false)
                        {
                            e.Cancel = true;
                            return;
                        }
                    }
                    else
                    {
                        if (col.IsEditable == false || vc.Settings.Levels[level].AllowModify==false)
                        {
                            e.Cancel = true;
                            return;
                        }
                    }
                }
            }
        }
        private void view_HiddenEditor(object sender, EditorEventArgs e)
        {
            smmWindowVC vc = getVC();
            TableView tableView1 = sender as TableView;
            int level = (int)tableView1.Grid.Tag;
            if (level == 1)
            {
                GridControl g = GetGrid(0);
                g.IsEnabled = true;
            }
        }
        private void view_PreviewKeyUp(object sender, KeyEventArgs e)
        {
            smmWindowVC vc = getVC();
            var tableView = sender as TableView;
            if ((e.Key == System.Windows.Input.Key.Up || e.Key == System.Windows.Input.Key.Down || e.Key == Key.Tab) && tableView.Grid.VisibleRowCount == 1)
            {
                Dispatcher.BeginInvoke(new Action(
                    () => { (sender as TableView).CommitEditing(); }
                ));
            }

        }
        private void view_MouseDown(object sender, MouseButtonEventArgs e)
        {
            var tableView = sender as TableView;
            var hitInfo = tableView.CalcHitInfo(Mouse.GetPosition(tableView));
            if (hitInfo.HitTest == TableViewHitTest.DataArea && tableView.Grid.VisibleRowCount == 1)
            {
                Dispatcher.BeginInvoke(new Action(() => {
                    (sender as TableView).CommitEditing();
                }));
            }
            if (hitInfo.HitTest == TableViewHitTest.Row && tableView.Grid.VisibleRowCount == 1)
            {
                Dispatcher.BeginInvoke(new Action(() => {
                    (sender as TableView).CommitEditing();
                }));
            }
        }
        private void ContextMenuItem_Click(object sender, RoutedEventArgs e)
        {
            smmWindowVC vc = getVC();
            MenuItem mi = sender as MenuItem;
            SMMWindow2ContextMenuItem itemInfo = mi.Tag as SMMWindow2ContextMenuItem;
            GridControl g = GetGrid(itemInfo.Level);
            itemInfo.ContextMenuItemClickedMethod?.Invoke(g.SelectedItem, vc, itemInfo);
        }
        private void barButtonExportToExel_ItemClick(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            BarButtonItem i = sender as BarButtonItem;
            GridControl g = null;
            String filename = "";
            if (i!=null)
            {
                smmWindowVC vc = getVC();
                if (i.Tag.ToString() == "0")
                {
                    g = Grid0;
                    filename = vc.Settings.Levels[0].Title;
                }
                if (i.Tag.ToString() == "1")
                {
                    g = Grid1;
                    filename = vc.Settings.Levels[0].Title;
                }
                if (g!=null)
                {
                    GlobalMethods.ExportGridToExcel(g, filename);
                }
            }
        }
        private void barButtonNew_ItemClick(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
        }
        private void biShowHelp_ItemClick(object sender, ItemClickEventArgs e)
        {
            smmWindowVC vc = getVC();
            HelpWindow w = new HelpWindow(vc.Settings.WindowTitle, "Help " + vc.Settings.WindowTitle);
            w.Show();
        }
        
        /** METHODS */
        private smmWindowVC getVC()
        {
            return (smmWindowVC)this.DataContext;
        }
        /// <summary>
        /// TreeView needs refactoring !!! TODO : Remove Duplicate C
        /// </summary>
        /// <param name="level"></param>
        private void InitGrid(int level)
        {
            smmWindowVC vc = getVC();
            if (vc.Settings.Levels[level] is SMMWindow2LevelTree)
            {
                Tree0.View.KeyFieldName = "id";
                Tree0.View.ParentFieldName = "parentid";
                foreach (SMMWindow2Column col in vc.Settings.Levels[level].Colums)
                {
                    TreeListColumn gcol = new TreeListColumn()
                    {
                        Tag = level,
                        FieldName = col.FieldName,
                        Header = col.ColumnHeader,
                        ReadOnly = true//For now a tree is always readonly, needs some programming to add this functionality
                    };
                    Tree0.Columns.Add(gcol);
                    if (col.Visible == false)
                    {
                        Tree0.Columns[col.FieldName].Visible = false;
                    }
                    if (col.IsComboBox)
                    {
                        Tree0.Columns[col.FieldName].EditSettings = new ComboBoxEditSettings() { ItemsSource = col.ComboboxItems, ValueMember = "Id", DisplayMember = "Name" };
                    }
                    if (col.IsCheckBox)
                    {
                        Tree0.Columns[col.FieldName].UnboundType = DevExpress.Data.UnboundColumnType.Boolean;
                        Tree0.Columns[col.FieldName].EditSettings = new CheckEditSettings();
                    }
                    if (col.IsPicture)
                    {
                        Tree0.Columns[col.FieldName].UnboundType = DevExpress.Data.UnboundColumnType.Object;
                        Tree0.Columns[col.FieldName].EditSettings = new ImageEditSettings();
                    }


                    if (col.EditSettings != null)
                    {
                        Tree0.Columns[col.FieldName].EditSettings = col.EditSettings;
                    }
                    if (vc.Settings.Levels[level].AllowFilter == false)
                    {
                        Tree0.Columns[col.FieldName].AllowColumnFiltering = DevExpress.Utils.DefaultBoolean.False;
                    }
                    if (col.customColumnDisplayText != null)
                    {
                        this.customColumnDisplayText.Add(col.FieldName, col.customColumnDisplayText);
                    }
                }
                Tree0.ItemsSource = vc.Settings.Levels[level].DataSource;
                Tree0.SelectedItemChanged -= Tree0_SelectedItemChanged;
                Tree0.SelectedItemChanged += Tree0_SelectedItemChanged;
                return;
            }

            GridControl grid = GetGrid(level);
            if (grid == null)
                return;
            if (level == 0)
                panelInfo0.Caption = vc.Settings.Levels[0].Title;
            if (level == 1)
                panelInfo1.Caption = vc.Settings.Levels[1].Title;
            

            //************ CONTEXT MENU OPBOUWEN ******************** 

            if (vc.Settings.Levels[level].ContextMenuItems != null && vc.Settings.Levels[level].ContextMenuItems.Count > 0)
            {
                grid.ContextMenu = new ContextMenu();
                foreach (SMMWindow2ContextMenuItem smmMi in vc.Settings.Levels[level].ContextMenuItems)
                {
                    MenuItem mi = new MenuItem();
                    mi.Click += ContextMenuItem_Click;
                    mi.Header = smmMi.Title;
                    mi.Tag = smmMi;
                    grid.ContextMenu.Items.Add(mi);
                }
            }

            //************ CONTEXT MENU OPBOUWEN EINDE ******************** 

            //Columns
            foreach (SMMWindow2Column col in vc.Settings.Levels[level].Colums)
            {
                GridColumn gcol = new GridColumn()
                {
                    Tag = level,
                    FieldName = col.FieldName,
                    Header = col.ColumnHeader
                };
                gcol.Validate += GridColumn_Validate;
                grid.Columns.Add(gcol);
        


                if (col.UnboundType != SMMWindow2ColumnUnboundType.Bound)
                {
                    //een column niet in de datasource
                    switch (col.UnboundType)
                    {
                        case SMMWindow2ColumnUnboundType.Image:
                            gcol.UnboundType = DevExpress.Data.UnboundColumnType.Object;
                            gcol.CellTemplate = BuildPictureTemplate(col);
                            break;
                        case SMMWindow2ColumnUnboundType.String:
                            gcol.UnboundType = DevExpress.Data.UnboundColumnType.String;
                            break;
                        case SMMWindow2ColumnUnboundType.DateTime:
                            gcol.UnboundType = DevExpress.Data.UnboundColumnType.DateTime;
                            break;
                        case SMMWindow2ColumnUnboundType.Decimal:
                            gcol.UnboundType = DevExpress.Data.UnboundColumnType.Decimal;
                            break;
                        case SMMWindow2ColumnUnboundType.Integer:
                            gcol.UnboundType = DevExpress.Data.UnboundColumnType.Decimal;
                            break;
                        case SMMWindow2ColumnUnboundType.Boolean:
                            gcol.UnboundType = DevExpress.Data.UnboundColumnType.Boolean;
                            break;
                    }
                    if (col.PictureWidth != null)
                        gcol.Width = col.PictureWidth.Value;
                }

                if (col.IsSummary)
                {
                    grid.TotalSummary.Add(new GridSummaryItem()
                    {
                        FieldName = col.FieldName,
                        SummaryType = SummaryItemType.Sum,
                       
                    });
                    grid.GroupSummary.Add(new GridSummaryItem()
                    {
                        FieldName = col.FieldName,
                        SummaryType = SummaryItemType.Sum,

                    });
                }
                if (col.Visible == false)
                {
                    grid.Columns[col.FieldName].Visible = false;
                }
                if (col.IsComboBox)
                {
                    grid.Columns[col.FieldName].EditSettings = new ComboBoxEditSettings() { ItemsSource = col.ComboboxItems, ValueMember = "Id", DisplayMember = "Name", AutoComplete = true, IsTextEditable = true, ValidateOnTextInput = false };
                }
                if (col.IsCheckBox)
                {
                    grid.Columns[col.FieldName].UnboundType = DevExpress.Data.UnboundColumnType.Boolean;
                    grid.Columns[col.FieldName].EditSettings = new CheckEditSettings();
                }
                if (col.IsPicture)
                {
                    grid.Columns[col.FieldName].UnboundType = DevExpress.Data.UnboundColumnType.Object;
                    grid.Columns[col.FieldName].EditSettings = new ImageEditSettings();
                }
                if (col.EditSettings != null)
                {
                    grid.Columns[col.FieldName].EditSettings = col.EditSettings;
                }
                if (vc.Settings.Levels[level].AllowFilter == false)
                    grid.Columns[col.FieldName].AllowColumnFiltering = DevExpress.Utils.DefaultBoolean.False;

                if (col.customColumnDisplayText != null)
                {
                    this.customColumnDisplayText.Add(col.FieldName, col.customColumnDisplayText);
                }
            }

            if (vc.Settings.Levels[level].AllowDelete)
                grid.PreviewKeyDown += Grid_PreviewKeyDown;


            /*Summary Group Row*/
            if (vc.Settings.Levels[level].GroupSummary != null)
            {
               
                GridSummaryItem groupSummaryItem = new GridSummaryItem();
                groupSummaryItem.FieldName = 
                
                groupSummaryItem.ShowInColumn = vc.Settings.Levels[level].GroupSummary.FieldName;  
                groupSummaryItem.DisplayFormat =  vc.Settings.Levels[level].GroupSummary.DisplayFormat; 
                groupSummaryItem.SummaryType = vc.Settings.Levels[level].GroupSummary.SummaryType;


                grid.GroupSummary.Add(groupSummaryItem);
                
            }


            var DataSource = vc.Settings.Levels[level].DataSource;
            
            grid.ItemsSource = DataSource;
            grid.SelectedItemChanged -= Grid_SelectedItemChanged;
            grid.SelectedItemChanged += Grid_SelectedItemChanged;
            if (grid.VisibleRowCount > 0)
                grid.View.FocusedRowHandle = 0;



         
        }
        private DataTemplate BuildPictureTemplate(SMMWindow2Column col)
        {
            DataTemplate template = new DataTemplate();
            FrameworkElementFactory imf = new FrameworkElementFactory(typeof(Image));

            if (col.PictureWidth != null)
                imf.SetValue(Image.WidthProperty, (double)col.PictureWidth);
            if (col.PictureHeight != null)
                imf.SetValue(Image.HeightProperty, (double)col.PictureHeight);
            imf.SetBinding(Image.SourceProperty, new Binding("Value"));
            template.VisualTree = imf;
            return template;
        }
        public GridControl GetGrid(int level)
        {
            switch (level)
            {
                case 0: return Grid0;
                case 1: return Grid1;
            }
            return null;
        }
        public void BindDataLevel<T>(int level, String query = "") where T : class
        {
        }
        private void FilterGrid(int parentLevel, GridType type)
        {
            GridDataControlBase gridParent = null;
            if (type == GridType.Grid)
            {
                gridParent = GetGrid(parentLevel);
            }
            else if (type == GridType.Tree)
            {
                gridParent = Tree0;
            }

            smmWindowVC vc = getVC();
            if (vc == null)
                return;
            int childLevel = parentLevel + 1;
            if (childLevel >= vc.Settings.LevelCount)
                return;
            GridControl gridChild = GetGrid(childLevel);
            foreach (SMMWindow2Column col in vc.Settings.Levels[parentLevel].Colums)
            {
                if (col.FieldName == vc.Settings.Levels[parentLevel].JoinField)
                {
                    if (gridParent.SelectedItem != null)
                    {
                        Object o = GetPropValue(gridParent.SelectedItem, col.FieldName);
                        if (o != null)
                            gridChild.FilterString = "[" + vc.Settings.Levels[1].JoinField + "] = '" + o.ToString() + "'";
                        else
                            gridChild.FilterString = "[" + vc.Settings.Levels[1].JoinField + "] = ''";
                    }
                    else
                    {
                        gridChild.FilterString = "[" + vc.Settings.Levels[1].JoinField + "] = ''";
                    }
                }
            }
        }
        private void BindDataSource1<T>() where T : class
        {
        }
        private String Save()
        {
            smmWindowVC vc = getVC();
            String errorMessage = null;
            try
            {
                ((GridViewBase)Grid0.View).PostEditor();
                ((GridViewBase)Grid1.View).PostEditor();
                vc.Entity.SaveChanges();
                return null;
            }
            catch (DbUpdateException e)
            {
                if (e.InnerException.InnerException is SqlException ex)
                {
                    if (ex.IsUniqueKeyViolation())
                    {
                        MessageBox.Show("Dit item bestaat al in deze lijst en kan geen 2e keer worden toegevoegd!", "Item bestaat al", MessageBoxButton.OK, MessageBoxImage.Exclamation, MessageBoxResult.OK);
                        errorMessage = e.Message + "\n\r" + e.InnerException;
                    }
                }
                else errorMessage = e.Message + "\n\r" + e.InnerException;
            }
            catch (Exception e)
            {
                errorMessage = e.Message + "\n\r" + e.InnerException;
            }
            return errorMessage;
        }
       
        public static object GetPropValue(object src, string propName)
        {
            return src.GetType().GetProperty(propName).GetValue(src, null);
        }
        public static void SetPropValue(object src, string propName, object value)
        {
            src.GetType().GetProperty(propName).SetValue(src, value);
        }

        private void barButtonExportToCsv_ItemClick(object sender, ItemClickEventArgs e)
        {
            BarButtonItem i = sender as BarButtonItem;
            GridControl g = null;
            String filename = "";
            if (i != null)
            {
                smmWindowVC vc = getVC();
                if (i.Tag.ToString() == "0")
                {
                    g = Grid0;
                    filename = vc.Settings.Levels[0].Title;
                }
                if (i.Tag.ToString() == "1")
                {
                    g = Grid1;
                    filename = vc.Settings.Levels[0].Title;
                }
                if (g != null)
                {
                    System.Windows.Forms.SaveFileDialog d = new System.Windows.Forms.SaveFileDialog()
                    {
                        Filter = "Excel CSV File|*.csv",
                        Title = "Exporteren naar Excel CSV",

                        RestoreDirectory = true,
                        CheckFileExists = false,
                        CheckPathExists = false,
                        SupportMultiDottedExtensions = true,
                        AutoUpgradeEnabled = true,

                        DefaultExt = System.IO.Path.GetExtension("csv"),
                        AddExtension = true,

                        FileName = filename
                    };
                    if (d.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        try
                        {
                            //CsvExportOptions opt = new CsvExportOptions();
                            //opt.TextExportMode = TextExportMode.Text;
                            ((DevExpress.Xpf.Grid.TableView)g.View).ExportToCsv(d.FileName/*,opt*/);

                            String question = String.Format("Wilt u het bestand {0} openen ?", d.FileName);
                            if (MessageBox.Show(question, "Openen", MessageBoxButton.YesNo, MessageBoxImage.Question) != MessageBoxResult.Yes)
                                return;

                            System.Diagnostics.Process.Start(d.FileName);
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message, "Fout", MessageBoxButton.OK, MessageBoxImage.Error);
                        }
                    }
                }
            }
        }
    }
    public enum GridType
    {
        Grid,
        Tree
    }
}
