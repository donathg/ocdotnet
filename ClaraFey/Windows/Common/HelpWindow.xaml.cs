﻿using ClaraFey.BLL;
using ClaraFey.CommonObjects;
using sharedcode.common;
using sharedcode.CommonObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ClaraFey.Windows.Common
{
    /// <summary>
    /// Interaction logic for HelpWindow.xaml
    /// </summary>
    public partial class HelpWindow : Window
    {
        bool changed = false;
        string _screenCode;
        public HelpWindow(String screenCode,String screenTitle)
        {
            _screenCode = screenCode;

            InitializeComponent();

            if (GlobalData.Instance.LoggedOnUser.GetAccesType("905") != AccessType.access)
            {
                richEditControl1.ReadOnly = true;
                ribbonControlPrint.Visibility = Visibility.Visible;
                ribbonControl1.Visibility = Visibility.Collapsed;
            }
            else
                ribbonControlPrint.Visibility = Visibility.Collapsed;

            WpfLayoutBLL.LoadHelp(richEditControl1, screenCode);

            this.Title = screenTitle;
            this.Loaded += HelpWindow_Loaded;
            this.Closing += HelpWindow_Closing;
            this.Width = System.Windows.SystemParameters.PrimaryScreenWidth / 100 * 90;
            this.Height = System.Windows.SystemParameters.PrimaryScreenHeight / 100 * 90;
            this.WindowStartupLocation = WindowStartupLocation.CenterScreen;
        }

        private void HelpWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (changed)
            {
                MessageBoxResult r =  MessageBox.Show("Wilt u de wijzigingen opslaan ?", "wijzigingen opslaan", MessageBoxButton.YesNoCancel, MessageBoxImage.Question);
                if (r == MessageBoxResult.Cancel)
                    e.Cancel = true;
                else if (r == MessageBoxResult.Yes)
                    Save();
            }
        }
        private void RichEditControl1_ContentChanged(object sender, EventArgs e)
        {
            changed = true;
        }
        private void HelpWindow_Loaded(object sender, RoutedEventArgs e)
        {
            changed = false;
            richEditControl1.ContentChanged += RichEditControl1_ContentChanged;
        }
        private void biFileSave_ItemClick(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            Save();
        }
        private void richEditControl1_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.S && Keyboard.Modifiers == ModifierKeys.Control)
            {
                Save();
                e.Handled = true;
            }
        }
        private void biPrint_ItemClick(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {

        }

        private void Save()
        {
            WpfLayoutBLL.SaveHelp(richEditControl1, _screenCode);
            changed = false;
        }
    }
}
