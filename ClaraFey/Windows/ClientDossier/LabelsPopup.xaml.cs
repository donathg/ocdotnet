﻿using ClaraFey.CommonObjects;
using sharedcode.BLL;
using sharedcode.common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ClaraFey.Windows.ClientDossier
{
    /// <summary>
    /// Interaction logic for LabelsPopup.xaml
    /// </summary>
    public partial class LabelsPopup : Window
    {
        public LabelConnectionManager ConnectionManager { get; set; }

        public LabelsPopup (int foreignKeyId,  ILabelConnectionDataProvider labelConnectionProvider)
        {
            InitializeComponent();
            GlobalMethods.SetTheme(this, GlobalMethods.Themes.clientdossier);
            ConnectionManager = new LabelConnectionManager(GlobalData.Instance.LoggedOnUser, labelConnectionProvider);
            ConnectionManager.LoadSelectedLabels(foreignKeyId);
            this.ucLabels.AllExistingLabels = ConnectionManager.AllExistingLabels;
            this.ucLabels.AllUserLabels = ConnectionManager.AllUserLabels;
            this.ucLabels.SelectedLabels = ConnectionManager.SelectedLabels;
        }


        public LabelsPopup( ILabelConnectionDataProvider labelConnectionProvider)
        {
            InitializeComponent();
            GlobalMethods.SetTheme(this, GlobalMethods.Themes.clientdossier);
            ConnectionManager = new LabelConnectionManager(GlobalData.Instance.LoggedOnUser,labelConnectionProvider);
            this.ucLabels.AllExistingLabels = ConnectionManager.AllExistingLabels;
            this.ucLabels.AllUserLabels = ConnectionManager.AllUserLabels;
            this.ucLabels.SelectedLabels = ConnectionManager.SelectedLabels;
            this.ucLabels.treeListView1.ExpandAllNodes();
        }



        private void btnOk_Click(object sender, RoutedEventArgs e)
        {
            if (this.ucLabels.SelectedLabels.Count > 0)
            {
                ConnectionManager.SelectedLabels = this.ucLabels.SelectedLabels;

                this.DialogResult = true;
            }
            else
                MessageBox.Show("Gelieve min. 1 label uit te kiezen");
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
}
