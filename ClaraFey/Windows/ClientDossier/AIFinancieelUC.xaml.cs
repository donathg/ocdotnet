﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ClaraFey.ViewControllers.ClientDossier;
using ClaraFey.ViewControllers.UserControls;

namespace ClaraFey.Windows.ClientDossier
{
    /// <summary>
    /// Interaction logic for AIFinanclieelUC.xaml
    /// </summary>
    public partial class AIFinanclieelUC : UserControl
    {
        AdministratieveInformatieVC _vc;

        public AIFinanclieelUC (AdministratieveInformatieVC vc)
        {
            this._vc = vc;
            this.DataContext = _vc;

            InitializeComponent();

            AutoSaveMultiCheckboxClientDossierGroup Group = new AutoSaveMultiCheckboxClientDossierGroup();
            Group.RegisterChildUCs(AutoSaveMultiCheckBoxFinanciëleInfoSoortInkomen.Vc);
            Group.RegisterChildUCs(AutoSaveMultiCheckBoxFinanciëleInfoArbInkomsten.Vc);
            Group.RegisterChildUCs(AutoSaveMultiCheckBoxFinanciëleInfoPersInkomsten.Vc);
            Group.RegisterChildUCs(AutoSaveMultiCheckBoxFinanciëleInfoVervInkomsten.Vc);
            Group.RegisterChildUCs(AutoSaveMultiCheckBoxFinanciëleInfoJaarlijksBelastbaarGezinsinkomen.Vc);
            Group.RegisterChildUCs(AutoSaveMultiCheckBoxFinanciëleInfoPersoonlijkeBijdrageFacturatie.Vc);
            vc.RegisterChildUCs(Group);


          /*  vc.RegisterChildUCs(this.AutoSaveMultiCheckBoxFinanciëleInfoSoortInkomen.Vc);
            vc.RegisterChildUCs(this.AutoSaveMultiCheckBoxFinanciëleInfoArbInkomsten.Vc);
            vc.RegisterChildUCs(this.AutoSaveMultiCheckBoxFinanciëleInfoPersInkomsten.Vc);
            vc.RegisterChildUCs(this.AutoSaveMultiCheckBoxFinanciëleInfoVervInkomsten.Vc);
            vc.RegisterChildUCs(this.AutoSaveMultiCheckBoxFinanciëleInfoJaarlijksBelastbaarGezinsinkomen.Vc);
            vc.RegisterChildUCs(this.AutoSaveMultiCheckBoxFinanciëleInfoPersoonlijkeBijdrageFacturatie.Vc);*/
            /*
            vc.RegisterChildUCs(this.AttachementAI_FIN_KINDERBIJSLAGFONDS.Vc);
            vc.RegisterChildUCs(this.AttachementAI_FIN_MUTUALITEIT.Vc);
            vc.RegisterChildUCs(this.AttachementAI_FIN_INKOMSTEN.Vc);
            */
            vc.RegisterChildUCs(this.AdressButtoMutualiteit.Vc);
            vc.RegisterChildUCs(this.AdressButtoKinderbijslagFonds.Vc);
        }
    }
}
