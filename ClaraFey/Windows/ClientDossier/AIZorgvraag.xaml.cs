﻿using ClaraFey.ViewControllers.ClientDossier;
using ClaraFey.ViewControllers.Common;
using ClaraFey.Windows.Common;
using sharedcode.BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ClaraFey.Windows.ClientDossier
{
    /// <summary>
    /// Interaction logic for AIZorgvraag.xaml
    /// </summary>
    public partial class AIZorgvraag : UserControl
    {
        AdministratieveInformatieVC _vc;
        public AIZorgvraag(AdministratieveInformatieVC vc)
        {
            this._vc = vc;
            InitializeComponent();

            vc.RegisterChildUCs(((IMyUserControl)this.AdressButtonVAPH.Vc));
            vc.RegisterChildUCs(((IMyUserControl)this.AdressButtonINSISTO.Vc));

            vc.RegisterChildUCs(((IMyUserControl)this.AutoSaveMultiCheckoBoxMFCIP.Vc));
            vc.RegisterChildUCs(((IMyUserControl)this.AutoSaveMultiCheckoBoxFAMVAPH.Vc));
           // vc.RegisterChildUCs(((IMyUserControl)this.AutoSaveMultiCheckoBoxVAPHOUDE.Vc));
            vc.RegisterChildUCs(((IMyUserControl)this.AutoSaveMultiCheckoBoxHANDICAP.Vc));

            vc.RegisterChildUCs(((IMyUserControl)this.AutoSaveMultiCheckoBoxFam.Vc));
            vc.RegisterChildUCs(((IMyUserControl)this.AutoSaveMultiCheckoBoxPFC.Vc));
            vc.RegisterChildUCs(((IMyUserControl)this.AutoSaveMultiCheckoBoxRTH.Vc));
            vc.RegisterChildUCs(((IMyUserControl)this.AutoSaveMultiCheckoBoxStatus.Vc));

            // register all bijlagenwidgets
            vc.RegisterChildUCs(((IMyUserControl)this.AttachementAI_A_DOCUMENT.Vc));
            vc.RegisterChildUCs(((IMyUserControl)this.AttachementAI_Ondersteuning.Vc));
            vc.RegisterChildUCs(((IMyUserControl)this.AttachementAI_ZORGVRAAG_FUNCVERSLAG.Vc));
            vc.RegisterChildUCs(((IMyUserControl)this.AttachementAI_ZORGVRAAG_VOORGAANDE_OPNAMES.Vc));
            vc.RegisterChildUCs(((IMyUserControl)this.AttachementAI_ZORGVRAAG_DIAGNOSE.Vc));
        }
    }
}