﻿using ClaraFey.CommonObjects;
using ClaraFey.ViewControllers.ClientDossier;
using ClaraFey.ViewControllers.Common;
using ClaraFey.Windows.Common;
using DevExpress.Xpf.Bars;
using DevExpress.Xpf.Core;
using sharedcode.BLL;
using sharedcode.common;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace ClaraFey.Windows.ClientDossier
{
    /// <summary>
    /// Interaction logic for AdministratieveInformatieVC.xaml
    /// </summary>
    public partial class AdministratieveInformatieUC : UserControl
    {
        /** FIELDS */
        private List<labels> _labelListContactverslagen = new List<labels>()
        {
            LabelManager.GetLabelById(1),
            LabelManager.GetLabelById(7),
            LabelManager.GetLabelById(8),
            LabelManager.GetLabelById(13),
            LabelManager.GetLabelById(18),
            LabelManager.GetLabelById(25)
        };

        /** CONSTRUCTOR */
        public AdministratieveInformatieUC()
        {
            InitializeComponent();
            this.Loaded += OvereenkomstUC_Loaded;
        }

        /** EVENTS */
        private void OvereenkomstUC_Loaded(object sender, RoutedEventArgs e)
        {
            if (GetVC() is AdministratieveInformatieVC vc)
            {
                tabAdressen.Content = new AdressenUC(new AdressenListSettings() { Bewoner = vc.Bewoner });
                
                if (!GlobalData.Instance.LoggedOnUser.HasAccess("1420"))
                    tabGeplandBegeleidingsTeamOCCF.Visibility = Visibility.Collapsed;
                
                if (_labelListContactverslagen.FindAll(x => x.contactverslagen == true).Count == 0)
                {
                    tabContactverslagen.Visibility = Visibility.Collapsed;
                }
            }
        }
        private void TabControl_SelectionChanging(object sender, TabControlSelectionChangingEventArgs e)
        {
            try
            {
                this.Cursor = Cursors.Wait;
                if (GetVC() is AdministratieveInformatieVC vc)
                {
                    if (((DXTabItem)e.NewSelectedItem).Name == "tabAdressen")
                    {
                        tabAdressen.Content = new AdressenUC(new AdressenListSettings() { Bewoner = vc.Bewoner });
                    }
                    else if (((DXTabItem)e.NewSelectedItem).Name == "tabGeplandBegeleidingsTeamOCCF")
                    {
                        tabGeplandBegeleidingsTeamOCCF.Content = new AIGeplandBegeleidingsTeamUC(vc);
                    }
                    else if (((DXTabItem)e.NewSelectedItem).Name == "tabZorgvraag")
                    {
                        tabZorgvraag.Content = new AIZorgvraag(vc);
                    }
                    else if (((DXTabItem)e.NewSelectedItem).Name == "tabJuridischegegevens")
                    {
                        tabJuridischegegevens.Content = new AIJruidischUC(vc);
                    }
                    else if (((DXTabItem)e.NewSelectedItem).Name == "tabFinancieleGegevens")
                    {
                        tabFinancieleGegevens.Content = new AIFinanclieelUC(vc);
                    }
                    else if (((DXTabItem)e.NewSelectedItem).Name == "tabMutaties")
                    {
                        tabMutaties.Content = new AIMutatiesUC(vc);
                    }
                    else if (((DXTabItem)e.NewSelectedItem).Name == "tabContactverslagen")
                    {
                        tabContactverslagen.Content = new ContactverslagUC();

                        LabelProviderEmpty labelProvider = new LabelProviderEmpty();
                        labelProvider.InjectLabels(_labelListContactverslagen);

                        ContactverslagVC contactverslagVC = new ContactverslagVC(new LabelManager(labelProvider));
                        contactverslagVC.Load(GetVC().Bewoner.id);

                        tabContactverslagen.DataContext = contactverslagVC;
                    }

                    vc.Save();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Fout", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                e.Cancel = true;
            }
            finally
            {
                this.Cursor = Cursors.Arrow;
            }
        }
        private void BiShowHelp_ItemClick(object sender, ItemClickEventArgs e)
        {
            HelpWindow w = new HelpWindow("cd_administratieve_informatie", "Help administratieve informatie");
            w.Show();
        }

        /** METHODS */
        private AdministratieveInformatieVC GetVC()
        {
            return this.DataContext as AdministratieveInformatieVC;
        }
    }
}
