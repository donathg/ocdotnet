﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ClaraFey.CommonObjects;
using ClaraFey.Reports;
using ClaraFey.ViewControllers.ClientDossier;
using DevExpress.XtraReports.UI;
using sharedcode.BLL;
using sharedcode.WeergaveClasses;

namespace ClaraFey.Windows.ClientDossier
{
    /// <summary>
    /// Interaction logic for ActiePlanPrintWindow.xaml
    /// </summary>
    public partial class ActiePlanPrintWindow : Window
    {
        private ActiePlanVC _vc = null;

        public ActiePlanPrintWindow(ActiePlanVC vc)
        {
            GlobalMethods.SetTheme(this, GlobalMethods.Themes.clientdossier);
            this._vc = vc;
            this.DataContext = _vc;

            InitializeComponent();

            GridOpvolgingen.SelectAll();
        }

        private void btnOk_Click(object sender, RoutedEventArgs e)
        {

        }
        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            this.Close();
        }
        private void btnPrint_Click(object sender, RoutedEventArgs e)
        {
            ActieplanReportModel reportModel = new ActieplanReportModel();
            reportModel.CreateReport(this._vc.Bewoner,this._vc.SelectedActieplan, GridOpvolgingen.SelectedItems.Cast<actieplanopvolging>().ToList());
            
            using (ReportPrintTool printTool = new ReportPrintTool(new ActieplanReport(reportModel)))
            {
                printTool.ShowRibbonPreviewDialog();
            }

            this.DialogResult = true;
            this.Close();
        }
    }
}
