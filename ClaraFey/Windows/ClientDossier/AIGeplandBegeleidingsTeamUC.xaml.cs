﻿using ClaraFey.ViewControllers.ClientDossier;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ClaraFey.UserControls;
using DevExpress.Xpf.Editors;
using sharedcode.BLL;

namespace ClaraFey.Windows.ClientDossier
{
    /// <summary>
    /// Interaction logic for AIGeplandBegeleidingsTeamUC.xaml
    /// </summary>
    public partial class AIGeplandBegeleidingsTeamUC : UserControl
    {
        /** FIELDS */
        private BegeleidingsteamChoiceItem GetSelectedBegeleiderFromAllList(string tag)
        {
            BegeleidingsteamChoiceItem g = null;
            switch (tag)
            {
                case "1":
                    g = LookUpEdit1.SelectedItem as BegeleidingsteamChoiceItem;
                    break;
                case "2":
                    g = LookUpEdit2.SelectedItem as BegeleidingsteamChoiceItem;
                    break;
                case "3":

                    g = LookUpEdit3.SelectedItem as BegeleidingsteamChoiceItem;
                    break;
                case "4":
                    g = LookUpEdit4.SelectedItem as BegeleidingsteamChoiceItem;
                    break;
                case "5":
                    g = LookUpEdit5.SelectedItem as BegeleidingsteamChoiceItem;
                    break;
                case "6":
                    g = LookUpEdit6.SelectedItem as BegeleidingsteamChoiceItem;
                    break;
                case "7":
                    g = LookUpEdit7.SelectedItem as BegeleidingsteamChoiceItem;
                    break;
                case "8":
                    g = LookUpEdit8.SelectedItem as BegeleidingsteamChoiceItem;
                    break;
                case "9":
                    g = LookUpEdit9.SelectedItem as BegeleidingsteamChoiceItem;
                    break;
                case "10":
                    g = LookUpEdit10.SelectedItem as BegeleidingsteamChoiceItem;
                    break;
                case "11":
                    g = LookUpEdit11.SelectedItem as BegeleidingsteamChoiceItem;
                    break;

            }
            return g;
        }
        private gepland_begeleidingsteam GetSelectedBegeleiderFromSelectedList(string tag)
        {
            gepland_begeleidingsteam g = null;
            switch (tag)
            {

                case "1":
                    g = listBoxSelectedOrthopedagoog.SelectedItem as gepland_begeleidingsteam;
                    break;
                case "2":
                    g = listBoxSelectedIndividueleAandachtsopvoeder.SelectedItem as gepland_begeleidingsteam;
                    break;
                case "3":
                    g = listBoxSelectedMaatschappelijkwerker.SelectedItem as gepland_begeleidingsteam;
                    break;
                case "4":
                    g = listBoxSelectedMobielebegeleider.SelectedItem as gepland_begeleidingsteam;
                    break;
                case "5":
                    g = listBoxSelectedVerpleegkundige.SelectedItem as gepland_begeleidingsteam;
                    break;
                case "6":
                    g = listBoxSelectedKinesist.SelectedItem as gepland_begeleidingsteam;
                    break;
                case "7":
                    g = listBoxSelectedPsychotherapeut.SelectedItem as gepland_begeleidingsteam;
                    break;
                case "8":
                    g = listBoxSelectedContactpersoonDagbesteding.SelectedItem as gepland_begeleidingsteam;
                    break;
                case "9":
                    g = listBoxSelectedKinderpsychiater.SelectedItem as gepland_begeleidingsteam;
                    break;
                case "10":
                    g = listBoxSelectedLogopedist.SelectedItem as gepland_begeleidingsteam;
                    break;
                case "11":
                    g = listBoxSelectedGroep.SelectedItem as gepland_begeleidingsteam;

                    break;

            }
            return g;
        }
        public AdministratieveInformatieVC _vc;

        /** CONSTRUCTORS */
        public AIGeplandBegeleidingsTeamUC(AdministratieveInformatieVC vc)
        {
            this._vc = vc;
            this.DataContext = _vc;
            InitializeComponent();
        }

        /** EVENTS */
        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            if (sender is Button button && button.Tag is string boxNumber)
                Add(boxNumber);
        }
        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            Button l = sender as Button;
            Delete(l.Tag.ToString());
        }
        private void miDelete_Click(object sender, RoutedEventArgs e)
        {
            MenuItem m = (MenuItem)sender;
            if (m != null)
            {
                Delete(m.Tag.ToString());
            }
        }
        private void listBoxSelected_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Delete)
            {
                ListBoxEdit l = sender as ListBoxEdit;
                Delete(l.Tag.ToString());
            }
        }
        private void LookUpEdit_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter || e.Key == Key.Tab)
            {
                MyLookUpEdit l = sender as MyLookUpEdit;
                Add(l.Tag.ToString());
            }
        }

        /** METHODS */
        private void Add(String tag)
        {
            try
            {
                if (GetSelectedBegeleiderFromAllList(tag) is BegeleidingsteamChoiceItem choiceItem && Int32.TryParse(tag, out int tagInt))
                {
                    _vc.AddBegeleidingsTeamEntry(choiceItem, tagInt);

                    LookUpEdit1.SelectedItem = null;
                    LookUpEdit2.SelectedItem = null;
                    LookUpEdit3.SelectedItem = null;
                    LookUpEdit4.SelectedItem = null;
                    LookUpEdit5.SelectedItem = null;
                    LookUpEdit6.SelectedItem = null;
                    LookUpEdit7.SelectedItem = null;
                    LookUpEdit8.SelectedItem = null;
                    LookUpEdit9.SelectedItem = null;
                    LookUpEdit10.SelectedItem = null;
                    LookUpEdit11.SelectedItem = null;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void Delete(String tag)
        {
            try
            {
                if (GetSelectedBegeleiderFromSelectedList(tag) is gepland_begeleidingsteam gebruiker)
                    _vc.RemoveBegeleidingsTeamEntry(gebruiker);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
