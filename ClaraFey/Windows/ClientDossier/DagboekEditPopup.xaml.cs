﻿using ClaraFey.CommonObjects;
using ClaraFey.ViewControllers.ClientDossier;
using ClaraFey.Windows.Common;
using sharedcode.BLL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ClaraFey.Windows.ClientDossier
{
    /// <summary>
    /// Interaction logic for DagboekEditPopup.xaml
    /// </summary>
    public partial class DagboekEditPopup : Window, INotifyPropertyChanged
    {
        /** FIELDS */
        public event PropertyChangedEventHandler PropertyChanged;

        public DagboekWeergave Dagboek { get; set; }
        public String SelectedItemName { get; set; }
 

        /** CONSTRUCTOR */
        public DagboekEditPopup(DagboekWeergave dagBoek, GlobalMethods.Themes theme)
        {
            if (dagBoek != null)
            {
                Dagboek = dagBoek;
                this.Title = @"Editeer dagboekitem n°" + Dagboek.DagboekId;

                SelectedItemName = dagBoek.FotoDescr;
            }
            else
            {
                Dagboek = new DagboekWeergave(new dagboek());
                this.Title = @"Nieuw dagboekitem";
            }
            this.DataContext = this;
            GlobalMethods.SetTheme(this, theme);
            this.Width = SystemParameters.PrimaryScreenWidth * 0.6; // 60% van de breedte van het scherm
            this.Height = SystemParameters.PrimaryScreenHeight * 0.6; // 60% van de hoogte van het scherm

            InitializeComponent();
        }

        /** EVENTS */
        private void LeefgroepOfBewonerToevoegen_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                LeefgroepBewonersTree win = new LeefgroepBewonersTree(CommonObjects.GlobalMethods.Themes.clientdossier,true);
                win.ShowDialog();

                if (win.SelectedItem.Object is Object selectedObj)
                {
                    if (selectedObj is bewoner bew)
                    {
                        Dagboek.dagboekDB.leefgroep_id = null;
                        Dagboek.dagboekDB.bewonerId = bew.id;
                        SelectedItemName = bew.VoornaamAchternaam;
                    }
                    else if (selectedObj is leefgroep groep)
                    {
                        Dagboek.dagboekDB.bewonerId = null;
                        Dagboek.dagboekDB.leefgroep_id = groep.id;
                        SelectedItemName = groep.naam;
                    }
                    NotifiyPropertyChanged(nameof(SelectedItemName));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Foutmelding", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            new DagboekBLL().ChangeDagboekItem(Dagboek);
            this.Close();
        }

        /** METHODS */
        public void NotifiyPropertyChanged(string property)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(property));
        }
    }
}
