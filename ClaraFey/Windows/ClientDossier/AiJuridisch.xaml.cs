﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ClaraFey.ViewControllers.ClientDossier;

namespace ClaraFey.Windows.ClientDossier
{
    /// <summary>
    /// Interaction logic for AiJuridisch.xaml
    /// </summary>
    public partial class AiJuridisch : UserControl
    {
      
        AdministratieveInformatieVC _vc;
        public AiJuridisch(AdministratieveInformatieVC vc)
        {
            this._vc = vc;
            InitializeComponent();


          /*  vc.RegisterChildUCs(((IMyUserControl)this.AutoSaveMultiCheckoBoxMFCIP.Vc));
            vc.RegisterChildUCs(((IMyUserControl)this.AutoSaveMultiCheckoBoxFAMVAPH.Vc));
            vc.RegisterChildUCs(((IMyUserControl)this.AutoSaveMultiCheckoBoxVAPHOUDE.Vc));
            vc.RegisterChildUCs(((IMyUserControl)this.AutoSaveMultiCheckoBoxHANDICAP.Vc));
            vc.RegisterChildUCs(((IMyUserControl)this.AttachementAI_A_DOCUMENT.Vc));
            vc.RegisterChildUCs(((IMyUserControl)this.AttachementAI_Ondersteuning.Vc));

            vc.RegisterChildUCs(((IMyUserControl)this.AutoSaveMultiCheckoBoxFam.Vc));
            vc.RegisterChildUCs(((IMyUserControl)this.AutoSaveMultiCheckoBoxPFC.Vc));
            vc.RegisterChildUCs(((IMyUserControl)this.AutoSaveMultiCheckoBoxRTH.Vc));
            vc.RegisterChildUCs(((IMyUserControl)this.AutoSaveMultiCheckoBoxStatus.Vc));
            //   vc.RegisterChildUCs(((IMyUserControl)this.AutoSaveMultiCheckoBoxNoodzVerslaggeving.Vc));

            vc.RegisterChildUCs(((IMyUserControl)this.AttachementAI_ZORGVRAAG_DIAGNOSE.Vc));
            vc.RegisterChildUCs(((IMyUserControl)this.AttachementAI_ZORGVRAAG_FUNCVERSLAG.Vc));
            vc.RegisterChildUCs(((IMyUserControl)this.AttachementAI_ZORGVRAAG_IQ_GEGEVENS.Vc));
            vc.RegisterChildUCs(((IMyUserControl)this.AttachementAI_ZORGVRAAG_VOORGAANDE_OPNAMES.Vc));*/
        }
    }
}
