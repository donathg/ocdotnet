﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ClaraFey.CommonObjects;
using sharedcode.BLL;
using sharedcode.WeergaveClasses;

namespace ClaraFey.Windows.ClientDossier
{
    /// <summary>
    /// Interaction logic for ActieplanPopupOpvolging.xaml
    /// </summary>
    public partial class ActieplanPopupOpvolging : Window
    {
        /** FIELDS */
        public ActieplanOpvolgingWeergave Opvolging { get; set; }
        public bool IsEditable { get; set; }

        /** CONSTRUCTORS */
        public ActieplanPopupOpvolging()
        {
            InitializeComponent();
            GlobalMethods.SetTheme(this, GlobalMethods.Themes.clientdossier);
            this.DataContext = this;
        }
        public ActieplanPopupOpvolging(ActieplanOpvolgingWeergave opvolging) : this() // edit opvolging
        {
            this.Title = "Opvolging aanpassen voor actieplan : " + opvolging.actieplan.title;
            Opvolging = opvolging;
            IsEditable = false;
        }
        public ActieplanPopupOpvolging(ActieplanWeergave actieplan) : this() // create new opvolging
        {
            this.Title = "Opvolging toevoegen voor actieplan : " + actieplan.title;
            Opvolging = new ActieplanOpvolgingWeergave() { actieplanid = actieplan.id };
            IsEditable = true;
        }

        /** EVENTS */
        private void BtnOk_Click(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(Opvolging.watlooptgoed) && !string.IsNullOrWhiteSpace(Opvolging.waaraanwerken) && ! string.IsNullOrWhiteSpace(Opvolging.hoeaanpakken))
            {
                this.DialogResult = true;
                this.Close();
            }
            else
                MessageBox.Show("De eerste 3 velden (\"Wat loopt al goed ?\", \"Waar moet ik nog aan werken ?\" en \"Wat hebben we gedaan met doelstelling ?\") zijn verplicht in te vullen!", "Verplichte velden!", MessageBoxButton.OK, MessageBoxImage.Exclamation, MessageBoxResult.OK);
        }
        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            this.Close();
        }
    }
} 