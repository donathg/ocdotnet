﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ClaraFey.CommonObjects;
using ClaraFey.ViewControllers.ClientDossier;
using DevExpress.Xpf.Grid;
using sharedcode.BLL;
using BeeldvormingReport = ClaraFey.Reports.BeeldvormingReport;
using DevExpress.XtraReports.UI;

namespace ClaraFey.Windows.ClientDossier
{
    /// <summary>
    /// Interaction logic for BeeldvormingPrintPopup.xaml
    /// </summary>
    public partial class BeeldvormingPrintPopup : Window
    {
        private BeeldvormingVC _vc;
        public BeeldvormingPrintPopup(BeeldvormingVC vc)
        {
            this._vc = vc;
            GlobalMethods.SetTheme(this, GlobalMethods.Themes.clientdossier);
            this.DataContext = vc;
            InitializeComponent();
            this.Width = (int)(double)System.Windows.SystemParameters.PrimaryScreenWidth / 100 * 90;
            this.Height = (int)(double)System.Windows.SystemParameters.PrimaryScreenHeight / 100 * 90;
            RefreshItemSource();
        }
        private void RefreshItemSource()
        {
            treeControlBeeldvorming.ItemsSource = null;
            treeControlBeeldvorming.ItemsSource = _vc.BeeldVormingTree;
        }
        private void BtnOk_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                BeeldvormingReportPrintTypes selectedType = BeeldvormingReportPrintTypes.A;

                ComboBoxItem cmbItem = (ComboBoxItem) cmbTypes.SelectedValue;
                if (cmbItem.Content.ToString() == "B")
                    selectedType = BeeldvormingReportPrintTypes.B;
                if (cmbItem.Content.ToString() == "A+B")
                    selectedType = BeeldvormingReportPrintTypes.AB;

                BeeldvormingReport report = new BeeldvormingReport(this._vc.CreateBeeldvormingReport(selectedType, this.myToggleSwitch.IsChecked));

                if (this._vc.isOldVersionSelected == false)
                {
                    report.Watermark.Text = "Ontwerp";
                    report.Watermark.TextDirection = DevExpress.XtraPrinting.Drawing.DirectionMode.BackwardDiagonal;
                }
                using (ReportPrintTool printTool = new ReportPrintTool(report))
                {
                    printTool.ShowRibbonPreviewDialog();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message,"Fout",MessageBoxButton.OK,MessageBoxImage.Error);
            }
        }
        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            this.Close();
        }
    }
}
