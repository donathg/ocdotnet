﻿using ClaraFey.ViewControllers.ClientDossier;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DevExpress.Xpf.Grid.TreeList;
using sharedcode.BLL;
using System.Collections;
using DevExpress.CodeParser;
using DevExpress.Xpf.Grid;
using DevExpress.Xpf.Utils;
using ToggleSwitch;
using ClaraFey.Windows.Common;

namespace ClaraFey.Windows.ClientDossier
{
    /// <summary>
    /// Interaction logic for BeeldvormingOverzichtUC.xaml
    /// </summary>
    public partial class BeeldvormingOverzichtUC : UserControl
    {
        BeeldvormingVC _vc;
        private HorizontalToggleSwitch _toggleSwitch = null;

        public BeeldvormingOverzichtUC(BeeldvormingVC vc)
        {
            _vc = vc;

            TreeListControl.AllowInfiniteGridSize = true;
            InitializeComponent();

            vc.RegisterChildUCs(AttachementBeeldvorming.Vc);

            RefreshItemSource();
        }

        private void RefreshItemSource()
        {
            treeControlBeeldvorming.ItemsSource = null;
            treeControlBeeldvorming.ItemsSource = _vc.BeeldVormingTree;
        }
        private void treeControlBeeldvorming_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (_vc.SelectedTreeItem != null && _vc.SelectedTreeItem.isEditable)
            {
                BeeldvormingPopup window = new BeeldvormingPopup(this._vc);
                if (window.ShowDialog().GetValueOrDefault())
                {
                    RefreshItemSource();
                }
            }
        }
        private void TreeViewBeeldvorming_OnCustomUnboundColumnData(object sender, TreeListUnboundColumnDataEventArgs e)
        {
            if (e.Column.FieldName == "statusImage")
            {
                if (e.IsGetData)
                {
                    MyBeeldvormingtree row = e.Node.Content as MyBeeldvormingtree;
                    if (row.isEditable)
                    {
                        if (row.IsClosed || _vc.isOldVersionSelected)
                            e.Value = new BitmapImage(new Uri("/images/redcircle.png", UriKind.Relative));
                        else
                        {
                            e.Value = new BitmapImage(new Uri("/images/greencircle.png", UriKind.Relative));
                        }
                    }
                }
            }
        }
        private void btnReport_Click(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            BeeldvormingPrintPopup w = new BeeldvormingPrintPopup(this._vc);
            w.ShowDialog();
        }
        private void FrameworkElement_OnInitialized(object sender, EventArgs e)
        {
            this._toggleSwitch = ((ToggleSwitch.HorizontalToggleSwitch)sender);
            ToggleSwitchRemoveEvents();
            ToggleSwitchAddEvents();
            RefreshToggleSwitch();
        }
        private void ToggleSwitchRemoveEvents()
        {
            this._toggleSwitch.Checked -= _toggleSwitch_Checked;
            this._toggleSwitch.Unchecked -= _toggleSwitch_Checked;

        }
        private void ToggleSwitchAddEvents()
        {
            this._toggleSwitch.Checked += _toggleSwitch_Checked;
            this._toggleSwitch.Unchecked += _toggleSwitch_Checked;
        }
        private void RefreshToggleSwitch()
        {
            ToggleSwitchRemoveEvents();
            if (!_vc.IsAllowedToOpenOrCloseCurrentHeaderVersion)
                _toggleSwitch.Visibility = Visibility.Hidden;
            if (_vc.isLatestVersionSelected)
            {
                _toggleSwitch.IsChecked = _vc.IsCurrentHeaderVersionClosed;
                _toggleSwitch.IsEnabled = true;
            }
            else
            {
                _toggleSwitch.IsChecked = true;
                _toggleSwitch.IsEnabled = false;
            }
            ToggleSwitchAddEvents();
        }
        private void _toggleSwitch_Checked(object sender, RoutedEventArgs e)
        {
            CheckOrUncheck(true);
        }
        void CheckOrUncheck(bool removeAddEvents)
        {
            try
            {    
                this._toggleSwitch.IsEnabled = false;

                if (!_vc.IsCurrentHeaderVersionClosed && !_vc.IsAllCurrentVersionClosed())
                {
                    if (MessageBox.Show("Niet alle onderdelen van beeldvorming zijn gesloten.\n\rWenst u de beeldvorming toch in zijn geheel af te sluiten ?", "Afsluiten", MessageBoxButton.OKCancel, MessageBoxImage.Question) == MessageBoxResult.Cancel)
                        return;
                }
                this._vc.CloseOpenCurrentVersion();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Fout", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            finally
            {
                this._toggleSwitch.IsEnabled = true;
                RefreshToggleSwitch();
             
            }
        }
        private void btnNewVersion_Click(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            try
            {
            
                this._toggleSwitch.IsEnabled = false;
                PopupDate popup = new PopupDate("Gelieve een vervaldatum in te geven", DateTime.Now.AddYears(1), false, CommonObjects.GlobalMethods.Themes.clientdossier);
                if (popup.ShowDialog().HasValue && popup.Result.HasValue)
                {
                    _vc.CreateNewVersion(popup.Result.Value);
                    RefreshItemSource();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Fout", MessageBoxButton.OK, MessageBoxImage.Error);

            }
            finally
            {
                this._toggleSwitch.IsEnabled = true;
                RefreshToggleSwitch();
            
            }
        }
        private void Selector_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
             
            RefreshItemSource();
            RefreshToggleSwitch();
   
        }
    }
    public class ColorValueConverter : IValueConverter
    {
        #region IValueConverter Members
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            MyBeeldvormingtree item = value as MyBeeldvormingtree;

            SolidColorBrush darkBlue = new SolidColorBrush(Color.FromRgb(216, 221, 230));
            SolidColorBrush lightBlue = new SolidColorBrush(Color.FromRgb(234, 242, 249));
            if (item != null)
            {
                if (item.level == 0)
                    return darkBlue;

                if (!item.isEditable)
                {
                    return lightBlue;
                }
            }
            return new SolidColorBrush(Colors.White);
        }
        public object ConvertBack(object value, Type targetType, object parameter,
        CultureInfo culture)
        {
            return null;
        }
        #endregion
    }
    public class FontWeightConverter : IValueConverter
    {
        #region IValueConverter Members
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is MyBeeldvormingtree item)
            {
                if (item.level == 0)
                    return FontWeights.Bold;

                if (!item.isEditable)
                {
                    return FontWeights.DemiBold;
                }
            }

            return FontWeights.Normal;

        }

        public object ConvertBack(object value, Type targetType, object parameter,
        CultureInfo culture)
        {
            return null;
        }
        #endregion
    }
}
