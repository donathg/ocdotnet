﻿using ClaraFey.ViewControllers.ClientDossier;
using DevExpress.Xpf.Grid;
using sharedcode.BLL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ClaraFey.Windows.ClientDossier
{
    /// <summary>
    /// Interaction logic for OvereenkomstPartUC.xaml
    /// </summary>
    public partial class OvereenkomstPartUC : UserControl
    {
        /** FIELDS */
        private OvereenkomstenPartVC _vc;

        /** CONSTRUCTORS */
        public OvereenkomstPartUC()
        {
            InitializeComponent();
        }
        public OvereenkomstPartUC(OvereenkomstenPartVC vc): this()
        {
            _vc = vc;
            this.DataContext = _vc;
        }

        /** EVENTS */
        private void Grid0_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (_vc == null)
                _vc = this.DataContext as OvereenkomstenPartVC;

            _vc.OpenBijlagenScherm();
        }
        private void GridControl_Loaded(object sender, RoutedEventArgs e)
        {
            if (sender is GridControl control && control.View is TableView view)
                view.BestFitColumns();
        }
    }
 }