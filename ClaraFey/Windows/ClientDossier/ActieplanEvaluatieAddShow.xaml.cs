﻿using ClaraFey.CommonObjects;
using ClaraFey.ViewControllers.ClientDossier;
using sharedcode.BLL;
using sharedcode.WeergaveClasses;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ClaraFey.Windows.ClientDossier
{
    /// <summary>
    /// Interaction logic for ActieplanEvaluatieAddShow.xaml
    /// </summary>
    public partial class ActieplanEvaluatieAddShow : Window, INotifyPropertyChanged
    {
        /** FIELDS */
        public event PropertyChangedEventHandler PropertyChanged;

        public ActieplanEvaluatiesWeergave Evaluatie { get; set; }
        public Boolean CanModifyDelete { get; set; }
        public Boolean CanModifyType { get; set; }
        public Boolean IsPeriodiekeEvaluatie { get; set; }
        public Boolean IsEindEvaluatie
        {
            get
            {
                return Evaluatie.actieplan_id.HasValue && Evaluatie.actieplan_id != 0;
            }
        }
        public List<int> SelectedTypesList { get; set; }
        public ObservableCollection<Actieplan_Resultaat> ActieplanResultatenList { get; set; }
        public ObservableCollection<actieplantypes> ActiePlanTypesList { get; set; }
        public ObservableCollection<actieplan> DoelstellingList { get; set; }

        /** CONSTRUCTORS */
        public ActieplanEvaluatieAddShow(ActieplanEvaluatiesAddModifyWindowSettings settings)
        {
            ActieplanResultatenList = new ObservableCollection<Actieplan_Resultaat>(ActieplanEvaluatiesBLL.GetAllActieplanResultaten());
            ActiePlanTypesList = new ObservableCollection<actieplantypes>(ActiePlanBLL.GetAllActieplantypes());

            ApplySettings(settings);
            InitDates();
            this.DataContext = this;

            InitializeComponent();

            GlobalMethods.SetTheme(this, GlobalMethods.Themes.clientdossier);
        }

        /** EVENTS */
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            GlobalMethods.SetSizeAndCenterWindow(this, 80);
        }
        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Evaluatie.Actieplantypes = listTypes.SelectedItems.Cast<actieplantypes>().ToList();
                if (!String.IsNullOrWhiteSpace(Evaluatie.hoeVerliepHet) && Evaluatie.resultaat_id != 0)
                {
                    if (Evaluatie.id == 0)
                        ActieplanEvaluatiesBLL.AddActieplanEvaluatie(Evaluatie);
                    else
                        ActieplanEvaluatiesBLL.ModifyActieplanEvaluatie(Evaluatie);
                }
                else
                    throw new Exception("Je moet alle velden invullen voordat je verder kan!");

                this.DialogResult = true;
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Foutmelding", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        /** METHODS */
        private void NotifiyPropertyChanged(string property)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(property));
        }
        private void InitDates()
        {
            if (IsPeriodiekeEvaluatie)
            {
                Evaluatie.start_datum = DateTime.Today.AddMonths(-1);
                Evaluatie.eind_datum = DateTime.Today;
            }
            else
            {
                Evaluatie.start_datum = null;
                Evaluatie.eind_datum = null;
            }
            Evaluatie.datum_evaluatie = DateTime.Today;
        }
        private void ApplySettings(ActieplanEvaluatiesAddModifyWindowSettings settings)
        {
            Evaluatie = settings.SelectedEvaluatie;

            if (Evaluatie != null && settings.ActieplanId != 0)
            {
                Evaluatie.actieplan_id = settings.ActieplanId;
                /*
                DoelstellingList = new ObservableCollection<actieplan>
                {
                    ActiePlanBLL.GetActieplanFromId(Evaluatie.actieplan_id.Value)
                };*/
            }
            if (settings.ActieplantypesList != null && settings.ActieplantypesList.Count != 0)
            {
                Evaluatie.Actieplantypes = settings.ActieplantypesList;
                CanModifyType = false;
            }
            else
                CanModifyType = settings.CanChangeData;
            Evaluatie.bewoner_id = settings.BewonerId;
            CanModifyDelete = settings.CanChangeData;
            IsPeriodiekeEvaluatie = settings.CanSeePeriode;

            SelectedTypesList = new List<int>();
            if ((settings.ActieplantypesList != null && settings.ActieplantypesList.Count != 0) || (settings.SelectedEvaluatie.Actieplantypes != null && settings.SelectedEvaluatie.Actieplantypes.Count != 0))
            {
                foreach (actieplantypes type in settings.SelectedEvaluatie.Actieplantypes)
                {
                    SelectedTypesList.Add(type.id);
                }
                NotifiyPropertyChanged(nameof(SelectedTypesList));
            }
        }
    }
}
