﻿using ClaraFey.CommonObjects;
using ClaraFey.ViewControllers.ClientDossier;
using sharedcode.common;
using sharedcode.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ClaraFey.Windows.ClientDossier
{
    /// <summary>
    /// Interaction logic for ActieplanEvaluatiesUC.xaml
    /// </summary>
    public partial class ActieplanEvaluatiesUC : UserControl
    {
        /** FIELDS */
        private ActieplanEvaluatieVC _vc;

        /** CONSTRUCTORS */
        public ActieplanEvaluatiesUC(ActieplanEvaluatieVersies versie, int bewonerId)
        {
            SetVC(new ActieplanEvaluatieVC(versie, bewonerId));

            InitializeComponent();

            if (_vc.CanModifyDelete)
                ModifyEvaluatieMenuItem.Header += "/aanpassen";

            GlobalMethods.SetTheme(this, GlobalMethods.Themes.clientdossier);
        }
        
        /** EVENTS */
        private void EvaluatiesGridControl_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            ModifyEvaluatie();
        }
        private void EvaluatiesGridControl_KeyDown(object sender, KeyEventArgs e)
        {
            if (_vc.CanModifyDelete && e.Key == Key.Delete)
                DeleteEvaluatie();
        }
        private void ModifyEvaluatieMenuItem_Click(object sender, RoutedEventArgs e)
        {
            ModifyEvaluatie();
        }
        private void DeleteEvaluatieMenuItem_Click(object sender, RoutedEventArgs e)
        {
            DeleteEvaluatie();
        }

        /** METHODS */
        public void SetVC(ActieplanEvaluatieVC vc)
        {
            _vc = vc;
            this.DataContext = _vc;
        }
        private void ModifyEvaluatie()
        {
            ActieplanEvaluatieAddShow popup = new ActieplanEvaluatieAddShow(new ActieplanEvaluatiesAddModifyWindowSettings()
            {
                Versie = _vc.Versie,
                SelectedEvaluatie = _vc.SelectedEvaluatie,
                BewonerId = _vc.BewonerId
            });
            popup.ShowDialog();
            _vc.FillEvaluatiesList();
        }
        private void DeleteEvaluatie()
        {
            if (MessageBox.Show("Ben je zeker dat je de evaluatie wil verwijderen?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                _vc.RemoveActieplanEvaluatie();
        }
    }
}
