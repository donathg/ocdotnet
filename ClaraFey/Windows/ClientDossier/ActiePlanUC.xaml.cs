﻿using ClaraFey.ViewControllers.ClientDossier;
using ClaraFey.Windows.Common;
using sharedcode.BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ClaraFey.Reports;
using DevExpress.DataAccess.UI.Native.Sql.QueryBuilder;
using DevExpress.Xpf.Core;
using sharedcode.common;
using sharedcode.DAL;
using sharedcode.WeergaveClasses;

namespace ClaraFey.Windows.ClientDossier
{
    /// <summary>
    /// Interaction logic for OvereenkomstUC.xaml
    /// </summary>
    public partial class ActiePlanUC : UserControl
    {
        /** FIELDS */
        private List<labels> _labelsListContactverslagen = new List<labels>()
        {
            LabelManager.GetLabelById(80)
        };

        /** CONSTRUCTORS */
        public ActiePlanUC()
        {
            InitializeComponent();
        }

        /** EVENTS */
        private void ActiePlanUC_Loaded(object sender, RoutedEventArgs e)
        {
            ActiePlanVC vc = GetVC();
            this.DataContext = vc;

            if (!GlobalData.Instance.LoggedOnUser.HasAccess("1810"))
            {
                tabDoelstellingClient.Visibility = Visibility.Collapsed;
                tabPeriodiekeEvaluaties.Visibility = Visibility.Collapsed;
            }
            if (!GlobalData.Instance.LoggedOnUser.HasAccess("1820"))
                tabDoelstellingOuders.Visibility = Visibility.Collapsed;

            if (!GlobalData.Instance.LoggedOnUser.HasAccess("1830"))
                tabDoelstellingBegeleidingsteam.Visibility = Visibility.Collapsed;

            if (!GlobalData.Instance.LoggedOnUser.HasAccess("1840"))
                tabHistoriek.Visibility = Visibility.Collapsed;

            if (_labelsListContactverslagen.FindAll(x => x.contactverslagen == true).Count == 0)
            {
                tabContactverslagen.Visibility = Visibility.Collapsed;
            }

            actiePlanTabControl.SelectFirst();
            if (actiePlanTabControl.SelectedItem is DXTabItem item)
                SetTabContent(item);
        }
        private void BiShowHelp_ItemClick(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            HelpWindow w = new HelpWindow("cd_actieplan", "Help actieplan");
            w.Show();
        }
        private void BarButtonNew_ItemClick(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            try
            {
                if (tabPeriodiekeEvaluaties.IsSelected)
                {
                    ActieplanEvaluatieAddShow popup = new ActieplanEvaluatieAddShow(new ActieplanEvaluatiesAddModifyWindowSettings()
                    {
                        Versie = ActieplanEvaluatieVersies.PERIODIEKE_EVALUATIE,
                        SelectedEvaluatie = new ActieplanEvaluatiesWeergave(),
                        BewonerId = GetVC().Bewoner.id
                    });
                    popup.ShowDialog();

                    if (((ActieplanEvaluatiesUC)tabPeriodiekeEvaluaties.Content).DataContext is ActieplanEvaluatieVC vc)
                        vc.FillEvaluatiesList();
                }
                else
                {
                    WindowNewActiePlan w = new WindowNewActiePlan(GetVC());
                    if (w.ShowDialog().GetValueOrDefault() && actiePlanTabControl.SelectedItem is DXTabItem tab)
                        GetVC().AddActiePlan(w.Actieplan);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void TabActiePlan_OnSelectionChanged(object sender, TabControlSelectionChangedEventArgs e)
        {
            if (sender is DXTabControl control && control.SelectedItem is DXTabItem item)
                SetTabContent(item);
        }
        private void BarButtonCloseActieplan_ItemClick(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            if (GetVC() is ActiePlanVC vc && vc.CanCloseActiePlan)
            {
                MessageBoxResult result = MessageBox.Show("Wil je volgend actieplan aflsuiten ? " + Environment.NewLine + vc.SelectedActieplan.title, "Actieplan afsluiten", 
                                                          MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (result == MessageBoxResult.Yes)
                    vc.CloseSelectedActiePlan();
            }
        }
        private void BtnReport_Click(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            if (GetVC() is ActiePlanVC vc && vc.CanPrintActieplan)
            {
                ActiePlanPrintWindow w = new ActiePlanPrintWindow(vc)
                {
                    WindowStartupLocation = WindowStartupLocation.CenterScreen
                };
                w.ShowDialog();
            }
        }

        /** METHODS */
        private ActiePlanVC GetVC()
        {
            return this.DataContext as ActiePlanVC;
        }
        private void SetTabContent(DXTabItem tab)
        {
            if (GetVC() is ActiePlanVC vc)
            {
                DateEndToColorConverter.ignoreConvertor = false;
                    
                if (tab == tabPeriodiekeEvaluaties)
                {
                    tab.Content = new ActieplanEvaluatiesUC(ActieplanEvaluatieVersies.PERIODIEKE_EVALUATIE, GetVC().Bewoner.id);
                    barButtonNewBig.IsVisible = true;
                }
                else if (tab == tabHistoriek)
                {
                    tab.Content = new ActiePlanPartUC(vc, tab.Tag.ToString());
                    barButtonNewBig.IsVisible = false;
                    DateEndToColorConverter.ignoreConvertor = true;
                }
                else if (tab == tabContactverslagen)
                {
                    tabContactverslagen.Content = new ContactverslagUC();

                    LabelProviderEmpty labelProvider = new LabelProviderEmpty();
                    
                    labelProvider.InjectLabels(_labelsListContactverslagen);
                    ContactverslagVC contactverslagVC = new ContactverslagVC(new LabelManager(labelProvider));
                    contactverslagVC.Load(GetVC().Bewoner.id);

                    tabContactverslagen.DataContext = contactverslagVC;

                    barButtonNewBig.IsVisible = false;
                }
                else
                {
                    tab.Content = new ActiePlanPartUC(vc, tab.Tag.ToString());
                    barButtonNewBig.IsVisible = true;
                }

                vc.SelectedActieplan = null;
            }
        }
    }
}
