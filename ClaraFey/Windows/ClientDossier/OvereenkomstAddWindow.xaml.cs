﻿using ClaraFey.CommonObjects;
using sharedcode.BLL;
using sharedcode.common;
using sharedcode.WeergaveClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ClaraFey.Windows.ClientDossier
{
    /// <summary>
    /// Interaction logic for OvereenkomstAddWindow.xaml
    /// </summary>
    public partial class OvereenkomstAddWindow : Window
    {
        /** FIELDS */
        private OvereenkomstenBLL _bll;
        public OvereenkomstWeergave Overeenkomst { get; set; }
        public List<Overeenkomst_type> TypesList { get; set; }

        /** CONSTRUCTORS */
        public OvereenkomstAddWindow(int bewonerId)
        {

            _bll = new OvereenkomstenBLL();
            TypesList = _bll.GetAllOvereenkomstTypes();

            Overeenkomst_status stat = _bll.GetStartingStatusForOvereenkomst();
            Overeenkomst = new OvereenkomstWeergave()
            {
                bewoner_id = bewonerId,
                Status = stat,
                status_id = stat.id,
                vanDatum = new DateTime(2010, 1, 1)
            };
            this.DataContext = this;
            InitializeComponent();
            GlobalMethods.SetTheme(this, GlobalMethods.Themes.clientdossier);
        }

        /** EVENTS */
        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        private void BtnOk_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ValidateInput();

                Overeenkomst.type_id = Overeenkomst.Type.id;

                _bll.AddOvereenkomst(Overeenkomst);

                this.DialogResult = true;
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Foutmelding", MessageBoxButton.OK, MessageBoxImage.Error, MessageBoxResult.OK);
            }
        }

        /** METHODS */
        private void ValidateInput()
        {
            String errorMessages = "";
            if (Overeenkomst.Type == null || Overeenkomst.Type.id == 0)
                errorMessages += @"- Je moet een type selecteren!";
            if (Overeenkomst.vanDatum == null || Overeenkomst.vanDatum == DateTime.MinValue)
                errorMessages += @"
- Je moet een startdatum ingeven!";

            if (!String.IsNullOrWhiteSpace(errorMessages))
                throw new Exception(errorMessages);
        }
    }
}
