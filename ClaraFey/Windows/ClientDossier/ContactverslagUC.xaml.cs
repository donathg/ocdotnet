﻿using ClaraFey.Reports;
using ClaraFey.Reports.PrintModels;
using ClaraFey.UserControls;
using ClaraFey.ViewControllers.ClientDossier;
using ClaraFey.Windows.Common;
using DevExpress.XtraReports.UI;
using sharedcode.BLL;
using sharedcode.common;
using sharedcode.WeergaveClasses;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ClaraFey.Windows.ClientDossier
{
    /// <summary>
    /// Interaction logic for ContactverslagUC.xaml
    /// </summary>
    public partial class ContactverslagUC : UserControl
    {
        /** CONSTRUCTOR */
        public ContactverslagUC()
        {
            InitializeComponent();
        }

        /** EVENTS */
        private ContactverslagVC GetVC()
        {
            return this.DataContext as ContactverslagVC;
        }
        private void ToevoegenButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ContactverslagWeergave item = new ContactverslagWeergave(new Contactverslag())
                {
                    Bewoner = GetVC().Bewoner
                };

                ContactverslagAddAndEditPopup window = new ContactverslagAddAndEditPopup(item, GetVC().LabelManager);
                window.ShowDialog();

                GetVC().RefreshList();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Fout", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void ExportButton_Click(object sender, RoutedEventArgs e)
        {
            ContactverslagVC vc = GetVC();
            ContactverslagReportModel model = new ContactverslagReportModel()
            {
                Contactverslagen = vc.ContactverslagList.ToList()
            };
            model.CreateFilterText(vc.Filter);

            ContactverslagReport report = new ContactverslagReport(model);

            using (ReportPrintTool printTool = new ReportPrintTool(report))
            {
                printTool.ShowRibbonPreviewDialog();
            }
        }
        private void DeleteImage_MouseDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                if (sender is Image image && image.DataContext is ContactverslagWeergave contactverslag)
                {
                    if (MessageBox.Show("Ben je zeker dat je contactverslag met titel " + contactverslag.Title + " wil verwijderen?", "Contactverslag verwijderen", MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No) == MessageBoxResult.Yes)
                        GetVC().DeleteContactverslag(contactverslag);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Fout", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void EditImage_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (sender is Image image && image.DataContext is ContactverslagWeergave contactverslag)
            {
                ContactverslagAddAndEditPopup window = new ContactverslagAddAndEditPopup(contactverslag);
                window.ShowDialog();
                GetVC().RefreshList();
            }
        }
        private void LabelIcon_Click(object sender, MouseButtonEventArgs e)
        {
            try
            {
                if (sender is Image image && image.DataContext is ContactverslagWeergave contactverslag)
                {
                    List<labels> presentLabels;
                    if (GetVC().LabelManager.GetAllActiveLabels() is List<labels> list && list.Count < GlobalData.Instance.LoggedOnUser.LabelManager.GetAllActiveLabels().Count)
                    {
                        presentLabels = new List<labels>();

                        foreach (labels label in list)
                        {
                            presentLabels.Add(label);
                            presentLabels.AddRange(LabelManager.GetChildLables(label.id));
                        }
                    }
                    else
                        presentLabels = GlobalData.Instance.LoggedOnUser.LabelManager.GetAllActiveLabels();

                    presentLabels = presentLabels.Where(x => x.active == true && x.contactverslagen == true && GlobalData.Instance.LoggedOnUser.LabelManager.GetAllActiveLabels().Exists(y => y.id == x.id)).ToList();

                    if (presentLabels.Count > 0)
                    {
                        LabelsPopup labels = new LabelsPopup(
                            contactverslag.ContactverslagId,
                            new LabelConnectionProviderContactverslag(GlobalData.Instance.LoggedOnUser, new LabelProviderDataBase().Load(),
                            presentLabels)
                        );

                        if (labels.ShowDialog().GetValueOrDefault())
                        {
                            labels.ConnectionManager.SaveSelectedLabels(null);
                            contactverslag.ContactverslagLabels = new ObservableCollection<labels>(labels.ConnectionManager.SelectedLabels);
                        }
                    }
                    else
                        throw new Exception("Er zijn geen labels geselecteerd voor dit scherm!");

                    contactverslag.InitLabelsKommaSeperated();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Fout", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void ZoekButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                GetVC().RefreshList();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Fout", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void ClearFilter_Click(object sender, RoutedEventArgs e)
        {
            GetVC().ClearFilter();
        }
        private void NamelookUpEdit_KeyUp(object sender, KeyEventArgs e)
        {
            MyLookUpEdit editBox = (MyLookUpEdit)sender;
            if (editBox.DisplayText.Length == 0)
                editBox.SelectedItem = null;
        }
        private void InfoButton_Click(object sender, RoutedEventArgs e)
        {
            HelpWindow w = new HelpWindow("cd_contactverslag", "Help Contactverslagen");
            w.Show();
        }
    }
}
