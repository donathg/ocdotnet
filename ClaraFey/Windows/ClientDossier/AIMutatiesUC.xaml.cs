﻿using ClaraFey.CommonObjects;
using ClaraFey.ViewControllers;
using ClaraFey.ViewControllers.ClientDossier;
using sharedcode.BLL;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ClaraFey.Windows.ClientDossier
{
    /// <summary>
    /// Interaction logic for AIMutatiesUC.xaml
    /// </summary>
    public partial class AIMutatiesUC : UserControl
    {
        private AdministratieveInformatieVC _vc;
        private AIMutatues MutatiesVc { get; set; } = new AIMutatues();

        public AIMutatiesUC(AdministratieveInformatieVC vc)
        {
            _vc = vc;
            this.DataContext = _vc;

            InitializeComponent();

            InitMutatiesPart();
        }

        private void InitMutatiesPart()
        {
            SMMWindow2Settings settings = new SMMWindow2Settings("", GlobalMethods.Themes.clientdossier);

            SMMWindow2Level level0 = new SMMWindow2Level(0, "")
            {
                AllowDelete = true,
                AllowModify = true,
                AllowAdd = true,
                AllowSearch = true,
                AllowFilter = true,
                CanExportToExcel = true
            };
            level0.AddColumns(
                new SMMWindow2Column() { FieldName = "id", Visible = false, IsEditable = false },
                new SMMWindow2Column() { FieldName = "bewoner_id", DefaultValue = _vc.BewonerId, Visible = false, IsEditable = true },
                new SMMWindow2Column() { FieldName = "datumVan", ColumnHeader = "Startdatum", Visible = true, IsEditable = true },
                new SMMWindow2Column() { FieldName = "datumTot", ColumnHeader = "Einddatum", Visible = true, IsEditable = true },
                new SMMWindow2Column() { FieldName = "ondersteuning", ColumnHeader = "Ondersteuning", Visible = true, IsEditable = true },
                new SMMWindow2Column() { FieldName = "zorgaanbieder", ColumnHeader = "Zorgaanbieder", Visible = true, IsEditable = true }
            );

            level0.DataSource = new ObservableCollection<Cd_mutaties>(MutatiesVc.Entity.Set<Cd_mutaties>().Where(x => x.bewoner_id == _vc.BewonerId).ToList());
            settings.AddLevel(level0);

            MutatiesVc.Settings = settings;

            // direct binding doesn't work so need to set datacontext after initializecomponent-method of UC
            smmUC.DataContext = MutatiesVc;
        }
    }
}
