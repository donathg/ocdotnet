﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ClaraFey.ViewControllers.ClientDossier;
using sharedcode.BLL;
using DevExpress.Xpf.Editors;
using sharedcode.common;
using DevExpress.Xpf.Grid;
using System.Windows.Controls.Primitives;
using ClaraFey.UserControls;
using System.Collections.ObjectModel;
using ClaraFey.Reports;
using ClaraFey.Reports.PrintModels;
using DevExpress.XtraReports.UI;
using ClaraFey.Windows.Common;

namespace ClaraFey.Windows.ClientDossier
{
    /// <summary>
    /// Interaction logic for DagboekUC.xaml
    /// </summary>
    public partial class DagboekUC : UserControl
    {
        /** CONSTRUCTOR */
        public DagboekUC()
        { 
            InitializeComponent();
        }

        /** EVENTS */
        private void AddDagboekItem(object sender, RoutedEventArgs e)
        {
            try
            {
                DagboekVC vc = GetVC();

                List<labels> presentLabels;
                if (GetVC().LabelManager.GetAllActiveLabels() is List<labels> list && list.Count < GlobalData.Instance.LoggedOnUser.LabelManager.GetAllActiveLabels().Count)
                {
                    presentLabels = new List<labels>();

                    foreach (labels label in list)
                    {
                        presentLabels.Add(label);
                        presentLabels.AddRange(LabelManager.GetChildLables(label.id));
                    }
                }
                else
                    presentLabels = GlobalData.Instance.LoggedOnUser.LabelManager.GetAllActiveLabels();

                presentLabels = presentLabels.Where(x => x.active == true && x.dagboek == true && GlobalData.Instance.LoggedOnUser.LabelManager.GetAllActiveLabels().Exists(y => y.id == x.id)).ToList();

                if (presentLabels.Count > 1)
                {
                    LabelsPopup labels = new LabelsPopup(new LabelConnectionProviderDagboek(GlobalData.Instance.LoggedOnUser, new LabelProviderDataBase().Load(), presentLabels));
                    if (labels.ShowDialog().GetValueOrDefault())
                        vc.AddDagboekItem(labels.ConnectionManager.SelectedLabels);
                }
                else
                    vc.AddDagboekItem(presentLabels);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Fout", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void ClearFilter_Click(object sender, RoutedEventArgs e)
        {
            DagboekVC vc = GetVC();

            vc.SetDefaultFilter();
            vc.NotifiyPropertyChanged(nameof(vc.Filter));
        }
        private void DeleteImage_MouseDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                MessageBoxResult messageBox = MessageBox.Show("Ben je zeker dat je dit dagboekitem wil verwijderen voor iedereen?", "Verwijderen dagboekitem", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (messageBox == MessageBoxResult.Yes)
                {
                    DagboekWeergave dagBoek = (sender as Image).DataContext as DagboekWeergave;
                    GetVC().DeleteItem(dagBoek);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Fout", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void EditImage_MouseDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                DagboekWeergave dagBoek = (sender as Image).DataContext as DagboekWeergave;

                GetVC().EditDagboek(dagBoek);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Fout", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void ExportButton_Click(object sender, RoutedEventArgs e)
        {
            DagboekReport report = new DagboekReport(new DagboekReportModel(GetVC()));

            using (ReportPrintTool printTool = new ReportPrintTool(report))
            {
                printTool.ShowRibbonPreviewDialog();
            }
        }
        private void FavorietenButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Image image = (Image)sender;
                var dagBoek = image.DataContext as DagboekWeergave;

                GetVC().ChangeFavorietStatus(dagBoek);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Fout", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void LabelIcon_Click(object sender, MouseButtonEventArgs e)
        {
            try
            {
                Image image = (Image)sender;

                List<labels> allUserLabels;
                if (GetVC().LabelManager.GetAllActiveLabels() is List<labels> list && list.Count < GlobalData.Instance.LoggedOnUser.LabelManager.GetAllActiveLabels().Count)
                {
                    allUserLabels = new List<labels>();

                    foreach (labels label in list)
                    {
                        allUserLabels.Add(label);
                        allUserLabels.AddRange(LabelManager.GetChildLables(label.id));
                    }
                }
                else
                    allUserLabels = GlobalData.Instance.LoggedOnUser.LabelManager.GetAllActiveLabels();

                allUserLabels = allUserLabels.Where(x => x.active == true && x.dagboek == true && GlobalData.Instance.LoggedOnUser.LabelManager.GetAllActiveLabels().Exists(y => y.id == x.id)).ToList();

                DagboekWeergave dagBoek = image.DataContext as DagboekWeergave;

                LabelsPopup labels = new LabelsPopup(
                    dagBoek.DagboekId, 
                    new LabelConnectionProviderDagboek(GlobalData.Instance.LoggedOnUser, new LabelProviderDataBase().Load(),
                    allUserLabels)
                );
                if (labels.ShowDialog().GetValueOrDefault())
                {
                    labels.ConnectionManager.SaveSelectedLabels(null);
                    dagBoek.DagboekLabels = new ObservableCollection<labels>(labels.ConnectionManager.SelectedLabels);
                    dagBoek.NotifiyPropertyChanged(nameof(dagBoek.DagboekLabels));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Fout", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void LeefgroepOfBewonerToevoegen_Click(object sender, RoutedEventArgs e)
        {
            LeefgroepBewonersTree win = new LeefgroepBewonersTree(CommonObjects.GlobalMethods.Themes.clientdossier,true);
            win.ShowDialog();

            if (win.SelectedItem is LeefgroepBewonerTreeItem treeItem)
            {
                GetVC().SelectedItem = treeItem.Object;
            }
        }
        private void LeefgroepOrBewonerZoeken_Click(object sender, RoutedEventArgs e)
        {
            LeefgroepBewonersTree win = new LeefgroepBewonersTree(CommonObjects.GlobalMethods.Themes.clientdossier,true);
            win.ShowDialog();

            if (win.SelectedItem is LeefgroepBewonerTreeItem treeItem)
            {
                DagboekVC vc = GetVC();
                if (treeItem.Object is leefgroep groep)
                {
                    vc.Filter.Bewoner = null;
                    vc.Filter.GekozenLeefgroep = groep;
                }
                else if (treeItem.Object is bewoner bew)
                {
                    vc.Filter.GekozenLeefgroep = null;
                    vc.Filter.Bewoner = bew;
                }
                vc.Filter.NotifiyPropertyChanged(nameof(vc.Filter.NaamBewonerOfLeefgroep));
            }
        }
        private void NamelookUpEdit_KeyUp(object sender, KeyEventArgs e)
        {
            MyLookUpEdit editBox = (MyLookUpEdit)sender;
            if (editBox.DisplayText.Length == 0)
            {
                editBox.SelectedItem = null;
            }
        }
        private void ToggleSwitch_Checked(object sender, RoutedEventArgs e)
        {
            try
            {
                GetVC().VeranderViewType(true);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Fout", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void ToggleSwitch_Unchecked(object sender, RoutedEventArgs e)
        {
            try
            {
                /** NOODZAKELIJK!!!
                 * 
                 *  wanneer je van UserControl verandert en de view staat op favorieten,
                 *  verandert hij de toggleswitch automatisch terug naar unchecked en dan wordt deze event opgeroepen.
                 *  Op dat moment heeft hij geen ViewControl meer => if vc == null, niets doen
                 **/
                if (GetVC() is DagboekVC vc)
                    vc.VeranderViewType(false);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Fout", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            ViewToggleSwitch.IsChecked = false;
        }
        private void ZoekButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                GetVC().FilterZoekResults();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Fout", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        /** METHODS */
        private DagboekVC GetVC()
        {
            return this.DataContext as DagboekVC;
        }
    }
}
