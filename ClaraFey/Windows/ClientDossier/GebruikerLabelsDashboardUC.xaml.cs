﻿using ClaraFey.ViewControllers.ClientDossier;
using ClaraFey.ViewControllers.Common;
using ClaraFey.Windows.Common;
using DevExpress.Xpf.Core;
using sharedcode.BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using static ClaraFey.CommonObjects.GlobalMethods;

namespace ClaraFey.Windows.ClientDossier
{
    /// <summary>
    /// Interaction logic for GebruikerLabelsDashboardUC.xaml
    /// </summary>
    public partial class GebruikerLabelsDashboardUC : UserControl
    {
        /** CONSTRUCTOR */
        public GebruikerLabelsDashboardUC()
        {
            InitializeComponent();
        }

        /** EVENTS */
        private void UserControl_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            tabControl.Items.Clear();
            if (GetVC() is GebruikerLabelsDashboardVC vc)
            {
                if (vc.GetAllBijlagenLabels().Count != 0)
                    tabControl.Items.Add(new DXTabItem() { Header = "Bijlagen", Name = "tabBijlagen" });

                if (vc.GetAllDagboekLabels().Count != 0)
                    tabControl.Items.Add(new DXTabItem() { Header = "Dagboek", Name = "tabDagboek" });

                if (vc.GetAllContactverslagenLabels().Count != 0)
                    tabControl.Items.Add(new DXTabItem() { Header = "Contactverslagen", Name = "tabContactverslagen" });

                if (tabControl.HasItems)
                {
                    tabControl.SelectFirst();
                    SetSelectedTabcontent(tabControl.SelectedContainer);
                }
            }
        }
        private void BiShowHelp_ItemClick(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            HelpWindow w = new HelpWindow("cd_labels", "Help labelscherm");
            w.Show();
        }
        private void TabControl_SelectionChanged(object sender, TabControlSelectionChangedEventArgs e)
        {
            if (sender is DXTabControl control && control.SelectedItem is DXTabItem newTab)
            {
                SetSelectedTabcontent(newTab);
            }
        }

        /** METHODS */
        private GebruikerLabelsDashboardVC GetVC()
        {
            return this.DataContext as GebruikerLabelsDashboardVC;
        }
        private void SetSelectedTabcontent(DXTabItem tab)
        {
            if (GetVC() is GebruikerLabelsDashboardVC vc)
            {
                if (tab.Name == "tabBijlagen")
                {
                    vc.SetBijlagentabContent();
                    tab.Content = new UploadFileControl(Themes.clientdossier,  vc.BijlagenVC);
                }
                else if (tab.Name == "tabDagboek")
                {
                    vc.SetDagboektabContent();
                    tab.Content = new DagboekUC();
                    tab.DataContext = vc.DagboekVC;
                }
                else if (tab.Name == "tabContactverslagen")
                {
                    vc.SetContactverslagentabContent();
                    tab.Content = new ContactverslagUC();
                    tab.DataContext = vc.ContactverslagenVC;
                }
                else
                    MessageBox.Show("Er is geen geldige tab geselecteerd", "Ongeldige Tab", MessageBoxButton.OK, MessageBoxImage.Error, MessageBoxResult.OK);
            }
        }
    }
}
