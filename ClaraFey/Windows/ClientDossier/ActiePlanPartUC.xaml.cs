﻿using System;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using ClaraFey.ViewControllers.ClientDossier;
using ClaraFey.Windows.Common;
using sharedcode.BLL;
using sharedcode.DAL;
using sharedcode.WeergaveClasses;
using MessageBox = System.Windows.MessageBox;
using UserControl = System.Windows.Controls.UserControl;

namespace ClaraFey.Windows.ClientDossier
{
    /// <summary>
    /// Interaction logic for ActiePlanPartUC.xaml
    /// </summary>
    public partial class ActiePlanPartUC : UserControl
    {
        /** FIELDS */
        private ActiePlanVC _vc;

        /** CONSTRUCTOR */
        public ActiePlanPartUC (ActiePlanVC vc, string mainType)
        {
            this._vc = vc;
            this.DataContext = _vc;
            _vc.SetMainCategory(mainType);

            InitializeComponent();
        }

        /** EVENTS */
        private void BiShowHelp_ItemClick(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            HelpWindow w = new HelpWindow("cd_actieplanOpvolging", "Help opvolging actieplan");
            w.Show();
        }
        private void NewOpvolging_Click(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            if (_vc != null && _vc.SelectedActieplan != null)
            {
                ActieplanPopupOpvolging w = new ActieplanPopupOpvolging(_vc.SelectedActieplan)
                {
                    WindowStartupLocation = WindowStartupLocation.CenterScreen
                };
                if (w.ShowDialog().GetValueOrDefault())
                    _vc.AddOpvolging(w.Opvolging);
            }
        }
        private void MiAfsluitenActiePlan_Click(object sender, RoutedEventArgs e)
        {
            if (_vc != null && _vc.CanCloseActiePlan)
            {
                MessageBoxResult result = MessageBox.Show("Wil je volgend actieplan aflsuiten ? " + Environment.NewLine + _vc.SelectedActieplan.title, "Actieplan afsluiten",
                                                          MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (result == MessageBoxResult.Yes)
                    _vc.CloseSelectedActiePlan();
            }
        }
        private void MiPrintActiePlan_Click(object sender, RoutedEventArgs e)
        {
            if (_vc != null)
            {
                ActiePlanPrintWindow w = new ActiePlanPrintWindow(_vc)
                {
                    WindowStartupLocation = WindowStartupLocation.CenterScreen
                };
                w.ShowDialog();
            }
        }
        private void MiEditActiePlan_Click(object sender, RoutedEventArgs e)
        {
            ModifyActiePlan();
        }
        private void EditOpvolgingButton_Click(object sender, RoutedEventArgs e)
        {
            if (_vc != null && sender is System.Windows.Controls.Button but && but.Tag is ActieplanOpvolgingWeergave opvolging)
            {
                opvolging.actieplan = _vc.SelectedActieplan;

                ActieplanPopupOpvolging w = new ActieplanPopupOpvolging(opvolging)
                {
                    WindowStartupLocation = WindowStartupLocation.CenterScreen
                };

                if (w.ShowDialog().GetValueOrDefault())
                    _vc.EditOpvolging(w.Opvolging);
            }
        }
        private void GridControl_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            ModifyActiePlan();
        }

        /** METHODS */
        private void ModifyActiePlan()
        {
            if (_vc != null &&  _vc.SelectedActieplan != null )
            {
                if (_vc.CanAddEditDelete)
                {
                    WindowNewActiePlan w = new WindowNewActiePlan(_vc, _vc.SelectedActieplan);

                    if (w.ShowDialog().GetValueOrDefault())
                        _vc.ModifyActiePlan(w.Actieplan);
                }
                else
                {
                    ActieplanEvaluatieAddShow popup = new ActieplanEvaluatieAddShow(new ActieplanEvaluatiesAddModifyWindowSettings()
                    {
                        Versie = ActieplanEvaluatieVersies.EINDEVALUATIE,
                        SelectedEvaluatie = ActieplanEvaluatiesBLL.GetEindevaluatieFromActieplanId(_vc.SelectedActieplan.id),
                        BewonerId = _vc.Bewoner.id
                    });
                    popup.ShowDialog();
                }
            }
        }
    }

    /***** BETER IN CONVERTORS KLASSE? *****/
    public class DateEndToColorConverter : MarkupExtension, IValueConverter
    {
        public static bool ignoreConvertor = false;

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!ignoreConvertor && value != null)
            {
                DateTime dt = (DateTime)value;
                
                if (dt.Date == DateTime.Now.Date)
                    return Brushes.DarkOrange;
                else if (dt.Date < DateTime.Now.Date)
                    return Brushes.Crimson;
                else if (dt.Date <= DateTime.Now.Date.AddDays(+7))
                    return Brushes.Tomato;
                else if (dt.Date <= DateTime.Now.Date.AddDays(+14))
                    return Brushes.Orange;
            }
            return Brushes.White;
        }
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            // TODO Not used yet
            return null;
        }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }
    }
    public class DateEndToForeColorConverter : MarkupExtension, IValueConverter
    {
        public static bool ignoreConvertor = false;

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!ignoreConvertor && value != null)
            {
                DateTime dt = (DateTime)value;

                if (dt.Date <= DateTime.Now.Date)
                    return Brushes.White;
            }
            return Brushes.Black;
        }
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            // TODO Not used yet
            return null;
        }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }
    }
}
