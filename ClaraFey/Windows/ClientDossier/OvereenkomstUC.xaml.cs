﻿using ClaraFey.ViewControllers.ClientDossier;
using ClaraFey.Windows.Common;
using DevExpress.Xpf.Bars;
using DevExpress.Xpf.Core;
using sharedcode.BLL;
using sharedcode.common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ClaraFey.Windows.ClientDossier
{
    /// <summary>
    /// Interaction logic for OvereenkomstUC.xaml
    /// </summary>
    public partial class OvereenkomstUC : UserControl
    {
        private DXTabItem _tabToelatingen;
        private DXTabItem _tabContactverslagen;
        private List<labels> _labelListContactverslagen = new List<labels>()
        {
            LabelManager.GetLabelById(34)
        };

        public OvereenkomstUC()
        {
            InitializeComponent();
        }

        /** EVENTS */
        private void OvereenkomstUC_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                foreach (Overeenkomst_type type in GetVC().TypeList)
                {
                    TabcontrolOvereenkomsten.Items.Add(new DXTabItem() { Header = type.naam, Name = type.naam.Replace("-", "").Replace(" ","") + "Tab", Tag = type });
                }
                TabcontrolOvereenkomsten.SelectFirst();

                if (TabcontrolOvereenkomsten.SelectedItem is DXTabItem item)
                    SetTabContent(item);
                if (GlobalData.Instance.LoggedOnUser.HasAccess("1370"))
                {
                    _tabToelatingen = new DXTabItem() { Header = "Toelatingen", Name = "tabToelatingen", Tag = "Toelatingen" };
                    TabcontrolOvereenkomsten.Items.Add(_tabToelatingen);
                }

                if (_labelListContactverslagen.FindAll(x => x.contactverslagen == true).Count != 0)
                {
                    _tabContactverslagen = new DXTabItem() { Header = "Contactverslagen", Name = "tabContactverslagen", Tag = "Contactverslagen" };
                    TabcontrolOvereenkomsten.Items.Add(_tabContactverslagen);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Foutmelding", MessageBoxButton.OK, MessageBoxImage.Error, MessageBoxResult.OK);
            }
        }
        private void TabcontrolOvereenkomsten_SelectionChanged(object sender, TabControlSelectionChangedEventArgs e)
        {
            try
            {
                if (sender is DXTabControl control && control.SelectedItem is DXTabItem item)
                    SetTabContent(item);
                else
                    throw new Exception("Er is een fout opgetreden bij het veranderen van de tabcontent");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Foutmelding", MessageBoxButton.OK, MessageBoxImage.Error, MessageBoxResult.OK);
            }
        }
        private void BiShowHelp_ItemClick(object sender, ItemClickEventArgs e)
        {
            try
            {
                HelpWindow w = new HelpWindow("cd_overeenkomsten", "Help overeenkomsten");
                w.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Foutmelding", MessageBoxButton.OK, MessageBoxImage.Error, MessageBoxResult.OK);
            }
        }
        private void BarButtonDeleteBig_ItemClick(object sender, ItemClickEventArgs e)
        {
            try
            {
                if (TabcontrolOvereenkomsten.SelectedItem is DXTabItem item && item.Content is OvereenkomstPartUC uc && uc.DataContext is OvereenkomstenPartVC subVC)
                {
                    GetVC().RemoveOvereenkomst(subVC.SelectedOvereenkomst);
                    subVC.RefreshList();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Foutmelding", MessageBoxButton.OK, MessageBoxImage.Error, MessageBoxResult.OK);
            }
        }
        private void BarButtonNewBig_ItemClick(object sender, ItemClickEventArgs e)
        {
            OvereenkomstAddWindow window = new OvereenkomstAddWindow(GetVC().Bewoner.id);

            if (window.ShowDialog().GetValueOrDefault())
                SetTabContent(TabcontrolOvereenkomsten.SelectedContainer);
        }

        /** METHODS */
        private OvereenkomstenVC GetVC()
        {
            return this.DataContext as OvereenkomstenVC;
        }
        private void SetTabContent(DXTabItem item)
        {
            if (GetVC() is OvereenkomstenVC vc)
            {
                if (TabcontrolOvereenkomsten.SelectedContainer.Header.ToString() == "Toelatingen")
                {
                    _tabToelatingen.Content = new toelatingenUC(GetVC());
                    vc.PartVC = null;
                }
                else if (TabcontrolOvereenkomsten.SelectedContainer.Header.ToString() == "Contactverslagen")
                {
                    vc.PartVC = null;
                    
                    _tabContactverslagen.Content = new ContactverslagUC();

                    LabelProviderEmpty labelProvider = new LabelProviderEmpty();
                    labelProvider.InjectLabels(_labelListContactverslagen);
                    ContactverslagVC contactverslagVC = new ContactverslagVC(new LabelManager(labelProvider));
                    contactverslagVC.Load(GetVC().Bewoner.id);

                    _tabContactverslagen.DataContext = contactverslagVC;
                }
                else
                    GetVC().SetTabContent(item);


                vc.NotifiyPropertyChanged(nameof(vc.CanRemoveOvereenkomst));
                vc.NotifiyPropertyChanged(nameof(vc.CanAddOvereenkomst));
            }
        }
    }
}
