﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ClaraFey.ViewControllers.ClientDossier;
using sharedcode.common;
using ClaraFey.UserControls;
using sharedcode.BLL;
using ClaraFey.CommonObjects;

namespace ClaraFey.Windows.ClientDossier
{
    /// <summary>
    /// Interaction logic for Dashboard.xaml
    /// </summary>
    public partial class DashboardUC : UserControl
    {
        // initialiseerd
        public DashboardUC()
        {
            GlobalMethods.WriteToLog("DashboardUC");
            InitializeComponent();
        }


        /***** METHODS SONNY *****/
        // geeft het DashboardVC-object dat in gebruik is terug
        private DashboardVC GetVC()
        {
            GlobalMethods.WriteToLog("DashboardUC.GetVC");
            DashboardVC vc = this.DataContext as DashboardVC;
            return vc;
        }

        // wissen van lookupeditbox wanneer het zoekveld leeg is
        private void NamelookUpEdit_KeyUp(object sender, KeyEventArgs e)
        {
            MyLookUpEdit editBox = (MyLookUpEdit)sender;
            if (editBox.DisplayText.Length == 0)
            {
                editBox.SelectedItem = null;
            }
        }

        // toont de zoekresultaten
        private void ZoekButton_Click(object sender, RoutedEventArgs e)
        {
            DashboardVC vc = GetVC();

            vc.FilterZoekResults();
        }

        // verandert de favorietstatus van een dagboek item
        private void FavorietenButton_Click(object sender, RoutedEventArgs e)
        {
            DashboardVC vc = GetVC();
            Image image = (Image)sender;
            dashboard dashboard = image.DataContext as dashboard;

            vc.ChangeFavorietStatus(dashboard);
        }

        /** methods voor de view te veranderen tussen favorieten en alle items **/
        // verandert naar favorieten
        private void ToggleSwitch_Checked(object sender, RoutedEventArgs e)
        {
            DashboardVC vc = GetVC();
            vc.VeranderViewType(true);
        }
        // verandert naar alle items
        private void ToggleSwitch_Unchecked(object sender, RoutedEventArgs e)
        {
            DashboardVC vc = GetVC();

            /** NOODZAKELIJK!!!
             * 
             *  wanneer je van UserControl verandert en de view staat op favorieten,
             *  verandert hij de toggleswitch automatisch terug naar unchecked en dan wordt deze event opgeroepen.
             *  Op dat moment heeft hij geen ViewControl meer => if vc == null, niets doen
             **/
            if (vc != null)
            {
                vc.VeranderViewType(false);
            }
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {

        }
    }
}
