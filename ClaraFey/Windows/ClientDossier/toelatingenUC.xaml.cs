﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ClaraFey.ViewControllers.ClientDossier;
using ClaraFey.Windows.Common;
using sharedcode.BLL;
using ToggleSwitch;
using ClaraFey.CommonObjects;

namespace ClaraFey.Windows.ClientDossier
{
    /// <summary>
    /// Interaction logic for toelatingenUC.xaml
    /// </summary>
    public partial class toelatingenUC : UserControl
    {
        OvereenkomstenVC _vc;

        public toelatingenUC(OvereenkomstenVC vc)
        {
            this._vc = vc;
            this.DataContext = _vc;
            InitializeComponent();
        }
        public toelatingenUC()
        {
            InitializeComponent();
        }

        private void barButtonNew_ItemClick(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            String resultText;
            bool result = GetInputText("Nieuwe toelating",String.Empty, out resultText);
            if (result)
                this._vc.AddCustomToelating(resultText);
        }

        private static bool GetInputText(String windowTitle, String defaultText,out String resultText)
        {
            PopupRemark window = new PopupRemark(windowTitle, "OK", defaultText,GlobalMethods.Themes.clientdossier);

            window.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            if (window.ShowDialog().GetValueOrDefault())
            {
                resultText = window.Opmerking;
                return true;
            }
            resultText = String.Empty;
            return false;
        }

        private void ButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            if (sender is Button btn && btn.Tag is myToelating toelating && GetInputText("Toelating wijzigen", toelating.Title, out string resultText))
            {
                toelating.Title = resultText;
                Save(toelating);
            }
        }

        private void ButtonAddRemark_Click(object sender, RoutedEventArgs e)
        {
            if (sender is Button btn)
            {
                myToelating toelating = (myToelating)btn.Tag;
                bool result = GetInputText("Opmerking toevoegen/wijzigen", toelating.Remark, out string resultText);
                if (result)
                {
                    toelating.Remark = resultText;
                    Save(toelating);
                }
            }
        }

        private void Save(myToelating toelating)
        {
            try
            {
                this._vc.ModifyCustomToelating(toelating);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Fout", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ToggleSwitchBase_OnChecked(object sender, RoutedEventArgs e)
        {
            ToggleSwitchBase btn = sender as ToggleSwitchBase;
            if (btn != null)
            {
                myToelating toelating = (myToelating)btn.Tag;
                Save(toelating);
            }
        }

        private void ToggleSwitchBase_OnUnchecked(object sender, RoutedEventArgs e)
        {
            ToggleSwitchBase btn = sender as ToggleSwitchBase;
            if (btn != null)
            {
                myToelating toelating = (myToelating)btn.Tag;
                Save(toelating);
            }
        }
        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (sender is Button btn && btn.Tag is myToelating toelating)
                {
                    if (MessageBox.Show("Ben je zeker dat je deze gepersonaliseerde toelating wil verwijderen", "Ben je zeker?", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
                        _vc.DeleteCustomToelating(toelating);
                }
                else
                    throw new Exception("Er is een fout opgetreden");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error, MessageBoxResult.OK);
            }
        }
    }
}
