﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ClaraFey.CommonObjects;
using ClaraFey.ViewControllers.ClientDossier;
using sharedcode.BLL;
using sharedcode.WeergaveClasses;

namespace ClaraFey.Windows.ClientDossier
{
    /// <summary>
    /// Interaction logic for windowNewActiePlan.xaml
    /// </summary>
    public partial class WindowNewActiePlan : Window, INotifyPropertyChanged
    {
        /** FIELDS */
        public event PropertyChangedEventHandler PropertyChanged;

        private ActiePlanVC _vc;
        public ActieplanWeergave Actieplan { get; set; }
        public List<actieplantypes> ActieplanTypesList
        {
            get
            {
                return _vc.ActiePlanTypesList;
            }
        }
        public List<int> SelectedTypesList { get; set; }

        /** CONSTRUCTOR */
        public WindowNewActiePlan(ActiePlanVC vc) // creates actieplanweergave
        {
            _vc = vc;
            Actieplan = new ActieplanWeergave()
            {
                bewonerid = _vc.Bewoner.id,
                category = _vc.MainCategory,
                Actieplantypes = new List<actieplantypes>()
            };
            this.DataContext = this;

            InitializeComponent();
            btnOk.Content = "Toevoegen";
            GlobalMethods.SetTheme(this, GlobalMethods.Themes.clientdossier);
        }
        public WindowNewActiePlan(ActiePlanVC vc, ActieplanWeergave ap): this(vc) // edits actiplanweergave
        {
            if (ap != null)
            {
                Actieplan = ap;
                SelectedTypesList = new List<int>();
                foreach (actieplantypes type in ap.Actieplantypes)
                {
                    SelectedTypesList.Add(type.id);
                }
                NotifiyPropertyChanged(nameof(SelectedTypesList));
                NotifiyPropertyChanged(nameof(Actieplan));
                btnOk.Content = "Aanpassen";
            }
        }

        /** EVENTS */
        private void BtnOk_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Actieplan.Actieplantypes = listTypes.SelectedItems.Cast<actieplantypes>().ToList();

                if (String.IsNullOrWhiteSpace(Actieplan.title) || !Actieplan.deadline.HasValue || Actieplan.Actieplantypes.Count == 0)
                    throw new Exception("Gelieve alle velden in te vullen !");
                else if (Actieplan.deadline.Value < DateTime.Today)
                    throw new Exception("Gelieve een datum uit te kiezen later dan vandaag");

                this.DialogResult = true;
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Je hebt verkeerd ingevuld", MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK);
            }
        }
        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            this.Close();
        }

        /** EVENTS */
        private void NotifiyPropertyChanged(string property)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(property));
        }
    }
}
