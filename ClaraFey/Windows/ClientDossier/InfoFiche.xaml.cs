﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ClaraFey.ViewControllers.ClientDossier;
using sharedcode.BLL;
using DevExpress.Xpf.Editors;
using sharedcode.common;
using DevExpress.Xpf.Grid;
using System.Windows.Controls.Primitives;
using ClaraFey.UserControls;
using System.Collections.ObjectModel;
using ClaraFey.Reports;
using ClaraFey.Reports.PrintModels;
using DevExpress.XtraReports.UI;
using ClaraFey.Windows.Common;
using ClaraFey.CommonObjects;
using ClaraFey.ViewControllers.UserControls;

namespace ClaraFey.Windows.ClientDossier
{
    /// <summary>
    /// Interaction logic for DagboekUC.xaml
    /// </summary>
    public partial class InfoFiche : UserControl
    {
        /** CONSTRUCTOR */
        public InfoFiche()
        { 
            InitializeComponent();
            this.Loaded += InfoFiche_Loaded; this.DataContextChanged += InfoFiche_DataContextChanged;
        }

        private void InfoFiche_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            BewonerChanged();
        }

        private void InfoFiche_Loaded(object sender, RoutedEventArgs e)
        {
            InfoFicheVC vc = GetVC();

            AutoSaveMultiCheckboxClientDossierGroup Group = new AutoSaveMultiCheckboxClientDossierGroup();
            Group.RegisterChildUCs(AutoSaveMultiCheckBoxClientInformatie.Vc);
            Group.RegisterChildUCs(AutoSaveMultiCheckBoxMedischeInformatie.Vc);
            Group.RegisterChildUCs(AutoSaveMultiCheckBoxPartners.Vc);
            Group.RegisterChildUCs(AutoSaveMultiCheckBoxGedrag.Vc);
            Group.RegisterChildUCs(AutoSaveMultiCheckBoxGezin.Vc);
            Group.RegisterChildUCs(AutoSaveMultiCheckBoxSchool.Vc);
            vc.RegisterChildUCs(Group);
            vc.RegisterChildUCs(this.AdressButtonDomicilie.Vc);
            vc.RegisterChildUCs(this.AdressButtonSchool.Vc);

            vc.RegisterChildUCs(this.AdressButtonGezinsleden.Vc);
            vc.BewonerChanged = BewonerChanged;


        }


        private void  BewonerChanged()
        {
            InfoFicheVC vc = GetVC();
            BewonerSetPicture(vc);

        }


        private InfoFicheVC GetVC()
        {
            return this.DataContext as InfoFicheVC;
        }
        private void BewonerSetPicture(InfoFicheVC _vc)
        {
           
            ImageEdit.Source = null;
            if (_vc != null && _vc.Bewoner.GetFoto() != null)
            {
                try
                {
                    ImageEdit.Source = GlobalSharedMethods.DecodePhoto(_vc.Bewoner.GetFoto());
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Fout", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }

            
        }

        private void ButtonLeefgroep_Click(object sender, RoutedEventArgs e)
        {

            InfoFicheVC _vc = GetVC();

            if (_vc != null)
            {
                LeefgroepBewonersTree win = new LeefgroepBewonersTree(GlobalMethods.Themes.clientdossier, _vc.Bewoner.id);
                if (win.ShowDialog().GetValueOrDefault())
                {

                }
            }
        }
    }
}
