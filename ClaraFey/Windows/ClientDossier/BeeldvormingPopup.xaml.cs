﻿using ClaraFey.CommonObjects;
using ClaraFey.ViewControllers.ClientDossier;
using DevExpress.Xpf.Grid;
using sharedcode.BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using sharedcode.common;
using ToggleSwitch;

namespace ClaraFey.Windows.ClientDossier
{
    /// <summary>
    /// Interaction logic for BeeldvormingPopup.xaml
    /// </summary>
    public partial class BeeldvormingPopup : Window
    {
        BeeldvormingVC _vc;

        public BeeldvormingPopup(BeeldvormingVC vc)
        {
          //  vc.SelectLatestHeader();
            this._vc = vc;
            this.DataContext = _vc;
            GridControl.AllowInfiniteGridSize = true;
            InitializeComponent();
            GlobalMethods.SetTheme(this, GlobalMethods.Themes.clientdossier);
            this.Title = _vc.SelectedTreeItem.name;
            this.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            this.Width = (int) (double) System.Windows.SystemParameters.PrimaryScreenWidth/100*90;
            this.Height = (int) (double) System.Windows.SystemParameters.PrimaryScreenHeight/100*90;
            this.Loaded += BeeldvormingPopup_Loaded;
        }

        private void BeeldvormingPopup_Loaded(object sender, RoutedEventArgs e)
        {
            if (_vc.Bewoner != null && _vc.Bewoner.GetFoto() != null)
            {
                try
                {
                    ImageEdit.Source = GlobalSharedMethods.DecodePhoto(_vc.Bewoner.GetFoto());
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Fout", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }

        }
        private void btnOk_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this._vc.SaveBeeldvorming();
                this.DialogResult = true;
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Fout", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            this.Close();
        }
    }
}
