﻿using ClaraFey.CommonObjects;
using ClaraFey.ViewControllers.ClientDossier;
using sharedcode.BLL;
using sharedcode.common;
using sharedcode.WeergaveClasses;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ClaraFey.Windows.ClientDossier
{
    /// <summary>
    /// Interaction logic for ContactverslagAddAndEditPopup.xaml
    /// </summary>
    public partial class ContactverslagAddAndEditPopup : Window, INotifyPropertyChanged
    {
        /** FIELDS */
        public event PropertyChangedEventHandler PropertyChanged;
        
        /// <summary>
        /// <para>if true: add new contactverslag</para>
        /// <para>if false: edit existing contactverslag</para>
        /// </summary>
        private bool _isAddMode;
        private ContactverslagWeergave _contactverslag;
        public ContactverslagWeergave Contactverslag
        {
            get
            {
                return _contactverslag;
            }
            set
            {
                _contactverslag = value;
                NotifiyPropertyChanged(nameof(Contactverslag));
            }
        }
        public ObservableCollection<Contactverslag_SoortBegeleiding> SoortBegeleidingList { get; set; }
        private Contactverslag_SoortBegeleiding _selectedSoortBegeleiding;
        public Contactverslag_SoortBegeleiding SelectedSoortBegeleiding
        {
            get
            {
                return _selectedSoortBegeleiding;
            }
            set
            {
                _selectedSoortBegeleiding = value;

                NotifiyPropertyChanged(nameof(SelectedSoortBegeleiding));
            }
        }
        private LabelManager _labelManager;

        /** CONSTRUCTOR */
        public ContactverslagAddAndEditPopup(ContactverslagWeergave weergave)
        {
            ContactverslagBLL bll = new ContactverslagBLL();

            SoortBegeleidingList = new ObservableCollection<Contactverslag_SoortBegeleiding>(bll.GetAllActiveSoortBegeleidingen());
            _isAddMode = weergave.ContactverslagId == 0;

            this.DataContext = this;
            GlobalMethods.SetTheme(this, GlobalMethods.Themes.clientdossier);

            Contactverslag = weergave;
            SelectedSoortBegeleiding = weergave.SoortBegeleiding;
            _labelManager = GlobalData.Instance.LoggedOnUser.LabelManager;

            InitializeComponent();
        }
        public ContactverslagAddAndEditPopup(ContactverslagWeergave weergave, LabelManager labelManager) : this(weergave)
        {
            _labelManager = labelManager;
        }

        /** EVENTS */
        private void Window_StateChanged(object sender, EventArgs e)
        {
            if (this.WindowState == WindowState.Normal)
            {
                this.Left = (SystemParameters.PrimaryScreenWidth - this.Width) / 2;
                this.Top = (SystemParameters.PrimaryScreenHeight - this.Height) / 2;
            }
        }
        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (SelectedSoortBegeleiding.id != 0 && !String.IsNullOrWhiteSpace(Contactverslag.Aanwezigen) &&
                    !String.IsNullOrWhiteSpace(Contactverslag.Afspraken) && !String.IsNullOrWhiteSpace(Contactverslag.Title))
                {
                    Contactverslag.SoortBegeleiding = SelectedSoortBegeleiding;
                    ContactverslagBLL bll = new ContactverslagBLL();
                    if (_isAddMode)
                    {
                        List<labels> presentLabels;
                        if (_labelManager.GetAllActiveLabels() is List<labels> list && list.Count < GlobalData.Instance.LoggedOnUser.LabelManager.GetAllActiveLabels().Count)
                        {
                            presentLabels = new List<labels>();

                            foreach (labels label in list)
                            {
                                presentLabels.Add(label);
                                presentLabels.AddRange(LabelManager.GetChildLables(label.id));
                            }
                        }
                        else
                            presentLabels = GlobalData.Instance.LoggedOnUser.LabelManager.GetAllActiveLabels();

                        presentLabels = presentLabels.Where(x => x.active == true && x.contactverslagen == true && GlobalData.Instance.LoggedOnUser.LabelManager.GetAllActiveLabels().Exists(y => y.id == x.id)).ToList();


                        if (presentLabels != null && presentLabels.Count == 1)
                        {
                            Contactverslag.ContactverslagLabels = new ObservableCollection<labels>(presentLabels);
                        }
                        else if (presentLabels.Count > 1)
                        {
                            LabelsPopup labels = new LabelsPopup(
                                Contactverslag.ContactverslagId,
                                new LabelConnectionProviderContactverslag(GlobalData.Instance.LoggedOnUser, new LabelProviderDataBase().Load(),
                                    presentLabels)
                            );
                            if (labels.ShowDialog().GetValueOrDefault())
                                Contactverslag.ContactverslagLabels = new ObservableCollection<labels>(labels.ConnectionManager.SelectedLabels);

                        }
                        else
                            throw new Exception("Er zijn geen labels meegegeven aan dit scherm!");

                        bll.AddContactverslag(Contactverslag);
                    }
                    else
                        bll.EditContactverslag(Contactverslag);

                    this.Close();
                }
                else
                    throw new Exception("Niet alle verplichte velden zijn ingevuld. Gelieve deze in te vullen en opnieuw te proberen");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Foutmelding", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        /** METHODS */
        public void NotifiyPropertyChanged(string property)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(property));
        }
    }
}
