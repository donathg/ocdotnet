﻿using ClaraFey.ViewControllers.ClientDossier;
using ClaraFey.Windows.Common;
using DevExpress.Xpf.Core;
using sharedcode.BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ClaraFey.Windows.ClientDossier
{
    /// <summary>
    /// Interaction logic for BeeldvormingUC.xaml
    /// </summary>
    public partial class BeeldvormingUC : UserControl
    {
        private List<labels> _labelListContactverslagen = new List<labels>()
        {
            LabelManager.GetLabelById(84)
        };

        public BeeldvormingUC()
        {
            InitializeComponent();
 
          
            this.Loaded += BeeldvormingUC_Loaded;
        }

        private void BeeldvormingUC_Loaded(object sender, RoutedEventArgs e)
        {
            if (GetVC() is BeeldvormingVC vc)
                tabBeeldvorming.Content = new BeeldvormingOverzichtUC(vc);

            if (_labelListContactverslagen.FindAll(x => x.contactverslagen == true).Count == 0)
            {
                tabContactverslagen.Visibility = Visibility.Collapsed;
            }
        }
        private void TabMain_SelectionChanging(object sender, TabControlSelectionChangingEventArgs e)
        {

            if (((DXTabItem)e.NewSelectedItem).Name == "tabBeeldvorming")
            {
                if (GetVC() is BeeldvormingVC vc)
                    tabBeeldvorming.Content = new BeeldvormingOverzichtUC(vc);
            }
            else if (((DXTabItem)e.NewSelectedItem).Name == "tabContactverslagen")
            {
                tabContactverslagen.Content = new ContactverslagUC();

                LabelProviderEmpty labelProvider = new LabelProviderEmpty();
                labelProvider.InjectLabels(_labelListContactverslagen);
                ContactverslagVC contactverslagVC = new ContactverslagVC(new LabelManager(labelProvider));
                contactverslagVC.Load(GetVC().Bewoner.id);

                tabContactverslagen.DataContext = contactverslagVC;
            }
        }
        private void biShowHelp_ItemClick(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            HelpWindow w = new HelpWindow("cd_beeldvorming", "Help beeldvorming");
            w.Show();
        }

        private BeeldvormingVC GetVC()
        {
            return this.DataContext as BeeldvormingVC;
        }
    }
}
