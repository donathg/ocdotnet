﻿using ClaraFey.CommonObjects;
using ClaraFey.ViewControllers.ClientDossier;
using ClaraFey.ViewControllers.Common;
using ClaraFey.ViewControllers.UserControls;
using ClaraFey.Windows.Common;
using sharedcode.BLL;
using sharedcode.common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ClaraFey.Windows.ClientDossier
{
    /// <summary>
    /// Interaction logic for AIAlgemeneGegevensUC.xaml
    /// </summary>
    public partial class AIJruidischUC : UserControl
    {
        AdministratieveInformatieVC _vc;

        public AIJruidischUC(AdministratieveInformatieVC vc)
        {
            _vc = vc;
            DataContext = _vc;

            InitializeComponent();

            AutoSaveMultiCheckboxClientDossierGroup Group = new AutoSaveMultiCheckboxClientDossierGroup();
            Group.RegisterChildUCs(AutoSaveMultiCheckBoxBewindVoerderAangesteldOm.Vc);
            Group.RegisterChildUCs(AutoSaveMultiCheckBoxBewindVoerderBeschermingOver.Vc);
            Group.RegisterChildUCs(AutoSaveMultiCheckBoxBewindVoerderBeschermingTermijn.Vc);
            vc.RegisterChildUCs(Group);

            vc.RegisterChildUCs(AdressButtonBewindvoerder.Vc);
            vc.RegisterChildUCs(AdressButtonJeugdrechter.Vc);
            vc.RegisterChildUCs(AdressButtonAdvocaat.Vc);
            vc.RegisterChildUCs(AdressButtonVredegerecht.Vc);
            vc.RegisterChildUCs(AdressButtonRechtbank.Vc);
        }

        private void TextBox_Scroll(object sender, System.Windows.Controls.Primitives.ScrollEventArgs e)
        {

        }
    }
}
