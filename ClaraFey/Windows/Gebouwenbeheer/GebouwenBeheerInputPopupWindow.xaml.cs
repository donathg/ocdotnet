﻿
using ClaraFey;
using ClaraFey.BLL;
using sharedcode.BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ClaraFey.Windows.Gebouwenbeheer
{
    /// <summary>
    /// Interaction logic for GebouwenBeheerInputPopupWindow.xaml
    /// </summary>
    public partial class GebouwenBeheerInputPopupWindow : Window
    {
        locatie _location;
        public GebouwenBeheerInputPopupWindow(locatie location, String windowTitle)
        {
            this._location = location;
            this.DataContext = location;
            InitializeComponent();
            this.Title = windowTitle;
        }

        private void btnOK_Click(object sender, RoutedEventArgs e)
        {
            if (String.IsNullOrWhiteSpace(tbNaam.Text))
            {
                MessageBox.Show("Gelieve een naam in te vullen.", "Fout", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            this.DialogResult = true;
            this.Close();
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            this.Close();
        }
    }
}
