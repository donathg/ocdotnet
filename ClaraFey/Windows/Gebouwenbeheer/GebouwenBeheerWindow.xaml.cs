﻿using ClaraFey;
using ClaraFey.BLL;
using ClaraFey.CommonObjects;
using ClaraFey.ViewControllers;
using ClaraFey.ViewControllers.Common;
using ClaraFey.Views;
using ClaraFey.Windows.ClientDossier;
using ClaraFey.Windows.Common;
using sharedcode.BLL;
using sharedcode.common;
using sharedcode.CommonObjects;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ClaraFey.Windows.Gebouwenbeheer
{
    /// <summary>
    /// Interaction logic for GebouwenBeheerWindow.xaml
    /// </summary>
    public partial class GebouwenBeheerWindow : Window
    {
        /** PROPERTIES */
        private GebouwenBeheerVC _vc;

        /** CONSTRUCTOR */
        public GebouwenBeheerWindow()
        {
            _vc = new GebouwenBeheerVC();
            this.DataContext = _vc;

            InitializeComponent();

            if (GlobalData.Instance.LoggedOnUser.GetAccesType("100") == AccessType.access)
               this.KeyDown += GebouwenBeheerWindow_KeyDown;
        }

        /** EVENTS */
        private void GebouwenBeheerWindow_KeyDown(object sender, KeyEventArgs e)
        {
            switch(e.Key)
            {
                case Key.Add:
                    AddSubLocation();
                    break;
                case Key.Delete:
                    DeleteLocation();
                    break;
                case Key.Space:
                    UpdateLocation();
                    break;
                case Key.Enter:
                    UpdateLocation();
                    break;
            }  
        }
        private void RightClick_UpdateLocation(object sender, RoutedEventArgs e)
        {
            UpdateLocation();
        }
        private void RightClick_DeleteLocation(object sender, RoutedEventArgs e)
        {
            DeleteLocation();
        }
        private void RightClick_AddSubLocation(object sender, RoutedEventArgs e)
        {
            AddSubLocation();
        }
        private void RightClick_AddCampus(object sender, RoutedEventArgs e)
        {
            try
            {
                locatie newLocation = new locatie();

                Window popupWindow = new GebouwenBeheerInputPopupWindow(newLocation, "Campus toevoegen");
                if (popupWindow.ShowDialog().GetValueOrDefault())
                {
                    newLocation.active = true;
                    _vc.AddLocatie(newLocation);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Fout", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void RightClick_AjustCampusAdres(object sender, RoutedEventArgs e)
        {
            try
            {
                if (_vc.SelectedLocatie is locatie loc)
                {
                    AdresDetailsWindow w = new AdresDetailsWindow(loc);
                    w.ShowDialog();
                }
                else
                    throw new Exception("Gelieve een campus uit te kiezen.");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Fout", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        /** METHODS */
        private void DeleteLocation()
        {
            try
            {
                if (treeView.SelectedItem is locatie location)
                {
                    MessageBoxResult result = MessageBox.Show(String.Format("Wil u de locatie '{0}' verwijderen ?", location.naam), "Verplaatsen", MessageBoxButton.YesNoCancel, MessageBoxImage.Warning);

                    if (result == MessageBoxResult.Yes)
                        _vc.RemoveLocation(location);
                }
                else
                    throw new Exception("Gelieve een locatie uit te kiezen.");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Fout", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void AddSubLocation()
        {
            try
            {
                if (treeView.SelectedItem is locatie location)
                {
                    int oldLocId = location.id;
                    Window popupWindow = new GebouwenBeheerInputPopupWindow(location, "Sublocatie toevoegen");
                    if (popupWindow.ShowDialog().GetValueOrDefault())
                    {
                        location.id = 0;
                        location.parent_id = oldLocId;
                        _vc.AddLocatie(location);
                    }
                }
                else
                    throw new Exception("Gelieve een locatie uit te kiezen.");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Fout", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void UpdateLocation()
        {
            try
            {
                if (_vc.SelectedLocatie is locatie location)
                {
                    Window popupWindow = new GebouwenBeheerInputPopupWindow(location, "Locatie wijzigen");
                    if (popupWindow.ShowDialog().GetValueOrDefault())
                        _vc.ChangeLocatie(location);
                }
                else
                    throw new Exception("Gelieve een locatie uit te kiezen.");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Foutmelding", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
