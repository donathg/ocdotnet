﻿using ClaraFey.BLL;
using ClaraFey.CommonObjects;

//using ClaraFey.Reports;
using ClaraFey.UserControls;
using ClaraFey.ViewControllers;
using ClaraFey.ViewControllers.Common;
using ClaraFey.ViewControllers.Inventaris;
using ClaraFey.Windows.Common;
using DevExpress.LookAndFeel;
using DevExpress.Xpf.Grid;
using DevExpress.XtraReports.UI;
using sharedcode.BLL;
using sharedcode.common;
using sharedcode.CommonObjects;
using sharedcode.WeergaveClasses;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ClaraFey.Windows.Inventaris
{
    /// <summary>
    /// Interaction logic for InventarisWindow.xaml
    /// </summary>
    public partial class InventarisWindow : Window
    {
        private InventarisViewController _vc = new InventarisViewController();

        public InventarisWindow()
        {
            this.DataContext = _vc;
            InitializeComponent();
            gridInventaris.View.ShowSearchPanel(true);
            this.Closing += InventarisWindow_Closing;
            this.Loaded += InventarisWindow_Loaded;
            this.Closed += InventarisWindow_Closed;
        }

        void InventarisWindow_Closed(object sender, EventArgs e)
        {
            _vc.CleanUp();
            _vc = null;
        }
        void InventarisWindow_Loaded(object sender, RoutedEventArgs e)
        {

             try
            {
                WpfLayoutBLL.LoadLayoutDockManager(myDockManager, "inventarisDocking");
                WpfLayoutBLL.LoadLayoutGrid(gridInventaris, "inventarisGrid");
                gridInventaris.SelectedItemChanged += GridControl_SelectedItemChanged;
                if (gridInventaris.CurrentItem != null)
                {
                    gridInventaris.SelectedItem = gridInventaris.CurrentItem;
                    _vc.SelectedInventarisItem = (InventarisWeergave) gridInventaris.CurrentItem;
                    this._vc.SelectLocation(_vc.SelectedInventarisItem.locatie_id);
                }
                else
                {
                    gridInventaris.SelectedItem = null;
                    gridInventaris.CurrentItem = null;
                }
        
            }
            catch
            { } 
        }
        void InventarisWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            try
            {
                SaveInventaris();
                WpfLayoutBLL.SaveLayoutDockManager(myDockManager, "inventarisDocking");
                WpfLayoutBLL.SaveLayoutGrid(gridInventaris, "inventarisGrid");

            }
            catch (Exception ex)
            { MessageBox.Show(ex.Message); }
        }      
        private void GridControl_SelectedItemChanged(object sender, SelectedItemChangedEventArgs e)
        {
            SaveInventaris();
            if (_vc.SelectedInventarisItem != null)
            {
                this._vc.SelectLocation(_vc.SelectedInventarisItem.locatie_id);
            }
        }
        private void btnReport_Click(object sender, RoutedEventArgs e)
        {
            List<InventarisWeergave> shownInventarisItems = new List<InventarisWeergave>();
            for (int i = 0; i < gridInventaris.VisibleRowCount; i++)
            {
                InventarisWeergave dataRow = (InventarisWeergave)gridInventaris.GetRow(gridInventaris.GetRowHandleByVisibleIndex(i));
                if (dataRow.id != 0)
                    shownInventarisItems.Add(dataRow);
            }
            List<InventarisPrintItem> PrintItems = this._vc.BllInventaris.ConvertToPrintItems(shownInventarisItems);
        }
        private void LocatieBoom_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
        }  
        private void MyHandler(object sender, MouseButtonEventArgs args)
        {
            if (args.Source is TreeViewItem item)
            {
                // do whatever you want with item
            }
        }               
        private void MenuItemLocation_Click(object sender, RoutedEventArgs e)
        {

         

        }
        private void miSelectNewLocationClick(object sender, RoutedEventArgs e)
        {
            MenuItem contextMenuItem = (MenuItem)sender;
            ContextMenu contextMenu = (ContextMenu)contextMenuItem.Parent;
            if (contextMenu.PlacementTarget.GetType() == typeof(TreeViewItem))
            {
                TreeViewItem originatingTreeViewItem = (TreeViewItem)contextMenu.PlacementTarget;
                if (originatingTreeViewItem.DataContext is Location loc)
                {
                    this._vc.SetInventarisLocation(loc);
                    this._vc.SelectLocation(loc.Id);
                }
            }
        }
        private void btnShowHistory_Click(object sender, RoutedEventArgs e)
        {
            if (btnShowHistory.IsChecked.HasValue && btnShowHistory.IsChecked.Value)
            {
                btnShowHistory.Content = "historiek aan";
                
                recHistory.Visibility = System.Windows.Visibility.Visible;
                 _vc.ChangeHistoryType(HistoryType.history);
            }
            else
            {
            
                btnShowHistory.Content = "historiek uit";
                _vc.ChangeHistoryType(HistoryType.active);
                recHistory.Visibility = System.Windows.Visibility.Hidden;
            }
        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {

        }
        private void btnSmmMerken_Click(object sender, RoutedEventArgs e)
        {
            try {

                SMMWindow2Settings settings = new SMMWindow2Settings("Inventaris - Merken", GlobalMethods.Themes.normal);
                SMMWindow2Level level0 = new SMMWindow2Level(0, "Merken") { AllowDelete = false, AllowModify = true, AllowAdd = true, AllowSearch = true, AllowFilter = true };

                level0.AddColumns(new SMMWindow2Column() { FieldName = "naam", ColumnHeader = "naam", IsEditable = true });

                /* settings.AddLevel(level0);
                 SMMWindow2 w = new SMMWindow2(settings);
                 w.BindDataLevel<merk>(0);*/
                smmWindowVC vc = new smmWindowVC();
                var someDbSet = vc.Entity.Set<merk>().ToList();
                var ds = new ObservableCollection<merk>(someDbSet.ToList());
                level0.DataSource = ds;
                settings.AddLevel(level0);
                SMMWindow2 w = new SMMWindow2(vc, settings);
                w.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + ex.InnerException, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }            
        }
        private void btnSmmLeveranciers_Click(object sender, RoutedEventArgs e)
        {
            recAdorner.Visibility = Visibility.Visible;
            try
            {
                SMMWindow2Settings settings = new SMMWindow2Settings("Inventaris - Leveranciers", GlobalMethods.Themes.normal);
                SMMWindow2Level level0 = new SMMWindow2Level(0, "Leveranciers") { AllowDelete = false, AllowModify = true, AllowAdd = true, AllowSearch = true, AllowFilter = true };
                level0.AddColumns(new SMMWindow2Column() { FieldName = "lev_naam", ColumnHeader = "naam", IsEditable = true });
                /*  settings.AddLevel(level0);
                    SMMWindow2 w = new SMMWindow2(settings);
                    w.BindDataLevel<leverancier>(0);
                    w.ShowDialog();*/

                smmWindowVC vc = new smmWindowVC();
                var someDbSet = vc.Entity.Set<leverancier>().ToList();
                var ds = new ObservableCollection<leverancier>(someDbSet.ToList());
                level0.DataSource = ds;
                settings.AddLevel(level0);

                /*  var someDbSet1 = vc.Entity.Set<rechten_groepenlocaties>().ToList();
                    var ds1 = new ObservableCollection<rechten_groepenlocaties>(someDbSet1.ToList());
                    level1.DataSource = ds;
                    settings.AddLevel(level1);*/

                SMMWindow2 w = new SMMWindow2(vc, settings);
                w.ShowDialog();
            }
            finally
            {
                recAdorner.Visibility = Visibility.Hidden;
            }
        }
        private void btnSmmToestellen_Click(object sender, RoutedEventArgs e)
        {
            recAdorner.Visibility = Visibility.Visible;

            try
            {
                List<SMMWindow2ComboItem> cmbItems = new List<SMMWindow2ComboItem>();
                foreach (string s in InventarisBLL.GetAllToestelCategorieTypes())
                    cmbItems.Add(new SMMWindow2ComboItem() { Id = s, Name = s });

                SMMWindow2Settings settings = new SMMWindow2Settings("Inventaris - Toestellen", GlobalMethods.Themes.normal);
                SMMWindow2Level level0 = new SMMWindow2Level(0, "Toestellen") { AllowDelete = false, AllowModify = true, AllowAdd = true, AllowSearch = true, AllowFilter = true };

                level0.AddColumns(
                    new SMMWindow2Column() { FieldName = "naam", ColumnHeader = "Toestel", IsEditable = true },
                    new SMMWindow2Column() { FieldName = "category", ColumnHeader = "Categorie", ComboboxItems = cmbItems , IsEditable = true }
                );

                smmWindowVC vc = new smmWindowVC();
                var someDbSet = vc.Entity.Set<toestel>().ToList();
                var ds = new ObservableCollection<toestel>(someDbSet.ToList());
                level0.DataSource = ds;
                settings.AddLevel(level0);

                SMMWindow2 w = new SMMWindow2(vc, settings);
                w.ShowDialog();
            }
            finally
            {
                recAdorner.Visibility = Visibility.Hidden;
            } 

        }
        private void miBestanden_Click(object sender, RoutedEventArgs e)
        {
            OpenBijlagenScherm();
        }
        private void grid_CustomUnboundColumnData(object sender, GridColumnDataEventArgs e)
        {
            if (e.Column.FieldName == "IconUnboundAttachment")
            {
                if (e.IsGetData)
                {
                    object row = ((IList)((GridControl)sender).ItemsSource)[e.ListSourceRowIndex];
                    InventarisWeergave woItem = (InventarisWeergave)row;
                    if (woItem.Aantalbijlagen.GetValueOrDefault() > 0)
                    {
                        Uri uri = new Uri("/images/attachment.png", UriKind.Relative);
                        e.Value = new BitmapImage(new Uri("/images/attachment.png", UriKind.Relative));
                    }
                    else e.Value = null;
                }
            }
        }
        private void grid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            TableViewHitInfo hitInfo = ((TableView)gridInventaris.View).CalcHitInfo(e.OriginalSource as DependencyObject);

            if (hitInfo.InRow)
            {
                if (hitInfo.Column != null || hitInfo.Column.FieldName == "IconUnboundAttachment")
                    OpenBijlagenScherm();
            }
        }
        private void barButtonExportToExel_ItemClick(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {

        }
        private void ButtonExportToExel_ItemClick(object sender, RoutedEventArgs e)
        {

            string dates = DateTime.Now.ToString("dd-MM-yyyy");


            GlobalMethods.ExportGridToExcel(gridInventaris, "inventaris_" + dates);




        }
        private void MyLookUpEdit_KeyUp(object sender, KeyEventArgs e)
        {
            MyLookUpEdit editBox = (MyLookUpEdit)sender;
            if (editBox.DisplayText.Length == 0)
            {
                editBox.SelectedItem = null;
            }
        }

        private void OpenBijlagenScherm()
        {
            try
            {
                if (_vc.SelectedInventarisItem == null)
                    throw new Exception("Je moet eerst een bijlage selecteren");
                else if (!(_vc.SelectedInventarisItem.id != 0))
                    throw new Exception("Dit item is nog niet geïnventariseerd.");

                UploadFileWindowSettings settings = new UploadFileWindowSettings("Bijlagen voor item " + _vc.SelectedInventarisItem.nummer_263, "inventaris")
                {
                    AllowAddFile = true,
                    AllowEditing = GlobalData.Instance.LoggedOnUser.GetAccesType("201") == AccessType.access,
                    AllowDeleteFile = GlobalData.Instance.LoggedOnUser.GetAccesType("201") == AccessType.access
                };
                UploadFileVC bijlagenVc = new UploadFileVC(
                    new BijlagenBLLProviderInventaris(),
                    _vc.SelectedInventarisItem.id,
                    settings
                );
                UploadFileWindow fw = new UploadFileWindow(bijlagenVc, GlobalMethods.Themes.normal);

                fw.ShowDialog();

                _vc.SelectedInventarisItem.Aantalbijlagen = fw.NumberOfItems;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Foutmelding", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void SaveInventaris()
        {
            try
            {
                this._vc.Save();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "fout", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

    }
}
