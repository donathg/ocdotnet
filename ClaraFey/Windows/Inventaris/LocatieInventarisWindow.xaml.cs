﻿using ClaraFey.CommonObjects;
using ClaraFey.ViewControllers;
using ClaraFey.ViewControllers.Common;
using ClaraFey.ViewControllers.Inventaris;
using ClaraFey.Windows.Common;
using DevExpress.Xpf.Bars;
using DevExpress.Xpf.Grid;
using sharedcode.BLL;
using sharedcode.common;
using sharedcode.CommonObjects;
using sharedcode.WeergaveClasses;
using System;
using System.Collections;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media.Imaging;

namespace ClaraFey.Windows.Inventaris
{
    /// <summary>
    /// Interaction logic for LocatieInventarisWindow.xaml
    /// </summary>
    public partial class LocatieInventarisWindow : Window
    {
        /** PROPERTIES */
        private LocatieInventarisVC _vc;

        /** CONSTRUCTOR */
        public LocatieInventarisWindow()
        {
            _vc = new LocatieInventarisVC();
            this.DataContext = _vc;
            _vc.InitObjectenUsercontrol();

            InitializeComponent();
            foreach (objecttype type in InventarisBLL.GetAllObjecttypes())
            {
                BarButtonItem item = new BarButtonItem
                {
                    Name = $"barButtonExportToExcel{type.naam}",
                    Hint = $"{type.naam}-overzicht",
                    Alignment = BarItemAlignment.Near,
                    Tag = type.id,
                    ToolTip = $"{type.naam}-overzicht",
                    Glyph = new BitmapImage(new Uri(type.GlyphLocation)),
                    LargeGlyph = new BitmapImage(new Uri(type.GlyphLocation))
                };
                item.ItemClick += objectenExport_Click;

                buttonMenu.Items.Add(item);
            }
        }

        /** EVENTS */
        private void GridInventaris_CustomUnboundColumnData(object sender, GridColumnDataEventArgs e)
        {
            if (e.Column.FieldName == "IconUnboundAttachment" && e.IsGetData)
            {
                object row = ((IList)((GridControl)sender).ItemsSource)[e.ListSourceRowIndex];
                InventarisWeergave woItem = (InventarisWeergave)row;
                if (woItem.Aantalbijlagen.HasValue && woItem.Aantalbijlagen.Value > 0)
                {
                    Uri uri = new Uri("/images/attachment.png", UriKind.Relative);
                    e.Value = new BitmapImage(new Uri("/images/attachment.png", UriKind.Relative));
                }
                else e.Value = null;
            }
        }
        private void GridInventaris_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            TableViewHitInfo hitInfo = ((TableView)gridInventaris.View).CalcHitInfo(e.OriginalSource as DependencyObject);

            if (hitInfo.Column != null || hitInfo.Column.FieldName == "IconUnboundAttachment")
            {
                UploadFileWindowSettings settings = new UploadFileWindowSettings("Bijlagen voor item " + _vc.SelectedInventaris.productOmschrijving, "inventaris")
                {
                    AllowAddFile = true,
                    AllowEditing = GlobalData.Instance.LoggedOnUser.GetAccesType("201") == AccessType.access,
                    AllowDeleteFile = GlobalData.Instance.LoggedOnUser.GetAccesType("201") == AccessType.access
                };
                UploadFileVC bijlagenVc = new UploadFileVC(
                    new BijlagenBLLProviderInventaris(),
                    _vc.SelectedInventaris.id,
                    settings
                );
                bijlagenVc.BijlagenChanged += new UploadFileVC.BijlagenChangedHandler(ChangedBijlagen);
                UploadFileWindow fw = new UploadFileWindow(bijlagenVc, GlobalMethods.Themes.normal);

                fw.ShowDialog();
            }
        }
        private void DocumentGroup_SelectedItemChanged(object sender, DevExpress.Xpf.Docking.Base.SelectedItemChangedEventArgs e)
        {
            if (e.OldItem == ObjectenTab && _vc.SmmObjectenVC != null)
            {
                GridControl grid = ucObjecten.GetGrid(0);
                if (grid.View.IsEditing)
                {
                    grid.View.CommitEditing();
                }
            }
        }
        private void objectenExport_Click(object sender, RoutedEventArgs e)
        {
            if (int.TryParse(((BarButtonItem)sender).Tag.ToString(), out int typeId))
            {
                SMMWindow2Level level0 = new SMMWindow2Level(0, "Objectenlijst met locaties")
                {
                    AllowDelete = false,
                    AllowModify = false,
                    AllowAdd = false,
                    AllowSearch = true,
                    AllowFilter = true,
                    CanExportToExcel = true
                };
                level0.AddColumns(
                    new SMMWindow2Column() { FieldName = "Naam", ColumnHeader = "Naam", IsEditable = false },
                    new SMMWindow2Column() { FieldName = "Opmerking", ColumnHeader = "Opmerking", IsEditable = false },
                    new SMMWindow2Column() { FieldName = "Hoeveelheid", ColumnHeader = "Hoeveelheid", IsEditable = false },
                    new SMMWindow2Column() { FieldName = "Eigenaar", ColumnHeader = "Eigenaar", IsEditable = false },
                    new SMMWindow2Column() { FieldName = "FullLocatiePath", ColumnHeader = "Locatie", IsEditable = false }
                );
                level0.DataSource = new ObservableCollection<LocatieobjectWeergave>(InventarisBLL.GetAllObjectenMetLocatiesFromObjecttypeId(typeId));

                SMMWindow2Settings settings = new SMMWindow2Settings("Objectenlijst met locaties", GlobalMethods.Themes.normal)
                {
                    WindowSizePercentage = 75
                };
                settings.AddLevel(level0);
                settings.theme = GlobalMethods.Themes.clientdossier;

                smmWindowVC vc = new smmWindowVC();
                SMMWindow2 w = new SMMWindow2(vc, settings);
                w.ShowDialog();
            }
            else
            {
                MessageBox.Show("Er is een fout opgetreden met de meegegeven id van het objecttype", "Foutmelding", MessageBoxButton.OK);
            }
        }
        private void biShowHelp_ItemClick(object sender, ItemClickEventArgs e)
        {

        }

        /** METHODS */
        private void ChangedBijlagen(int aantalBijlagen)
        {
            _vc.SelectedTreeItem.AantalBijlagen = _vc.BijlagenVC.NumberOfItems;
        }
    }
}
