﻿using ClaraFey.CommonObjects;
using ClaraFey.ViewControllers.Discussion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ClaraFey.Windows.Discussions
{
    /// <summary>
    /// Interaction logic for SaveDiscussion.xaml
    /// </summary>
    public partial class SaveDiscussion : Window
    {
        SaveDiscussionViewController _vc = new SaveDiscussionViewController();
        public SaveDiscussion()
        {
            this.DataContext = _vc;
            InitializeComponent();
           
        }
 
        private void MedewerkerLookUpEdit_KeyDown(object sender, KeyEventArgs e)
        {
           
        }

        private void btnAddMedewerker_Click(object sender, RoutedEventArgs e)
        {

        }

        private void listBoxSelectedMedewerker_KeyDown(object sender, KeyEventArgs e)
        {

        }

        private void miDeleteMedewerker_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnDeleteMedewerker_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
