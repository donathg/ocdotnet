﻿using ClaraFey.ViewControllers;
using ClaraFey.ViewControllers.Discussion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ClaraFey.Windows.Discussions
{
    /// <summary>
    /// Interaction logic for DiscussionWindow.xaml
    /// </summary>
    public partial class DiscussionWindow : Window
    {

        DiscussionViewController _vc = new DiscussionViewController();
        public DiscussionWindow()
        {
            InitializeComponent();
            this.DataContext = _vc;
        }

        private void biNew_ItemClick(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            SaveDiscussion w = new SaveDiscussion();
            w.ShowDialog();
        }
    }
}
