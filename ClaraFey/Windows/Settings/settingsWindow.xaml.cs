﻿using ClaraFey.CommonObjects;
using sharedcode.BLL;
using sharedcode.common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ClaraFey.Windows.Settings
{
    /// <summary>
    /// Interaction logic for settingsWindow.xaml
    /// </summary>
    public partial class settingsWindow : Window
    {
        private int _gebruikerId;
        public int browserIndex { get; set; }
        Entities entities;
        sharedcode.BLL.gebruiker_settings settings;

        private const string chromePath = @"chrome.exe";
        private const string firefoxPath = @"firefox.exe";
        private const string iePath = @"iexplore.exe";
        private const String edgePath = "microsoft-edge";

        public settingsWindow(int gebruikerId)
        {
            _gebruikerId = gebruikerId;

            entities = new Entities(GlobalData.Instance.Entity);

            settings = gebruiker_settingsBLL.GetGebruikerSettings(_gebruikerId);
         
            if (settings != null)
            {
                if (String.IsNullOrWhiteSpace(settings.browserpath))
                    browserIndex = 0;
                else if (settings.browserpath == chromePath)
                    browserIndex = 1;
                else if (settings.browserpath == firefoxPath)
                    browserIndex = 2;
                else if (settings.browserpath == iePath)
                    browserIndex = 3;
                else if (settings.browserpath == edgePath)
                    browserIndex = 5;
                else
                {
                    browserIndex = 4;
                }
            }
            DataContext = this;
            InitializeComponent();

            this.Loaded += SettingsWindow_Loaded;
        }

        private void SettingsWindow_Loaded(object sender, RoutedEventArgs e)
        {
            if (browserIndex==4)
                customBrowserUrl.Text = settings.browserpath;


        }

        private void btnOK_Click(object sender, RoutedEventArgs e)
        {


            if (browserIndex == 0)
                settings.browserpath = null;

            if (browserIndex == 1)
                settings.browserpath = chromePath;

            if (browserIndex == 2)
                settings.browserpath = firefoxPath;

            if (browserIndex == 3)
                settings.browserpath = iePath;
            if (browserIndex == 4)
                settings.browserpath = customBrowserUrl.Text;

            if (browserIndex == 5)
                settings.browserpath = edgePath;


            try
            {
                gebruiker_settingsBLL.UpdateBrowser(_gebruikerId, settings.browserpath);
                this.DialogResult = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "fout", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ResetButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (GlobalData.Instance.LoggedOnUser.Id.HasValue)
                {
                    int number = GlobalMethods.NumWindowOpen<Window>();
                    if (number != 2)
                    {
                        MessageBox.Show("Gelieve alle schermen behalve het Inlogscherm te sluiten", "Waarschuwing", MessageBoxButton.OK,
                            MessageBoxImage.Warning);
                    }
                    else
                    {
                        gebruiker_settingsBLL.DeleteAllInputsFromUser(GlobalData.Instance.LoggedOnUser.Id.Value);
                        MessageBox.Show("Alle weergaves zijn teruggezet naar hun oorspronkelijke status", "Confirmatie", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Foutmelding", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
