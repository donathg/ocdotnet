﻿

using DevExpress.Xpf.Docking;
using DevExpress.Xpf.Grid;
using DevExpress.Xpf.RichEdit;
using sharedcode.common;
using sharedcode.CommonObjects;
using sharedcode.DAL;
using System;

using System.IO;


namespace ClaraFey.BLL
{
    public class WpfLayoutBLL
    {
        public static void SaveLayout(Stream s, String controlCode)
        {
            WpfLayoutDAL.SaveLayout(s, controlCode, GlobalData.Instance.LoggedOnUser.Id.GetValueOrDefault(), System.Environment.MachineName);
        }
        public static MemoryStream LoadLayout(String controlCode)
        {

            return WpfLayoutDAL.LoadLayout(controlCode, GlobalData.Instance.LoggedOnUser.Id.GetValueOrDefault(), System.Environment.MachineName);
        }
        public static void SaveLayoutDockManager(DockLayoutManager dockmanager, string controlCode)
        {
            if (GlobalData.Instance.LoggedOnUser.GetAccesType("900") != AccessType.none)
            {
                System.IO.MemoryStream str = new System.IO.MemoryStream();
                if (str != null)
                {
                    dockmanager.SaveLayoutToStream(str);
                    str.Seek(0, System.IO.SeekOrigin.Begin);
                    SaveLayout(str, controlCode);
                }
            }
        }
        public static void LoadLayoutDockManager(DockLayoutManager dockmanager, string controlCode)
        {
            if (GlobalData.Instance.LoggedOnUser.GetAccesType("900") != AccessType.none)
            {
                System.IO.MemoryStream str = WpfLayoutBLL.LoadLayout(controlCode);
                if (str != null)
                {
                    str.Seek(0, SeekOrigin.Begin);
                    dockmanager.RestoreLayoutFromStream(str);
                }
            }
        }
        public static void SaveLayoutGrid(GridControl grid, string controlCode)
        {
            if (GlobalData.Instance.LoggedOnUser.GetAccesType("900") != AccessType.none)
            {
                MemoryStream str = new MemoryStream();
                if (str != null)
                {
                    grid.SaveLayoutToStream(str);
                    str.Seek(0, SeekOrigin.Begin);
                    SaveLayout(str, controlCode);
                }
            }

        }
        public static void LoadLayoutGrid(GridControl grid, string controlCode)
        {

            if (GlobalData.Instance.LoggedOnUser.GetAccesType("900") != AccessType.none)
            {
                System.IO.MemoryStream str = WpfLayoutBLL.LoadLayout(controlCode);
                if (str != null)
                {
                    str.Seek(0, System.IO.SeekOrigin.Begin);
                    grid.RestoreLayoutFromStream(str);
                }
            }
        }

        public static void LoadHelp(RichEditControl richEdit, string screenCode)
        {
            MemoryStream str = LoadHelp(screenCode);
            if (str != null)
            {
                str.Seek(0, SeekOrigin.Begin);
                
                richEdit.LoadDocument(str, DevExpress.XtraRichEdit.DocumentFormat.Rtf);
            }
        }
        public static void SaveHelp(RichEditControl richEdit, string controlCode)
        {


          
                System.IO.MemoryStream str = new System.IO.MemoryStream();
                if (str != null)
                {
                richEdit.SaveDocument(str, DevExpress.XtraRichEdit.DocumentFormat.Rtf);
                str.Seek(0, System.IO.SeekOrigin.Begin);
                    WpfLayoutBLL.SaveHelp(str, controlCode);
                }
           

        }
        private static void SaveHelp(Stream s, String controlCode)
        {

            WpfLayoutDAL.SaveHelp(s, controlCode, GlobalData.Instance.LoggedOnUser.Id.GetValueOrDefault());
        }
        private static MemoryStream LoadHelp(String screenCode)
        {
            return WpfLayoutDAL.LoadHelp(screenCode);
        }

    }


}
