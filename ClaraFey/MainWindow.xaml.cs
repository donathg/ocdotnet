﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using ClaraFey.Views;
using ClaraFey.Windows;
using ClaraFey.Windows.Gebouwenbeheer;
using ClaraFey.Windows.Inventaris;
using System.IO;
using ClaraFey.Windows.Kilometervergoeding;
using ClaraFey.Windows.Common;
using ClaraFey.Windows.Kalender;
using ClaraFey.Windows.Registratie;
using sharedcode.BLL;
using sharedcode.common;
using ClaraFey.CommonObjects;
using ClaraFey.Windows.Settings;
using ClaraFey.ViewControllers;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using ClaraFey.Notifications;
using sharedcode.CommonObjects;
using DevExpress.Xpf.Core;
using ClaraFey.Windows.ClientDossier;
using DevExpress.Xpf.Editors.Settings;
using ClaraFey.Windows.CardReader;
using System.Data;
using DevExpress.Xpf.Editors;
using DevExpress.Xpf.Bars;
using sharedcode.WeergaveClasses;
using ClaraFey.Windows.Rapportage;
using ClaraFey.CommonObjects.WordParsers;
using ClaraFey.Windows.Aankoop.IM;

namespace ClaraFey
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
#if DEBUG
        String _kalenderHost = "http://localhost:8080";
       
#else
        String _kalenderHost = "http://10.118.0.43:81";
#endif

        /** FIELDS */
        private readonly LogonView _logonView = new LogonView();
        static bool UserDoNotWantsToSeeMaintenanceMessageAgain = false;

        /** CONSTRUCTOR */
        public MainWindow()
        {
            SelectDatabase();

            // The following line provides localization for data formats. 
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("nl-NL");
            // The following line provides localization for the application's user interface. 
            System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("nl-NL");
            
            //setting default theme for application
            ApplicationThemeHelper.ApplicationThemeName = Theme.DeepBlueName;
            
            this.DataContext = _logonView;
            InitializeComponent();

            this.Title = String.Format("{0} (versie {1})", this.Title, GlobalData.Instance.ApplicationVersion);
            AppDomain.CurrentDomain.ProcessExit += CurrentDomain_ProcessExit;
        }
        /** EVENTS */
        #region MainWindow
        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            if (Properties.Settings.Default.lastRememberMe)
            {
                tbPersoneelsNummer.Text = Properties.Settings.Default.lastPersoneelsNr;
                checkBoxRemember.IsChecked = Properties.Settings.Default.lastRememberMe;
                //Properties.Settings.Default.DevExpressTheme;
            }

            tbPersoneelsNummer.Focus();

            GlobalData.Instance.GlobalShutDownTimer.StartTimer(out bool immidiateShutDown);
            if (immidiateShutDown)
            {
                ShutDownApplication(3);
                MessageBox.Show("De Clara Fey App krijgt momenteel een update. Probeer het later nog eens.");
                return;

            }
            GlobalData.Instance.GlobalShutDownTimer.globalShutDownTimerUpdateAvailable += GlobalShutDownTimer_globalShutDownTimerUpdateAvailable;
            GlobalData.Instance.GlobalShutDownTimer.Check();




            // DevExpress.Data.ShellHelper.TryCreateShortcut("sample_notification_app", "DXSampleNotificationSevice");

#if DEBUG

            /*            tbPersoneelsNummer.Text = "20376";
                        tbPaswoord.Text = "xxlgert";
                        DoLogon();

                        WerkbonnenbeheerWindow win = new WerkbonnenbeheerWindow(ClaraFey.BLL.WerkopdrachtType.Werkaanvraag);
                        win.Show();*/
#endif
        }
        private void MainWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (CheckOpenWindows())
            {
                e.Cancel = true;
                return;
            }

            SaveProperties();
            BijlagenBLLManager.RemoveTempFolder();
            DoLogOff(true);
           
        }
        private void CurrentDomain_ProcessExit(object sender, EventArgs e)
        {
            DoLogOff(true);          
        }
        #endregion
        #region Buttons
        private void Button_Click_Logon(object sender, RoutedEventArgs e)
        {
            btnInloggen.IsEnabled = false;
            bool loggedOn = false;
            try
            {
                this.Cursor = Cursors.Wait;
                loggedOn = UserBLL.DoLogon(tbPersoneelsNummer.Text.Trim(), tbPaswoord.Text, out bool newPasswordNeeded);
                this.Cursor = Cursors.Arrow;

                if (newPasswordNeeded)
                {
                    String naam = GlobalData.Instance.LoggedOnUser.Name;
                    RegistratieWindow regWindow = new RegistratieWindow(naam);

                    if (regWindow.ShowDialog().GetValueOrDefault())
                    {
                        String persNr = GlobalData.Instance.LoggedOnUser.PersNr;

                        UserBLL.ChangePassword(persNr, regWindow.newPaswoord);
                        MessageBox.Show(naam + ", u kan inloggen met uw nieuw paswoord");
                        if (GlobalData.Instance.LoggedOnUser.IsLoggedOn)
                            DoLogOff(false);
                        tbPaswoord.Text = String.Empty;
                        tbPersoneelsNummer.Text = persNr;
                    }
                }
                else if (loggedOn)
                {
                    lblWelcomeText.Content = "Welkom " + GlobalData.Instance.LoggedOnUser.Name;

                    GlobalMethods.DeleteLog();

                    //we gaan hier reeds in een andere thread de Locatieboom inladen
                    System.Threading.ThreadPool.QueueUserWorkItem(
                        delegate { GebouwenBeheerLoader.Load(); },
                        null
                    );
                    SetDataBaseInfo();

                    RefreshWerkbonToolMenuItems();

                }
                else
                    throw new Exception("personeelsnr/wachtwoord combinatie onbekend.");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Foutmelding", MessageBoxButton.OK, MessageBoxImage.Error, MessageBoxResult.OK);
            }
            finally
            {
                if (this.Cursor != Cursors.Arrow)
                    this.Cursor = Cursors.Arrow;

                btnInloggen.IsEnabled = true;
            }
        }
        private void Button_Click_Logoff(object sender, RoutedEventArgs e)
        {
            if (CheckOpenWindows() == false)
            {
                if (checkBoxRemember.IsChecked.GetValueOrDefault() == false)
                {
                    tbPaswoord.Clear();
                    tbPersoneelsNummer.Clear();
                }
                DoLogOff(false);
            }
        }
        /// <summary>
        /// Registreren en paswoord vergeten
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ResetPassword_Click(object sender, RoutedEventArgs e)
        {
            ChangePassword();
        }
        private void ToolBarButton_Phone_Click(object sender, RoutedEventArgs e)
        {
            MnuTelefoonBoek_Click(null, null);
            //  GlobalMethods.StartBrowserWithBlazorApp("telefoonboek");
        }
        #endregion

        #region Clientdossier
        private void BiClientDossierClick(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            if (GlobalData.Instance.LoggedOnUser.LabelManager.GetAllActiveLabels().Count != 0)
            {
                SplashScreen s = new SplashScreen("images/splashcd.jpg");
                s.Show(true);
                WindowMain w = new WindowMain();
                w.Show();
            }
            else
            {
                MessageBox.Show("Je hebt geen labels. Er zijn dus geen elementen om weer te geven in het clientdossier", "Geen labels", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        #endregion
        #region Personeelsdossier
        private void BiPersoneelClick(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            Windows.Personeel.WindowMain w = new Windows.Personeel.WindowMain();
            w.Show();
        }
        #endregion
        #region Inventaris
        private void BiInventaris_ItemClick(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            bool isOpen = GlobalMethods.IsWindowOpen<Window>("WindowInventaris");
            if (isOpen)
                GlobalMethods.ActivateWindown<Window>("WindowInventaris");
            else
            {
                this.Cursor = Cursors.Wait;
                InventarisWindow win = new InventarisWindow()
                {
                    Name = "WindowInventaris"
                };
                win.Show();
                this.Cursor = Cursors.Arrow;
            }
        }
        private void BiLocatieInventaris_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (GlobalMethods.IsWindowOpen<Window>("LocatieInventarisWindow"))
                GlobalMethods.ActivateWindown<Window>("LocatieInventarisWindow");
            else
            {
                this.Cursor = Cursors.Wait;
                LocatieInventarisWindow w = new LocatieInventarisWindow()
                {
                    Name = "LocatieInventarisWindow"
                };
                w.Show();
                this.Cursor = Cursors.Arrow;
            }
        }
        #endregion
        #region Werbonnenbeheer
        private void BiSuperVisorWB_ItemClick1(object sender, ItemClickEventArgs e)
        {
            if (sender is BarButtonItem item)
            {
                this.Cursor = Cursors.Wait;
                werkopdracht_diensten _werkopdracht_diensten = GlobalData.Instance.LoggedOnUser.GetDienstByCode(item.Tag.ToString());
                GlobalMethods.OpenWerkbonWindow(_werkopdracht_diensten, WerkopdrachtUserType.supervisor, "Werkbonnen voor dienst " + _werkopdracht_diensten.wodienst_naam);
                this.Cursor = Cursors.Arrow;
            }
        }
        private void BiSuperVisorWA_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (sender is BarButtonItem item)
            {

                this.Cursor = Cursors.Wait;
                werkopdracht_diensten _werkopdracht_diensten = GlobalData.Instance.LoggedOnUser.GetDienstByCode(item.Tag.ToString());
                GlobalMethods.OpenWerkaanvraagWindow(_werkopdracht_diensten, WerkopdrachtUserType.supervisor, "Werkaanvragen voor dienst " + _werkopdracht_diensten.wodienst_naam);
                this.Cursor = Cursors.Arrow;
            }
        }
        private void BiMijnWerkbonnen_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (sender is BarButtonItem item)
            {

                this.Cursor = Cursors.Wait;
                werkopdracht_diensten _werkopdracht_diensten = GlobalData.Instance.LoggedOnUser.GetDienstByCode(item.Tag.ToString());
                GlobalMethods.OpenWerkbonWindow(_werkopdracht_diensten, WerkopdrachtUserType.excecuter, _werkopdracht_diensten.wodienst_naam);
                this.Cursor = Cursors.Arrow;
            }
        }
        private void BiWerkaanvragen_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (sender is BarButtonItem item)
            {

                this.Cursor = Cursors.Wait;
                if (item.Tag is werkopdracht_diensten dienst)
                {
                    GlobalMethods.OpenWerkaanvraagWindow(dienst, WerkopdrachtUserType.normal, "Werkaanvragen voor dienst " + dienst.wodienst_naam);
                }
                this.Cursor = Cursors.Arrow;
            }
        }

        #endregion
        #region Kalender
        private void MnuKalCKKZalen_Click(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            String url = String.Format("{0}/externallogon/?personeelsnummer={1}&campus=CKK&kaltype=zaal", _kalenderHost, GlobalData.Instance.LoggedOnUser.PersNr);

            GlobalMethods.StartBrowser(url);
        }
        private void mnuKalCKKVergaderzaalXL_Click(object sender, ItemClickEventArgs e)
        {
            String url = String.Format("{0}/externallogon/?personeelsnummer={1}&campus=CKK&kaltype=zaalxl", _kalenderHost, sharedcode.common.GlobalData.Instance.LoggedOnUser.PersNr);

            GlobalMethods.StartBrowser(url);

        }
        private void MnuKalCKKVoertuigen_Click(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            String url = String.Format("{0}/externallogon/?personeelsnummer={1}&campus=CKK&kaltype=voertuig", _kalenderHost, GlobalData.Instance.LoggedOnUser.PersNr);
            GlobalMethods.StartBrowser(url);
        }
        private void mnuKalCKKVoertuigenTechd_Click(object sender, ItemClickEventArgs e)
        {
            String url = String.Format("{0}/externallogon/?personeelsnummer={1}&campus=CKK&kaltype=voertuig_td", _kalenderHost, GlobalData.Instance.LoggedOnUser.PersNr);

            GlobalMethods.StartBrowser(url);
        }
        private void MnuKalCKKVoertuigenIndividuelebegeleiding_Click(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            String url = String.Format("{0}/externallogon/?personeelsnummer={1}&campus=CKK&kaltype=voertuig_individuele_begeleiding", _kalenderHost, GlobalData.Instance.LoggedOnUser.PersNr);

            GlobalMethods.StartBrowser(url);
        }
        private void MnuKalTroonVoertuigen_Click(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            String url = String.Format("{0}/externallogon/?personeelsnummer={1}&campus=TROON&kaltype=voertuig", _kalenderHost, sharedcode.common.GlobalData.Instance.LoggedOnUser.PersNr);

            GlobalMethods.StartBrowser(url);
        }
        private void mnuKalDCVoertuigen_ItemClick(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            String url = String.Format("{0}/externallogon/?personeelsnummer={1}&campus=DAGCENTRUM&kaltype=voertuig", _kalenderHost, sharedcode.common.GlobalData.Instance.LoggedOnUser.PersNr);

            GlobalMethods.StartBrowser(url);
        }
        private void MnuKalCKKActiviteiten_Click(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            String url = String.Format("{0}/externallogon/?personeelsnummer={1}&campus=CKK&kaltype=activiteit", _kalenderHost, sharedcode.common.GlobalData.Instance.LoggedOnUser.PersNr);

            GlobalMethods.StartBrowser(url);
        }
        private void MnuKalCKKLaptops_Click(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            String url = String.Format("{0}/externallogon/?personeelsnummer={1}&campus=CKK&kaltype=laptop", _kalenderHost, sharedcode.common.GlobalData.Instance.LoggedOnUser.PersNr);

            GlobalMethods.StartBrowser(url);
        }
        private void MnuKalCSRLaptops_Click(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            String url = String.Format("{0}/externallogon/?personeelsnummer={1}&campus=CSR&kaltype=laptop", _kalenderHost, sharedcode.common.GlobalData.Instance.LoggedOnUser.PersNr);

            GlobalMethods.StartBrowser(url);
        }
        private void MnuKalCSRActiviteiten_Click(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            String url = String.Format("{0}/externallogon/?personeelsnummer={1}&campus=CSR&kaltype=zaal", _kalenderHost, sharedcode.common.GlobalData.Instance.LoggedOnUser.PersNr);

            GlobalMethods.StartBrowser(url);
        }
        private void MnuKalCSRTOAH_Click(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            String url = String.Format("{0}/externallogon/?personeelsnummer={1}&campus=CSR&kaltype=TOAH", _kalenderHost, sharedcode.common.GlobalData.Instance.LoggedOnUser.PersNr);

            GlobalMethods.StartBrowser(url);
        }
        private void MnuKalTroonZaal_Click(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            String url = String.Format("{0}/externallogon/?personeelsnummer={1}&campus=TROON&kaltype=zaal", _kalenderHost, sharedcode.common.GlobalData.Instance.LoggedOnUser.PersNr);
            GlobalMethods.StartBrowser(url);
        }
        private void MnuKalKauriVoertuigen_ItemClick(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            String url = String.Format("{0}/externallogon/?personeelsnummer={1}&campus=KAURI&kaltype=voertuig", _kalenderHost, sharedcode.common.GlobalData.Instance.LoggedOnUser.PersNr);

            GlobalMethods.StartBrowser(url);
        }
        private void MnuKalEyndovensteenwegVoertuigen_ItemClick(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            String url = String.Format("{0}/externallogon/?personeelsnummer={1}&campus=EYNDOVENSTEENWEG&kaltype=voertuig", _kalenderHost, sharedcode.common.GlobalData.Instance.LoggedOnUser.PersNr);

            GlobalMethods.StartBrowser(url);
        }
        private void mnuKalCKKZalenAdministraties_Click(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            String url = String.Format("{0}/externallogon/?personeelsnummer={1}&campus=CKK&kaltype=zaaladmin", _kalenderHost, sharedcode.common.GlobalData.Instance.LoggedOnUser.PersNr);

            GlobalMethods.StartBrowser(url);
        }
        private void mnuKalSocDienst_Click(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            String url = String.Format("{0}/externallogon/?personeelsnummer={1}&campus=CKK&kaltype=socdienst", _kalenderHost, sharedcode.common.GlobalData.Instance.LoggedOnUser.PersNr);

            GlobalMethods.StartBrowser(url);
        }
        private void mnuKalInstroom_Click(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            String url = String.Format("{0}/externallogon/?personeelsnummer={1}&campus=CKK&kaltype=instroom", _kalenderHost, sharedcode.common.GlobalData.Instance.LoggedOnUser.PersNr);

            GlobalMethods.StartBrowser(url);

        }
        /* private void mnuKalender_Click(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            KalenderWindow win = new KalenderWindow();
            win.Show();
        }*/
        #endregion
        #region Kilometervergoeding
        private void MnuKilometer_Click(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            OpenKilometerWindow(KilometerModeType.ingave, "WindowKilometersIngave", "Kilometervergoeding - eigen kilometers");
        }
        private void MnuKilometerBoekhouding_Click(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            OpenKilometerWindow(KilometerModeType.boekhouding, "WindowKilometersBoekhouding", "Kilometervergoeding boekhouding");
        }
        private void MnuKilometerPersoneelsdienst_Click(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            OpenKilometerWindow(KilometerModeType.personeelsdienst, "WindowKilometersPersoneelsdienst", "Kilometervergoeding personeelsdienst");
        }
        private void MnuKilometerBewonersAdmin_Click(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            OpenKilometerWindow(KilometerModeType.bewoners, "WindowKilometersBewoners", "Kilometervergoeding bewonersadministratie");
        }
        private void MnuKilometerOverzicht_Click(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            OpenKilometerWindow(KilometerModeType.overzicht, "WindowKilometersOverzicht", "Kilometervergoeding - Overzicht");
        }
        #endregion
        #region Kasbeheer
        private void BiKasbeheerExport_Click(object sender, ItemClickEventArgs e)
        {
            if (GlobalMethods.IsWindowOpen<Window>("KasbeheerExportWindow"))
                GlobalMethods.ActivateWindown<Window>("KasbeheerExportWindow");
            else
            {
                this.Cursor = Cursors.Wait;
                KasbeheerExportWindow w = new KasbeheerExportWindow();
                w.Show();
                this.Cursor = Cursors.Arrow;
            }
        }
        private void mnuKasbeheer_Click(object sender, ItemClickEventArgs e)
        {
            GlobalMethods.StartBrowserWithBlazorApp("kasbeheer");
        }
        #endregion
        #region Telefoonboek
        private void MnuTelefoonBoek_Click(object sender, ItemClickEventArgs e)
        {
            try
            {
                SMMWindow2Settings settings = new SMMWindow2Settings("Telefoonboek", GlobalMethods.Themes.normal)
                {
                    ShowHelpButton = true,
                    HelpText = "Volledige lijst"
                };              
                TextEditSettings internNummerSetting = new TextEditSettings()
                {
                    HorizontalContentAlignment = EditSettingsHorizontalAlignment.Right,
                    MaxLength = 3
                };
                ImageEditSettings ImageSettings = new ImageEditSettings() { MaxWidth = 100 };
                SMMWindow2Level level0 = new SMMWindow2Level(0, "Telefoonboek")
                {
                    CanExportToExcel = this._logonView.CanModifyTelefoonboek,
                    MouseDoubleClickMethod = new SMMWindow2Level.MouseDoubleClick(this.TelefoonboekDoubleClick),
                    AllowDelete = false,
                    AllowModify = false,
                    AllowAdd = false,
                    AllowSearch = true, 
                    AllowFilter = true,
                    AllowGroup = true 
                };
                level0.AddColumns(
                    new SMMWindow2Column() { IsPicture = true, EditSettings= ImageSettings, FieldName = "foto", ColumnHeader = "foto van de medewerker", IsEditable = false, IsNullable = true },
                    new SMMWindow2Column() { FieldName = "personeelscode", ColumnHeader = "Personeelsnr.", IsEditable = false },
                    new SMMWindow2Column() { FieldName = "voornaam", ColumnHeader = "voornaam", IsEditable = true },
                    new SMMWindow2Column() { FieldName = "achternaam", ColumnHeader = "achternaam", IsEditable = true },
                    new SMMWindow2Column() { FieldName = "email", ColumnHeader = "email", IsEditable = true, IsNullable = true },
                    new SMMWindow2Column() { FieldName = "telefoon", ColumnHeader = "telefoon", IsEditable = true, IsNullable = true },
                    new SMMWindow2Column() { FieldName = "gsm", ColumnHeader = "gsm", IsEditable = true, IsNullable = true },
                    new SMMWindow2Column() { FieldName = "telefoon_intern", EditSettings = internNummerSetting, ColumnHeader = "intern nummer", IsEditable = true, IsNullable = true },
                    new SMMWindow2Column() { EditSettings = internNummerSetting, FieldName = "telefoon_intern_groep", ColumnHeader = "intern groepsnummer", IsEditable = true, IsNullable = true },
                    new SMMWindow2Column() { FieldName = "functiebeschrijving", ColumnHeader = "functie", IsEditable = true, IsNullable = true },
                    new SMMWindow2Column() { FieldName = "werkplaats", ColumnHeader = "werkplaats", IsEditable = true, IsNullable = true }
                );

          /*      if (this.logonView.CanModifyTelefoonboek)
                    level0.ContextMenuItems.Add(new SMMWindow2ContextMenuItem() { ContextMenuItemClickedMethod = new SMMWindow2ContextMenuItem.ContextMenuItemClicked(this.TelefoonboekContextMenuItemClick), Title = "Paswoord resetten", UniqueId = 1 });
                    */
                smmWindowVC vc = new smmWindowVC();

               DataTable dt =  GebruikerBLL.GetTelefoonBoekItems();
                level0.DataSource = dt;
                settings.AddLevel(level0);
              
               SMMWindow2 w = new SMMWindow2(vc, settings);
                w.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + ex.InnerException, "Fout", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        #endregion
        #region Rapportage
        private void Mnutijdsregistratie_CLick(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            SMMWindow2Settings settings = new SMMWindow2Settings("Tijdregistratie", GlobalMethods.Themes.normal);

            SMMWindow2Level level0 = new SMMWindow2Level(0, "Tijdregistratie") { AllowDelete = false, AllowModify = false, AllowAdd = false, AllowSearch = true, AllowFilter = true, AllowGroup = true };
            level0.CanExportToExcel = true;
            level0.AddColumns(new SMMWindow2Column() { FieldName = "Afdeling", ColumnHeader = "afdeling" }
            , new SMMWindow2Column() { FieldName = "Datum", ColumnHeader = "datum" }
            , new SMMWindow2Column() { FieldName = "vbegeleider", ColumnHeader = "begeleider" }
            , new SMMWindow2Column() { FieldName = "Bewoner", ColumnHeader = "bewoner" }
            , new SMMWindow2Column() { FieldName = "begeleiding", ColumnHeader = "begeleiding" }
            , new SMMWindow2Column() { FieldName = "Type", ColumnHeader = "type" }
            , new SMMWindow2Column() { FieldName = "aanrekenen", ColumnHeader = "aanrekenen" }
            , new SMMWindow2Column() { FieldName = "van", ColumnHeader = "van" }
            , new SMMWindow2Column() { FieldName = "tot", ColumnHeader = "tot" }
            , new SMMWindow2Column() { FieldName = "tijd", ColumnHeader = "tijd", IsSummary = true }
            , new SMMWindow2Column() { FieldName = "memo", ColumnHeader = "memo" });

            smmWindowVC vc = new smmWindowVC();

            level0.DataSource = ReportsBLL.GetTijdsRegistratieRapport();
            settings.AddLevel(level0);

            SMMWindow2 w = new SMMWindow2(vc, settings);
            w.ShowDialog();
        }
        private void MnuiBewonerOverzichtClick(object sender, ItemClickEventArgs e)
        {
            SMMWindow2Settings settings = new SMMWindow2Settings("Cliëntenoverzicht", GlobalMethods.Themes.normal);
            SMMWindow2Level level0 = new SMMWindow2Level(0, "Cliëntenoverzicht") { AllowGroup = true, AllowDelete = false, AllowModify = false, AllowAdd = false, AllowSearch = true, AllowFilter = true, CanExportToExcel = true };

            level0.AddColumns(
                new SMMWindow2Column() { FieldName = "voornaam", ColumnHeader = "Voornaam", IsEditable = false },
                new SMMWindow2Column() { FieldName = "achternaam", ColumnHeader = "Achternaam", IsEditable = false },
                new SMMWindow2Column() { FieldName = "leefGroepName", ColumnHeader = "Leefgroep", IsEditable = false },
                new SMMWindow2Column() { FieldName = "opnameeinde", ColumnHeader = "Einde opname", IsEditable = false }

            );

            smmWindowVC vc = new smmWindowVC();

            var someDbSet = vc.Entity.Set<vw_bewonersLeefgroep>().SqlQuery(@"select * from vw_bewonersLeefgroep 
 where leefGroepName != 'Disney'
 order by leefGroepName, voornaam, achternaam 
 ").ToList();
            level0.DataSource = new ObservableCollection<vw_bewonersLeefgroep>(someDbSet.ToList());
            settings.AddLevel(level0);

            SMMWindow2 w = new SMMWindow2(vc, settings);
            w.ShowDialog();
        }
        private void MnuiBewonerOverzichtWeb_CLick(object sender, ItemClickEventArgs e)
        {
            GlobalMethods.StartBrowserWithBlazorApp("clientenoverzicht");
        }
        private void MnuClientIDOAddendumClick(object sender, ItemClickEventArgs e)
        {
            IdoAddendumWordParser wordParser = new IdoAddendumWordParser
            {
                WordDocumentLocation = AppDomain.CurrentDomain.BaseDirectory + "\\Templates\\idotemplate.docx"
            };

            RichEditIDOWindow w = new RichEditIDOWindow(wordParser, GlobalMethods.Themes.normal);
            w.ShowDialog();
        }
        private void MnuClientIDOClick(object sender, ItemClickEventArgs e)
        {
            IdoMFCWordParser wordParser = new IdoMFCWordParser
            {
                WordDocumentLocation = AppDomain.CurrentDomain.BaseDirectory + "\\Templates\\idoMFCtemplate.docx"
            };

            RichEditIDOWindow w = new RichEditIDOWindow(wordParser, GlobalMethods.Themes.normal);
            w.ShowDialog();
        }
        public void MnuiApotheekImport_CLick(object sender, ItemClickEventArgs e)
        {
            ApotheekImport w = new ApotheekImport();
            w.Show();
        }
        #endregion
        #region SMM
        private void mnuItemCardReaderTest_Click(object sender, ItemClickEventArgs e)
        {
            CardWindow w = new CardWindow(GlobalMethods.Themes.clientdossier);
            w.ShowDialog();
        }
        private void mnuVoertuigenSMM_Click(object sender, ItemClickEventArgs e)
        {
            List<SMMWindow2ComboItem> list = new List<SMMWindow2ComboItem>
            {
                new SMMWindow2ComboItem() { Id = "CAR", Name = "wagen" },
                new SMMWindow2ComboItem() { Id = "BIKE", Name = "fiets" },
                new SMMWindow2ComboItem() { Id = "BUS", Name = "bus" }
            };

            List<SMMWindow2ComboItem> cmbItemLocaties = new List<SMMWindow2ComboItem>();

            GebouwenBeheerBLL bll = new GebouwenBeheerBLL(new GebouwenBeheerSettings());

            foreach (KeyValuePair<int, Location> kvp in bll.flatList)
                cmbItemLocaties.Add(new SMMWindow2ComboItem() { Id = kvp.Value.Id, Name = kvp.Value.GetFullPath() });

            SMMWindow2Settings settings = new SMMWindow2Settings("Voertuigen", GlobalMethods.Themes.normal);
            SMMWindow2Level level0 = new SMMWindow2Level(0, "Voertuigen") { AllowGroup = true, AllowDelete = false, AllowModify = true, AllowAdd = true, AllowSearch = true, AllowFilter = true, CanExportToExcel = true };
            level0.AddColumns(
                new SMMWindow2Column() { FieldName = "Nummerplaat", ColumnHeader = "Nummerplaat", IsEditable = true, IsNullable = true },
                new SMMWindow2Column() { FieldName = "Merk", ColumnHeader = "Merk", IsEditable = true, IsNullable = true },
                new SMMWindow2Column() { FieldName = "Kleur", ColumnHeader = "Kleur", IsEditable = true, IsNullable = true },
                new SMMWindow2Column() { FieldName = "Bouwjaar", ColumnHeader = "Bouwjaar", IsEditable = true, IsNullable = true },
                new SMMWindow2Column() { FieldName = "LocatieId", ColumnHeader = "Locatie", ComboboxItems = cmbItemLocaties, IsEditable = true, IsNullable = true },
                new SMMWindow2Column() { FieldName = "priceType", ColumnHeader = "Tarieftype", ComboboxItems = list, DefaultValue = "CAR", IsEditable = false },
                new SMMWindow2Column() { FieldName = "actief", ColumnHeader = "Actief", IsEditable = true }
            );
            smmWindowVC vc = new smmWindowVC();
            var someDbSet = vc.Entity.Set<kilometer_wagen>().ToList();
            level0.DataSource = new ObservableCollection<kilometer_wagen>(someDbSet.ToList());
            settings.AddLevel(level0);
            SMMWindow2 w = new SMMWindow2(vc, settings);
            w.ShowDialog();
        }
        private void mnuKasbeheerSMM_Click(object sender, ItemClickEventArgs e)
        {
            List<SMMWindow2ComboItem> cmbItemsGebruikers = new List<SMMWindow2ComboItem>();

            using (Entities entity = new sharedcode.BLL.Entities(sharedcode.common.GlobalData.Instance.Entity))
            {
                var gebruikers = entity.gebruiker.OrderBy(c => c.voornaam).ToList();
                foreach (gebruiker g in gebruikers)
                    cmbItemsGebruikers.Add(new SMMWindow2ComboItem() { Id = g.id, Name = g.voornaam + " " + g.achternaam + " (" + g.personeelscode + ")" });
            }
            SMMWindow2Settings settings = new SMMWindow2Settings("Kasbeheer", GlobalMethods.Themes.normal) { ShowHelpButton = true };
            SMMWindow2Level level0 = new SMMWindow2Level(0, "Leefgroepen") { AllowDelete = false, AllowModify = false, AllowAdd = false, AllowSearch = true, AllowFilter = true };
            level0.JoinField = "id";
            level0.AddColumns(

                new SMMWindow2Column() { FieldName = "id", ColumnHeader = "id", IsEditable = false, Visible = false },
                new SMMWindow2Column() { FieldName = "naam", ColumnHeader = "Leeefgroep", IsEditable = false });




            SMMWindow2Level level1 = new SMMWindow2Level(1, "Gebruikers") { AllowDelete = true, AllowModify = true, AllowAdd = true, AllowSearch = false, AllowFilter = false };
            level1.JoinField = "leefGroepId";
            level1.AddColumns(new SMMWindow2Column() { FieldName = "leefGroepId", ColumnHeader = "Leefgroep", ComboboxItems = cmbItemsGebruikers, IsEditable = false, Visible = false });
            level1.AddColumns(new SMMWindow2Column() { FieldName = "gebruikerId", ColumnHeader = "Medewerker", ComboboxItems = cmbItemsGebruikers, IsEditable = true });
            smmWindowVC vc = new smmWindowVC();
            var someDbSet = vc.Entity.Set<leefgroep>().Where(x => x.actief == "Y" && x.fromorbis == "Y").OrderBy(x => x.naam).ToList();
            var ds = new ObservableCollection<leefgroep>(someDbSet.ToList());
            level0.DataSource = ds;
            settings.AddLevel(level0);
            var someDbSet1 = vc.Entity.Set<RechtenKasbeheer>().ToList();
            var ds1 = new ObservableCollection<RechtenKasbeheer>(someDbSet1.ToList());
            level1.DataSource = ds1;
            settings.AddLevel(level1);

            SMMWindow2 w = new SMMWindow2(vc, settings);
            w.ShowDialog();

        }
        private void mnuSmmOverzichtFuncties_Click(object sender, ItemClickEventArgs e)
        {
            SMMWindow2Settings settings = new SMMWindow2Settings("Functies", GlobalMethods.Themes.normal);
            SMMWindow2Level level0 = new SMMWindow2Level(0, "Functies") { AllowGroup = true, AllowDelete = false, AllowModify = true, AllowAdd = false, AllowSearch = true, AllowFilter = true, CanExportToExcel = true };

            level0.AddColumns(
                new SMMWindow2Column() { FieldName = "func_code", ColumnHeader = "Code", IsEditable = false },
                new SMMWindow2Column() { FieldName = "func_naam", ColumnHeader = "naam", IsEditable = false },
                new SMMWindow2Column() { FieldName = "func_opm", ColumnHeader = "opmerking", IsEditable = true },
                new SMMWindow2Column() { FieldName = "func_cat", ColumnHeader = "module", IsEditable = false }

            );

            smmWindowVC vc = new smmWindowVC();

            var someDbSet = vc.Entity.Set<rechten_functie>().SqlQuery(@"select * from  rechten_functie 
where cast (func_code as int) < 1000 or cast (func_code as int) > 2000 
order by cast (func_code as int)").ToList();
            level0.DataSource = new ObservableCollection<rechten_functie>(someDbSet.ToList());
            settings.AddLevel(level0);

            SMMWindow2 w = new SMMWindow2(vc, settings);
            w.ShowDialog();
        }
        private void mnuWerkbonnenbeheer_Prioriteiten_Click(object sender, ItemClickEventArgs e)
        {
            SMMWindow2Settings settings = new SMMWindow2Settings("Prioriteiten", GlobalMethods.Themes.normal);
            SMMWindow2Level level0 = new SMMWindow2Level(1, "Prioriteiten") { AllowDelete = false, AllowModify = true, AllowAdd = false, AllowSearch = false, AllowFilter = false };
            level0.AddColumns(
                    new SMMWindow2Column() { FieldName = "wop_id", ColumnHeader = "Id", IsEditable = false, Visible = false },
                    new SMMWindow2Column() { FieldName = "wop_naam", ColumnHeader = "Prioriteit", IsEditable = true },
                    new SMMWindow2Column() { FieldName = "wop_emails", ColumnHeader = "Emailadressen", IsEditable = true, IsNullable = true }
                );
            smmWindowVC vc = new smmWindowVC();
            var someDbSet = vc.Entity.Set<werkopdracht_prioriteit>().ToList();
            var ds = new ObservableCollection<werkopdracht_prioriteit>(someDbSet.ToList());
            level0.DataSource = ds;
            settings.AddLevel(level0);
            SMMWindow2 w = new SMMWindow2(vc, settings);
            w.ShowDialog();
        }
        private void mnuSmmApplicationSettings_Click(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            SMMWindow2Settings settings = new SMMWindow2Settings("Applicatie instellingen", GlobalMethods.Themes.normal);
            SMMWindow2Level level0 = new SMMWindow2Level(1, "Applicatie instellingen") { AllowDelete = false, AllowModify = true, AllowAdd = false, AllowSearch = false, AllowFilter = false };
            level0.AddColumns(
                    new SMMWindow2Column() { FieldName = "id", ColumnHeader = "Id", IsEditable = false, Visible = false }/*,
                   // new SMMWindow2Column() { FieldName = "telefoonboekfotopad", ColumnHeader = "Locatie medewerkerfoto's", IsEditable = true }*/
                );
            smmWindowVC vc = new smmWindowVC();
            var someDbSet = vc.Entity.Set<application_settings>().ToList();
            var ds = new ObservableCollection<application_settings>(someDbSet.ToList());
            level0.DataSource = ds;
            settings.AddLevel(level0);
            SMMWindow2 w = new SMMWindow2(vc, settings);
            w.ShowDialog();
        }
        private void mnuSmmRechtenGroepen_Click(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            List<SMMWindow2ComboItem> list = new List<SMMWindow2ComboItem>
            {
                new SMMWindow2ComboItem() { Id = "Y", Name = "Y" },
                new SMMWindow2ComboItem() { Id = "N", Name = "N" }
            };

            SMMWindow2Settings settings = new SMMWindow2Settings("Groepen", GlobalMethods.Themes.normal) { ShowHelpButton = true };

            SMMWindow2Level level0 = new SMMWindow2Level(0, "Groepen") { AllowDelete = false, AllowModify = true, AllowAdd = true, AllowSearch = true, AllowFilter = true , CanExportToExcel=true};
            level0.AddColumns(
                new SMMWindow2Column() { IsEditableInNewRow = true, ValidateMethod = new SMMWindow2Column.ValidateDelegate(this.ValidateGroepCode), FieldName = "groep_code", ColumnHeader = "Code", IsEditable = false },
                new SMMWindow2Column() { FieldName = "groep_naam", ColumnHeader = "Naam", IsEditable = true },
                new SMMWindow2Column() { FieldName = "groep_orbis", ColumnHeader = "Groep uit Orbis", ComboboxItems = list, DefaultValue = "N", IsEditable = false },
                new SMMWindow2Column() { FieldName = "groep_email_werkopdrachten", ColumnHeader = "email werkopdrachten", IsEditable = true, IsNullable = true },
                new SMMWindow2Column() { FieldName = "actif", ColumnHeader = "Actief", IsEditable = true, IsNullable = false, DefaultValue = true, IsCheckBox = true }
            );

            smmWindowVC vc = new smmWindowVC();

            var someDbSet = vc.Entity.Set<rechten_groep>().Where(x=>x.actif).ToList();
            var ds = new ObservableCollection<rechten_groep>(someDbSet.ToList());
            level0.DataSource = ds;
            settings.AddLevel(level0);
            SMMWindow2 w = new SMMWindow2(vc, settings);
            w.ShowDialog();
        }
        private void MnuSmmRechtenGroepenFuncties_Click(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            smmWindowVC vc = new smmWindowVC();

            List<SMMWindow2ComboItem> cmbItemsRechten = new List<SMMWindow2ComboItem>(new SMMWindow2ComboItem[] { new SMMWindow2ComboItem() { Id = "schrijven", Name = "schrijven" } });
            List<SMMWindow2ComboItem> cmbItemsFuncCodes = new List<SMMWindow2ComboItem>();
            foreach (rechten_functie f in vc.Entity.rechten_functie.Where(x => x.module_code != "CD"))
            {
                cmbItemsFuncCodes.Add(new SMMWindow2ComboItem() { Id = f.func_code, Name = f.func_code + " " + f.func_naam });
            }

            SMMWindow2Settings settings = new SMMWindow2Settings("Groepen-rechten", GlobalMethods.Themes.normal) { ShowHelpButton = true };

            SMMWindow2Level level0 = new SMMWindow2Level(0, "Groepen") { AllowDelete = false, AllowModify = false, AllowAdd = false, AllowSearch = true, AllowFilter = true };
            level0.AddColumns(
                new SMMWindow2Column() { FieldName = "groep_code", ColumnHeader = "Code", IsEditable = true },
                new SMMWindow2Column() { FieldName = "groep_naam", ColumnHeader = "Naam", IsEditable = true },
                new SMMWindow2Column() { FieldName = "groep_orbis", ColumnHeader = "Groep uit Orbis", DefaultValue = "N", IsEditable = false }
            );
            level0.JoinField = "groep_code";

            SMMWindow2Level level1 = new SMMWindow2Level(1, "Rechten") { AllowDelete = true, AllowModify = true, AllowAdd = true, AllowSearch = false, AllowFilter = false };
            level1.AddColumns(
                new SMMWindow2Column() { FieldName = "groepfunc_groep_code", ColumnHeader = "Groepcode", IsEditable = false },
                new SMMWindow2Column() { FieldName = "groepfunc_func_code", ColumnHeader = "Functiecode", ComboboxItems = cmbItemsFuncCodes, IsEditable = true },
                new SMMWindow2Column() { FieldName = "groepfunc_recht", ColumnHeader = "recht", DefaultValue = "schrijven", ComboboxItems = cmbItemsRechten, IsEditable = true, Visible = false }
            );
            level1.JoinField = "groepfunc_groep_code";

            var someDbSet = vc.Entity.rechten_groep.Where(x => x.actif);
            level0.DataSource = new ObservableCollection<rechten_groep>(someDbSet.ToList());
            settings.AddLevel(level0);

            var someDbSet1 = vc.Entity.rechten_groepenfuncties.Include("rechten_functie").Where(x => x.rechten_functie.module_code != "CD").OrderBy(x => x.groepfunc_func_code);
            level1.DataSource = new ObservableCollection<rechten_groepenfuncties>(someDbSet1.ToList());
            settings.AddLevel(level1);

            SMMWindow2 w = new SMMWindow2(vc, settings);
            w.ShowDialog();
        }
        private void mnuSmmRechtenGroepenGebruikers_Click(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            List<SMMWindow2ComboItem> cmbItemsGebruikers = new List<SMMWindow2ComboItem>();

            using (Entities entity = new sharedcode.BLL.Entities(sharedcode.common.GlobalData.Instance.Entity))
            {
                var gebruikers = entity.gebruiker.OrderBy(c => c.voornaam).ToList();
                foreach (gebruiker g in gebruikers)
                    cmbItemsGebruikers.Add(new SMMWindow2ComboItem() { Id = g.id, Name = g.voornaam + " " + g.achternaam + " (" + g.personeelscode + ")" });
            }
            SMMWindow2Settings settings = new SMMWindow2Settings("Groepen-rechten", GlobalMethods.Themes.normal) { ShowHelpButton = true };
            SMMWindow2Level level0 = new SMMWindow2Level(0, "Groepen") { AllowDelete = false, AllowModify = false, AllowAdd = false, AllowSearch = true, AllowFilter = true };
            level0.JoinField = "groep_code";
            level0.AddColumns(new SMMWindow2Column() { FieldName = "groep_code", ColumnHeader = "Code", IsEditable = true }, new SMMWindow2Column() { FieldName = "groep_naam", ColumnHeader = "Naam", IsEditable = true }, new SMMWindow2Column() { FieldName = "groep_orbis", ColumnHeader = "Groep uit Orbis", DefaultValue = "N", IsEditable = false });


            SMMWindow2Level level1 = new SMMWindow2Level(1, "gebruikers") { AllowDelete = true, AllowModify = true, AllowAdd = true, AllowSearch = false, AllowFilter = false };


            level1.JoinField = "groepgeb_groep_code";
            level1.AddColumns(new SMMWindow2Column() { FieldName = "groepgeb_groep_code", ColumnHeader = "Groepcode", IsEditable = false }, new SMMWindow2Column() { FieldName = "groepgeb_geb_id", ColumnHeader = "Medewerker", ComboboxItems = cmbItemsGebruikers, IsEditable = true });

            smmWindowVC vc = new smmWindowVC();
            var someDbSet = vc.Entity.Set<rechten_groep>().Where(x => x.actif).ToList();
            var ds = new ObservableCollection<rechten_groep>(someDbSet.ToList());
            level0.DataSource = ds;
            settings.AddLevel(level0);

            var someDbSet1 = vc.Entity.Set<rechten_groepengebruikers>().ToList();
            var ds1 = new ObservableCollection<rechten_groepengebruikers>(someDbSet1.ToList());
            level1.DataSource = ds1;
            settings.AddLevel(level1);

            SMMWindow2 w = new SMMWindow2(vc, settings);
            w.ShowDialog();
        }
        private void mnuSMMRechtenMedewerkers_Click(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            SMMWindow2Settings settings = new SMMWindow2Settings("Rechten - Gebruikers", GlobalMethods.Themes.normal);

            SMMWindow2Level level0 = new SMMWindow2Level(0, "Gebruikers") { AllowDelete = false, AllowModify = false, AllowAdd = false, AllowSearch = true, AllowFilter = true };
            level0.AddColumns(
                new SMMWindow2Column() { FieldName = "gebruiker.personeelscode", ColumnHeader = "Personeelscode", IsEditable = false },
                new SMMWindow2Column() { FieldName = "gebruiker.voornaam", ColumnHeader = "Voornaam", IsEditable = false },
                new SMMWindow2Column() { FieldName = "gebruiker.achternaam", ColumnHeader = "Achternaam", IsEditable = false },
                new SMMWindow2Column() { FieldName = "gebruiker.email", ColumnHeader = "Email", IsEditable = false },
                new SMMWindow2Column() { FieldName = "groepgeb_groep_code", ColumnHeader = "Groepcode", IsEditable = false }
            );

            smmWindowVC vc = new smmWindowVC();
            level0.DataSource = new ObservableCollection<rechten_groepengebruikers>(vc.Entity.rechten_groepengebruikers.Include("gebruiker").OrderBy(y => y.gebruiker.personeelscode).ToList());
            settings.AddLevel(level0);

            SMMWindow2 w = new SMMWindow2(vc, settings);
            w.ShowDialog();
        }
        private void mnuGebouwenBeheer_Click(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            bool isOpen = GlobalMethods.IsWindowOpen<Window>("WindowGebouwenbeheer");
            if (isOpen)
                GlobalMethods.ActivateWindown<Window>("WindowGebouwenbeheer");
            else
            {
                this.Cursor = Cursors.Wait;
                GebouwenBeheerWindow win = new GebouwenBeheerWindow()
                {
                    Name = "WindowGebouwenbeheer"
                };

                win.Show();
                this.Cursor = Cursors.Arrow;
            }
        }
        private void MnuInventarisObjecttypen_Click(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            SMMWindow2Level level0 = new SMMWindow2Level(0, "Objectttypen")
            {
                AllowDelete = true,
                AllowModify = true,
                AllowAdd = true,
                AllowSearch = true,
                AllowFilter = true
            };
            level0.AddColumns(
                new SMMWindow2Column() { FieldName = "naam", ColumnHeader = "Naam", IsEditable = true },
                new SMMWindow2Column() { FieldName = "beschrijving", ColumnHeader = "Beschrijving", IsEditable = true, IsNullable = true }
            );

            smmWindowVC vc = new smmWindowVC();
            level0.DataSource = new ObservableCollection<objecttype>(vc.Entity.objecttype.OrderBy(x => x.naam).ToList());

            SMMWindow2Settings settings = new SMMWindow2Settings("Inventaris - Objecttypen", GlobalMethods.Themes.normal);
            settings.AddLevel(level0);

            SMMWindow2 w = new SMMWindow2(vc, settings);
            w.ShowDialog();
        }
        private void mnuWerkbonnenBeheerLocaties_Click(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            List<SMMWindow2ComboItem> cmbItemLocaties = new List<SMMWindow2ComboItem>();
            GebouwenBeheerBLL bll = new GebouwenBeheerBLL(new GebouwenBeheerSettings());

            foreach (KeyValuePair<int, Location> kvp in bll.flatList)
                cmbItemLocaties.Add(new SMMWindow2ComboItem() { Id = kvp.Value.Id, Name = kvp.Value.GetFullPath() });
            
            List<SMMWindow2ComboItem> cmbDienst = new List<SMMWindow2ComboItem>();
            List<DienstItem> diensten = WerkbonnenBeheerBLL.GetDienstItems();
            
            foreach (DienstItem d in diensten)
                cmbDienst.Add(new SMMWindow2ComboItem() { Id = d.Code, Name = d.Naam });

            SMMWindow2Settings settings = new SMMWindow2Settings("Werkbonnenbeheer : groepen-locaties", GlobalMethods.Themes.normal);
            SMMWindow2Level level0 = new SMMWindow2Level(0, "Groepen") { AllowDelete = false, AllowModify = false, AllowAdd = false, AllowSearch = true, AllowFilter = true };
            level0.JoinField = "groep_code";
            level0.AddColumns(new SMMWindow2Column() { FieldName = "groep_code", ColumnHeader = "Code", IsEditable = true }, new SMMWindow2Column() { FieldName = "groep_naam", ColumnHeader = "Naam", IsEditable = true }, new SMMWindow2Column() { FieldName = "groep_orbis", ColumnHeader = "Groep uit Orbis", DefaultValue = "N", IsEditable = false });
            SMMWindow2Level level1 = new SMMWindow2Level(1, "Rechten") { AllowDelete = true, AllowModify = true, AllowAdd = true, AllowSearch = false, AllowFilter = false };
            level1.JoinField = "groeploc_groep_code";
            level1.AddColumns(new SMMWindow2Column() { FieldName = "groeploc_groep_code", ColumnHeader = "Groepcode", IsEditable = false }, new SMMWindow2Column() { FieldName = "groeploc_loc_id", ColumnHeader = "Locatie", ComboboxItems = cmbItemLocaties, IsEditable = true }
                , new SMMWindow2Column() { FieldName = "groeploc_dienst", ColumnHeader = "Dienst", ComboboxItems = cmbDienst, IsEditable = true, IsNullable = false });

            smmWindowVC vc = new smmWindowVC();
            var someDbSet = vc.Entity.Set<rechten_groep>().SqlQuery("select * from rechten_groep where actif=1").ToList();
            var ds = new ObservableCollection<rechten_groep>(someDbSet.ToList());
            level0.DataSource = ds;
            settings.AddLevel(level0);

            var someDbSet1 = vc.Entity.Set<rechten_groepenlocaties>().ToList();
            var ds1 = new ObservableCollection<rechten_groepenlocaties>(someDbSet1.ToList());
            level1.DataSource = ds1;
            settings.AddLevel(level1);

            SMMWindow2 w = new SMMWindow2(vc, settings);
            w.ShowDialog();
        }
        private void mnuFunctieRechtenGebruikers_Click(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            SMMWindow2Settings settings = new SMMWindow2Settings("Gebruiker-Functie", GlobalMethods.Themes.normal);
            SMMWindow2Level level0 = new SMMWindow2Level(0, "Gebruiker-Functie") { AllowDelete = false, AllowModify = false, AllowAdd = false, AllowSearch = true, AllowFilter = true, CanExportToExcel=true };
            
            level0.AddColumns(new SMMWindow2Column() { FieldName = "personeelscode", ColumnHeader = "personeelscode", IsEditable = false },
                new SMMWindow2Column() { FieldName = "voornaam", ColumnHeader = "voornaam", IsEditable = false },
                new SMMWindow2Column() { FieldName = "achternaam", ColumnHeader = "achternaam",   IsEditable = false },
                new SMMWindow2Column() { FieldName = "groep_code", ColumnHeader = "groepcode", IsEditable = false },
                new SMMWindow2Column() { FieldName = "groep_naam", ColumnHeader = "groepnaam", IsEditable = false },
                new SMMWindow2Column() { FieldName = "functiebeschrijving", ColumnHeader = "functiebeschrijving", IsEditable = false },
                new SMMWindow2Column() { FieldName = "werkplaats", ColumnHeader = "werkplaats", IsEditable = false },
                new SMMWindow2Column() { FieldName = "func_code", ColumnHeader = "functiecode", IsEditable = false },
                new SMMWindow2Column() { FieldName = "func_naam", ColumnHeader = "functienaam", IsEditable = false },
                new SMMWindow2Column() { FieldName = "func_cat", ColumnHeader = "functiemodule", IsEditable = false }
            );
          
            smmWindowVC vc = new smmWindowVC();

            var someDbSet = vc.Entity.Set<vw_gebruikerrechtenall>().SqlQuery(@"select * from vw_gebruikergroepall ,rechten_functie, rechten_groepenfuncties
 where rechten_functie.func_code = rechten_groepenfuncties.groepfunc_func_code
 and rechten_groepenfuncties.groepfunc_groep_code = vw_gebruikergroepall.groep_code
 order by voornaam, groep_code, id").ToList();
            level0.DataSource = new ObservableCollection<vw_gebruikerrechtenall>(someDbSet.ToList());
            settings.AddLevel(level0);

            SMMWindow2 w = new SMMWindow2(vc, settings);
            w.ShowDialog();
        }
        private void mnuSMMLeefgroepen_Click(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            bool isReadonly = sharedcode.common.GlobalData.Instance.LoggedOnUser.GetAccesType("110") != AccessType.access;
            List<SMMWindow2ComboItem> list = new List<SMMWindow2ComboItem>
            {
                new SMMWindow2ComboItem() { Id = "Y", Name = "Y" },
                new SMMWindow2ComboItem() { Id = "N", Name = "N" }
            };
            SMMWindow2Settings settings = new SMMWindow2Settings("Leefgroepen", GlobalMethods.Themes.normal);
            SMMWindow2Level level0 = new SMMWindow2Level(0, "Leefgroepen") { AllowDelete = false, AllowModify = !isReadonly, AllowAdd = !isReadonly, AllowSearch = true, AllowFilter = true };
            level0.AddColumns(new SMMWindow2Column() { IsEditableInNewRow = true, FieldName = "code", ColumnHeader = "Code", IsEditable = false }, new SMMWindow2Column() { FieldName = "naam", ColumnHeader = "Naam", IsEditable = true }, new SMMWindow2Column() { FieldName = "actief", ColumnHeader = "Actief", ComboboxItems = list, DefaultValue = "Y", IsEditable = true }

            , new SMMWindow2Column() { FieldName = "fromorbis", ColumnHeader = "Uit orbis?", ComboboxItems = list, DefaultValue = "N", IsEditable = false }
            , new SMMWindow2Column() { FieldName = "forboekh", ColumnHeader = "Voor boekhouding (km)", ComboboxItems = list, DefaultValue = "Y", IsEditable = true }
            );
            smmWindowVC vc = new smmWindowVC();
            var someDbSet = vc.Entity.Set<leefgroep>().ToList();
            var ds = new ObservableCollection<leefgroep>(someDbSet.ToList());
            level0.DataSource = ds;
            settings.AddLevel(level0);
            SMMWindow2 w = new SMMWindow2(vc, settings);
            w.ShowDialog();
            LeefgroepBLL.ClearCache();
        }
        private void mnuSMMKampen_Click(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            SMMWindow2Settings settings = new SMMWindow2Settings("Kampen", GlobalMethods.Themes.normal);
            List<SMMWindow2ComboItem> cmbItemsGebruikers = new List<SMMWindow2ComboItem>();

            using (Entities entity = new sharedcode.BLL.Entities(sharedcode.common.GlobalData.Instance.Entity))
            {
                var gebruikers = entity.gebruiker.OrderBy(c => c.voornaam).ToList();
                foreach (gebruiker g in gebruikers)
                    cmbItemsGebruikers.Add(new SMMWindow2ComboItem() { Id = g.id, Name = g.voornaam + " " + g.achternaam + " (" + g.personeelscode + ")" });
            }
            SMMWindow2Level level0 = new SMMWindow2Level(0, "Kampen") { CanExportToExcel = true, AllowDelete = false, AllowModify = true, AllowAdd = true, AllowSearch = true, AllowFilter = true };
            level0.AddColumns(new SMMWindow2Column() { FieldName = "id", ColumnHeader = "id.", IsEditable = false, Visible = false }, new SMMWindow2Column() { FieldName = "nr", ColumnHeader = "nr", IsEditable = true }, new SMMWindow2Column() { FieldName = "bestemming", ColumnHeader = "bestemming", IsEditable = true }, new SMMWindow2Column() { FieldName = "wie", ColumnHeader = "door wie ?", IsEditable = true }, new SMMWindow2Column() { FieldName = "datum_van", ColumnHeader = "van", IsEditable = true }
                , new SMMWindow2Column() { FieldName = "datum_tot", ColumnHeader = "tot", IsEditable = true }, new SMMWindow2Column() { FieldName = "aantaldeelnemers", ColumnHeader = "aantal deelnemers", IsEditable = true, IsNullable = true }, new SMMWindow2Column() { FieldName = "begeleiders", ColumnHeader = "begeleiders", IsEditable = true, IsNullable = true }, new SMMWindow2Column() { FieldName = "voorschot", ColumnHeader = "voorschot", IsEditable = true, IsNullable = true }
                , new SMMWindow2Column() { FieldName = "saldo", ColumnHeader = "saldo", IsEditable = true, IsNullable = true }
                , new SMMWindow2Column() { FieldName = "voorschot", ColumnHeader = "voorschot", IsEditable = true, IsNullable = true }
            , new SMMWindow2Column() { FieldName = "dossierbeheerderid", ColumnHeader = "dossierbeheerder", ComboboxItems = cmbItemsGebruikers, IsEditable = true, IsNullable = true }
                            , new SMMWindow2Column() { FieldName = "aktief", ColumnHeader = "aktief", IsEditable = true }
                                , new SMMWindow2Column() { FieldName = "opmerking", ColumnHeader = "opmerking", IsEditable = true, IsNullable = true }

                );
            smmWindowVC vc = new smmWindowVC();
            var someDbSet = vc.Entity.Set<kampen>().SqlQuery(@"select * from kampen order by datum_tot desc ").ToList();
            var ds = new ObservableCollection<kampen>(someDbSet.ToList());
            level0.DataSource = ds;
            settings.AddLevel(level0);
            SMMWindow2 w = new SMMWindow2(vc, settings);
            w.Show();
        }
        #endregion
        #region Instellingen
        private void mnuInstellingen_Click(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            if (GlobalData.Instance.LoggedOnUser.IsLoggedOn)
            {
                settingsWindow w = new settingsWindow(GlobalData.Instance.LoggedOnUser.Id.GetValueOrDefault());
                w.ShowDialog();
            }
        }
        private void mnuPaswoordWijzigen_Click(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            ChangePassword();
        }
        #endregion

        /** METHODS */
        private void RefreshWerkbonToolMenuItems()
        {
            biSuperVisor.Items.Clear();
            bWerkaanvragen.Items.Clear();
            biMijnWerkbonnen.Items.Clear();
            biSuperVisor.IsVisible = false;
            bWerkaanvragen.IsVisible = false;
            biMijnWerkbonnen.IsVisible = false;

            if (GlobalData.Instance.LoggedOnUser.IsLoggedOn)
            {
                foreach (string dienst in GlobalData.Instance.LoggedOnUser.SupervisorDiensten)
                {
                    biSuperVisor.IsVisible = true;
                    BarButtonItem item = new BarButtonItem() { Content = "Werkaanvragen " + dienst, Tag = dienst };
                    item.ItemClick += BiSuperVisorWA_ItemClick;
                    biSuperVisor.Items.Add(item);

                    item = new BarButtonItem() { Content = "Werkbon " + dienst, Tag = dienst };
                    item.ItemClick += BiSuperVisorWB_ItemClick1;
                    biSuperVisor.Items.Add(item);
                }

                foreach (werkopdracht_diensten dienst in GlobalData.Instance.LoggedOnUser.WerkaanvraagDiensten)
                {
                    bWerkaanvragen.IsVisible = true;
                    BarButtonItem item = new BarButtonItem() { Content = "Werkaanvragen voor " + dienst.wodienst_naam, Tag = dienst };
                    item.ItemClick += BiWerkaanvragen_ItemClick;
                    bWerkaanvragen.Items.Add(item);
                }

                foreach (String dienst in GlobalData.Instance.LoggedOnUser.WerkbonnenDiensten)
                {
                    biMijnWerkbonnen.IsVisible = true;
                    BarButtonItem item = new BarButtonItem() { Content = "Mijn werkbonnen " + dienst, Tag = dienst };
                    item.ItemClick += BiMijnWerkbonnen_ItemClick;
                    biMijnWerkbonnen.Items.Add(item);
                }
            }
        }
        private void SelectDatabase()
        {

#if  RELEASEPRODUCTIE
            sharedcode.common.GlobalData.Instance.ChangeDataBase(ClaraFeyDatabase.produktie);
#endif

#if DEBUGPRODUCTIE
            sharedcode.common.GlobalData.Instance.ChangeDataBase(ClaraFeyDatabase.produktie);
#endif

#if RELEASETEST
            sharedcode.common.GlobalData.Instance.ChangeDataBase(ClaraFeyDatabase.test);
#endif

#if DEBUGTEST
            sharedcode.common.GlobalData.Instance.ChangeDataBase(ClaraFeyDatabase.test);
#endif

        }
        private void SetDataBaseInfo()
        {
            lblDatabaseInfo.Content = "";
            if (GlobalData.Instance.GetConnectedDatabaseType() == ClaraFeyDatabase.test)
                lblDatabaseInfo.Content = "connected to the test database";
        }
        private void SaveProperties()
        {
            Properties.Settings.Default.lastRememberMe = checkBoxRemember.IsChecked.GetValueOrDefault();

            if (checkBoxRemember.IsChecked.GetValueOrDefault())
            {
                Properties.Settings.Default.lastPersoneelsNr = tbPersoneelsNummer.Text.Trim();
            }
            else
            {
                Properties.Settings.Default.lastPersoneelsNr = String.Empty;
            }

            Properties.Settings.Default.Save();
        }
        private static bool CheckOpenWindows()
        {
#if RELEASEPRODUCTIE
  if (GlobalMethods.IsWindowOpen<Window>("logonForm") && GlobalMethods.NumWindowOpen<Window>() == 1)
                return false;
            MessageBox.Show("Gelieve eerst alle schermen te sluiten.", "afsluiten", MessageBoxButton.OK, MessageBoxImage.Stop);
                  return true;
          
#endif

            return false;


        }
        private void ShutDownApplication(int seconds)
        {
            DoLogOff(true);
            GlobalData.Instance.GlobalShutDownTimer.ShutDownApplicationHardWay(seconds);
        }
        private void GlobalShutDownTimer_globalShutDownTimerUpdateAvailable(bool isUpdateAvailable, maintenance_message updateManagerInfo, bool immidiateShutdown)
        {
            if (isUpdateAvailable)
            {
                if (immidiateShutdown)
                {
                    frmMaintenanceMessageWindow w = new frmMaintenanceMessageWindow(updateManagerInfo, true);
                    ShutDownApplication(30);
                    w.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                    w.ShowDialog();
                }
                else
                {
                    if (UserDoNotWantsToSeeMaintenanceMessageAgain == false)
                    {
                        frmMaintenanceMessageWindow w = new frmMaintenanceMessageWindow(updateManagerInfo, false)
                        {
                            WindowStartupLocation = WindowStartupLocation.CenterScreen
                        };
                        w.ShowDialog();
                        if (w.DoNotShowMesssage)
                            UserDoNotWantsToSeeMaintenanceMessageAgain = true;
                    }
                }
            }
        }
        private  void DoLogOff(bool clearCache)
        {
            UserBLL.DoLogoff();
            RefreshWerkbonToolMenuItems();
            if (clearCache)
                ProjectMethods.ClearCache();
        }
        public string ValidateGroepCode(SMMWindow2Column col, object value, object vc)
        {
            if (String.IsNullOrWhiteSpace((string)value))
                return col.ColumnHeader + " mag niet leeg zijn.";
            if (((string)value).Length > 12)
                return col.ColumnHeader + " mag max.12 lang zijn.";

            return null;
        }     
        public void TelefoonboekDoubleClick(Object row)
        {

           // MessageBox.Show("bij de volgende update zal de grote foto weer getoond worden.");
            if (row == null)
                return;
            DataRow g =( (DataRowView)row).Row;
            if (g[0] == DBNull.Value)
                return;
            ImageEdit im = new ImageEdit()
            {
                Source =  GlobalSharedMethods.DecodePhoto((Byte[])g[0])
           };
            Window w = new Window()
            {
                ResizeMode = ResizeMode.NoResize,
                Title = g[2].ToString() + " " + g[3].ToString(),
                Width = 480,
                Height = 600,
                Content = im
            };
            w.ShowDialog(); 
        }
        private void OpenKilometerWindow(KilometerModeType mode, String windowName, String windowsTitle)
        {
            if (GlobalMethods.IsWindowOpen<Window>(windowName))
                GlobalMethods.ActivateWindown<Window>(windowName);
            else
            {
                this.Cursor = Cursors.Wait;
                KilometerWindow win = new KilometerWindow(mode)
                {
                    Name = windowName,
                    Title = windowsTitle
                };
                win.Show();
                this.Cursor = Cursors.Arrow;
            }
        }
        private void ChangePassword()
        {
            try
            {
                String persNr = String.Empty;
                String naam = String.Empty;
                if (sharedcode.common.GlobalData.Instance.LoggedOnUser.IsLoggedOn == false)
                {
                    SMMWindow2Settings settings = new SMMWindow2Settings("Wie bent u ?", GlobalMethods.Themes.normal) { IsChoiceWindow = true };
                    SMMWindow2Level level0 = new SMMWindow2Level(0, "Wie bent u ?") {/* MouseDoubleClickMethod = new SMMWindow2Level.MouseDoubleClick(this.TelefoonboekDoubleClick),*/ AllowDelete = false, AllowModify = false, AllowAdd = false, AllowSearch = true, AllowFilter = true };
                    level0.AddColumns
                    (
                        new SMMWindow2Column() { FieldName = "personeelscode", ColumnHeader = "Personeelsnr.", IsEditable = false },
                        new SMMWindow2Column() { FieldName = "voornaam", ColumnHeader = "voornaam", IsEditable = false },
                        new SMMWindow2Column() { FieldName = "achternaam", ColumnHeader = "achternaam", IsEditable = false },
                        new SMMWindow2Column() { FieldName = "werkplaats", ColumnHeader = "werkplaats", IsEditable = false, IsNullable = true }
                    );
                    smmWindowVC vc = new smmWindowVC();
                    var someDbSet = vc.Entity.Set<gebruiker>().SqlQuery("select * from gebruiker where wachtwoord=personeelscode").ToList();
                    var ds = new ObservableCollection<gebruiker>(someDbSet.ToList());
                    level0.DataSource = ds;
                    settings.AddLevel(level0);
                    SMMWindow2 w = new SMMWindow2(vc, settings)
                    {
                        WindowStartupLocation = WindowStartupLocation.CenterScreen
                    };
                    if (w.ShowDialog().GetValueOrDefault())
                    {
                        gebruiker g = (gebruiker)vc.SelectedItem0;
                        if (g != null && MessageBox.Show(String.Format("Bent u {0} {1} ({2}) ", g.voornaam, g.achternaam, g.personeelscode), "bevestigen", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                        {
                            persNr = g.personeelscode;
                            naam = g.voornaam + " " + g.achternaam;
                        }
                        else
                            return;
                    }
                    else
                        return;
                }
                else
                {
                    persNr = GlobalData.Instance.LoggedOnUser.PersNr;
                    naam = GlobalData.Instance.LoggedOnUser.Name;
                }

                RegistratieWindow regWindow = new RegistratieWindow(naam);
                if (regWindow.ShowDialog().GetValueOrDefault())
                {
                    UserBLL.ChangePassword(persNr, regWindow.newPaswoord);
                    MessageBox.Show(naam + ", u kan inloggen met uw nieuw paswoord");
                    if (GlobalData.Instance.LoggedOnUser.IsLoggedOn)
                        DoLogOff(false);
                    tbPaswoord.Text = String.Empty;
                    tbPersoneelsNummer.Text = persNr;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + ex.InnerException, "Fout", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

#region Notifications
        public void ShowCustomNotification(String caption, String text, byte[] image)
        {
            ClaraFey.Notifications.MainViewModel mv = (ClaraFey.Notifications.MainViewModel)this.viewNotifications.DataContext;
            mv.ShowCustomNotification(caption, text, image);
        }
        public void ShowCustomNotification(String caption, String text, User user)
        {
            ClaraFey.Notifications.MainViewModel mv = (ClaraFey.Notifications.MainViewModel)this.viewNotifications.DataContext;
            mv.ShowCustomNotification(caption, text, user);
        }
        public void ShowCustomNotificationBewoner(NotificationSubModuleType subModule, User User, string caption,
                                                  string text, bewoner bewoner, NotificationDurationType duration,
                                                  bool canGotoModule)
        {
            MainViewModel mv = (MainViewModel)viewNotifications.DataContext;
            mv.ShowCustomNotificationBewoner(subModule, User, caption, text, bewoner, duration, canGotoModule);
        }


        #endregion

        private void biAankoopIMAflsuit_ItemClick(object sender, ItemClickEventArgs e)
        {
            AankoopIMAfsluitWindow w = new AankoopIMAfsluitWindow();
            w.Show();

        }

        private void biAankoopIMApp_ItemClick(object sender, ItemClickEventArgs e)
        {
            GlobalMethods.StartBrowserWithBlazorApp("aankoop");
        }
    }
}
