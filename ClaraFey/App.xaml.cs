﻿using DevExpress.Xpf.Core;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;

namespace ClaraFey
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private void OnAppStartup_UpdateThemeName(object sender, StartupEventArgs e)
        {
 
        }
        protected override void OnStartup(StartupEventArgs e)
        {
            Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("nl-NL");
            Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("nl-NL");
            base.OnStartup(e);
            // here you take control
        }
        private void UnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {

            MessageBox.Show(e.Exception.Message + "\r\n" + e.Exception.StackTrace);
            var time = DateTime.Now;
            //File.AppendAllText(@"C:\temp\clarafey_wpf_errorlog.csv", string.Format("{0},{1}", time, e.Exception.Message));
            //File.AppendAllText(@"C:\temp\clarafey_wpf_errorlog_stacktrace.txt", time + ":\n" + e.Exception.StackTrace + "\n");
            e.Handled = false; 
        }

    }
}
